// Copyright 2023 The libmpfr-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libmpfr is a ccgo/v4 version of libmpfr.a (https://mpfr.org)
package libmpfr // import "modernc.org/libmpfr"

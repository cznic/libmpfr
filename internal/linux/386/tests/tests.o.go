// Code generated for linux/386 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DMPFR_LONG_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/386 -UHAVE_NEARBYINT -mlong-double-64 -c tests.c -o tests.o.go', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvBUS_ADRALN = 1
const mvBUS_ADRERR = 2
const mvBUS_MCEERR_AO = 5
const mvBUS_MCEERR_AR = 4
const mvBUS_OBJERR = 3
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCLD_CONTINUED = 6
const mvCLD_DUMPED = 3
const mvCLD_EXITED = 1
const mvCLD_KILLED = 2
const mvCLD_STOPPED = 5
const mvCLD_TRAPPED = 4
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.2250738585072014e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFD_SETSIZE = 1024
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.1754943508222875e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvFPE_FLTDIV = 3
const mvFPE_FLTINV = 7
const mvFPE_FLTOVF = 4
const mvFPE_FLTRES = 6
const mvFPE_FLTSUB = 8
const mvFPE_FLTUND = 5
const mvFPE_INTDIV = 1
const mvFPE_INTOVF = 2
const mvGMP_LIMB_BITS = 32
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvILL_BADSTK = 8
const mvILL_COPROC = 7
const mvILL_ILLADR = 3
const mvILL_ILLOPC = 1
const mvILL_ILLOPN = 2
const mvILL_ILLTRP = 4
const mvILL_PRVOPC = 5
const mvILL_PRVREG = 6
const mvINT_MAX = 2147483647
const mvIOV_MAX = 1024
const mvITIMER_PROF = 2
const mvITIMER_REAL = 0
const mvITIMER_VIRTUAL = 1
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMINSIGSTKSZ = 2048
const mvMPFR_AI_THRESHOLD2 = 2303
const mvMPFR_AI_THRESHOLD3 = 37484
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 522
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 6920
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 5
const mvMPFR_LOG2_PREC_BITS = 5
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 13
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 32
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 28160
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 18
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/x86/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNSIG = "_NSIG"
const mvNZERO = 20
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPOLL_ERR = 4
const mvPOLL_HUP = 6
const mvPOLL_IN = 1
const mvPOLL_MSG = 3
const mvPOLL_OUT = 2
const mvPOLL_PRI = 5
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSA_EXPOSE_TAGBITS = 0x00000800
const mvSA_NOCLDSTOP = 1
const mvSA_NOCLDWAIT = 2
const mvSA_NODEFER = 0x40000000
const mvSA_NOMASK = "SA_NODEFER"
const mvSA_ONESHOT = "SA_RESETHAND"
const mvSA_ONSTACK = 0x08000000
const mvSA_RESETHAND = 0x80000000
const mvSA_RESTART = 0x10000000
const mvSA_RESTORER = 0x04000000
const mvSA_SIGINFO = 4
const mvSA_UNSUPPORTED = 0x00000400
const mvSCHAR_MAX = 127
const mvSEGV_ACCERR = 2
const mvSEGV_BNDERR = 3
const mvSEGV_MAPERR = 1
const mvSEGV_MTEAERR = 8
const mvSEGV_MTESERR = 9
const mvSEGV_PKUERR = 4
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSIGABRT = 6
const mvSIGALRM = 14
const mvSIGBUS = 7
const mvSIGCHLD = 17
const mvSIGCONT = 18
const mvSIGEV_NONE = 1
const mvSIGEV_SIGNAL = 0
const mvSIGEV_THREAD = 2
const mvSIGEV_THREAD_ID = 4
const mvSIGFPE = 8
const mvSIGHUP = 1
const mvSIGILL = 4
const mvSIGINT = 2
const mvSIGIO = 29
const mvSIGIOT = "SIGABRT"
const mvSIGKILL = 9
const mvSIGPIPE = 13
const mvSIGPOLL = 29
const mvSIGPROF = 27
const mvSIGPWR = 30
const mvSIGQUIT = 3
const mvSIGSEGV = 11
const mvSIGSTKFLT = 16
const mvSIGSTKSZ = 8192
const mvSIGSTOP = 19
const mvSIGSYS = 31
const mvSIGTERM = 15
const mvSIGTRAP = 5
const mvSIGTSTP = 20
const mvSIGTTIN = 21
const mvSIGTTOU = 22
const mvSIGUNUSED = "SIGSYS"
const mvSIGURG = 23
const mvSIGUSR1 = 10
const mvSIGUSR2 = 12
const mvSIGVTALRM = 26
const mvSIGWINCH = 28
const mvSIGXCPU = 24
const mvSIGXFSZ = 25
const mvSIG_BLOCK = 0
const mvSIG_SETMASK = 2
const mvSIG_UNBLOCK = 1
const mvSI_KERNEL = 128
const mvSI_USER = 0
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSS_DISABLE = 2
const mvSS_FLAG_BITS = "SS_AUTODISARM"
const mvSS_ONSTACK = 1
const mvSYMLOOP_MAX = 40
const mvSYS_SECCOMP = 1
const mvSYS_USER_DISPATCH = 2
const mvTMP_MAX = 10000
const mvTRAP_BRANCH = 3
const mvTRAP_BRKPT = 1
const mvTRAP_HWBKPT = 4
const mvTRAP_TRACE = 2
const mvTRAP_UNK = 5
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_ILP32 = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_NSIG = 65
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_REDIR_TIME64 = 1
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 2
const mv__FLT_EVAL_METHOD__ = 2
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "i686-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__ILP32__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LAHF_SAHF__ = 1
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 2147483647
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 12
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__code_model_32__ = 1
const mv__gnu_linux__ = 1
const mv__i386 = 1
const mv__i386__ = 1
const mv__i686 = 1
const mv__i686__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pentiumpro = 1
const mv__pentiumpro__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__ucontext = "ucontext"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvi386 = 1
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint32

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppint8
	fdfrac_digits        ppint8
	fdp_cs_precedes      ppint8
	fdp_sep_by_space     ppint8
	fdn_cs_precedes      ppint8
	fdn_sep_by_space     ppint8
	fdp_sign_posn        ppint8
	fdn_sign_posn        ppint8
	fdint_p_cs_precedes  ppint8
	fdint_p_sep_by_space ppint8
	fdint_n_cs_precedes  ppint8
	fdint_n_sep_by_space ppint8
	fdint_p_sign_posn    ppint8
	fdint_n_sign_posn    ppint8
}

type tnlocale_t = ppuintptr

type tnsize_t = ppuint32

type tntime_t = ppint64

type tnsuseconds_t = ppint64

type tstimeval = struct {
	fdtv_sec  tntime_t
	fdtv_usec tnsuseconds_t
}

type tstimespec = struct {
	fdtv_sec   tntime_t
	fdtv_nsec  ppint32
	fd__ccgo12 uint32
}

type tnsigset_t = struct {
	fd__bits [32]ppuint32
}

type ts__sigset_t = tnsigset_t

type tnfd_mask = ppuint32

type tnfd_set = struct {
	fdfds_bits [32]ppuint32
}

type tsitimerval = struct {
	fdit_interval tstimeval
	fdit_value    tstimeval
}

type tstimezone = struct {
	fdtz_minuteswest ppint32
	fdtz_dsttime     ppint32
}

type tnclock_t = ppint32

type tnpid_t = ppint32

type tnuid_t = ppuint32

type tnpthread_t = ppuintptr

type tnpthread_attr_t = struct {
	fd__u struct {
		fd__vi [0][9]ppint32
		fd__s  [0][9]ppuint32
		fd__i  [9]ppint32
	}
}

type tnstack_t = struct {
	fdss_sp    ppuintptr
	fdss_flags ppint32
	fdss_size  tnsize_t
}

type tssigaltstack = tnstack_t

const ecREG_GS = 0
const ecREG_FS = 1
const ecREG_ES = 2
const ecREG_DS = 3
const ecREG_EDI = 4
const ecREG_ESI = 5
const ecREG_EBP = 6
const ecREG_ESP = 7
const ecREG_EBX = 8
const ecREG_EDX = 9
const ecREG_ECX = 10
const ecREG_EAX = 11
const ecREG_TRAPNO = 12
const ecREG_ERR = 13
const ecREG_EIP = 14
const ecREG_CS = 15
const ecREG_EFL = 16
const ecREG_UESP = 17
const ecREG_SS = 18

type tngreg_t = ppint32

type tngregset_t = [19]ppint32

type tnfpregset_t = ppuintptr

type ts_fpstate = struct {
	fdcw      ppuint32
	fdsw      ppuint32
	fdtag     ppuint32
	fdipoff   ppuint32
	fdcssel   ppuint32
	fddataoff ppuint32
	fddatasel ppuint32
	fd_st     [8]struct {
		fdsignificand [4]ppuint16
		fdexponent    ppuint16
	}
	fdstatus ppuint32
}

type tssigcontext = struct {
	fdgs            ppuint16
	fd__gsh         ppuint16
	fdfs            ppuint16
	fd__fsh         ppuint16
	fdes            ppuint16
	fd__esh         ppuint16
	fdds            ppuint16
	fd__dsh         ppuint16
	fdedi           ppuint32
	fdesi           ppuint32
	fdebp           ppuint32
	fdesp           ppuint32
	fdebx           ppuint32
	fdedx           ppuint32
	fdecx           ppuint32
	fdeax           ppuint32
	fdtrapno        ppuint32
	fderr           ppuint32
	fdeip           ppuint32
	fdcs            ppuint16
	fd__csh         ppuint16
	fdeflags        ppuint32
	fdesp_at_signal ppuint32
	fdss            ppuint16
	fd__ssh         ppuint16
	fdfpstate       ppuintptr
	fdoldmask       ppuint32
	fdcr2           ppuint32
}

type tnmcontext_t = struct {
	fdgregs   tngregset_t
	fdfpregs  tnfpregset_t
	fdoldmask ppuint32
	fdcr2     ppuint32
}

type tnucontext_t = struct {
	fduc_flags     ppuint32
	fduc_link      ppuintptr
	fduc_stack     tnstack_t
	fduc_mcontext  tnmcontext_t
	fduc_sigmask   tnsigset_t
	fd__fpregs_mem [28]ppuint32
}

type tsucontext = tnucontext_t

type tusigval = struct {
	fdsival_ptr [0]ppuintptr
	fdsival_int ppint32
}

type tnsiginfo_t = struct {
	fdsi_signo    ppint32
	fdsi_errno    ppint32
	fdsi_code     ppint32
	fd__si_fields struct {
		fd__si_common [0]struct {
			fd__first struct {
				fd__timer [0]struct {
					fdsi_timerid ppint32
					fdsi_overrun ppint32
				}
				fd__piduid struct {
					fdsi_pid tnpid_t
					fdsi_uid tnuid_t
				}
			}
			fd__second struct {
				fd__sigchld [0]struct {
					fdsi_status ppint32
					fdsi_utime  tnclock_t
					fdsi_stime  tnclock_t
				}
				fdsi_value    tusigval
				fd__ccgo_pad2 [8]byte
			}
		}
		fd__sigfault [0]struct {
			fdsi_addr     ppuintptr
			fdsi_addr_lsb ppint16
			fd__first     struct {
				fdsi_pkey    [0]ppuint32
				fd__addr_bnd struct {
					fdsi_lower ppuintptr
					fdsi_upper ppuintptr
				}
			}
		}
		fd__sigpoll [0]struct {
			fdsi_band ppint32
			fdsi_fd   ppint32
		}
		fd__sigsys [0]struct {
			fdsi_call_addr ppuintptr
			fdsi_syscall   ppint32
			fdsi_arch      ppuint32
		}
		fd__pad [116]ppint8
	}
}

type tssigaction = struct {
	fd__sa_handler struct {
		fdsa_sigaction [0]ppuintptr
		fdsa_handler   ppuintptr
	}
	fdsa_mask     tnsigset_t
	fdsa_flags    ppint32
	fdsa_restorer ppuintptr
}

type tssigevent = struct {
	fdsigev_value  tusigval
	fdsigev_signo  ppint32
	fdsigev_notify ppint32
	fd__sev_fields struct {
		fdsigev_notify_thread_id [0]tnpid_t
		fd__sev_thread           [0]struct {
			fdsigev_notify_function   ppuintptr
			fdsigev_notify_attributes ppuintptr
		}
		fd__pad [52]ppint8
	}
}

type tnsig_t = ppuintptr

type tnsighandler_t = ppuintptr

type tnsig_atomic_t = ppint32

type tnssize_t = ppint32

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint32

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint32

type tnmpfr_ulong = ppuint32

type tnmpfr_size_t = ppuint32

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint32

type tnmpfr_uprec_t = ppuint32

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint32

type tnmpfr_uexp_t = ppuint32

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint32

type tnmpfr_ueexp_t = ppuint32

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

var Xmpfr_rands_initialized ppint8
var Xmpfr_rands tngmp_randstate_t

var Xlocale ppuintptr

// C documentation
//
//	/* Programs that test GMP's mp_set_memory_functions() need to set
//	   tests_memory_disabled = 2 before calling tests_start_mpfr(). */
var Xtests_memory_disabled ppint32

var sidefault_emin tnmpfr_exp_t
var sidefault_emax tnmpfr_exp_t
var Xdummy_func = ppuintptr(0)

func init() {
	p := iqunsafe.ppPointer(&Xdummy_func)
	*(*uintptr)(iqunsafe.ppAdd(p, 0)) = pp__ccgo_fp(Xmpfr_dump)
}

// C documentation
//
//	/* Various version checks.
//	   A mismatch on the GMP version is not regarded as fatal. A mismatch
//	   on the MPFR version is regarded as fatal, since this means that we
//	   would not check the MPFR library that has just been built (the goal
//	   of "make check") but a different library that is already installed,
//	   i.e. any test result would be meaningless; in such a case, we exit
//	   immediately with an error (exit status = 1).
//	   Return value: 0 for no errors, 1 in case of any non-fatal error.
//	   Note: If the return value is 0, no data must be sent to stdout. */
func Xtest_version(cgtls *iqlibc.ppTLS) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(288)
	defer cgtls.ppFree(288)

	var aaerr, aai ppint32
	var aaversion, ccv4, ccv5 ppuintptr
	var ccv1, ccv2 ppbool
	var pp_ /* buffer at bp+0 */ [256]ppint8
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaerr, aai, aaversion, ccv1, ccv2, ccv4, ccv5
	aaerr = 0

	Xsprintf(cgtls, cgbp, "%d.%d.%d\x00", iqlibc.ppVaList(cgbp+264, ppint32(mv__GNU_MP_VERSION), ppint32(mv__GNU_MP_VERSION_MINOR), ppint32(mv__GNU_MP_VERSION_PATCHLEVEL)))

	if ccv2 = Xstrcmp(cgtls, cgbp, X__gmp_version) != 0; ccv2 {
		if ccv1 = iqlibc.Bool(ppint32(mv__GNU_MP_VERSION_PATCHLEVEL) != 0); !ccv1 {
			Xsprintf(cgtls, cgbp, "%d.%d\x00", iqlibc.ppVaList(cgbp+264, ppint32(mv__GNU_MP_VERSION), ppint32(mv__GNU_MP_VERSION_MINOR)))
		}
	}
	if ccv2 && iqlibc.Bool(ccv1 || Xstrcmp(cgtls, cgbp, X__gmp_version) != iqlibc.ppInt32FromInt32(0)) {
		aaerr = ppint32(1)
	}

	/* In some cases, it may be acceptable to have different versions for
	   the header and the library, in particular when shared libraries are
	   used (e.g., after a bug-fix upgrade of the library, and versioning
	   ensures that this can be done only when the binary interface is
	   compatible). However, when recompiling software like here, this
	   should never happen (except if GMP has been upgraded between two
	   "make check" runs, but there's no reason for that). A difference
	   between the versions of gmp.h and libgmp probably indicates either
	   a bad configuration or some other inconsistency in the development
	   environment, and it is better to fail (in particular for automatic
	   installations). */
	if aaerr != 0 {

		Xprintf(cgtls, "ERROR! The versions of gmp.h (%s) and libgmp (%s) do not match.\nThe possible causes are:\n\x00", iqlibc.ppVaList(cgbp+264, cgbp, X__gmp_version))
		Xprintf(cgtls, "  * A bad configuration in your include/library search paths.\n  * An inconsistency in the include/library search paths of\n    your development environment; an example:\n      https://gcc.gnu.org/legacy-ml/gcc-help/2010-11/msg00359.html\n  * GMP has been upgraded after the first \"make check\".\n    In such a case, try again after a \"make clean\".\n  * A new or non-standard version naming is used in GMP.\n    In this case, a patch may already be available on the\n    MPFR web site.  Otherwise please report the problem.\n\x00", 0)
		Xprintf(cgtls, "In the first two cases, this may lead to errors, in particular with MPFR.\nIf some other tests fail, please solve that problem first.\n\x00", 0)
	}

	/* VL: I get the following error on an OpenSUSE machine, and changing
	   the value of shlibpath_overrides_runpath in the libtool file from
	   'no' to 'yes' fixes the problem. */
	aaversion = Xmpfr_get_version(cgtls)
	if Xstrcmp(cgtls, "4.2.0\x00", aaversion) == 0 {

		Xsprintf(cgtls, cgbp, "%d.%d.%d\x00", iqlibc.ppVaList(cgbp+264, ppint32(mvMPFR_VERSION_MAJOR), ppint32(mvMPFR_VERSION_MINOR), mvMPFR_VERSION_PATCHLEVEL))
		aai = 0
		for {
			if !(ppint32((*(*[256]ppint8)(iqunsafe.ppPointer(cgbp)))[aai]) == ppint32(*(*ppint8)(iqunsafe.ppPointer(aaversion + ppuintptr(aai))))) {
				break
			}
			if ppint32((*(*[256]ppint8)(iqunsafe.ppPointer(cgbp)))[aai]) == ppint32('\000') {
				return aaerr
			}
			goto cg_3
		cg_3:
			;
			aai++
		}
		if ppint32((*(*[256]ppint8)(iqunsafe.ppPointer(cgbp)))[aai]) == ppint32('\000') && ppint32(*(*ppint8)(iqunsafe.ppPointer(aaversion + ppuintptr(aai)))) == ppint32('-') {
			return aaerr
		}
		if aaerr != 0 {
			ccv4 = "\n\x00"
		} else {
			ccv4 = "\x00"
		}
		Xprintf(cgtls, "%sMPFR_VERSION_MAJOR.MPFR_VERSION_MINOR.MPFR_VERSION_PATCHLEVEL (%s)\nand MPFR_VERSION_STRING (%s) do not match!\nIt seems that the mpfr.h file has been corrupted.\n\x00", iqlibc.ppVaList(cgbp+264, ccv4, cgbp, aaversion))
	} else {
		if aaerr != 0 {
			ccv5 = "\n\x00"
		} else {
			ccv5 = "\x00"
		}
		Xprintf(cgtls, "%sIncorrect MPFR version! (%s header vs %s library)\nNothing else has been tested since for this reason, any other test\nmay fail.  Please fix this problem first, as suggested below.  It\nprobably comes from libtool (included in the MPFR tarball), which\nis responsible for setting up the search paths depending on the\nplatform, or automake.\n  * On some platforms such as Solaris, $LD_LIBRARY_PATH overrides\n    the rpath, and if the MPFR library is already installed in a\n    $LD_LIBRARY_PATH directory, you typically get this error.  Do\n    not use $LD_LIBRARY_PATH permanently on such platforms; it may\n    also break other things.\n  * You may have an ld option that specifies a library search path\n    where MPFR can be found, taking the precedence over the path\n    added by libtool.  Check your environment variables, such as\n    LD_OPTIONS under Solaris.  Moreover, under Solaris, the run path\n    generated by libtool 2.4.6 may be incorrect: the build directory\n    may not appear first in the run path; set $LD_LIBRARY_PATH to\n    /path/to/builddir/src/.libs for the tests as a workaround.\n  * Then look at https://www.mpfr.org/mpfr-current/ for any update.\n  * Try again on a completely clean source (some errors might come\n    from a previous build or previous source changes).\n  * If the error still occurs, you can try to change the value of\n    shlibpath_overrides_runpath ('yes' or 'no') in the \"libtool\"\n    file and rebuild MPFR (make clean && make && make check).  You\n    may want to report the problem to the libtool and/or automake\n    developers, with the effect of this change.\n\x00", iqlibc.ppVaList(cgbp+264, ccv5, "4.2.0\x00", aaversion))
	}
	/* Note about $LD_LIBRARY_PATH under Solaris:
	 *   https://en.wikipedia.org/wiki/Rpath#Solaris_ld.so
	 * This cause has been confirmed by a user who got this error.
	 * And about the libtool 2.4.6 bug also concerning Solaris:
	 *   https://debbugs.gnu.org/cgi/bugreport.cgi?bug=30222
	 *   https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=888059
	 */
	Xexit(cgtls, ppint32(1))
	return cgr
}

/* The inexact exception occurs very often, and is normal.
   The underflow exception also might occur, for example in test_generic
   for mpfr_xxx_d functions. Same for overflow. Thus we only check for
   the division-by-zero and invalid exceptions, which should not occur
   inside MPFR. */

func Xtests_start_mpfr(cgtls *iqlibc.ppTLS) {

	/* Don't buffer, so output is not lost if a test causes a segv, etc.
	   For stdout, this is important as it will typically be fully buffered
	   by default with "make check". For stderr, the C standard just says
	   that it is not fully buffered (it may be line buffered by default);
	   disabling buffering completely might be useful in some cases.
	   Warning! No operations must have already been done on stdout/stderr
	   (this is a requirement of ISO C, and this is important on AIX).
	   Thus tests_start_mpfr should be called at the beginning of main(),
	   possibly after some variable settings. */
	Xsetbuf(cgtls, Xstdout, iqlibc.ppUintptrFromInt32(0))
	Xsetbuf(cgtls, Xstderr, iqlibc.ppUintptrFromInt32(0))

	Xtest_version(cgtls)

	/* Added on 2005-07-09. This allows to test MPFR under various
	   locales. New bugs will probably be found, in particular with
	   LC_ALL="tr_TR.ISO8859-9" because of the i/I character... */
	Xlocale = Xsetlocale(cgtls, ppint32(mvLC_ALL), "\x00")

	if Xtests_memory_disabled != ppint32(2) {

		if Xtests_memory_disabled == 0 {
			Xtests_memory_start(cgtls)
		}
		sitests_rand_start(cgtls)
	}
	sitests_limit_start(cgtls)

	sidefault_emin = X__gmpfr_emin
	sidefault_emax = X__gmpfr_emax
}

func Xtests_end_mpfr(cgtls *iqlibc.ppTLS) {

	var aaerr ppint32
	pp_ = aaerr
	aaerr = 0

	if X__gmpfr_emin != sidefault_emin {

		Xprintf(cgtls, "Default emin value has not been restored!\n\x00", 0)
		aaerr = ppint32(1)
	}

	if X__gmpfr_emax != sidefault_emax {

		Xprintf(cgtls, "Default emax value has not been restored!\n\x00", 0)
		aaerr = ppint32(1)
	}

	Xmpfr_free_cache(cgtls)
	Xmpfr_free_cache2(cgtls, ppint32(ecMPFR_FREE_GLOBAL_CACHE))
	if Xtests_memory_disabled != ppint32(2) {

		sitests_rand_end(cgtls)
		if Xtests_memory_disabled == 0 {
			Xtests_memory_end(cgtls)
		}
	}

	if aaerr != 0 {
		Xexit(cgtls, aaerr)
	}
}

func sitests_limit_start(cgtls *iqlibc.ppTLS) {

}

func sitests_rand_start(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aaperform_seed ppuintptr
	var aaseed ppuint32
	var pp_ /* tv at bp+0 */ tstimeval
	pp_, pp_ = aaperform_seed, aaseed

	if Xmpfr_rands_initialized != 0 {

		Xprintf(cgtls, "Please let tests_start() initialize the global mpfr_rands, i.e.\nensure that function is called before the first use of RANDS.\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_rands_initialized = ppint8(1)

	aaperform_seed = Xgetenv(cgtls, "GMP_CHECK_RANDOMIZE\x00")
	if aaperform_seed != iqlibc.ppUintptrFromInt32(0) {

		aaseed = Xstrtoul(cgtls, aaperform_seed, iqlibc.ppUintptrFromInt32(0), ppint32(10))
		if !(aaseed == ppuint32(0) || aaseed == ppuint32(1)) {

			Xprintf(cgtls, "Re-seeding with GMP_CHECK_RANDOMIZE=%lu\n\x00", iqlibc.ppVaList(cgbp+24, aaseed))
			X__gmp_randseed_ui(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), aaseed)
		} else {

			Xgettimeofday(cgtls, cgbp, iqlibc.ppUintptrFromInt32(0))
			/* Note: If time_t is a "floating type" (as allowed by ISO C99),
			   the cast below can yield undefined behavior. But this would
			   be uncommon (gettimeofday() is specified by POSIX only and
			   POSIX requires time_t to be an integer type) and this line
			   is not executed by default. So, this should be OK. Moreover,
			   gettimeofday() is marked obsolescent by POSIX.1-2008. */
			aaseed = iqlibc.ppUint32FromInt64(iqlibc.ppInt64FromUint32(ppuint32(1000000)*iqlibc.ppUint32FromInt64((*(*tstimeval)(iqunsafe.ppPointer(cgbp))).fdtv_sec)) + (*(*tstimeval)(iqunsafe.ppPointer(cgbp))).fdtv_usec)
			X__gmp_randseed_ui(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), aaseed)
			Xprintf(cgtls, "Seed GMP_CHECK_RANDOMIZE=%lu (include this in bug reports)\n\x00", iqlibc.ppVaList(cgbp+24, aaseed))
		}
	} else {
		X__gmp_randseed_ui(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppuint32(0x2143FEDC))
	}
}

func sitests_rand_end(cgtls *iqlibc.ppTLS) {

	if Xmpfr_rands_initialized != 0 {
		Xmpfr_rands_initialized = 0
		X__gmp_randclear(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	}
}

// C documentation
//
//	/* true if subnormals are supported for double */
func Xhave_subnorm_dbl(cgtls *iqlibc.ppTLS) (cgr ppint32) {

	var aax, aay ppfloat64
	pp_, pp_ = aax, aay
	aax = ppfloat64(2.2250738585072014e-308)

	aay = aax / ppfloat64(2)
	return iqlibc.ppBoolInt32(ppfloat64(2)*aay == aax)
}

// C documentation
//
//	/* true if subnormals are supported for float */
func Xhave_subnorm_flt(cgtls *iqlibc.ppTLS) (cgr ppint32) {

	var aax, aay ppfloat32
	pp_, pp_ = aax, aay
	aax = iqlibc.ppFloat32FromFloat32(1.1754943508222875e-38)

	aay = aax / iqlibc.ppFloat32FromFloat32(2)
	return iqlibc.ppBoolInt32(iqlibc.ppFloat32FromFloat32(2)*aay == aax)
}

// C documentation
//
//	/* initialization function for tests using the hardware floats
//	   Not very useful now. */
func Xmpfr_test_init(cgtls *iqlibc.ppTLS) {

	/* generate DBL_EPSILON with a loop to avoid that the compiler
	   optimizes the code below in non-IEEE 754 mode, deciding that
	   c = d is always false. */
}

// C documentation
//
//	/* generate a random limb */
func Xrandlimb(cgtls *iqlibc.ppTLS) (cgr tnmp_limb_t) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var pp_ /* limb at bp+0 */ tnmp_limb_t

	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_rand_raw(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
	return *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp))
}

func Xrandulong(cgtls *iqlibc.ppTLS) (cgr ppuint32) {

	return Xrandlimb(cgtls)

}

func Xrandlong(cgtls *iqlibc.ppTLS) (cgr ppint32) {

	var aau ppuint32
	var ccv1 ppint32
	pp_, pp_ = aau, ccv1
	aau = Xrandulong(cgtls)

	if aau > ppuint32(0x7fffffff) {
		ccv1 = ppint32(-ppint32(1)) - iqlibc.ppInt32FromUint32(^aau)
	} else {
		ccv1 = iqlibc.ppInt32FromUint32(aau)
	}
	return ccv1
}

// C documentation
//
//	/* returns ulp(x) for x a 'normal' double-precision number */
func XUlp(cgtls *iqlibc.ppTLS, aax ppfloat64) (cgr ppfloat64) {

	var aaeps, aay, ccv1 ppfloat64
	pp_, pp_, pp_ = aaeps, aay, ccv1

	if aax < iqlibc.ppFloat64FromInt32(0) {
		aax = -aax
	}

	aay = aax * ppfloat64(2.220446049250313e-16) /* x / 2^52 */

	/* as ulp(x) <= y = x/2^52 < 2*ulp(x),
	   we have x + ulp(x) <= x + y <= x + 2*ulp(x),
	   therefore o(x + y) = x + ulp(x) or x + 2*ulp(x) */

	aaeps = aax + aay
	aaeps = aaeps - aax /* ulp(x) or 2*ulp(x) */

	if aaeps > aay {
		ccv1 = ppfloat64(0.5) * aaeps
	} else {
		ccv1 = aaeps
	}
	return ccv1
}

// C documentation
//
//	/* returns the number of ulp's between a and b,
//	   where a and b can be any floating-point number, except NaN
//	 */
func Xulp(cgtls *iqlibc.ppTLS, aaa ppfloat64, aab ppfloat64) (cgr ppint32) {

	var aatwoa ppfloat64
	var ccv1 ppint32
	pp_, pp_ = aatwoa, ccv1

	if aaa == aab {
		return 0
	} /* also deals with a=b=inf or -inf */

	aatwoa = aaa + aaa
	if aatwoa == aaa { /* a is +/-0.0 or +/-Inf */
		if aab < aaa {
			ccv1 = ppint32(mvINT_MAX)
		} else {
			ccv1 = -ppint32(mvINT_MAX)
		}
		return ccv1
	}

	return ppint32((aaa - aab) / XUlp(cgtls, aaa))
}

// C documentation
//
//	/* return double m*2^e */
func Xdbl(cgtls *iqlibc.ppTLS, aam ppfloat64, aae ppint32) (cgr ppfloat64) {

	var ccv1, ccv2 ppint32
	pp_, pp_ = ccv1, ccv2
	if aae >= 0 {
		for {
			ccv1 = aae
			aae--
			if !(ccv1 > 0) {
				break
			}

			aam *= ppfloat64(2)
		}
	} else {
		for {
			ccv2 = aae
			aae++
			if !(ccv2 < 0) {
				break
			}

			aam /= ppfloat64(2)
		}
	}
	return aam
}

// C documentation
//
//	/* Warning: NaN values cannot be distinguished if MPFR_NANISNAN is defined. */
func XIsnan(cgtls *iqlibc.ppTLS, aad ppfloat64) (cgr ppint32) {

	return iqlibc.ppBoolInt32(aad != aad)
}

func Xd_trace(cgtls *iqlibc.ppTLS, aaname ppuintptr, aad ppfloat64) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aai ppint32
	var aap ppuintptr
	var pp_ /* x at bp+0 */ ppfloat64
	pp_, pp_ = aai, aap
	*(*ppfloat64)(iqunsafe.ppPointer(cgbp)) = aad
	aap = cgbp

	if aaname != iqlibc.ppUintptrFromInt32(0) && ppint32(*(*ppint8)(iqunsafe.ppPointer(aaname))) != ppint32('\000') {
		Xprintf(cgtls, "%s = \x00", iqlibc.ppVaList(cgbp+16, aaname))
	}

	Xprintf(cgtls, "[\x00", 0)
	aai = 0
	for {
		if !(aai < iqlibc.ppInt32FromInt64(8)) {
			break
		}

		if aai != 0 {
			Xprintf(cgtls, " \x00", 0)
		}
		Xprintf(cgtls, "%02X\x00", iqlibc.ppVaList(cgbp+16, ppuint32(*(*ppuint8)(iqunsafe.ppPointer(aap + ppuintptr(aai))))))

		goto cg_1
	cg_1:
		;
		aai++
	}
	Xprintf(cgtls, "] %.20g\n\x00", iqlibc.ppVaList(cgbp+16, aad))
}

func Xld_trace(cgtls *iqlibc.ppTLS, aaname ppuintptr, aald ppfloat64) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aai ppint32
	var aap ppuintptr
	var pp_ /* x at bp+0 */ ppfloat64
	pp_, pp_ = aai, aap
	*(*ppfloat64)(iqunsafe.ppPointer(cgbp)) = aald
	aap = cgbp

	if aaname != iqlibc.ppUintptrFromInt32(0) && ppint32(*(*ppint8)(iqunsafe.ppPointer(aaname))) != ppint32('\000') {
		Xprintf(cgtls, "%s = \x00", iqlibc.ppVaList(cgbp+16, aaname))
	}

	Xprintf(cgtls, "[\x00", 0)
	aai = 0
	for {
		if !(aai < iqlibc.ppInt32FromInt64(8)) {
			break
		}

		if aai != 0 {
			Xprintf(cgtls, " \x00", 0)
		}
		Xprintf(cgtls, "%02X\x00", iqlibc.ppVaList(cgbp+16, ppuint32(*(*ppuint8)(iqunsafe.ppPointer(aap + ppuintptr(aai))))))

		goto cg_1
	cg_1:
		;
		aai++
	}
	Xprintf(cgtls, "] %.20Lg\n\x00", iqlibc.ppVaList(cgbp+16, aald))
}

func Xn_trace(cgtls *iqlibc.ppTLS, aaname ppuintptr, aap ppuintptr, aan tnmp_size_t) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aabuf ppuintptr
	var aabufsize tnsize_t
	var aai, aam, ccv1 tnmp_size_t
	pp_, pp_, pp_, pp_, pp_ = aabuf, aabufsize, aai, aam, ccv1

	if aaname != iqlibc.ppUintptrFromInt32(0) && ppint32(*(*ppint8)(iqunsafe.ppPointer(aaname))) != ppint32('\000') {
		Xprintf(cgtls, "%s=\x00", iqlibc.ppVaList(cgbp+8, aaname))
	}

	/* similar to gmp_printf ("%NX\n",...), which is not available
	   with mini-gmp */
	aabufsize = iqlibc.ppUint32FromInt32(ppint32(2) + (aan*ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))-ppint32(1))/ppint32(4))
	aabuf = Xtests_allocate(cgtls, aabufsize)
	aam = iqlibc.ppInt32FromUint32(X__gmpn_get_str(cgtls, aabuf, ppint32(16), aap, aan))
	aai = 0
	for aai < aam-ppint32(1) && iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aabuf + ppuintptr(aai)))) == 0 {
		aai++
	} /* skip leading zeros (keeping at least one digit) */
	for aai < aam {
		ccv1 = aai
		aai++
		Xputchar(cgtls, ppint32(*(*ppint8)(iqunsafe.ppPointer("0123456789ABCDEF\x00" + ppuintptr(*(*ppuint8)(iqunsafe.ppPointer(aabuf + ppuintptr(ccv1))))))))
	}
	Xputchar(cgtls, ppint32('\n'))
	Xtests_free(cgtls, aabuf, aabufsize)
}

// C documentation
//
//	/* Open a file in the SRCDIR directory, i.e. the "tests" source directory,
//	   which is different from the current directory when objdir is different
//	   from srcdir. One should generally use this function instead of fopen
//	   directly. */
func Xsrc_fopen(cgtls *iqlibc.ppTLS, aafilename ppuintptr, aamode ppuintptr) (cgr ppuintptr) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aabuffer, aaf, aasrcdir ppuintptr
	var aabuffsize tnsize_t
	pp_, pp_, pp_, pp_ = aabuffer, aabuffsize, aaf, aasrcdir
	aasrcdir = ".\x00"

	aabuffsize = Xstrlen(cgtls, aafilename) + Xstrlen(cgtls, aasrcdir) + ppuint32(2)
	aabuffer = Xtests_allocate(cgtls, aabuffsize)
	if aabuffer == iqlibc.ppUintptrFromInt32(0) {

		Xprintf(cgtls, "src_fopen: failed to alloc memory)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xsprintf(cgtls, aabuffer, "%s/%s\x00", iqlibc.ppVaList(cgbp+8, aasrcdir, aafilename))
	aaf = Xfopen(cgtls, aabuffer, aamode)
	Xtests_free(cgtls, aabuffer, aabuffsize)
	return aaf
}

func Xset_emin(cgtls *iqlibc.ppTLS, aaexponent tnmpfr_exp_t) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	if Xmpfr_set_emin(cgtls, aaexponent) != 0 {

		Xprintf(cgtls, "set_emin: setting emin to %ld failed\n\x00", iqlibc.ppVaList(cgbp+8, aaexponent))
		Xexit(cgtls, ppint32(1))
	}
}

func Xset_emax(cgtls *iqlibc.ppTLS, aaexponent tnmpfr_exp_t) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	if Xmpfr_set_emax(cgtls, aaexponent) != 0 {

		Xprintf(cgtls, "set_emax: setting emax to %ld failed\n\x00", iqlibc.ppVaList(cgbp+8, aaexponent))
		Xexit(cgtls, ppint32(1))
	}
}

// C documentation
//
//	/* pos is 512 times the proportion of negative numbers.
//	   If pos=256, half of the numbers are negative.
//	   If pos=0, all generated numbers are positive.
//	*/
func Xtests_default_random(cgtls *iqlibc.ppTLS, aax tnmpfr_ptr, aapos ppint32, aaemin tnmpfr_exp_t, aaemax tnmpfr_exp_t, aaalways_scale ppint32) {

	var aae tnmpfr_exp_t
	var ccv1, ccv2, ccv3, ccv5 ppbool
	pp_, pp_, pp_, pp_, pp_ = aae, ccv1, ccv2, ccv3, ccv5

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaemin <= aaemax)), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tests.c\x00", ppint32(716), "emin <= emax\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaemin >= iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2)))), ppint32(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tests.c\x00", ppint32(717), "emin >= (1-((mpfr_exp_t) 1 << ((32 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2)))\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaemax <= iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))-iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tests.c\x00", ppint32(718), "emax <= (((mpfr_exp_t) 1 << ((32 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1)\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	/* but it isn't required that emin and emax are in the current
	   exponent range (see below), so that underflow/overflow checks
	   can be done on 64-bit machines without a manual change of the
	   exponent range (well, this is a bit ugly...). */

	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, aax, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))

	if ccv5 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(aax)).fd_mpfr_exp <= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)); ccv5 {
	}
	if ccv5 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) && (aaemin >= ppint32(1) || aaalways_scale != 0 || Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0)) {

		aae = aaemin + iqlibc.ppInt32FromUint32(Xrandlimb(cgtls)%iqlibc.ppUint32FromInt32(aaemax-aaemin+iqlibc.ppInt32FromInt32(1)))
		/* Note: There should be no overflow here because both terms are
		   between MPFR_EMIN_MIN and MPFR_EMAX_MAX. */

		if Xmpfr_set_exp(cgtls, aax, aae) != 0 {

			/* The random number doesn't fit in the current exponent range.
			   In this case, test the function in the extended exponent range,
			   which should be restored by the caller. */
			Xset_emin(cgtls, iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2)))
			Xset_emax(cgtls, iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))-iqlibc.ppInt32FromInt32(1))
			Xmpfr_set_exp(cgtls, aax, aae)
		}
	}
	if Xrandlimb(cgtls)%ppuint32(512) < iqlibc.ppUint32FromInt32(aapos) {
		Xmpfr_neg(cgtls, aax, aax, ppint32(ecMPFR_RNDN))
	}
}

// C documentation
//
//	/* If sb = -1, then the function is tested in only one rounding mode
//	   (the one provided in rnd) and the ternary value is not checked.
//	   Otherwise, the function is tested in the 5 rounding modes, rnd must
//	   initially be MPFR_RNDZ, y = RNDZ(f(x)), and sb is 0 if f(x) is exact,
//	   1 if f(x) is inexact (in which case, y must be a regular number,
//	   i.e. not the result of an overflow or an underflow); the successive
//	   rounding modes are:
//	     * MPFR_RNDZ, MPFR_RNDD, MPFR_RNDA, MPFR_RNDU, MPFR_RNDN for positive y;
//	     * MPFR_RNDZ, MPFR_RNDU, MPFR_RNDA, MPFR_RNDD, MPFR_RNDN for negative y;
//	   for the last test MPFR_RNDN, the target precision is decreased by 1 in
//	   order to be able to deduce the result (anyway, for a hard-to-round case
//	   in directed rounding modes, if yprec is chosen to be minimum precision
//	   preserving this hard-to-round case, then one has a hard-to-round case
//	   in round-to-nearest for precision yprec-1). If the target precision was
//	   MPFR_PREC_MIN, then skip the MPFR_RNDN test; thus to test exact special
//	   cases, use a target precision larger than MPFR_PREC_MIN.
//	   Note: if y is a regular number, sb corresponds to the sticky bit when
//	   considering round-to-nearest with precision yprec-1.
//	   As examples of use, see the calls to test5rm from the data_check and
//	   bad_cases functions. */
func sitest5rm(cgtls *iqlibc.ppTLS, aafct ppuintptr, aax tnmpfr_srcptr, aay tnmpfr_ptr, aaz tnmpfr_ptr, aarnd tnmpfr_rnd_t, aasb ppint32, aaname ppuintptr) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aaexpected_inex, aainex, ccv10, ccv11, ccv3, ccv4, ccv6, ccv7, ccv9 ppint32
	var aarndnext tnmpfr_rnd_t
	var aayprec, ccv8 tnmpfr_prec_t
	var ccv1, ccv2, ccv5 ppbool
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaexpected_inex, aainex, aarndnext, aayprec, ccv1, ccv10, ccv11, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9
	aayprec = (*tn__mpfr_struct)(iqunsafe.ppPointer(aay)).fd_mpfr_prec
	aarndnext = ppint32(ecMPFR_RNDF) + iqlibc.ppInt32FromInt32(1) /* means uninitialized */
	aaexpected_inex = -iqlibc.ppInt32FromInt32(1) - iqlibc.ppInt32FromInt32(0x7fffffff)

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aasb == -ppint32(1) || aarnd == ppint32(ecMPFR_RNDZ))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tests.c\x00", ppint32(774), "sb == -1 || rnd == MPFR_RNDZ\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_prec(cgtls, aaz, aayprec)
	for ppint32(1) != 0 {

		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aarnd != ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tests.c\x00", ppint32(780), "rnd != ((mpfr_rnd_t)((MPFR_RNDF)+1))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aainex = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafct})))(cgtls, aaz, aax, aarnd)
		if aasb == -ppint32(1) {
			aaexpected_inex = aainex
		} else {
			if aarnd != ppint32(ecMPFR_RNDN) {
				if aasb == 0 {
					ccv3 = 0
				} else {
					if aarnd == ppint32(ecMPFR_RNDD) || aarnd == ppint32(ecMPFR_RNDZ) && (*tn__mpfr_struct)(iqunsafe.ppPointer(aay)).fd_mpfr_sign > 0 || aarnd == ppint32(ecMPFR_RNDA) && (*tn__mpfr_struct)(iqunsafe.ppPointer(aay)).fd_mpfr_sign < 0 {
						ccv4 = -ppint32(1)
					} else {
						ccv4 = ppint32(1)
					}
					ccv3 = ccv4
				}
				aaexpected_inex = ccv3
			}
		}

		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaexpected_inex != -iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fffffff))), ppint32(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tests.c\x00", ppint32(787), "expected_inex != (-1-0x7fffffff)\x00")
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if !(((*tn__mpfr_struct)(iqunsafe.ppPointer(aay)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(aaz)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_equal_p(cgtls, aay, aaz) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(aay)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(aaz)).fd_mpfr_sign) && iqlibc.ppBoolInt32(aainex > 0)-iqlibc.ppBoolInt32(aainex < 0) == iqlibc.ppBoolInt32(aaexpected_inex > 0)-iqlibc.ppBoolInt32(aaexpected_inex < 0)) {

			Xprintf(cgtls, "test5rm: error for %s with xprec=%lu, yprec=%lu, rnd=%s\nx = \x00", iqlibc.ppVaList(cgbp+8, aaname, iqlibc.ppUint32FromInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(aax)).fd_mpfr_prec), iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), aax, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, "\nexpected \x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), aay, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, "\ngot      \x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), aaz, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, "\n\x00", 0)
			if aasb != -ppint32(1) {
				Xprintf(cgtls, "Expected inex = %d, got %d\n\x00", iqlibc.ppVaList(cgbp+8, aaexpected_inex, aainex))
			}
			Xexit(cgtls, ppint32(1))
		}

		if aasb == -ppint32(1) || aarnd == ppint32(ecMPFR_RNDN) {
			break
		} else {
			if aarnd == ppint32(ecMPFR_RNDZ) {

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(aay)).fd_mpfr_sign < 0 {
					ccv6 = ppint32(ecMPFR_RNDU)
				} else {
					ccv6 = ppint32(ecMPFR_RNDD)
				}
				aarnd = ccv6
				aarndnext = ppint32(ecMPFR_RNDA)
			} else {

				aarnd = aarndnext
				if aarnd == ppint32(ecMPFR_RNDA) {

					if aasb != 0 {
						Xmpfr_nexttoinf(cgtls, aay)
					}
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(aay)).fd_mpfr_sign < 0 {
						ccv7 = ppint32(ecMPFR_RNDD)
					} else {
						ccv7 = ppint32(ecMPFR_RNDU)
					}
					aarndnext = ccv7
				} else {
					if aarndnext != ppint32(ecMPFR_RNDN) {
						aarndnext = ppint32(ecMPFR_RNDN)
					} else {

						if aayprec == ppint32(mvMPFR_PREC_MIN) {
							break
						}
						/* If sb = 1, then mpfr_nexttoinf was called on y for the
						   MPFR_RNDA test, i.e. y = RNDA(yprec,f(x)); we use MPFR_RNDZ
						   since one has the property RNDN(p,w) = RNDZ(p,RNDA(p+1,w))
						   when w is not a midpoint in precision p. If sb = 0, then
						   y = f(x), so that RNDN(yprec-1,f(x)) = RNDN(yprec-1,y). */
						aayprec--
						ccv8 = aayprec
						if aasb != 0 {
							ccv9 = ppint32(ecMPFR_RNDZ)
						} else {
							ccv9 = ppint32(ecMPFR_RNDN)
						}
						aainex = Xmpfr_prec_round(cgtls, aay, ccv8, ccv9)
						if aasb != 0 {
							if aainex == 0 {
								ccv11 = ppint32(1)
							} else {
								ccv11 = -ppint32(1)
							}
							ccv10 = (*tn__mpfr_struct)(iqunsafe.ppPointer(aay)).fd_mpfr_sign * ccv11
						} else {
							ccv10 = aainex
						}
						aaexpected_inex = ccv10
						Xmpfr_set_prec(cgtls, aaz, aayprec)
					}
				}
			}
		}
	}
}

// C documentation
//
//	/* Check data in file f for function foo, with name 'name'.
//	   Each line consists of the file f one:
//
//	   xprec yprec rnd x y
//
//	   where:
//
//	   xprec is the input precision
//	   yprec is the output precision
//	   rnd is the rounding mode (n, z, u, d, a, Z, *)
//	   x is the input (hexadecimal format)
//	   y is the expected output (hexadecimal format) for foo(x) with rounding rnd
//
//	   If rnd is Z, then y is the expected output in round-toward-zero and
//	   it is assumed to be inexact; the four directed rounding modes are
//	   tested, and the round-to-nearest mode is tested in precision yprec-1.
//	   See details in the description of test5rm above.
//
//	   If rnd is *, y must be an exact case (possibly a special case).
//	   All the rounding modes are tested and the ternary value is checked
//	   (it must be 0).
//	 */
func Xdata_check(cgtls *iqlibc.ppTLS, aaf ppuintptr, aafoo ppuintptr, aaname ppuintptr) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aac, ccv1, ccv4 ppint32
	var aafp, aav ppuintptr
	var aarnd tnmpfr_rnd_t
	var ccv2, ccv3 ppbool
	var pp_ /* r at bp+56 */ ppint8
	var pp_ /* x at bp+8 */ tnmpfr_t
	var pp_ /* xprec at bp+0 */ ppint32
	var pp_ /* y at bp+24 */ tnmpfr_t
	var pp_ /* yprec at bp+4 */ ppint32
	var pp_ /* z at bp+40 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aac, aafp, aarnd, aav, ccv1, ccv2, ccv3, ccv4

	aafp = Xfopen(cgtls, aaf, "r\x00")
	if aafp == iqlibc.ppUintptrFromInt32(0) {
		aafp = Xsrc_fopen(cgtls, aaf, "r\x00")
	}
	if aafp == iqlibc.ppUintptrFromInt32(0) {

		aav = "4.2.0\x00"

		/* In the '-dev' versions, assume that the data file exists and
		   return an error if the file cannot be opened to make sure
		   that such failures are detected. */
		for ppint32(*(*ppint8)(iqunsafe.ppPointer(aav))) != ppint32('\000') {
			aav++
		}
		if ppint32(*(*ppint8)(iqunsafe.ppPointer(aav + ppuintptr(-iqlibc.ppInt32FromInt32(4))))) == ppint32('-') && ppint32(*(*ppint8)(iqunsafe.ppPointer(aav + ppuintptr(-iqlibc.ppInt32FromInt32(3))))) == ppint32('d') && ppint32(*(*ppint8)(iqunsafe.ppPointer(aav + ppuintptr(-iqlibc.ppInt32FromInt32(2))))) == ppint32('e') && ppint32(*(*ppint8)(iqunsafe.ppPointer(aav + ppuintptr(-iqlibc.ppInt32FromInt32(1))))) == ppint32('v') {

			Xprintf(cgtls, "Error: unable to open file '%s'\n\x00", iqlibc.ppVaList(cgbp+72, aaf))
			Xexit(cgtls, ppint32(1))
		} else {
			return
		}
	}

	Xmpfr_init(cgtls, cgbp+8)
	Xmpfr_init(cgtls, cgbp+24)
	Xmpfr_init(cgtls, cgbp+40)

	for !(Xfeof(cgtls, aafp) != 0) {

		/* skip whitespace, for consistency */
		if Xfscanf(cgtls, aafp, " \x00", 0) == -ppint32(1) {

			if Xferror(cgtls, aafp) != 0 {

				Xperror(cgtls, "data_check\x00")
				Xexit(cgtls, ppint32(1))
			} else {
				break
			} /* end of file */
		}

		ccv1 = Xgetc(cgtls, aafp)
		aac = ccv1
		if ccv1 == -ppint32(1) {

			if Xferror(cgtls, aafp) != 0 {

				Xperror(cgtls, "data_check\x00")
				Xexit(cgtls, ppint32(1))
			} else {
				break
			} /* end of file */
		}

		if aac == ppint32('#') { /* comment: read entire line */

			for cgcond := true; cgcond; cgcond = !(Xfeof(cgtls, aafp) != 0) && aac != ppint32('\n') {

				aac = Xgetc(cgtls, aafp)
			}
		} else {

			Xungetc(cgtls, aac, aafp)

			aac = Xfscanf(cgtls, aafp, "%ld %ld %c\x00", iqlibc.ppVaList(cgbp+72, cgbp, cgbp+4, cgbp+56))

			if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(*(*ppint32)(iqunsafe.ppPointer(cgbp)) >= ppint32(mvMPFR_PREC_MIN) && *(*ppint32)(iqunsafe.ppPointer(cgbp)) <= iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)-iqlibc.ppUint32FromInt32(256)))), ppint32(1)) != 0; !ccv2 {
				Xmpfr_assert_fail(cgtls, "tests.c\x00", ppint32(936), "((xprec) >= 1 && (xprec) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))\x00")
			}
			pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

			if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 4)) >= ppint32(mvMPFR_PREC_MIN) && *(*ppint32)(iqunsafe.ppPointer(cgbp + 4)) <= iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)-iqlibc.ppUint32FromInt32(256)))), ppint32(1)) != 0; !ccv3 {
				Xmpfr_assert_fail(cgtls, "tests.c\x00", ppint32(937), "((yprec) >= 1 && (yprec) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))\x00")
			}
			pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if aac == -ppint32(1) {

				Xperror(cgtls, "data_check\x00")
				Xexit(cgtls, ppint32(1))
			} else {
				if aac != ppint32(3) {

					Xprintf(cgtls, "Error: corrupted line in file '%s'\n\x00", iqlibc.ppVaList(cgbp+72, aaf))
					Xexit(cgtls, ppint32(1))
				}
			}

			switch ppint32(*(*ppint8)(iqunsafe.ppPointer(cgbp + 56))) {

			case ppint32('n'):
				aarnd = ppint32(ecMPFR_RNDN)
				break
				fallthrough
			case ppint32('z'):
				fallthrough
			case ppint32('Z'):
				aarnd = ppint32(ecMPFR_RNDZ)
				break
				fallthrough
			case ppint32('u'):
				aarnd = ppint32(ecMPFR_RNDU)
				break
				fallthrough
			case ppint32('d'):
				aarnd = ppint32(ecMPFR_RNDD)
				break
				fallthrough
			case ppint32('*'):
				aarnd = ppint32(ecMPFR_RNDF) + iqlibc.ppInt32FromInt32(1) /* non-existing rounding mode */
				break
				fallthrough
			default:
				Xprintf(cgtls, "Error: unexpected rounding mode in file '%s': %c\n\x00", iqlibc.ppVaList(cgbp+72, aaf, ppint32(*(*ppint8)(iqunsafe.ppPointer(cgbp + 56)))))
				Xexit(cgtls, ppint32(1))
			}
			Xmpfr_set_prec(cgtls, cgbp+8, *(*ppint32)(iqunsafe.ppPointer(cgbp)))
			Xmpfr_set_prec(cgtls, cgbp+24, *(*ppint32)(iqunsafe.ppPointer(cgbp + 4)))
			if X__gmpfr_inp_str(cgtls, cgbp+8, aafp, 0, ppint32(ecMPFR_RNDN)) == ppuint32(0) {

				Xprintf(cgtls, "Error: corrupted argument in file '%s'\n\x00", iqlibc.ppVaList(cgbp+72, aaf))
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_inp_str(cgtls, cgbp+24, aafp, 0, ppint32(ecMPFR_RNDN)) == ppuint32(0) {

				Xprintf(cgtls, "Error: corrupted result in file '%s'\n\x00", iqlibc.ppVaList(cgbp+72, aaf))
				Xexit(cgtls, ppint32(1))
			}
			if Xgetc(cgtls, aafp) != ppint32('\n') {

				Xprintf(cgtls, "Error: result not followed by \\n in file '%s'\n\x00", iqlibc.ppVaList(cgbp+72, aaf))
				Xexit(cgtls, ppint32(1))
			}
			/* Skip whitespace, in particular at the end of the file. */
			if Xfscanf(cgtls, aafp, " \x00", 0) == -ppint32(1) && Xferror(cgtls, aafp) != 0 {

				Xperror(cgtls, "data_check\x00")
				Xexit(cgtls, ppint32(1))
			}
			if ppint32(*(*ppint8)(iqunsafe.ppPointer(cgbp + 56))) == ppint32('*') {
				sitest5rm(cgtls, aafoo, cgbp+8, cgbp+24, cgbp+40, ppint32(ecMPFR_RNDZ), 0, aaname)
			} else {
				if ppint32(*(*ppint8)(iqunsafe.ppPointer(cgbp + 56))) == ppint32('Z') {
					ccv4 = ppint32(1)
				} else {
					ccv4 = -ppint32(1)
				}
				sitest5rm(cgtls, aafoo, cgbp+8, cgbp+24, cgbp+40, aarnd, ccv4, aaname)
			}
		}
	}

	Xmpfr_clear(cgtls, cgbp+8)
	Xmpfr_clear(cgtls, cgbp+24)
	Xmpfr_clear(cgtls, cgbp+40)

	Xfclose(cgtls, aafp)
}

// C documentation
//
//	/* Test n random bad cases. A precision py in [pymin,pymax] and
//	 * a number y of precision py are chosen randomly. One computes
//	 * x = inv(y) in precision px = py + psup (rounded to nearest).
//	 * Then (in general), y is a bad case for fct in precision py (in
//	 * the directed rounding modes, but also in the rounding-to-nearest
//	 * mode for some lower precision: see data_check).
//	 * fct, inv, name: data related to the function.
//	 * pos, emin, emax: arguments for tests_default_random.
//	 * For debugging purpose (e.g. in case of crash or infinite loop),
//	 * you can set the MPFR_DEBUG_BADCASES environment variable to 1 in
//	 * order to output information about the tested worst cases. You can
//	 * also enable logging (when supported), but this may give too much
//	 * information.
//	 */
func Xbad_cases(cgtls *iqlibc.ppTLS, aafct ppuintptr, aainv ppuintptr, aaname ppuintptr, aapos ppint32, aaemin tnmpfr_exp_t, aaemax tnmpfr_exp_t, aapymin tnmpfr_prec_t, aapymax tnmpfr_prec_t, aapsup tnmpfr_prec_t, aan ppint32) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aa_p tnmpfr_srcptr
	var aacnt, aadbg, aai, aainex, aainex_inv, aasb, ccv1, ccv3 ppint32
	var aadbgenv ppuintptr
	var aaold_emax, aaold_emin tnmpfr_exp_t
	var aapx, aapy, aapz tnmpfr_prec_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aacnt, aadbg, aadbgenv, aai, aainex, aainex_inv, aaold_emax, aaold_emin, aapx, aapy, aapz, aasb, ccv1, ccv3
	aacnt = 0

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	aadbgenv = Xgetenv(cgtls, "MPFR_DEBUG_BADCASES\x00")
	if aadbgenv != ppuintptr(0) {
		ccv1 = Xatoi(cgtls, aadbgenv)
	} else {
		ccv1 = 0
	}
	aadbg = ccv1 /* debug level */
	Xmpfr_inits2(cgtls, ppint32(mvMPFR_PREC_MIN), cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
	aai = 0
	for {
		if !(aai < aan) {
			break
		}

		if aadbg != 0 {
			Xprintf(cgtls, "bad_cases: %s, i = %d\n\x00", iqlibc.ppVaList(cgbp+56, aaname, aai))
		}
		aapy = iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(aapymin) + Xrandlimb(cgtls)%iqlibc.ppUint32FromInt32(aapymax-aapymin+iqlibc.ppInt32FromInt32(1)))
		Xmpfr_set_prec(cgtls, cgbp+16, aapy)
		Xtests_default_random(cgtls, cgbp+16, aapos, aaemin, aaemax, 0)
		if aadbg != 0 {

			Xprintf(cgtls, "bad_cases: yprec =%4ld, y = \x00", iqlibc.ppVaList(cgbp+56, aapy))
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, "\n\x00", 0)
		}
		aapx = aapy + aapsup
		Xmpfr_set_prec(cgtls, cgbp, aapx)
		if aadbg != 0 {
			Xprintf(cgtls, "bad_cases: xprec =%4ld\n\x00", iqlibc.ppVaList(cgbp+56, aapx))
		}
		Xmpfr_clear_flags(cgtls)
		aainex_inv = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aainv})))(cgtls, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
		if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {

			if aadbg != 0 {
				Xprintf(cgtls, "bad_cases: no normal inverse\n\x00", 0)
			}
			goto ppnext_i
		}
		if aadbg > ppint32(1) {

			Xprintf(cgtls, "bad_cases: x = \x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, "\n\x00", 0)
		}
		aapz = aapx
		for cgcond := true; cgcond; cgcond = aainex == 0 {

			aapz += ppint32(32)
			Xmpfr_set_prec(cgtls, cgbp+32, aapz)
			aasb = iqlibc.ppBoolInt32((*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafct})))(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN)) != 0)
			if !(aasb != 0) {

				if aadbg != 0 {

					Xprintf(cgtls, "bad_cases: exact case z = \x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp+32, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\n\x00", 0)
				}
				if aainex_inv != 0 {

					Xprintf(cgtls, "bad_cases: f exact while f^(-1) inexact,\ndue to a poor choice of the parameters.\n\x00", 0)
					Xexit(cgtls, ppint32(1))
					/* alternatively, goto next_i */
				}
				aainex = 0
				break
			}
			if aadbg != 0 {

				if aadbg > ppint32(1) {

					Xprintf(cgtls, "bad_cases: %s(x) ~= \x00", iqlibc.ppVaList(cgbp+56, aaname))
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp+32, ppint32(ecMPFR_RNDN))
				} else {

					Xprintf(cgtls, "bad_cases:   [MPFR_RNDZ]  ~= \x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(40), cgbp+32, ppint32(ecMPFR_RNDZ))
				}
				Xprintf(cgtls, "\n\x00", 0)
			}
			aainex = Xmpfr_prec_round(cgtls, cgbp+32, aapy, ppint32(ecMPFR_RNDN))
			if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+16) != 0) {

				if aadbg != 0 {

					Xprintf(cgtls, "bad_cases: inverse doesn't match for %s\ny = \x00", iqlibc.ppVaList(cgbp+56, aaname))
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\nz = \x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp+32, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\n\x00", 0)
				}
				goto ppnext_i
			}
		}
		/* We really have a bad case (or some special case). */
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {

			/* This can occur on tlog (GMP_CHECK_RANDOMIZE=1630879377004032
			   in r14570, 2021-09-07):
			   y = -0, giving x = 1 and z = 0. We have y = z, but here,
			   y and z have different signs. Since test5rm will test f(x)
			   and its sign (in particular for 0), we need to take the
			   sign of f(x), i.e. of z.
			   Note: To avoid this special case, one might want to detect and
			   ignore y = 0 (of any sign) when taking the random number above,
			   as this case should be redundant with some other tests. */
			{
				aa_p = cgbp + 32
				ccv3 = Xmpfr_set4(cgtls, cgbp+16, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
			}
			pp_ = ccv3
			aapy = ppint32(mvMPFR_PREC_MIN)
		} else {

			for cgcond := true; cgcond; cgcond = aapy >= ppint32(mvMPFR_PREC_MIN) && Xmpfr_prec_round(cgtls, cgbp+32, aapy, ppint32(ecMPFR_RNDZ)) == 0 {
				aapy--
			}
			aapy++
		}
		/* py is now the smallest output precision such that we have
		   a bad case in the directed rounding modes. */
		if Xmpfr_prec_round(cgtls, cgbp+16, aapy, ppint32(ecMPFR_RNDZ)) != 0 {

			Xprintf(cgtls, "Internal error for i = %d\n\x00", iqlibc.ppVaList(cgbp+56, aai))
			Xexit(cgtls, ppint32(1))
		}
		if aainex > 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0 || aainex < 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 {

			Xmpfr_nexttozero(cgtls, cgbp+16)
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
				goto ppnext_i
			}
		}
		if aadbg != 0 {

			Xprintf(cgtls, "bad_cases: yprec =%4ld, y = \x00", iqlibc.ppVaList(cgbp+56, aapy))
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, "\n\x00", 0)
		}
		/* Note: y is now the expected result rounded toward zero. */
		sitest5rm(cgtls, aafct, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDZ), aasb, aaname)
		aacnt++
		goto ppnext_i
	ppnext_i:
		;
		/* In case the exponent range has been changed by
		   tests_default_random()... */
		Xset_emin(cgtls, aaold_emin)
		Xset_emax(cgtls, aaold_emax)

		goto cg_2
	cg_2:
		;
		aai++
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

	if aadbg != 0 {
		Xprintf(cgtls, "bad_cases: %d bad cases over %d generated values for %s\n\x00", iqlibc.ppVaList(cgbp+56, aacnt, aan, aaname))
	}

	if Xgetenv(cgtls, "MPFR_CHECK_BADCASES\x00") != 0 && aan-aacnt > aan/ppint32(10) {

		Xprintf(cgtls, "bad_cases: too few bad cases (%d over %d generated values) for %s\n\x00", iqlibc.ppVaList(cgbp+56, aacnt, aan, aaname))
		Xexit(cgtls, ppint32(1))
	}
}

func Xflags_out(cgtls *iqlibc.ppTLS, aaflags ppuint32) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aanone ppint32
	pp_ = aanone
	aanone = ppint32(1)

	if aaflags&ppuint32(mvMPFR_FLAGS_UNDERFLOW) != 0 {
		aanone = 0
		Xprintf(cgtls, " underflow\x00", 0)
	}
	if aaflags&ppuint32(mvMPFR_FLAGS_OVERFLOW) != 0 {
		aanone = 0
		Xprintf(cgtls, " overflow\x00", 0)
	}
	if aaflags&ppuint32(mvMPFR_FLAGS_NAN) != 0 {
		aanone = 0
		Xprintf(cgtls, " nan\x00", 0)
	}
	if aaflags&ppuint32(mvMPFR_FLAGS_INEXACT) != 0 {
		aanone = 0
		Xprintf(cgtls, " inexact\x00", 0)
	}
	if aaflags&ppuint32(mvMPFR_FLAGS_ERANGE) != 0 {
		aanone = 0
		Xprintf(cgtls, " erange\x00", 0)
	}
	if aanone != 0 {
		Xprintf(cgtls, " none\x00", 0)
	}
	Xprintf(cgtls, " (%u)\n\x00", iqlibc.ppVaList(cgbp+8, aaflags))
}

func siabort_called(cgtls *iqlibc.ppTLS, aax ppint32) {

	/* Ok, abort has been called */
	Xexit(cgtls, 0)
}

// C documentation
//
//	/* This function has to be called for a test
//	   that will call the abort function */
func Xtests_expect_abort(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(144)
	defer cgtls.ppFree(144)

	var aaret ppint32
	var pp_ /* act at bp+0 */ tssigaction
	pp_ = aaret

	Xmemset(cgtls, cgbp, 0, ppuint32(140))
	*(*ppuintptr)(iqunsafe.ppPointer(cgbp)) = pp__ccgo_fp(siabort_called)
	aaret = Xsigaction(cgtls, ppint32(mvSIGABRT), cgbp, iqlibc.ppUintptrFromInt32(0))
	if aaret != 0 {

		/* Can't register error handler: Skip test */
		Xexit(cgtls, ppint32(77))
	}
}

// C documentation
//
//	/* Guess whether the test runs within Valgrind.
//	   Note: This should work at least under Linux and Solaris.
//	   If need be, support for macOS (with DYLD_INSERT_LIBRARIES) and
//	   i386 FreeBSD on amd64 (with LD_32_PRELOAD) could be added; thanks
//	   to Paul Floyd for the information.
//	   Up-to-date information should be found at
//	   <https://stackoverflow.com/a/62364698/3782797>. */
func Xtests_run_within_valgrind(cgtls *iqlibc.ppTLS) (cgr ppint32) {

	var aap ppuintptr
	pp_ = aap

	aap = Xgetenv(cgtls, "LD_PRELOAD\x00")
	if aap == iqlibc.ppUintptrFromInt32(0) {
		return 0
	}
	return iqlibc.ppBoolInt32(Xstrstr(cgtls, aap, "/valgrind/\x00") != iqlibc.ppUintptrFromInt32(0) || Xstrstr(cgtls, aap, "/vgpreload\x00") != iqlibc.ppUintptrFromInt32(0))
}

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

func ___gmp_randclear(*iqlibc.ppTLS, ppuintptr)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

func ___gmp_randseed_ui(*iqlibc.ppTLS, ppuintptr, ppuint32)

var ___gmp_version ppuintptr

var ___gmpfr_emax ppint32

var ___gmpfr_emin ppint32

var ___gmpfr_flags ppuint32

func ___gmpfr_inp_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32, ppuintptr, ppint32) ppuint32

func ___gmpn_get_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr, ppint32) ppuint32

func _atoi(*iqlibc.ppTLS, ppuintptr) ppint32

func _exit(*iqlibc.ppTLS, ppint32)

func _fclose(*iqlibc.ppTLS, ppuintptr) ppint32

func _feof(*iqlibc.ppTLS, ppuintptr) ppint32

func _ferror(*iqlibc.ppTLS, ppuintptr) ppint32

func _fopen(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _fscanf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _getc(*iqlibc.ppTLS, ppuintptr) ppint32

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _gettimeofday(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _memset(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32) ppuintptr

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_free_cache(*iqlibc.ppTLS)

func _mpfr_free_cache2(*iqlibc.ppTLS, ppint32)

func _mpfr_get_version(*iqlibc.ppTLS) ppuintptr

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_inits2(*iqlibc.ppTLS, ppint32, ppuintptr, ppuintptr)

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nexttoinf(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttozero(*iqlibc.ppTLS, ppuintptr)

func _mpfr_prec_round(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_rand_raw(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32)

var _mpfr_rands [1]tn__gmp_randstate_struct

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_emax(*iqlibc.ppTLS, ppint32) ppint32

func _mpfr_set_emin(*iqlibc.ppTLS, ppint32) ppint32

func _mpfr_set_exp(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _perror(*iqlibc.ppTLS, ppuintptr)

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _putchar(*iqlibc.ppTLS, ppint32) ppint32

func _setbuf(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _setlocale(*iqlibc.ppTLS, ppint32, ppuintptr) ppuintptr

func _sigaction(*iqlibc.ppTLS, ppint32, ppuintptr, ppuintptr) ppint32

func _sprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

var _stderr ppuintptr

var _stdout ppuintptr

func _strcmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _strlen(*iqlibc.ppTLS, ppuintptr) ppuint32

func _strstr(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _strtoul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppuint32

func _tests_allocate(*iqlibc.ppTLS, ppuint32) ppuintptr

func _tests_free(*iqlibc.ppTLS, ppuintptr, ppuint32)

func _tests_memory_end(*iqlibc.ppTLS)

func _tests_memory_start(*iqlibc.ppTLS)

func _ungetc(*iqlibc.ppTLS, ppint32, ppuintptr) ppint32

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

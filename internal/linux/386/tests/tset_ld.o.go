// Code generated for linux/386 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DMPFR_LONG_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/386 -UHAVE_NEARBYINT -mlong-double-64 -c -o tset_ld.o.go tset_ld.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.7976931348623157e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 32
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 2303
const mvMPFR_AI_THRESHOLD3 = 37484
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 522
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 6920
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 5
const mvMPFR_LOG2_PREC_BITS = 5
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 13
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 32
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 28160
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 18
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/x86/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_ILP32 = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_REDIR_TIME64 = 1
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 2
const mv__FLT_EVAL_METHOD__ = 2
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "i686-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__ILP32__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LAHF_SAHF__ = 1
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 2147483647
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 12
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__code_model_32__ = 1
const mv__gnu_linux__ = 1
const mv__i386 = 1
const mv__i386__ = 1
const mv__i686 = 1
const mv__i686__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pentiumpro = 1
const mv__pentiumpro__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvi386 = 1
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint32

type tnsize_t = ppuint32

type tnssize_t = ppint32

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint32

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint32

type tnmpfr_ulong = ppuint32

type tnmpfr_size_t = ppuint32

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint32

type tnmpfr_uprec_t = ppuint32

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint32

type tnmpfr_uexp_t = ppuint32

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint32

type tnmpfr_ueexp_t = ppuint32

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppint8
	fdfrac_digits        ppint8
	fdp_cs_precedes      ppint8
	fdp_sep_by_space     ppint8
	fdn_cs_precedes      ppint8
	fdn_sep_by_space     ppint8
	fdp_sign_posn        ppint8
	fdn_sign_posn        ppint8
	fdint_p_cs_precedes  ppint8
	fdint_p_sep_by_space ppint8
	fdint_n_cs_precedes  ppint8
	fdint_n_sep_by_space ppint8
	fdint_p_sign_posn    ppint8
	fdint_n_sign_posn    ppint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

func sicheck_gcc33_bug(cgtls *iqlibc.ppTLS) {

	var aax ppfloat64
	pp_ = aax

	aax = iqlibc.ppFloat64FromFloat64(9.007199254740992e+15) + iqlibc.ppFloat64FromFloat64(1)
	if aax != iqlibc.ppFloat64FromFloat64(0) {
		return
	} /* OK */
	Xprintf(cgtls, "Detected optimization bug of gcc 3.3 on Alpha concerning long double\ncomparisons; set_ld tests might fail (set_ld won't work correctly).\nSee https://gcc.gnu.org/legacy-ml/gcc-bugs/2003-10/msg00853.html for\nmore information.\n\x00", 0)
}

func siIsnan_ld(cgtls *iqlibc.ppTLS, cg_d ppfloat64) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)
	*(*ppfloat64)(iqunsafe.ppPointer(cgbp)) = cg_d

	var aa__x ppfloat64
	pp_ = aa__x
	/* Do not convert d to double as this can give an overflow, which
	   may confuse compilers without IEEE 754 support (such as clang
	   -fsanitize=undefined), or trigger a trap if enabled.
	   The DOUBLE_ISNAN macro should work fine on long double. */
	if (cgbp == cgbp || cgbp != cgbp) && !(iqlibc.ppBoolInt32(*(*ppfloat64)(iqunsafe.ppPointer(cgbp)) >= iqlibc.ppFloat64FromInt32(0))+iqlibc.ppBoolInt32(*(*ppfloat64)(iqunsafe.ppPointer(cgbp)) <= iqlibc.ppFloat64FromInt32(0)) != 0 && -*(*ppfloat64)(iqunsafe.ppPointer(cgbp))**(*ppfloat64)(iqunsafe.ppPointer(cgbp)) <= iqlibc.ppFloat64FromInt32(0)) {
		return ppint32(1)
	}
	aa__x = *(*ppfloat64)(iqunsafe.ppPointer(cgbp))
	if *(*ppfloat64)(iqunsafe.ppPointer(cgbp)) != aa__x {
		goto ppyes
	}
	return 0
	goto ppyes
ppyes:
	;
	return ppint32(1)
	return cgr
}

// C documentation
//
//	/* Return the minimal number of bits to represent d exactly (0 for zero).
//	   If flag is non-zero, also print d. */
//	/* FIXME: This function doesn't work if the rounding precision is reduced. */
func siprint_binary(cgtls *iqlibc.ppTLS, aad ppfloat64, aaflag ppint32) (cgr tnmpfr_prec_t) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aae, aaf, aar ppfloat64
	var aaexp ppint32
	var aaprec tnmpfr_prec_t
	var ccv1, ccv2, ccv3, ccv4 ppbool
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aae, aaexp, aaf, aaprec, aar, ccv1, ccv2, ccv3, ccv4
	aaexp = ppint32(1)
	aaprec = 0

	if siIsnan_ld(cgtls, aad) != 0 {

		if aaflag != 0 {
			Xprintf(cgtls, "NaN\n\x00", 0)
		}
		return 0
	}
	if aad < iqlibc.ppFloat64FromFloat64(0) || aad == iqlibc.ppFloat64FromFloat64(0) && ppfloat64(1)/ppfloat64(aad) < ppfloat64(0) {

		if aaflag != 0 {
			Xprintf(cgtls, "-\x00", 0)
		}
		aad = -aad
	}
	/* now d >= 0 */
	/* Use 2 different tests for Inf, to avoid potential bugs
	   in implementations. */
	if siIsnan_ld(cgtls, aad-aad) != 0 || aad > iqlibc.ppFloat64FromInt32(1) && aad*iqlibc.ppFloat64FromFloat64(0.5) == aad {

		if aaflag != 0 {
			Xprintf(cgtls, "Inf\n\x00", 0)
		}
		return 0
	}
	if aad == iqlibc.ppFloat64FromFloat64(0) {

		if aaflag != 0 {
			Xprintf(cgtls, "0.0\n\x00", 0)
		}
		return aaprec
	}

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aad > iqlibc.ppFloat64FromInt32(0))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(98), "d > 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aae = iqlibc.ppFloat64FromFloat64(1)
	for aae > aad {

		aae = aae * iqlibc.ppFloat64FromFloat64(0.5)
		aaexp--
	}
	if aaflag == ppint32(2) {
		Xprintf(cgtls, "1: e=%.36Le\n\x00", iqlibc.ppVaList(cgbp+8, aae))
	}

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aad >= aae)), ppint32(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(106), "d >= e\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	/* FIXME: There can be an overflow here, which may not be supported
	   on all platforms. */
	for {
		aaf = aae + aae
		if !(aad >= aaf) {
			break
		}

		aae = aaf
		aaexp++

	}
	if aaflag == ppint32(2) {
		Xprintf(cgtls, "2: e=%.36Le\n\x00", iqlibc.ppVaList(cgbp+8, aae))
	}

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aae <= aad && aad < aaf)), ppint32(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(115), "e <= d && d < f\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if aaflag == ppint32(1) {
		Xprintf(cgtls, "0.\x00", 0)
	}
	if aaflag == ppint32(2) {
		Xprintf(cgtls, "3: d=%.36Le e=%.36Le prec=%ld\n\x00", iqlibc.ppVaList(cgbp+8, aad, aae, aaprec))
	}
	/* Note: the method we use here to extract the bits of d is the following,
	   to deal with the case where the rounding precision is less than the
	   precision of d:
	   (1) we accumulate the upper bits of d into f
	   (2) when accumulating a new bit into f is not exact, we subtract
	       f from d and reset f to 0
	   This is guaranteed to work only when the rounding precision is at least
	   half the precision of d, since otherwise d-f might not be exact.
	   This method does not work with flush-to-zero on underflow. */
	aaf = iqlibc.ppFloat64FromFloat64(0) /* will hold accumulated powers of 2 */
	for ppint32(1) != 0 {

		aaprec++
		aar = aaf + aae
		/* r is close to f (in particular in the cases where f+e may
		   not be exact), so that r - f should be exact. */
		if aar-aaf != aae { /* f+e is not exact */

			aad -= aaf /* should be exact */
			aaf = iqlibc.ppFloat64FromFloat64(0)
			aar = aae
		}
		if aad >= aar {

			if aaflag == ppint32(1) {
				Xprintf(cgtls, "1\x00", 0)
			}
			if aad == aar {
				break
			}
			aaf = aar
		} else {

			if aaflag == ppint32(1) {
				Xprintf(cgtls, "0\x00", 0)
			}
		}

		aae *= iqlibc.ppFloat64FromFloat64(0.5)

		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aae != iqlibc.ppFloat64FromInt32(0))), ppint32(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(156), "e != 0\x00")
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* may fail with flush-to-zero on underflow */
		if aaflag == ppint32(2) {
			Xprintf(cgtls, "4: d=%.36Le e=%.36Le prec=%ld\n\x00", iqlibc.ppVaList(cgbp+8, aad, aae, aaprec))
		}
	}
	if aaflag == ppint32(1) {
		Xprintf(cgtls, "e%ld\n\x00", iqlibc.ppVaList(cgbp+8, aaexp))
	}
	return aaprec
}

// C documentation
//
//	/* Checks that a long double converted exactly to a MPFR number, then
//	   converted back to a long double gives the initial value, or in other
//	   words, mpfr_get_ld(mpfr_set_ld(d)) = d.
//	*/
func sicheck_set_get(cgtls *iqlibc.ppTLS, aad ppfloat64) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aae ppfloat64
	var aaemax, aaemin, aaex tnmpfr_exp_t
	var aainex, aar, aared ppint32
	var aaprec tnmpfr_prec_t
	var ccv3 ppuintptr
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aae, aaemax, aaemin, aaex, aainex, aaprec, aar, aared, ccv3

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax

	/* Select a precision to ensure that the conversion of d to x be exact. */
	aaprec = siprint_binary(cgtls, aad, 0)
	if aaprec < ppint32(mvMPFR_PREC_MIN) {
		aaprec = ppint32(mvMPFR_PREC_MIN)
	}
	Xmpfr_init2(cgtls, cgbp, aaprec)

	aar = 0
	for {
		if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}

		aainex = Xmpfr_set_ld(cgtls, cgbp, aad, aar)
		if aainex != 0 {

			Xprintf(cgtls, "Error: mpfr_set_ld should be exact (rnd = %s)\n\x00", iqlibc.ppVaList(cgbp+24, Xmpfr_print_rnd_mode(cgtls, aar)))
			/* We use 36 digits here, as the maximum LDBL_MANT_DIG value
			   seen in the current implementations is 113 (binary128),
			   and ceil(1+113*log(2)/log(10)) = 36. But the current glibc
			   implementation of printf with double-double arithmetic
			   (e.g. on PowerPC) is not accurate. */
			Xprintf(cgtls, "  d ~= %.36Le (output may be wrong!)\n\x00", iqlibc.ppVaList(cgbp+24, aad))
			Xprintf(cgtls, "  inex = %d\n\x00", iqlibc.ppVaList(cgbp+24, aainex))
			if aaemin >= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1) {
				Xprintf(cgtls, "  emin = %ld\n\x00", iqlibc.ppVaList(cgbp+24, aaemin))
			}
			if aaemax <= ppint32(0x7fffffff) {
				Xprintf(cgtls, "  emax = %ld\n\x00", iqlibc.ppVaList(cgbp+24, aaemax))
			}
			Xld_trace(cgtls, "  d\x00", aad)
			Xprintf(cgtls, "  d = \x00", 0)
			siprint_binary(cgtls, aad, ppint32(1))
			Xprintf(cgtls, "  x = \x00", 0)
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "  MPFR_LDBL_MANT_DIG=%u\n\x00", iqlibc.ppVaList(cgbp+24, ppint32(mvLDBL_MANT_DIG)))
			Xprintf(cgtls, "  prec=%ld\n\x00", iqlibc.ppVaList(cgbp+24, aaprec))
			siprint_binary(cgtls, aad, ppint32(2))
			Xexit(cgtls, ppint32(1))
		}
		aared = 0
		for {
			if !(aared < ppint32(2)) {
				break
			}

			if aared != 0 {

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3) {
					break
				}
				aaex = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				Xset_emin(cgtls, aaex)
				Xset_emax(cgtls, aaex)
			}
			aae = Xmpfr_get_ld(cgtls, cgbp, aar)
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			if aainex == 0 && (siIsnan_ld(cgtls, aad) != 0 && !(siIsnan_ld(cgtls, aae) != 0) || siIsnan_ld(cgtls, aae) != 0 && !(siIsnan_ld(cgtls, aad) != 0) || aae != aad && !(siIsnan_ld(cgtls, aae) != 0 && siIsnan_ld(cgtls, aad) != 0)) {

				if aared != 0 {
					ccv3 = ", reduced exponent range\x00"
				} else {
					ccv3 = "\x00"
				}
				Xprintf(cgtls, "Error: mpfr_get_ld o mpfr_set_ld <> Id%s\n\x00", iqlibc.ppVaList(cgbp+24, ccv3))
				Xprintf(cgtls, "  rnd = %s\n\x00", iqlibc.ppVaList(cgbp+24, Xmpfr_print_rnd_mode(cgtls, aar)))
				Xprintf(cgtls, "  d ~= %.36Le (output may be wrong!)\n\x00", iqlibc.ppVaList(cgbp+24, aad))
				Xprintf(cgtls, "  e ~= %.36Le (output may be wrong!)\n\x00", iqlibc.ppVaList(cgbp+24, aae))
				Xld_trace(cgtls, "  d\x00", aad)
				Xprintf(cgtls, "  x = \x00", 0)
				X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
				Xprintf(cgtls, "\n\x00", 0)
				Xld_trace(cgtls, "  e\x00", aae)
				Xprintf(cgtls, "  d = \x00", 0)
				siprint_binary(cgtls, aad, ppint32(1))
				Xprintf(cgtls, "  x = \x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "  e = \x00", 0)
				siprint_binary(cgtls, aae, ppint32(1))
				Xprintf(cgtls, "  MPFR_LDBL_MANT_DIG=%u\n\x00", iqlibc.ppVaList(cgbp+24, ppint32(mvLDBL_MANT_DIG)))
				Xexit(cgtls, ppint32(1))
			}

			goto cg_2
		cg_2:
			;
			aared++
		}

		goto cg_1
	cg_1:
		;
		aar++
	}

	Xmpfr_clear(cgtls, cgbp)
}

func sitest_small(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aad ppfloat64
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_ = aad

	Xmpfr_init2(cgtls, cgbp, ppint32(mvLDBL_MANT_DIG))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(mvLDBL_MANT_DIG))
	Xmpfr_init2(cgtls, cgbp+32, ppint32(mvLDBL_MANT_DIG))

	/* x = 11906603631607553907/2^(16381+64) */
	Xmpfr_set_str(cgtls, cgbp, "0.1010010100111100110000001110101101000111010110000001111101110011E-16381\x00", ppint32(2), ppint32(ecMPFR_RNDN))
	aad = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN)) /* infinite loop? */
	Xmpfr_set_ld(cgtls, cgbp+16, aad, ppint32(ecMPFR_RNDN))
	Xmpfr_sub(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	Xmpfr_set4(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN), ppint32(1))
	Xmpfr_clear_erangeflag(cgtls)
	/* If long double = double, d should be equal to 0;
	   in this case, everything is OK. */
	if aad != iqlibc.ppFloat64FromInt32(0) && (Xmpfr_cmp_str(cgtls, cgbp+32, "1E-16434\x00", ppint32(2), ppint32(ecMPFR_RNDN)) > 0 || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0) {

		Xprintf(cgtls, "Error with x = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(21), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, " = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n        -> d = %.33Le\x00", iqlibc.ppVaList(cgbp+56, aad))
		Xprintf(cgtls, "\n        -> y = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(21), cgbp+16, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, " = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n        -> |x-y| = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint32(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp+32)
}

func sitest_fixed_bugs(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aal, aam ppfloat64
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_ = aal, aam

	/* bug found by Steve Kargl (2009-03-14) */
	Xmpfr_init2(cgtls, cgbp, ppint32(mvLDBL_MANT_DIG))
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint32(1), ppint32(-ppint32(16447)), ppint32(ecMPFR_RNDN))
	Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN)) /* an assertion failed in init2.c:50 */

	/* bug reported by Jakub Jelinek (2010-10-17)
	   https://gforge.inria.fr/tracker/?func=detail&aid=11300 */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(mvLDBL_MANT_DIG))
	/* l = 0x1.23456789abcdef0123456789abcdp-914L; */
	aal = iqlibc.ppFloat64FromFloat64(8.215640181713713e-276)
	Xmpfr_set_ld(cgtls, cgbp, aal, ppint32(ecMPFR_RNDN))
	aam = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN))
	if aam != aal {

		Xprintf(cgtls, "Error in get_ld o set_ld for l=%Le\n\x00", iqlibc.ppVaList(cgbp+24, aal))
		Xprintf(cgtls, "Got m=%Le instead of l\n\x00", iqlibc.ppVaList(cgbp+24, aam))
		Xexit(cgtls, ppint32(1))
	}

	/* another similar test which failed with extended double precision and the
	   generic code for mpfr_set_ld */
	/* l = 0x1.23456789abcdef0123456789abcdp-968L; */
	aal = iqlibc.ppFloat64FromFloat64(4.560596445887085e-292)
	Xmpfr_set_ld(cgtls, cgbp, aal, ppint32(ecMPFR_RNDN))
	aam = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN))
	if aam != aal {

		Xprintf(cgtls, "Error in get_ld o set_ld for l=%Le\n\x00", iqlibc.ppVaList(cgbp+24, aal))
		Xprintf(cgtls, "Got m=%Le instead of l\n\x00", iqlibc.ppVaList(cgbp+24, aam))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_subnormal(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aad, aae ppfloat64
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_ = aad, aae

	aad = iqlibc.ppFloat64FromFloat64(17)
	Xmpfr_init2(cgtls, cgbp, ppint32(mvLDBL_MANT_DIG))
	for aad != iqlibc.ppFloat64FromFloat64(0) {

		Xmpfr_set_ld(cgtls, cgbp, aad, ppint32(ecMPFR_RNDN))
		aae = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN))
		if aae != aad {

			Xprintf(cgtls, "Error in check_subnormal for mpfr_get_ld o mpfr_set_ld\n\x00", 0)
			Xprintf(cgtls, "d=%.30Le\n\x00", iqlibc.ppVaList(cgbp+24, aad))
			Xprintf(cgtls, "x=\x00", 0)
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "e=%.30Le\n\x00", iqlibc.ppVaList(cgbp+24, aae))
			Xexit(cgtls, ppint32(1))
		}

		aad *= iqlibc.ppFloat64FromFloat64(0.5)
	}
	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_overflow(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aad, ccv2 ppfloat64
	var aai, ccv3 ppint32
	var ccv4 ppbool
	var pp_ /* e at bp+0 */ ppfloat64
	var pp_ /* x at bp+8 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aad, aai, ccv2, ccv3, ccv4

	Xmpfr_init2(cgtls, cgbp+8, ppint32(mvLDBL_MANT_DIG))
	aai = 0
	for {
		if !(aai < ppint32(2)) {
			break
		}

		if aai == 0 {
			ccv2 = iqlibc.ppFloat64FromFloat64(1.79769313486231570815e+308)
		} else {
			ccv2 = -iqlibc.ppFloat64FromFloat64(1.79769313486231570815e+308)
		}
		aad = ccv2
		Xmpfr_set_ld(cgtls, cgbp+8, aad, ppint32(ecMPFR_RNDN))
		Xmpfr_mul_2ui(cgtls, cgbp+8, cgbp+8, ppuint32(1), ppint32(ecMPFR_RNDN))
		*(*ppfloat64)(iqunsafe.ppPointer(cgbp)) = Xmpfr_get_ld(cgtls, cgbp+8, ppint32(ecMPFR_RNDN))

		if ccv4 = !((cgbp == cgbp || cgbp != cgbp) && (*(*ppfloat64)(iqunsafe.ppPointer(cgbp)) > iqlibc.ppFloat64FromFloat64(1.7976931348623157e+308) || *(*ppfloat64)(iqunsafe.ppPointer(cgbp)) < ppfloat64(-iqlibc.ppFloat64FromFloat64(1.7976931348623157e+308)))); !ccv4 {
			if aai == 0 {
				ccv3 = iqlibc.ppBoolInt32(*(*ppfloat64)(iqunsafe.ppPointer(cgbp)) < iqlibc.ppFloat64FromInt32(0))
			} else {
				ccv3 = iqlibc.ppBoolInt32(*(*ppfloat64)(iqunsafe.ppPointer(cgbp)) > iqlibc.ppFloat64FromInt32(0))
			}
		}
		if ccv4 || ccv3 != 0 {

			Xprintf(cgtls, "Error in check_overflow.\n\x00", 0)
			Xprintf(cgtls, "d=%Le\n\x00", iqlibc.ppVaList(cgbp+32, aad))
			Xprintf(cgtls, "x=\x00", 0)
			Xmpfr_dump(cgtls, cgbp+8)
			Xprintf(cgtls, "e=%Le\n\x00", iqlibc.ppVaList(cgbp+32, *(*ppfloat64)(iqunsafe.ppPointer(cgbp))))
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aai++
	}
	Xmpfr_clear(cgtls, cgbp+8)
}

// C documentation
//
//	/* issue reported by Sisyphus on powerpc */
func sitest_20140212(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aac1, aac2, aai, ccv3 ppint32
	var aah, aal, aald, aald2 ppfloat64
	var ccv4, ccv5, ccv6 ppbool
	var pp_ /* fr1 at bp+0 */ tnmpfr_t
	var pp_ /* fr2 at bp+16 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aac1, aac2, aah, aai, aal, aald, aald2, ccv3, ccv4, ccv5, ccv6

	Xmpfr_init2(cgtls, cgbp, ppint32(106))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(2098))

	aah = iqlibc.ppFloat64FromFloat64(1)
	aai = iqlibc.ppInt32FromInt32(0)
	for {
		if !(aai < ppint32(1023)) {
			break
		}

		aah *= iqlibc.ppFloat64FromFloat64(2)
		goto cg_1
	cg_1:
		;
		aai++
	}
	aal = iqlibc.ppFloat64FromFloat64(1)
	aai = iqlibc.ppInt32FromInt32(0)
	for {
		if !(aai < ppint32(1074)) {
			break
		}

		aal *= iqlibc.ppFloat64FromFloat64(0.5)
		goto cg_2
	cg_2:
		;
		aai++
	}
	aald = aah + aal /* rounding of 2^1023 + 2^(-1074) */

	Xmpfr_set_ld(cgtls, cgbp, aald, ppint32(ecMPFR_RNDN))
	Xmpfr_set_ld(cgtls, cgbp+16, aald, ppint32(ecMPFR_RNDN))

	aac1 = Xmpfr_cmp_ld(cgtls, cgbp, aald)
	aac2 = Xmpfr_cmp_ld(cgtls, cgbp+16, aald)

	/* If long double is binary64, then ld = fr1 = fr2 = 2^1023.
	   If long double is double-double, then ld = 2^1023 + 2^(-1074),
	   fr1 = 2^1023 and fr2 = 2^1023 + 2^(-1074) */
	if aald == aah {
		ccv3 = iqlibc.ppBoolInt32(aac1 == 0)
	} else {
		ccv3 = iqlibc.ppBoolInt32(aac1 < 0)
	}
	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv3 != 0)), ppint32(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(427), "ld == h ? (c1 == 0) : (c1 < 0)\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aac2 == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(429), "c2 == 0\x00")
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aald2 = Xmpfr_get_ld(cgtls, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aald2 == aald)), ppint32(1)) != 0; !ccv6 {
		Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(432), "ld2 == ld\x00")
	}
	pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
}

// C documentation
//
//	/* bug reported by Walter Mascarenhas
//	   https://sympa.inria.fr/sympa/arc/mpfr/2016-09/msg00005.html */
func sibug_20160907(cgtls *iqlibc.ppTLS) {

}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aad, aae, aamaxp2, ccv1 ppfloat64
	var aaemax tnmpfr_exp_t
	var aai ppint32
	var ccv10, ccv11, ccv12, ccv13, ccv15, ccv16, ccv17, ccv19, ccv20, ccv21, ccv22, ccv23, ccv5, ccv6, ccv7, ccv9 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aad, aae, aaemax, aai, aamaxp2, ccv1, ccv10, ccv11, ccv12, ccv13, ccv15, ccv16, ccv17, ccv19, ccv20, ccv21, ccv22, ccv23, ccv5, ccv6, ccv7, ccv9

	Xtests_start_mpfr(cgtls)
	Xmpfr_test_init(cgtls)

	sicheck_gcc33_bug(cgtls)
	sitest_fixed_bugs(cgtls)

	Xmpfr_init2(cgtls, cgbp, ppint32(iqlibc.ppInt32FromInt32(mvLDBL_MANT_DIG)+iqlibc.ppInt32FromInt32(64)))

	/* check NaN */
	Xmpfr_set_nan(cgtls, cgbp)
	aad = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN))
	sicheck_set_get(cgtls, aad)

	/* check +0.0 and -0.0 */
	aad = iqlibc.ppFloat64FromFloat64(0)
	sicheck_set_get(cgtls, aad)
	aad = ppfloat64(-iqlibc.ppFloat64FromFloat64(0))
	sicheck_set_get(cgtls, aad)

	/* check that the sign of -0.0 is set */
	Xmpfr_set_ld(cgtls, cgbp, ppfloat64(-iqlibc.ppFloat64FromFloat64(0)), ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 {

		Xprintf(cgtls, "Error: sign of -0.0 is not set correctly\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* check +Inf */
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	aad = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN))
	sicheck_set_get(cgtls, aad)

	/* check -Inf */
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	aad = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN))
	sicheck_set_get(cgtls, aad)

	/* check the largest power of two */
	aamaxp2 = iqlibc.ppFloat64FromFloat64(1)
	for aamaxp2 < iqlibc.ppFloat64FromFloat64(1.79769313486231570815e+308)/iqlibc.ppFloat64FromFloat64(2) {

		aamaxp2 *= iqlibc.ppFloat64FromFloat64(2)
	}
	sicheck_set_get(cgtls, aamaxp2)
	sicheck_set_get(cgtls, -aamaxp2)

	aad = iqlibc.ppFloat64FromFloat64(1.79769313486231570815e+308)
	aae = aad / iqlibc.ppFloat64FromFloat64(2)
	if aae != aamaxp2 { /* false under NetBSD/x86 */

		/* d = LDBL_MAX does not have excess precision. */
		sicheck_set_get(cgtls, aad)
		sicheck_set_get(cgtls, -aad)
	}

	/* check the smallest power of two */
	aad = iqlibc.ppFloat64FromFloat64(1)
	for {
		ccv1 = aad / iqlibc.ppFloat64FromFloat64(2)
		aae = ccv1
		if !(ccv1 != iqlibc.ppFloat64FromFloat64(0) && aae != aad) {
			break
		}
		aad = aae
	}
	sicheck_set_get(cgtls, aad)
	sicheck_set_get(cgtls, -aad)

	/* check that 2^i, 2^i+1, 2^i-1 and 2^i-2^(i-2)-1 are correctly converted */
	aad = iqlibc.ppFloat64FromFloat64(1)
	aai = ppint32(1)
	for {
		if !(aai < iqlibc.ppInt32FromInt32(mvLDBL_MANT_DIG)+iqlibc.ppInt32FromInt32(8)) {
			break
		}

		aad = iqlibc.ppFloat64FromFloat64(2) * aad /* d = 2^i */
		sicheck_set_get(cgtls, aad)
		if aad+iqlibc.ppFloat64FromFloat64(1) != aad {
			sicheck_set_get(cgtls, aad+iqlibc.ppFloat64FromFloat64(1))
		} else {

			Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint32(1), ppint32(aai), ppint32(ecMPFR_RNDN))
			Xmpfr_add_ui(cgtls, cgbp, cgbp, ppuint32(1), ppint32(ecMPFR_RNDN))
			aae = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN))
			sicheck_set_get(cgtls, aae)
		}
		if aad-iqlibc.ppFloat64FromFloat64(1) != aad {
			sicheck_set_get(cgtls, aad-iqlibc.ppFloat64FromFloat64(1))
		} else {

			Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint32(1), ppint32(aai), ppint32(ecMPFR_RNDN))
			Xmpfr_sub_ui(cgtls, cgbp, cgbp, ppuint32(1), ppint32(ecMPFR_RNDN))
			aae = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN))
			sicheck_set_get(cgtls, aae)
		}
		if aai < ppint32(3) {
			goto cg_2
		}
		/* The following test triggers a failure in r10844 for i = 56,
		   with gcc -mpc64 on x86 (64-bit ABI). */
		Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint32(3), ppint32(aai-ppint32(2)), ppint32(ecMPFR_RNDN))
		Xmpfr_sub_ui(cgtls, cgbp, cgbp, ppuint32(1), ppint32(ecMPFR_RNDN))
		aae = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN))
		sicheck_set_get(cgtls, aae)

		goto cg_2
	cg_2:
		;
		aai++
	}

	aai = 0
	for {
		if !(aai < ppint32(10000)) {
			break
		}

		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		aad = Xmpfr_get_ld(cgtls, cgbp, ppint32(ecMPFR_RNDN))
		sicheck_set_get(cgtls, aad)

		goto cg_3
	cg_3:
		;
		aai++
	}

	/* check with reduced emax to exercise overflow */
	aaemax = X__gmpfr_emax
	Xmpfr_set_prec(cgtls, cgbp, ppint32(2))
	Xset_emax(cgtls, ppint32(1))
	Xmpfr_set_ld(cgtls, cgbp, iqlibc.ppFloat64FromFloat64(2), ppint32(ecMPFR_RNDN))

	if ccv7 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv7 {
		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(644), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(644), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv7 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv13 {
		Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(644), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tset_ld.c\", 644, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tset_ld.c\", 644, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0\x00")
		if ccv12 = iqlibc.Bool(!(0 != 0)); !ccv12 {
			if ccv11 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv11 {
				if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv9 {
					Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(644), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv10 {
					Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(644), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv12 || ccv11 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aad = iqlibc.ppFloat64FromFloat64(2)
	aai = iqlibc.ppInt32FromInt32(0)
	for {
		if !(aai < ppint32(13)) {
			break
		}
		goto cg_14
	cg_14:
		;
		aai++
		aad *= aad
	}
	/* now d = 2^8192, or an infinity (e.g. with double or double-double) */
	Xmpfr_set_ld(cgtls, cgbp, aad, ppint32(ecMPFR_RNDN))

	if ccv17 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv17 {
		if ccv15 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv15 {
			Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(648), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv15 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv16 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv16 {
			Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(648), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv16 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv23 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv17 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv23 {
		Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(648), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tset_ld.c\", 648, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tset_ld.c\", 648, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0\x00")
		if ccv22 = iqlibc.Bool(!(0 != 0)); !ccv22 {
			if ccv21 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv21 {
				if ccv19 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv19 {
					Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(648), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv19 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv20 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv20 {
					Xmpfr_assert_fail(cgtls, "tset_ld.c\x00", ppint32(648), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv20 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv22 || ccv21 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv23 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emax(cgtls, aaemax)

	Xmpfr_clear(cgtls, cgbp)

	sitest_small(cgtls)

	sicheck_subnormal(cgtls)
	sicheck_overflow(cgtls)

	sitest_20140212(cgtls)
	sibug_20160907(cgtls)

	Xtests_end_mpfr(cgtls)

	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

func ___builtin_unreachable(*iqlibc.ppTLS)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

var ___gmpfr_emax ppint32

var ___gmpfr_emin ppint32

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32, ppuintptr, ppint32) ppuint32

func _exit(*iqlibc.ppTLS, ppint32)

func _ld_trace(*iqlibc.ppTLS, ppuintptr, ppfloat64)

func _mpfr_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_erangeflag(*iqlibc.ppTLS)

func _mpfr_cmp_ld(*iqlibc.ppTLS, ppuintptr, ppfloat64) ppint32

func _mpfr_cmp_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_get_ld(*iqlibc.ppTLS, ppuintptr, ppint32) ppfloat64

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_mul_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppint8

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_ld(*iqlibc.ppTLS, ppuintptr, ppfloat64, ppint32) ppint32

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint32, ppint32, ppint32) ppint32

func _mpfr_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_sub_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_test_init(*iqlibc.ppTLS)

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _set_emax(*iqlibc.ppTLS, ppint32)

func _set_emin(*iqlibc.ppTLS, ppint32)

var _stdout ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

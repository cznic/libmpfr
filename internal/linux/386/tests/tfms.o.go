// Code generated for linux/386 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DMPFR_LONG_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/386 -UHAVE_NEARBYINT -mlong-double-64 -c -o tfms.o.go tfms.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 32
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 2303
const mvMPFR_AI_THRESHOLD3 = 37484
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 522
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 6920
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 5
const mvMPFR_LOG2_PREC_BITS = 5
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 13
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 32
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 28160
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 18
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/x86/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSTRTU1 = "Error in test_underflow1 (signy = %d, signz = %d, %s)\n  NaN flag is set\n"
const mvSTRTU2 = "Error in test_underflow2 (b = %d, i = %d)\n  flags = %u instead of %u\n"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_ILP32 = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_REDIR_TIME64 = 1
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 2
const mv__FLT_EVAL_METHOD__ = 2
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "i686-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__ILP32__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LAHF_SAHF__ = 1
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 2147483647
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 12
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__code_model_32__ = 1
const mv__gnu_linux__ = 1
const mv__i386 = 1
const mv__i386__ = 1
const mv__i686 = 1
const mv__i686__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pentiumpro = 1
const mv__pentiumpro__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvi386 = 1
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint32

type tnsize_t = ppuint32

type tnssize_t = ppint32

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint32

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint32

type tnmpfr_ulong = ppuint32

type tnmpfr_size_t = ppuint32

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint32

type tnmpfr_uprec_t = ppuint32

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint32

type tnmpfr_uexp_t = ppuint32

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint32

type tnmpfr_ueexp_t = ppuint32

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppint8
	fdfrac_digits        ppint8
	fdp_cs_precedes      ppint8
	fdp_sep_by_space     ppint8
	fdn_cs_precedes      ppint8
	fdn_sep_by_space     ppint8
	fdp_sign_posn        ppint8
	fdn_sign_posn        ppint8
	fdint_p_cs_precedes  ppint8
	fdint_p_sep_by_space ppint8
	fdint_n_cs_precedes  ppint8
	fdint_n_sep_by_space ppint8
	fdint_p_sign_posn    ppint8
	fdint_n_sign_posn    ppint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

// C documentation
//
//	/* When a * b is exact, the FMS is equivalent to the separate operations. */
func sitest_exact(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aai, aaj, aak, aarnd, aasv ppint32
	var ccv1 ppbool
	var pp_ /* a at bp+36 */ tnmpfr_t
	var pp_ /* b at bp+52 */ tnmpfr_t
	var pp_ /* c at bp+68 */ tnmpfr_t
	var pp_ /* r1 at bp+84 */ tnmpfr_t
	var pp_ /* r2 at bp+100 */ tnmpfr_t
	var pp_ /* val at bp+0 */ [9]ppuintptr
	pp_, pp_, pp_, pp_, pp_, pp_ = aai, aaj, aak, aarnd, aasv, ccv1
	*(*[9]ppuintptr)(iqunsafe.ppPointer(cgbp)) = [9]ppuintptr{
		0: "@NaN@\x00",
		1: "-@Inf@\x00",
		2: "-2\x00",
		3: "-1\x00",
		4: "-0\x00",
		5: "0\x00",
		6: "1\x00",
		7: "2\x00",
		8: "@Inf@\x00",
	}
	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(cgbp == cgbp)), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(31), "(void *) &(val) == (void *) &(val)[0]\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aasv = ppint32(iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt64(36) / iqlibc.ppUint32FromInt64(4)))

	Xmpfr_inits2(cgtls, ppint32(8), cgbp+36, iqlibc.ppVaList(cgbp+128, cgbp+52, cgbp+68, cgbp+84, cgbp+100, iqlibc.ppUintptrFromInt32(0)))

	aai = 0
	for {
		if !(aai < aasv) {
			break
		}
		aaj = 0
		for {
			if !(aaj < aasv) {
				break
			}
			aak = 0
			for {
				if !(aak < aasv) {
					break
				}
				aarnd = 0
				for {
					if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
						break
					}

					if Xmpfr_set_str(cgtls, cgbp+36, (*(*[9]ppuintptr)(iqunsafe.ppPointer(cgbp)))[aai], ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || Xmpfr_set_str(cgtls, cgbp+52, (*(*[9]ppuintptr)(iqunsafe.ppPointer(cgbp)))[aaj], ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || Xmpfr_set_str(cgtls, cgbp+68, (*(*[9]ppuintptr)(iqunsafe.ppPointer(cgbp)))[aak], ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || Xmpfr_mul(cgtls, cgbp+84, cgbp+36, cgbp+52, aarnd) != 0 || Xmpfr_sub(cgtls, cgbp+84, cgbp+84, cgbp+68, aarnd) != 0 {

						if aarnd == ppint32(ecMPFR_RNDF) {
							break
						}
						Xprintf(cgtls, "test_exact internal error for (%d,%d,%d,%d,%s)\n\x00", iqlibc.ppVaList(cgbp+128, aai, aaj, aak, aarnd, Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xexit(cgtls, ppint32(1))
					}
					if Xmpfr_fms(cgtls, cgbp+100, cgbp+36, cgbp+52, cgbp+68, aarnd) != 0 {

						Xprintf(cgtls, "test_exact(%d,%d,%d,%d): mpfr_fms should be exact\n\x00", iqlibc.ppVaList(cgbp+128, aai, aaj, aak, aarnd))
						Xexit(cgtls, ppint32(1))
					}
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+84)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) {

						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+100)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) {
							goto cg_5
						}
						Xprintf(cgtls, "test_exact(%d,%d,%d,%d): mpfr_fms should be NaN\n\x00", iqlibc.ppVaList(cgbp+128, aai, aaj, aak, aarnd))
						Xexit(cgtls, ppint32(1))
					}
					if !(Xmpfr_equal_p(cgtls, cgbp+84, cgbp+100) != 0) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+84)).fd_mpfr_sign != (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+100)).fd_mpfr_sign {

						Xprintf(cgtls, "test_exact(%d,%d,%d,%d):\nexpected \x00", iqlibc.ppVaList(cgbp+128, aai, aaj, aak, aarnd))
						X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp+84, ppint32(ecMPFR_RNDN))
						Xprintf(cgtls, "\n     got \x00", 0)
						X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp+100, ppint32(ecMPFR_RNDN))
						Xprintf(cgtls, "\n\x00", 0)
						Xexit(cgtls, ppint32(1))
					}

					goto cg_5
				cg_5:
					;
					aarnd++
				}
				goto cg_4
			cg_4:
				;
				aak++
			}
			goto cg_3
		cg_3:
			;
			aaj++
		}
		goto cg_2
	cg_2:
		;
		aai++
	}

	Xmpfr_clears(cgtls, cgbp+36, iqlibc.ppVaList(cgbp+128, cgbp+52, cgbp+68, cgbp+84, cgbp+100, iqlibc.ppUintptrFromInt32(0)))
}

func sitest_overflow1(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aa_p tnmpfr_srcptr
	var aainex, ccv1 ppint32
	var pp_ /* r at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_ = aa_p, aainex, ccv1

	Xmpfr_inits2(cgtls, ppint32(8), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)                                                                               /* x = 2^emax - ulp */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN)) /* y = 2 */
	{
		aa_p = cgbp
		ccv1 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
	}
	pp_ = ccv1 /* z = x = 2^emax - ulp */
	Xmpfr_clear_flags(cgtls)
	/* The intermediate multiplication x * y overflows, but x * y - z = x
	   is representable. */
	aainex = Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if aainex != 0 || !(Xmpfr_equal_p(cgtls, cgbp+48, cgbp) != 0) {

		Xprintf(cgtls, "Error in test_overflow1\nexpected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, " with inex = 0\n     got \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+48, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, " with inex = %d\n\x00", iqlibc.ppVaList(cgbp+72, aainex))
		Xexit(cgtls, ppint32(1))
	}
	if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 {

		Xprintf(cgtls, "Error in test_overflow1: overflow flag set\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
}

func sitest_overflow2(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaerr, aai, aainex, aainf, aaoverflow, aarnd, ccv6 ppint32
	var ccv3, ccv4, ccv5 ppbool
	var pp_ /* r at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaerr, aai, aainex, aainf, aaoverflow, aarnd, ccv3, ccv4, ccv5, ccv6
	aaerr = 0

	Xmpfr_inits2(cgtls, ppint32(8), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))

	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	Xmpfr_setmin(cgtls, cgbp, X__gmpfr_emax)                                                               /* x = 0.1@emax */
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(-iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN)) /* y = -2 */
	/* The intermediate multiplication x * y will overflow. */

	aai = -ppint32(9)
	for {
		if !(aai <= ppint32(9)) {
			break
		}
		aarnd = 0
		for {
			if !(aarnd < ppint32(ecMPFR_RNDF)) {
				break
			}

			aainf = iqlibc.ppBoolInt32(aarnd == ppint32(ecMPFR_RNDN) || aarnd == ppint32(ecMPFR_RNDD) || aarnd == ppint32(ecMPFR_RNDA))
			aaoverflow = iqlibc.ppBoolInt32(aainf != 0 || aai <= 0)

			aainex = Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint32(-aai), X__gmpfr_emin, ppint32(ecMPFR_RNDN))

			if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv3 {
				Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(137), "inex == 0\x00")
			}
			pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

			Xmpfr_clear_flags(cgtls)
			/* One has: x * y = -1@emax exactly (but not representable). */
			aainex = Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, aarnd)
			if aaoverflow^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) != 0 {

				Xprintf(cgtls, "Error in test_overflow2 (i = %d, %s): wrong overflow flag (should be %d)\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), aaoverflow))
				aaerr = ppint32(1)
			}
			if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 {

				Xprintf(cgtls, "Error in test_overflow2 (i = %d, %s): NaN flag should not be set\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aarnd)))
				aaerr = ppint32(1)
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {

				Xprintf(cgtls, "Error in test_overflow2 (i = %d, %s): got NaN\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aarnd)))
				aaerr = ppint32(1)
			} else {
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_sign > 0 {

					Xprintf(cgtls, "Error in test_overflow2 (i = %d, %s): wrong sign (+ instead of -)\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aarnd)))
					aaerr = ppint32(1)
				} else {
					if aainf != 0 && !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

						Xprintf(cgtls, "Error in test_overflow2 (i = %d, %s): expected -Inf, got\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xmpfr_dump(cgtls, cgbp+48)
						aaerr = ppint32(1)
					} else {
						if ccv5 = !(aainf != 0); ccv5 {
							if ccv4 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); !ccv4 {
								Xmpfr_nextbelow(cgtls, cgbp+48)
							}
						}
						if ccv5 && (ccv4 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))) {

							Xprintf(cgtls, "Error in test_overflow2 (i = %d, %s): expected -MAX, got\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aarnd)))
							Xmpfr_dump(cgtls, cgbp+48)
							aaerr = ppint32(1)
						}
					}
				}
			}
			if aainf != 0 {
				ccv6 = iqlibc.ppBoolInt32(aainex >= 0)
			} else {
				ccv6 = iqlibc.ppBoolInt32(aainex <= 0)
			}
			if ccv6 != 0 {

				Xprintf(cgtls, "Error in test_overflow2 (i = %d, %s): wrong inexact flag (got %d)\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), aainex))
				aaerr = ppint32(1)
			}

			goto cg_2
		cg_2:
			;
			aarnd++
		}
		goto cg_1
	cg_1:
		;
		aai++
	}

	if aaerr != 0 {
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
}

func sitest_underflow1(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaerr, aainex, aarnd, aasigny, aasignz, ccv4 ppint32
	var pp_ /* r at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_ = aaerr, aainex, aarnd, aasigny, aasignz, ccv4
	aaerr = 0

	Xmpfr_inits2(cgtls, ppint32(8), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))

	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	Xmpfr_setmin(cgtls, cgbp, X__gmpfr_emin) /* x = 0.1@emin */

	aasigny = -ppint32(1)
	for {
		if !(aasigny <= ppint32(1)) {
			break
		}

		Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(aasigny), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN)) /* |y| = 1/2 */
		aasignz = -ppint32(3)
		for {
			if !(aasignz <= ppint32(3)) {
				break
			}

			aarnd = 0
			for {
				if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
					break
				}

				pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint32(aasignz), 0, ppint32(ecMPFR_RNDN))
				if aasignz > 0 {
					ccv4 = aasignz
				} else {
					ccv4 = -aasignz
				}
				if ccv4 != ppint32(1) {
					Xmpfr_setmax(cgtls, cgbp+32, X__gmpfr_emax)
				}
				/* |z| = 1 or 2^emax - ulp */
				Xmpfr_clear_flags(cgtls)
				aainex = Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, aarnd)
				if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 {

					Xprintf(cgtls, "Error in test_underflow1 (signy = %d, signz = %d, %s)\n  NaN flag is set\n\x00", iqlibc.ppVaList(cgbp+72, aasigny, aasignz, Xmpfr_print_rnd_mode(cgtls, aarnd)))
					aaerr = ppint32(1)
				}
				Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))
				if aasigny < 0 && (aarnd == ppint32(ecMPFR_RNDD) || aarnd == ppint32(ecMPFR_RNDZ) && -aasignz > 0 || aarnd == ppint32(ecMPFR_RNDA) && -aasignz < 0) {
					Xmpfr_nextbelow(cgtls, cgbp+32)
				}
				if aasigny > 0 && (aarnd == ppint32(ecMPFR_RNDU) || aarnd == ppint32(ecMPFR_RNDZ) && -aasignz < 0 || aarnd == ppint32(ecMPFR_RNDA) && -aasignz > 0) {
					Xmpfr_nextabove(cgtls, cgbp+32)
				}
				if iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0)^iqlibc.ppBoolInt32(iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) != 0) != 0 {

					Xprintf(cgtls, "Error in test_underflow1 (signy = %d, signz = %d, %s)\n  wrong overflow flag\n\x00", iqlibc.ppVaList(cgbp+72, aasigny, aasignz, Xmpfr_print_rnd_mode(cgtls, aarnd)))
					aaerr = ppint32(1)
				}
				if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {

					Xprintf(cgtls, "Error in test_underflow1 (signy = %d, signz = %d, %s)\n  underflow flag is set\n\x00", iqlibc.ppVaList(cgbp+72, aasigny, aasignz, Xmpfr_print_rnd_mode(cgtls, aarnd)))
					aaerr = ppint32(1)
				}
				if !(Xmpfr_equal_p(cgtls, cgbp+48, cgbp+32) != 0) {

					Xprintf(cgtls, "Error in test_underflow1 (signy = %d, signz = %d, %s)\n  got        \x00", iqlibc.ppVaList(cgbp+72, aasigny, aasignz, Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xmpfr_dump(cgtls, cgbp+48)
					Xprintf(cgtls, "  instead of \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					aaerr = ppint32(1)
				}
				if aainex >= 0 && (aarnd == ppint32(ecMPFR_RNDD) || aarnd == ppint32(ecMPFR_RNDZ) && aasignz < 0 || aarnd == ppint32(ecMPFR_RNDN) && aasigny > 0) {

					Xprintf(cgtls, "Error in test_underflow1 (signy = %d, signz = %d, %s)\n  ternary value = %d instead of < 0\n\x00", iqlibc.ppVaList(cgbp+72, aasigny, aasignz, Xmpfr_print_rnd_mode(cgtls, aarnd), aainex))
					aaerr = ppint32(1)
				}
				if aainex <= 0 && (aarnd == ppint32(ecMPFR_RNDU) || aarnd == ppint32(ecMPFR_RNDZ) && aasignz > 0 || aarnd == ppint32(ecMPFR_RNDN) && aasigny < 0) {

					Xprintf(cgtls, "Error in test_underflow1 (signy = %d, signz = %d, %s)\n  ternary value = %d instead of > 0\n\x00", iqlibc.ppVaList(cgbp+72, aasigny, aasignz, Xmpfr_print_rnd_mode(cgtls, aarnd), aainex))
					aaerr = ppint32(1)
				}

				goto cg_3
			cg_3:
				;
				aarnd++
			}

			goto cg_2
		cg_2:
			;
			aasignz += ppint32(2)
		}

		goto cg_1
	cg_1:
		;
		aasigny += ppint32(2)
	}

	if aaerr != 0 {
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
}

func sitest_underflow2(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aab, aaerr, aai, aainex, aasame, ccv3, ccv4 ppint32
	var pp_ /* r at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aab, aaerr, aai, aainex, aasame, ccv3, ccv4
	aaerr = 0

	Xmpfr_inits2(cgtls, ppint32(32), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))

	Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint32(-ppint32(1)), X__gmpfr_emin, ppint32(ecMPFR_RNDN)) /* z = -2^emin */
	Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(1), X__gmpfr_emin, ppint32(ecMPFR_RNDN))              /* x = 2^emin */

	aab = 0
	for {
		if !(aab <= ppint32(1)) {
			break
		}

		aai = ppint32(15)
		for {
			if !(aai <= ppint32(17)) {
				break
			}

			Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(aai), ppint32(-ppint32(4))-(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_prec, ppint32(ecMPFR_RNDN))
			/*  z = -1.000...00b
			 * xy =             01111
			 *   or             10000
			 *   or             10001
			 */
			Xmpfr_clear_flags(cgtls)
			aainex = Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
			if X__gmpfr_flags != ppuint32(mvMPFR_FLAGS_INEXACT) {

				Xprintf(cgtls, "Error in test_underflow2 (b = %d, i = %d)\n  flags = %u instead of %u\n\x00", iqlibc.ppVaList(cgbp+72, aab, aai, X__gmpfr_flags, iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)))
				aaerr = ppint32(1)
			}
			aasame = iqlibc.ppBoolInt32(aai == ppint32(15) || aai == ppint32(16) && aab == 0)
			if aasame != 0 {
				ccv3 = iqlibc.ppBoolInt32(aainex >= 0)
			} else {
				ccv3 = iqlibc.ppBoolInt32(aainex <= 0)
			}
			if ccv3 != 0 {

				if aasame != 0 {
					ccv4 = ppint32('<')
				} else {
					ccv4 = ppint32('>')
				}
				Xprintf(cgtls, "Error in test_underflow2 (b = %d, i = %d)\n  incorrect ternary value (%d instead of %c 0)\n\x00", iqlibc.ppVaList(cgbp+72, aab, aai, aainex, ccv4))
				aaerr = ppint32(1)
			}
			Xmpfr_neg(cgtls, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
			if !(aasame != 0) {
				Xmpfr_nextabove(cgtls, cgbp+16)
			}
			if !(Xmpfr_equal_p(cgtls, cgbp+48, cgbp+16) != 0) {

				Xprintf(cgtls, "Error in test_underflow2 (b = %d, i = %d)\n  expected \x00", iqlibc.ppVaList(cgbp+72, aab, aai))
				Xmpfr_dump(cgtls, cgbp+16)
				Xprintf(cgtls, "  got      \x00", 0)
				Xmpfr_dump(cgtls, cgbp+48)
				aaerr = ppint32(1)
			}

			goto cg_2
		cg_2:
			;
			aai++
		}
		Xmpfr_nextbelow(cgtls, cgbp+32)

		goto cg_1
	cg_1:
		;
		aab++
	}

	if aaerr != 0 {
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaN, aan ppuint32
	var aa_p tnmpfr_srcptr
	var aa_p1, aa_p2, aa_p3, aa_p4 tnmpfr_ptr
	var aacompare, aainexact, ccv1, ccv20, ccv22, ccv41, ccv44 ppint32
	var aaexpo tnmpfr_save_expo_t
	var aap0, aap1, aaprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv28, ccv29, ccv30, ccv33, ccv34, ccv35, ccv38, ccv39, ccv40, ccv8, ccv9 ppbool
	var pp_ /* s at bp+48 */ tnmpfr_t
	var pp_ /* slong at bp+80 */ tnmpfr_t
	var pp_ /* t at bp+64 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaN, aa_p, aa_p1, aa_p2, aa_p3, aa_p4, aacompare, aaexpo, aainexact, aan, aap0, aap1, aaprec, aarnd, ccv1, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv20, ccv22, ccv28, ccv29, ccv30, ccv33, ccv34, ccv35, ccv38, ccv39, ccv40, ccv41, ccv44, ccv8, ccv9

	Xtests_start_mpfr(cgtls)

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+48)
	Xmpfr_init(cgtls, cgbp+16)
	Xmpfr_init(cgtls, cgbp+32)

	/* check special cases */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(2))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(2))
	Xmpfr_set_prec(cgtls, cgbp+48, ppint32(2))
	Xmpfr_set_str(cgtls, cgbp, "-0.75\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+16, "0.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "-0.375\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDU)) /* result is 0 */
	if Xmpfr_sgn(cgtls, cgbp+48) != 0 {

		Xprintf(cgtls, "Error: -0.75 * 0.5 - -0.375 should be equal to 0 for prec=2\n\x00", 0)
		Xprintf(cgtls, "got instead \x00", 0)
		Xmpfr_dump(cgtls, cgbp+48)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(27))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(27))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(27))
	Xmpfr_set_prec(cgtls, cgbp+48, ppint32(27))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.11111111111111111111111111e-1\x00")
	{
		aa_p = cgbp
		ccv1 = Xmpfr_set4(cgtls, cgbp+16, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
	}
	pp_ = ccv1
	Xmpfr_set_str_binary(cgtls, cgbp+32, "1.00011110100011001011001001e-1\x00")
	if Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN)) >= 0 {

		Xprintf(cgtls, "Wrong inexact flag for x=y=1-2^(-27)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_nan(cgtls, cgbp)
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+16, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "evaluation of function in x=NAN does not return NAN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_nan(cgtls, cgbp+16)
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "evaluation of function in y=NAN does not return NAN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_nan(cgtls, cgbp+32)
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+16, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "evaluation of function in z=NAN does not return NAN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	Xmpfr_set_inf(cgtls, cgbp+16, ppint32(1))
	Xmpfr_set_inf(cgtls, cgbp+32, -ppint32(1))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv10 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv10 {
		if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv8 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(417), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv9 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(417), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv10 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error for (+inf) * (+inf) - (-inf)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_set_inf(cgtls, cgbp+16, -ppint32(1))
	Xmpfr_set_inf(cgtls, cgbp+32, -ppint32(1))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv13 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv13 {
		if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv11 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(427), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv12 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(427), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv13 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error for (-inf) * (-inf) - (-inf)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	Xmpfr_set_inf(cgtls, cgbp+16, -ppint32(1))
	Xmpfr_set_inf(cgtls, cgbp+32, ppint32(1))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv16 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv16 {
		if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv14 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(437), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv15 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv15 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(437), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv15 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv16 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_sign > 0 {

		Xprintf(cgtls, "Error for (+inf) * (-inf) - (+inf)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_set_inf(cgtls, cgbp+16, ppint32(1))
	Xmpfr_set_inf(cgtls, cgbp+32, ppint32(1))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv19 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv19 {
		if ccv17 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv17 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(447), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv17 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv18 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(447), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv19 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_sign > 0 {

		Xprintf(cgtls, "Error for (-inf) * (+inf) - (+inf)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	{
		aa_p1 = cgbp + 16
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv20 = 0
	}
	pp_ = ccv20
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "evaluation of function in x=INF y=0  does not return NAN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp+16, ppint32(1))
	{
		aa_p2 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv22 = 0
	}
	pp_ = ccv22
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "evaluation of function in x=0 y=INF does not return NAN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+16, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands))) /* always positive */
	Xmpfr_set_inf(cgtls, cgbp+32, ppint32(1))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "evaluation of function in x=INF y>0 z=INF does not return NAN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp+16, ppint32(1))
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_set_inf(cgtls, cgbp+32, ppint32(1))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "evaluation of function in x>0 y=INF z=INF does not return NAN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	for cgcond := true; cgcond; cgcond = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp+16, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	}
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv30 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv30 {
		if ccv28 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv28 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(497), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv28 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv29 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv29 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(497), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv29 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv30 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "evaluation of function at x=INF does not return INF\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp+16, ppint32(1))
	for cgcond := true; cgcond; cgcond = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	}
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv35 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv35 {
		if ccv33 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv33 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(507), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv33 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv34 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv34 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(507), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv34 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv35 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "evaluation of function at y=INF does not return INF\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp+32, -ppint32(1))
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+16, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv40 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv40 {
		if ccv38 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv38 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(517), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv38 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv39 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv39 {
			Xmpfr_assert_fail(cgtls, "tfms.c\x00", ppint32(517), "! (((s)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv39 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv40 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "evaluation of function in z=-INF does not return INF\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	{
		aa_p3 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv41 = 0
	}
	pp_ = ccv41
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+16, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))
	if !(Xmpfr_equal_p(cgtls, cgbp+48, cgbp+32) != 0) {

		Xprintf(cgtls, "evaluation of function in x=0 does not return -z\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	{
		aa_p4 = cgbp + 16
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p4)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p4)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv44 = 0
	}
	pp_ = ccv44
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))
	if !(Xmpfr_equal_p(cgtls, cgbp+48, cgbp+32) != 0) {

		Xprintf(cgtls, "evaluation of function in y=0 does not return -z\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	aap0 = ppint32(2)
	aap1 = ppint32(200)
	aaN = ppuint32(200)

	Xmpfr_init(cgtls, cgbp+64)
	Xmpfr_init(cgtls, cgbp+80)

	/* generic test */
	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+16, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+32, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+48, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+64, aaprec)

		aan = ppuint32(0)
		for {
			if !(aan < aaN) {
				break
			}

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp+16, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))

			if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
				Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
			}
			if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
				Xmpfr_neg(cgtls, cgbp+16, cgbp+16, ppint32(ecMPFR_RNDN))
			}
			if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
				Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))
			}

			aarnd = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % ppuint32(ecMPFR_RNDF))
			Xmpfr_set_prec(cgtls, cgbp+80, ppint32(2)*aaprec)
			if Xmpfr_mul(cgtls, cgbp+80, cgbp, cgbp+16, aarnd) != 0 {

				Xprintf(cgtls, "x*y should be exact\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
			aacompare = Xmpfr_sub(cgtls, cgbp+64, cgbp+80, cgbp+32, aarnd)
			aainexact = Xmpfr_fms(cgtls, cgbp+48, cgbp, cgbp+16, cgbp+32, aarnd)
			if !(Xmpfr_equal_p(cgtls, cgbp+48, cgbp+64) != 0) {

				Xprintf(cgtls, "results differ for\n\x00", 0)
				Xprintf(cgtls, "  x=\x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "  y=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+16)
				Xprintf(cgtls, "  z=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xprintf(cgtls, "  with prec=%u rnd_mode=%s\n\x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppUint32FromInt32(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
				Xprintf(cgtls, "got      \x00", 0)
				Xmpfr_dump(cgtls, cgbp+48)
				Xprintf(cgtls, "expected \x00", 0)
				Xmpfr_dump(cgtls, cgbp+64)
				Xprintf(cgtls, "approx   \x00", 0)
				Xmpfr_dump(cgtls, cgbp+80)
				Xexit(cgtls, ppint32(1))
			}
			if !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aacompare > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aacompare < iqlibc.ppInt32FromInt32(0))) {

				Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+104, Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare, aainexact))
				Xprintf(cgtls, " x=\x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, " y=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+16)
				Xprintf(cgtls, " z=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xprintf(cgtls, " s=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+48)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_48
		cg_48:
			;
			aan++
		}

		goto cg_47
	cg_47:
		;
		aaprec++
	}
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+80)

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+48)

	sitest_exact(cgtls)

	aaexpo.fdsaved_flags = X__gmpfr_flags
	aaexpo.fdsaved_emin = X__gmpfr_emin
	aaexpo.fdsaved_emax = X__gmpfr_emax
	X__gmpfr_emin = iqlibc.ppInt32FromInt32(1) - iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))
	X__gmpfr_emax = iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2)) - iqlibc.ppInt32FromInt32(1)
	sitest_overflow1(cgtls)
	sitest_overflow2(cgtls)
	sitest_underflow1(cgtls)
	sitest_underflow2(cgtls)
	X__gmpfr_flags = aaexpo.fdsaved_flags
	X__gmpfr_emin = aaexpo.fdsaved_emin
	X__gmpfr_emax = aaexpo.fdsaved_emax

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

var ___gmpfr_emax ppint32

var ___gmpfr_emin ppint32

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32, ppuintptr, ppint32) ppuint32

func _exit(*iqlibc.ppTLS, ppint32)

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_fms(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_inits2(*iqlibc.ppTLS, ppint32, ppuintptr, ppuintptr)

func _mpfr_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppint8

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint32, ppint32, ppint32) ppint32

func _mpfr_setmax(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_setmin(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_sgn(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint32

var _stdout ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

// Code generated for linux/386 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DMPFR_LONG_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/386 -UHAVE_NEARBYINT -mlong-double-64 -c -o tpow.o.go tpow.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 32
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT32_MAX"
const mvINTPTR_MIN = "INT32_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 2303
const mvMPFR_AI_THRESHOLD3 = 37484
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 522
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 6920
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_FSPEC = "j"
const mvMPFR_INTMAX_MAX = "INTMAX_MAX"
const mvMPFR_INTMAX_MIN = "INTMAX_MIN"
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 5
const mvMPFR_LOG2_PREC_BITS = 5
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 13
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 32
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 28160
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 18
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/x86/mparam.h"
const mvMPFR_UINTMAX_MAX = "UINTMAX_MAX"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT32_MAX"
const mvPTRDIFF_MIN = "INT32_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZE_MAX = "UINT32_MAX"
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTGENERIC_NOWARNING = 1
const mvTGENERIC_SO_TEST = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT32_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_ILP32 = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_H_HAVE_INTMAX_T = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_REDIR_TIME64 = 1
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 2
const mv__FLT_EVAL_METHOD__ = 2
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "i686-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__ILP32__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LAHF_SAHF__ = 1
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 2147483647
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "ll"
const mv__PRIPTR = ""
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 12
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__code_model_32__ = 1
const mv__gnu_linux__ = 1
const mv__i386 = 1
const mv__i386__ = 1
const mv__i686 = 1
const mv__i686__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pentiumpro = 1
const mv__pentiumpro__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvi386 = 1
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_sj = "__gmpfr_mpfr_get_sj"
const mvmpfr_get_uj = "__gmpfr_mpfr_get_uj"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpfr_pow_sj = "__gmpfr_mpfr_pow_sj"
const mvmpfr_pow_uj = "__gmpfr_mpfr_pow_uj"
const mvmpfr_pown = "mpfr_pow_sj"
const mvmpfr_set_sj = "__gmpfr_set_sj"
const mvmpfr_set_sj_2exp = "__gmpfr_set_sj_2exp"
const mvmpfr_set_uj = "__gmpfr_set_uj"
const mvmpfr_set_uj_2exp = "__gmpfr_set_uj_2exp"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvtest_pow = "mpfr_pow"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint32

type tnsize_t = ppuint32

type tnssize_t = ppint32

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnuintptr_t = ppuint32

type tnintptr_t = ppint32

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fdquot tnintmax_t
	fdrem  tnintmax_t
}

type tnmpfr_uintmax_t = ppuint64

type tnmpfr_intmax_t = ppint64

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint32

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint32

type tnmpfr_ulong = ppuint32

type tnmpfr_size_t = ppuint32

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint32

type tnmpfr_uprec_t = ppuint32

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint32

type tnmpfr_uexp_t = ppuint32

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint32

type tnmpfr_ueexp_t = ppuint32

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppint8
	fdfrac_digits        ppint8
	fdp_cs_precedes      ppint8
	fdp_sep_by_space     ppint8
	fdn_cs_precedes      ppint8
	fdn_sep_by_space     ppint8
	fdp_sign_posn        ppint8
	fdn_sign_posn        ppint8
	fdint_p_cs_precedes  ppint8
	fdint_p_sep_by_space ppint8
	fdint_n_cs_precedes  ppint8
	fdint_n_sep_by_space ppint8
	fdint_p_sign_posn    ppint8
	fdint_n_sign_posn    ppint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

/* Generic test file for functions with one or two arguments (the second being
   either mpfr_t or double or unsigned long).

Copyright 2001-2023 Free Software Foundation, Inc.
Contributed by the AriC and Caramba projects, INRIA.

This file is part of the GNU MPFR Library.

The GNU MPFR Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The GNU MPFR Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

/* Define TWO_ARGS for two-argument functions like mpfr_pow.
   Define DOUBLE_ARG1 or DOUBLE_ARG2 for function with a double operand in
   first or second place like sub_d or d_sub.
   Define ULONG_ARG1 or ULONG_ARG2 for function with an unsigned long
   operand in first or second place like sub_ui or ui_sub.
   Define THREE_ARGS for three-argument functions like mpfr_atan2u. */

/* TODO: Add support for type long and extreme integer values, as done
   in tgeneric_ui.c; then tgeneric_ui.c could probably disappear. */

/* For the random function: one number on two is negative. */

/* If the MPFR_SUSPICIOUS_OVERFLOW test fails but this is not a bug,
   then define TGENERIC_SO_TEST with an adequate test (possibly 0) to
   omit this particular case. */

/* For some functions (for example cos), the argument reduction is too
   expensive when using mpfr_get_emax(). Then simply define REDUCE_EMAX
   to some reasonable value before including tgeneric.c. */

/* same for mpfr_get_emin() */

func sitest_generic(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aanmax ppuint32) {
	cgbp := cgtls.ppAlloc(192)
	defer cgtls.ppFree(192)

	var aa_p, aa_p1, aa_p2, aa_p3 tnmpfr_ptr
	var aa_p4 tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, aainfinite_input, aatest_of, aatest_uf, ccv11, ccv13, ccv14, ccv15, ccv17, ccv19, ccv22, ccv24, ccv25, ccv26, ccv28, ccv30, ccv33, ccv35, ccv36, ccv37, ccv39, ccv41, ccv44, ccv46, ccv47, ccv48, ccv52, ccv6, ccv8 ppint32
	var aactrn, aactrt, aan, ccv3 ppuint32
	var aae, aaemax, aaemin, aaoemax, aaoemin, aaold_emax, aaold_emin tnmpfr_exp_t
	var aaex_flags, aaex_flags1, aaflags, aaoldflags tnmpfr_flags_t
	var aaprec, aaxprec, aayprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv12, ccv16, ccv20, ccv23, ccv27, ccv31, ccv34, ccv38, ccv42, ccv45, ccv49, ccv5, ccv50, ccv51, ccv53, ccv54, ccv55, ccv56, ccv9 ppbool
	var ccv4 ppfloat64
	var pp_ /* t at bp+48 */ tnmpfr_t
	var pp_ /* w at bp+64 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* x2 at bp+112 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* yd at bp+80 */ tnmpfr_t
	var pp_ /* yu at bp+96 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aa_p2, aa_p3, aa_p4, aacompare, aacompare2, aactrn, aactrt, aae, aaemax, aaemin, aaex_flags, aaex_flags1, aaflags, aainexact, aainfinite_input, aan, aaoemax, aaoemin, aaold_emax, aaold_emin, aaoldflags, aaprec, aarnd, aatest_of, aatest_uf, aaxprec, aayprec, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv17, ccv19, ccv20, ccv22, ccv23, ccv24, ccv25, ccv26, ccv27, ccv28, ccv3, ccv30, ccv31, ccv33, ccv34, ccv35, ccv36, ccv37, ccv38, ccv39, ccv4, ccv41, ccv42, ccv44, ccv45, ccv46, ccv47, ccv48, ccv49, ccv5, ccv50, ccv51, ccv52, ccv53, ccv54, ccv55, ccv56, ccv6, ccv8, ccv9
	aactrt = ppuint32(0)
	aactrn = ppuint32(0)

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	Xmpfr_inits2(cgtls, ppint32(mvMPFR_PREC_MIN), cgbp, iqlibc.ppVaList(cgbp+136, cgbp+16, cgbp+80, cgbp+96, cgbp+32, cgbp+48, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_init2(cgtls, cgbp+112, ppint32(mvMPFR_PREC_MIN))

	/* generic tests */
	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		/* Number of overflow/underflow tests for each precision.
		   Since MPFR uses several algorithms and there may also be
		   early overflow/underflow detection, several tests may be
		   needed to detect a bug. */
		aatest_of = ppint32(3)
		aatest_uf = ppint32(3)

		Xmpfr_set_prec(cgtls, cgbp+32, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+48, aaprec)
		aayprec = aaprec + ppint32(20)
		Xmpfr_set_prec(cgtls, cgbp+16, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+80, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+96, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+64, aayprec)

		/* Note: in precision p1, we test NSPEC special cases. */
		aan = ppuint32(0)
		for {
			if aaprec == aap1 {
				ccv3 = aanmax + ppuint32(9)
			} else {
				ccv3 = aanmax
			}
			if !(aan < ccv3) {
				break
			}

			aainfinite_input = 0

			aaxprec = aaprec
			if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {

				/* In half cases, modify the precision of the inputs:
				   If the base precision (for the result) is small,
				   take a larger input precision in general, else
				   take a smaller precision. */
				if aaprec < ppint32(16) {
					ccv4 = ppfloat64(256)
				} else {
					ccv4 = ppfloat64(1)
				}
				aaxprec = tnmpfr_prec_t(ppfloat64(aaxprec) * (ccv4 * ppfloat64(Xrandlimb(cgtls)) / ppfloat64(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1)))))
				if aaxprec < ppint32(mvMPFR_PREC_MIN) {
					aaxprec = ppint32(mvMPFR_PREC_MIN)
				}
			}
			Xmpfr_set_prec(cgtls, cgbp, aaxprec)
			Xmpfr_set_prec(cgtls, cgbp+112, aaxprec)

			/* Generate random arguments, even in the special cases
			   (this may not be needed, but this is simpler).
			   Note that if RAND_FUNCTION is defined, this specific
			   random function is used for all arguments; this is
			   typically mpfr_random2, which generates a positive
			   random mpfr_t with long runs of consecutive ones and
			   zeros in the binary representation. */

			Xtests_default_random(cgtls, cgbp, ppint32(16), ppint32(-ppint32(256)), ppint32(255), 0)
			Xtests_default_random(cgtls, cgbp+112, ppint32(256), ppint32(-ppint32(256)), ppint32(255), 0)

			if aan < ppuint32(9) && aaprec == aap1 {

				/* Special cases tested in precision p1 if n < NSPEC. They are
				   useful really in the extended exponent range. */
				/* TODO: x2 is set even when it is associated with a double;
				   check whether this really makes sense. */
				Xset_emin(cgtls, iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2)))
				Xset_emax(cgtls, iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))-iqlibc.ppInt32FromInt32(1))
				if aan == ppuint32(0) {

					Xmpfr_set_nan(cgtls, cgbp)
				} else {
					if aan <= ppuint32(2) {

						if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aan == ppuint32(1) || aan == ppuint32(2))), ppint32(1)) != 0; !ccv5 {
							Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(290), "n == 1 || n == 2\x00")
						}
						pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
						if aan == ppuint32(1) {
							ccv6 = ppint32(1)
						} else {
							ccv6 = -ppint32(1)
						}
						pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(ccv6), 0, ppint32(ecMPFR_RNDN))
						Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)

						if ccv9 = iqlibc.Bool(0 != 0); ccv9 {
							if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
								ccv8 = -ppint32(1)
							} else {
								ccv8 = ppint32(1)
							}
						}
						if ccv9 && ppint32(ccv8) >= 0 {
							if ccv12 = iqlibc.Bool(0 != 0); ccv12 {
								if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
									ccv11 = -ppint32(1)
								} else {
									ccv11 = ppint32(1)
								}
							}
							if ccv12 && iqlibc.ppUint32FromInt32(ppint32(ccv11)) == ppuint32(0) {
								{
									aa_p = cgbp + 112
									(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
									(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
									pp_ = ppint32(ecMPFR_RNDN)
									ccv13 = 0
								}
								pp_ = ccv13
							} else {
								if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
									ccv14 = -ppint32(1)
								} else {
									ccv14 = ppint32(1)
								}
								Xmpfr_set_ui_2exp(cgtls, cgbp+112, iqlibc.ppUint32FromInt32(ppint32(ccv14)), 0, ppint32(ecMPFR_RNDN))
							}
						} else {
							if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
								ccv15 = -ppint32(1)
							} else {
								ccv15 = ppint32(1)
							}
							Xmpfr_set_si_2exp(cgtls, cgbp+112, ppint32(ccv15), 0, ppint32(ecMPFR_RNDN))
						}
						Xmpfr_set_exp(cgtls, cgbp+112, X__gmpfr_emin)
					} else {
						if aan <= ppuint32(4) {

							if ccv16 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aan == ppuint32(3) || aan == ppuint32(4))), ppint32(1)) != 0; !ccv16 {
								Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(300), "n == 3 || n == 4\x00")
							}
							pp_ = ccv16 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
							if aan == ppuint32(3) {
								ccv17 = ppint32(1)
							} else {
								ccv17 = -ppint32(1)
							}
							pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(ccv17), 0, ppint32(ecMPFR_RNDN))
							Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)

							if ccv20 = iqlibc.Bool(0 != 0); ccv20 {
								if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
									ccv19 = -ppint32(1)
								} else {
									ccv19 = ppint32(1)
								}
							}
							if ccv20 && ppint32(ccv19) >= 0 {
								if ccv23 = iqlibc.Bool(0 != 0); ccv23 {
									if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
										ccv22 = -ppint32(1)
									} else {
										ccv22 = ppint32(1)
									}
								}
								if ccv23 && iqlibc.ppUint32FromInt32(ppint32(ccv22)) == ppuint32(0) {
									{
										aa_p1 = cgbp + 112
										(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
										(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
										pp_ = ppint32(ecMPFR_RNDN)
										ccv24 = 0
									}
									pp_ = ccv24
								} else {
									if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
										ccv25 = -ppint32(1)
									} else {
										ccv25 = ppint32(1)
									}
									Xmpfr_set_ui_2exp(cgtls, cgbp+112, iqlibc.ppUint32FromInt32(ppint32(ccv25)), 0, ppint32(ecMPFR_RNDN))
								}
							} else {
								if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
									ccv26 = -ppint32(1)
								} else {
									ccv26 = ppint32(1)
								}
								Xmpfr_set_si_2exp(cgtls, cgbp+112, ppint32(ccv26), 0, ppint32(ecMPFR_RNDN))
							}
							Xmpfr_setmax(cgtls, cgbp+112, X__gmpfr_emax)
						} else {
							if aan <= ppuint32(6) {

								if ccv27 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aan == ppuint32(5) || aan == ppuint32(6))), ppint32(1)) != 0; !ccv27 {
									Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(311), "n == 5 || n == 6\x00")
								}
								pp_ = ccv27 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
								if aan == ppuint32(5) {
									ccv28 = ppint32(1)
								} else {
									ccv28 = -ppint32(1)
								}
								pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(ccv28), 0, ppint32(ecMPFR_RNDN))
								Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)

								if ccv31 = iqlibc.Bool(0 != 0); ccv31 {
									if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
										ccv30 = -ppint32(1)
									} else {
										ccv30 = ppint32(1)
									}
								}
								if ccv31 && ppint32(ccv30) >= 0 {
									if ccv34 = iqlibc.Bool(0 != 0); ccv34 {
										if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
											ccv33 = -ppint32(1)
										} else {
											ccv33 = ppint32(1)
										}
									}
									if ccv34 && iqlibc.ppUint32FromInt32(ppint32(ccv33)) == ppuint32(0) {
										{
											aa_p2 = cgbp + 112
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_sign = ppint32(1)
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
											pp_ = ppint32(ecMPFR_RNDN)
											ccv35 = 0
										}
										pp_ = ccv35
									} else {
										if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
											ccv36 = -ppint32(1)
										} else {
											ccv36 = ppint32(1)
										}
										Xmpfr_set_ui_2exp(cgtls, cgbp+112, iqlibc.ppUint32FromInt32(ppint32(ccv36)), 0, ppint32(ecMPFR_RNDN))
									}
								} else {
									if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
										ccv37 = -ppint32(1)
									} else {
										ccv37 = ppint32(1)
									}
									Xmpfr_set_si_2exp(cgtls, cgbp+112, ppint32(ccv37), 0, ppint32(ecMPFR_RNDN))
								}
								Xmpfr_setmax(cgtls, cgbp+112, X__gmpfr_emax)
							} else {

								if ccv38 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aan == ppuint32(7) || aan == ppuint32(8))), ppint32(1)) != 0; !ccv38 {
									Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(319), "n == 7 || n == 8\x00")
								}
								pp_ = ccv38 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
								if aan == ppuint32(7) {
									ccv39 = ppint32(1)
								} else {
									ccv39 = -ppint32(1)
								}
								pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(ccv39), 0, ppint32(ecMPFR_RNDN))
								Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)

								if ccv42 = iqlibc.Bool(0 != 0); ccv42 {
									if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
										ccv41 = -ppint32(1)
									} else {
										ccv41 = ppint32(1)
									}
								}
								if ccv42 && ppint32(ccv41) >= 0 {
									if ccv45 = iqlibc.Bool(0 != 0); ccv45 {
										if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
											ccv44 = -ppint32(1)
										} else {
											ccv44 = ppint32(1)
										}
									}
									if ccv45 && iqlibc.ppUint32FromInt32(ppint32(ccv44)) == ppuint32(0) {
										{
											aa_p3 = cgbp + 112
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_sign = ppint32(1)
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
											pp_ = ppint32(ecMPFR_RNDN)
											ccv46 = 0
										}
										pp_ = ccv46
									} else {
										if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
											ccv47 = -ppint32(1)
										} else {
											ccv47 = ppint32(1)
										}
										Xmpfr_set_ui_2exp(cgtls, cgbp+112, iqlibc.ppUint32FromInt32(ppint32(ccv47)), 0, ppint32(ecMPFR_RNDN))
									}
								} else {
									if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
										ccv48 = -ppint32(1)
									} else {
										ccv48 = ppint32(1)
									}
									Xmpfr_set_si_2exp(cgtls, cgbp+112, ppint32(ccv48), 0, ppint32(ecMPFR_RNDN))
								}
								Xmpfr_set_exp(cgtls, cgbp+112, X__gmpfr_emin)
							}
						}
					}
				}
			}

			/* Exponent range for the test. */
			aaoemin = X__gmpfr_emin
			aaoemax = X__gmpfr_emax

			aarnd = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % iqlibc.ppUint32FromInt32(ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)))
			Xmpfr_clear_flags(cgtls)
			aacompare = Xmpfr_pow(cgtls, cgbp+16, cgbp, cgbp+112, aarnd)
			aaflags = X__gmpfr_flags
			if X__gmpfr_emin != aaoemin || X__gmpfr_emax != aaoemax {

				Xprintf(cgtls, "tgeneric: the exponent range has been modified by the tested function!\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
			if aarnd != ppint32(ecMPFR_RNDF) {
				if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) == iqlibc.ppInt32FromInt32(0)) != 0) {
					Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "bad inexact flag for mpfr_pow\x00"))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					if cgbp+112 != ppuintptr(0) {
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+112)
					}
					if -ppint32(1) >= 0 {
						Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
					}
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}
			aactrt++

			/* If rnd = RNDF, check that we obtain the same result as
			   RNDD or RNDU. */
			if aarnd == ppint32(ecMPFR_RNDF) {

				Xmpfr_pow(cgtls, cgbp+80, cgbp, cgbp+112, ppint32(ecMPFR_RNDD))
				Xmpfr_pow(cgtls, cgbp+96, cgbp, cgbp+112, ppint32(ecMPFR_RNDU))
				if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+16, cgbp+80) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+80)).fd_mpfr_sign || ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+16, cgbp+96) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_sign)) {

					Xprintf(cgtls, "tgeneric: error formpfr_pow, RNDF; result matches neither RNDD nor RNDU\n\x00", 0)
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "x2 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+112)
					Xprintf(cgtls, "yd (RNDD) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+80)
					Xprintf(cgtls, "yu (RNDU) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+96)
					Xprintf(cgtls, "y  (RNDF) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xexit(cgtls, ppint32(1))
				}
			}

			/* Tests in a reduced exponent range. */

			aaoldflags = aaflags

			/* Determine the smallest exponent range containing the
			   exponents of the mpfr_t inputs (x, and u if TWO_ARGS)
			   and output (y). */
			aaemin = iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2)) - iqlibc.ppInt32FromInt32(1)
			aaemax = iqlibc.ppInt32FromInt32(1) - iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))

			if ccv49 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)); ccv49 {
			}
			if ccv49 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv50 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mpfr_exp <= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)); ccv50 {
			}
			if ccv50 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 112)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv51 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp <= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)); ccv51 {
			}
			if ccv51 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 16)).fd_mpfr_exp /* exponent of the result */

				if aatest_of > 0 && aae-ppint32(1) >= aaemax { /* overflow test */

					/* Exponent e of the result > exponents of the inputs;
					   let's set emax to e - 1, so that one should get an
					   overflow. */
					Xset_emax(cgtls, aae-ppint32(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = Xmpfr_pow(cgtls, cgbp+64, cgbp, cgbp+112, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emax(cgtls, aaoemax)
					aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for mpfr_pow, reduced exponent range [%ld,%ld] (overflow test) on:\n\x00", iqlibc.ppVaList(cgbp+136, aaoemin, aae-ppint32(1)))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+112)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+136, aainexact))
						Xmpfr_dump(cgtls, cgbp+64)
						Xexit(cgtls, ppint32(1))
					}
					aatest_of--
				}

				if aatest_uf > 0 && aae+ppint32(1) <= aaemin { /* underflow test */

					/* Exponent e of the result < exponents of the inputs;
					   let's set emin to e + 1, so that one should get an
					   underflow. */
					Xset_emin(cgtls, aae+ppint32(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = Xmpfr_pow(cgtls, cgbp+64, cgbp, cgbp+112, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emin(cgtls, aaoemin)
					aaex_flags1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags1 && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for mpfr_pow, reduced exponent range [%ld,%ld] (underflow test) on:\n\x00", iqlibc.ppVaList(cgbp+136, aae+ppint32(1), aaoemax))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+112)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags1)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+136, aainexact))
						Xmpfr_dump(cgtls, cgbp+64)
						Xexit(cgtls, ppint32(1))
					}
					aatest_uf--
				}

				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			} /* MPFR_IS_PURE_FP (y) */

			if aaemin > aaemax {
				aaemin = aaemax
			} /* case where all values are singular */

			/* Consistency test in a reduced exponent range. Doing it
			   for the first 10 samples and for prec == p1 (which has
			   some special cases) should be sufficient. */
			if aactrt <= ppuint32(10) || aaprec == aap1 {

				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				Xmpfr_clear_flags(cgtls)
				aainexact = Xmpfr_pow(cgtls, cgbp+64, cgbp, cgbp+112, aarnd)
				aaflags = X__gmpfr_flags
				Xset_emin(cgtls, aaoemin)
				Xset_emax(cgtls, aaoemax)
				/* That test makes no sense for RNDF. */
				if aarnd != ppint32(ecMPFR_RNDF) && !(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+64, cgbp+16) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign) && iqlibc.ppBoolInt32(aainexact > 0)-iqlibc.ppBoolInt32(aainexact < 0) == iqlibc.ppBoolInt32(aacompare > 0)-iqlibc.ppBoolInt32(aacompare < 0) && aaflags == aaoldflags) {

					Xprintf(cgtls, "tgeneric: error for mpfr_pow, reduced exponent range [%ld,%ld] on:\n\x00", iqlibc.ppVaList(cgbp+136, aaemin, aaemax))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "x2 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+112)
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "Expected:\n  y = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+136, aacompare))
					Xflags_out(cgtls, aaoldflags)
					Xprintf(cgtls, "Got:\n  w = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+64)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+136, aainexact))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}

			X__gmpfr_flags = aaoldflags /* restore the flags */
			/* tests in a reduced exponent range */

			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp <= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3) {

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 {
					if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "bad NaN flag for mpfr_pow\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+112 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+112)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3) {

						if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "bad overflow flag for mpfr_pow\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+112 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+112)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(iqlibc.ppBoolInt32(aacompare == 0 && !(aainfinite_input != 0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "bad divide-by-zero flag for mpfr_pow\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+112 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+112)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {
							if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "bad underflow flag for mpfr_pow\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+112 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+112)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						}
					}
				}
			} else {
				if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0 {

					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "both overflow and divide-by-zero for mpfr_pow\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+112 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+112)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "both underflow and divide-by-zero for mpfr_pow\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+112 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+112)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !(aacompare == iqlibc.ppInt32FromInt32(0)) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "bad compare value (divide-by-zero) for mpfr_pow\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+112 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+112)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 {

						if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "both underflow and overflow for mpfr_pow\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+112 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+112)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "bad compare value (overflow) for mpfr_pow\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+112 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+112)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						Xmpfr_nexttoinf(cgtls, cgbp+16)
						if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "should have been max MPFR number (overflow) for mpfr_pow\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+112 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+112)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {

							if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "bad compare value (underflow) for mpfr_pow\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+112 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+112)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
							Xmpfr_nexttozero(cgtls, cgbp+16)
							if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+136, "should have been min MPFR number (underflow) for mpfr_pow\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+112 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+112)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						} else {
							if aacompare == 0 || aarnd == ppint32(ecMPFR_RNDF) || Xmpfr_can_round(cgtls, cgbp+16, aayprec, aarnd, aarnd, aaprec) != 0 {

								aactrn++
								{
									aa_p4 = cgbp + 16
									ccv52 = Xmpfr_set4(cgtls, cgbp+48, aa_p4, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p4)).fd_mpfr_sign)
								}
								pp_ = ccv52
								/* Risk of failures are known when some flags are already set
								   before the function call. Do not set the erange flag, as
								   it will remain set after the function call and no checks
								   are performed in such a case (see the mpfr_erangeflag_p
								   test below). */
								if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
									X__gmpfr_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0) ^ iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE))
								}
								/* Let's increase the precision of the inputs in a random way.
								   In most cases, this doesn't make any difference, but for
								   the mpfr_fmod bug fixed in r6230, this triggers the bug. */
								Xmpfr_prec_round(cgtls, cgbp, iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint32(15)), ppint32(ecMPFR_RNDN))
								Xmpfr_prec_round(cgtls, cgbp+112, iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint32(15)), ppint32(ecMPFR_RNDN))
								aainexact = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+112, aarnd)
								if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0 {
									goto ppnext_n
								}
								if !(Xmpfr_equal_p(cgtls, cgbp+48, cgbp+32) != 0) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "tgeneric: results differ for mpfr_pow on\n\x00", 0)
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp+112)
									Xprintf(cgtls, "prec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
									Xprintf(cgtls, "Got      \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xprintf(cgtls, "Expected \x00", 0)
									Xmpfr_dump(cgtls, cgbp+48)
									Xprintf(cgtls, "Approx   \x00", 0)
									Xmpfr_dump(cgtls, cgbp+16)
									Xexit(cgtls, ppint32(1))
								}
								aacompare2 = Xmpfr_cmp3(cgtls, cgbp+48, cgbp+16, ppint32(1))
								/* if rounding to nearest, cannot know the sign of t - f(x)
								   because of composed rounding: y = o(f(x)) and t = o(y) */
								if aacompare*aacompare2 >= 0 {
									aacompare = aacompare + aacompare2
								} else {
									aacompare = aainexact
								} /* cannot determine sign(t-f(x)) */
								if !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aacompare > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aacompare < iqlibc.ppInt32FromInt32(0))) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+136, Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare, aainexact))
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp+112)
									Xprintf(cgtls, "y = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+16)
									Xprintf(cgtls, "t = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+48)
									Xexit(cgtls, ppint32(1))
								}
							} else {
								if Xgetenv(cgtls, "MPFR_SUSPICIOUS_OVERFLOW\x00") != iqlibc.ppUintptrFromInt32(0) {

									/* For developers only! */

									if ccv53 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp <= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)); ccv53 {
									}
									if ccv54 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv53 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0))), ppint32(1)) != 0; !ccv54 {
										Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(803), "(!(((y)->_mpfr_exp) <= (((-0x7fffffffL-1))+3)) && ((! __builtin_constant_p (!!(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((32 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((32 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (32 - 0))]) & ((((mp_limb_t) 1)) << ((32 - 0) - 1)))) != 0)) || !(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((32 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((32 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (32 - 0))]) & ((((mp_limb_t) 1)) << ((32 - 0) - 1)))) != 0))) || (((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((32 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((32 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (32 - 0))]) & ((((mp_limb_t) 1)) << ((32 - 0) - 1)))) != 0)) ? (void) 0 : __builtin_unreachable()), 1))\x00")
									}
									pp_ = ccv54 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
									Xmpfr_nexttoinf(cgtls, cgbp+16)

									if ccv56 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3); ccv56 {
										if ccv55 = aarnd == ppint32(ecMPFR_RNDZ); !ccv55 {
										}
									}
									if ccv56 && (ccv55 || aarnd+iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)) && !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) && iqlibc.Bool(ppint32(mvTGENERIC_SO_TEST) != 0) {

										Xprintf(cgtls, "Possible bug! |y| is the maximum finite number (with yprec = %u) and has\nbeen obtained when rounding toward zero (%s). Thus there is a very\nprobable overflow, but the overflow flag is not set!\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
										Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp)
										Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint32FromInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+112)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp+112)
										Xexit(cgtls, ppint32(1))
									}
								}
							}
						}
					}
				}
			}

			goto ppnext_n
		ppnext_n:
			;
			/* In case the exponent range has been changed by
			   tests_default_random() or for special values... */
			Xset_emin(cgtls, aaold_emin)
			Xset_emax(cgtls, aaold_emax)

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	if Xgetenv(cgtls, "MPFR_TGENERIC_STAT\x00") != iqlibc.ppUintptrFromInt32(0) {
		Xprintf(cgtls, "tgeneric: normal cases / total = %lu / %lu\n\x00", iqlibc.ppVaList(cgbp+136, aactrn, aactrt))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+16, cgbp+80, cgbp+96, cgbp+32, cgbp+48, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_clear(cgtls, cgbp+112)
}

/* Generic test file for functions with one mpfr_t argument and an integer.

Copyright 2005-2023 Free Software Foundation, Inc.
Contributed by the AriC and Caramba projects, INRIA.

This file is part of the GNU MPFR Library.

The GNU MPFR Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The GNU MPFR Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

/* define INTEGER_TYPE to what we want */

func sitest_generic_ui(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aaN ppuint32) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aa_p tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, ccv4, ccv6, ccv7, ccv8 ppint32
	var aan, aau, ccv5 ppuint32
	var aaprec, aayprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var pp_ /* t at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aacompare, aacompare2, aainexact, aan, aaprec, aarnd, aau, aayprec, ccv4, ccv5, ccv6, ccv7, ccv8

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+16)
	Xmpfr_init(cgtls, cgbp+32)
	Xmpfr_init(cgtls, cgbp+48)

	/* generic test */
	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+32, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+48, aaprec)
		aayprec = aaprec + ppint32(10)

		aan = ppuint32(0)
		for {
			if !(aan <= aaN) {
				break
			}

			if aan > ppuint32(1) || aaprec < aap1 {
				if !(Xmpfr_rands_initialized != 0) {
					Xmpfr_rands_initialized = ppint8(1)
					X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
					pp_ = iqlibc.ppInt32FromInt32(0)
				}
				Xmpfr_random2(cgtls, cgbp, ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec-iqlibc.ppInt32FromInt32(1))/ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt32FromInt32(1), ppint32(1), ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			} else {

				/* Special cases tested in precision p1 if n <= 1. */
				if aan == ppuint32(0) {
					ccv4 = ppint32(1)
				} else {
					ccv4 = -ppint32(1)
				}
				pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(ccv4), 0, ppint32(ecMPFR_RNDN))
				Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)
			}
			if aan < ppuint32(2) || aan > ppuint32(3) || aaprec < aap1 {
				if Xrandlimb(cgtls)%ppuint32(16) == ppuint32(0) {
					ccv5 = Xrandulong(cgtls)
				} else {
					ccv5 = Xrandlimb(cgtls) % iqlibc.ppUint32FromInt32(32)
				}
				aau = ccv5
			} else {

				/* Special cases tested in precision p1 if n = 2 or 3. */
				if iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1)) < ppuint32(0) { /* signed, type long assumed */
					if aan == ppuint32(2) {
						ccv6 = -iqlibc.ppInt32FromInt32(0x7fffffff) - iqlibc.ppInt32FromInt32(1)
					} else {
						ccv6 = ppint32(0x7fffffff)
					}
					aau = iqlibc.ppUint32FromInt32(ccv6)
				} else { /* unsigned */
					if aan == ppuint32(2) {
						ccv7 = 0
					} else {
						ccv7 = -ppint32(1)
					}
					aau = iqlibc.ppUint32FromInt32(ccv7)
				}
			}
			aarnd = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % ppuint32(ecMPFR_RNDF))
			Xmpfr_set_prec(cgtls, cgbp+16, aayprec)
			aacompare = Xmpfr_pow_ui(cgtls, cgbp+16, cgbp, aau, aarnd)
			if Xmpfr_can_round(cgtls, cgbp+16, aayprec, aarnd, aarnd, aaprec) != 0 {

				{
					aa_p = cgbp + 16
					ccv8 = Xmpfr_set4(cgtls, cgbp+48, aa_p, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
				}
				pp_ = ccv8
				aainexact = Xmpfr_pow_ui(cgtls, cgbp+32, cgbp, aau, aarnd)
				if Xmpfr_cmp3(cgtls, cgbp+48, cgbp+32, ppint32(1)) != 0 {

					Xprintf(cgtls, "results differ for x=\x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), iqlibc.ppUint32FromInt32(aaprec), cgbp, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\nu=%lu\x00", iqlibc.ppVaList(cgbp+72, aau))
					Xprintf(cgtls, " prec=%lu rnd_mode=%s\n\x00", iqlibc.ppVaList(cgbp+72, iqlibc.ppUint32FromInt32(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "got      \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "expected \x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xprintf(cgtls, "approx   \x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xexit(cgtls, ppint32(1))
				}
				aacompare2 = Xmpfr_cmp3(cgtls, cgbp+48, cgbp+16, ppint32(1))
				/* if rounding to nearest, cannot know the sign of t - f(x)
				   because of composed rounding: y = o(f(x)) and t = o(y) */
				if aacompare*aacompare2 >= 0 {
					aacompare = aacompare + aacompare2
				} else {
					aacompare = aainexact
				} /* cannot determine sign(t-f(x)) */
				if !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aacompare > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aacompare < iqlibc.ppInt32FromInt32(0))) && aarnd != ppint32(ecMPFR_RNDF) {

					Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+72, Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare, aainexact))
					Xprintf(cgtls, "x = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+72, aau))
					Xprintf(cgtls, "y = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xprintf(cgtls, "t = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+48)
}

/* Generic test file for functions with one mpfr_t argument and an integer.

Copyright 2005-2023 Free Software Foundation, Inc.
Contributed by the AriC and Caramba projects, INRIA.

This file is part of the GNU MPFR Library.

The GNU MPFR Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The GNU MPFR Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

/* define INTEGER_TYPE to what we want */

func sitest_generic_si(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aaN ppuint32) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aa_p tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, aau, ccv4, ccv5, ccv6, ccv7, ccv8 ppint32
	var aan ppuint32
	var aaprec, aayprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var pp_ /* t at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aacompare, aacompare2, aainexact, aan, aaprec, aarnd, aau, aayprec, ccv4, ccv5, ccv6, ccv7, ccv8

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+16)
	Xmpfr_init(cgtls, cgbp+32)
	Xmpfr_init(cgtls, cgbp+48)

	/* generic test */
	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+32, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+48, aaprec)
		aayprec = aaprec + ppint32(10)

		aan = ppuint32(0)
		for {
			if !(aan <= aaN) {
				break
			}

			if aan > ppuint32(1) || aaprec < aap1 {
				if !(Xmpfr_rands_initialized != 0) {
					Xmpfr_rands_initialized = ppint8(1)
					X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
					pp_ = iqlibc.ppInt32FromInt32(0)
				}
				Xmpfr_random2(cgtls, cgbp, ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec-iqlibc.ppInt32FromInt32(1))/ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt32FromInt32(1), ppint32(1), ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			} else {

				/* Special cases tested in precision p1 if n <= 1. */
				if aan == ppuint32(0) {
					ccv4 = ppint32(1)
				} else {
					ccv4 = -ppint32(1)
				}
				pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(ccv4), 0, ppint32(ecMPFR_RNDN))
				Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)
			}
			if aan < ppuint32(2) || aan > ppuint32(3) || aaprec < aap1 {
				if Xrandlimb(cgtls)%ppuint32(16) == ppuint32(0) {
					ccv5 = Xrandlong(cgtls)
				} else {
					ccv5 = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls)%iqlibc.ppUint32FromInt32(31)) - ppint32(15)
				}
				aau = ccv5
			} else {

				/* Special cases tested in precision p1 if n = 2 or 3. */
				if ppint32(-iqlibc.ppInt32FromInt32(1)) < 0 { /* signed, type long assumed */
					if aan == ppuint32(2) {
						ccv6 = -iqlibc.ppInt32FromInt32(0x7fffffff) - iqlibc.ppInt32FromInt32(1)
					} else {
						ccv6 = ppint32(0x7fffffff)
					}
					aau = ccv6
				} else { /* unsigned */
					if aan == ppuint32(2) {
						ccv7 = 0
					} else {
						ccv7 = -ppint32(1)
					}
					aau = ppint32(ccv7)
				}
			}
			aarnd = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % ppuint32(ecMPFR_RNDF))
			Xmpfr_set_prec(cgtls, cgbp+16, aayprec)
			aacompare = Xmpfr_pow_si(cgtls, cgbp+16, cgbp, aau, aarnd)
			if Xmpfr_can_round(cgtls, cgbp+16, aayprec, aarnd, aarnd, aaprec) != 0 {

				{
					aa_p = cgbp + 16
					ccv8 = Xmpfr_set4(cgtls, cgbp+48, aa_p, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
				}
				pp_ = ccv8
				aainexact = Xmpfr_pow_si(cgtls, cgbp+32, cgbp, aau, aarnd)
				if Xmpfr_cmp3(cgtls, cgbp+48, cgbp+32, ppint32(1)) != 0 {

					Xprintf(cgtls, "results differ for x=\x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), iqlibc.ppUint32FromInt32(aaprec), cgbp, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\nu=%lu\x00", iqlibc.ppVaList(cgbp+72, iqlibc.ppUint32FromInt32(aau)))
					Xprintf(cgtls, " prec=%lu rnd_mode=%s\n\x00", iqlibc.ppVaList(cgbp+72, iqlibc.ppUint32FromInt32(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "got      \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "expected \x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xprintf(cgtls, "approx   \x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xexit(cgtls, ppint32(1))
				}
				aacompare2 = Xmpfr_cmp3(cgtls, cgbp+48, cgbp+16, ppint32(1))
				/* if rounding to nearest, cannot know the sign of t - f(x)
				   because of composed rounding: y = o(f(x)) and t = o(y) */
				if aacompare*aacompare2 >= 0 {
					aacompare = aacompare + aacompare2
				} else {
					aacompare = aainexact
				} /* cannot determine sign(t-f(x)) */
				if !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aacompare > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aacompare < iqlibc.ppInt32FromInt32(0))) && aarnd != ppint32(ecMPFR_RNDF) {

					Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+72, Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare, aainexact))
					Xprintf(cgtls, "x = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+72, iqlibc.ppUint32FromInt32(aau)))
					Xprintf(cgtls, "y = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xprintf(cgtls, "t = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+48)
}

func sipowu2(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_ui(cgtls, aay, aax, ppuint32(2), aarnd)
}

func sipows2(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(2), aarnd)
}

func sipowm2(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(-ppint32(2)), aarnd)
}

func siroot2(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {

	var ccv1 ppint32
	pp_ = ccv1
	if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
		ccv1 = Xmpfr_root(cgtls, aay, aax, ppuint32(2), aarnd)
	} else {
		ccv1 = Xmpfr_rootn_ui(cgtls, aay, aax, ppuint32(2), aarnd)
	}
	return ccv1
}

func sirootm2(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_rootn_si(cgtls, aay, aax, ppint32(-ppint32(2)), aarnd)
}

func sipowu3(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_ui(cgtls, aay, aax, ppuint32(3), aarnd)
}

func sipows3(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(3), aarnd)
}

func sipowm3(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(-ppint32(3)), aarnd)
}

func siroot3(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {

	var ccv1 ppint32
	pp_ = ccv1
	if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
		ccv1 = Xmpfr_root(cgtls, aay, aax, ppuint32(3), aarnd)
	} else {
		ccv1 = Xmpfr_rootn_ui(cgtls, aay, aax, ppuint32(3), aarnd)
	}
	return ccv1
}

func sirootm3(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_rootn_si(cgtls, aay, aax, ppint32(-ppint32(3)), aarnd)
}

func sipowu4(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_ui(cgtls, aay, aax, ppuint32(4), aarnd)
}

func sipows4(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(4), aarnd)
}

func sipowm4(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(-ppint32(4)), aarnd)
}

func siroot4(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {

	var ccv1 ppint32
	pp_ = ccv1
	if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
		ccv1 = Xmpfr_root(cgtls, aay, aax, ppuint32(4), aarnd)
	} else {
		ccv1 = Xmpfr_rootn_ui(cgtls, aay, aax, ppuint32(4), aarnd)
	}
	return ccv1
}

func sirootm4(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_rootn_si(cgtls, aay, aax, ppint32(-ppint32(4)), aarnd)
}

func sipowu5(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_ui(cgtls, aay, aax, ppuint32(5), aarnd)
}

func sipows5(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(5), aarnd)
}

func sipowm5(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(-ppint32(5)), aarnd)
}

func siroot5(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {

	var ccv1 ppint32
	pp_ = ccv1
	if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
		ccv1 = Xmpfr_root(cgtls, aay, aax, ppuint32(5), aarnd)
	} else {
		ccv1 = Xmpfr_rootn_ui(cgtls, aay, aax, ppuint32(5), aarnd)
	}
	return ccv1
}

func sirootm5(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_rootn_si(cgtls, aay, aax, ppint32(-ppint32(5)), aarnd)
}

func sipowu17(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_ui(cgtls, aay, aax, ppuint32(17), aarnd)
}

func sipows17(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(17), aarnd)
}

func sipowm17(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(-ppint32(17)), aarnd)
}

func siroot17(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {

	var ccv1 ppint32
	pp_ = ccv1
	if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
		ccv1 = Xmpfr_root(cgtls, aay, aax, ppuint32(17), aarnd)
	} else {
		ccv1 = Xmpfr_rootn_ui(cgtls, aay, aax, ppuint32(17), aarnd)
	}
	return ccv1
}

func sirootm17(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_rootn_si(cgtls, aay, aax, ppint32(-ppint32(17)), aarnd)
}

func sipowu120(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_ui(cgtls, aay, aax, ppuint32(120), aarnd)
}

func sipows120(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(120), aarnd)
}

func sipowm120(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_pow_si(cgtls, aay, aax, ppint32(-ppint32(120)), aarnd)
}

func siroot120(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {

	var ccv1 ppint32
	pp_ = ccv1
	if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
		ccv1 = Xmpfr_root(cgtls, aay, aax, ppuint32(120), aarnd)
	} else {
		ccv1 = Xmpfr_rootn_ui(cgtls, aay, aax, ppuint32(120), aarnd)
	}
	return ccv1
}

func sirootm120(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aarnd tnmpfr_rnd_t) (cgr ppint32) {
	return Xmpfr_rootn_si(cgtls, aay, aax, ppint32(-ppint32(120)), aarnd)
}

func sicheck_pow_ui(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aan ppuint32
	var aares ppint32
	var ccv1, ccv2, ccv3, ccv4, ccv5, ccv6 ppbool
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+16 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aan, aares, ccv1, ccv2, ccv3, ccv4, ccv5, ccv6

	Xmpfr_init2(cgtls, cgbp, ppint32(53))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(53))

	/* check in-place operations */
	Xmpfr_set_str(cgtls, cgbp+16, "0.6926773\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_pow_ui(cgtls, cgbp, cgbp+16, ppuint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_pow_ui(cgtls, cgbp+16, cgbp+16, ppuint32(10), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for mpfr_pow_ui (b, b, ...)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* check large exponents */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_pow_ui(cgtls, cgbp, cgbp+16, ppuint32(4294967295), ppint32(ecMPFR_RNDN))

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_pow_ui(cgtls, cgbp, cgbp, ppuint32(4049053855), ppint32(ecMPFR_RNDN))

	if ccv3 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv3 {
		if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv1 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(121), "! (((a)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(121), "! (((a)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv3 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign >= 0 {

		Xprintf(cgtls, "Error for (-Inf)^4049053855\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_pow_ui(cgtls, cgbp, cgbp, iqlibc.ppUint32FromInt32(30002752), ppint32(ecMPFR_RNDN))

	if ccv6 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv6 {
		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(129), "! (((a)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(129), "! (((a)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv6 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign <= 0 {

		Xprintf(cgtls, "Error for (-Inf)^30002752\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* Check underflow */
	Xmpfr_set_str_binary(cgtls, cgbp, "1E-1\x00")
	aares = Xmpfr_pow_ui(cgtls, cgbp, cgbp, iqlibc.ppUint32FromInt32(-X__gmpfr_emin), ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp != X__gmpfr_emin+ppint32(1) {

		Xprintf(cgtls, "Error for (1e-1)^MPFR_EMAX_MAX\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str_binary(cgtls, cgbp, "1E-10\x00")
	aares = Xmpfr_pow_ui(cgtls, cgbp, cgbp, iqlibc.ppUint32FromInt32(-X__gmpfr_emin), ppint32(ecMPFR_RNDZ))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {

		Xprintf(cgtls, "Error for (1e-10)^MPFR_EMAX_MAX\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	/* Check overflow */
	Xmpfr_set_str_binary(cgtls, cgbp, "1E10\x00")
	aares = Xmpfr_pow_ui(cgtls, cgbp, cgbp, iqlibc.ppUint32FromUint32(2)*iqlibc.ppUint32FromInt32(0x7fffffff)+iqlibc.ppUint32FromInt32(1), ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error for (1e10)^ULONG_MAX\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* Bug in pow_ui.c from r3214 to r5107: if x = y (same mpfr_t argument),
	   the input argument is negative, n is odd, an overflow or underflow
	   occurs, and the temporary result res is positive, then the result
	   gets a wrong sign (positive instead of negative). */
	Xmpfr_set_str_binary(cgtls, cgbp, "-1E10\x00")
	aan = iqlibc.ppUint32FromUint32(2)*iqlibc.ppUint32FromInt32(0x7fffffff) + iqlibc.ppUint32FromInt32(1) ^ (iqlibc.ppUint32FromUint32(2)*iqlibc.ppUint32FromInt32(0x7fffffff)+iqlibc.ppUint32FromInt32(1))>>iqlibc.ppInt32FromInt32(1) + iqlibc.ppUint32FromInt32(1)
	aares = Xmpfr_pow_ui(cgtls, cgbp, cgbp, aan, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 {

		Xprintf(cgtls, "Error for (-1e10)^%lu, expected -Inf,\ngot \x00", iqlibc.ppVaList(cgbp+40, aan))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	/* Check 0 */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt32FromInt32(0x7fffffff) - iqlibc.ppInt32FromInt32(1) + iqlibc.ppInt32FromInt32(1)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	aares = Xmpfr_pow_ui(cgtls, cgbp+16, cgbp, ppuint32(1), ppint32(ecMPFR_RNDN))
	if aares != 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error for (0+)^1\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt32FromInt32(0x7fffffff) - iqlibc.ppInt32FromInt32(1) + iqlibc.ppInt32FromInt32(1)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = -iqlibc.ppInt32FromInt32(1)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	aares = Xmpfr_pow_ui(cgtls, cgbp+16, cgbp, ppuint32(5), ppint32(ecMPFR_RNDN))
	if aares != 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0 {

		Xprintf(cgtls, "Error for (0-)^5\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt32FromInt32(0x7fffffff) - iqlibc.ppInt32FromInt32(1) + iqlibc.ppInt32FromInt32(1)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = -iqlibc.ppInt32FromInt32(1)
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	aares = Xmpfr_pow_ui(cgtls, cgbp+16, cgbp, ppuint32(6), ppint32(ecMPFR_RNDN))
	if aares != 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error for (0-)^6\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(122))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(122))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10000010010000111101001110100101101010011110011100001111000001001101000110011001001001001011001011010110110110101000111011E1\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+16, "0.11111111100101001001000001000001100011100000001110111111100011111000111011100111111111110100011000111011000100100011001011E51290375\x00")
	Xmpfr_pow_ui(cgtls, cgbp, cgbp, ppuint32(2026876995), ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for x^2026876995\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(29))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(29))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.0000000000000000000000001111\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+16, "1.1001101111001100111001010111e165\x00")
	Xmpfr_pow_ui(cgtls, cgbp, cgbp, ppuint32(2055225053), ppint32(ecMPFR_RNDZ))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for x^2055225053\n\x00", 0)
		Xprintf(cgtls, "Expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\nGot      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* worst case found by Vincent Lefevre, 25 Nov 2006 */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(53))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(53))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.0000010110000100001000101101101001011101101011010111\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+16, "1.0000110111101111011010110100001100010000001010110100E1\x00")
	Xmpfr_pow_ui(cgtls, cgbp, cgbp, ppuint32(35), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_pow_ui for worst case (1)\n\x00", 0)
		Xprintf(cgtls, "Expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\nGot      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	/* worst cases found on 2006-11-26 */
	Xmpfr_set_str_binary(cgtls, cgbp, "1.0110100111010001101001010111001110010100111111000011\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+16, "1.1111010011101110001111010110000101110000110110101100E17\x00")
	Xmpfr_pow_ui(cgtls, cgbp, cgbp, ppuint32(36), ppint32(ecMPFR_RNDD))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_pow_ui for worst case (2)\n\x00", 0)
		Xprintf(cgtls, "Expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\nGot      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str_binary(cgtls, cgbp, "1.1001010100001110000110111111100011011101110011000100\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+16, "1.1100011101101101100010110001000001110001111110010001E23\x00")
	Xmpfr_pow_ui(cgtls, cgbp, cgbp, ppuint32(36), ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_pow_ui for worst case (3)\n\x00", 0)
		Xprintf(cgtls, "Expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\nGot      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
}

func sicheck_pow_si(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aa_p, aa_p1, aa_p2 tnmpfr_ptr
	var ccv1, ccv10, ccv11, ccv12, ccv13, ccv14, ccv16, ccv17, ccv18, ccv2, ccv20, ccv21, ccv22, ccv23, ccv24, ccv26, ccv27, ccv28, ccv3, ccv30, ccv31, ccv32, ccv33, ccv34, ccv35, ccv37, ccv38, ccv4, ccv40, ccv41, ccv43, ccv44, ccv46, ccv6, ccv7, ccv8 ppbool
	var ccv15, ccv25, ccv5 ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aa_p2, ccv1, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv17, ccv18, ccv2, ccv20, ccv21, ccv22, ccv23, ccv24, ccv25, ccv26, ccv27, ccv28, ccv3, ccv30, ccv31, ccv32, ccv33, ccv34, ccv35, ccv37, ccv38, ccv4, ccv40, ccv41, ccv43, ccv44, ccv46, ccv5, ccv6, ccv7, ccv8

	Xmpfr_init(cgtls, cgbp)

	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_pow_si(cgtls, cgbp, cgbp, ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(290), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (1 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	Xmpfr_pow_si(cgtls, cgbp, cgbp, ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(294), "(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)) && (((x)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_pow_si(cgtls, cgbp, cgbp, ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN))

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0)), ppint32(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(298), "(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)) && (((x)->_mpfr_sign) < 0)\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_pow_si(cgtls, cgbp, cgbp, ppint32(-ppint32(2)), ppint32(ecMPFR_RNDN))

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(302), "(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)) && (((x)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv5 = 0
	}
	pp_ = ccv5
	Xmpfr_pow_si(cgtls, cgbp, cgbp, ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN))

	if ccv8 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv8 {
		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(306), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv7 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(306), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv8 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv14 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(306), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 306, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tpow.c\", 306, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0\x00")
		if ccv13 = iqlibc.Bool(!(0 != 0)); !ccv13 {
			if ccv12 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv12 {
				if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv10 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(306), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv11 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(306), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv13 || ccv12 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	{
		aa_p1 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv15 = 0
	}
	pp_ = ccv15
	Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_pow_si(cgtls, cgbp, cgbp, ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN))

	if ccv18 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv18 {
		if ccv16 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv16 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(311), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv16 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv17 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv17 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(311), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv17 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv24 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv18 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0)), ppint32(1)) != 0; !ccv24 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(311), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 311, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tpow.c\", 311, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) < 0\x00")
		if ccv23 = iqlibc.Bool(!(0 != 0)); !ccv23 {
			if ccv22 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv22 {
				if ccv20 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv20 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(311), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv20 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv21 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv21 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(311), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv21 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv23 || ccv22 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv24 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	{
		aa_p2 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv25 = 0
	}
	pp_ = ccv25
	Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_pow_si(cgtls, cgbp, cgbp, ppint32(-ppint32(2)), ppint32(ecMPFR_RNDN))

	if ccv28 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv28 {
		if ccv26 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv26 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(316), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv26 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv27 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv27 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(316), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv27 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv34 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv28 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv34 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(316), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 316, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tpow.c\", 316, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0\x00")
		if ccv33 = iqlibc.Bool(!(0 != 0)); !ccv33 {
			if ccv32 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv32 {
				if ccv30 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv30 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(316), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv30 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv31 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv31 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(316), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv31 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv33 || ccv32 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv34 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(ppint32(iqlibc.ppInt32FromInt32(2))), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_pow_si(cgtls, cgbp, cgbp, ppint32(0x7fffffff), ppint32(ecMPFR_RNDN)) /* 2^LONG_MAX */
	if ppint32(0x7fffffff) > X__gmpfr_emax-ppint32(1) {                        /* LONG_MAX + 1 > emax */

		if ccv35 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint32(1)) != 0; !ccv35 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(322), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
		}
		pp_ = ccv35 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	} else {

		if ccv37 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint32(1), iqlibc.ppInt32FromInt32(0x7fffffff)) != 0)), ppint32(1)) != 0; !ccv37 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(326), "mpfr_cmp_si_2exp (x, 1, (mpfr_exp_t) 0x7fffffffL)\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint32(1), iqlibc.ppInt32FromInt32(0x7fffffff)) != 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv37 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(ppint32(iqlibc.ppInt32FromInt32(2))), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_pow_si(cgtls, cgbp, cgbp, -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1), ppint32(ecMPFR_RNDN)) /* 2^LONG_MIN */
	if -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) < X__gmpfr_emin {

		if ccv38 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint32(1)) != 0; !ccv38 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(333), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
		}
		pp_ = ccv38 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	} else {

		if ccv40 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint32(1), -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)) != 0)), ppint32(1)) != 0; !ccv40 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(337), "mpfr_cmp_si_2exp (x, 1, (mpfr_exp_t) (-0x7fffffffL-1))\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint32(1), -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)) != 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv40 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(ppint32(iqlibc.ppInt32FromInt32(2))), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_pow_si(cgtls, cgbp, cgbp, -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1), ppint32(ecMPFR_RNDN)) /* 2^(LONG_MIN+1) */
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {

		Xprintf(cgtls, "Error in pow_si(2, LONG_MIN+1): got NaN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) < X__gmpfr_emin {

		if ccv41 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint32(1)) != 0; !ccv41 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(349), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
		}
		pp_ = ccv41 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	} else {

		if ccv43 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint32(1), -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1)) != 0)), ppint32(1)) != 0; !ccv43 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(353), "mpfr_cmp_si_2exp (x, 1, (mpfr_exp_t) ((-0x7fffffffL-1) + 1))\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint32(1), -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1)) != 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv43 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}

	Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(1), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN))                                 /* 0.5 */
	Xmpfr_pow_si(cgtls, cgbp, cgbp, -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1), ppint32(ecMPFR_RNDN)) /* 2^(-LONG_MIN) */
	if -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1) < ppint32(1)-X__gmpfr_emax {                        /* 1 - LONG_MIN > emax */

		if ccv44 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint32(1)) != 0; !ccv44 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(360), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
		}
		pp_ = ccv44 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	} else {

		if ccv46 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint32(2), -(-iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))) != 0)), ppint32(1)) != 0; !ccv46 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(364), "mpfr_cmp_si_2exp (x, 2, (mpfr_exp_t) - ((-0x7fffffffL-1) + 1))\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint32(2), -(-iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))) != 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv46 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}

	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* check the IEEE 754-2019 special rules for pown */
func sicheck_pown_ieee754_2019(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var ccv1, ccv10, ccv100, ccv101, ccv102, ccv103, ccv104, ccv105, ccv106, ccv11, ccv13, ccv14, ccv15, ccv16, ccv18, ccv19, ccv20, ccv21, ccv23, ccv24, ccv25, ccv26, ccv28, ccv29, ccv3, ccv30, ccv31, ccv33, ccv34, ccv35, ccv36, ccv37, ccv38, ccv4, ccv40, ccv41, ccv42, ccv43, ccv44, ccv45, ccv46, ccv47, ccv49, ccv5, ccv50, ccv51, ccv52, ccv53, ccv54, ccv55, ccv56, ccv58, ccv59, ccv6, ccv60, ccv61, ccv62, ccv63, ccv64, ccv65, ccv67, ccv68, ccv69, ccv70, ccv71, ccv72, ccv73, ccv74, ccv75, ccv76, ccv77, ccv78, ccv8, ccv80, ccv81, ccv82, ccv83, ccv84, ccv85, ccv86, ccv87, ccv89, ccv9, ccv90, ccv91, ccv92, ccv93, ccv94, ccv95, ccv96, ccv98, ccv99 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = ccv1, ccv10, ccv100, ccv101, ccv102, ccv103, ccv104, ccv105, ccv106, ccv11, ccv13, ccv14, ccv15, ccv16, ccv18, ccv19, ccv20, ccv21, ccv23, ccv24, ccv25, ccv26, ccv28, ccv29, ccv3, ccv30, ccv31, ccv33, ccv34, ccv35, ccv36, ccv37, ccv38, ccv4, ccv40, ccv41, ccv42, ccv43, ccv44, ccv45, ccv46, ccv47, ccv49, ccv5, ccv50, ccv51, ccv52, ccv53, ccv54, ccv55, ccv56, ccv58, ccv59, ccv6, ccv60, ccv61, ccv62, ccv63, ccv64, ccv65, ccv67, ccv68, ccv69, ccv70, ccv71, ccv72, ccv73, ccv74, ccv75, ccv76, ccv77, ccv78, ccv8, ccv80, ccv81, ccv82, ccv83, ccv84, ccv85, ccv86, ccv87, ccv89, ccv9, ccv90, ccv91, ccv92, ccv93, ccv94, ccv95, ccv96, ccv98, ccv99

	Xmpfr_init2(cgtls, cgbp, ppint32(5)) /* ensures 17 is exact */

	/* pown (x, 0) is 1 if x is not a signaling NaN: in MPFR we decide to
	   return 1 for a NaN */
	Xmpfr_set_nan(cgtls, cgbp)
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, 0, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(383), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(383), "(((void) (((__builtin_expect(!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 383, \"!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), (__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (x) : mpfr_cmp_ui_2exp ((x), (1), 0))) == 0\x00")
		if ccv4 = iqlibc.Bool(!(0 != 0)); !ccv4 {
			if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv3 {
				Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(383), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
			}
			pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv4 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, 0, ppint32(ecMPFR_RNDN))

	if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv6 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(386), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv10 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(386), "(((void) (((__builtin_expect(!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 386, \"!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), (__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (x) : mpfr_cmp_ui_2exp ((x), (1), 0))) == 0\x00")
		if ccv9 = iqlibc.Bool(!(0 != 0)); !ccv9 {
			if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv8 {
				Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(386), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
			}
			pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv9 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, 0, ppint32(ecMPFR_RNDN))

	if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv11 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(389), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv15 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv15 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(389), "(((void) (((__builtin_expect(!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 389, \"!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), (__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (x) : mpfr_cmp_ui_2exp ((x), (1), 0))) == 0\x00")
		if ccv14 = iqlibc.Bool(!(0 != 0)); !ccv14 {
			if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv13 {
				Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(389), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
			}
			pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv14 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv15 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_zero(cgtls, cgbp, ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, 0, ppint32(ecMPFR_RNDN))

	if ccv16 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv16 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(392), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv16 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv20 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv20 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(392), "(((void) (((__builtin_expect(!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 392, \"!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), (__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (x) : mpfr_cmp_ui_2exp ((x), (1), 0))) == 0\x00")
		if ccv19 = iqlibc.Bool(!(0 != 0)); !ccv19 {
			if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv18 {
				Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(392), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
			}
			pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv19 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv20 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_zero(cgtls, cgbp, -ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, 0, ppint32(ecMPFR_RNDN))

	if ccv21 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv21 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(395), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv21 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv25 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv25 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(395), "(((void) (((__builtin_expect(!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 395, \"!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), (__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (x) : mpfr_cmp_ui_2exp ((x), (1), 0))) == 0\x00")
		if ccv24 = iqlibc.Bool(!(0 != 0)); !ccv24 {
			if ccv23 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv23 {
				Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(395), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
			}
			pp_ = ccv23 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv24 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv25 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(ppint32(iqlibc.ppInt32FromInt32(17))), 0, ppint32(ecMPFR_RNDN))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, 0, ppint32(ecMPFR_RNDN))

	if ccv26 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv26 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(398), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv26 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv30 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv30 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(398), "(((void) (((__builtin_expect(!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 398, \"!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), (__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (x) : mpfr_cmp_ui_2exp ((x), (1), 0))) == 0\x00")
		if ccv29 = iqlibc.Bool(!(0 != 0)); !ccv29 {
			if ccv28 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv28 {
				Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(398), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
			}
			pp_ = ccv28 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv29 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv30 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(-iqlibc.ppInt32FromInt32(17)), 0, ppint32(ecMPFR_RNDN))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, 0, ppint32(ecMPFR_RNDN))

	if ccv31 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv31 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(401), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv31 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv35 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv35 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(401), "(((void) (((__builtin_expect(!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 401, \"!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), (__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (x) : mpfr_cmp_ui_2exp ((x), (1), 0))) == 0\x00")
		if ccv34 = iqlibc.Bool(!(0 != 0)); !ccv34 {
			if ccv33 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv33 {
				Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(401), "!(((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
			}
			pp_ = ccv33 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv34 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv35 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* pown (±0, n) is ±∞ and signals the divideByZero exception for odd n < 0 */
	Xmpfr_set_zero(cgtls, cgbp, ppint32(1))
	Xmpfr_clear_divby0(cgtls)
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(-ppint32(17)), ppint32(ecMPFR_RNDN))

	if ccv38 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv38 {
		if ccv36 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv36 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(407), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv36 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv37 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv37 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(407), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv37 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv44 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv38 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0)), ppint32(1)) != 0; !ccv44 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(407), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 407, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tpow.c\", 407, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0 && ((int) (__gmpfr_flags & 32))\x00")
		if ccv43 = iqlibc.Bool(!(0 != 0)); !ccv43 {
			if ccv42 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv42 {
				if ccv40 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv40 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(407), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv40 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv41 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv41 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(407), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv41 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv43 || ccv42 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv44 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_zero(cgtls, cgbp, -ppint32(1))
	Xmpfr_clear_divby0(cgtls)
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(-ppint32(17)), ppint32(ecMPFR_RNDN))

	if ccv47 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv47 {
		if ccv45 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv45 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(411), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv45 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv46 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv46 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(411), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv46 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv53 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv47 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0)), ppint32(1)) != 0; !ccv53 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(411), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 411, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tpow.c\", 411, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) < 0 && ((int) (__gmpfr_flags & 32))\x00")
		if ccv52 = iqlibc.Bool(!(0 != 0)); !ccv52 {
			if ccv51 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv51 {
				if ccv49 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv49 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(411), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv49 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv50 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv50 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(411), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv50 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv52 || ccv51 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv53 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* pown (±0, n) is +∞ and signals the divideByZero exception for even n < 0*/
	Xmpfr_set_zero(cgtls, cgbp, ppint32(1))
	Xmpfr_clear_divby0(cgtls)
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(-ppint32(42)), ppint32(ecMPFR_RNDN))

	if ccv56 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv56 {
		if ccv54 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv54 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(417), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv54 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv55 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv55 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(417), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv55 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv62 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv56 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0)), ppint32(1)) != 0; !ccv62 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(417), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 417, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tpow.c\", 417, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0 && ((int) (__gmpfr_flags & 32))\x00")
		if ccv61 = iqlibc.Bool(!(0 != 0)); !ccv61 {
			if ccv60 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv60 {
				if ccv58 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv58 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(417), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv58 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv59 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv59 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(417), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv59 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv61 || ccv60 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv62 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_zero(cgtls, cgbp, -ppint32(1))
	Xmpfr_clear_divby0(cgtls)
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(-ppint32(42)), ppint32(ecMPFR_RNDN))

	if ccv65 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv65 {
		if ccv63 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv63 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(421), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv63 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv64 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv64 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(421), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv64 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv71 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv65 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0)), ppint32(1)) != 0; !ccv71 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(421), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 421, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tpow.c\", 421, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0 && ((int) (__gmpfr_flags & 32))\x00")
		if ccv70 = iqlibc.Bool(!(0 != 0)); !ccv70 {
			if ccv69 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv69 {
				if ccv67 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv67 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(421), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv67 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv68 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv68 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(421), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv68 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv70 || ccv69 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv71 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* pown (±0, n) is +0 for even n > 0 */
	Xmpfr_set_zero(cgtls, cgbp, ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(42), ppint32(ecMPFR_RNDN))

	if ccv72 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) == 0)), ppint32(1)) != 0; !ccv72 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(426), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign))) < 0) == 0\x00")
	}
	pp_ = ccv72 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_zero(cgtls, cgbp, -ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(42), ppint32(ecMPFR_RNDN))

	if ccv73 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) == 0)), ppint32(1)) != 0; !ccv73 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(429), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign))) < 0) == 0\x00")
	}
	pp_ = ccv73 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* pown (±0, n) is ±0 for odd n > 0 */
	Xmpfr_set_zero(cgtls, cgbp, ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(17), ppint32(ecMPFR_RNDN))

	if ccv74 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) == 0)), ppint32(1)) != 0; !ccv74 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(434), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign))) < 0) == 0\x00")
	}
	pp_ = ccv74 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_zero(cgtls, cgbp, -ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(17), ppint32(ecMPFR_RNDN))

	if ccv75 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) == ppint32(1))), ppint32(1)) != 0; !ccv75 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(437), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign))) < 0) == 1\x00")
	}
	pp_ = ccv75 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* pown (+∞, n) is +∞ for n > 0 */
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(17), ppint32(ecMPFR_RNDN))

	if ccv78 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv78 {
		if ccv76 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv76 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(442), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv76 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv77 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv77 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(442), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv77 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv84 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv78 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv84 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(442), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 442, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tpow.c\", 442, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0\x00")
		if ccv83 = iqlibc.Bool(!(0 != 0)); !ccv83 {
			if ccv82 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv82 {
				if ccv80 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv80 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(442), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv80 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv81 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv81 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(442), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv81 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv83 || ccv82 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv84 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* pown (−∞, n) is −∞ for odd n > 0 */
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(17), ppint32(ecMPFR_RNDN))

	if ccv87 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv87 {
		if ccv85 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv85 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(447), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv85 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv86 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv86 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(447), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv86 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv93 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv87 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0)), ppint32(1)) != 0; !ccv93 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(447), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 447, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tpow.c\", 447, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) < 0\x00")
		if ccv92 = iqlibc.Bool(!(0 != 0)); !ccv92 {
			if ccv91 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv91 {
				if ccv89 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv89 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(447), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv89 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv90 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv90 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(447), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv90 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv92 || ccv91 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv93 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* pown (−∞, n) is +∞ for even n > 0 */
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(42), ppint32(ecMPFR_RNDN))

	if ccv96 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv96 {
		if ccv94 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv94 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(452), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv94 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv95 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv95 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(452), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv95 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv102 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv96 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv102 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(452), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 452, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tpow.c\", 452, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0\x00")
		if ccv101 = iqlibc.Bool(!(0 != 0)); !ccv101 {
			if ccv100 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv100 {
				if ccv98 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv98 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(452), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv98 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv99 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv99 {
					Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(452), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv99 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv101 || ccv100 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv102 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* pown (+∞, n) is +0 for n < 0 */
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(-ppint32(17)), ppint32(ecMPFR_RNDN))

	if ccv103 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) == 0)), ppint32(1)) != 0; !ccv103 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(457), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign))) < 0) == 0\x00")
	}
	pp_ = ccv103 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(-ppint32(42)), ppint32(ecMPFR_RNDN))

	if ccv104 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) == 0)), ppint32(1)) != 0; !ccv104 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(460), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign))) < 0) == 0\x00")
	}
	pp_ = ccv104 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* pown (−∞, n) is −0 for odd n < 0 */
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(-ppint32(17)), ppint32(ecMPFR_RNDN))

	if ccv105 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) != 0)), ppint32(1)) != 0; !ccv105 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(465), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign))) < 0) != 0\x00")
	}
	pp_ = ccv105 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* pown (−∞, n) is +0 for even n < 0 */
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	X__gmpfr_mpfr_pow_sj(cgtls, cgbp, cgbp, ppint64(-ppint32(42)), ppint32(ecMPFR_RNDN))

	if ccv106 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) == 0)), ppint32(1)) != 0; !ccv106 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(470), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x))))->_mpfr_sign))) < 0) == 0\x00")
	}
	pp_ = ccv106 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_special_pow_si(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aaemin tnmpfr_exp_t
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+16 */ tnmpfr_t
	pp_ = aaemin

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+16)
	Xmpfr_set_str(cgtls, cgbp, "2E100000000\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(-iqlibc.ppInt32FromInt32(10)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_pow(cgtls, cgbp+16, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {

		Xprintf(cgtls, "Pow(2E10000000, -10) failed\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint32(-ppint32(10)))
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(-iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_pow_si(cgtls, cgbp+16, cgbp, ppint32(-ppint32(10000)), ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {

		Xprintf(cgtls, "Pow_so (1, -10000) doesn't underflow if emin=-10.\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xset_emin(cgtls, aaemin)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
}

func sipow_si_long_min(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aainex ppint32
	var ccv1, ccv2 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_ = aainex, ccv1, ccv2

	Xmpfr_inits2(cgtls, iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt64(4)*iqlibc.ppUint32FromInt32(mvCHAR_BIT)+iqlibc.ppUint32FromInt32(32)), cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(3), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN)) /* 1.5 */

	aainex = Xmpfr_set_si_2exp(cgtls, cgbp+16, -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1), 0, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(521), "inex == 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_nextbelow(cgtls, cgbp+16)
	Xmpfr_pow(cgtls, cgbp+16, cgbp, cgbp+16, ppint32(ecMPFR_RNDD))

	aainex = Xmpfr_set_si_2exp(cgtls, cgbp+32, -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1), 0, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(526), "inex == 0\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_nextabove(cgtls, cgbp+32)
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))

	Xmpfr_pow_si(cgtls, cgbp, cgbp, -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1), ppint32(ecMPFR_RNDN)) /* 1.5^LONG_MIN */
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) < 0 || Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) > 0 {

		Xprintf(cgtls, "Error in pow_si_long_min\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
}

func sicheck_inexact(cgtls *iqlibc.ppTLS, aap tnmpfr_prec_t) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aa_p tnmpfr_srcptr
	var aacmp, aainexact, aarnd, ccv4 ppint32
	var aaq tnmpfr_prec_t
	var aau ppuint32
	var pp_ /* t at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aacmp, aainexact, aaq, aarnd, aau, ccv4

	Xmpfr_init2(cgtls, cgbp, aap)
	Xmpfr_init(cgtls, cgbp+16)
	Xmpfr_init(cgtls, cgbp+32)
	Xmpfr_init(cgtls, cgbp+48)
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	aau = iqlibc.ppBoolUint32(Xrandlimb(cgtls)&iqlibc.ppUint32FromInt32(1) != iqlibc.ppUint32FromInt32(0))
	aaq = ppint32(mvMPFR_PREC_MIN)
	for {
		if !(aaq <= aap) {
			break
		}
		aarnd = 0
		for {
			if !(aarnd < ppint32(ecMPFR_RNDF)) {
				break
			}

			Xmpfr_set_prec(cgtls, cgbp+16, aaq)
			Xmpfr_set_prec(cgtls, cgbp+32, aaq+ppint32(10))
			Xmpfr_set_prec(cgtls, cgbp+48, aaq)
			aainexact = Xmpfr_pow_ui(cgtls, cgbp+16, cgbp, aau, aarnd)
			aacmp = Xmpfr_pow_ui(cgtls, cgbp+32, cgbp, aau, aarnd)
			/* Note: that test makes no sense for RNDF, since according to the
			   reference manual, if the mpfr_can_round() call succeeds, one would
			   have to use RNDN in the mpfr_set() call below, which might give a
			   different value for t that the value y obtained above. */
			if Xmpfr_can_round(cgtls, cgbp+32, aaq+ppint32(10), aarnd, aarnd, aaq) != 0 {

				{
					aa_p = cgbp + 32
					ccv4 = Xmpfr_set4(cgtls, cgbp+48, aa_p, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
				}
				aacmp = iqlibc.ppBoolInt32(ccv4 != 0 || aacmp != 0)
				if Xmpfr_cmp3(cgtls, cgbp+16, cgbp+48, ppint32(1)) != 0 {

					Xprintf(cgtls, "results differ for u=%lu rnd=%s\n\x00", iqlibc.ppVaList(cgbp+72, aau, Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "x=\x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "y=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xprintf(cgtls, "t=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xprintf(cgtls, "z=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xexit(cgtls, ppint32(1))
				}
				if aainexact == 0 && aacmp != 0 || aainexact != 0 && aacmp == 0 {

					Xprintf(cgtls, "Wrong inexact flag for p=%u, q=%u, rnd=%s\n\x00", iqlibc.ppVaList(cgbp+72, iqlibc.ppUint32FromInt32(aap), iqlibc.ppUint32FromInt32(aaq), Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+72, aacmp, aainexact))
					Xprintf(cgtls, "u=%lu x=\x00", iqlibc.ppVaList(cgbp+72, aau))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "y=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_3
		cg_3:
			;
			aarnd++
		}
		goto cg_2
	cg_2:
		;
		aaq++
	}

	/* check exact power */
	Xmpfr_set_prec(cgtls, cgbp, aap)
	Xmpfr_set_prec(cgtls, cgbp+16, aap)
	Xmpfr_set_prec(cgtls, cgbp+32, aap)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(4)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+16, "0.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDZ))

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+48)
}

func sispecial(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aaemax, aaemin tnmpfr_exp_t
	var aainex ppint32
	var ccv1, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv17, ccv18, ccv19, ccv2, ccv20, ccv22, ccv23, ccv24, ccv25, ccv26, ccv27, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8 ppbool
	var pp_ /* t at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaemin, aainex, ccv1, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv17, ccv18, ccv19, ccv2, ccv20, ccv22, ccv23, ccv24, ccv25, ccv26, ccv27, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8

	Xmpfr_init2(cgtls, cgbp, ppint32(53))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(53))
	Xmpfr_init2(cgtls, cgbp+32, ppint32(53))
	Xmpfr_init2(cgtls, cgbp+48, ppint32(2))

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_pow_si(cgtls, cgbp, cgbp, ppint32(-ppint32(2)), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint32(1), ppint32(-ppint32(2))) != 0 {

		Xprintf(cgtls, "Error in pow_si(x,x,-2) for x=2\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(-iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_pow(cgtls, cgbp, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint32(1), ppint32(-ppint32(2))) != 0 {

		Xprintf(cgtls, "Error in pow(x,x,y) for x=2, y=-2\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(2))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.0e-1\x00")
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(53))
	Xmpfr_set_str_binary(cgtls, cgbp+16, "0.11010110011100101010110011001010100111000001000101110E-1\x00")
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(2))
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDZ))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.0e-1\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_pow (1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(64))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(64))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(64))
	Xmpfr_set_prec(cgtls, cgbp+48, ppint32(64))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.111011000111100000111010000101010100110011010000011\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+16, "0.111110010100110000011101100011010111000010000100101\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+48, "0.1110110011110110001000110100100001001111010011111000010000011001\x00")

	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp+32, cgbp+48, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_pow for prec=64, rnd=MPFR_RNDN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(53))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(53))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(53))
	Xmpfr_set_str(cgtls, cgbp, "5.68824667828621954868e-01\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+16, "9.03327850535952658895e-01\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDZ))
	if Xmpfr_cmp_str(cgtls, cgbp+32, "0.60071044650456473235\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "Error in mpfr_pow for prec=53, rnd=MPFR_RNDZ\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp+48, ppint32(2))
	Xmpfr_set_prec(cgtls, cgbp, ppint32(30))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(30))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(30))
	Xmpfr_set_str(cgtls, cgbp, "1.00000000001010111110001111011e1\x00", ppint32(2), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+48, "-0.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+48, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+16, "1.01101001111010101110000101111e-1\x00", ppint32(2), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp+32, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_pow for prec=30, rnd=MPFR_RNDN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(21))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(21))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(21))
	Xmpfr_set_str(cgtls, cgbp, "1.11111100100001100101\x00", ppint32(2), ppint32(ecMPFR_RNDN))
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+48, ppint32(ecMPFR_RNDZ))
	Xmpfr_set_str(cgtls, cgbp+16, "1.01101011010001100000e-1\x00", ppint32(2), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp+32, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_pow for prec=21, rnd=MPFR_RNDZ\n\x00", 0)
		Xprintf(cgtls, "Expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\nGot      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* From https://web.archive.org/web/20050824044408/http://www.terra.es/personal9/ismaeljc/hall.htm */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(113))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(169))
	Xmpfr_set_str(cgtls, cgbp, "6078673043126084065007902175846955\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(3), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN))
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDZ))
	if Xmpfr_cmp_str(cgtls, cgbp+32, "473928882491000966028828671876527456070714790264144\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "Error in mpfr_pow for 6078673043126084065007902175846955\x00", 0)
		Xprintf(cgtls, "^(3/2), MPFR_RNDZ\nExpected \x00", 0)
		Xprintf(cgtls, "4.73928882491000966028828671876527456070714790264144e50\x00", 0)
		Xprintf(cgtls, "\nGot      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp_str(cgtls, cgbp+32, "473928882491000966028828671876527456070714790264145\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "Error in mpfr_pow for 6078673043126084065007902175846955\x00", 0)
		Xprintf(cgtls, "^(3/2), MPFR_RNDU\nExpected \x00", 0)
		Xprintf(cgtls, "4.73928882491000966028828671876527456070714790264145e50\x00", 0)
		Xprintf(cgtls, "\nGot      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(2))
	Xmpfr_set_str_binary(cgtls, cgbp+16, "1E10\x00")
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(740), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(743), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(10))
	Xmpfr_set_str_binary(cgtls, cgbp+16, "1.000000001E9\x00")
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0)), ppint32(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(747), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) < 0)\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_str_binary(cgtls, cgbp+16, "1.000000001E8\x00")
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(750), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(ppint32(2)*X__gmp_bits_per_limb))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_mul_2ui(cgtls, cgbp+16, cgbp+16, iqlibc.ppUint32FromInt32(X__gmp_bits_per_limb-ppint32(1)), ppint32(ecMPFR_RNDN))
	/* y = 2^(mp_bits_per_limb - 1) */
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(758), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_nextabove(cgtls, cgbp+16)
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	/* y = 2^(mp_bits_per_limb - 1) + epsilon */

	if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv6 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(762), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_nextbelow(cgtls, cgbp+16)
	Xmpfr_div_2ui(cgtls, cgbp+16, cgbp+16, ppuint32(1), ppint32(ecMPFR_RNDN))
	Xmpfr_nextabove(cgtls, cgbp+16)
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	/* y = 2^(mp_bits_per_limb - 2) + epsilon */

	if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv7 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(768), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(2))
	Xmpfr_set_str_binary(cgtls, cgbp+16, "1E10\x00")
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv8 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(774), "!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv12 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(774), "(((void) (((__builtin_expect(!!(!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 774, \"!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), (__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (z) : mpfr_cmp_ui_2exp ((z), (1), 0))) == 0\x00")
		if ccv11 = iqlibc.Bool(!(0 != 0)); !ccv11 {
			if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv10 {
				Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(774), "!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
			}
			pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv11 || Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* Check (-0)^(17.0001) */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(6))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(640))
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt32FromInt32(0x7fffffff) - iqlibc.ppInt32FromInt32(1) + iqlibc.ppInt32FromInt32(1)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = -iqlibc.ppInt32FromInt32(1)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(17)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_nextabove(cgtls, cgbp+16)
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv13 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(782), "(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+1)) && (((z)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* Bugs reported by Kevin Rauch on 29 Oct 2007 */
	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	Xset_emin(cgtls, ppint32(-ppint32(1000000)))
	Xset_emax(cgtls, ppint32(1000000))
	Xmpfr_set_prec(cgtls, cgbp, ppint32(64))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(64))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(64))
	Xmpfr_set_str(cgtls, cgbp, "-0.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+16, "-0.ffffffffffffffff\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_set_exp(cgtls, cgbp+16, X__gmpfr_emax)
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	/* (-0.5)^(-n) = 1/2^n for n even */

	if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0 && aainex > 0)), ppint32(1)) != 0; !ccv14 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(797), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) > 0) && inex > 0\x00")
	}
	pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* (-1)^(-n) = 1 for n even */
	Xmpfr_set_str(cgtls, cgbp, "-1\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv15 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv15 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(802), "!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv15 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv19 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0 && aainex == 0)), ppint32(1)) != 0; !ccv19 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(802), "(((void) (((__builtin_expect(!!(!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 802, \"!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), (__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (z) : mpfr_cmp_ui_2exp ((z), (1), 0))) == 0 && inex == 0\x00")
		if ccv18 = iqlibc.Bool(!(0 != 0)); !ccv18 {
			if ccv17 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv17 {
				Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(802), "!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
			}
			pp_ = ccv17 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv18 || Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0 && aainex == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv19 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* (-1)^n = 1 for n even */
	Xmpfr_set_str(cgtls, cgbp, "-1\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_neg(cgtls, cgbp+16, cgbp+16, ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv20 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv20 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(808), "!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv20 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv24 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0 && aainex == 0)), ppint32(1)) != 0; !ccv24 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(808), "(((void) (((__builtin_expect(!!(!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tpow.c\", 808, \"!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), (__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (z) : mpfr_cmp_ui_2exp ((z), (1), 0))) == 0 && inex == 0\x00")
		if ccv23 = iqlibc.Bool(!(0 != 0)); !ccv23 {
			if ccv22 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv22 {
				Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(808), "!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
			}
			pp_ = ccv22 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv23 || Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0 && aainex == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv24 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* (-1.5)^n = +Inf for n even */
	Xmpfr_set_str(cgtls, cgbp, "-1.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv25 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0 && aainex > 0)), ppint32(1)) != 0; !ccv25 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(813), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) > 0) && inex > 0\x00")
	}
	pp_ = ccv25 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* (-n)^1.5 = NaN for n even */
	Xmpfr_neg(cgtls, cgbp+16, cgbp+16, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp, "1.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp+16, cgbp, ppint32(ecMPFR_RNDN))

	if ccv26 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint32(1)) != 0; !ccv26 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(819), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (1 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv26 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* x^(-1.5) = NaN for x small < 0 */
	Xmpfr_set_str(cgtls, cgbp, "-0.8\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)
	Xmpfr_set_str(cgtls, cgbp+16, "-1.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv27 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint32(1)) != 0; !ccv27 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(826), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (1 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv27 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+48)
}

func siparticular_cases(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(288)
	defer cgtls.ppFree(288)

	var aa_p tnmpfr_ptr
	var aad, ccv13, ccv17, ccv21, ccv25, ccv9 ppfloat64
	var aaerror, aai, aaj, aap, aasi, ccv10, ccv11, ccv12, ccv14, ccv15, ccv16, ccv18, ccv19, ccv2, ccv20, ccv22, ccv23, ccv24, ccv6, ccv7, ccv8 ppint32
	var pp_ /* r at bp+176 */ tnmpfr_t
	var pp_ /* r2 at bp+192 */ tnmpfr_t
	var pp_ /* t at bp+0 */ [11]tnmpfr_t
	var pp_ /* z at bp+208 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aad, aaerror, aai, aaj, aap, aasi, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv2, ccv20, ccv21, ccv22, ccv23, ccv24, ccv25, ccv6, ccv7, ccv8, ccv9

	var snname = [11]ppuintptr{
		0:  "NaN\x00",
		1:  "+inf\x00",
		2:  "-inf\x00",
		3:  "+0\x00",
		4:  "-0\x00",
		5:  "+1\x00",
		6:  "-1\x00",
		7:  "+2\x00",
		8:  "-2\x00",
		9:  "+0.5\x00",
		10: "-0.5\x00",
	}
	aaerror = 0

	Xmpfr_mpz_init(cgtls, cgbp+208)

	aai = 0
	for {
		if !(aai < ppint32(11)) {
			break
		}
		Xmpfr_init2(cgtls, cgbp+ppuintptr(aai)*16, ppint32(2))
		goto cg_1
	cg_1:
		;
		aai++
	}
	Xmpfr_init2(cgtls, cgbp+176, ppint32(6))
	Xmpfr_init2(cgtls, cgbp+192, ppint32(6))

	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_set_inf(cgtls, cgbp+1*16, ppint32(1))
	{
		aa_p = cgbp + 3*16
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv2 = 0
	}
	pp_ = ccv2
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+5*16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+7*16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp+9*16, cgbp+5*16, ppuint32(1), ppint32(ecMPFR_RNDN))
	aai = ppint32(1)
	for {
		if !(aai < ppint32(11)) {
			break
		}
		Xmpfr_neg(cgtls, cgbp+ppuintptr(aai+ppint32(1))*16, cgbp+ppuintptr(aai)*16, ppint32(ecMPFR_RNDN))
		goto cg_3
	cg_3:
		;
		aai += ppint32(2)
	}

	aai = 0
	for {
		if !(aai < ppint32(11)) {
			break
		}
		aaj = 0
		for {
			if !(aaj < ppint32(11)) {
				break
			}

			var snq = [11][11]ppint32{
				0: {
					3: ppint32(128),
					4: ppint32(128),
				},
				1: {
					1:  ppint32(1),
					2:  ppint32(2),
					3:  ppint32(128),
					4:  ppint32(128),
					5:  ppint32(1),
					6:  ppint32(2),
					7:  ppint32(1),
					8:  ppint32(2),
					9:  ppint32(1),
					10: ppint32(2),
				},
				2: {
					1:  ppint32(1),
					2:  ppint32(2),
					3:  ppint32(128),
					4:  ppint32(128),
					5:  -ppint32(1),
					6:  -ppint32(2),
					7:  ppint32(1),
					8:  ppint32(2),
					9:  ppint32(1),
					10: ppint32(2),
				},
				3: {
					1:  ppint32(2),
					2:  ppint32(1),
					3:  ppint32(128),
					4:  ppint32(128),
					5:  ppint32(2),
					6:  ppint32(1),
					7:  ppint32(2),
					8:  ppint32(1),
					9:  ppint32(2),
					10: ppint32(1),
				},
				4: {
					1:  ppint32(2),
					2:  ppint32(1),
					3:  ppint32(128),
					4:  ppint32(128),
					5:  -ppint32(2),
					6:  -ppint32(1),
					7:  ppint32(2),
					8:  ppint32(1),
					9:  ppint32(2),
					10: ppint32(1),
				},
				5: {
					0:  ppint32(128),
					1:  ppint32(128),
					2:  ppint32(128),
					3:  ppint32(128),
					4:  ppint32(128),
					5:  ppint32(128),
					6:  ppint32(128),
					7:  ppint32(128),
					8:  ppint32(128),
					9:  ppint32(128),
					10: ppint32(128),
				},
				6: {
					1: ppint32(128),
					2: ppint32(128),
					3: ppint32(128),
					4: ppint32(128),
					5: -ppint32(128),
					6: -ppint32(128),
					7: ppint32(128),
					8: ppint32(128),
				},
				7: {
					1:  ppint32(1),
					2:  ppint32(2),
					3:  ppint32(128),
					4:  ppint32(128),
					5:  ppint32(256),
					6:  ppint32(64),
					7:  ppint32(512),
					8:  ppint32(32),
					9:  ppint32(180),
					10: ppint32(90),
				},
				8: {
					1: ppint32(1),
					2: ppint32(2),
					3: ppint32(128),
					4: ppint32(128),
					5: -ppint32(256),
					6: -ppint32(64),
					7: ppint32(512),
					8: ppint32(32),
				},
				9: {
					1:  ppint32(2),
					2:  ppint32(1),
					3:  ppint32(128),
					4:  ppint32(128),
					5:  ppint32(64),
					6:  ppint32(256),
					7:  ppint32(32),
					8:  ppint32(512),
					9:  ppint32(90),
					10: ppint32(180),
				},
				10: {
					1: ppint32(2),
					2: ppint32(1),
					3: ppint32(128),
					4: ppint32(128),
					5: -ppint32(64),
					6: -ppint32(256),
					7: ppint32(32),
					8: ppint32(512),
				},
			}
			/* This define is used to make the following table readable */
			var snf = [11][11]ppuint32{
				0: {
					0:  ppuint32(mvMPFR_FLAGS_NAN),
					1:  ppuint32(mvMPFR_FLAGS_NAN),
					2:  ppuint32(mvMPFR_FLAGS_NAN),
					5:  ppuint32(mvMPFR_FLAGS_NAN),
					6:  ppuint32(mvMPFR_FLAGS_NAN),
					7:  ppuint32(mvMPFR_FLAGS_NAN),
					8:  ppuint32(mvMPFR_FLAGS_NAN),
					9:  ppuint32(mvMPFR_FLAGS_NAN),
					10: ppuint32(mvMPFR_FLAGS_NAN),
				},
				1: {
					0: ppuint32(mvMPFR_FLAGS_NAN),
				},
				2: {
					0: ppuint32(mvMPFR_FLAGS_NAN),
				},
				3: {
					0:  ppuint32(mvMPFR_FLAGS_NAN),
					6:  ppuint32(mvMPFR_FLAGS_DIVBY0),
					8:  ppuint32(mvMPFR_FLAGS_DIVBY0),
					10: ppuint32(mvMPFR_FLAGS_DIVBY0),
				},
				4: {
					0:  ppuint32(mvMPFR_FLAGS_NAN),
					6:  ppuint32(mvMPFR_FLAGS_DIVBY0),
					8:  ppuint32(mvMPFR_FLAGS_DIVBY0),
					10: ppuint32(mvMPFR_FLAGS_DIVBY0),
				},
				5: {},
				6: {
					0:  ppuint32(mvMPFR_FLAGS_NAN),
					9:  ppuint32(mvMPFR_FLAGS_NAN),
					10: ppuint32(mvMPFR_FLAGS_NAN),
				},
				7: {
					0:  ppuint32(mvMPFR_FLAGS_NAN),
					9:  ppuint32(mvMPFR_FLAGS_INEXACT),
					10: ppuint32(mvMPFR_FLAGS_INEXACT),
				},
				8: {
					0:  ppuint32(mvMPFR_FLAGS_NAN),
					9:  ppuint32(mvMPFR_FLAGS_NAN),
					10: ppuint32(mvMPFR_FLAGS_NAN),
				},
				9: {
					0:  ppuint32(mvMPFR_FLAGS_NAN),
					9:  ppuint32(mvMPFR_FLAGS_INEXACT),
					10: ppuint32(mvMPFR_FLAGS_INEXACT),
				},
				10: {
					0:  ppuint32(mvMPFR_FLAGS_NAN),
					9:  ppuint32(mvMPFR_FLAGS_NAN),
					10: ppuint32(mvMPFR_FLAGS_NAN),
				},
			}
			Xmpfr_clear_flags(cgtls)
			Xmpfr_pow(cgtls, cgbp+176, cgbp+ppuintptr(aai)*16, cgbp+ppuintptr(aaj)*16, ppint32(ecMPFR_RNDN))
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
				ccv6 = 0
			} else {
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
					ccv7 = ppint32(1)
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
						ccv8 = ppint32(2)
					} else {
						aad = Xmpfr_get_d(cgtls, cgbp+176, ppint32(ecMPFR_RNDN))
						if aad > iqlibc.ppFloat64FromInt32(0) {
							ccv9 = aad
						} else {
							ccv9 = -aad
						}
						ccv8 = ppint32(ccv9 * iqlibc.ppFloat64FromFloat64(128))
					}
					ccv7 = ccv8
				}
				ccv6 = ccv7
			}
			aap = ccv6
			if aap != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_sign < 0 {
				aap = -aap
			}
			if aap != *(*ppint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snq)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4)) {

				Xprintf(cgtls, "Error in mpfr_pow for (%s)^(%s) (%d,%d):\ngot %d instead of %d\n\x00", iqlibc.ppVaList(cgbp+232, snname[aai], snname[aaj], aai, aaj, aap, *(*ppint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snq)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4))))
				Xmpfr_dump(cgtls, cgbp+176)
				aaerror = ppint32(1)
			}
			if X__gmpfr_flags != *(*ppuint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snf)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4)) {

				Xprintf(cgtls, "Error in mpfr_pow for (%s)^(%s) (%d,%d):\nFlags = %u instead of expected %u\n\x00", iqlibc.ppVaList(cgbp+232, snname[aai], snname[aaj], aai, aaj, X__gmpfr_flags, *(*ppuint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snf)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4))))
				Xmpfr_dump(cgtls, cgbp+176)
				aaerror = ppint32(1)
			}
			/* Perform the same tests with pow_z & pow_si & pow_ui
			   if t[j] is an integer */
			if Xmpfr_integer_p(cgtls, cgbp+ppuintptr(aaj)*16) != 0 {

				/* mpfr_pow_z */
				Xmpfr_clear_flags(cgtls)
				Xmpfr_get_z(cgtls, cgbp+208, cgbp+ppuintptr(aaj)*16, ppint32(ecMPFR_RNDN))
				Xmpfr_pow_z(cgtls, cgbp+176, cgbp+ppuintptr(aai)*16, cgbp+208, ppint32(ecMPFR_RNDN))
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
					ccv10 = 0
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
						ccv11 = ppint32(1)
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
							ccv12 = ppint32(2)
						} else {
							aad = Xmpfr_get_d(cgtls, cgbp+176, ppint32(ecMPFR_RNDN))
							if aad > iqlibc.ppFloat64FromInt32(0) {
								ccv13 = aad
							} else {
								ccv13 = -aad
							}
							ccv12 = ppint32(ccv13 * iqlibc.ppFloat64FromFloat64(128))
						}
						ccv11 = ccv12
					}
					ccv10 = ccv11
				}
				aap = ccv10
				if aap != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_sign < 0 {
					aap = -aap
				}
				if aap != *(*ppint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snq)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4)) {

					Xprintf(cgtls, "Error in mpfr_pow_z for (%s)^(%s) (%d,%d):\ngot %d instead of %d\n\x00", iqlibc.ppVaList(cgbp+232, snname[aai], snname[aaj], aai, aaj, aap, *(*ppint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snq)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4))))
					Xmpfr_dump(cgtls, cgbp+176)
					aaerror = ppint32(1)
				}
				if X__gmpfr_flags != *(*ppuint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snf)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4)) {

					Xprintf(cgtls, "Error in mpfr_pow_z for (%s)^(%s) (%d,%d):\nFlags = %u instead of expected %u\n\x00", iqlibc.ppVaList(cgbp+232, snname[aai], snname[aaj], aai, aaj, X__gmpfr_flags, *(*ppuint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snf)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4))))
					Xmpfr_dump(cgtls, cgbp+176)
					aaerror = ppint32(1)
				}
				/* mpfr_pow_si */
				Xmpfr_clear_flags(cgtls)
				aasi = Xmpfr_get_si(cgtls, cgbp+ppuintptr(aaj)*16, ppint32(ecMPFR_RNDN))
				Xmpfr_pow_si(cgtls, cgbp+176, cgbp+ppuintptr(aai)*16, aasi, ppint32(ecMPFR_RNDN))
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
					ccv14 = 0
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
						ccv15 = ppint32(1)
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
							ccv16 = ppint32(2)
						} else {
							aad = Xmpfr_get_d(cgtls, cgbp+176, ppint32(ecMPFR_RNDN))
							if aad > iqlibc.ppFloat64FromInt32(0) {
								ccv17 = aad
							} else {
								ccv17 = -aad
							}
							ccv16 = ppint32(ccv17 * iqlibc.ppFloat64FromFloat64(128))
						}
						ccv15 = ccv16
					}
					ccv14 = ccv15
				}
				aap = ccv14
				if aap != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_sign < 0 {
					aap = -aap
				}
				if aap != *(*ppint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snq)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4)) {

					Xprintf(cgtls, "Error in mpfr_pow_si for (%s)^(%s) (%d,%d):\ngot %d instead of %d\n\x00", iqlibc.ppVaList(cgbp+232, snname[aai], snname[aaj], aai, aaj, aap, *(*ppint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snq)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4))))
					Xmpfr_dump(cgtls, cgbp+176)
					aaerror = ppint32(1)
				}
				if X__gmpfr_flags != *(*ppuint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snf)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4)) {

					Xprintf(cgtls, "Error in mpfr_pow_si for (%s)^(%s) (%d,%d):\nFlags = %u instead of expected %u\n\x00", iqlibc.ppVaList(cgbp+232, snname[aai], snname[aaj], aai, aaj, X__gmpfr_flags, *(*ppuint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snf)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4))))
					Xmpfr_dump(cgtls, cgbp+176)
					aaerror = ppint32(1)
				}
				/* if si >= 0, test mpfr_pow_ui */
				if aasi >= 0 {

					Xmpfr_clear_flags(cgtls)
					Xmpfr_pow_ui(cgtls, cgbp+176, cgbp+ppuintptr(aai)*16, iqlibc.ppUint32FromInt32(aasi), ppint32(ecMPFR_RNDN))
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
						ccv18 = 0
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
							ccv19 = ppint32(1)
						} else {
							if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
								ccv20 = ppint32(2)
							} else {
								aad = Xmpfr_get_d(cgtls, cgbp+176, ppint32(ecMPFR_RNDN))
								if aad > iqlibc.ppFloat64FromInt32(0) {
									ccv21 = aad
								} else {
									ccv21 = -aad
								}
								ccv20 = ppint32(ccv21 * iqlibc.ppFloat64FromFloat64(128))
							}
							ccv19 = ccv20
						}
						ccv18 = ccv19
					}
					aap = ccv18
					if aap != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_sign < 0 {
						aap = -aap
					}
					if aap != *(*ppint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snq)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4)) {

						Xprintf(cgtls, "Error in mpfr_pow_ui for (%s)^(%s) (%d,%d):\ngot %d instead of %d\n\x00", iqlibc.ppVaList(cgbp+232, snname[aai], snname[aaj], aai, aaj, aap, *(*ppint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snq)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4))))
						Xmpfr_dump(cgtls, cgbp+176)
						aaerror = ppint32(1)
					}
					if X__gmpfr_flags != *(*ppuint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snf)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4)) {

						Xprintf(cgtls, "Error in mpfr_pow_ui for (%s)^(%s) (%d,%d):\nFlags = %u instead of expected %u\n\x00", iqlibc.ppVaList(cgbp+232, snname[aai], snname[aaj], aai, aaj, X__gmpfr_flags, *(*ppuint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snf)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4))))
						Xmpfr_dump(cgtls, cgbp+176)
						aaerror = ppint32(1)
					}
				}
			} /* integer_p */
			/* Perform the same tests with mpfr_ui_pow */
			if Xmpfr_integer_p(cgtls, cgbp+ppuintptr(aai)*16) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+ppuintptr(aai)*16)).fd_mpfr_sign > 0 {

				/* mpfr_ui_pow */
				Xmpfr_clear_flags(cgtls)
				aasi = Xmpfr_get_si(cgtls, cgbp+ppuintptr(aai)*16, ppint32(ecMPFR_RNDN))
				Xmpfr_ui_pow(cgtls, cgbp+176, iqlibc.ppUint32FromInt32(aasi), cgbp+ppuintptr(aaj)*16, ppint32(ecMPFR_RNDN))
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
					ccv22 = 0
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
						ccv23 = ppint32(1)
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
							ccv24 = ppint32(2)
						} else {
							aad = Xmpfr_get_d(cgtls, cgbp+176, ppint32(ecMPFR_RNDN))
							if aad > iqlibc.ppFloat64FromInt32(0) {
								ccv25 = aad
							} else {
								ccv25 = -aad
							}
							ccv24 = ppint32(ccv25 * iqlibc.ppFloat64FromFloat64(128))
						}
						ccv23 = ccv24
					}
					ccv22 = ccv23
				}
				aap = ccv22
				if aap != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+176)).fd_mpfr_sign < 0 {
					aap = -aap
				}
				if aap != *(*ppint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snq)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4)) {

					Xprintf(cgtls, "Error in mpfr_ui_pow for (%s)^(%s) (%d,%d):\ngot %d instead of %d\n\x00", iqlibc.ppVaList(cgbp+232, snname[aai], snname[aaj], aai, aaj, aap, *(*ppint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snq)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4))))
					Xmpfr_dump(cgtls, cgbp+176)
					aaerror = ppint32(1)
				}
				if X__gmpfr_flags != *(*ppuint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snf)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4)) {

					Xprintf(cgtls, "Error in mpfr_ui_pow for (%s)^(%s) (%d,%d):\nFlags = %u instead of expected %u\n\x00", iqlibc.ppVaList(cgbp+232, snname[aai], snname[aaj], aai, aaj, X__gmpfr_flags, *(*ppuint32)(iqunsafe.ppPointer(ppuintptr(iqunsafe.ppPointer(&snf)) + ppuintptr(aai)*44 + ppuintptr(aaj)*4))))
					Xmpfr_dump(cgtls, cgbp+176)
					aaerror = ppint32(1)
				}
			}

			goto cg_5
		cg_5:
			;
			aaj++
		}
		goto cg_4
	cg_4:
		;
		aai++
	}

	aai = 0
	for {
		if !(aai < ppint32(11)) {
			break
		}
		Xmpfr_clear(cgtls, cgbp+ppuintptr(aai)*16)
		goto cg_26
	cg_26:
		;
		aai++
	}
	Xmpfr_clear(cgtls, cgbp+176)
	Xmpfr_clear(cgtls, cgbp+192)
	Xmpfr_mpz_clear(cgtls, cgbp+208)

	if aaerror != 0 {
		Xexit(cgtls, ppint32(1))
	}
}

func siunderflows(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aa_p tnmpfr_ptr
	var aaemin tnmpfr_exp_t
	var aaerr, aai, aainexact, ccv2, ccv6 ppint32
	var ccv10, ccv11, ccv3, ccv4, ccv8, ccv9 ppuintptr
	var ccv12, ccv7 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aaemin, aaerr, aai, aainexact, ccv10, ccv11, ccv12, ccv2, ccv3, ccv4, ccv6, ccv7, ccv8, ccv9
	aaerr = 0

	Xmpfr_init2(cgtls, cgbp, ppint32(64))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(64))

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)

	aai = ppint32(3)
	for {
		if !(aai < ppint32(10)) {
			break
		}

		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(aai), 0, ppint32(ecMPFR_RNDN))
		Xmpfr_div_2ui(cgtls, cgbp+16, cgbp+16, ppuint32(1), ppint32(ecMPFR_RNDN))
		Xmpfr_pow(cgtls, cgbp+16, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {

			Xprintf(cgtls, "Error in mpfr_pow for \x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, " ^ (%d/2)\nGot \x00", iqlibc.ppVaList(cgbp+56, aai))
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, " instead of 0.\n\x00", 0)
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	Xmpfr_init2(cgtls, cgbp+32, ppint32(55))
	Xmpfr_set_str(cgtls, cgbp, "0.110011010011101001110001110100010000110111101E0\x00", ppint32(2), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+16, "0.101110010011111001011010100011011100111110011E40\x00", ppint32(2), ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	aainexact = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDU))
	if !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {

		Xprintf(cgtls, "Underflow flag is not set for special underflow test.\n\x00", 0)
		aaerr = ppint32(1)
	}
	if aainexact <= 0 {

		Xprintf(cgtls, "Ternary value is wrong for special underflow test.\n\x00", 0)
		aaerr = ppint32(1)
	}
	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv2 = 0
	}
	pp_ = ccv2
	Xmpfr_nextabove(cgtls, cgbp)
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Wrong value for special underflow test.\nGot \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\ninstead of \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(2), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		aaerr = ppint32(1)
	}
	if aaerr != 0 {
		Xexit(cgtls, ppint32(1))
	}

	/* MPFR currently (2006-08-19) segfaults on the following code (and
	   possibly makes other programs crash due to the lack of memory),
	   because y is converted into an mpz_t, and the required precision
	   is too high. */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(2))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(12))
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint32(3), ppint32(-ppint32(2)), ppint32(ecMPFR_RNDN))
	Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(1), X__gmpfr_emax-ppint32(1), ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {

		Xprintf(cgtls, "Underflow test with large y fails.\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint32(-ppint32(256)))
	Xmpfr_set_prec(cgtls, cgbp, ppint32(2))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(12))
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint32(3), ppint32(-ppint32(2)), ppint32(ecMPFR_RNDN))
	Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(1), ppint32(38), ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	aainexact = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) || aainexact >= 0 {

		if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {
			ccv3 = "yes\x00"
		} else {
			ccv3 = "no\x00"
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {
			ccv4 = "yes\x00"
		} else {
			ccv4 = "no\x00"
		}
		Xprintf(cgtls, "Bad underflow detection for 0.75^(2^38). Obtained:\nUnderflow flag... %-3s (should be 'yes')\nZero result...... %-3s (should be 'yes')\nInexact value.... %-3d (should be negative)\n\x00", iqlibc.ppVaList(cgbp+56, ccv3, ccv4, aainexact))
		Xexit(cgtls, ppint32(1))
	}
	Xset_emin(cgtls, aaemin)

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint32(-ppint32(256)))
	Xmpfr_set_prec(cgtls, cgbp, ppint32(2))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(40))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint32(12))
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint32(3), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN))
	Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(-ppint32(1)), ppint32(38), ppint32(ecMPFR_RNDN))
	aai = 0
	for {
		if !(aai < ppint32(4)) {
			break
		}

		if aai == ppint32(2) {
			Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
		}
		Xmpfr_clear_flags(cgtls)
		aainexact = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

		if ccv7 = !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1); !ccv7 {
			if aai == ppint32(3) {
				ccv6 = iqlibc.ppBoolInt32(aainexact <= 0)
			} else {
				ccv6 = iqlibc.ppBoolInt32(aainexact >= 0)
			}
		}
		if ccv7 || ccv6 != 0 {

			Xprintf(cgtls, "Bad underflow detection for (\x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
			if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 {
				ccv8 = "yes\x00"
			} else {
				ccv8 = "no\x00"
			}
			if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {
				ccv9 = "yes\x00"
			} else {
				ccv9 = "no\x00"
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {
				ccv10 = "yes\x00"
			} else {
				ccv10 = "no\x00"
			}
			if aai == ppint32(3) {
				ccv11 = "positive\x00"
			} else {
				ccv11 = "negative\x00"
			}
			Xprintf(cgtls, ")^(-2^38-%d). Obtained:\nOverflow flag.... %-3s (should be 'no')\nUnderflow flag... %-3s (should be 'yes')\nZero result...... %-3s (should be 'yes')\nInexact value.... %-3d (should be %s)\n\x00", iqlibc.ppVaList(cgbp+56, aai, ccv8, ccv9, ccv10, aainexact, ccv11))
			Xexit(cgtls, ppint32(1))
		}
		aainexact = Xmpfr_sub_ui(cgtls, cgbp+16, cgbp+16, ppuint32(1), ppint32(ecMPFR_RNDN))

		if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv12 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1185), "inexact == 0\x00")
		}
		pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		goto cg_5
	cg_5:
		;
		aai++
	}
	Xset_emin(cgtls, aaemin)

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
}

func sioverflows(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var ccv1, ccv2, ccv3 ppbool
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+16 */ tnmpfr_t
	pp_, pp_, pp_ = ccv1, ccv2, ccv3

	/* bug found by Ming J. Tsai <mingjt@delvron.us>, 4 Oct 2003 */

	Xmpfr_init_set_str(cgtls, cgbp, "5.1e32\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_init(cgtls, cgbp+16)

	Xmpfr_pow(cgtls, cgbp+16, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv3 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv3 {
		if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv1 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1203), "! (((b)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1203), "! (((b)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if !(ccv3 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0) {

		Xprintf(cgtls, "Error for a^a for a=5.1e32\n\x00", 0)
		Xprintf(cgtls, "Expected +Inf, got \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
}

func sioverflows2(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aae ppint32
	var aaemax, aaemin tnmpfr_exp_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_ = aae, aaemax, aaemin

	/* x^y in reduced exponent range, where x = 2^b and y is not an integer
	   (so that mpfr_pow_z is not used). */

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	Xset_emin(cgtls, ppint32(-ppint32(128)))

	Xmpfr_inits2(cgtls, ppint32(16), cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

	Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(1), ppint32(-ppint32(64)), ppint32(ecMPFR_RNDN))             /* 2^(-64) */
	Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(-ppint32(1)), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN)) /* -0.5 */
	aae = ppint32(2)
	for {
		if !(aae <= ppint32(32)) {
			break
		}

		Xset_emax(cgtls, ppint32(aae))
		Xmpfr_clear_flags(cgtls)
		Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

			Xprintf(cgtls, "Error in overflows2 (e = %d): expected +Inf, got \x00", iqlibc.ppVaList(cgbp+56, aae))
			Xmpfr_dump(cgtls, cgbp+32)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)) {

			Xprintf(cgtls, "Error in overflows2 (e = %d): bad flags (%u)\n\x00", iqlibc.ppVaList(cgbp+56, aae, X__gmpfr_flags))
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aae += ppint32(17)
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

func sioverflows3(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aa_p tnmpfr_srcptr
	var aaemax, aaemin tnmpfr_exp_t
	var aaflags ppuint32
	var aai, ccv2 ppint32
	var pp_ /* t at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aaemax, aaemin, aaflags, aai, ccv2
	/* x^y where x = 2^b, y is not an integer (so that mpfr_pow_z is not used)
	   and b * y = emax in the extended exponent range. If emax is divisible
	   by 3, we choose x = 2^(-2*emax/3) and y = -3/2.
	   Test also with nextbelow(x). */

	if (iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))-iqlibc.ppInt32FromInt32(1))%iqlibc.ppInt32FromInt32(3) == 0 {

		aaemin = X__gmpfr_emin
		aaemax = X__gmpfr_emax
		Xset_emin(cgtls, iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2)))
		Xset_emax(cgtls, iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))-iqlibc.ppInt32FromInt32(1))

		Xmpfr_inits2(cgtls, ppint32(16), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))

		Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(1), ppint32(-iqlibc.ppInt32FromInt32(2))*((iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))-iqlibc.ppInt32FromInt32(1))/iqlibc.ppInt32FromInt32(3)), ppint32(ecMPFR_RNDN))
		aai = 0
		for {
			if !(aai <= ppint32(1)) {
				break
			}

			Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(-ppint32(3)), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN))
			Xmpfr_clear_flags(cgtls)
			Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

				Xprintf(cgtls, "Error in overflows3 (RNDN, i = %d): expected +Inf, got \x00", iqlibc.ppVaList(cgbp+72, aai))
				Xmpfr_dump(cgtls, cgbp+32)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)) {

				Xprintf(cgtls, "Error in overflows3 (RNDN, i = %d): bad flags (%u)\n\x00", iqlibc.ppVaList(cgbp+72, aai, X__gmpfr_flags))
				Xexit(cgtls, ppint32(1))
			}

			Xmpfr_clear_flags(cgtls)
			Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDZ))
			aaflags = X__gmpfr_flags
			{
				aa_p = cgbp + 32
				ccv2 = Xmpfr_set4(cgtls, cgbp+48, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
			}
			pp_ = ccv2
			Xmpfr_nextabove(cgtls, cgbp+48)
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

				Xprintf(cgtls, "Error in overflows3 (RNDZ, i = %d):\nexpected \x00", iqlibc.ppVaList(cgbp+72, aai))
				Xmpfr_set_inf(cgtls, cgbp+48, ppint32(1))
				Xmpfr_nextbelow(cgtls, cgbp+48)
				Xmpfr_dump(cgtls, cgbp+48)
				Xprintf(cgtls, "got      \x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xexit(cgtls, ppint32(1))
			}
			if aaflags != iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)) {

				Xprintf(cgtls, "Error in overflows3 (RNDZ, i = %d): bad flags (%u)\n\x00", iqlibc.ppVaList(cgbp+72, aai, aaflags))
				Xexit(cgtls, ppint32(1))
			}
			Xmpfr_nextbelow(cgtls, cgbp)

			goto cg_1
		cg_1:
			;
			aai++
		}

		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))

		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
	}
}

func six_near_one(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aainex ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_ = aainex

	Xmpfr_init2(cgtls, cgbp, ppint32(32))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(4))
	Xmpfr_init2(cgtls, cgbp+32, ppint32(33))

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_nextbelow(cgtls, cgbp)
	Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(11), ppint32(-ppint32(2)), ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp+32, "0.111111111111111111111111111111011E0\x00", ppint32(2), ppint32(ecMPFR_RNDN)) != 0 || aainex <= 0 {

		Xprintf(cgtls, "Failure in x_near_one, got inex = %d and\nz = \x00", iqlibc.ppVaList(cgbp+56, aainex))
		Xmpfr_dump(cgtls, cgbp+32)
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
}

func simpfr_pow275(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_srcptr, aar tnmpfr_rnd_t) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aainex ppint32
	var pp_ /* z at bp+0 */ tnmpfr_t
	pp_ = aainex

	Xmpfr_init2(cgtls, cgbp, ppint32(4))
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint32(11), ppint32(-ppint32(2)), ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_pow(cgtls, aay, aax, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_clear(cgtls, cgbp)
	return aainex
}

// C documentation
//
//	/* Bug found by Kevin P. Rauch */
func sibug20071103(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaemax, aaemin tnmpfr_exp_t
	var ccv1 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_ = aaemax, aaemin, ccv1

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	Xset_emin(cgtls, ppint32(-ppint32(1000000)))
	Xset_emax(cgtls, ppint32(1000000))

	Xmpfr_inits2(cgtls, ppint32(64), cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(-ppint32(3)), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN)) /* x = -1.5 */
	Xmpfr_set_str(cgtls, cgbp+16, "-0.ffffffffffffffff\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_set_exp(cgtls, cgbp+16, X__gmpfr_emax)
	Xmpfr_clear_flags(cgtls)
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0 && X__gmpfr_flags == iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1387), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) > 0) && __gmpfr_flags == (1 | 8)\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

// C documentation
//
//	/* Bug found by Kevin P. Rauch */
func sibug20071104(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aa_p tnmpfr_ptr
	var aaemax, aaemin tnmpfr_exp_t
	var aainex, ccv1 ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aa_p, aaemax, aaemin, aainex, ccv1

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	Xset_emin(cgtls, ppint32(-ppint32(1000000)))
	Xset_emax(cgtls, ppint32(1000000))

	Xmpfr_inits2(cgtls, ppint32(20), cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1
	Xmpfr_nextbelow(cgtls, cgbp)                                                                           /* x = -2^(emin-1) */
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+16, ppint32(-iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN)) /* y = -2 */
	Xmpfr_clear_flags(cgtls)
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error in bug20071104: expected +Inf, got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}
	if aainex <= 0 {

		Xprintf(cgtls, "Error in bug20071104: bad ternary value (%d)\n\x00", iqlibc.ppVaList(cgbp+56, aainex))
		Xexit(cgtls, ppint32(1))
	}
	if X__gmpfr_flags != iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)) {

		Xprintf(cgtls, "Error in bug20071104: bad flags (%u)\n\x00", iqlibc.ppVaList(cgbp+56, X__gmpfr_flags))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+56, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

// C documentation
//
//	/* Bug found by Kevin P. Rauch */
func sibug20071127(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aaemax, aaemin tnmpfr_exp_t
	var aai, aatern ppint32
	var ccv2 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aaemax, aaemin, aai, aatern, ccv2

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	Xset_emin(cgtls, ppint32(-ppint32(1000000)))
	Xset_emax(cgtls, ppint32(1000000))

	Xmpfr_init2(cgtls, cgbp, ppint32(128))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(128))
	Xmpfr_init2(cgtls, cgbp+32, ppint32(128))

	Xmpfr_set_str(cgtls, cgbp, "0.80000000000000000000000000000001\x00", ppint32(16), ppint32(ecMPFR_RNDN))

	aai = ppint32(1)
	for {
		if !(aai < ppint32(9)) {
			break
		}

		Xmpfr_set_str(cgtls, cgbp+16, "8000000000000000\x00", ppint32(16), ppint32(ecMPFR_RNDN))
		Xmpfr_add_si(cgtls, cgbp+16, cgbp+16, ppint32(aai), ppint32(ecMPFR_RNDN))
		aatern = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0 && aatern < 0)), ppint32(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1461), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) > 0) && tern < 0\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		goto cg_1
	cg_1:
		;
		aai *= ppint32(2)
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp+32)

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

// C documentation
//
//	/* Bug found by Kevin P. Rauch */
func sibug20071128(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aaemax, aaemin tnmpfr_exp_t
	var aai, aatern ppint32
	var ccv2, ccv4 ppbool
	var pp_ /* max_val at bp+0 */ tnmpfr_t
	var pp_ /* x at bp+16 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+48 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaemin, aai, aatern, ccv2, ccv4

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	Xset_emin(cgtls, ppint32(-ppint32(1000000)))
	Xset_emax(cgtls, ppint32(1000000))

	Xmpfr_init2(cgtls, cgbp, ppint32(64))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(64))
	Xmpfr_init2(cgtls, cgbp+32, ppint32(64))
	Xmpfr_init2(cgtls, cgbp+48, ppint32(64))

	Xmpfr_set_str(cgtls, cgbp, "0.ffffffffffffffff\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emax)

	Xmpfr_neg(cgtls, cgbp+16, cgbp, ppint32(ecMPFR_RNDN))

	/* on 64-bit machines */
	aai = ppint32(41)
	for {
		if !(aai < ppint32(45)) {
			break
		}

		Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint32(-ppint32(1)), ppint32(aai), ppint32(ecMPFR_RNDN))
		Xmpfr_add_si(cgtls, cgbp+32, cgbp+32, ppint32(1), ppint32(ecMPFR_RNDN))
		aatern = Xmpfr_pow(cgtls, cgbp+48, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))

		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_sign < 0 && aatern > 0)), ppint32(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1501), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) < 0) && tern > 0\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		goto cg_1
	cg_1:
		;
		aai++
	}

	/* on 32-bit machines */
	aai = ppint32(9)
	for {
		if !(aai < ppint32(13)) {
			break
		}

		Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint32(-ppint32(1)), ppint32(aai), ppint32(ecMPFR_RNDN))
		Xmpfr_add_si(cgtls, cgbp+32, cgbp+32, ppint32(1), ppint32(ecMPFR_RNDN))
		aatern = Xmpfr_pow(cgtls, cgbp+48, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))

		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_sign < 0)), ppint32(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1510), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((z)->_mpfr_sign) < 0)\x00")
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		goto cg_3
	cg_3:
		;
		aai++
	}

	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+48)
	Xmpfr_clear(cgtls, cgbp)

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

// C documentation
//
//	/* Bug found by Kevin P. Rauch */
func sibug20071218(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aa_p tnmpfr_ptr
	var aatern, ccv1 ppint32
	var ccv2 ppbool
	var pp_ /* t at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aa_p, aatern, ccv1, ccv2

	Xmpfr_inits2(cgtls, ppint32(64), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_str(cgtls, cgbp, "0x.80000000000002P-1023\x00", 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+16, "100000.000000002\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	{
		aa_p = cgbp + 48
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt32FromInt32(0) - iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1
	Xmpfr_nextabove(cgtls, cgbp+48)
	aatern = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2)) && !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2)))), ppint32(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1535), "!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2)) && !(((t)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if Xmpfr_cmp3(cgtls, cgbp+32, cgbp+48, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in bug20071218 (1): Expected\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp+48)
		Xprintf(cgtls, "Got\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}
	if aatern <= 0 {

		Xprintf(cgtls, "Error in bug20071218 (1): bad ternary value (%d instead of positive)\n\x00", iqlibc.ppVaList(cgbp+72, aatern))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_mul_2ui(cgtls, cgbp+16, cgbp+16, ppuint32(32), ppint32(ecMPFR_RNDN))
	aatern = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error in bug20071218 (2): expected 0, got\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}
	if aatern >= 0 {

		Xprintf(cgtls, "Error in bug20071218 (2): bad ternary value (%d instead of negative)\n\x00", iqlibc.ppVaList(cgbp+72, aatern))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
}

// C documentation
//
//	/* With revision 5429, this gives:
//	 *   pow.c:43:  assertion failed: !mpfr_integer_p (y)
//	 * This is fixed in revision 5432.
//	 */
func sibug20080721(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaerr, aai, aainex, aainex0, aarnd, ccv3 ppint32
	var aaflags ppuint32
	var ccv1 ppbool
	var pp_ /* t at bp+48 */ [2]tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaerr, aaflags, aai, aainex, aainex0, aarnd, ccv1, ccv3
	aaerr = 0

	/* Note: input values have been chosen in a way to select the
	 * general case. If mpfr_pow is modified, in particular line
	 *     if (y_is_integer && (MPFR_GET_EXP (y) <= 256))
	 * make sure that this test still does what we want.
	 */
	Xmpfr_inits2(cgtls, ppint32(4913), cgbp, iqlibc.ppVaList(cgbp+88, cgbp+16, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_inits2(cgtls, ppint32(8), cgbp+32, iqlibc.ppVaList(cgbp+88, cgbp+48, cgbp+48+1*16, iqlibc.ppUintptrFromInt32(0)))
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_nextbelow(cgtls, cgbp)
	Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(1), (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_prec-ppint32(1), ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_add_ui(cgtls, cgbp+16, cgbp+16, ppuint32(1), ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1589), "inex == 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_str_binary(cgtls, cgbp+48, "-0.10101101e2\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+48+1*16, "-0.10101110e2\x00")
	aarnd = 0
	for {
		if !(aarnd < ppint32(ecMPFR_RNDF)) {
			break
		}

		aai = iqlibc.ppBoolInt32(aarnd == ppint32(ecMPFR_RNDN) || aarnd == ppint32(ecMPFR_RNDD) || aarnd == ppint32(ecMPFR_RNDA))
		if aai != 0 {
			ccv3 = -ppint32(1)
		} else {
			ccv3 = ppint32(1)
		}
		aainex0 = ccv3
		Xmpfr_clear_flags(cgtls)
		aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, aarnd)

		if X__gmpfr_flags != ppuint32(mvMPFR_FLAGS_INEXACT) || !(iqlibc.ppBoolInt32(aainex > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex0 > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex0 < iqlibc.ppInt32FromInt32(0))) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_cmp3(cgtls, cgbp+32, cgbp+48+ppuintptr(aai)*16, ppint32(1)) != 0 {

			aaflags = X__gmpfr_flags

			Xprintf(cgtls, "Error in bug20080721 with %s\n\x00", iqlibc.ppVaList(cgbp+88, Xmpfr_print_rnd_mode(cgtls, aarnd)))
			Xprintf(cgtls, "expected \x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+48+ppuintptr(aai)*16, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, ", inex = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+88, aainex0, iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)))
			Xprintf(cgtls, "got      \x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint32(0), cgbp+32, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, ", inex = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+88, aainex, aaflags))
			aaerr = ppint32(1)
		}

		goto cg_2
	cg_2:
		;
		aarnd++
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+88, cgbp+16, cgbp+32, cgbp+48, cgbp+48+1*16, iqlibc.ppUintptrFromInt32(0)))
	if aaerr != 0 {
		Xexit(cgtls, ppint32(1))
	}
}

// C documentation
//
//	/* The following test fails in r5552 (32-bit and 64-bit). This is due to:
//	 *   mpfr_log (t, absx, MPFR_RNDU);
//	 *   mpfr_mul (t, y, t, MPFR_RNDU);
//	 * in pow.c, that is supposed to compute an upper bound on exp(y*ln|x|),
//	 * but this is incorrect if y is negative.
//	 */
func sibug20080820(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaemin tnmpfr_exp_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z1 at bp+32 */ tnmpfr_t
	var pp_ /* z2 at bp+48 */ tnmpfr_t
	pp_ = aaemin

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2)))
	Xmpfr_init2(cgtls, cgbp, ppint32(80))
	Xmpfr_init2(cgtls, cgbp+16, iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt64(4)*iqlibc.ppUint32FromInt32(mvCHAR_BIT)+iqlibc.ppUint32FromInt32(32)))
	Xmpfr_init2(cgtls, cgbp+32, ppint32(2))
	Xmpfr_init2(cgtls, cgbp+48, ppint32(80))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_nextbelow(cgtls, cgbp)
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+16, X__gmpfr_emin-ppint32(2), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_nextabove(cgtls, cgbp+16)
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	Xmpfr_pow(cgtls, cgbp+48, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	/* As x > 0, the rounded value of x^y to nearest in precision p is equal
	   to 0 iff x^y <= 2^(emin - 2). In particular, this does not depend on
	   the precision p. Hence the following test. */
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+48)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) {

		Xprintf(cgtls, "Error in bug20080820\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
	Xset_emin(cgtls, aaemin)
}

func sibug20110320(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaemin tnmpfr_exp_t
	var aaflags ppuint32
	var aainex ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z1 at bp+32 */ tnmpfr_t
	var pp_ /* z2 at bp+48 */ tnmpfr_t
	pp_, pp_, pp_ = aaemin, aaflags, aainex

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint32(11))
	Xmpfr_inits2(cgtls, ppint32(2), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint32(1), ppint32(215), ppint32(ecMPFR_RNDN))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1024)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	aaflags = X__gmpfr_flags
	Xmpfr_set_ui_2exp(cgtls, cgbp+48, ppuint32(1), ppint32(iqlibc.ppInt32FromInt32(215)*iqlibc.ppInt32FromInt32(1024)), ppint32(ecMPFR_RNDN))
	if aainex != 0 || aaflags != ppuint32(0) || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+48) != 0) {

		Xprintf(cgtls, "Error in bug20110320\n\x00", 0)
		Xprintf(cgtls, "Expected inex = 0, flags = 0, z = \x00", 0)
		Xmpfr_dump(cgtls, cgbp+48)
		Xprintf(cgtls, "Got      inex = %d, flags = %u, z = \x00", iqlibc.ppVaList(cgbp+72, aainex, aaflags))
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
	Xset_emin(cgtls, aaemin)
}

func sitst20140422(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaflags ppuint32
	var aainex, aarnd ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z1 at bp+32 */ tnmpfr_t
	var pp_ /* z2 at bp+48 */ tnmpfr_t
	pp_, pp_, pp_ = aaflags, aainex, aarnd

	Xmpfr_inits2(cgtls, ppint32(128), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1296)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(3), ppint32(-ppint32(2)), ppint32(ecMPFR_RNDN))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+48, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(216)), 0, ppint32(ecMPFR_RNDN))
	aarnd = 0
	for {
		if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}

		Xmpfr_clear_flags(cgtls)
		aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, aarnd)
		aaflags = X__gmpfr_flags
		if aainex != 0 || aaflags != ppuint32(0) || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+48) != 0) {

			Xprintf(cgtls, "Error in bug20140422 with %s\n\x00", iqlibc.ppVaList(cgbp+72, Xmpfr_print_rnd_mode(cgtls, aarnd)))
			Xprintf(cgtls, "Expected inex = 0, flags = 0, z = \x00", 0)
			Xmpfr_dump(cgtls, cgbp+48)
			Xprintf(cgtls, "Got      inex = %d, flags = %u, z = \x00", iqlibc.ppVaList(cgbp+72, aainex, aaflags))
			Xmpfr_dump(cgtls, cgbp+32)
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aarnd++
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
}

func sicoverage(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaemin tnmpfr_exp_t
	var aaflags, aaflags_u tnmpfr_flags_t
	var aai, aainex, aainex_u ppint32
	var ccv1, ccv10, ccv11, ccv13, ccv3, ccv4, ccv5, ccv7, ccv8, ccv9 ppbool
	var pp_ /* t at bp+48 */ tnmpfr_t
	var pp_ /* u at bp+64 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemin, aaflags, aaflags_u, aai, aainex, aainex_u, ccv1, ccv10, ccv11, ccv13, ccv3, ccv4, ccv5, ccv7, ccv8, ccv9

	/* check a corner case with prec(z)=1 */
	Xmpfr_init2(cgtls, cgbp, ppint32(2))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(8))
	Xmpfr_init2(cgtls, cgbp+32, ppint32(1))
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint32(3), ppint32(-ppint32(2)), ppint32(ecMPFR_RNDN)) /* x = 3/4 */
	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint32(-ppint32(40)))
	Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(199), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN)) /* y = 99.5 */
	/* x^y ~ 0.81*2^-41 */
	Xmpfr_clear_underflow(cgtls)
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex > iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1738), "inex > 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, ppuint32(1), ppint32(-ppint32(41))) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1739), "mpfr_cmp_ui_2exp (z, 1, -41) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, ppuint32(1), ppint32(-ppint32(41))) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	/* there should be no underflow, since with unbounded exponent range,
	   and a precision of 1 bit, x^y is rounded to 1.0*2^-41 */

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint32(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1742), "!((int) (__gmpfr_flags & 1))\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(201), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN)) /* y = 100.5 */
	/* x^y ~ 0.61*2^-41 */
	Xmpfr_clear_underflow(cgtls)
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex > iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1747), "inex > 0\x00")
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, ppuint32(1), ppint32(-ppint32(41))) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv7 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1748), "mpfr_cmp_ui_2exp (z, 1, -41) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, ppuint32(1), ppint32(-ppint32(41))) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	/* there should be an underflow, since with unbounded exponent range,
	   and a precision of 1 bit, x^y is rounded to 0.5*2^-41 */

	if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint32(1)) != 0; !ccv8 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1751), "((int) (__gmpfr_flags & 1))\x00")
	}
	pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(203), ppint32(-ppint32(1)), ppint32(ecMPFR_RNDN)) /* y = 101.5 */
	/* x^y ~ 0.46*2^-41 */
	Xmpfr_clear_underflow(cgtls)
	aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))

	if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex < iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv9 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1756), "inex < 0\x00")
	}
	pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0) == 0)), ppint32(1)) != 0; !ccv10 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1757), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign))) < 0) == 0\x00")
	}
	pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint32(1)) != 0; !ccv11 {
		Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1758), "((int) (__gmpfr_flags & 1))\x00")
	}
	pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+88, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
	Xset_emin(cgtls, aaemin)

	/* test for x = -2, y an odd integer with EXP(y) > i */
	Xmpfr_inits2(cgtls, ppint32(10), cgbp+48, iqlibc.ppVaList(cgbp+88, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_ui_2exp(cgtls, cgbp+48, ppuint32(1), iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromUint32(1)<<iqlibc.ppInt32FromInt32(8)), ppint32(ecMPFR_RNDN))
	aai = ppint32(8)
	for {
		if !(aai <= ppint32(300)) {
			break
		}

		pp_ = Xmpfr_mul_si(cgtls, cgbp+64, cgbp+48, ppint32(-iqlibc.ppInt32FromInt32(2)), ppint32(ecMPFR_RNDN)) /* u = (-2)^(2^i + 1) */
		Xmpfr_init2(cgtls, cgbp, ppint32(10))
		Xmpfr_init2(cgtls, cgbp+16, ppint32(aai+ppint32(1)))
		Xmpfr_init2(cgtls, cgbp+32, ppint32(10))
		pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint32(-iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
		Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(1), ppint32(aai), ppint32(ecMPFR_RNDN))
		Xmpfr_nextabove(cgtls, cgbp+16) /* y = 2^i + 1 */
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3) {

			aainex_u = -ppint32(1)
			aaflags_u = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
		} else {

			aainex_u = 0
			aaflags_u = ppuint32(0)
		}
		Xmpfr_clear_flags(cgtls)
		aainex = Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
		aaflags = X__gmpfr_flags

		if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2)) && !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2)))), ppint32(1)) != 0; !ccv13 {
			Xmpfr_assert_fail(cgtls, "tpow.c\x00", ppint32(1790), "!(((z)->_mpfr_exp) == (((-0x7fffffffL-1))+2)) && !(((u)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if Xmpfr_cmp3(cgtls, cgbp+32, cgbp+64, ppint32(1)) != 0 || !(iqlibc.ppBoolInt32(aainex > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex_u > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex_u < iqlibc.ppInt32FromInt32(0))) || aaflags != aaflags_u {

			Xprintf(cgtls, "Error in coverage for (-2)^(2^%d + 1):\n\x00", iqlibc.ppVaList(cgbp+88, aai))
			Xprintf(cgtls, "Expected \x00", 0)
			Xmpfr_dump(cgtls, cgbp+64)
			Xprintf(cgtls, "  with inex = %d and flags:\x00", iqlibc.ppVaList(cgbp+88, aainex_u))
			Xflags_out(cgtls, aaflags_u)
			Xprintf(cgtls, "Got      \x00", 0)
			Xmpfr_dump(cgtls, cgbp+32)
			Xprintf(cgtls, "  with inex = %d and flags:\x00", iqlibc.ppVaList(cgbp+88, aainex))
			Xflags_out(cgtls, aaflags)
			Xexit(cgtls, ppint32(1))
		}
		Xmpfr_sqr(cgtls, cgbp+48, cgbp+48, ppint32(ecMPFR_RNDN))
		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+88, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

		goto cg_12
	cg_12:
		;
		aai++
	}
	Xmpfr_clears(cgtls, cgbp+48, iqlibc.ppVaList(cgbp+88, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

}

func sicheck_binary128(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var pp_ /* t at bp+48 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t

	Xmpfr_init2(cgtls, cgbp, ppint32(113))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(113))
	Xmpfr_init2(cgtls, cgbp+32, ppint32(113))
	Xmpfr_init2(cgtls, cgbp+48, ppint32(113))

	/* x = 1-2^(-113) */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_nextbelow(cgtls, cgbp)
	/* y = 1.125*2^126 = 9*2^123 */
	Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint32(9), ppint32(123), ppint32(ecMPFR_RNDN))
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	/* x^y ~ 3.48e-4003 */
	Xmpfr_set_str(cgtls, cgbp+48, "1.16afef53c30899a5c172bb302882p-13296\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	if !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+48) != 0) {

		Xprintf(cgtls, "Error in check_binary128\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+48)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	/* x = 5192296858534827628530496329220095/2^112 */
	Xmpfr_set_str(cgtls, cgbp, "1.fffffffffffffffffffffffffffep-1\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	/* y = -58966440806378323534486035691038613504 */
	Xmpfr_set_str(cgtls, cgbp+16, "-1.62e42fefa39ef35793c7673007e5p125\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_pow(cgtls, cgbp+32, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+48, "1.fffffffffffffffffffffffff105p16383\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	if !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+48) != 0) {

		Xprintf(cgtls, "Error in check_binary128 (2)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+48)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+48)
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	var aap tnmpfr_prec_t
	pp_ = aap

	Xtests_start_mpfr(cgtls)

	sicoverage(cgtls)
	sibug20071127(cgtls)
	sispecial(cgtls)
	siparticular_cases(cgtls)
	sicheck_pow_ui(cgtls)
	sicheck_pow_si(cgtls)
	sicheck_pown_ieee754_2019(cgtls)
	sicheck_special_pow_si(cgtls)
	sipow_si_long_min(cgtls)
	aap = ppint32(mvMPFR_PREC_MIN)
	for {
		if !(aap < ppint32(100)) {
			break
		}
		sicheck_inexact(cgtls, aap)
		goto cg_1
	cg_1:
		;
		aap++
	}
	siunderflows(cgtls)
	sioverflows(cgtls)
	sioverflows2(cgtls)
	sioverflows3(cgtls)
	six_near_one(cgtls)
	sibug20071103(cgtls)
	sibug20071104(cgtls)
	sibug20071128(cgtls)
	sibug20071218(cgtls)
	sibug20080721(cgtls)
	sibug20080820(cgtls)
	sibug20110320(cgtls)
	sitst20140422(cgtls)
	sicheck_binary128(cgtls)

	sitest_generic(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100))
	sitest_generic_ui(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100))
	sitest_generic_si(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100))

	Xdata_check(cgtls, "data/pow275\x00", pp__ccgo_fp(simpfr_pow275), "mpfr_pow275\x00")

	Xbad_cases(cgtls, pp__ccgo_fp(sipowu2), pp__ccgo_fp(siroot2), "mpfr_pow_ui[2]\x00", ppint32(8), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipows2), pp__ccgo_fp(siroot2), "mpfr_pow_si[2]\x00", ppint32(8), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowm2), pp__ccgo_fp(sirootm2), "mpfr_pow_si[-2]\x00", ppint32(8), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowu3), pp__ccgo_fp(siroot3), "mpfr_pow_ui[3]\x00", ppint32(256), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipows3), pp__ccgo_fp(siroot3), "mpfr_pow_si[3]\x00", ppint32(256), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowm3), pp__ccgo_fp(sirootm3), "mpfr_pow_si[-3]\x00", ppint32(256), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowu4), pp__ccgo_fp(siroot4), "mpfr_pow_ui[4]\x00", ppint32(8), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipows4), pp__ccgo_fp(siroot4), "mpfr_pow_si[4]\x00", ppint32(8), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowm4), pp__ccgo_fp(sirootm4), "mpfr_pow_si[-4]\x00", ppint32(8), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowu5), pp__ccgo_fp(siroot5), "mpfr_pow_ui[5]\x00", ppint32(256), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipows5), pp__ccgo_fp(siroot5), "mpfr_pow_si[5]\x00", ppint32(256), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowm5), pp__ccgo_fp(sirootm5), "mpfr_pow_si[-5]\x00", ppint32(256), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowu17), pp__ccgo_fp(siroot17), "mpfr_pow_ui[17]\x00", ppint32(256), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipows17), pp__ccgo_fp(siroot17), "mpfr_pow_si[17]\x00", ppint32(256), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowm17), pp__ccgo_fp(sirootm17), "mpfr_pow_si[-17]\x00", ppint32(256), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowu120), pp__ccgo_fp(siroot120), "mpfr_pow_ui[120]\x00", ppint32(8), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipows120), pp__ccgo_fp(siroot120), "mpfr_pow_si[120]\x00", ppint32(8), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))
	Xbad_cases(cgtls, pp__ccgo_fp(sipowm120), pp__ccgo_fp(sirootm120), "mpfr_pow_si[-120]\x00", ppint32(8), ppint32(-ppint32(256)), ppint32(255), ppint32(4), ppint32(128), ppint32(800), ppint32(40))

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

func ___builtin_unreachable(*iqlibc.ppTLS)

var ___gmp_bits_per_limb ppint32

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

var ___gmpfr_emax ppint32

var ___gmpfr_emin ppint32

var ___gmpfr_flags ppuint32

func ___gmpfr_mpfr_pow_sj(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64, ppint32) ppint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32, ppuintptr, ppint32) ppuint32

func _bad_cases(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32, ppint32, ppint32, ppint32, ppint32, ppint32, ppint32)

func _data_check(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _mpfr_add_si(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_can_round(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32, ppint32, ppint32) ppint32

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_divby0(*iqlibc.ppTLS)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clear_underflow(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp3(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_cmp_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_cmp_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_div_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_get_d(*iqlibc.ppTLS, ppuintptr, ppint32) ppfloat64

func _mpfr_get_si(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_get_z(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_init_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_inits2(*iqlibc.ppTLS, ppint32, ppuintptr, ppuintptr)

func _mpfr_integer_p(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_mpz_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mpz_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mul_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_mul_si(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttoinf(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttozero(*iqlibc.ppTLS, ppuintptr)

func _mpfr_pow(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_pow_si(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_pow_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_pow_z(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_prec_round(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_random2(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32, ppuintptr)

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppint8

func _mpfr_root(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_rootn_si(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_rootn_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_exp(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint32, ppint32, ppint32) ppint32

func _mpfr_set_zero(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_setmax(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_sqr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_sub_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_ui_pow(*iqlibc.ppTLS, ppuintptr, ppuint32, ppuintptr, ppint32) ppint32

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint32

func _randlong(*iqlibc.ppTLS) ppint32

func _randulong(*iqlibc.ppTLS) ppuint32

func _set_emax(*iqlibc.ppTLS, ppint32)

func _set_emin(*iqlibc.ppTLS, ppint32)

var _stdout ppuintptr

func _tests_default_random(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32, ppint32, ppint32)

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

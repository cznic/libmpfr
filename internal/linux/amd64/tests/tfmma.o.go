// Code generated for linux/amd64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/amd64 -UHAVE_NEARBYINT -mlong-double-64 -c -o tfmma.o.go tfmma.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1680
const mvMPFR_AI_THRESHOLD3 = 24368
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 894
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 6522
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 12
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 23540
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 19
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/x86_64/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 2
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__FXSR__ = 1
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "x86_64-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MMX_WITH_SSE__ = 1
const mv__MMX__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 16
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__SSE2_MATH__ = 1
const mv__SSE2__ = 1
const mv__SSE_MATH__ = 1
const mv__SSE__ = 1
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__amd64 = 1
const mv__amd64__ = 1
const mv__code_model_small__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__k8 = 1
const mv__k8__ = 1
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv__x86_64 = 1
const mv__x86_64__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppint8
	fdfrac_digits        ppint8
	fdp_cs_precedes      ppint8
	fdp_sep_by_space     ppint8
	fdn_cs_precedes      ppint8
	fdn_sep_by_space     ppint8
	fdp_sign_posn        ppint8
	fdn_sign_posn        ppint8
	fdint_p_cs_precedes  ppint8
	fdint_p_sep_by_space ppint8
	fdint_n_cs_precedes  ppint8
	fdint_n_sep_by_space ppint8
	fdint_p_sign_posn    ppint8
	fdint_n_sign_posn    ppint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

// C documentation
//
//	/* check both mpfr_fmma and mpfr_fmms */
func sirandom_test(cgtls *iqlibc.ppTLS, aaa tnmpfr_ptr, aab tnmpfr_ptr, aac tnmpfr_ptr, aad tnmpfr_ptr, aarnd tnmpfr_rnd_t) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aainex_ref, aainex_res ppint32
	var aap tnmpfr_prec_t
	var pp_ /* ab at bp+64 */ tnmpfr_t
	var pp_ /* cd at bp+96 */ tnmpfr_t
	var pp_ /* ref at bp+0 */ tnmpfr_t
	var pp_ /* res at bp+32 */ tnmpfr_t
	pp_, pp_, pp_ = aainex_ref, aainex_res, aap
	aap = (*tn__mpfr_struct)(iqunsafe.ppPointer(aaa)).fd_mpfr_prec

	Xmpfr_init2(cgtls, cgbp+32, aap)
	Xmpfr_init2(cgtls, cgbp, aap)
	Xmpfr_init2(cgtls, cgbp+64, (*tn__mpfr_struct)(iqunsafe.ppPointer(aaa)).fd_mpfr_prec+(*tn__mpfr_struct)(iqunsafe.ppPointer(aab)).fd_mpfr_prec)
	Xmpfr_init2(cgtls, cgbp+96, (*tn__mpfr_struct)(iqunsafe.ppPointer(aac)).fd_mpfr_prec+(*tn__mpfr_struct)(iqunsafe.ppPointer(aad)).fd_mpfr_prec)

	/* first check fmma */
	aainex_res = Xmpfr_fmma(cgtls, cgbp+32, aaa, aab, aac, aad, aarnd)
	Xmpfr_mul(cgtls, cgbp+64, aaa, aab, aarnd)
	Xmpfr_mul(cgtls, cgbp+96, aac, aad, aarnd)
	aainex_ref = Xmpfr_add(cgtls, cgbp, cgbp+64, cgbp+96, aarnd)
	if !(iqlibc.ppBoolInt32(aainex_res > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex_res < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex_ref > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex_ref < iqlibc.ppInt32FromInt32(0))) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp) != 0) {

		Xprintf(cgtls, "mpfr_fmma failed for p=%ld rnd=%s\n\x00", iqlibc.ppVaList(cgbp+136, aap, Xmpfr_print_rnd_mode(cgtls, aarnd)))
		Xprintf(cgtls, "a=\x00", 0)
		Xmpfr_dump(cgtls, aaa)
		Xprintf(cgtls, "b=\x00", 0)
		Xmpfr_dump(cgtls, aab)
		Xprintf(cgtls, "c=\x00", 0)
		Xmpfr_dump(cgtls, aac)
		Xprintf(cgtls, "d=\x00", 0)
		Xmpfr_dump(cgtls, aad)
		Xprintf(cgtls, "Expected\n  \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+136, aainex_ref))
		Xprintf(cgtls, "Got\n  \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+136, aainex_res))
		Xexit(cgtls, ppint32(1))
	}

	/* then check fmms */
	aainex_res = Xmpfr_fmms(cgtls, cgbp+32, aaa, aab, aac, aad, aarnd)
	Xmpfr_mul(cgtls, cgbp+64, aaa, aab, aarnd)
	Xmpfr_mul(cgtls, cgbp+96, aac, aad, aarnd)
	aainex_ref = Xmpfr_sub(cgtls, cgbp, cgbp+64, cgbp+96, aarnd)
	if !(iqlibc.ppBoolInt32(aainex_res > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex_res < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex_ref > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex_ref < iqlibc.ppInt32FromInt32(0))) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp) != 0) {

		Xprintf(cgtls, "mpfr_fmms failed for p=%ld rnd=%s\n\x00", iqlibc.ppVaList(cgbp+136, aap, Xmpfr_print_rnd_mode(cgtls, aarnd)))
		Xprintf(cgtls, "a=\x00", 0)
		Xmpfr_dump(cgtls, aaa)
		Xprintf(cgtls, "b=\x00", 0)
		Xmpfr_dump(cgtls, aab)
		Xprintf(cgtls, "c=\x00", 0)
		Xmpfr_dump(cgtls, aac)
		Xprintf(cgtls, "d=\x00", 0)
		Xmpfr_dump(cgtls, aad)
		Xprintf(cgtls, "Expected\n  \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+136, aainex_ref))
		Xprintf(cgtls, "Got\n  \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+136, aainex_res))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp)
}

func sirandom_tests(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aap tnmpfr_prec_t
	var aar ppint32
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	pp_, pp_ = aap, aar

	aap = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aap <= ppint64(4096)) {
			break
		}

		Xmpfr_inits2(cgtls, aap, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp+96, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		aar = 0
		for {
			if !(aar < ppint32(ecMPFR_RNDF)) {
				break
			}
			sirandom_test(cgtls, cgbp, cgbp+32, cgbp+64, cgbp+96, aar)
			goto cg_6
		cg_6:
			;
			aar++
		}
		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))

		goto cg_1
	cg_1:
		;
		aap++
	}
}

func sizero_tests(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(208)
	defer cgtls.ppFree(208)

	var aa_p, aa_p1 tnmpfr_srcptr
	var aaemax, aaemin tnmpfr_exp_t
	var aaflags tnmpfr_flags_t
	var aai, aainex, aaj, aar, ccv3, ccv4, ccv6, ccv8 ppint32
	var ccv7 ppbool
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	var pp_ /* res at bp+128 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aaemax, aaemin, aaflags, aai, aainex, aaj, aar, ccv3, ccv4, ccv6, ccv7, ccv8

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
	Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))

	Xmpfr_inits2(cgtls, ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)), cgbp, iqlibc.ppVaList(cgbp+168, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
	aai = 0
	for {
		if !(aai <= ppint32(4)) {
			break
		}

		switch aai {

		case 0:
			fallthrough
		case ppint32(1):
			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(aai), 0, ppint32(ecMPFR_RNDN))
			break
			fallthrough
		case ppint32(2):
			Xmpfr_setmax(cgtls, cgbp, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
			break
			fallthrough
		case ppint32(3):
			Xmpfr_setmin(cgtls, cgbp, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
			break
			fallthrough
		case ppint32(4):
			Xmpfr_setmin(cgtls, cgbp, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))+iqlibc.ppInt64FromInt32(1))
			break
		}
		aar = 0
		for {
			if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}

			{
				aa_p = cgbp
				ccv3 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
			}
			pp_ = ccv3
			{
				aa_p1 = cgbp
				ccv4 = Xmpfr_set4(cgtls, cgbp+64, aa_p1, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign)
			}
			pp_ = ccv4
			Xmpfr_neg(cgtls, cgbp+96, cgbp, ppint32(ecMPFR_RNDN))
			/* We also want to test cases where the precision of the
			   result is twice the precision of each input, in case
			   add1sp.c and/or sub1sp.c could be involved. */
			aaj = ppint32(1)
			for {
				if !(aaj <= ppint32(2)) {
					break
				}

				Xmpfr_init2(cgtls, cgbp+128, ppint64((iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*aaj))
				Xmpfr_clear_flags(cgtls)
				aainex = Xmpfr_fmma(cgtls, cgbp+128, cgbp, cgbp+32, cgbp+64, cgbp+96, aar)
				aaflags = X__gmpfr_flags

				if ccv7 = aaflags != ppuint32(0) || aainex != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv7 {
					if aar == ppint32(ecMPFR_RNDD) {
						ccv6 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_sign > 0)
					} else {
						ccv6 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_sign < 0)
					}
				}
				if ccv7 || ccv6 != 0 {

					Xprintf(cgtls, "Error in zero_tests on i = %d, %s\n\x00", iqlibc.ppVaList(cgbp+168, aai, Xmpfr_print_rnd_mode(cgtls, aar)))
					if aar == ppint32(ecMPFR_RNDD) {
						ccv8 = ppint32('-')
					} else {
						ccv8 = ppint32('+')
					}
					Xprintf(cgtls, "Expected %c0, inex = 0\n\x00", iqlibc.ppVaList(cgbp+168, ccv8))
					Xprintf(cgtls, "Got      \x00", 0)
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_sign > 0 {
						Xprintf(cgtls, "+\x00", 0)
					}
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp+128, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, ", inex = %d\n\x00", iqlibc.ppVaList(cgbp+168, aainex))
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, ppuint32(0))
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
				Xmpfr_clear(cgtls, cgbp+128)

				goto cg_5
			cg_5:
				;
				aaj++
			} /* j */

			goto cg_2
		cg_2:
			;
			aar++
		} /* r */

		goto cg_1
	cg_1:
		;
		aai++
	} /* i */
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+168, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

func simax_tests(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaemax, aaemin tnmpfr_exp_t
	var aaflags1, aaflags2 tnmpfr_flags_t
	var aai, aainex1, aainex2, aar ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y1 at bp+32 */ tnmpfr_t
	var pp_ /* y2 at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaemin, aaflags1, aaflags2, aai, aainex1, aainex2, aar

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
	Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))

	Xmpfr_init2(cgtls, cgbp, ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
	Xmpfr_setmax(cgtls, cgbp, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
	aaflags1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
	aar = 0
	for {
		if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}

		/* We also want to test cases where the precision of the
		   result is twice the precision of each input, in case
		   add1sp.c and/or sub1sp.c could be involved. */
		aai = ppint32(1)
		for {
			if !(aai <= ppint32(2)) {
				break
			}

			Xmpfr_inits2(cgtls, ppint64((iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*aai), cgbp+32, iqlibc.ppVaList(cgbp+104, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
			aainex1 = Xmpfr_mul(cgtls, cgbp+32, cgbp, cgbp, aar)
			Xmpfr_clear_flags(cgtls)
			aainex2 = Xmpfr_fmma(cgtls, cgbp+64, cgbp, cgbp, cgbp, cgbp, aar)
			aaflags2 = X__gmpfr_flags
			if !(aaflags1 == aaflags2 && iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0) == iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0) && Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

				Xprintf(cgtls, "Error in max_tests for %s\n\x00", iqlibc.ppVaList(cgbp+104, Xmpfr_print_rnd_mode(cgtls, aar)))
				Xprintf(cgtls, "Expected \x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+104, aainex1))
				Xflags_out(cgtls, aaflags1)
				Xprintf(cgtls, "Got      \x00", 0)
				Xmpfr_dump(cgtls, cgbp+64)
				Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+104, aainex2))
				Xflags_out(cgtls, aaflags2)
				Xexit(cgtls, ppint32(1))
			}
			Xmpfr_clears(cgtls, cgbp+32, iqlibc.ppVaList(cgbp+104, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

			goto cg_2
		cg_2:
			;
			aai++
		} /* i */

		goto cg_1
	cg_1:
		;
		aar++
	} /* r */
	Xmpfr_clear(cgtls, cgbp)

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

// C documentation
//
//	/* a^2 - (a+k)(a-k) = k^2 where a^2 overflows but k^2 usually doesn't.
//	 * a^2 + cd where a^2 overflows and cd doesn't affect the overflow
//	 * (and cd may even underflow).
//	 */
func sioverflow_tests(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(256)
	defer cgtls.ppFree(256)

	var aaadd, aai, aainex, aainex1, aainex2, ccv13, ccv7 ppint32
	var aaemax, aaexp_a, aaold_emax tnmpfr_exp_t
	var aaflags1, aaflags2 tnmpfr_flags_t
	var aaprec_a, aaprec_z tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv11, ccv2, ccv9 ppint64
	var ccv12, ccv5, ccv6 ppbool
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	var pp_ /* k at bp+32 */ tnmpfr_t
	var pp_ /* z1 at bp+128 */ tnmpfr_t
	var pp_ /* z2 at bp+160 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaadd, aaemax, aaexp_a, aaflags1, aaflags2, aai, aainex, aainex1, aainex2, aaold_emax, aaprec_a, aaprec_z, aarnd, ccv11, ccv12, ccv13, ccv2, ccv5, ccv6, ccv7, ccv9

	aaold_emax = X__gmpfr_emax

	aai = 0
	for {
		if !(aai < ppint32(40)) {
			break
		}

		aaadd = aai & ppint32(1)

		/* In most cases, we do the test with the maximum exponent. */
		if aai%ppint32(8) != 0 {
			ccv2 = iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) - iqlibc.ppInt64FromInt32(1)
		} else {
			ccv2 = ppint64(ppint32(64) + iqlibc.ppBoolInt32(Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0)))
		}
		aaemax = ccv2
		Xset_emax(cgtls, aaemax)
		aaexp_a = aaemax/ppint64(2) + ppint64(32)

		aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % ppuint64(ecMPFR_RNDF))
		aaprec_a = iqlibc.ppInt64FromUint64(ppuint64(64) + Xrandlimb(cgtls)%ppuint64(100))
		aaprec_z = iqlibc.ppInt64FromUint64(ppuint64(mvMPFR_PREC_MIN) + Xrandlimb(cgtls)%ppuint64(160))

		Xmpfr_init2(cgtls, cgbp, aaprec_a)
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandom(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppint32(ecMPFR_RNDU))
		Xmpfr_set_exp(cgtls, cgbp, aaexp_a)

		Xmpfr_init2(cgtls, cgbp+32, aaprec_a-ppint64(32))
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandom(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppint32(ecMPFR_RNDU))
		Xmpfr_set_exp(cgtls, cgbp+32, aaexp_a-ppint64(32))

		Xmpfr_init2(cgtls, cgbp+64, aaprec_a+ppint64(1))
		aainex = Xmpfr_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))

		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(280), "inex == 0\x00")
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		Xmpfr_init2(cgtls, cgbp+96, aaprec_a)
		aainex = Xmpfr_sub(cgtls, cgbp+96, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))

		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(284), "inex == 0\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if aaadd != 0 {
			Xmpfr_neg(cgtls, cgbp+96, cgbp+96, ppint32(ecMPFR_RNDN))
		}

		Xmpfr_init2(cgtls, cgbp+128, aaprec_z)
		Xmpfr_clear_flags(cgtls)
		aainex1 = Xmpfr_sqr(cgtls, cgbp+128, cgbp+32, aarnd)
		aaflags1 = X__gmpfr_flags

		Xmpfr_init2(cgtls, cgbp+160, aaprec_z)
		Xmpfr_clear_flags(cgtls)
		if aaadd != 0 {
			ccv7 = Xmpfr_fmma(cgtls, cgbp+160, cgbp, cgbp, cgbp+64, cgbp+96, aarnd)
		} else {
			ccv7 = Xmpfr_fmms(cgtls, cgbp+160, cgbp, cgbp, cgbp+64, cgbp+96, aarnd)
		}
		aainex2 = ccv7
		aaflags2 = X__gmpfr_flags

		if !(aaflags1 == aaflags2 && iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0) == iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0) && Xmpfr_equal_p(cgtls, cgbp+128, cgbp+160) != 0) {

			Xprintf(cgtls, "Error 1 in overflow_tests for %s\n\x00", iqlibc.ppVaList(cgbp+200, Xmpfr_print_rnd_mode(cgtls, aarnd)))
			Xprintf(cgtls, "Expected \x00", 0)
			Xmpfr_dump(cgtls, cgbp+128)
			Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+200, aainex1))
			Xflags_out(cgtls, aaflags1)
			Xprintf(cgtls, "Got      \x00", 0)
			Xmpfr_dump(cgtls, cgbp+160)
			Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+200, aainex2))
			Xflags_out(cgtls, aaflags2)
			Xexit(cgtls, ppint32(1))
		}

		/* c and d such that a^2 +/- cd ~= a^2 (overflow) */
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandom(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppint32(ecMPFR_RNDU))
		if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
			ccv9 = aaexp_a - ppint64(2)
		} else {
			ccv9 = X__gmpfr_emin
		}
		Xmpfr_set_exp(cgtls, cgbp+64, ccv9)
		if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
			Xmpfr_neg(cgtls, cgbp+64, cgbp+64, ppint32(ecMPFR_RNDN))
		}
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandom(cgtls, cgbp+96, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppint32(ecMPFR_RNDU))
		if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
			ccv11 = aaexp_a - ppint64(2)
		} else {
			ccv11 = X__gmpfr_emin
		}
		Xmpfr_set_exp(cgtls, cgbp+96, ccv11)
		if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
			Xmpfr_neg(cgtls, cgbp+96, cgbp+96, ppint32(ecMPFR_RNDN))
		}

		Xmpfr_clear_flags(cgtls)
		aainex1 = Xmpfr_sqr(cgtls, cgbp+128, cgbp, aarnd)
		aaflags1 = X__gmpfr_flags

		if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aaflags1 == iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)))), ppint64(1)) != 0; !ccv12 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(329), "flags1 == (2 | 8)\x00")
		}
		pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		Xmpfr_clear_flags(cgtls)
		if aaadd != 0 {
			ccv13 = Xmpfr_fmma(cgtls, cgbp+160, cgbp, cgbp, cgbp+64, cgbp+96, aarnd)
		} else {
			ccv13 = Xmpfr_fmms(cgtls, cgbp+160, cgbp, cgbp, cgbp+64, cgbp+96, aarnd)
		}
		aainex2 = ccv13
		aaflags2 = X__gmpfr_flags

		if !(aaflags1 == aaflags2 && iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0) == iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0) && Xmpfr_equal_p(cgtls, cgbp+128, cgbp+160) != 0) {

			Xprintf(cgtls, "Error 2 in overflow_tests for %s\n\x00", iqlibc.ppVaList(cgbp+200, Xmpfr_print_rnd_mode(cgtls, aarnd)))
			Xprintf(cgtls, "Expected \x00", 0)
			Xmpfr_dump(cgtls, cgbp+128)
			Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+200, aainex1))
			Xflags_out(cgtls, aaflags1)
			Xprintf(cgtls, "Got      \x00", 0)
			Xmpfr_dump(cgtls, cgbp+160)
			Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+200, aainex2))
			Xflags_out(cgtls, aaflags2)
			Xprintf(cgtls, "a=\x00", 0)
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "c=\x00", 0)
			Xmpfr_dump(cgtls, cgbp+64)
			Xprintf(cgtls, "d=\x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xexit(cgtls, ppint32(1))
		}

		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+200, cgbp+32, cgbp+64, cgbp+96, cgbp+128, cgbp+160, iqlibc.ppUintptrFromInt32(0)))

		goto cg_1
	cg_1:
		;
		aai++
	}

	Xset_emax(cgtls, aaold_emax)
}

// C documentation
//
//	/* (1/2)x + (1/2)x = x tested near underflow */
func sihalf_plus_half(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aa_p tnmpfr_srcptr
	var aaemin tnmpfr_exp_t
	var aaflags tnmpfr_flags_t
	var aai, aainex, aaneg, aar, ccv2 ppint32
	var ccv3 ppbool
	var pp_ /* h at bp+0 */ tnmpfr_t
	var pp_ /* x1 at bp+32 */ tnmpfr_t
	var pp_ /* x2 at bp+64 */ tnmpfr_t
	var pp_ /* y at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aaemin, aaflags, aai, aainex, aaneg, aar, ccv2, ccv3

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
	Xmpfr_inits2(cgtls, ppint64(4), cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(1), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN))

	Xmpfr_setmin(cgtls, cgbp+32, X__gmpfr_emin)
	for {
		if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp < X__gmpfr_emin+ppint64(2)) {
			break
		}

		{
			aa_p = cgbp + 32
			ccv2 = Xmpfr_set4(cgtls, cgbp+64, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
		}
		aainex = ccv2

		if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv3 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(382), "inex == 0\x00")
		}
		pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aaneg = 0
		for {
			if !(aaneg < ppint32(2)) {
				break
			}

			aar = 0
			for {
				if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
					break
				}

				/* We also want to test cases where the precision of the
				   result is twice the precision of each input, in case
				   add1sp.c and/or sub1sp.c could be involved. */
				aai = ppint32(1)
				for {
					if !(aai <= ppint32(2)) {
						break
					}

					Xmpfr_init2(cgtls, cgbp+96, ppint64((iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*aai))
					Xmpfr_clear_flags(cgtls)
					aainex = Xmpfr_fmma(cgtls, cgbp+96, cgbp, cgbp+64, cgbp, cgbp+64, aar)
					aaflags = X__gmpfr_flags
					if !(aaflags == ppuint32(0) && aainex == 0 && Xmpfr_equal_p(cgtls, cgbp+96, cgbp+64) != 0) {

						Xprintf(cgtls, "Error in half_plus_half for %s\n\x00", iqlibc.ppVaList(cgbp+136, Xmpfr_print_rnd_mode(cgtls, aar)))
						Xprintf(cgtls, "Expected \x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "  with inex = 0, flags =\x00", 0)
						Xflags_out(cgtls, ppuint32(0))
						Xprintf(cgtls, "Got      \x00", 0)
						Xmpfr_dump(cgtls, cgbp+96)
						Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+136, aainex))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					Xmpfr_clear(cgtls, cgbp+96)

					goto cg_6
				cg_6:
					;
					aai++
				}

				goto cg_5
			cg_5:
				;
				aar++
			}
			Xmpfr_neg(cgtls, cgbp+64, cgbp+64, ppint32(ecMPFR_RNDN))

			goto cg_4
		cg_4:
			;
			aaneg++
		}

		goto cg_1
	cg_1:
		;
		Xmpfr_nextabove(cgtls, cgbp+32)
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xset_emin(cgtls, aaemin)
}

// C documentation
//
//	/* check that result has exponent in [emin, emax]
//	   (see https://sympa.inria.fr/sympa/arc/mpfr/2017-04/msg00016.html)
//	   Overflow detection in sub1.c was incorrect (only for UBF cases);
//	   fixed in r11414. */
func sibug20170405(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var ccv1, ccv10, ccv11, ccv12, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv2, ccv20, ccv21, ccv23, ccv25, ccv3, ccv5, ccv6, ccv7, ccv8, ccv9 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = ccv1, ccv10, ccv11, ccv12, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv2, ccv20, ccv21, ccv23, ccv25, ccv3, ccv5, ccv6, ccv7, ccv8, ccv9

	Xmpfr_inits2(cgtls, ppint64(866), cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

	Xmpfr_set_str_binary(cgtls, cgbp, "-0.10010101110110001111000110010100011001111011110001010100010000111110001010111110100001000000011010001000010000101110000000001100001011000110010000100111001100000101110000000001001101101101010110000110100010010111011001101101010011111000101100000010001100010000011100000000011110100010111011101011000101101011110110001010011001101110011101100001111000011000000011000010101010000101001001010000111101100001000001011110011000110010001100001101101001001010000111100101000010111001001101010011001110110001000010101001100000101010110000100100100010101011111001001100010001010110011000000001011110011000110001000100101000111010010111111110010111001101110101010010101101010100111001011100101101010111010011001000001010011001010001101000111011010010100110011001111111000011101111001010111001001011011011110101101001100011010001010110011100001101100100001001100111010100010100E768635654\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.11010001010111110010110101010011000010010011010011011101100100110000110101100110111010001001110101110000011101100010110100100011001101111010100011111001011100111101110101101001000101011110101101101011010100110010111111010011011100101111110011001001010101011101111100011101100001010010011000110010110101001110010001101111111001100100000101010100110011101101101010011001000110100001001100000010110010101111000110110000111011000110001000100100100101111110001111100101011100100100110111010000010110110001110010001001101000000110111000101000110101111110000110001110100010101111010110001111010111111111010011001001100110011000110010110011000101110001010001101000100010000110011101010010010011110100000111100000101100110001111010000100011111000001101111110100000011011110010100010010011111111000010110000000011010011001100110001110111111010111110000111110010110011001000010E768635576\x00")
	/* since emax = 1073741821, x^2-y^2 should overflow */
	Xmpfr_fmms(cgtls, cgbp+64, cgbp, cgbp, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv3 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv3 {
		if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv1 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(436), "! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(436), "! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv3 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0)), ppint64(1)) != 0; !ccv9 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(436), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tfmma.c\", 436, \"! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tfmma.c\", 436, \"! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((z)->_mpfr_sign)) > 0\x00")
		if ccv8 = iqlibc.Bool(!(0 != 0)); !ccv8 {
			if ccv7 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv7 {
				if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv5 {
					Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(436), "! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv6 {
					Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(436), "! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv8 || ccv7 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* same test when z has a different precision */
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(867))
	Xmpfr_fmms(cgtls, cgbp+64, cgbp, cgbp, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv12 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv12 {
		if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv10 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(441), "! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv11 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(441), "! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv12 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0)), ppint64(1)) != 0; !ccv18 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(441), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tfmma.c\", 441, \"! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tfmma.c\", 441, \"! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((z)->_mpfr_sign)) > 0\x00")
		if ccv17 = iqlibc.Bool(!(0 != 0)); !ccv17 {
			if ccv16 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv16 {
				if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv14 {
					Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(441), "! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv15 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv15 {
					Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(441), "! (((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv15 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv17 || ccv16 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(564))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(564))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(2256))
	Xmpfr_set_str_binary(cgtls, cgbp, "1e-536870913\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-1e-536870913\x00")
	Xmpfr_fmma(cgtls, cgbp+64, cgbp, cgbp+32, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	/* we should get -0 as result */

	if ccv19 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0)), ppint64(1)) != 0; !ccv19 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(450), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign))) < 0)\x00")
	}
	pp_ = ccv19 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(564))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(564))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(2256))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.10010000111100110011001101111111101000111001011000110100110010000101000100010001000000111100010000101001011011111001111000110101111100101111001100001100011101100100011110000000011000010110111100111000100101010001011111010111011001110010001011101111001011001110110000010000011100010001010001011100100110111110101001001111001011101111110011101110101010110100010010111011111100010101111100011110111001011111101110101101101110100101111010000101011110100000000110111101000001100001000100010110100111010011011010110011100111010000101110010101111001011100110101100001100e-737194993\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-1.00101000100001001101011110100010110011101010011011010111100110101011111100000100101000111010111101100100110010001110011011100100110110000001011001000111101111101111110101100110111000000011000001101001010100010010001110001000011010000100111001001100101111111100010101110101001101101101111010100011011110001000010000010100011000011000010110101100000111111110111001100100100001101011111011100101110111000100101010110100010011101010110010100110100111000000100111101101101000000011110000100110100100011000010011110010001010000110100011111101101101110001110001101101010e-737194903\x00")
	Xmpfr_fmma(cgtls, cgbp+64, cgbp, cgbp+32, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	/* we should get -0 as result */

	if ccv20 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0)), ppint64(1)) != 0; !ccv20 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(459), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign))) < 0)\x00")
	}
	pp_ = ccv20 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(2))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(2))
	/* (a+i*b)*(c+i*d) with:
	   a=0.10E1
	   b=0.10E-536870912
	   c=0.10E-536870912
	   d=0.10E1 */
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10E1\x00")             /* x = a = d */
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10E-536870912\x00") /* y = b = c */
	/* real part is a*c-b*d = x*y-y*x */
	Xmpfr_fmms(cgtls, cgbp+64, cgbp, cgbp+32, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))

	if ccv21 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)))), ppint64(1)) != 0; !ccv21 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(473), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && !((0 ? (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign))) < 0)\x00")
	}
	pp_ = ccv21 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	/* imaginary part is a*d+b*c = x*x+y*y */
	Xmpfr_fmma(cgtls, cgbp+64, cgbp, cgbp, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv23 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv23 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(476), "(__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (z) : mpfr_cmp_ui_2exp ((z), (1), 0)) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv23 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_fmma(cgtls, cgbp+64, cgbp+32, cgbp+32, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv25 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv25 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(478), "(__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (z) : mpfr_cmp_ui_2exp ((z), (1), 0)) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv25 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

func siunderflow_tests(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaemin tnmpfr_exp_t
	var aap tnmpfr_prec_t
	var ccv2, ccv3, ccv4 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aaemin, aap, ccv2, ccv3, ccv4

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint64(-ppint32(17)))
	aap = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aap <= ppint64(1024)) {
			break
		}

		Xmpfr_inits2(cgtls, aap, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
		Xmpfr_init2(cgtls, cgbp+64, aap+ppint64(1))
		Xmpfr_set_str_binary(cgtls, cgbp, "1e-10\x00")
		Xmpfr_set_str_binary(cgtls, cgbp+32, "1e-11\x00")
		Xmpfr_clear_underflow(cgtls)
		Xmpfr_fmms(cgtls, cgbp+64, cgbp, cgbp, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))
		/* the exact result is 2^-20-2^-22, and should underflow */

		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(501), "((int) (__gmpfr_flags & 1))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv3 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(502), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
		}
		pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(503), "((0 ? (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign))) < 0) == 0\x00")
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

		goto cg_1
	cg_1:
		;
		aap++
	}
	Xset_emin(cgtls, aaemin)
}

func sibug20180604(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aaemin tnmpfr_exp_t
	var aaret ppint32
	var ccv1, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* yneg at bp+64 */ tnmpfr_t
	var pp_ /* z at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemin, aaret, ccv1, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint64(-ppint32(1073741821)))
	Xmpfr_inits2(cgtls, ppint64(564), cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_init2(cgtls, cgbp+96, ppint64(2256))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.10010000111100110011001101111111101000111001011000110100110010000101000100010001000000111100010000101001011011111001111000110101111100101111001100001100011101100100011110000000011000010110111100111000100101010001011111010111011001110010001011101111001011001110110000010000011100010001010001011100100110111110101001001111001011101111110011101110101010110100010010111011111100010101111100011110111001011111101110101101101110100101111010000101011110100000000110111101000001100001000100010110100111010011011010110011100111010000101110010101111001011100110101100001100E-737194993\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-1.00101000100001001101011110100010110011101010011011010111100110101011111100000100101000111010111101100100110010001110011011100100110110000001011001000111101111101111110101100110111000000011000001101001010100010010001110001000011010000100111001001100101111111100010101110101001101101101111010100011011110001000010000010100011000011000010110101100000111111110111001100100100001101011111011100101110111000100101010110100010011101010110010100110100111000000100111101101101000000011110000100110100100011000010011110010001010000110100011111101101101110001110001101101010E-737194903\x00")

	Xmpfr_clear_underflow(cgtls)
	aaret = Xmpfr_fmms(cgtls, cgbp+96, cgbp, cgbp, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(525), "((int) (__gmpfr_flags & 1))\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(526), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppInt32FromInt32(1))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(527), "((0 ? (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign))) < 0) == 1\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aaret > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(528), "ret > 0\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear_underflow(cgtls)
	aaret = Xmpfr_fmms(cgtls, cgbp+96, cgbp+32, cgbp+32, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(532), "((int) (__gmpfr_flags & 1))\x00")
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv6 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(533), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv7 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(534), "((0 ? (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign))) < 0) == 0\x00")
	}
	pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aaret < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv8 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(535), "ret < 0\x00")
	}
	pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_neg(cgtls, cgbp+64, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_clear_underflow(cgtls)
	aaret = Xmpfr_fmms(cgtls, cgbp+96, cgbp, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))

	if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv9 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(540), "((int) (__gmpfr_flags & 1))\x00")
	}
	pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv10 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(541), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv11 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(542), "((0 ? (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign))) < 0) == 0\x00")
	}
	pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aaret < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv12 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(543), "ret < 0\x00")
	}
	pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear_underflow(cgtls)
	aaret = Xmpfr_fmms(cgtls, cgbp+96, cgbp+32, cgbp+64, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv13 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(547), "((int) (__gmpfr_flags & 1))\x00")
	}
	pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv14 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(548), "(((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv15 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppInt32FromInt32(1))), ppint64(1)) != 0; !ccv15 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(549), "((0 ? (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (z) : (mpfr_srcptr) (z))))->_mpfr_sign))) < 0) == 1\x00")
	}
	pp_ = ccv15 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv16 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aaret > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv16 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(550), "ret > 0\x00")
	}
	pp_ = ccv16 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
	Xset_emin(cgtls, aaemin)
}

// C documentation
//
//	/* this bug was discovered from mpc_mul */
func sibug20200206(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(208)
	defer cgtls.ppFree(208)

	var aaemin tnmpfr_exp_t
	var pp_ /* xim at bp+32 */ tnmpfr_t
	var pp_ /* xre at bp+0 */ tnmpfr_t
	var pp_ /* yim at bp+96 */ tnmpfr_t
	var pp_ /* yre at bp+64 */ tnmpfr_t
	var pp_ /* zre at bp+128 */ tnmpfr_t
	pp_ = aaemin
	aaemin = X__gmpfr_emin

	Xset_emin(cgtls, ppint64(-ppint32(1073)))
	Xmpfr_inits2(cgtls, ppint64(53), cgbp, iqlibc.ppVaList(cgbp+168, cgbp+32, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_str(cgtls, cgbp, "-0x8.294611b331c8p-904\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "-0x1.859278c2992fap-676\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+64, "0x9.ac54802a95f8p-820\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+96, "0x3.17e59e7612aap-412\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_fmms(cgtls, cgbp+128, cgbp, cgbp+64, cgbp+32, cgbp+96, ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_exp > iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_exp < ppint64(-ppint32(1073)) {

		Xprintf(cgtls, "Error, mpfr_fmms returns an out-of-range exponent:\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp+128)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+168, cgbp+32, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
	Xset_emin(cgtls, aaemin)
}

// C documentation
//
//	/* check for integer overflow (see bug fixed in r13798) */
func siextreme_underflow(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aa_p tnmpfr_srcptr
	var aaemin tnmpfr_exp_t
	var ccv1 ppint32
	var ccv2, ccv3 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aa_p, aaemin, ccv1, ccv2, ccv3
	aaemin = X__gmpfr_emin

	Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
	Xmpfr_inits2(cgtls, ppint64(64), cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(1), iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1), ppint32(ecMPFR_RNDN))
	{
		aa_p = cgbp
		ccv1 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
	}
	pp_ = ccv1
	Xmpfr_nextabove(cgtls, cgbp)
	Xmpfr_clear_flags(cgtls)
	Xmpfr_fmms(cgtls, cgbp+64, cgbp, cgbp, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(594), "__gmpfr_flags == (1 | 8)\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0)), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tfmma.c\x00", ppint32(595), "(((z)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)) && (((z)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xset_emin(cgtls, aaemin)
}

// C documentation
//
//	/* Test double-rounding cases in mpfr_set_1_2, which is called when
//	   all the precisions are the same. With set.c before r13347, this
//	   triggers errors for neg=0. */
func sidouble_rounding(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(256)
	defer cgtls.ppFree(256)

	var aainex1, aainex2, aaneg, aap ppint32
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	var pp_ /* z1 at bp+128 */ tnmpfr_t
	var pp_ /* z2 at bp+160 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aainex1, aainex2, aaneg, aap

	aap = ppint32(4)
	for {
		if !(aap < iqlibc.ppInt32FromInt32(4)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))) {
			break
		}

		Xmpfr_inits2(cgtls, ppint64(aap), cgbp, iqlibc.ppVaList(cgbp+200, cgbp+32, cgbp+64, cgbp+96, cgbp+128, cgbp+160, iqlibc.ppUintptrFromInt32(0)))
		/* Choose a, b, c, d such that a*b = 2^p and c*d = 1 + epsilon */
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
		Xmpfr_set_ui_2exp(cgtls, cgbp+32, ppuint64(1), ppint64(aap-ppint32(1)), ppint32(ecMPFR_RNDN))
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
		Xmpfr_nextabove(cgtls, cgbp+64)
		/* c = 1 + ulp(1) = 1 + 2 * ulp(1/2) */

		aaneg = 0
		for {
			if !(aaneg <= ppint32(1)) {
				break
			}

			/* Set d = 1 - (1 + neg) * ulp(1/2), thus
			 * c*d = 1 + (1 - neg) * ulp(1/2) - 2 * (1 + neg) * ulp(1/2)^2,
			 * so that a*b + c*d rounds to 2^p + 1 and epsilon has the
			 * same sign as (-1)^neg.
			 */
			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+96, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
			Xmpfr_nextbelow(cgtls, cgbp+96)
			Xmpfr_set_ui_2exp(cgtls, cgbp+128, ppuint64(1), ppint64(aap), ppint32(ecMPFR_RNDN))
			if aaneg != 0 {

				Xmpfr_nextbelow(cgtls, cgbp+96)
				aainex1 = -ppint32(1)
			} else {

				Xmpfr_nextabove(cgtls, cgbp+128)
				aainex1 = ppint32(1)
			}

			aainex2 = Xmpfr_fmma(cgtls, cgbp+160, cgbp, cgbp+32, cgbp+64, cgbp+96, ppint32(ecMPFR_RNDN))
			if !(Xmpfr_equal_p(cgtls, cgbp+128, cgbp+160) != 0 && iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0) == iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0)) {

				Xprintf(cgtls, "Error in double_rounding for precision %d, neg=%d\n\x00", iqlibc.ppVaList(cgbp+200, aap, aaneg))
				Xprintf(cgtls, "Expected \x00", 0)
				Xmpfr_dump(cgtls, cgbp+128)
				Xprintf(cgtls, "with inex = %d\n\x00", iqlibc.ppVaList(cgbp+200, aainex1))
				Xprintf(cgtls, "Got      \x00", 0)
				Xmpfr_dump(cgtls, cgbp+160)
				Xprintf(cgtls, "with inex = %d\n\x00", iqlibc.ppVaList(cgbp+200, aainex2))
				Xexit(cgtls, ppint32(1))
			}

			goto cg_2
		cg_2:
			;
			aaneg++
		}

		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+200, cgbp+32, cgbp+64, cgbp+96, cgbp+128, cgbp+160, iqlibc.ppUintptrFromInt32(0)))

		goto cg_1
	cg_1:
		;
		aap++
	}
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	Xtests_start_mpfr(cgtls)

	sibug20200206(cgtls)
	sibug20180604(cgtls)
	siunderflow_tests(cgtls)
	sirandom_tests(cgtls)
	sizero_tests(cgtls)
	simax_tests(cgtls)
	sioverflow_tests(cgtls)
	sihalf_plus_half(cgtls)
	sibug20170405(cgtls)
	sidouble_rounding(cgtls)
	siextreme_underflow(cgtls)

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___builtin_unreachable(*iqlibc.ppTLS)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuint64

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _mpfr_add(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clear_underflow(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_fmma(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_fmms(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_inits2(*iqlibc.ppTLS, ppint64, ppuintptr, ppuintptr)

func _mpfr_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppint8

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_exp(*iqlibc.ppTLS, ppuintptr, ppint64) ppint32

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_setmax(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_setmin(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_sqr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_urandom(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

var _stdout ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

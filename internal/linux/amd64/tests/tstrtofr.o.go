// Code generated for linux/amd64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/amd64 -UHAVE_NEARBYINT -mlong-double-64 -c -o tstrtofr.o.go tstrtofr.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBSIZE = 512
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1680
const mvMPFR_AI_THRESHOLD3 = 24368
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 894
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 6522
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 12
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 23540
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 19
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/x86_64/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 2
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__FXSR__ = 1
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "x86_64-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MMX_WITH_SSE__ = 1
const mv__MMX__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 16
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__SSE2_MATH__ = 1
const mv__SSE2__ = 1
const mv__SSE_MATH__ = 1
const mv__SSE__ = 1
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__amd64 = 1
const mv__amd64__ = 1
const mv__code_model_small__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__k8 = 1
const mv__k8__ = 1
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv__x86_64 = 1
const mv__x86_64__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppint8
	fdfrac_digits        ppint8
	fdp_cs_precedes      ppint8
	fdp_sep_by_space     ppint8
	fdn_cs_precedes      ppint8
	fdn_sep_by_space     ppint8
	fdp_sign_posn        ppint8
	fdn_sign_posn        ppint8
	fdint_p_cs_precedes  ppint8
	fdint_p_sep_by_space ppint8
	fdint_n_cs_precedes  ppint8
	fdint_n_sep_by_space ppint8
	fdint_p_sign_posn    ppint8
	fdint_n_sign_posn    ppint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

// C documentation
//
//	/* The implicit \0 is useless, but we do not write num_to_text[62] otherwise
//	   g++ complains. */
var sinum_to_text36 = [37]ppint8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'}
var sinum_to_text62 = [63]ppint8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'}

func sicheck_special(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aa_p, aa_p1, aa_p2, aa_p3, aa_p4, aa_p5, aa_p6, aa_p7, aa_p8 tnmpfr_ptr
	var aabase, aai, aaj, aares, ccv1, ccv14, ccv15, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9 ppint32
	var aap, cgp12 ppuintptr
	var ccv16 ppbool
	var pp_ /* s at bp+64 */ ppuintptr
	var pp_ /* t at bp+72 */ [11]ppint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aa_p2, aa_p3, aa_p4, aa_p5, aa_p6, aa_p7, aa_p8, aabase, aai, aaj, aap, aares, ccv1, ccv14, ccv15, ccv16, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9, cgp12

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)

	/* Check dummy case */
	aares = Xmpfr_strtofr(cgtls, cgbp, "1234567.89E1\x00", iqlibc.ppUintptrFromInt32(0), ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "1234567.89E1\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Results differ between strtofr and set_str.\n set_str gives: \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, " strtofr gives: \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	/* Check NAN  */
	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1 /* make sure that x is modified */
	aares = Xmpfr_strtofr(cgtls, cgbp, "NaN\x00", cgbp+64, ppint32(10), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))) != 0 {

		Xprintf(cgtls, "Error for setting NAN (1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	{
		aa_p1 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv2 = 0
	}
	pp_ = ccv2 /* make sure that x is modified */
	aares = Xmpfr_strtofr(cgtls, cgbp, "+NaN\x00", cgbp+64, ppint32(10), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))) != 0 {

		Xprintf(cgtls, "Error for setting +NAN (1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	{
		aa_p2 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv3 = 0
	}
	pp_ = ccv3 /* make sure that x is modified */
	aares = Xmpfr_strtofr(cgtls, cgbp, " -NaN\x00", cgbp+64, ppint32(10), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))) != 0 {

		Xprintf(cgtls, "Error for setting -NAN (1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	{
		aa_p3 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv4 = 0
	}
	pp_ = ccv4 /* make sure that x is modified */
	aares = Xmpfr_strtofr(cgtls, cgbp, "@nAn@xx\x00", cgbp+64, ppint32(16), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "xx\x00") != 0 {

		Xprintf(cgtls, "Error for setting NAN (2)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	{
		aa_p4 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p4)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p4)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv5 = 0
	}
	pp_ = ccv5 /* make sure that x is modified */
	aares = Xmpfr_strtofr(cgtls, cgbp, "NAN(abcdEDF__1256)Hello\x00", cgbp+64, ppint32(10), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "Hello\x00") != 0 {

		Xprintf(cgtls, "Error for setting NAN (3)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	{
		aa_p5 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p5)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p5)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv6 = 0
	}
	pp_ = ccv6 /* make sure that x is modified */
	aares = Xmpfr_strtofr(cgtls, cgbp, "NAN(abcdEDF)__1256)Hello\x00", cgbp+64, ppint32(10), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "__1256)Hello\x00") != 0 {

		Xprintf(cgtls, "Error for setting NAN (4)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	{
		aa_p6 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p6)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p6)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv7 = 0
	}
	pp_ = ccv7 /* make sure that x is modified */
	aares = Xmpfr_strtofr(cgtls, cgbp, "NAN(abc%dEDF)__1256)Hello\x00", cgbp+64, ppint32(10), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "(abc%dEDF)__1256)Hello\x00") != 0 {

		Xprintf(cgtls, "Error for setting NAN (5)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	{
		aa_p7 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p7)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p7)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv8 = 0
	}
	pp_ = ccv8 /* make sure that x is modified */
	aares = Xmpfr_strtofr(cgtls, cgbp, "NAN((abc))\x00", cgbp+64, ppint32(10), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "((abc))\x00") != 0 {

		Xprintf(cgtls, "Error for setting NAN (6)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	{
		aa_p8 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p8)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p8)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv9 = 0
	}
	pp_ = ccv9 /* make sure that x is modified */
	aares = Xmpfr_strtofr(cgtls, cgbp, "NAN()foo\x00", cgbp+64, ppint32(10), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "foo\x00") != 0 {

		Xprintf(cgtls, "Error for setting NAN (7)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* Check infinity */
	aai = 0
	for {
		if !(aai <= ppint32(0xff)) {
			break
		}

		*(*[11]ppint8)(iqunsafe.ppPointer(cgbp + 72)) = [11]ppint8{'+', '@', 'I', 'N', 'F', 'I', 'N', 'I', 'T', 'Y'}

		/* Test all the case variants, assuming ASCII or similar.
		   The first letters are changed first, so that at i = 8,
		   the 2^3 = 8 "INF" case variants have been tested, and
		   they don't need to be tested again for i > 8. */
		aaj = 0
		for {
			if !(aaj < ppint32(8)) {
				break
			}
			if aai>>aaj%ppint32(2) != 0 {

				cgp12 = cgbp + 72 + ppuintptr(aaj+ppint32(2))
				*(*ppint8)(iqunsafe.ppPointer(cgp12)) = ppint8(ppint32(*(*ppint8)(iqunsafe.ppPointer(cgp12))) + (iqlibc.ppInt32FromUint8('a') - iqlibc.ppInt32FromUint8('A')))
			}
			goto cg_11
		cg_11:
			;
			aaj++
		}

		/* Test "INFINITY", "+INFINITY", "-INFINITY",
		     "INF", "+INF", "-INF",
		     "@INF@", "+@INF@", "-@INF@",
		up to case changes. */
		aaj = 0
		for {
			if !(aaj < ppint32(9)) {
				break
			}

			if aaj == ppint32(3) {

				/* At i = 8, we have tested all the "INF" case variants. */
				if aai >= ppint32(8) {
					break
				}
				(*(*[11]ppint8)(iqunsafe.ppPointer(cgbp + 72)))[ppint32(5)] = ppint8('\000')
			}
			if aaj == ppint32(6) {

				(*(*[11]ppint8)(iqunsafe.ppPointer(cgbp + 72)))[ppint32(1)] = ppint8('@')
				(*(*[11]ppint8)(iqunsafe.ppPointer(cgbp + 72)))[ppint32(5)] = ppint8('@')
				(*(*[11]ppint8)(iqunsafe.ppPointer(cgbp + 72)))[ppint32(6)] = ppint8('\000')
			}
			if aaj%ppint32(3) == ppint32(1) {
				(*(*[11]ppint8)(iqunsafe.ppPointer(cgbp + 72)))[iqlibc.ppBoolInt32(aaj != ppint32(7))] = ppint8('+')
			}
			if aaj%ppint32(3) == ppint32(2) {
				(*(*[11]ppint8)(iqunsafe.ppPointer(cgbp + 72)))[iqlibc.ppBoolInt32(aaj != ppint32(8))] = ppint8('-')
			}
			aap = cgbp + 72 + iqlibc.ppBoolUintptr(aaj%iqlibc.ppInt32FromInt32(3) == iqlibc.ppInt32FromInt32(0)) + iqlibc.ppBoolUintptr(aaj < iqlibc.ppInt32FromInt32(6))
			if aaj < ppint32(6) {
				ccv14 = ppint32(17)
			} else {
				ccv14 = ppint32(63)
			}
			aabase = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt32(ccv14))
			if aabase == ppint32(1) {
				aabase = 0
			}
			aares = Xmpfr_strtofr(cgtls, cgbp, aap, cgbp+64, aabase, ppint32(ecMPFR_RNDN))

			if ccv16 = aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))) != 0; !ccv16 {
				if aaj%ppint32(3) != ppint32(2) {
					ccv15 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0)
				} else {
					ccv15 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)
				}
			}
			if ccv16 || ccv15 != 0 {

				Xprintf(cgtls, "Error for setting \"%s\" in base %d\n s=\"%s\"\n x=\x00", iqlibc.ppVaList(cgbp+96, aap, aabase, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_13
		cg_13:
			;
			aaj++
		}

		goto cg_10
	cg_10:
		;
		aai++
	}
	aares = Xmpfr_strtofr(cgtls, cgbp, "INFANITY\x00", cgbp+64, ppint32(8), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "ANITY\x00") != 0 {

		Xprintf(cgtls, "Error for setting INFINITY (2)\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+96, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_strtofr(cgtls, cgbp, "@INF@*2\x00", cgbp+64, ppint32(11), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "*2\x00") != 0 {

		Xprintf(cgtls, "Error for setting INFINITY (3)\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+96, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	/* Check Zero */
	aares = Xmpfr_strtofr(cgtls, cgbp, " 00000\x00", cgbp+64, ppint32(11), ppint32(ecMPFR_RNDN))
	if aares != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))) != 0 {

		Xprintf(cgtls, "Error for setting ZERO (1)\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+96, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	/* Check base 62 */
	aares = Xmpfr_strtofr(cgtls, cgbp, "A\x00", iqlibc.ppUintptrFromInt32(0), ppint32(62), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(10)), 0) != 0 {

		Xprintf(cgtls, "Error for setting 'A' in base 62\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_strtofr(cgtls, cgbp, "a\x00", iqlibc.ppUintptrFromInt32(0), ppint32(62), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(36)), 0) != 0 {

		Xprintf(cgtls, "Error for setting 'a' in base 62\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_strtofr(cgtls, cgbp, "Z\x00", iqlibc.ppUintptrFromInt32(0), ppint32(62), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(35)), 0) != 0 {

		Xprintf(cgtls, "Error for setting 'Z' in base 62\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_strtofr(cgtls, cgbp, "z\x00", iqlibc.ppUintptrFromInt32(0), ppint32(62), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(61)), 0) != 0 {

		Xprintf(cgtls, "Error for setting 'z' in base 62\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_strtofr(cgtls, cgbp, "ZA\x00", iqlibc.ppUintptrFromInt32(0), ppint32(62), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2180)), 0) != 0 {

		Xprintf(cgtls, "Error for setting 'ZA' in base 62\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_strtofr(cgtls, cgbp, "za\x00", iqlibc.ppUintptrFromInt32(0), ppint32(62), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(3818)), 0) != 0 {

		Xprintf(cgtls, "Error for setting 'za' in base 62\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_strtofr(cgtls, cgbp, "aZ\x00", iqlibc.ppUintptrFromInt32(0), ppint32(62), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2267)), 0) != 0 {

		Xprintf(cgtls, "Error for setting 'aZ' in base 62\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_strtofr(cgtls, cgbp, "Az\x00", iqlibc.ppUintptrFromInt32(0), ppint32(62), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(681)), 0) != 0 {

		Xprintf(cgtls, "Error for setting 'Az' in base 62\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}

	/* Check base 60 */
	aares = Xmpfr_strtofr(cgtls, cgbp, "Aa\x00", iqlibc.ppUintptrFromInt32(0), ppint32(60), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(636)), 0) != 0 {

		Xprintf(cgtls, "Error for setting 'Aa' in base 60\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_strtofr(cgtls, cgbp, "Zz\x00", cgbp+64, ppint32(60), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(35)), 0) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "z\x00") != 0 {

		Xprintf(cgtls, "Error for setting 'Zz' in base 60\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}

	/* Check base 61 */
	aares = Xmpfr_strtofr(cgtls, cgbp, "z\x00", cgbp+64, ppint32(61), ppint32(ecMPFR_RNDN))
	if aares != 0 || Xmpfr_sgn(cgtls, cgbp) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "z\x00") != 0 {

		Xprintf(cgtls, "Error for setting 'z' in base 61\n x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
}

/* The following RefTable has been generated by this following code */

type tsdymmy_test = struct {
	fdprec   tnmpfr_prec_t
	fdbase   ppint32
	fdstr    ppuintptr
	fdbinstr ppuintptr
}

/* The following RefTable has been generated by this following code */

var siRefTable = [100]tsdymmy_test{
	0: {
		fdprec:   ppint64(39),
		fdbase:   ppint32(20),
		fdstr:    "1.1c9jeh9jg12d8iiggf26b8ce2cig24agai51d9@1445\x00",
		fdbinstr: "1.00111010111010001110110001101011101011e6245\x00",
	},
	1: {
		fdprec:   ppint64(119),
		fdbase:   ppint32(3),
		fdstr:    "1.2210112120221020220021000020101121120011021202212101222000011110211211122222001001221110102220122021121021101010120101e-5655\x00",
		fdbinstr: "1.1111101110011110001101011100011000011100001011011100010011010010001000000111001010000001110111010100011000110010000000e-8963\x00",
	},
	2: {
		fdprec:   ppint64(166),
		fdbase:   ppint32(18),
		fdstr:    "3.ecg67g31434b74d8hhbe2dbbb46g9546cae72cae0cfghfh00ed7gebe9ca63b47h08bgbdeb880a76dea12he31e1ccd67e9dh22a911b46h517b745169b2g43egg2e4eah820cdb2132d6a4f9c63505dd4a0dafbc@-5946\x00",
		fdbinstr: "1.011110010000110011111011111100110110010110000010100001101111111000010000011111110101100000010110011001100000010001100101000001101000010010001011001011000110100011001e-24793\x00",
	},
	3: {
		fdprec:   ppint64(139),
		fdbase:   ppint32(4),
		fdstr:    "1.020302230021023320300300101212330121100031233000032101123133120221012000000000000000000000000000000000000000000000000000000000000000000000e11221\x00",
		fdbinstr: "1.001000110010101100001001001011111000110000110000010001100110111100011001010000001101101111000000001110010001011011011111011000101001000110e22442\x00",
	},
	4: {
		fdprec:   ppint64(126),
		fdbase:   ppint32(13),
		fdstr:    "4.a3cb351c6c548a0475218519514c6c54366681447019ac70a387862c39c86546ab27608c9c2863328860aa2464288070a76c0773882728c5213a335289259@2897\x00",
		fdbinstr: "1.01011010000001110001100001101111100111011010010111000011000101111011000100001010010100110111101001001001000000011100010000000e10722\x00",
	},
	5: {
		fdprec:   ppint64(6),
		fdbase:   ppint32(26),
		fdstr:    "1.j79f6@-1593\x00",
		fdbinstr: "1.00000e-7487\x00",
	},
	6: {
		fdprec:   ppint64(26),
		fdbase:   ppint32(18),
		fdstr:    "3.5e99682hh310aa89hb2fb4h88@-5704\x00",
		fdbinstr: "1.0110010100010101000101100e-23784\x00",
	},
	7: {
		fdprec:   ppint64(12),
		fdbase:   ppint32(21),
		fdstr:    "4.j7f3e2ccdfa@-3524\x00",
		fdbinstr: "1.10110101011e-15477\x00",
	},
	8: {
		fdprec:   ppint64(38),
		fdbase:   ppint32(28),
		fdstr:    "o.agr0m367b9bmm76rplg7b53qlj7f02g717cab@6452\x00",
		fdbinstr: "1.1001010011101100110100111000111010001e31021\x00",
	},
	9: {
		fdprec:   ppint64(75),
		fdbase:   ppint32(17),
		fdstr:    "4.00abd9gc99902e1cae2caa7647gcc029g01370e96d3f8e9g02f814d3ge5faa29d40b9db470@5487\x00",
		fdbinstr: "1.11100000110101010111101001110001001010111111010100000100001010100111011101e22429\x00",
	},
	10: {
		fdprec:   ppint64(91),
		fdbase:   ppint32(16),
		fdstr:    "1.0a812a627160014a3bda1f00000000000000000000000000000000000000000000000000000000000000000000@7897\x00",
		fdbinstr: "1.000010101000000100101010011000100111000101100000000000010100101000111011110110100001111100e31588\x00",
	},
	11: {
		fdprec:   ppint64(154),
		fdbase:   ppint32(19),
		fdstr:    "1.989279dda02a8ic15e936ahig3c695f6059a3i01b7d1ge6a418bf84gd87e36061hb2bi62ciagcgb9226fafea41d2ig1e2f0a10ea3i40d6dahf598bdbh372bdf5901gh276866804ah53b6338bi@5285\x00",
		fdbinstr: "1.110101101101101111110010001011110001100000010100011101101001000100110100000011110111000011011101011110010100110101011011111100101101001100000101101000010e22450\x00",
	},
	12: {
		fdprec:   ppint64(53),
		fdbase:   ppint32(2),
		fdstr:    "1.0100010111100111001010000100011011111011011100110111e-20319\x00",
		fdbinstr: "1.0100010111100111001010000100011011111011011100110111e-20319\x00",
	},
	13: {
		fdprec:   ppint64(76),
		fdbase:   ppint32(3),
		fdstr:    "2.101212121100222100012112101120011222102000021110201110000202111122221100001e1511\x00",
		fdbinstr: "1.000110101010111000011001011111110000001001101001011011111110111111010000111e2396\x00",
	},
	14: {
		fdprec:   ppint64(31),
		fdbase:   ppint32(9),
		fdstr:    "1.171774371505084376877631528681e3258\x00",
		fdbinstr: "1.110101101011111011111000110011e10327\x00",
	},
	15: {
		fdprec:   ppint64(175),
		fdbase:   ppint32(8),
		fdstr:    "4.506242760242070533035566017365410474451421355546570157251400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e3483\x00",
		fdbinstr: "1.001010001100101000101111100000101000100001110001010110110000111011011101100000011110111101011000010001001111001001010011000100010111011011011001101011110000011011110101010011e10451\x00",
	},
	16: {
		fdprec:   ppint64(103),
		fdbase:   ppint32(24),
		fdstr:    "8.0hmlm3g183cj358fn4bimn5bie1l89k95m647474mm8jg5kh1c011gi0m7de9j7b48c595g1bki4n32kll7b882eg7klgga0h0gf11@4510\x00",
		fdbinstr: "1.001000110101001101011010101001111010110100010100110101010101110000001011001101110110010111000101010111e20681\x00",
	},
	17: {
		fdprec:   ppint64(12),
		fdbase:   ppint32(9),
		fdstr:    "3.00221080453e2479\x00",
		fdbinstr: "1.11000111010e7859\x00",
	},
	18: {
		fdprec:   ppint64(86),
		fdbase:   ppint32(11),
		fdstr:    "6.873680186953174a274754118026423965415553a088387303452447389287133a0956111602a5a085446@5035\x00",
		fdbinstr: "1.0000000000110100010110000111010001010100101011000100101010010011101010000110011110001e17421\x00",
	},
	19: {
		fdprec:   ppint64(68),
		fdbase:   ppint32(10),
		fdstr:    "6.1617378719016284192718392572980535262609909598793237475124371481233e481\x00",
		fdbinstr: "1.0110001011000101110010111101100101111110001100001011110011001101111e1600\x00",
	},
	20: {
		fdprec:   ppint64(11),
		fdbase:   ppint32(15),
		fdstr:    "5.ab10c18d45@907\x00",
		fdbinstr: "1.0000101111e3546\x00",
	},
	21: {
		fdprec:   ppint64(77),
		fdbase:   ppint32(26),
		fdstr:    "6.e6kl84g6h30o3nfnj7fjjff4n1ee6e5iop76gabj23e7hgan9o6724domc7bp4hdll95g817519f@5114\x00",
		fdbinstr: "1.1011000101111111111110011011101100000100101000001001100000001011010001001000e24040\x00",
	},
	22: {
		fdprec:   ppint64(28),
		fdbase:   ppint32(27),
		fdstr:    "d.odiqp9kgh84o8d2aoqg4c21hemi@3566\x00",
		fdbinstr: "1.101001111001111111110011110e16959\x00",
	},
	23: {
		fdprec:   ppint64(45),
		fdbase:   ppint32(14),
		fdstr:    "7.cddc6295a576980adbc8c16111d6301bad3146a1143c@-6227\x00",
		fdbinstr: "1.10000000110011000000101100110001011100010111e-23706\x00",
	},
	24: {
		fdprec:   ppint64(54),
		fdbase:   ppint32(19),
		fdstr:    "1.b6e67i2124hfga2g819g1d6527g2b603eg3cd8hhca9gecig8geg1@4248\x00",
		fdbinstr: "1.11010100100010101101110110010100000010111010010101110e18045\x00",
	},
	25: {
		fdprec:   ppint64(49),
		fdbase:   ppint32(20),
		fdstr:    "1.jj68bj6idadg44figi10d2ji99g6ddi6c6ich96a5h86i529@-3149\x00",
		fdbinstr: "1.001011111101100100001010001000011100000000101110e-13609\x00",
	},
	26: {
		fdprec:   ppint64(171),
		fdbase:   ppint32(16),
		fdstr:    "6.22cf0e566d8ff11359d70bd9200065cfd72600b12e00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000@5602\x00",
		fdbinstr: "1.10001000101100111100001110010101100110110110001111111100010001001101011001110101110000101111011001001000000000000001100101110011111101011100100110000000001011000100101110e22410\x00",
	},
	27: {
		fdprec:   ppint64(144),
		fdbase:   ppint32(14),
		fdstr:    "1.425d9709b4c125651ab88bb1a0370c14270d067a9a74a612dad48d5c025531c175c1b905201d0d9773aa686c8249db9c0b841b10821791c02baa2525a4aa7571850439c2cc965cd@-3351\x00",
		fdbinstr: "1.11100111110001001101010111010000101010011000111001101011000001011110101110011011100100111001101101111011001001101011001101001011011101101111011e-12759\x00",
	},
	28: {
		fdprec:   ppint64(166),
		fdbase:   ppint32(6),
		fdstr:    "3.324252232403440543134003140400220120040245215204322153511143504542403430152410543444455151104314552352030352125540101550151410414122051500201022252511512332523431554e8340\x00",
		fdbinstr: "1.010101111101111101001001110010111110010000001111010101100110011011010110011001001100001111010101100000010111011111101110110111101110010001110001111000001010001111000e21560\x00",
	},
	29: {
		fdprec:   ppint64(141),
		fdbase:   ppint32(24),
		fdstr:    "2.i3c88lkm2958l9ncb9f85kk35namjli84clek5j6jjkli82kb9m4e4i2g39me63db2094cif80gcba8ie6l15ia0d667kn9i1f77bdak599e1ach0j05cdn8kf6c6kfd82j2k6hj2c4d@4281\x00",
		fdbinstr: "1.10011100001010110111001000000000101011100010101011001010001101110100110111011000111101000001111101100000110100100010101011001100100011001011e19629\x00",
	},
	30: {
		fdprec:   ppint64(84),
		fdbase:   ppint32(6),
		fdstr:    "2.41022133512503223022555143021524424430350133500020112434301542311050052304150111243e982\x00",
		fdbinstr: "1.11010001111111001010011100011000011100100111111010001111010010101001001000011100001e2539\x00",
	},
	31: {
		fdprec:   ppint64(56),
		fdbase:   ppint32(9),
		fdstr:    "1.5305472255016741401411184703518332515066156086511016413e2936\x00",
		fdbinstr: "1.0111110010001101000000110101110000110101001011001100111e9307\x00",
	},
	32: {
		fdprec:   ppint64(18),
		fdbase:   ppint32(8),
		fdstr:    "3.63542400000000000e-599\x00",
		fdbinstr: "1.11100111011000101e-1796\x00",
	},
	33: {
		fdprec:   ppint64(111),
		fdbase:   ppint32(13),
		fdstr:    "8.b693ac7a24679b98708a0057a6202c867bc146740ab1971b380756a24c99804b63436419239ba0510030b819933771a636c57c5747b883@-6160\x00",
		fdbinstr: "1.01011011111110100101110010100100000110000011011101001110010110000011101110111111010111000011011101101001100100e-22792\x00",
	},
	34: {
		fdprec:   ppint64(162),
		fdbase:   ppint32(16),
		fdstr:    "4.f2abe958a313566adbf3169e55cdcff3785dbd5c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000@382\x00",
		fdbinstr: "1.00111100101010101111101001010110001010001100010011010101100110101011011011111100110001011010011110010101011100110111001111111100110111100001011101101111010101110e1530\x00",
	},
	35: {
		fdprec:   ppint64(117),
		fdbase:   ppint32(23),
		fdstr:    "2.4b6kk3ag3if217ih1hggkk69bmcecfil1cd38dijh35j8e6ckhd335a4gj7l05bedk19473i8449b1ajc3jd3ka95eceheh72lh2jh17jamlm1142gll@-3628\x00",
		fdbinstr: "1.10010010001010001110011000010000011111011101111100110101100100101111101110010011101001111010100010001111110100101111e-16411\x00",
	},
	36: {
		fdprec:   ppint64(179),
		fdbase:   ppint32(2),
		fdstr:    "1.1101101011111010101000110101010101101110001011011010101001110111011010011110001000000110101100010010001110010110011000000110001011111001011110100011101000110001001000110100100110e14203\x00",
		fdbinstr: "1.1101101011111010101000110101010101101110001011011010101001110111011010011110001000000110101100010010001110010110011000000110001011111001011110100011101000110001001000110100100110e14203\x00",
	},
	37: {
		fdprec:   ppint64(18),
		fdbase:   ppint32(27),
		fdstr:    "4.ll743n2f654gh3154@-6039\x00",
		fdbinstr: "1.01101001111010011e-28713\x00",
	},
	38: {
		fdprec:   ppint64(178),
		fdbase:   ppint32(15),
		fdstr:    "1.e5443105cad2d014b700c42aa3de854c4b95322420695d07db3564ec07473da83bde123b74c794139265a838ebeca745ad3dc97d7c356271ca935ea8e83306562c2a8edc6e886c1b6b2d3e17038379c33826526770985c068@821\x00",
		fdbinstr: "1.011100001000101100111111111111000100110111110011101010001111011001111101111001010011100100100101100011101001000000101001010100011111001011001010011101101001000111111010101101011e3208\x00",
	},
	39: {
		fdprec:   ppint64(161),
		fdbase:   ppint32(22),
		fdstr:    "2.46ikji624bg042877h8g2jdki4ece6ede62841j7li843a4becdkkii86c54192jkefehikkb3kcb26ij1b3k9agfbb07dih88d6ej0ee0d63i8hedc7f0g0i9g7jf9gf6423j70h421bg5hf2bja9j0a432lb10@-5125\x00",
		fdbinstr: "1.0111011000111110000010011100001100100110001011101001011110111010100000011100000010011101011100101100111100110000001101010101011110100011101111001011001111100000e-22854\x00",
	},
	40: {
		fdprec:   ppint64(62),
		fdbase:   ppint32(19),
		fdstr:    "7.bgd1g0886a6c3a9ee67cc7g3bgf718i98d90788idi5587358e660iffc0ic6@3257\x00",
		fdbinstr: "1.0101100100001110000100010110100100000111110001111001011110100e13838\x00",
	},
	41: {
		fdprec:   ppint64(127),
		fdbase:   ppint32(19),
		fdstr:    "1.413bgf99eidba75ged25f7187080bce3h7ebdeghea4ig6c79g94di7b42a3e4cdi4ic6a53i71d2e4hdbe50ih0a0egf2fi469732131ig6g496bf7h8g3c86ie7h@-4465\x00",
		fdbinstr: "1.001101111000011011100010010010010110111001001001110011110101111111000001110101111110001110010000110011111101000011000101111101e-18967\x00",
	},
	42: {
		fdprec:   ppint64(17),
		fdbase:   ppint32(21),
		fdstr:    "4.7d5b70gh4k0gj4fj@-116\x00",
		fdbinstr: "1.1000100010000110e-508\x00",
	},
	43: {
		fdprec:   ppint64(141),
		fdbase:   ppint32(13),
		fdstr:    "2.2b4988c5cb57072a6a1a9c42224794a1cbc175a9bc673bb28aa045c3182b9396ca8bb8590969672b0239608a845a2c35c08908a58c2a83748c89241a6561422c7cc4866c8454@4358\x00",
		fdbinstr: "1.10010110101000001000001001111001000100111110100010100110111011111011010010101000110101110000111100010000101101000110000000000001111110110011e16127\x00",
	},
	44: {
		fdprec:   ppint64(39),
		fdbase:   ppint32(7),
		fdstr:    "3.00350342452505221136410100232265245244e202\x00",
		fdbinstr: "1.10011000111110011010100110101101010010e568\x00",
	},
	45: {
		fdprec:   ppint64(119),
		fdbase:   ppint32(24),
		fdstr:    "5.2aene587kc2d9a55mm8clhn4dn0a551de58b1fcli8e8hf1jlm7i0376dl5fhb2k8acka03077mnbn9d4dmi0641dce871c81g2b3ge76m3kngm4a9g5gh@-892\x00",
		fdbinstr: "1.0111101010010100001001111110000000100101110010010111111100100101100001010010100110111000101100101010111000101111000010e-4088\x00",
	},
	46: {
		fdprec:   ppint64(41),
		fdbase:   ppint32(14),
		fdstr:    "5.c3dc5c49373d0c0075624931133022185bd08b16@-5294\x00",
		fdbinstr: "1.0101011000010111111111000010100110011111e-20154\x00",
	},
	47: {
		fdprec:   ppint64(41),
		fdbase:   ppint32(6),
		fdstr:    "3.2411143454422033245255450304104450302500e2250\x00",
		fdbinstr: "1.1110111101010101001001100000100011110111e5817\x00",
	},
	48: {
		fdprec:   ppint64(17),
		fdbase:   ppint32(13),
		fdstr:    "3.65789aa26aa273b1@-4490\x00",
		fdbinstr: "1.1100011101010111e-16614\x00",
	},
	49: {
		fdprec:   ppint64(10),
		fdbase:   ppint32(26),
		fdstr:    "1.5p4hag1fl@6017\x00",
		fdbinstr: "1.110010111e28282\x00",
	},
	50: {
		fdprec:   ppint64(130),
		fdbase:   ppint32(11),
		fdstr:    "2.606a72601843700427667823172635a47055021a0a68a99326875195a179483948407aa13726244552332114a1784aaa7239956521604460876871a65708458aa@-6285\x00",
		fdbinstr: "1.110001001110111110110111000010101000110010011110010101100100001000101011010010000001000101000110111111110101000100000111100010100e-21742\x00",
	},
	51: {
		fdprec:   ppint64(29),
		fdbase:   ppint32(20),
		fdstr:    "j.4356d9b7i38i955jjj1j442501bj@163\x00",
		fdbinstr: "1.1010101011110011100000100100e708\x00",
	},
	52: {
		fdprec:   ppint64(140),
		fdbase:   ppint32(21),
		fdstr:    "9.2f5k7aid6bj2b2g5bff29i73hk3a8d8g0i7ifa07hkb79g4hd3c7j6g4hjj2jbhai01gkje3h9g3gj3i34f0194kaed32iea9dcgcj8h7i1khdkf965c1ak97gf3h03fcab3ggi03fa@4864\x00",
		fdbinstr: "1.0101011100011101000110101001010011111111010011000111111111100000011011100111010001100101100110001110001001100101001100110000011110100101101e21367\x00",
	},
	53: {
		fdprec:   ppint64(133),
		fdbase:   ppint32(13),
		fdstr:    "2.3721a9107307a71c75c07c83b70a25a9853619030b5bcb55101ca5c2060bca46c331b92b33aa957c3ac7c817335287c6917999c38c3806b6b5919623023ac52063bb@6602\x00",
		fdbinstr: "1.011001101111100001100100110100010100010011100010111110110100100000000010011101001011000100000110011011101001010010011110111100010010e24431\x00",
	},
	54: {
		fdprec:   ppint64(118),
		fdbase:   ppint32(2),
		fdstr:    "1.001010111011011000100010001110111000001100101000101101010001110110000111101110111011011101111100110010000101001001001e18960\x00",
		fdbinstr: "1.001010111011011000100010001110111000001100101000101101010001110110000111101110111011011101111100110010000101001001001e18960\x00",
	},
	55: {
		fdprec:   ppint64(102),
		fdbase:   ppint32(23),
		fdstr:    "l.26lhk42clcm9g940eihakhi32gb3331lld488cf1j4f73ge051bfl8gcmcg78gkjc2iibjf752eag0dee6dafa97k79jlh11j3270@-2160\x00",
		fdbinstr: "1.01101011011000100101110111110001011000101101011001011111001101000110111010000010011111101110101100010e-9767\x00",
	},
	56: {
		fdprec:   ppint64(156),
		fdbase:   ppint32(18),
		fdstr:    "b.eb927dd4g48abee3cc2begehb9c3b8h83cae152db850ac2f3g816d6787825122c8h3aa3g8023h23000a8hg61065b3e367ac59ca373067730f96dd0d3b73b3c43fef91750b333gd497b8ce9228e7@5504\x00",
		fdbinstr: "1.11000110111100011101100011001001110011101100011111010100101110010010010011111001100000011010011111111011001011111010001001011001110001100001101000000110000e22954\x00",
	},
	57: {
		fdprec:   ppint64(158),
		fdbase:   ppint32(5),
		fdstr:    "3.0112043214104344433122444210142004032004444213123303302023242414000243311324332203224340334422234000104132020124210141322013240010134130441413233111204013422e-10468\x00",
		fdbinstr: "1.1001011000111111110100100101110011100001110100101001101110011001101001101011010010111010111111101010100011100010101100110111011101000110110100000111001100011e-24305\x00",
	},
	58: {
		fdprec:   ppint64(7),
		fdbase:   ppint32(9),
		fdstr:    "2.141540e-146\x00",
		fdbinstr: "1.001111e-462\x00",
	},
	59: {
		fdprec:   ppint64(111),
		fdbase:   ppint32(5),
		fdstr:    "3.21233234110011204313222402442032333320324004133424222041314021020411320312421014434003440431230413141402230403e7641\x00",
		fdbinstr: "1.10010000000101010000101010101011011010000100010010010000010110001111000111111111000110111001100101101110101101e17743\x00",
	},
	60: {
		fdprec:   ppint64(76),
		fdbase:   ppint32(13),
		fdstr:    "7.1c0861453a4ac156b6119ba7548251b5cb00b7c409c2bb8138214676468c9949676226013c1@4639\x00",
		fdbinstr: "1.001000011000000011101101101010100010010001010111100110010101111110110010111e17169\x00",
	},
	61: {
		fdprec:   ppint64(6),
		fdbase:   ppint32(25),
		fdstr:    "c.aj660@-6978\x00",
		fdbinstr: "1.11000e-32402\x00",
	},
	62: {
		fdprec:   ppint64(156),
		fdbase:   ppint32(3),
		fdstr:    "2.22101000022222000012110122210202211110020121210120112102122121111210000211020001020201202200011021211102012110220222110022001121011022011202000110120021012e-14744\x00",
		fdbinstr: "1.11010001111000101111110000010011001101000100010010110011100100110001100111011101011111111100011111001100001111100101100000001000001100000000010010001011101e-23368\x00",
	},
	63: {
		fdprec:   ppint64(7),
		fdbase:   ppint32(23),
		fdstr:    "1.4hclk2@2148\x00",
		fdbinstr: "1.110110e9716\x00",
	},
	64: {
		fdprec:   ppint64(69),
		fdbase:   ppint32(11),
		fdstr:    "2.77684920493191632416690544493465617a187218365952a6740034288687745a26@3263\x00",
		fdbinstr: "1.01111000111000001111001110000110000110001111110011101100101111011100e11289\x00",
	},
	65: {
		fdprec:   ppint64(146),
		fdbase:   ppint32(21),
		fdstr:    "3.agg4d0dj636d526d4i8643ch5jee4ge2c3i46k121857dbedagd98cjifaf0fgc09ca739g2fkfbfh06i687kic2kb8c7i48gda57bb6d9bh81eh49h0d8e3i7ad2kgb1ek86b86g3589k27d@3562\x00",
		fdbinstr: "1.0010111111111100101010101010001100110101010011011100001110111000101101001110001110010100000001010001000111010000010011110100010010101100101000001e15647\x00",
	},
	66: {
		fdprec:   ppint64(20),
		fdbase:   ppint32(3),
		fdstr:    "1.2000000021102111102e-16642\x00",
		fdbinstr: "1.1011101011111110000e-26377\x00",
	},
	67: {
		fdprec:   ppint64(68),
		fdbase:   ppint32(13),
		fdstr:    "1.a43205b2164676727806614acc0398925569c3962a3ba419881a5c63b651aa3ab46@-618\x00",
		fdbinstr: "1.1111011000001110010100111000110010110110011001110001100101011111000e-2287\x00",
	},
	68: {
		fdprec:   ppint64(129),
		fdbase:   ppint32(4),
		fdstr:    "2.22033002012102010122130132103000303000120122313322000222121000300000000000000000000000000000000000000000000000000000000000000000e13222\x00",
		fdbinstr: "1.01010001111000010000110010010000100011010011100011110010011000000110011000000011000011010110111111010000000101010011001000000110e26445\x00",
	},
	69: {
		fdprec:   ppint64(22),
		fdbase:   ppint32(6),
		fdstr:    "1.420033001013011530142e11704\x00",
		fdbinstr: "1.001000110010110110001e30255\x00",
	},
	70: {
		fdprec:   ppint64(108),
		fdbase:   ppint32(6),
		fdstr:    "1.03345424443433104422104400512453214240453335230205104304115343030341144544051005432030344054100542125304500e7375\x00",
		fdbinstr: "1.00101101110001011101101111000010101011101000001111001110001101100000111100010101010101101100011110111010000e19064\x00",
	},
	71: {
		fdprec:   ppint64(91),
		fdbase:   ppint32(27),
		fdstr:    "2.ao077kf8oqoihn5pm6f5eqdcgnd2132d7p6n7di8ep82a1a9be99pm36g1emacbenaeiqphpgpdjhmm9ke3pn4pdea@-5482\x00",
		fdbinstr: "1.111101100001000011101010001000000111000100100111110010101101110001101101101101101010111110e-26066\x00",
	},
	72: {
		fdprec:   ppint64(96),
		fdbase:   ppint32(9),
		fdstr:    "8.25805186310703506315505842015248775712246416686874260383701323213202658278523870037877823670166e-8134\x00",
		fdbinstr: "1.11010111111000011100111001011010001110010001011101011101110101000101100100100010110011001010000e-25782\x00",
	},
	73: {
		fdprec:   ppint64(161),
		fdbase:   ppint32(16),
		fdstr:    "7.3a7627c1e42ef738698e292f0b81728c4b14fe8c000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000@-3342\x00",
		fdbinstr: "1.1100111010011101100010011111000001111001000010111011110111001110000110100110001110001010010010111100001011100000010111001010001100010010110001010011111110100011e-13366\x00",
	},
	74: {
		fdprec:   ppint64(90),
		fdbase:   ppint32(3),
		fdstr:    "2.10212200011211012002002221112120210222002020100202111000211012122020011102022112222021001e-3447\x00",
		fdbinstr: "1.11100010111011011000101111110001000101000111110001100001010111101101011011110001000010001e-5463\x00",
	},
	75: {
		fdprec:   ppint64(100),
		fdbase:   ppint32(27),
		fdstr:    "a.f81hjjakdnc021op6ffh530ec8ige6n2fqc8f8j7ia7qelebgqkm4ic5ohh652hq1kgpag6pp0ldin6ce1fg6mj34077f5qc5oe@6576\x00",
		fdbinstr: "1.011101001010010011110001100011111111010001110110100100101001010000101011101011110010010011111100000e31271\x00",
	},
	76: {
		fdprec:   ppint64(152),
		fdbase:   ppint32(16),
		fdstr:    "e.37ac48a0303f903c9d20883eddea4300d1190000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000@-1388\x00",
		fdbinstr: "1.1100011011110101100010010001010000000110000001111111001000000111100100111010010000010001000001111101101110111101010010000110000000011010001000110010000e-5549\x00",
	},
	77: {
		fdprec:   ppint64(106),
		fdbase:   ppint32(20),
		fdstr:    "1.3g2h7i2776d50gjibi937f8cdci3idecdeh3j2gba0j8d1ghgg3eg609ji55h5g7jeai1bii3a4f9jhjfij6jd1g3cg0f6024e252gc3e@6422\x00",
		fdbinstr: "1.100110000101011010100111100110000000100101000110110011010010000101000100110010001110011110111100010000111e27755\x00",
	},
	78: {
		fdprec:   ppint64(23),
		fdbase:   ppint32(17),
		fdstr:    "9.f72e724454d1g0f60g93g6@-6563\x00",
		fdbinstr: "1.0011100011110110010001e-26823\x00",
	},
	79: {
		fdprec:   ppint64(98),
		fdbase:   ppint32(6),
		fdstr:    "1.2444223304453415235424343034030405514010421403514410005234221430055051205523402412055242134042045e-8535\x00",
		fdbinstr: "1.1101110011010001101001001111100101111010100111001011110001000010100101101110011011101100000111011e-22063\x00",
	},
	80: {
		fdprec:   ppint64(4),
		fdbase:   ppint32(18),
		fdstr:    "1.gec@-6711\x00",
		fdbinstr: "1.100e-27984\x00",
	},
	81: {
		fdprec:   ppint64(69),
		fdbase:   ppint32(24),
		fdstr:    "8.d45gdfnhkhb7a20nj96dnggic83imhjne0cceldechn1m4e9fbd9db0ablngjf9n7810@6975\x00",
		fdbinstr: "1.00100111111100101100110011110110110000110110110010100101011111000100e31983\x00",
	},
	82: {
		fdprec:   ppint64(122),
		fdbase:   ppint32(8),
		fdstr:    "4.0227760456667717717077553523466457265600000000000000000000000000000000000000000000000000000000000000000000000000000000000e-1767\x00",
		fdbinstr: "1.0000001001011111111000010010111011011011111100111111100111100011111110110101110101001110011011010010111101011010111000000e-5299\x00",
	},
	83: {
		fdprec:   ppint64(144),
		fdbase:   ppint32(23),
		fdstr:    "8.b01c48dg20bek9a5k376clc501aecg92bdjaeji2dm9230i7j3k36jm50b0c5a0753i2b18534cji34bcl2li033cc534m52k2gbegc25a5g30lf4calag58026i5d7li61jg9digj5ceb1@-4456\x00",
		fdbinstr: "1.00010000110011010111011011110111001101111001010110001101011100100101101110110000010011011111100000100110001001001111111011010110000000001111110e-20154\x00",
	},
	84: {
		fdprec:   ppint64(111),
		fdbase:   ppint32(4),
		fdstr:    "2.23100111310122202021232133233012212012232222323230133100000000000000000000000000000000000000000000000000000000e-10458\x00",
		fdbinstr: "1.01011010000010101110100011010100010001001101110011111101111000110100110000110101110101010111011101100011111010e-20915\x00",
	},
	85: {
		fdprec:   ppint64(117),
		fdbase:   ppint32(10),
		fdstr:    "1.61207328685870427963864999744776917701013812304254540861834226053316419217753608451422967376154318603744156166920074e-6440\x00",
		fdbinstr: "1.01100011000100111001100010000000110010100001001011111010100001101111100100101100111010100011101110001010011010010010e-21393\x00",
	},
	86: {
		fdprec:   ppint64(106),
		fdbase:   ppint32(16),
		fdstr:    "1.dd30a1d24091263243ca1c144f0000000000000000000000000000000000000000000000000000000000000000000000000000000@354\x00",
		fdbinstr: "1.110111010011000010100001110100100100000010010001001001100011001001000011110010100001110000010100010011110e1416\x00",
	},
	87: {
		fdprec:   ppint64(77),
		fdbase:   ppint32(14),
		fdstr:    "4.90d6913ba57b149d8d85a58c311b4d537c10bd7d3c10d69c62bc08d32269760126a58115a105@-7311\x00",
		fdbinstr: "1.1001000000111100000111001001011000110101001111100001100111010100010000011111e-27834\x00",
	},
	88: {
		fdprec:   ppint64(8),
		fdbase:   ppint32(4),
		fdstr:    "3.2230000e15197\x00",
		fdbinstr: "1.1101011e30395\x00",
	},
	89: {
		fdprec:   ppint64(81),
		fdbase:   ppint32(24),
		fdstr:    "1.84ni25h558abmhg2dk7bl2jbbmkf4i8i2bemc5cgmk9jf301c00k24271m9h7mgm4301be1lnldn4364@2573\x00",
		fdbinstr: "1.01110010011000110110100101011001011111101111101100010110101101011101100001000010e11797\x00",
	},
	90: {
		fdprec:   ppint64(94),
		fdbase:   ppint32(2),
		fdstr:    "1.010010010101111001001011111111100100011110110100010001101111111100100101101100110101001011111e32427\x00",
		fdbinstr: "1.010010010101111001001011111111100100011110110100010001101111111100100101101100110101001011111e32427\x00",
	},
	91: {
		fdprec:   ppint64(77),
		fdbase:   ppint32(21),
		fdstr:    "1.87e4k9df85g50ead6fcj4h86820ikdjdjg93d90ca406g470hhkh7ciaba1aggg753g36553ebh5@2538\x00",
		fdbinstr: "1.0010001100011000111010000010011001010011000000100101010001100000111101000111e11148\x00",
	},
	92: {
		fdprec:   ppint64(80),
		fdbase:   ppint32(17),
		fdstr:    "1.923gga999230g94fce02efg753ce001045a35e0264c9c2cb17850e32484fc3526dcg38ed874g5f2@3392\x00",
		fdbinstr: "1.0011100111101001001101111001110100001100111110011110110001100110101010111001110e13865\x00",
	},
	93: {
		fdprec:   ppint64(99),
		fdbase:   ppint32(7),
		fdstr:    "4.53646362336126606650465342500160461331562113222506144210636341332436342025203333264316511653025011e-5540\x00",
		fdbinstr: "1.01101101111001001100001101101101010011001001100110111000010000101000011001001001101000011101011001e-15551\x00",
	},
	94: {
		fdprec:   ppint64(119),
		fdbase:   ppint32(20),
		fdstr:    "1.c8966iabcf4de94ad15f9e83j407i3he7fch54h5jh0g5d74e06c057gg72a107didj8d1j8geibbfec5j36c3fgd5e12edjb9g10j7c9i03f33hi80ce0@7153\x00",
		fdbinstr: "1.0101110101100011110001001110100110011000100000001001000110111110011111100011111010011101011111101101010011110111110100e30915\x00",
	},
	95: {
		fdprec:   ppint64(93),
		fdbase:   ppint32(13),
		fdstr:    "2.c749cb562c3a758b1a21a650666a4c6c53c76ca58a1a75a0358c9ac3866887972b3551a03aa6c150856531258508@193\x00",
		fdbinstr: "1.10101111101001011010111101100100111110011111010110111101100100010011001001100011110100111110e715\x00",
	},
	96: {
		fdprec:   ppint64(145),
		fdbase:   ppint32(14),
		fdstr:    "1.c61614b64261d22c62cb9d16163ca4d144ac23351b708506b3b610b1b67b764ca974448d7a2c6515a6bc97503d4b2a530c75b2b677a464c6629c69b6c3d7860d7749b4b653c434d5@2050\x00",
		fdbinstr: "1.111111100001101111100011001111100010010000101000011110000001110100111001011010100001001010111111010001111101000110011000011101110110001001100101e7805\x00",
	},
	97: {
		fdprec:   ppint64(159),
		fdbase:   ppint32(23),
		fdstr:    "4.bj9l07l0215e7l6lf1dkf62i056l37jaa0gdih717656f1kk1a77883jf99jg31le43em76bmcg4lddl782ihkla0m392886d8lm67d6c3a1l4j12kg0l1k52ee77lmk0gech11g8jeei680k85bi460c7el17@-1539\x00",
		fdbinstr: "1.01010100110100100101100001011100000001100011110001001101000010000001000010000110000110010001110100001101011101101001001101101111001101101111101001010010010100e-6960\x00",
	},
	98: {
		fdprec:   ppint64(24),
		fdbase:   ppint32(25),
		fdstr:    "g.m749al09kflg5b42jnn4a7b@-2820\x00",
		fdbinstr: "1.01010010101011010111011e-13092\x00",
	},
	99: {
		fdprec:   ppint64(88),
		fdbase:   ppint32(18),
		fdstr:    "3.5ed0gad0bhhb7aa9ge2ad1dhcg6833f3e068936hghf23gd2aa69f13539f15hfce50aa64achfee49bfg7249g@-4058\x00",
		fdbinstr: "1.001000010110011011000101100000101111101001100011101101001111110111000010010110010001100e-16920\x00",
	},
}

func sicheck_reftable(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aabase, aai ppint32
	var aap tnmpfr_prec_t
	var ccv2 ppbool
	var pp_ /* s at bp+64 */ ppuintptr
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aabase, aai, aap, ccv2

	Xmpfr_init2(cgtls, cgbp, ppint64(200))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(200))
	aai = 0
	for {
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppuintptr(iqunsafe.ppPointer(&siRefTable)) == ppuintptr(iqunsafe.ppPointer(&siRefTable)))), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(655), "(void *) &(RefTable) == (void *) &(RefTable)[0]\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if !(ppint64(aai) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(3200)/iqlibc.ppUint64FromInt64(32))) {
			break
		}

		aabase = siRefTable[aai].fdbase
		aap = siRefTable[aai].fdprec
		Xmpfr_set_prec(cgtls, cgbp, aap)
		Xmpfr_set_prec(cgtls, cgbp+32, aap)
		Xmpfr_set_str_binary(cgtls, cgbp, siRefTable[aai].fdbinstr)
		Xmpfr_strtofr(cgtls, cgbp+32, siRefTable[aai].fdstr, cgbp+64, aabase, ppint32(ecMPFR_RNDN))
		if *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)) == iqlibc.ppUintptrFromInt32(0) || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))) != 0 {

			Xprintf(cgtls, "strtofr didn't parse entire input for i=%d:\n Str=%s\x00", iqlibc.ppVaList(cgbp+80, aai, siRefTable[aai].fdstr))
			Xexit(cgtls, ppint32(1))
		}
		if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

			Xprintf(cgtls, "Results differ between strtofr and set_binary for i=%d:\n Set binary gives: \x00", iqlibc.ppVaList(cgbp+80, aai))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, " strtofr    gives: \x00", 0)
			Xmpfr_dump(cgtls, cgbp+32)
			Xprintf(cgtls, " setstr     gives: \x00", 0)
			Xmpfr_set_str(cgtls, cgbp, siRefTable[aai].fdstr, aabase, ppint32(ecMPFR_RNDN))
			Xmpfr_dump(cgtls, cgbp)
			Xmpfr_set_prec(cgtls, cgbp, ppint64(2)*aap)
			Xmpfr_set_str(cgtls, cgbp, siRefTable[aai].fdstr, aabase, ppint32(ecMPFR_RNDN))
			Xprintf(cgtls, " setstr ++  gives: \x00", 0)
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aai++
	}
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_parse(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aares ppint32
	var ccv1, ccv2 ppbool
	var pp_ /* s at bp+32 */ ppuintptr
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_ = aares, ccv1, ccv2

	Xmpfr_init(cgtls, cgbp)

	/* Invalid data */
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	aares = Xmpfr_strtofr(cgtls, cgbp, "  invalid\x00", iqlibc.ppUintptrFromInt32(0), ppint32(10), ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp != -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Failed parsing '  invalid' (1)\n X=\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aares == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(708), "res == 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	aares = Xmpfr_strtofr(cgtls, cgbp, "  invalid\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp != -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "  invalid\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '  invalid' (2)\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aares == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(717), "res == 0\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	/* Check if it stops correctly */
	Xmpfr_strtofr(cgtls, cgbp, "15*x\x00", cgbp+32, ppint32(10), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(15)), 0) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "*x\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '15*x'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	/* Check for leading spaces */
	Xmpfr_strtofr(cgtls, cgbp, "  1.5E-10 *x^2\x00", cgbp+32, ppint32(10), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "1.5E-10\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), " *x^2\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '1.5E-10*x^2'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	/* Check for leading sign */
	Xmpfr_strtofr(cgtls, cgbp, "  +17.5E-42E \x00", cgbp+32, ppint32(10), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "17.5E-42\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "E \x00") != 0 {

		Xprintf(cgtls, "Failed parsing '+17.5E-42E '\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "-17.5E+42E\n\x00", cgbp+32, ppint32(10), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "-17.5E42\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "E\n\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '-17.5E+42\\n'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	/* P form */
	Xmpfr_strtofr(cgtls, cgbp, "0x42P17\x00", cgbp+32, ppint32(16), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "8650752\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '0x42P17' (base = 16)\n S='%s'\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "-0X42p17\x00", cgbp+32, ppint32(16), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "-8650752\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '-0x42p17' (base = 16)\n S='%s'\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "42p17\x00", cgbp+32, ppint32(16), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "8650752\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '42p17' (base = 16)\n S='%s'\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "-42P17\x00", cgbp+32, ppint32(16), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "-8650752\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '-42P17' (base = 16)\n S='%s'\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "0b1001P17\x00", cgbp+32, ppint32(2), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "1179648\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '0b1001P17' (base = 2)\n S='%s'\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "-0B1001p17\x00", cgbp+32, ppint32(2), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "-1179648\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '-0B1001p17' (base = 2)\n S='%s'\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "1001p17\x00", cgbp+32, ppint32(2), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "1179648\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '1001p17' (base = 2)\n S='%s'\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "-1001P17\x00", cgbp+32, ppint32(2), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "-1179648\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '-1001P17' (base = 2)\n S='%s'\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	/* Check for auto-detection of the base */
	Xmpfr_strtofr(cgtls, cgbp, "+0x42P17\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "42P17\x00", ppint32(16), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '+0x42P17'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "-42E17\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "-42E17\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '-42E17'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "-42P17\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "-42\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "P17\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '-42P17' (base = 0)\n S='%s'\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, " 0b0101.011@42\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "0101.011@42\x00", ppint32(2), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '0101.011@42'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, " 0b0101.011P42\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "0101.011@42\x00", ppint32(2), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '0101.011@42'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "+0x42@17\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "4.2@18\x00", ppint32(16), ppint32(ecMPFR_RNDN)) != 0 || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 {

		Xprintf(cgtls, "Failed parsing '+0x42P17'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}

	/* Check for space inside the mantissa */
	Xmpfr_strtofr(cgtls, cgbp, "+0x4 2@17\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(4)), 0) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), " 2@17\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '+0x4 2@17'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "+0x42 P17\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0x42)), 0) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), " P17\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '+0x42 P17'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	/* Space between mantissa and exponent */
	Xmpfr_strtofr(cgtls, cgbp, " -0b0101P 17\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(5)), 0) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "P 17\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '-0b0101P 17'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	/* Check for Invalid exponent. */
	Xmpfr_strtofr(cgtls, cgbp, " -0b0101PF17\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(5)), 0) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "PF17\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '-0b0101PF17'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	/* At least one digit in the mantissa. */
	Xmpfr_strtofr(cgtls, cgbp, " .E10\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), " .E10\x00") != 0 {

		Xprintf(cgtls, "Failed parsing ' .E10'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	/* Check 2 '.': 2.3.4   */
	Xmpfr_strtofr(cgtls, cgbp, "-1.2.3E4\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "-1.2\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), ".3E4\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '-1.2.3E4'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	/* Check for 0x and 0b */
	Xmpfr_strtofr(cgtls, cgbp, "  0xG\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_sgn(cgtls, cgbp) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "xG\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '  0xG'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "  0b2\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_sgn(cgtls, cgbp) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "b2\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '  0b2'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "-0x.23@2Z33\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(0x23)), 0) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "Z33\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '-0x.23@2Z33'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "  0x\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_sgn(cgtls, cgbp) != 0 || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), "x\x00") != 0 {

		Xprintf(cgtls, "Failed parsing '  0x'\n S=%s\n X=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_overflow(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var pp_ /* s at bp+32 */ ppuintptr
	var pp_ /* x at bp+0 */ tnmpfr_t

	Xmpfr_init(cgtls, cgbp)

	/* Huge overflow */
	Xmpfr_strtofr(cgtls, cgbp, "123456789E2147483646\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check overflow failed (1) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "123456789E9223372036854775807\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check overflow failed (2) with:\n s='%s'\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "This failure is triggered by GCC bug 86554:\n  https://gcc.gnu.org/bugzilla/show_bug.cgi?id=86554\n  https://gcc.gnu.org/bugzilla/show_bug.cgi?id=87276 (about this test)\nWorkaround: disable code hoisting with -fno-code-hoisting in CFLAGS.\n\x00", 0)
		/* Note: In Debian, this error is obtained with gcc-snapshot from
		   20180908-1 to 20181127-1. With gcc-snapshot from 20181209-1 to
		   20190102-1 (at least), the MPFR build no longer seems affected
		   in general, but using --with-gmp-build=... together with
		   --enable-assert still triggers this failure. This bug has been
		   fixed in the GCC trunk rev 267725, thus the future gcc-snapshot
		   versions should no longer have this bug. */
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "123456789E170141183460469231731687303715884105728\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check overflow failed (3) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	/* Limit overflow */
	Xmpfr_strtofr(cgtls, cgbp, "12E2147483646\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check overflow failed (4) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "12E2147483645\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check overflow failed (5) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "0123456789ABCDEF@2147483640\x00", cgbp+32, ppint32(16), ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check overflow failed (6) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "0123456789ABCDEF@540000000\x00", cgbp+32, ppint32(16), ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check overflow failed (7) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "1@2305843009213693951\x00", cgbp+32, ppint32(16), ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check overflow failed (8) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "1@2305843009213693951\x00", cgbp+32, ppint32(17), ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check overflow failed (9) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	/* Check underflow */
	Xmpfr_strtofr(cgtls, cgbp, "123456789E-2147483646\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check underflow failed (1) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "123456789E-9223372036854775807\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check underflow failed (2) with:\n s='%s'\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "-123456789E-170141183460469231731687303715884105728\x00", cgbp+32, 0, ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check underflow failed (3) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "0123456789ABCDEF@-540000000\x00", cgbp+32, ppint32(16), ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check overflow failed (7) with:\n s=%s\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_strtofr(cgtls, cgbp, "1@-2305843009213693952\x00", cgbp+32, ppint32(16), ppint32(ecMPFR_RNDN))
	if ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "Check underflow failed (8) with:\n s='%s'\n x=\x00", iqlibc.ppVaList(cgbp+48, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_retval(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aares ppint32
	var ccv1, ccv2, ccv3 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aares, ccv1, ccv2, ccv3

	Xmpfr_init2(cgtls, cgbp, ppint64(10))

	aares = Xmpfr_strtofr(cgtls, cgbp, "01011000111\x00", iqlibc.ppUintptrFromInt32(0), ppint32(2), ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aares == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1071), "res == 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aares = Xmpfr_strtofr(cgtls, cgbp, "11011000111\x00", iqlibc.ppUintptrFromInt32(0), ppint32(2), ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aares > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1073), "res > 0\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aares = Xmpfr_strtofr(cgtls, cgbp, "110110001101\x00", iqlibc.ppUintptrFromInt32(0), ppint32(2), ppint32(ecMPFR_RNDN))

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aares < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1075), "res < 0\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* Bug found by Christoph Lauter (in mpfr_set_str). */
type tsbug20081025_test = struct {
	fdrnd     tnmpfr_rnd_t
	fdinexact ppint32
	fdstr     ppuintptr
	fdbinstr  ppuintptr
}

// C documentation
//
//	/* Bug found by Christoph Lauter (in mpfr_set_str). */
var siBug20081028Table = [16]tsbug20081025_test{
	0: {
		fdinexact: -ppint32(1),
		fdstr:     "1.00000000000000000006\x00",
		fdbinstr:  "1\x00",
	},
	1: {
		fdrnd:     ppint32(ecMPFR_RNDZ),
		fdinexact: -ppint32(1),
		fdstr:     "1.00000000000000000006\x00",
		fdbinstr:  "1\x00",
	},
	2: {
		fdrnd:     ppint32(ecMPFR_RNDU),
		fdinexact: +iqlibc.ppInt32FromInt32(1),
		fdstr:     "1.00000000000000000006\x00",
		fdbinstr:  "10000000000000000000000000000001e-31\x00",
	},
	3: {
		fdrnd:     ppint32(ecMPFR_RNDD),
		fdinexact: -ppint32(1),
		fdstr:     "1.00000000000000000006\x00",
		fdbinstr:  "1\x00",
	},
	4: {
		fdinexact: +iqlibc.ppInt32FromInt32(1),
		fdstr:     "-1.00000000000000000006\x00",
		fdbinstr:  "-1\x00",
	},
	5: {
		fdrnd:     ppint32(ecMPFR_RNDZ),
		fdinexact: +iqlibc.ppInt32FromInt32(1),
		fdstr:     "-1.00000000000000000006\x00",
		fdbinstr:  "-1\x00",
	},
	6: {
		fdrnd:     ppint32(ecMPFR_RNDU),
		fdinexact: +iqlibc.ppInt32FromInt32(1),
		fdstr:     "-1.00000000000000000006\x00",
		fdbinstr:  "-1\x00",
	},
	7: {
		fdrnd:     ppint32(ecMPFR_RNDD),
		fdinexact: -ppint32(1),
		fdstr:     "-1.00000000000000000006\x00",
		fdbinstr:  "-10000000000000000000000000000001e-31\x00",
	},
	8: {
		fdinexact: +iqlibc.ppInt32FromInt32(1),
		fdstr:     "0.999999999999999999999999999999999999999999999\x00",
		fdbinstr:  "1\x00",
	},
	9: {
		fdrnd:     ppint32(ecMPFR_RNDZ),
		fdinexact: -ppint32(1),
		fdstr:     "0.999999999999999999999999999999999999999999999\x00",
		fdbinstr:  "11111111111111111111111111111111e-32\x00",
	},
	10: {
		fdrnd:     ppint32(ecMPFR_RNDU),
		fdinexact: +iqlibc.ppInt32FromInt32(1),
		fdstr:     "0.999999999999999999999999999999999999999999999\x00",
		fdbinstr:  "1\x00",
	},
	11: {
		fdrnd:     ppint32(ecMPFR_RNDD),
		fdinexact: -ppint32(1),
		fdstr:     "0.999999999999999999999999999999999999999999999\x00",
		fdbinstr:  "11111111111111111111111111111111e-32\x00",
	},
	12: {
		fdinexact: -ppint32(1),
		fdstr:     "-0.999999999999999999999999999999999999999999999\x00",
		fdbinstr:  "-1\x00",
	},
	13: {
		fdrnd:     ppint32(ecMPFR_RNDZ),
		fdinexact: +iqlibc.ppInt32FromInt32(1),
		fdstr:     "-0.999999999999999999999999999999999999999999999\x00",
		fdbinstr:  "-11111111111111111111111111111111e-32\x00",
	},
	14: {
		fdrnd:     ppint32(ecMPFR_RNDU),
		fdinexact: +iqlibc.ppInt32FromInt32(1),
		fdstr:     "-0.999999999999999999999999999999999999999999999\x00",
		fdbinstr:  "-11111111111111111111111111111111e-32\x00",
	},
	15: {
		fdrnd:     ppint32(ecMPFR_RNDD),
		fdinexact: -ppint32(1),
		fdstr:     "-0.999999999999999999999999999999999999999999999\x00",
		fdbinstr:  "-1\x00",
	},
}

func sibug20081028(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aai, aainexact, aares ppint32
	var aarnd tnmpfr_rnd_t
	var ccv2 ppbool
	var ccv3 ppuintptr
	var pp_ /* s at bp+64 */ ppuintptr
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_ = aai, aainexact, aares, aarnd, ccv2, ccv3

	Xmpfr_init2(cgtls, cgbp, ppint64(32))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(32))
	aai = 0
	for {
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppuintptr(iqunsafe.ppPointer(&siBug20081028Table)) == ppuintptr(iqunsafe.ppPointer(&siBug20081028Table)))), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1126), "(void *) &(Bug20081028Table) == (void *) &(Bug20081028Table)[0]\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if !(ppint64(aai) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(384)/iqlibc.ppUint64FromInt64(24))) {
			break
		}

		aarnd = siBug20081028Table[aai].fdrnd
		aainexact = siBug20081028Table[aai].fdinexact
		Xmpfr_set_str_binary(cgtls, cgbp, siBug20081028Table[aai].fdbinstr)
		aares = Xmpfr_strtofr(cgtls, cgbp+32, siBug20081028Table[aai].fdstr, cgbp+64, ppint32(10), aarnd)
		if *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)) == iqlibc.ppUintptrFromInt32(0) || ppint32(*(*ppint8)(iqunsafe.ppPointer(*(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))) != 0 {

			Xprintf(cgtls, "Error in Bug20081028: strtofr didn't parse entire input\nfor (i=%d) Str=\"%s\"\x00", iqlibc.ppVaList(cgbp+80, aai, siBug20081028Table[aai].fdstr))
			Xexit(cgtls, ppint32(1))
		}
		if !(iqlibc.ppBoolInt32(aares > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aares < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0))) {

			if aainexact > 0 {
				ccv3 = "positive\x00"
			} else {
				ccv3 = "negative\x00"
			}
			Xprintf(cgtls, "Error in Bug20081028: expected %s ternary value, got %d\nfor (i=%d) Rnd=%s Str=\"%s\"\n Set binary gives: \x00", iqlibc.ppVaList(cgbp+80, ccv3, aares, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), siBug20081028Table[aai].fdstr))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, " strtofr    gives: \x00", 0)
			Xmpfr_dump(cgtls, cgbp+32)
			Xexit(cgtls, ppint32(1))
		}
		if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

			Xprintf(cgtls, "Error in Bug20081028: Results differ between strtofr and set_binary\nfor (i=%d) Rnd=%s Str=\"%s\"\n Set binary gives: \x00", iqlibc.ppVaList(cgbp+80, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), siBug20081028Table[aai].fdstr))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, " strtofr    gives: \x00", 0)
			Xmpfr_dump(cgtls, cgbp+32)
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aai++
	}
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* check that 1.23e is correctly parsed, cf
//	   https://gmplib.org/list-archives/gmp-bugs/2010-March/001898.html */
func sitest20100310(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var pp_ /* endptr at bp+72 */ ppuintptr
	var pp_ /* str at bp+66 */ [6]ppint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	*(*[6]ppint8)(iqunsafe.ppPointer(cgbp + 66)) = [6]ppint8{'1', '.', '2', '3', 'e'}

	Xmpfr_init2(cgtls, cgbp, ppint64(53))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(53))
	Xmpfr_strtofr(cgtls, cgbp, cgbp+66, cgbp+72, ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_strtofr(cgtls, cgbp+32, "1.23\x00", iqlibc.ppUintptrFromInt32(0), ppint32(10), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "x <> y in test20100310\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 72)) != cgbp+66+ppuintptr(4) { /* strtofr should take into account '1.23',
		   not '1.23e' */

		Xprintf(cgtls, "endptr <> str + 4 in test20100310\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
}

// C documentation
//
//	/* From a bug reported by Joseph S. Myers
//	   https://sympa.inria.fr/sympa/arc/mpfr/2012-08/msg00005.html */
func sibug20120814(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aaemin tnmpfr_exp_t
	var aap ppuintptr
	var aar ppint32
	var pp_ /* e at bp+0 */ tnmpfr_exp_t
	var pp_ /* s at bp+72 */ [64]ppint8
	var pp_ /* x at bp+8 */ tnmpfr_t
	var pp_ /* y at bp+40 */ tnmpfr_t
	pp_, pp_, pp_ = aaemin, aap, aar
	aaemin = ppint64(-ppint32(30))

	Xmpfr_init2(cgtls, cgbp+8, ppint64(2))
	Xmpfr_set_ui_2exp(cgtls, cgbp+8, ppuint64(3), aaemin-ppint64(2), ppint32(ecMPFR_RNDN))
	Xmpfr_get_str(cgtls, cgbp+72+ppuintptr(1), cgbp, ppint32(10), ppuint64(19), cgbp+8, ppint32(ecMPFR_RNDD))
	(*(*[64]ppint8)(iqunsafe.ppPointer(cgbp + 72)))[0] = (*(*[64]ppint8)(iqunsafe.ppPointer(cgbp + 72)))[ppint32(1)]
	(*(*[64]ppint8)(iqunsafe.ppPointer(cgbp + 72)))[ppint32(1)] = ppint8('.')
	aap = cgbp + 72
	for {
		if !(ppint32(*(*ppint8)(iqunsafe.ppPointer(aap))) != 0) {
			break
		}
		goto cg_1
	cg_1:
		;
		aap++
	}
	*(*ppint8)(iqunsafe.ppPointer(aap)) = ppint8('e')
	Xsprintf(cgtls, aap+ppuintptr(1), "%d\x00", iqlibc.ppVaList(cgbp+144, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp)))-ppint32(1)))

	Xmpfr_init2(cgtls, cgbp+40, ppint64(4))
	aar = Xmpfr_strtofr(cgtls, cgbp+40, cgbp+72, iqlibc.ppUintptrFromInt32(0), 0, ppint32(ecMPFR_RNDN))
	if aar <= 0 || !(Xmpfr_equal_p(cgtls, cgbp+8, cgbp+40) != 0) {

		Xprintf(cgtls, "Error in bug20120814\n\x00", 0)
		Xprintf(cgtls, "mpfr_strtofr failed on string \"%s\"\n\x00", iqlibc.ppVaList(cgbp+144, cgbp+72))
		Xprintf(cgtls, "Expected inex > 0 and y = 0.1100E%d\n\x00", iqlibc.ppVaList(cgbp+144, ppint32(aaemin)))
		Xprintf(cgtls, "Got inex = %-6d and y = \x00", iqlibc.ppVaList(cgbp+144, aar))
		Xmpfr_dump(cgtls, cgbp+40)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp+8)
	Xmpfr_clear(cgtls, cgbp+40)
}

func sibug20120829(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aai, aainex1, aainex2, aar ppint32
	var aarnd tnmpfr_rnd_t
	var ccv1, ccv3 ppbool
	var pp_ /* e at bp+64 */ tnmpfr_t
	var pp_ /* s at bp+96 */ [48]ppint8
	var pp_ /* x1 at bp+0 */ tnmpfr_t
	var pp_ /* x2 at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aai, aainex1, aainex2, aar, aarnd, ccv1, ccv3
	*(*[48]ppint8)(iqunsafe.ppPointer(cgbp + 96)) = [48]ppint8{'1', 'e', '-', '1'}

	Xmpfr_init2(cgtls, cgbp+64, ppint64(128))
	Xmpfr_inits2(cgtls, ppint64(4), cgbp, iqlibc.ppVaList(cgbp+152, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

	aainex1 = Xmpfr_set_si_2exp(cgtls, cgbp+64, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex1 == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1238), "inex1 == 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aai = ppint32(1)
	for {
		if !(iqlibc.ppUint64FromInt32(aai) <= iqlibc.ppUint64FromInt64(48)-iqlibc.ppUint64FromInt32(5)) {
			break
		}

		(*(*[48]ppint8)(iqunsafe.ppPointer(cgbp + 96)))[ppint32(3)+aai] = ppint8('0')
		(*(*[48]ppint8)(iqunsafe.ppPointer(cgbp + 96)))[ppint32(4)+aai] = 0
		aainex1 = Xmpfr_mul_ui(cgtls, cgbp+64, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(10)), ppint32(ecMPFR_RNDN))

		if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex1 == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv3 {
			Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1245), "inex1 == 0\x00")
		}
		pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aar = 0
		for {
			if !(aar < ppint32(ecMPFR_RNDF)) {
				break
			}

			aarnd = aar

			aainex1 = Xmpfr_exp10(cgtls, cgbp, cgbp+64, aarnd)
			aainex1 = iqlibc.ppBoolInt32(aainex1 > 0) - iqlibc.ppBoolInt32(aainex1 < 0)
			aainex2 = Xmpfr_strtofr(cgtls, cgbp+32, cgbp+96, iqlibc.ppUintptrFromInt32(0), 0, aarnd)
			aainex2 = iqlibc.ppBoolInt32(aainex2 > 0) - iqlibc.ppBoolInt32(aainex2 < 0)
			/* On 32-bit machines, for i = 7, r8389, r8391 and r8394 do:
			   strtofr.c:...: MPFR assertion failed: cy == 0
			   r8396 is OK.
			   On 64-bit machines, for i = 15,
			   r8389 does: strtofr.c:678: MPFR assertion failed: err < (64 - 0)
			   r8391 does: strtofr.c:680: MPFR assertion failed: h < ysize
			   r8394 and r8396 are OK.
			*/
			if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+32) != 0) || aainex1 != aainex2 {

				Xprintf(cgtls, "Error in bug20120829 for i = %d, rnd = %s\n\x00", iqlibc.ppVaList(cgbp+152, aai, Xmpfr_print_rnd_mode(cgtls, aarnd)))
				Xprintf(cgtls, "Expected inex = %d, x = \x00", iqlibc.ppVaList(cgbp+152, aainex1))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Got      inex = %d, x = \x00", iqlibc.ppVaList(cgbp+152, aainex2))
				Xmpfr_dump(cgtls, cgbp+32)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_4
		cg_4:
			;
			aar++
		}

		goto cg_2
	cg_2:
		;
		aai++
	}

	Xmpfr_clears(cgtls, cgbp+64, iqlibc.ppVaList(cgbp+152, cgbp, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
}

// C documentation
//
//	/* https://sympa.inria.fr/sympa/arc/mpfr/2016-12/msg00043.html
//	   mpfr_strtofr can return an incorrect ternary value.
//	   Note: As a consequence, the value can also be incorrect if the current
//	   exponent range is not the maximum one (since the ternary value is used
//	   to resolve double rounding in mpfr_check_range); this can happen only
//	   if the value is a midpoint between 0 and the minimum positive number
//	   or the opposite. */
func sibug20161217(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aainex ppint32
	var ccv1, ccv3, ccv4, ccv6 ppbool
	var pp_ /* fp at bp+0 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aainex, ccv1, ccv3, ccv4, ccv6
	var snnum = "0.1387778780781445675529539585113525390625e31\x00"

	Xmpfr_init2(cgtls, cgbp, ppint64(110))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(110))

	aainex = Xmpfr_strtofr(cgtls, cgbp, snnum, iqlibc.ppUintptrFromInt32(0), ppint32(10), ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1297), "inex == 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_str_binary(cgtls, cgbp+32, "10001100001000010011110110011101101001010000001011011110010001010100010100100110111101000010001011001100001101E-9\x00")

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp, cgbp+32) != 0)), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1299), "mpfr_equal_p (fp, z)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp, cgbp+32) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* try with 109 bits */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(109))
	aainex = Xmpfr_strtofr(cgtls, cgbp, snnum, iqlibc.ppUintptrFromInt32(0), ppint32(10), ppint32(ecMPFR_RNDN))

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1304), "inex < 0\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_str_binary(cgtls, cgbp+32, "10001100001000010011110110011101101001010000001011011110010001010100010100100110111101000010001011001100001100E-9\x00")

	if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp, cgbp+32) != 0)), ppint64(1)) != 0; !ccv6 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1306), "mpfr_equal_p (fp, z)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp, cgbp+32) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
}

// C documentation
//
//	/* check bug in MPFR 3.1.5 is fixed: cf
//	   https://sympa.inria.fr/sympa/arc/mpfr/2017-03/msg00009.html
//	   Note: same bug as bug20161217. See also the comments of bug20161217;
//	   here, this is a case where the value is incorrect. */
func sibug20170308(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aaemin tnmpfr_exp_t
	var aainex ppint32
	var ccv2, ccv4 ppbool
	var pp_ /* str at bp+0 */ [28]ppint8
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aaemin, aainex, ccv2, ccv4
	/* the following is slightly larger than 2^-1075, thus should be rounded
	   to 0.5*2^-1074, with ternary value < 0 */
	*(*[28]ppint8)(iqunsafe.ppPointer(cgbp)) = [28]ppint8{'2', '.', '4', '7', '0', '3', '2', '8', '2', '2', '9', '2', '0', '6', '2', '3', '2', '7', '2', '0', '8', '9', 'E', '-', '3', '2', '4'}

	aaemin = X__gmpfr_emin
	Xmpfr_init2(cgtls, cgbp+32, ppint64(53))
	Xset_emin(cgtls, ppint64(-ppint32(1073)))
	/* with emin = -1073, the smallest positive number is 0.5*2^emin = 2^(-1074),
	   thus str should be rounded to 2^(-1074) with inex > 0 */
	aainex = Xmpfr_strtofr(cgtls, cgbp+32, cgbp, iqlibc.ppUintptrFromInt32(0), ppint32(10), ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex > 0 && Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, ppuint64(1), ppint64(-ppint32(1074))) == 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1332), "inex > 0 && mpfr_cmp_ui_2exp (z, 1, -1074) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || aainex > 0 && Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, ppuint64(1), ppint64(-ppint32(1074))) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emin(cgtls, ppint64(-ppint32(1074)))
	/* with emin = -1074, str should be rounded to 2^(-1075) with inex < 0 */
	aainex = Xmpfr_strtofr(cgtls, cgbp+32, cgbp, iqlibc.ppUintptrFromInt32(0), ppint32(10), ppint32(ecMPFR_RNDN))

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex < 0 && Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, ppuint64(1), ppint64(-ppint32(1075))) == 0)), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1336), "inex < 0 && mpfr_cmp_ui_2exp (z, 1, -1075) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || aainex < 0 && Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, ppuint64(1), ppint64(-ppint32(1075))) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp+32)
	Xset_emin(cgtls, aaemin)
}

// C documentation
//
//	/* r13299 fails with 8-bit limbs (micro-gmp/8). */
func sibug20181127(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var pp_ /* s at bp+0 */ [35]ppint8
	var pp_ /* x at bp+40 */ tnmpfr_t
	var pp_ /* y at bp+72 */ tnmpfr_t
	*(*[35]ppint8)(iqunsafe.ppPointer(cgbp)) = [35]ppint8{'9', '.', 'Z', '6', 'n', 'r', 'L', 'V', 'S', 'M', 'G', '1', 'b', 'D', 'c', 'C', 'F', '2', 'O', 'N', 'J', 'd', 'X', '@', '-', '1', '8', '3', '2', '9', '5', '5', '2', '5'}

	Xmpfr_inits2(cgtls, ppint64(6), cgbp+40, iqlibc.ppVaList(cgbp+112, cgbp+72, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_ui_2exp(cgtls, cgbp+40, ppuint64(5), ppint64(-ppint32(1073741701)), ppint32(ecMPFR_RNDN))
	Xmpfr_strtofr(cgtls, cgbp+72, cgbp, iqlibc.ppUintptrFromInt32(0), ppint32(58), ppint32(ecMPFR_RNDZ))
	if !(Xmpfr_equal_p(cgtls, cgbp+40, cgbp+72) != 0) {

		Xprintf(cgtls, "Error in bug20181127 on %s (base 58)\n\x00", iqlibc.ppVaList(cgbp+112, cgbp))
		Xprintf(cgtls, "Expected x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp+40)
		Xprintf(cgtls, "Got      y = \x00", 0)
		Xmpfr_dump(cgtls, cgbp+72)
		Xprintf(cgtls, "*Note* In base 58, x ~= \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(58), ppuint64(8), cgbp+40, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp+40, iqlibc.ppVaList(cgbp+112, cgbp+72, iqlibc.ppUintptrFromInt32(0)))
}

func sicoverage(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaemin tnmpfr_exp_t
	var aainex ppint32
	var ccv1, ccv3, ccv4, ccv6 ppbool
	var pp_ /* str3 at bp+0 */ [23]ppint8
	var pp_ /* str31 at bp+23 */ [22]ppint8
	var pp_ /* x at bp+48 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_ = aaemin, aainex, ccv1, ccv3, ccv4, ccv6
	*(*[23]ppint8)(iqunsafe.ppPointer(cgbp)) = [23]ppint8{'1', '@', '-', '2', '1', '1', '2', '0', '0', '9', '1', '3', '0', '0', '7', '2', '4', '0', '6', '8', '9', '2'}
	*(*[22]ppint8)(iqunsafe.ppPointer(cgbp + 23)) = [22]ppint8{'1', '@', '-', '5', '1', '1', '1', '7', '0', '9', '7', '3', '3', '1', '4', '0', '8', '5', '8', '3', '1'}

	/* exercise assertion cy == 0 around line 698 of strtofr.c */
	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, Xmpfr_get_emin_min(cgtls))
	/* emin = -4611686018427387903 on a 64-bit machine */
	Xmpfr_init2(cgtls, cgbp+48, ppint64(1))
	aainex = Xmpfr_strtofr(cgtls, cgbp+48, cgbp, iqlibc.ppUintptrFromInt32(0), ppint32(3), ppint32(ecMPFR_RNDN))
	/* 3^-2112009130072406892 is slightly larger than (2^64)^-52303988630398057
	   thus we should get inex < 0 */

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1384), "inex < 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp+48, ppuint64(1), -iqlibc.ppInt64FromInt64(52303988630398057)*iqlibc.ppInt64FromInt32(64)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1385), "mpfr_cmp_ui_2exp (x, 1, -52303988630398057 * 64) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp+48, ppuint64(1), -iqlibc.ppInt64FromInt64(52303988630398057)*iqlibc.ppInt64FromInt32(64)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aainex = Xmpfr_strtofr(cgtls, cgbp+48, cgbp+23, iqlibc.ppUintptrFromInt32(0), ppint32(31), ppint32(ecMPFR_RNDN))
	/* 31^-511170973314085831 is slightly smaller than (2^64)^-39569396093273623
	   thus we should get inex > 0 */

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1389), "inex > 0\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp+48, ppuint64(1), -iqlibc.ppInt64FromInt64(39569396093273623)*iqlibc.ppInt64FromInt32(64)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv6 {
		Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1390), "mpfr_cmp_ui_2exp (x, 1, -39569396093273623 * 64) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp+48, ppuint64(1), -iqlibc.ppInt64FromInt64(39569396093273623)*iqlibc.ppInt64FromInt32(64)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp+48)
	Xset_emin(cgtls, aaemin)
}

func sirandom_tests(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(1744)
	defer cgtls.ppFree(1744)

	var aa_p tnmpfr_srcptr
	var aabase, aai, aaj, aak, aaneg, aanoteq, aaprec, ccv4, ccv5 ppint32
	var aad, ccv10, ccv11 ppint8
	var aanum_to_text, ccv3 ppuintptr
	var ccv12, ccv7, ccv8, ccv9 ppbool
	var pp_ /* e0 at bp+1696 */ tnmpfr_exp_t
	var pp_ /* e1 at bp+1704 */ tnmpfr_exp_t
	var pp_ /* s0 at bp+0 */ [512]ppint8
	var pp_ /* s1 at bp+512 */ [512]ppint8
	var pp_ /* s2 at bp+1024 */ [576]ppint8
	var pp_ /* x0 at bp+1600 */ tnmpfr_t
	var pp_ /* x1 at bp+1632 */ tnmpfr_t
	var pp_ /* x2 at bp+1664 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aabase, aad, aai, aaj, aak, aaneg, aanoteq, aanum_to_text, aaprec, ccv10, ccv11, ccv12, ccv3, ccv4, ccv5, ccv7, ccv8, ccv9

	aaprec = ppint32(mvMPFR_PREC_MIN)
	for {
		if !(aaprec < ppint32(300)) {
			break
		}

		Xmpfr_inits2(cgtls, ppint64(aaprec), cgbp+1600, iqlibc.ppVaList(cgbp+1720, cgbp+1632, cgbp+1664, iqlibc.ppUintptrFromInt32(0)))

		aai = 0
		for {
			if !(aai < ppint32(20)) {
				break
			}

			aanoteq = 0

			/* We want the same exponent for x0 and its successor x1.
			   This is not possible for precision 1 in base 2. */
			for cgcond := true; cgcond; cgcond = aaprec == ppint32(1) && aabase == ppint32(2) {
				aabase = iqlibc.ppInt32FromUint64(ppuint64(2) + Xrandlimb(cgtls)%ppuint64(61))
			}

			if aabase <= ppint32(36) {
				ccv3 = ppuintptr(iqunsafe.ppPointer(&sinum_to_text36))
			} else {
				ccv3 = ppuintptr(iqunsafe.ppPointer(&sinum_to_text62))
			}
			aanum_to_text = ccv3

			for cgcond := true; cgcond; cgcond = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+1600)).fd_mpfr_exp > iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+1632)).fd_mpfr_exp > iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 1696)) == *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 1704))) {

				/* Let's consider only positive numbers. We should test
				   negative numbers, but they should be built later, just
				   for the test itself. */
				Xtests_default_random(cgtls, cgbp+1600, 0, X__gmpfr_emin, X__gmpfr_emax, ppint32(1))
				{
					aa_p = cgbp + 1600
					ccv4 = Xmpfr_set4(cgtls, cgbp+1632, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
				}
				pp_ = ccv4
				Xmpfr_nextabove(cgtls, cgbp+1632)
				Xmpfr_get_str(cgtls, cgbp, cgbp+1696, aabase, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvBSIZE)-iqlibc.ppInt32FromInt32(1)), cgbp+1600, ppint32(ecMPFR_RNDU))
				Xmpfr_get_str(cgtls, cgbp+512, cgbp+1704, aabase, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvBSIZE)-iqlibc.ppInt32FromInt32(1)), cgbp+1632, ppint32(ecMPFR_RNDD))
			}

			/* 0 < x0 <= (s0,e) <= (s1,e) <= x1 with e = e0 = e1.
			   Let's build a string s2 randomly formed by:
			   - the common prefix of s0 and s1;
			   - some of the following digits of s0 (possibly none);
			   - the next digit of s0 + 1;
			   - some of the following digits of s1 (possibly none).
			   Then 0 < x0 <= (s0,e) < (s2,e) <= (s1,e) <= x1, and with
			   a very high probability that (s2,e) < (s1,e); noteq is
			   set to true in this case.
			   For instance, if:
			     s0 = 123456789
			     s1 = 124012345
			   one can have, e.g.:
			     s2 = 12345734
			     s2 = 123556789
			     s2 = 124
			     s2 = 124012
			   s2 is not taken completely randomly between s0 and s1, but it
			   will be built rather easily, and with the randomness of x0,
			   we should cover all cases, with s2 very close to s0, s2 very
			   close to s1, or not too close to either. */

			aaneg = iqlibc.ppBoolInt32(Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0))
			if aaneg != 0 {
				ccv5 = ppint32('-')
			} else {
				ccv5 = ppint32('+')
			}
			(*(*[576]ppint8)(iqunsafe.ppPointer(cgbp + 1024)))[0] = ppint8(ccv5)
			(*(*[576]ppint8)(iqunsafe.ppPointer(cgbp + 1024)))[ppint32(1)] = ppint8('.')

			aaj = 0
			for {
				if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppint32((*(*[512]ppint8)(iqunsafe.ppPointer(cgbp)))[aaj]) != 0 && ppint32((*(*[512]ppint8)(iqunsafe.ppPointer(cgbp + 512)))[aaj]) != 0)), ppint64(1)) != 0; !ccv7 {
					Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1466), "s0[j] != 0 && s1[j] != 0\x00")
				}
				pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if !(ppint32((*(*[512]ppint8)(iqunsafe.ppPointer(cgbp)))[aaj]) == ppint32((*(*[512]ppint8)(iqunsafe.ppPointer(cgbp + 512)))[aaj])) {
					break
				}
				(*(*[576]ppint8)(iqunsafe.ppPointer(cgbp + 1024)))[aaj+ppint32(2)] = (*(*[512]ppint8)(iqunsafe.ppPointer(cgbp)))[aaj]
				goto cg_6
			cg_6:
				;
				aaj++
			}

			/* k is the position of the first differing digit. */
			aak = aaj

			for aaj < iqlibc.ppInt32FromInt32(mvBSIZE)-iqlibc.ppInt32FromInt32(2) && Xrandlimb(cgtls)%ppuint64(8) != ppuint64(0) {

				if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppint32((*(*[512]ppint8)(iqunsafe.ppPointer(cgbp)))[aaj]) != iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv8 {
					Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1475), "s0[j] != 0\x00")
				}
				pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				(*(*[576]ppint8)(iqunsafe.ppPointer(cgbp + 1024)))[aaj+ppint32(2)] = (*(*[512]ppint8)(iqunsafe.ppPointer(cgbp)))[aaj]
				aaj++
			}

			if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppint32((*(*[512]ppint8)(iqunsafe.ppPointer(cgbp)))[aaj]) != iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv9 {
				Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1480), "s0[j] != 0\x00")
			}
			pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			/* We will increment the next digit. Thus while s0[j] is the
			   maximum digit, go back until this is no longer the case
			   (the first digit after the common prefix cannot be the
			   maximum digit, so that we will stop early enough). */
			for {
				ccv10 = (*(*[512]ppint8)(iqunsafe.ppPointer(cgbp)))[aaj]
				aad = ccv10
				if !(ppint32(ccv10) == ppint32(*(*ppint8)(iqunsafe.ppPointer(aanum_to_text + ppuintptr(aabase-ppint32(1)))))) {
					break
				}
				aaj--
			}
			aanoteq = iqlibc.ppBoolInt32(aaj != aak)
			ccv11 = *(*ppint8)(iqunsafe.ppPointer(Xstrchr(cgtls, aanum_to_text, ppint32(aad)) + iqlibc.ppUintptrFromInt32(1)))
			aad = ccv11
			(*(*[576]ppint8)(iqunsafe.ppPointer(cgbp + 1024)))[aaj+ppint32(2)] = ccv11
			if ppint32(aad) != ppint32((*(*[512]ppint8)(iqunsafe.ppPointer(cgbp + 512)))[aaj]) {
				aanoteq = ppint32(1)
			}
			aaj++

			for aaj < iqlibc.ppInt32FromInt32(mvBSIZE)-iqlibc.ppInt32FromInt32(1) && Xrandlimb(cgtls)%ppuint64(8) != ppuint64(0) {

				if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppint32((*(*[512]ppint8)(iqunsafe.ppPointer(cgbp + 512)))[aaj]) != iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv12 {
					Xmpfr_assert_fail(cgtls, "tstrtofr.c\x00", ppint32(1495), "s1[j] != 0\x00")
				}
				pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				(*(*[576]ppint8)(iqunsafe.ppPointer(cgbp + 1024)))[aaj+ppint32(2)] = (*(*[512]ppint8)(iqunsafe.ppPointer(cgbp + 512)))[aaj]
				aaj++
			}

			Xsprintf(cgtls, cgbp+1024+ppuintptr(aaj+iqlibc.ppInt32FromInt32(2)), "@%ld\x00", iqlibc.ppVaList(cgbp+1720, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 1696))))

			for aanoteq == 0 && aaj < iqlibc.ppInt32FromInt32(mvBSIZE)-iqlibc.ppInt32FromInt32(1) {

				if ppint32((*(*[512]ppint8)(iqunsafe.ppPointer(cgbp + 512)))[aaj]) != ppint32('0') {
					aanoteq = ppint32(1)
				}
				aaj++
			}

			if aaneg != 0 {

				Xmpfr_neg(cgtls, cgbp+1600, cgbp+1600, ppint32(ecMPFR_RNDN))
				Xmpfr_neg(cgtls, cgbp+1632, cgbp+1632, ppint32(ecMPFR_RNDN))
			}

			if aanoteq != 0 {

				Xmpfr_strtofr(cgtls, cgbp+1664, cgbp+1024, iqlibc.ppUintptrFromInt32(0), aabase, ppint32(ecMPFR_RNDZ))
				if !(Xmpfr_equal_p(cgtls, cgbp+1664, cgbp+1600) != 0) {

					Xprintf(cgtls, "Error in random_tests for prec=%d i=%d base=%d\n\x00", iqlibc.ppVaList(cgbp+1720, aaprec, aai, aabase))
					Xprintf(cgtls, "s0 = %s\ns1 = %s\ns2 = %s\n\x00", iqlibc.ppVaList(cgbp+1720, cgbp, cgbp+512, cgbp+1024))
					Xprintf(cgtls, "x0 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+1600)
					Xprintf(cgtls, "x2 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+1664)
					Xexit(cgtls, ppint32(1))
				}
			}

			Xmpfr_strtofr(cgtls, cgbp+1664, cgbp+1024, iqlibc.ppUintptrFromInt32(0), aabase, ppint32(ecMPFR_RNDA))
			if !(Xmpfr_equal_p(cgtls, cgbp+1664, cgbp+1632) != 0) {

				Xprintf(cgtls, "Error in random_tests for prec=%d i=%d base=%d\n\x00", iqlibc.ppVaList(cgbp+1720, aaprec, aai, aabase))
				Xprintf(cgtls, "s0 = %s\ns1 = %s\ns2 = %s\n\x00", iqlibc.ppVaList(cgbp+1720, cgbp, cgbp+512, cgbp+1024))
				Xprintf(cgtls, "x1 = \x00", 0)
				Xmpfr_dump(cgtls, cgbp+1632)
				Xprintf(cgtls, "x2 = \x00", 0)
				Xmpfr_dump(cgtls, cgbp+1664)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_2
		cg_2:
			;
			aai++
		}
		Xmpfr_clears(cgtls, cgbp+1600, iqlibc.ppVaList(cgbp+1720, cgbp+1632, cgbp+1664, iqlibc.ppUintptrFromInt32(0)))

		goto cg_1
	cg_1:
		;
		aaprec++
	}
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	Xtests_start_mpfr(cgtls)

	sicoverage(cgtls)
	sicheck_special(cgtls)
	sicheck_reftable(cgtls)
	sicheck_parse(cgtls)
	sicheck_overflow(cgtls)
	sicheck_retval(cgtls)
	sibug20081028(cgtls)
	sitest20100310(cgtls)
	sibug20120814(cgtls)
	sibug20120829(cgtls)
	sibug20161217(cgtls)
	sibug20170308(cgtls)
	sibug20181127(cgtls)
	sirandom_tests(cgtls)

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___builtin_unreachable(*iqlibc.ppTLS)

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuint64

func _exit(*iqlibc.ppTLS, ppint32)

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp3(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_cmp_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64) ppint32

func _mpfr_cmp_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_exp10(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_get_emin_min(*iqlibc.ppTLS) ppint64

func _mpfr_get_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuintptr

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_inits2(*iqlibc.ppTLS, ppint64, ppuintptr, ppuintptr)

func _mpfr_mul_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_sgn(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_strtofr(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _putchar(*iqlibc.ppTLS, ppint32) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emin(*iqlibc.ppTLS, ppint64)

func _sprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

var _stdout ppuintptr

func _strchr(*iqlibc.ppTLS, ppuintptr, ppint32) ppuintptr

func _strcmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _tests_default_random(*iqlibc.ppTLS, ppuintptr, ppint32, ppint64, ppint64, ppint32)

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

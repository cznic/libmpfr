// Code generated for linux/amd64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/amd64 -UHAVE_NEARBYINT -mlong-double-64 -c -o tsub.o.go tsub.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMAX_PREC = 200
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1680
const mvMPFR_AI_THRESHOLD3 = 24368
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 894
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 6522
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 12
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 23540
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 19
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/x86_64/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPAGESIZE = 4096
const mvPAGE_SIZE = "PAGESIZE"
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvREXP = 1024
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTGENERIC_SO_TEST = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_HLE_ACQUIRE = 65536
const mv__ATOMIC_HLE_RELEASE = 131072
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_BID_FORMAT__ = 1
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 2
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FUNCTION__ = "__func__"
const mv__FXSR__ = 1
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "x86_64-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_DOUBLE_64__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MMX_WITH_SSE__ = 1
const mv__MMX__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SEG_FS = 1
const mv__SEG_GS = 1
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT128__ = 16
const mv__SIZEOF_FLOAT80__ = 16
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__SSE2_MATH__ = 1
const mv__SSE2__ = 1
const mv__SSE_MATH__ = 1
const mv__SSE__ = 1
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__amd64 = 1
const mv__amd64__ = 1
const mv__code_model_small__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__k8 = 1
const mv__k8__ = 1
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv__x86_64 = 1
const mv__x86_64__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppint8
	fdfrac_digits        ppint8
	fdp_cs_precedes      ppint8
	fdp_sep_by_space     ppint8
	fdn_cs_precedes      ppint8
	fdn_sep_by_space     ppint8
	fdp_sign_posn        ppint8
	fdn_sign_posn        ppint8
	fdint_p_cs_precedes  ppint8
	fdint_p_sep_by_space ppint8
	fdint_n_cs_precedes  ppint8
	fdint_n_sep_by_space ppint8
	fdint_p_sign_posn    ppint8
	fdint_n_sign_posn    ppint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

func sitest_sub(cgtls *iqlibc.ppTLS, aaa tnmpfr_ptr, aab tnmpfr_srcptr, aac tnmpfr_srcptr, aarnd_mode tnmpfr_rnd_t) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aa_p, aa_p1 tnmpfr_srcptr
	var aainex, aainex_r, aareuse_b, aareuse_c, ccv1, ccv2 ppint32
	var pp_ /* t at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aainex, aainex_r, aareuse_b, aareuse_c, ccv1, ccv2

	aainex = Xmpfr_sub(cgtls, aaa, aab, aac, aarnd_mode)

	if aaa != aab && aaa != aac && !((*tn__mpfr_struct)(iqunsafe.ppPointer(aaa)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2)) {

		aareuse_b = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(aaa)).fd_mpfr_prec == (*tn__mpfr_struct)(iqunsafe.ppPointer(aab)).fd_mpfr_prec)
		aareuse_c = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(aaa)).fd_mpfr_prec == (*tn__mpfr_struct)(iqunsafe.ppPointer(aac)).fd_mpfr_prec)

		if aareuse_b != 0 || aareuse_c != 0 {
			Xmpfr_init2(cgtls, cgbp, (*tn__mpfr_struct)(iqunsafe.ppPointer(aaa)).fd_mpfr_prec)
		}

		if aareuse_b != 0 {

			{
				aa_p = aab
				ccv1 = Xmpfr_set4(cgtls, cgbp, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
			}
			pp_ = ccv1
			aainex_r = Xmpfr_sub(cgtls, cgbp, cgbp, aac, aarnd_mode)
			if !(Xmpfr_equal_p(cgtls, cgbp, aaa) != 0 && iqlibc.ppBoolInt32(aainex_r > 0)-iqlibc.ppBoolInt32(aainex_r < 0) == iqlibc.ppBoolInt32(aainex > 0)-iqlibc.ppBoolInt32(aainex < 0)) {

				Xprintf(cgtls, "reuse of b error in b - c in %s for\n\x00", iqlibc.ppVaList(cgbp+40, Xmpfr_print_rnd_mode(cgtls, aarnd_mode)))
				Xprintf(cgtls, "b = \x00", 0)
				Xmpfr_dump(cgtls, aab)
				Xprintf(cgtls, "c = \x00", 0)
				Xmpfr_dump(cgtls, aac)
				Xprintf(cgtls, "Expected \x00", 0)
				Xmpfr_dump(cgtls, aaa)
				Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+40, aainex))
				Xprintf(cgtls, "Got      \x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+40, aainex_r))
				Xexit(cgtls, ppint32(1))
			}
		}

		if aareuse_c != 0 {

			{
				aa_p1 = aac
				ccv2 = Xmpfr_set4(cgtls, cgbp, aa_p1, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign)
			}
			pp_ = ccv2
			aainex_r = Xmpfr_sub(cgtls, cgbp, aab, cgbp, aarnd_mode)
			if !(Xmpfr_equal_p(cgtls, cgbp, aaa) != 0 && iqlibc.ppBoolInt32(aainex_r > 0)-iqlibc.ppBoolInt32(aainex_r < 0) == iqlibc.ppBoolInt32(aainex > 0)-iqlibc.ppBoolInt32(aainex < 0)) {

				Xprintf(cgtls, "reuse of c error in b - c in %s for\n\x00", iqlibc.ppVaList(cgbp+40, Xmpfr_print_rnd_mode(cgtls, aarnd_mode)))
				Xprintf(cgtls, "b = \x00", 0)
				Xmpfr_dump(cgtls, aab)
				Xprintf(cgtls, "c = \x00", 0)
				Xmpfr_dump(cgtls, aac)
				Xprintf(cgtls, "Expected \x00", 0)
				Xmpfr_dump(cgtls, aaa)
				Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+40, aainex))
				Xprintf(cgtls, "Got      \x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+40, aainex_r))
				Xexit(cgtls, ppint32(1))
			}
		}

		if aareuse_b != 0 || aareuse_c != 0 {
			Xmpfr_clear(cgtls, cgbp)
		}
	}

	return aainex
}

func sicheck_diverse(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aa_p tnmpfr_ptr
	var aainexact, ccv14, ccv15, ccv16 ppint32
	var ccv1, ccv10, ccv11, ccv12, ccv13, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7, ccv9 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aainexact, ccv1, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7, ccv9

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)
	Xmpfr_init(cgtls, cgbp+64)

	/* check corner case cancel=0, but add_exp=1 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(4))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(2))
	Xmpfr_setmax(cgtls, cgbp+32, X__gmpfr_emax)
	Xmpfr_set_str_binary(cgtls, cgbp+64, "0.1E-10\x00")             /* tiny */
	sitest_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN)) /* should round to 2^emax, i.e. overflow */

	if ccv3 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv3 {
		if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv1 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(127), "! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(127), "! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv3 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error in mpfr_sub(a,b,c,RNDN) for b=maxfloat, prec(a)<prec(b), c tiny\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* other coverage test */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(2))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(2))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+64, ppint64(-iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
	sitest_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(3)), 0) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub(1,-2,RNDD)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* yet another coverage test */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(3))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(1))
	Xmpfr_set_ui_2exp(cgtls, cgbp+32, ppuint64(1), X__gmpfr_emax, ppint32(ecMPFR_RNDZ))
	/* y = (1 - 2^(-3))*2^emax */
	Xmpfr_set_ui_2exp(cgtls, cgbp+64, ppuint64(1), X__gmpfr_emax-ppint64(4), ppint32(ecMPFR_RNDZ))
	/* z = 2^(emax - 4) */
	/* y - z = (1 - 2^(-3) - 2^(-4))*2^emax > (1-2^(-2))*2^emax */
	aainexact = Xmpfr_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(156), "inexact > 0\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv7 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv7 {
		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(157), "! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(157), "! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv7 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint64(1)) != 0; !ccv13 {
		Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(157), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tsub.c\", 157, \"! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tsub.c\", 157, \"! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0\x00")
		if ccv12 = iqlibc.Bool(!(0 != 0)); !ccv12 {
			if ccv11 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv11 {
				if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv9 {
					Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(157), "! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv10 {
					Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(157), "! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv12 || ccv11 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(288))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(288))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(288))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.111000110011000001000111101010111011110011101001101111111110000011100101000001001010110010101010011001010100000001110011110001010101101010001011101110100100001011110100110000101101100011010001001011011010101010000010001101001000110010010111111011110001111101001000101101001100101100101000E80\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+64, "0.100001111111101001011010001100110010100111001110000110011101001011010100001000000100111011010110110010000000000010101101011000010000110001110010100001100101011100100100001011000100011110000001010101000100011101001000010111100000111000111011001000100100011000100000010010111000000100100111E-258\x00")
	aainexact = sitest_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))
	if aainexact <= 0 {

		Xprintf(cgtls, "Wrong inexact flag for prec=288\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(32))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(63))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(63))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.101101111011011100100100100111E31\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.111110010010100100110101101010001001100101110001000101110111111E-1\x00")
	sitest_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1011011110110111001001001001101100000110110101101100101001011E31\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (5)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(63))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(63))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1011011110110111001001001001101100000110110101101100101001011E31\x00")
	Xmpfr_sub_ui(cgtls, cgbp+64, cgbp+32, ppuint64(1541116494), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.11111001001010010011010110101E-1\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (7)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(63))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(63))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1011011110110111001001001001101100000110110101101100101001011E31\x00")
	Xmpfr_sub_ui(cgtls, cgbp+64, cgbp+32, ppuint64(1541116494), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.11111001001010010011010110101E-1\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (6)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(32))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(32))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10110111101001110100100101111000E0\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10001100100101000100110111000100E0\x00")
	ccv14 = sitest_sub(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	aainexact = ccv14
	if ccv14 != 0 {

		Xprintf(cgtls, "Wrong inexact flag (2): got %d instead of 0\n\x00", iqlibc.ppVaList(cgbp+104, aainexact))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(32))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(32))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11111000110111011000100111011010E0\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10011111101111000100001000000000E-3\x00")
	ccv15 = sitest_sub(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	aainexact = ccv15
	if ccv15 != 0 {

		Xprintf(cgtls, "Wrong inexact flag (1): got %d instead of 0\n\x00", iqlibc.ppVaList(cgbp+104, aainexact))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(33))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(33))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.1E-32\x00")
	Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.111111111111111111111111111111111E0\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (1 - 1E-33) with prec=33\n\x00", 0)
		Xprintf(cgtls, "Expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(32))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(33))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.1E-32\x00")
	Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (1 - 1E-33) with prec=32\n\x00", 0)
		Xprintf(cgtls, "Expected 1.0, got \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(65))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(65))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(64))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.1110111011110001110111011111111111101000011001011100101100101101\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1110111011110001110111011111111111101000011001011100101100101100\x00")
	sitest_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	sitest_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.000000000000000000000000000000000000000000000000000000000000001\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (2)\n\x00", 0)
		Xprintf(cgtls, "Expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str_binary(cgtls, cgbp, "1.1110111011110001110111011111111111101000011001011100101100101101\x00")
	sitest_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (3)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aainexact = sitest_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDA))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.000000000000000000000000000000000000000000000000000000000000001\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp, ppint32(1)) != 0 || aainexact <= 0 {

		Xprintf(cgtls, "Error in mpfr_sub (4)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_prec(cgtls, cgbp, ppint64(66))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.11101110111100011101110111111111111010000110010111001011001010111\x00")
	sitest_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (5)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* check in-place operations */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	sitest_sub(cgtls, cgbp, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_sgn(cgtls, cgbp) != 0 {

		Xprintf(cgtls, "Error for mpfr_sub (x, x, x, MPFR_RNDN) with x=1.0\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(53))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(53))
	Xmpfr_set_str(cgtls, cgbp, "1.229318102e+09\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "2.32221184180698677665e+05\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sitest_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp+64, "1229085880.815819263458251953125\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (1.22e9 - 2.32e5)\n\x00", 0)
		Xprintf(cgtls, "expected 1229085880.815819263458251953125, got \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp+64, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(112))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(98))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(54))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11111100100000000011000011100000101101010001000111E-401\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10110000100100000101101100011111111011101000111000101E-464\x00")
	sitest_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp, ppint32(1)) != 0 {
		Xprintf(cgtls, "mpfr_sub(z, x, y) failed for prec(x)=112, prec(y)=98\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(33))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(32), ppint32(ecMPFR_RNDN))
	Xmpfr_sub_ui(cgtls, cgbp, cgbp, ppuint64(1), ppint32(ecMPFR_RNDN))

	Xmpfr_set_prec(cgtls, cgbp, ppint64(5))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(5))
	Xmpfr_set_str_binary(cgtls, cgbp, "1e-12\x00")
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	sitest_sub(cgtls, cgbp, cgbp+32, cgbp, ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.11111\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub (x, y, x, MPFR_RNDD) for x=2^(-12), y=1\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(24))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(24))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.100010000000000000000000E19\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.100000000000000000000100E15\x00")
	Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.1E19\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_add (2)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(10))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(10))
	{
		aa_p = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv16 = 0
	}
	pp_ = ccv16
	Xmpfr_set_str_binary(cgtls, cgbp+64, "0.10001\x00")
	if sitest_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN)) <= 0 {

		Xprintf(cgtls, "Wrong inexact flag in x=mpfr_sub(0,z) for prec(z)>prec(x)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if sitest_sub(cgtls, cgbp, cgbp+64, cgbp+32, ppint32(ecMPFR_RNDN)) >= 0 {

		Xprintf(cgtls, "Wrong inexact flag in x=mpfr_sub(z,0) for prec(z)>prec(x)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
}

func sibug_ddefour(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(192)
	defer cgtls.ppFree(192)

	var aa_p, aa_p1 tnmpfr_srcptr
	var ccv1, ccv2 ppint32
	var pp_ /* ex at bp+0 */ tnmpfr_t
	var pp_ /* ex1 at bp+32 */ tnmpfr_t
	var pp_ /* ex2 at bp+64 */ tnmpfr_t
	var pp_ /* ex3 at bp+96 */ tnmpfr_t
	var pp_ /* tot at bp+128 */ tnmpfr_t
	var pp_ /* tot1 at bp+160 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aa_p, aa_p1, ccv1, ccv2

	Xmpfr_init2(cgtls, cgbp, ppint64(53))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(53))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(53))
	Xmpfr_init2(cgtls, cgbp+96, ppint64(53))
	Xmpfr_init2(cgtls, cgbp+128, ppint64(150))
	Xmpfr_init2(cgtls, cgbp+160, ppint64(150))

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(906), ppint32(ecMPFR_RNDN))
	Xmpfr_log(cgtls, cgbp+128, cgbp, ppint32(ecMPFR_RNDN))
	{
		aa_p = cgbp + 128
		ccv1 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
	}
	pp_ = ccv1                                                           /* ex1 = high(tot) */
	sitest_sub(cgtls, cgbp+64, cgbp+128, cgbp+32, ppint32(ecMPFR_RNDN))  /* ex2 = high(tot - ex1) */
	sitest_sub(cgtls, cgbp+160, cgbp+128, cgbp+32, ppint32(ecMPFR_RNDN)) /* tot1 = tot - ex1 */
	{
		aa_p1 = cgbp + 160
		ccv2 = Xmpfr_set4(cgtls, cgbp+96, aa_p1, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign)
	}
	pp_ = ccv2 /* ex3 = high(tot - ex1) */

	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+96, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in ddefour test.\n\x00", 0)
		Xprintf(cgtls, "ex2=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xprintf(cgtls, "ex3=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+96)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
	Xmpfr_clear(cgtls, cgbp+128)
	Xmpfr_clear(cgtls, cgbp+160)
}

// C documentation
//
//	/* if u = o(x-y), v = o(u-x), w = o(v+y), then x-y = u-w */
func sicheck_two_sum(cgtls *iqlibc.ppTLS, aap tnmpfr_prec_t) {
	cgbp := cgtls.ppAlloc(192)
	defer cgtls.ppFree(192)

	var aainexact ppint32
	var aarnd tnmpfr_rnd_t
	var pp_ /* u at bp+64 */ tnmpfr_t
	var pp_ /* v at bp+96 */ tnmpfr_t
	var pp_ /* w at bp+128 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_ = aainexact, aarnd

	Xmpfr_init2(cgtls, cgbp, aap)
	Xmpfr_init2(cgtls, cgbp+32, aap)
	Xmpfr_init2(cgtls, cgbp+64, aap)
	Xmpfr_init2(cgtls, cgbp+96, aap)
	Xmpfr_init2(cgtls, cgbp+128, aap)
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	if !(Xmpfr_rands_initialized != 0) {
		Xmpfr_rands_initialized = ppint8(1)
		X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		pp_ = iqlibc.ppInt32FromInt32(0)
	}
	Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	if Xmpfr_cmpabs(cgtls, cgbp, cgbp+32) < 0 {
		Xmpfr_swap(cgtls, cgbp, cgbp+32)
	}
	aarnd = ppint32(ecMPFR_RNDN)
	aainexact = sitest_sub(cgtls, cgbp+64, cgbp, cgbp+32, aarnd)
	sitest_sub(cgtls, cgbp+96, cgbp+64, cgbp, aarnd)
	Xmpfr_add(cgtls, cgbp+128, cgbp+96, cgbp+32, aarnd)
	/* as u = (x-y) - w, we should have inexact and w of opposite signs */
	if aainexact == 0 && Xmpfr_sgn(cgtls, cgbp+128) != 0 || aainexact > 0 && Xmpfr_sgn(cgtls, cgbp+128) <= 0 || aainexact < 0 && Xmpfr_sgn(cgtls, cgbp+128) >= 0 {

		Xprintf(cgtls, "Wrong inexact flag for prec=%u, rnd=%s\n\x00", iqlibc.ppVaList(cgbp+168, iqlibc.ppUint32FromInt64(aap), Xmpfr_print_rnd_mode(cgtls, aarnd)))
		Xprintf(cgtls, "x=\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "y=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "u=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xprintf(cgtls, "v=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+96)
		Xprintf(cgtls, "w=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+128)
		Xprintf(cgtls, "inexact = %d\n\x00", iqlibc.ppVaList(cgbp+168, aainexact))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
	Xmpfr_clear(cgtls, cgbp+128)
}

func sicheck_inexact(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aacmp, aainexact ppint32
	var aapu, aapx, aapy, aapz tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv5, ccv6 ppint64
	var pp_ /* u at bp+96 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aacmp, aainexact, aapu, aapx, aapy, aapz, aarnd, ccv5, ccv6

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)
	Xmpfr_init(cgtls, cgbp+64)
	Xmpfr_init(cgtls, cgbp+96)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(6)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(4), ppint32(ecMPFR_RNDN)) /* x = 6/16 */
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(2))
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp+32, cgbp+32, ppuint64(4), ppint32(ecMPFR_RNDN))   /* y = -1/16 */
	aainexact = sitest_sub(cgtls, cgbp+32, cgbp+32, cgbp, ppint32(ecMPFR_RNDN)) /* y = round(-7/16) = -1/2 */
	if aainexact >= 0 {

		Xprintf(cgtls, "Error: wrong inexact flag for -1/16 - (6/16)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	aapx = ppint64(2)
	for {
		if !(aapx < ppint64(mvMAX_PREC)) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aapx)
		for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp) == 0 {

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		}
		aapu = ppint64(2)
		for {
			if !(aapu < ppint64(mvMAX_PREC)) {
				break
			}

			Xmpfr_set_prec(cgtls, cgbp+96, aapu)
			for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp+96) == 0 {

				if !(Xmpfr_rands_initialized != 0) {
					Xmpfr_rands_initialized = ppint8(1)
					X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
					pp_ = iqlibc.ppInt32FromInt32(0)
				}
				Xmpfr_urandomb(cgtls, cgbp+96, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			}

			aapy = iqlibc.ppInt64FromUint64(ppuint64(2) + Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvMAX_PREC)-iqlibc.ppInt32FromInt32(2)))
			Xmpfr_set_prec(cgtls, cgbp+32, aapy)
			/* warning: MPFR_EXP is undefined for 0 */
			if Xmpfr_cmpabs(cgtls, cgbp, cgbp+96) >= 0 {
				ccv5 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp - (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp
			} else {
				ccv5 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp - (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			}
			aapz = ccv5
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec > (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_prec {
				ccv6 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec
			} else {
				ccv6 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mpfr_prec
			}
			aapz = aapz + ccv6
			Xmpfr_set_prec(cgtls, cgbp+64, aapz)
			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % ppuint64(ecMPFR_RNDF))
			if sitest_sub(cgtls, cgbp+64, cgbp, cgbp+96, aarnd) != 0 {

				Xprintf(cgtls, "z <- x - u should be exact\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}

			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % ppuint64(ecMPFR_RNDF))
			aainexact = sitest_sub(cgtls, cgbp+32, cgbp, cgbp+96, aarnd)
			aacmp = Xmpfr_cmp3(cgtls, cgbp+32, cgbp+64, ppint32(1))
			if aainexact == 0 && aacmp != 0 || aainexact > 0 && aacmp <= 0 || aainexact < 0 && aacmp >= 0 {

				Xprintf(cgtls, "Wrong inexact flag for rnd=%s\n\x00", iqlibc.ppVaList(cgbp+136, Xmpfr_print_rnd_mode(cgtls, aarnd)))
				Xprintf(cgtls, "expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+136, aacmp, aainexact))
				Xprintf(cgtls, "x=\x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "u=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+96)
				Xprintf(cgtls, "y=  \x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xprintf(cgtls, "x-u=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+64)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_3
		cg_3:
			;
			aapu++
		}

		goto cg_1
	cg_1:
		;
		aapx++
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
}

// C documentation
//
//	/* Bug found by Jakub Jelinek
//	 * https://bugzilla.redhat.com/show_bug.cgi?id=643657
//	 * https://gforge.inria.fr/tracker/index.php?func=detail&aid=11301
//	 * The consequence can be either an assertion failure (i = 2 in the
//	 * testcase below, in debug mode) or an incorrectly rounded value.
//	 */
func sibug20101017(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aa_p tnmpfr_srcptr
	var aai, aainex, ccv2 ppint32
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aa_p, aai, aainex, ccv2

	Xmpfr_init2(cgtls, cgbp, ppint64((iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppInt32FromInt32(2)))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))

	/* a = 2^(2N) + k.2^(2N-1) + 2^N and b = 1
	   with N = GMP_NUMB_BITS and k = 0 or 1.
	   c = a - b should round to the same value as a. */

	aai = ppint32(2)
	for {
		if !(aai <= ppint32(3)) {
			break
		}

		Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(aai), ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1)), ppint32(ecMPFR_RNDN))
		Xmpfr_add_ui(cgtls, cgbp, cgbp, ppuint64(1), ppint32(ecMPFR_RNDN))
		Xmpfr_mul_2ui(cgtls, cgbp, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)), ppint32(ecMPFR_RNDN))
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
		aainex = Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		{
			aa_p = cgbp
			ccv2 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
		}
		pp_ = ccv2
		if !(Xmpfr_equal_p(cgtls, cgbp+64, cgbp+32) != 0) {

			Xprintf(cgtls, "Error in bug20101017 for i = %d.\n\x00", iqlibc.ppVaList(cgbp+104, aai))
			Xprintf(cgtls, "Expected \x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
			Xputchar(cgtls, ppint32('\n'))
			Xprintf(cgtls, "Got      \x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp+64, ppint32(ecMPFR_RNDN))
			Xputchar(cgtls, ppint32('\n'))
			Xexit(cgtls, ppint32(1))
		}
		if aainex >= 0 {

			Xprintf(cgtls, "Error in bug20101017 for i = %d: bad inex value.\n\x00", iqlibc.ppVaList(cgbp+104, aai))
			Xprintf(cgtls, "Expected negative, got %d.\n\x00", iqlibc.ppVaList(cgbp+104, aainex))
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(64))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(129))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(2))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.100000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000001E65\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+64, "0.10E1\x00")
	aainex = Xmpfr_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), ppint64(64)) != 0 || aainex >= 0 {

		Xprintf(cgtls, "Error in mpfr_sub for b-c for b=2^64+1+2^(-64), c=1\n\x00", 0)
		Xprintf(cgtls, "Expected result 2^64 with inex < 0\n\x00", 0)
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "with inex=%d\n\x00", iqlibc.ppVaList(cgbp+104, aainex))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

// C documentation
//
//	/* hard test of rounding */
func sicheck_rounding(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aai ppint32
	var aak, aal ppint64
	var aap tnmpfr_prec_t
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* res at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aai, aak, aal, aap

	aap = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aap <= ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))) {
			break
		}

		Xmpfr_init2(cgtls, cgbp, aap)
		Xmpfr_init2(cgtls, cgbp+96, aap)
		Xmpfr_init2(cgtls, cgbp+32, aap+ppint64(1)+ppint64(iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))))
		Xmpfr_init2(cgtls, cgbp+64, ppint64(mvMPFR_PREC_MIN))

		/* b = 2^p + 1 + 2^(-k), c = 2^(-l) */
		aak = 0
		for {
			if !(aak <= ppint64(iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) {
				break
			}
			aal = 0
			for {
				if !(aal <= ppint64(iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))) {
					break
				}

				Xmpfr_set_ui_2exp(cgtls, cgbp+32, ppuint64(1), aap, ppint32(ecMPFR_RNDN))
				Xmpfr_add_ui(cgtls, cgbp+32, cgbp+32, ppuint64(1), ppint32(ecMPFR_RNDN))
				Xmpfr_mul_2ui(cgtls, cgbp+32, cgbp+32, iqlibc.ppUint64FromInt64(aak), ppint32(ecMPFR_RNDN))
				Xmpfr_add_ui(cgtls, cgbp+32, cgbp+32, ppuint64(1), ppint32(ecMPFR_RNDN))
				Xmpfr_div_2ui(cgtls, cgbp+32, cgbp+32, iqlibc.ppUint64FromInt64(aak), ppint32(ecMPFR_RNDN))
				Xmpfr_set_ui_2exp(cgtls, cgbp+64, ppuint64(1), -aal, ppint32(ecMPFR_RNDN))
				aai = Xmpfr_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))
				/* b - c = 2^p + 1 + 2^(-k) - 2^(-l), should be rounded to
				   2^p for l <= k, and 2^p+2 for l < k, except when p=1 and
				   k=l, in which case b - c = 3, and the round-away rule implies
				   a = 4 = 2^p+2 = 2^(p+1) */
				if aal < aak || aal == aak && aap > ppint64(1) {

					if Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), aap) != 0 {

						Xprintf(cgtls, "Wrong result in check_rounding\n\x00", 0)
						Xprintf(cgtls, "p=%lu k=%ld l=%ld\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint64FromInt64(aap), aak, aal))
						Xprintf(cgtls, "b=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+32)
						Xprintf(cgtls, "c=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "Expected 2^%lu\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint64FromInt64(aap)))
						Xprintf(cgtls, "Got      \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xexit(cgtls, ppint32(1))
					}
					if aai >= 0 {

						Xprintf(cgtls, "Wrong ternary value in check_rounding\n\x00", 0)
						Xprintf(cgtls, "p=%lu k=%ld l=%ld\n\x00", iqlibc.ppVaList(cgbp+136, iqlibc.ppUint64FromInt64(aap), aak, aal))
						Xprintf(cgtls, "b=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+32)
						Xprintf(cgtls, "c=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "a=\x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "Expected < 0, got %d\n\x00", iqlibc.ppVaList(cgbp+136, aai))
						Xexit(cgtls, ppint32(1))
					}
				} else { /* l < k  or (l = k and p = 1) */

					Xmpfr_set_ui_2exp(cgtls, cgbp+96, ppuint64(1), aap, ppint32(ecMPFR_RNDN))
					Xmpfr_add_ui(cgtls, cgbp+96, cgbp+96, ppuint64(2), ppint32(ecMPFR_RNDN))
					if Xmpfr_cmp3(cgtls, cgbp, cgbp+96, ppint32(1)) != 0 {

						Xprintf(cgtls, "Wrong result in check_rounding\n\x00", 0)
						Xprintf(cgtls, "b=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+32)
						Xprintf(cgtls, "c=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "Expected \x00", 0)
						Xmpfr_dump(cgtls, cgbp+96)
						Xprintf(cgtls, "Got      \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xexit(cgtls, ppint32(1))
					}
					if aai <= 0 {

						Xprintf(cgtls, "Wrong ternary value in check_rounding\n\x00", 0)
						Xprintf(cgtls, "b=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+32)
						Xprintf(cgtls, "c=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "Expected > 0, got %d\n\x00", iqlibc.ppVaList(cgbp+136, aai))
						Xexit(cgtls, ppint32(1))
					}
				}

				goto cg_3
			cg_3:
				;
				aal++
			}
			goto cg_2
		cg_2:
			;
			aak++
		}

		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+96)
		Xmpfr_clear(cgtls, cgbp+32)
		Xmpfr_clear(cgtls, cgbp+64)

		goto cg_1
	cg_1:
		;
		aap++
	}
}

// C documentation
//
//	/* Check a = b - c, where the significand of b has all 1's, c is small
//	   compared to b, and PREC(a) = PREC(b) - 1. Thus b is a midpoint for
//	   the precision of the result a. The test is done with the extended
//	   exponent range and with some reduced exponent range. Two choices
//	   are made for the exponent of b: the maximum exponent - 1 (similar
//	   to some normal case) and the maximum exponent (overflow case or
//	   near overflow case, depending on the rounding mode).
//	   This test is useful to trigger a bug in r10382: Since c is small,
//	   the computation in sub1.c was done by first rounding b in the
//	   precision of a, then correcting the result if b was a breakpoint
//	   for this precision (exactly representable number for the directed
//	   rounding modes, or midpoint for the round-to-nearest mode). The
//	   problem was that for a midpoint in the round-to-nearest mode, the
//	   rounding of b gave a spurious overflow; not only the overflow flag
//	   was incorrect, but the result could not be corrected, since due to
//	   this overflow, the "even rounding" information was lost.
//	   In the case of reduced exponent range, an additional test is done
//	   for consistency checks: the subtraction is done in the extended
//	   exponent range (no overflow), then the result is converted to the
//	   initial exponent range with mpfr_check_range. */
func sicheck_max_almosteven(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aaemax, aaemin [2]tnmpfr_exp_t
	var aaflags1, aaflags2 tnmpfr_flags_t
	var aai, aainex1, aainex2, aaj, aaneg, aarnd, ccv10, ccv11, ccv7, ccv8, ccv9 ppint32
	var aaold_emax, aaold_emin tnmpfr_exp_t
	var aap tnmpfr_prec_t
	var ccv5, ccv6 ppbool
	var pp_ /* a1 at bp+0 */ tnmpfr_t
	var pp_ /* a2 at bp+32 */ tnmpfr_t
	var pp_ /* b at bp+64 */ tnmpfr_t
	var pp_ /* c at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaemin, aaflags1, aaflags2, aai, aainex1, aainex2, aaj, aaneg, aaold_emax, aaold_emin, aap, aarnd, ccv10, ccv11, ccv5, ccv6, ccv7, ccv8, ccv9
	aaemin = [2]tnmpfr_exp_t{
		0: iqlibc.ppInt64FromInt32(1) - iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)),
		1: ppint64(-ppint32(1000)),
	}
	aaemax = [2]tnmpfr_exp_t{
		0: iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) - iqlibc.ppInt64FromInt32(1),
		1: ppint64(1000),
	}

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	aai = 0
	for {
		if !(aai < ppint32(2)) {
			break
		}

		Xset_emin(cgtls, aaemin[aai])
		Xset_emax(cgtls, aaemax[aai])

		aap = iqlibc.ppInt64FromUint64(ppuint64(mvMPFR_PREC_MIN) + Xrandlimb(cgtls)%ppuint64(70))
		Xmpfr_init2(cgtls, cgbp, aap)
		Xmpfr_init2(cgtls, cgbp+32, aap)
		Xmpfr_init2(cgtls, cgbp+64, aap+ppint64(1))
		Xmpfr_init2(cgtls, cgbp+96, ppint64(mvMPFR_PREC_MIN))

		Xmpfr_setmax(cgtls, cgbp+64, 0)
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+96, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))

		aaneg = 0
		for {
			if !(aaneg < ppint32(2)) {
				break
			}

			aaj = ppint32(1)
			for {
				if !(aaj >= 0) {
					break
				}

				Xmpfr_set_exp(cgtls, cgbp+64, X__gmpfr_emax-ppint64(aaj))
				aarnd = 0
				for {
					if !(aarnd < ppint32(ecMPFR_RNDF)) {
						break
					}

					/* Expected result. */
					aaflags1 = ppuint32(mvMPFR_FLAGS_INEXACT)

					if ccv6 = aarnd == ppint32(ecMPFR_RNDN); !ccv6 {
						if ccv5 = aarnd == ppint32(ecMPFR_RNDZ); !ccv5 {
						}
					}
					if ccv6 || (ccv5 || aarnd+aaneg == ppint32(ecMPFR_RNDD)) {

						if aaneg != 0 {
							ccv7 = ppint32(1)
						} else {
							ccv7 = -ppint32(1)
						}
						aainex1 = ccv7
						Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax-ppint64(aaj))
					} else {

						if aaneg != 0 {
							ccv8 = -ppint32(1)
						} else {
							ccv8 = ppint32(1)
						}
						aainex1 = ccv8
						if aaj == 0 {

							aaflags1 |= ppuint32(mvMPFR_FLAGS_OVERFLOW)
							Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
						} else {

							Xmpfr_setmin(cgtls, cgbp, X__gmpfr_emax)
						}
					}
					if aaneg != 0 {
						ccv9 = -ppint32(1)
					} else {
						ccv9 = ppint32(1)
					}
					(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = ccv9

					/* Computed result. */
					Xmpfr_clear_flags(cgtls)
					aainex2 = Xmpfr_sub(cgtls, cgbp+32, cgbp+64, cgbp+96, aarnd)
					aaflags2 = X__gmpfr_flags

					if !(aaflags1 == aaflags2 && iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0) == iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0) && Xmpfr_equal_p(cgtls, cgbp, cgbp+32) != 0) {

						Xprintf(cgtls, "Error 1 in check_max_almosteven for %s, i = %d, j = %d, neg = %d\n\x00", iqlibc.ppVaList(cgbp+136, Xmpfr_print_rnd_mode(cgtls, aarnd), aai, aaj, aaneg))
						Xprintf(cgtls, "     b = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "Expected \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+136, aainex1))
						Xflags_out(cgtls, aaflags1)
						Xprintf(cgtls, "Got      \x00", 0)
						Xmpfr_dump(cgtls, cgbp+32)
						Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+136, aainex2))
						Xflags_out(cgtls, aaflags2)
						Xexit(cgtls, ppint32(1))
					}

					if aai == 0 {
						break
					}

					/* Additional test for the reduced exponent range. */
					Xmpfr_clear_flags(cgtls)
					Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
					Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
					aainex2 = Xmpfr_sub(cgtls, cgbp+32, cgbp+64, cgbp+96, aarnd)
					Xset_emin(cgtls, aaemin[aai])
					Xset_emax(cgtls, aaemax[aai])
					if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp >= X__gmpfr_emin && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= X__gmpfr_emax)), ppint64(1)) != 0 {
						if aainex2 != 0 {
							X__gmpfr_flags |= ppuint32(mvMPFR_FLAGS_INEXACT)
							ccv11 = aainex2
						} else {
							ccv11 = 0
						}
						ccv10 = ccv11
					} else {
						ccv10 = Xmpfr_check_range(cgtls, cgbp+32, aainex2, aarnd)
					}
					aainex2 = ccv10
					aaflags2 = X__gmpfr_flags

					if !(aaflags1 == aaflags2 && iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0) == iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0) && Xmpfr_equal_p(cgtls, cgbp, cgbp+32) != 0) {

						Xprintf(cgtls, "Error 2 in check_max_almosteven for %s, i = %d, j = %d, neg = %d\n\x00", iqlibc.ppVaList(cgbp+136, Xmpfr_print_rnd_mode(cgtls, aarnd), aai, aaj, aaneg))
						Xprintf(cgtls, "     b = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "Expected \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+136, aainex1))
						Xflags_out(cgtls, aaflags1)
						Xprintf(cgtls, "Got      \x00", 0)
						Xmpfr_dump(cgtls, cgbp+32)
						Xprintf(cgtls, "  with inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+136, aainex2))
						Xflags_out(cgtls, aaflags2)
						Xexit(cgtls, ppint32(1))
					}

					goto cg_4
				cg_4:
					;
					aarnd++
				}

				goto cg_3
			cg_3:
				;
				aaj--
			} /* j */

			Xmpfr_neg(cgtls, cgbp+64, cgbp+64, ppint32(ecMPFR_RNDN))
			Xmpfr_neg(cgtls, cgbp+96, cgbp+96, ppint32(ecMPFR_RNDN))

			goto cg_2
		cg_2:
			;
			aaneg++
		} /* neg */

		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))

		goto cg_1
	cg_1:
		;
		aai++
	} /* i */

	Xset_emin(cgtls, aaold_emin)
	Xset_emax(cgtls, aaold_emax)
}

func sitest_rndf(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var ccv1 ppbool
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	pp_ = ccv1

	Xmpfr_init2(cgtls, cgbp, ppint64(7))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(7))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(7))
	Xmpfr_init2(cgtls, cgbp+96, ppint64(7))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-1.000000e-7\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+64, "-1.000000\x00")
	Xmpfr_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDF))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_d + ppuintptr(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))*8))&(iqlibc.ppUint64FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1))) != iqlibc.ppUint64FromInt32(0))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(869), "(((mp_limb_t) ((((a)->_mpfr_d)[((((! __builtin_constant_p (!!(((((a)->_mpfr_prec)) >= 1 && (((a)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((a)->_mpfr_prec)) >= 1 && (((a)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((a)->_mpfr_prec)) >= 1 && (((a)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((a)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_sub(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD))
	if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

		Xmpfr_sub(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))
		if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

			Xprintf(cgtls, "Error: mpfr_sub(a,b,c,RNDF) does not match RNDD/RNDU\n\x00", 0)
			Xprintf(cgtls, "b=\x00", 0)
			Xmpfr_dump(cgtls, cgbp+32)
			Xprintf(cgtls, "c=\x00", 0)
			Xmpfr_dump(cgtls, cgbp+64)
			Xprintf(cgtls, "a=\x00", 0)
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
	}
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
}

func sitestall_rndf(cgtls *iqlibc.ppTLS, aapmax tnmpfr_prec_t) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaeb tnmpfr_exp_t
	var aapa, aapb, aapc tnmpfr_prec_t
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aaeb, aapa, aapb, aapc

	aapa = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aapa <= aapmax) {
			break
		}

		Xmpfr_init2(cgtls, cgbp, aapa)
		Xmpfr_init2(cgtls, cgbp+96, aapa)
		aapb = ppint64(mvMPFR_PREC_MIN)
		for {
			if !(aapb <= aapmax) {
				break
			}

			Xmpfr_init2(cgtls, cgbp+32, aapb)
			aaeb = 0
			for {
				if !(aaeb <= aapmax+ppint64(3)) {
					break
				}

				Xmpfr_set_ui_2exp(cgtls, cgbp+32, ppuint64(1), aaeb, ppint32(ecMPFR_RNDN))
				for Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, ppuint64(1), aaeb+ppint64(1)) < 0 {

					aapc = ppint64(mvMPFR_PREC_MIN)
					for {
						if !(aapc <= aapmax) {
							break
						}

						Xmpfr_init2(cgtls, cgbp+64, aapc)
						pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
						for Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) < 0 {

							Xmpfr_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDF))
							Xmpfr_sub(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD))
							if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

								Xmpfr_sub(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))
								if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

									Xprintf(cgtls, "Error: mpfr_sub(a,b,c,RNDF) does not match RNDD/RNDU\n\x00", 0)
									Xprintf(cgtls, "b=\x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xprintf(cgtls, "c=\x00", 0)
									Xmpfr_dump(cgtls, cgbp+64)
									Xprintf(cgtls, "a=\x00", 0)
									Xmpfr_dump(cgtls, cgbp)
									Xexit(cgtls, ppint32(1))
								}
							}
							Xmpfr_nextabove(cgtls, cgbp+64)
						}
						Xmpfr_clear(cgtls, cgbp+64)

						goto cg_4
					cg_4:
						;
						aapc++
					}
					Xmpfr_nextabove(cgtls, cgbp+32)
				}

				goto cg_3
			cg_3:
				;
				aaeb++
			}
			Xmpfr_clear(cgtls, cgbp+32)

			goto cg_2
		cg_2:
			;
			aapb++
		}
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+96)

		goto cg_1
	cg_1:
		;
		aapa++
	}
}

func sitest_rndf_exact(cgtls *iqlibc.ppTLS, aapmax tnmp_size_t) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaeb tnmpfr_exp_t
	var aapa, aapb, aapc tnmpfr_prec_t
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aaeb, aapa, aapb, aapc

	aapa = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aapa <= aapmax) {
			break
		}

		/* only check pa mod GMP_NUMB_BITS = -2, -1, 0, 1, 2 */
		if (aapa+ppint64(2))%ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)) > ppint64(4) {
			goto cg_1
		}
		Xmpfr_init2(cgtls, cgbp, aapa)
		Xmpfr_init2(cgtls, cgbp+96, aapa)
		aapb = ppint64(mvMPFR_PREC_MIN)
		for {
			if !(aapb <= aapmax) {
				break
			}

			if (aapb+ppint64(2))%ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)) > ppint64(4) {
				goto cg_2
			}
			Xmpfr_init2(cgtls, cgbp+32, aapb)
			aaeb = 0
			for {
				if !(aaeb <= aapmax+ppint64(3)) {
					break
				}

				if !(Xmpfr_rands_initialized != 0) {
					Xmpfr_rands_initialized = ppint8(1)
					X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
					pp_ = iqlibc.ppInt32FromInt32(0)
				}
				Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				Xmpfr_mul_2ui(cgtls, cgbp+32, cgbp+32, iqlibc.ppUint64FromInt64(aaeb), ppint32(ecMPFR_RNDN))
				aapc = ppint64(mvMPFR_PREC_MIN)
				for {
					if !(aapc <= aapmax) {
						break
					}

					if (aapc+ppint64(2))%ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)) > ppint64(4) {
						goto cg_5
					}
					Xmpfr_init2(cgtls, cgbp+64, aapc)
					if !(Xmpfr_rands_initialized != 0) {
						Xmpfr_rands_initialized = ppint8(1)
						X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
						pp_ = iqlibc.ppInt32FromInt32(0)
					}
					Xmpfr_urandomb(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
					Xmpfr_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDF))
					Xmpfr_sub(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD))
					if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

						Xmpfr_sub(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))
						if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

							Xprintf(cgtls, "Error: mpfr_sub(a,b,c,RNDF) does not match RNDD/RNDU\n\x00", 0)
							Xprintf(cgtls, "b=\x00", 0)
							Xmpfr_dump(cgtls, cgbp+32)
							Xprintf(cgtls, "c=\x00", 0)
							Xmpfr_dump(cgtls, cgbp+64)
							Xprintf(cgtls, "a=\x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							Xexit(cgtls, ppint32(1))
						}
					}

					/* now make the low bits from c match those from b */
					Xmpfr_add(cgtls, cgbp+64, cgbp+32, cgbp+96, ppint32(ecMPFR_RNDN))
					Xmpfr_sub(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDF))
					Xmpfr_sub(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD))
					if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

						Xmpfr_sub(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))
						if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

							Xprintf(cgtls, "Error: mpfr_sub(a,b,c,RNDF) does not match RNDD/RNDU\n\x00", 0)
							Xprintf(cgtls, "b=\x00", 0)
							Xmpfr_dump(cgtls, cgbp+32)
							Xprintf(cgtls, "c=\x00", 0)
							Xmpfr_dump(cgtls, cgbp+64)
							Xprintf(cgtls, "a=\x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							Xexit(cgtls, ppint32(1))
						}
					}

					Xmpfr_clear(cgtls, cgbp+64)

					goto cg_5
				cg_5:
					;
					aapc++
				}

				goto cg_3
			cg_3:
				;
				aaeb++
			}
			Xmpfr_clear(cgtls, cgbp+32)

			goto cg_2
		cg_2:
			;
			aapb++
		}
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+96)

		goto cg_1
	cg_1:
		;
		aapa++
	}
}

// C documentation
//
//	/* Bug in the case 2 <= d < p in generic code mpfr_sub1sp() introduced
//	 * in r12242. Before this change, the special case that is failing was
//	 * handled by the MPFR_UNLIKELY(ap[n-1] == MPFR_LIMB_HIGHBIT) in the
//	 * "truncate:" code.
//	 */
func sibug20180215(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(192)
	defer cgtls.ppFree(192)

	var aa_p tnmpfr_srcptr
	var aai, aap, ccv2 ppint32
	var ccv4 ppbool
	var pp_ /* r at bp+128 */ [3]tnmpfr_rnd_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z1 at bp+64 */ tnmpfr_t
	var pp_ /* z2 at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aa_p, aai, aap, ccv2, ccv4
	*(*[3]tnmpfr_rnd_t)(iqunsafe.ppPointer(cgbp + 128)) = [3]tnmpfr_rnd_t{
		1: ppint32(ecMPFR_RNDU),
		2: ppint32(ecMPFR_RNDA),
	}

	aap = ppint32(3)
	for {
		if !(aap <= iqlibc.ppInt32FromInt32(3)+iqlibc.ppInt32FromInt32(4)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))) {
			break
		}

		Xmpfr_inits2(cgtls, ppint64(aap), cgbp, iqlibc.ppVaList(cgbp+152, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
		Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(1), ppint64(aap-ppint32(1)), ppint32(ecMPFR_RNDN))
		Xmpfr_nextabove(cgtls, cgbp)
		Xmpfr_set_ui_2exp(cgtls, cgbp+32, ppuint64(3), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN))
		{
			aa_p = cgbp
			ccv2 = Xmpfr_set4(cgtls, cgbp+64, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
		}
		pp_ = ccv2
		Xmpfr_nextbelow(cgtls, cgbp+64)
		Xmpfr_nextbelow(cgtls, cgbp+64)
		aai = 0
		for {
			if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(cgbp+128 == cgbp+128)), ppint64(1)) != 0; !ccv4 {
				Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1037), "(void *) &(r) == (void *) &(r)[0]\x00")
			}
			pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if !(ppint64(aai) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(12)/iqlibc.ppUint64FromInt64(4))) {
				break
			}

			sitest_sub(cgtls, cgbp+96, cgbp, cgbp+32, (*(*[3]tnmpfr_rnd_t)(iqunsafe.ppPointer(cgbp + 128)))[aai])
			if !(Xmpfr_equal_p(cgtls, cgbp+64, cgbp+96) != 0) {

				Xprintf(cgtls, "Error in bug20180215 in precision %d, %s\n\x00", iqlibc.ppVaList(cgbp+152, aap, Xmpfr_print_rnd_mode(cgtls, (*(*[3]tnmpfr_rnd_t)(iqunsafe.ppPointer(cgbp + 128)))[aai])))
				Xprintf(cgtls, "expected \x00", 0)
				Xmpfr_dump(cgtls, cgbp+64)
				Xprintf(cgtls, "got      \x00", 0)
				Xmpfr_dump(cgtls, cgbp+96)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_3
		cg_3:
			;
			aai++
		}
		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+152, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))

		goto cg_1
	cg_1:
		;
		aap++
	}
}

func sibug20180216(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aad, aainex, aap, aar ppint32
	var ccv3, ccv4, ccv5, ccv6, ccv8 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z1 at bp+64 */ tnmpfr_t
	var pp_ /* z2 at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aad, aainex, aap, aar, ccv3, ccv4, ccv5, ccv6, ccv8

	aap = ppint32(3)
	for {
		if !(aap <= iqlibc.ppInt32FromInt32(3)+iqlibc.ppInt32FromInt32(4)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))) {
			break
		}

		Xmpfr_inits2(cgtls, ppint64(aap), cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
		aad = ppint32(1)
		for {
			if !(aad <= aap-ppint32(2)) {
				break
			}

			aainex = Xmpfr_set_ui_2exp(cgtls, cgbp+64, ppuint64(1), ppint64(aad), ppint32(ecMPFR_RNDN)) /* z1 = 2^d */

			if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv3 {
				Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1065), "inex == 0\x00")
			}
			pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			aainex = Xmpfr_add_ui(cgtls, cgbp, cgbp+64, ppuint64(1), ppint32(ecMPFR_RNDN))

			if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv4 {
				Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1067), "inex == 0\x00")
			}
			pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			Xmpfr_nextabove(cgtls, cgbp)                                            /* x = 2^d + 1 + epsilon */
			aainex = Xmpfr_sub(cgtls, cgbp+32, cgbp, cgbp+64, ppint32(ecMPFR_RNDN)) /* y = 1 + epsilon */

			if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv5 {
				Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1070), "inex == 0\x00")
			}
			pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			aainex = Xmpfr_add(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))

			if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv6 {
				Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1072), "inex == 0\x00")
			}
			pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

			if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp+96, cgbp) != 0)), ppint64(1)) != 0; !ccv8 {
				Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1073), "mpfr_equal_p (z2, x)\x00")
				if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+96, cgbp) != 0) {
					X__builtin_unreachable(cgtls)
				}
			}
			pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* consistency check */
			aar = 0
			for {
				if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
					break
				}

				aainex = sitest_sub(cgtls, cgbp+96, cgbp, cgbp+32, aar)
				if !(Xmpfr_equal_p(cgtls, cgbp+64, cgbp+96) != 0) || aainex != 0 {

					Xprintf(cgtls, "Error in bug20180216 with p=%d, d=%d, %s\n\x00", iqlibc.ppVaList(cgbp+136, aap, aad, Xmpfr_print_rnd_mode(cgtls, aar)))
					Xprintf(cgtls, "expected \x00", 0)
					Xmpfr_dump(cgtls, cgbp+64)
					Xprintf(cgtls, "  with inex = 0\n\x00", 0)
					Xprintf(cgtls, "got      \x00", 0)
					Xmpfr_dump(cgtls, cgbp+96)
					Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+136, aainex))
					Xexit(cgtls, ppint32(1))
				}

				goto cg_9
			cg_9:
				;
				aar++
			}

			goto cg_2
		cg_2:
			;
			aad++
		}
		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))

		goto cg_1
	cg_1:
		;
		aap++
	}
}

// C documentation
//
//	/* Fails with r12281: "reuse of c error in b - c in MPFR_RNDN".
//	 *
//	 * If the fix in r10697 (2016-07-29) is reverted, this test also fails
//	 * (there were no non-regression tests for this bug until this one);
//	 * note that if --enable-assert=full is used, the error message is:
//	 * "sub1 & sub1sp return different values for MPFR_RNDN".
//	 */
func sibug20180217(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aa_p tnmpfr_srcptr
	var aad, aai, aainex1, aainex2, aap, aar, ccv5 ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z1 at bp+64 */ tnmpfr_t
	var pp_ /* z2 at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aad, aai, aainex1, aainex2, aap, aar, ccv5

	aap = ppint32(3)
	for {
		if !(aap <= iqlibc.ppInt32FromInt32(3)+iqlibc.ppInt32FromInt32(4)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))) {
			break
		}

		Xmpfr_inits2(cgtls, ppint64(aap), cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
		aad = aap
		for {
			if !(aad <= aap+ppint32(4)) {
				break
			}

			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
			Xmpfr_set_ui_2exp(cgtls, cgbp+32, ppuint64(1), ppint64(-aad), ppint32(ecMPFR_RNDN))
			aai = 0
			for {
				if !(aai < ppint32(3)) {
					break
				}

				aar = 0
				for {
					if !(aar < ppint32(ecMPFR_RNDF)) {
						break
					}

					{
						aa_p = cgbp
						ccv5 = Xmpfr_set4(cgtls, cgbp+64, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
					}
					pp_ = ccv5
					if aad == aap {

						Xmpfr_nextbelow(cgtls, cgbp+64)
						if aai == 0 {
							aainex1 = 0
						} else {
							if aar == ppint32(ecMPFR_RNDD) || aar == ppint32(ecMPFR_RNDZ) || aar == ppint32(ecMPFR_RNDN) && aai > ppint32(1) {

								Xmpfr_nextbelow(cgtls, cgbp+64)
								aainex1 = -ppint32(1)
							} else {
								aainex1 = ppint32(1)
							}
						}
					} else {
						if aar == ppint32(ecMPFR_RNDD) || aar == ppint32(ecMPFR_RNDZ) || aar == ppint32(ecMPFR_RNDN) && aad == aap+ppint32(1) && aai > 0 {

							Xmpfr_nextbelow(cgtls, cgbp+64)
							aainex1 = -ppint32(1)
						} else {
							aainex1 = ppint32(1)
						}
					}
					aainex2 = sitest_sub(cgtls, cgbp+96, cgbp, cgbp+32, aar)
					if !(Xmpfr_equal_p(cgtls, cgbp+64, cgbp+96) != 0 && iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0) == iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0)) {

						Xprintf(cgtls, "Error in bug20180217 with p=%d, d=%d, i=%d, %s\n\x00", iqlibc.ppVaList(cgbp+136, aap, aad, aai, Xmpfr_print_rnd_mode(cgtls, aar)))
						Xprintf(cgtls, "x = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "y = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+32)
						Xprintf(cgtls, "Expected \x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+136, aainex1))
						Xprintf(cgtls, "Got      \x00", 0)
						Xmpfr_dump(cgtls, cgbp+96)
						Xprintf(cgtls, "  with inex = %d\n\x00", iqlibc.ppVaList(cgbp+136, aainex2))
						Xexit(cgtls, ppint32(1))
					}

					goto cg_4
				cg_4:
					;
					aar++
				}
				if aai == 0 {
					Xmpfr_nextabove(cgtls, cgbp+32)
				} else {

					if aap < ppint32(6) {
						break
					}
					Xmpfr_nextbelow(cgtls, cgbp+32)
					pp_ = Xmpfr_mul_ui(cgtls, cgbp+32, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(25)), ppint32(ecMPFR_RNDD))
					Xmpfr_div_2ui(cgtls, cgbp+32, cgbp+32, ppuint64(4), ppint32(ecMPFR_RNDN))
				}

				goto cg_3
			cg_3:
				;
				aai++
			}

			goto cg_2
		cg_2:
			;
			aad++
		}
		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))

		goto cg_1
	cg_1:
		;
		aap++
	}
}

/* Tests on UBF.
 *
 * Note: mpfr_sub1sp will never be used as it does not support UBF.
 * Thus there is no need to generate tests for both mpfr_sub1 and
 * mpfr_sub1sp.
 *
 * Note that mpfr_sub1 has a special branch "c small", where the second
 * argument c is sufficiently smaller than the ulp of the first argument
 * and the ulp of the result: MAX (aq, bq) + 2 <= diff_exp.
 * Tests should be done for both the main branch and this special branch
 * when this makes sense.
 */

func sitest_ubf_aux(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(768)
	defer cgtls.ppFree(768)

	var aa_p tnmpfr_srcptr
	var aad, aai, aainex_y, aainexact, aaj, aak, aakn, aaneg, aarnd, aasign, ccv10, ccv33, ccv37, ccv38, ccv40, ccv41, ccv42, ccv43, ccv44, ccv45, ccv51, ccv52 ppint32
	var aae0 tnmpfr_exp_t
	var aaex [11]ppint32
	var aaflags, aaflags_y tnmpfr_flags_t
	var aap [11]tnmpfr_ptr
	var aar tnmpfr_rnd_t
	var ccv11, ccv14, ccv17, ccv18, ccv19, ccv20, ccv21, ccv22, ccv23, ccv26, ccv27, ccv28, ccv39, ccv4, ccv47, ccv49, ccv5, ccv56, ccv6 ppbool
	var ccv16 ppuint64
	var ccv36, ccv50 ppint64
	var pp_ /* e at bp+656 */ [7]tnmpfr_exp_t
	var pp_ /* ee at bp+528 */ tnmpfr_t
	var pp_ /* w at bp+624 */ tnmpfr_t
	var pp_ /* x at bp+0 */ [11]tnmpfr_ubf_t
	var pp_ /* y at bp+560 */ tnmpfr_t
	var pp_ /* z at bp+592 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aad, aae0, aaex, aaflags, aaflags_y, aai, aainex_y, aainexact, aaj, aak, aakn, aaneg, aap, aar, aarnd, aasign, ccv10, ccv11, ccv14, ccv16, ccv17, ccv18, ccv19, ccv20, ccv21, ccv22, ccv23, ccv26, ccv27, ccv28, ccv33, ccv36, ccv37, ccv38, ccv39, ccv4, ccv40, ccv41, ccv42, ccv43, ccv44, ccv45, ccv47, ccv49, ccv5, ccv50, ccv51, ccv52, ccv56, ccv6
	aakn = ppint32(2)
	*(*[7]tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 656)) = [7]tnmpfr_exp_t{
		0: -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1),
		1: iqlibc.ppInt64FromInt32(1) - iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)),
		2: ppint64(-ppint32(mvREXP)),
		4: ppint64(mvREXP),
		5: iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) - iqlibc.ppInt64FromInt32(1),
		6: iqlibc.ppInt64FromInt64(0x7fffffffffffffff),
	}

	Xmpfr_init2(cgtls, cgbp+528, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(8)*iqlibc.ppUint64FromInt32(mvCHAR_BIT)))
	Xmpfr_inits2(cgtls, ppint64(64), cgbp+560, iqlibc.ppVaList(cgbp+720, cgbp+592, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_init2(cgtls, cgbp+624, ppint64(2))

	aai = 0
	for {
		if !(aai < ppint32(11)) {
			break
		}
		aap[aai] = cgbp + ppuintptr(aai)*48
		goto cg_1
	cg_1:
		;
		aai++
	}

	/* exact zero result, with small and large exponents */
	aai = 0
	for {
		if !(aai < ppint32(2)) {
			break
		}

		Xmpfr_init2(cgtls, aap[aai], iqlibc.ppInt64FromUint64(ppuint64(5)+Xrandlimb(cgtls)%ppuint64(128)))
		pp_ = Xmpfr_set_ui_2exp(cgtls, aap[aai], iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(17)), 0, ppint32(ecMPFR_RNDN))
		pp_ = iqlibc.ppUint64FromInt64(8)
		Xmpfr_mpz_init(cgtls, aap[aai]+32)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(4)

		goto cg_2
	cg_2:
		;
		aai++
	}
	aaj = 0
	for {
		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(cgbp+656 == cgbp+656)), ppint64(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1214), "(void *) &(e) == (void *) &(e)[0]\x00")
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if !(ppint64(aaj) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(56)/iqlibc.ppUint64FromInt64(8))) {
			break
		}

		aainexact = Xmpfr_set_si_2exp(cgtls, cgbp+528, (*(*[7]tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 656)))[aaj], 0, ppint32(ecMPFR_RNDN))

		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1217), "inexact == 0\x00")
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		pp_ = iqlibc.ppUint64FromInt64(8)
		aainexact = Xmpfr_get_z(cgtls, aap[0]+32, cgbp+528, ppint32(ecMPFR_RNDN))

		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1219), "inexact == 0\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		pp_ = iqlibc.ppUint64FromInt64(8)
		pp_ = iqlibc.ppUint64FromInt64(8)
		X__gmpz_sub_ui(cgtls, aap[0]+32, aap[0]+32, iqlibc.ppUint64FromInt32(aakn))

		aak = -aakn
		for {
			if !(aak <= aakn) {
				break
			}

			/* exponent: e[j] + k, with |k| <= kn */
			pp_ = iqlibc.ppUint64FromInt64(8)
			pp_ = iqlibc.ppUint64FromInt64(8)
			X__gmpz_set(cgtls, aap[ppint32(1)]+32, aap[0]+32)

			aaneg = 0
			for {
				if !(aaneg <= ppint32(1)) {
					break
				}

				aarnd = 0
				for {
					if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
						break
					}

					/* Note: x[0] and x[1] are equal MPFR numbers, but do not
					   test mpfr_sub with arg2 == arg3 as pointers in order to
					   skip potentially optimized mpfr_sub code. */
					aainexact = Xmpfr_sub(cgtls, cgbp+592, aap[0], aap[ppint32(1)], aarnd)

					if ccv11 = aainexact != 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+592)).fd_mpfr_exp != -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1); !ccv11 {
						if aarnd != ppint32(ecMPFR_RNDD) {
							ccv10 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+592)).fd_mpfr_sign < 0)
						} else {
							ccv10 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+592)).fd_mpfr_sign > 0)
						}
					}
					if ccv11 || ccv10 != 0 {

						Xprintf(cgtls, "Error 1 in test_ubf for exact zero result: j=%d k=%d neg=%d, rnd=%s\nGot \x00", iqlibc.ppVaList(cgbp+720, aaj, aak, aaneg, Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xmpfr_dump(cgtls, cgbp+592)
						Xprintf(cgtls, "inexact = %d\n\x00", iqlibc.ppVaList(cgbp+720, aainexact))
						Xexit(cgtls, ppint32(1))
					}

					goto cg_9
				cg_9:
					;
					aarnd++
				}

				aai = 0
				for {
					if !(aai < ppint32(2)) {
						break
					}
					(*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_sign = -(*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_sign
					goto cg_12
				cg_12:
					;
					aai++
				}

				goto cg_8
			cg_8:
				;
				aaneg++
			}

			pp_ = iqlibc.ppUint64FromInt64(8)
			pp_ = iqlibc.ppUint64FromInt64(8)
			X__gmpz_add_ui(cgtls, aap[0]+32, aap[0]+32, ppuint64(1))

			goto cg_7
		cg_7:
			;
			aak++
		}

		goto cg_3
	cg_3:
		;
		aaj++
	}
	aai = 0
	for {
		if !(aai < ppint32(2)) {
			break
		}

		if ccv14 = (*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(4); ccv14 {
			pp_ = iqlibc.ppUint64FromInt64(8)
			Xmpfr_mpz_clear(cgtls, aap[aai]+32)
		}
		pp_ = ccv14 && iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		Xmpfr_clear(cgtls, aap[aai])

		goto cg_13
	cg_13:
		;
		aai++
	}

	/* Up to a given exponent (for the result) and sign, test:
	 *   (t + .11010) - (t + .00001) = .11001
	 *   (t + 8) - (t + 111.00111)   = .11001
	 * where t = 0 or a power of 2, e.g. 2^200. Test various exponents
	 * (including those near the underflow/overflow boundaries) so that
	 * the subtraction yields a normal number, an overflow or an underflow.
	 * In MPFR_RNDA, also test with a 2-bit precision target, as this
	 * yields an exponent change.
	 *
	 * Also test the "MAX (aq, bq) + 2 <= diff_exp" branch of sub1.c with
	 * .1 - epsilon (possible decrease of the exponent) and .111 - epsilon
	 * in precision 2 (possible increase of the exponent). The first test
	 * triggers a possible decrease of the exponent (see bug fixed in r13806).
	 * The second test triggers a possible increase of the exponent (see the
	 * "exp_a != MPFR_EXP_MAX" test to avoid an integer overflow).
	 */
	aai = 0
	for {
		if !(aai < ppint32(8)) {
			break
		}

		var snv = [4]ppint32{
			0: ppint32(26),
			1: ppint32(1),
			2: ppint32(256),
			3: ppint32(231),
		}

		if aai < ppint32(4) {
			ccv16 = ppuint64(8) + Xrandlimb(cgtls)%ppuint64(128)
		} else {
			ccv16 = ppuint64(256)
		}
		Xmpfr_init2(cgtls, aap[aai], iqlibc.ppInt64FromUint64(ccv16))
		if aai < ppint32(4) {

			aainexact = Xmpfr_set_si_2exp(cgtls, aap[aai], ppint64(snv[aai]), ppint64(-ppint32(5)), ppint32(ecMPFR_RNDN))

			if ccv17 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv17 {
				Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1284), "inexact == 0\x00")
			}
			pp_ = ccv17 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		} else {

			aainexact = Xmpfr_set_si_2exp(cgtls, aap[aai], ppint64(1), ppint64(200), ppint32(ecMPFR_RNDN))

			if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv18 {
				Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1289), "inexact == 0\x00")
			}
			pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			aainexact = Xmpfr_add(cgtls, aap[aai], aap[aai], aap[aai-ppint32(4)], ppint32(ecMPFR_RNDN))

			if ccv19 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv19 {
				Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1291), "inexact == 0\x00")
			}
			pp_ = ccv19 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		aaex[aai] = ppint32((*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_exp + ppint64(5))

		if ccv20 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aaex[aai] >= iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv20 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1294), "ex[i] >= 0\x00")
		}
		pp_ = ccv20 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		goto cg_15
	cg_15:
		;
		aai++
	}
	Xmpfr_inits2(cgtls, ppint64(3), aap[ppint32(8)], iqlibc.ppVaList(cgbp+720, aap[ppint32(9)], aap[ppint32(10)], iqlibc.ppUintptrFromInt32(0)))
	aainexact = Xmpfr_set_si_2exp(cgtls, aap[ppint32(8)], ppint64(1), 0, ppint32(ecMPFR_RNDN))

	if ccv21 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv21 {
		Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1298), "inexact == 0\x00")
	}
	pp_ = ccv21 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaex[ppint32(8)] = ppint32(5)
	aainexact = Xmpfr_set_si_2exp(cgtls, aap[ppint32(9)], ppint64(1), 0, ppint32(ecMPFR_RNDN)) /* will be epsilon */

	if ccv22 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv22 {
		Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1301), "inexact == 0\x00")
	}
	pp_ = ccv22 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaex[ppint32(9)] = 0
	aainexact = Xmpfr_set_si_2exp(cgtls, aap[ppint32(10)], ppint64(7), 0, ppint32(ecMPFR_RNDN))

	if ccv23 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv23 {
		Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1304), "inexact == 0\x00")
	}
	pp_ = ccv23 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaex[ppint32(10)] = ppint32(5)

	aai = 0
	for {
		if !(aai < ppint32(11)) {
			break
		}

		pp_ = iqlibc.ppUint64FromInt64(8)
		Xmpfr_mpz_init(cgtls, aap[aai]+32)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(4)

		goto cg_24
	cg_24:
		;
		aai++
	}

	aaj = 0
	for {
		if ccv26 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(cgbp+656 == cgbp+656)), ppint64(1)) != 0; !ccv26 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1313), "(void *) &(e) == (void *) &(e)[0]\x00")
		}
		pp_ = ccv26 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if !(ppint64(aaj) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(56)/iqlibc.ppUint64FromInt64(8))) {
			break
		}

		aainexact = Xmpfr_set_si_2exp(cgtls, cgbp+528, (*(*[7]tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 656)))[aaj], 0, ppint32(ecMPFR_RNDN))

		if ccv27 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv27 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1316), "inexact == 0\x00")
		}
		pp_ = ccv27 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		pp_ = iqlibc.ppUint64FromInt64(8)
		aainexact = Xmpfr_get_z(cgtls, aap[0]+32, cgbp+528, ppint32(ecMPFR_RNDN))

		if ccv28 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainexact == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv28 {
			Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1318), "inexact == 0\x00")
		}
		pp_ = ccv28 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aai = ppint32(1)
		for {
			if !(aai < ppint32(11)) {
				break
			}
			pp_ = iqlibc.ppUint64FromInt64(8)
			pp_ = iqlibc.ppUint64FromInt64(8)
			X__gmpz_set(cgtls, aap[aai]+32, aap[0]+32)
			goto cg_29
		cg_29:
			;
			aai++
		}
		aai = 0
		for {
			if !(aai < ppint32(11)) {
				break
			}

			pp_ = iqlibc.ppUint64FromInt64(8)
			pp_ = iqlibc.ppUint64FromInt64(8)
			X__gmpz_add_ui(cgtls, aap[aai]+32, aap[aai]+32, iqlibc.ppUint64FromInt32(aaex[aai]))
			pp_ = iqlibc.ppUint64FromInt64(8)
			pp_ = iqlibc.ppUint64FromInt64(8)
			X__gmpz_sub_ui(cgtls, aap[aai]+32, aap[aai]+32, iqlibc.ppUint64FromInt32(ppint32(5)+aakn))

			goto cg_30
		cg_30:
			;
			aai++
		}
		pp_ = iqlibc.ppUint64FromInt64(8)
		pp_ = iqlibc.ppUint64FromInt64(8)
		X__gmpz_sub_ui(cgtls, aap[ppint32(9)]+32, aap[ppint32(9)]+32, ppuint64(256))
		aak = -aakn
		for {
			if !(aak <= aakn) {
				break
			}

			aaneg = 0
			for {
				if !(aaneg <= ppint32(1)) {
					break
				}
				if aaneg != 0 {
					ccv33 = -ppint32(1)
				} else {
					ccv33 = ppint32(1)
				}
				aasign = ccv33

				aarnd = 0
				for {
					if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
						break
					}
					aai = 0
					for {
						if !(aai <= ppint32(10)) {
							break
						}

						if aai >= ppint32(8) {

							if (*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(4) {
								pp_ = iqlibc.ppUint64FromInt64(8)
								ccv36 = Xmpfr_ubf_zexp2exp(cgtls, aap[aai]+32)
							} else {
								ccv36 = (*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_exp
							}
							aae0 = ccv36
							if aae0 < -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

								aae0 += ppint64(3)
							}

							if aarnd == ppint32(ecMPFR_RNDN) {
								if aai == ppint32(8) {
									if aae0 == X__gmpfr_emin-ppint64(1) {
										ccv38 = ppint32(3)
									} else {
										ccv38 = ppint32(4)
									}
									ccv37 = ccv38
								} else {
									ccv37 = ppint32(6)
								}
								aad = ccv37
							} else {
								if ccv39 = aarnd == ppint32(ecMPFR_RNDZ); !ccv39 {
								}
								if ccv39 || aarnd+aaneg == ppint32(ecMPFR_RNDD) {
									if aai == ppint32(8) {
										ccv40 = ppint32(3)
									} else {
										ccv40 = ppint32(6)
									}
									aad = ccv40
								} else {
									if aai == ppint32(8) {
										ccv41 = ppint32(4)
									} else {
										ccv41 = ppint32(8)
									}
									aad = ccv41
								}
							}

							Xmpfr_clear_flags(cgtls)
							aainex_y = Xmpfr_set_si_2exp(cgtls, cgbp+624, ppint64(aasign*aad), aae0-ppint64(3), aarnd)
							aaflags_y = X__gmpfr_flags | ppuint32(mvMPFR_FLAGS_INEXACT)
							if aainex_y == 0 {
								if aarnd == ppint32(ecMPFR_RNDN) {
									if aai == ppint32(8) {
										ccv43 = ppint32(1)
									} else {
										ccv43 = -ppint32(1)
									}
									ccv42 = aasign * ccv43
								} else {
									if aarnd == ppint32(ecMPFR_RNDD) || aarnd == ppint32(ecMPFR_RNDZ) && aasign > 0 || aarnd == ppint32(ecMPFR_RNDA) && aasign < 0 {
										ccv44 = -ppint32(1)
									} else {
										ccv44 = ppint32(1)
									}
									ccv42 = ccv44
								}
								aainex_y = ccv42
							}
							{
								aa_p = cgbp + 624
								ccv45 = Xmpfr_set4(cgtls, cgbp+560, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
							}
							pp_ = ccv45

							Xmpfr_clear_flags(cgtls)
							aainexact = Xmpfr_sub(cgtls, cgbp+624, aap[aai], aap[ppint32(9)], aarnd)
							aaflags = X__gmpfr_flags

							/* For MPFR_RNDF, only do a basic test. */

							if ccv47 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_check(cgtls, cgbp+624) != 0)), ppint64(1)) != 0; !ccv47 {
								Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1371), "mpfr_check (w)\x00")
								if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_check(cgtls, cgbp+624) != 0) {
									X__builtin_unreachable(cgtls)
								}
							}
							pp_ = ccv47 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
							if aarnd == ppint32(ecMPFR_RNDF) {
								goto cg_35
							}

							goto pptestw
						}

						Xmpfr_clear_flags(cgtls)
						aainexact = Xmpfr_sub(cgtls, cgbp+592, aap[aai], aap[aai+ppint32(1)], aarnd)
						aaflags = X__gmpfr_flags

						/* For MPFR_RNDF, only do a basic test. */

						if ccv49 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_check(cgtls, cgbp+592) != 0)), ppint64(1)) != 0; !ccv49 {
							Xmpfr_assert_fail(cgtls, "tsub.c\x00", ppint32(1383), "mpfr_check (z)\x00")
							if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_check(cgtls, cgbp+592) != 0) {
								X__builtin_unreachable(cgtls)
							}
						}
						pp_ = ccv49 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
						if aarnd == ppint32(ecMPFR_RNDF) {
							goto cg_35
						}

						if (*tn__mpfr_struct)(iqunsafe.ppPointer(aap[0])).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(4) {
							pp_ = iqlibc.ppUint64FromInt64(8)
							ccv50 = Xmpfr_ubf_zexp2exp(cgtls, aap[0]+32)
						} else {
							ccv50 = (*tn__mpfr_struct)(iqunsafe.ppPointer(aap[0])).fd_mpfr_exp
						}
						aae0 = ccv50

						if aae0 < X__gmpfr_emin {
							if aarnd == ppint32(ecMPFR_RNDN) && aae0 < X__gmpfr_emin-ppint64(1) {
								ccv51 = ppint32(ecMPFR_RNDZ)
							} else {
								ccv51 = aarnd
							}
							aar = ccv51
							aaflags_y = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
							aainex_y = Xmpfr_underflow(cgtls, cgbp+560, aar, aasign)
						} else {
							if aae0 > X__gmpfr_emax {

								aaflags_y = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
								aainex_y = Xmpfr_overflow(cgtls, cgbp+560, aarnd, aasign)
							} else {

								Xmpfr_set_si_2exp(cgtls, cgbp+560, ppint64(aasign*ppint32(25)), aae0-ppint64(5), ppint32(ecMPFR_RNDN))
								aaflags_y = ppuint32(0)
								aainex_y = 0
							}
						}

						if aaflags != aaflags_y || !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex_y > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex_y < iqlibc.ppInt32FromInt32(0))) || !(Xmpfr_equal_p(cgtls, cgbp+560, cgbp+592) != 0) {

							Xprintf(cgtls, "Error 2 in test_ubf with j=%d k=%d neg=%d i=%d rnd=%s\n\x00", iqlibc.ppVaList(cgbp+720, aaj, aak, aaneg, aai, Xmpfr_print_rnd_mode(cgtls, aarnd)))
							Xprintf(cgtls, "emin=%ld emax=%ld\n\x00", iqlibc.ppVaList(cgbp+720, X__gmpfr_emin, X__gmpfr_emax))
							Xprintf(cgtls, "b = \x00", 0)
							Xmpfr_dump(cgtls, aap[aai])
							Xprintf(cgtls, "c = \x00", 0)
							Xmpfr_dump(cgtls, aap[aai+ppint32(1)])
							Xprintf(cgtls, "Expected \x00", 0)
							Xmpfr_dump(cgtls, cgbp+560)
							Xprintf(cgtls, "with inex = %d and flags =\x00", iqlibc.ppVaList(cgbp+720, aainex_y))
							Xflags_out(cgtls, aaflags_y)
							Xprintf(cgtls, "Got      \x00", 0)
							Xmpfr_dump(cgtls, cgbp+592)
							Xprintf(cgtls, "with inex = %d and flags =\x00", iqlibc.ppVaList(cgbp+720, aainexact))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}

						/* Do the following 2-bit precision test only in RNDA. */
						if aarnd != ppint32(ecMPFR_RNDA) {
							goto cg_35
						}

						Xmpfr_clear_flags(cgtls)
						aainexact = Xmpfr_sub(cgtls, cgbp+624, aap[aai], aap[aai+ppint32(1)], ppint32(ecMPFR_RNDA))
						aaflags = X__gmpfr_flags
						if aae0 < ppint64(0x7fffffffffffffff) {
							aae0++
						}

						if aae0 < X__gmpfr_emin {

							aaflags_y = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
							aainex_y = Xmpfr_underflow(cgtls, cgbp+560, ppint32(ecMPFR_RNDA), aasign)
						} else {
							if aae0 > X__gmpfr_emax {

								aaflags_y = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
								aainex_y = Xmpfr_overflow(cgtls, cgbp+560, ppint32(ecMPFR_RNDA), aasign)
							} else {

								Xmpfr_set_si_2exp(cgtls, cgbp+560, ppint64(aasign), aae0-ppint64(1), ppint32(ecMPFR_RNDN))
								aaflags_y = ppuint32(mvMPFR_FLAGS_INEXACT)
								aainex_y = aasign
							}
						}

						goto pptestw
					pptestw:
						;
						if aaflags != aaflags_y || !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex_y > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex_y < iqlibc.ppInt32FromInt32(0))) || !(Xmpfr_equal_p(cgtls, cgbp+560, cgbp+624) != 0) {

							Xprintf(cgtls, "Error 3 in test_ubf with j=%d k=%d neg=%d i=%d rnd=%s\n\x00", iqlibc.ppVaList(cgbp+720, aaj, aak, aaneg, aai, Xmpfr_print_rnd_mode(cgtls, aarnd)))
							Xprintf(cgtls, "emin=%ld emax=%ld\n\x00", iqlibc.ppVaList(cgbp+720, X__gmpfr_emin, X__gmpfr_emax))
							Xprintf(cgtls, "b = \x00", 0)
							Xmpfr_dump(cgtls, aap[aai])
							Xprintf(cgtls, "c = \x00", 0)
							if aai <= ppint32(8) {
								ccv52 = aai + ppint32(1)
							} else {
								ccv52 = ppint32(9)
							}
							Xmpfr_dump(cgtls, aap[ccv52])
							Xprintf(cgtls, "Expected \x00", 0)
							/* Set y to a 2-bit precision just for the output.
							   Since we exit, this will have no other effect. */
							Xmpfr_prec_round(cgtls, cgbp+560, ppint64(2), ppint32(ecMPFR_RNDA))
							Xmpfr_dump(cgtls, cgbp+560)
							Xprintf(cgtls, "with inex = %d and flags =\x00", iqlibc.ppVaList(cgbp+720, aainex_y))
							Xflags_out(cgtls, aaflags_y)
							Xprintf(cgtls, "Got      \x00", 0)
							Xmpfr_dump(cgtls, cgbp+624)
							Xprintf(cgtls, "with inex = %d and flags =\x00", iqlibc.ppVaList(cgbp+720, aainexact))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}

						goto cg_35
					cg_35:
						;
						aai += ppint32(2)
					}
					goto cg_34
				cg_34:
					;
					aarnd++
				}

				aai = 0
				for {
					if !(aai < ppint32(11)) {
						break
					}
					(*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_sign = -(*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_sign
					goto cg_53
				cg_53:
					;
					aai++
				}

				goto cg_32
			cg_32:
				;
				aaneg++
			}

			aai = 0
			for {
				if !(aai < ppint32(11)) {
					break
				}
				pp_ = iqlibc.ppUint64FromInt64(8)
				pp_ = iqlibc.ppUint64FromInt64(8)
				X__gmpz_add_ui(cgtls, aap[aai]+32, aap[aai]+32, ppuint64(1))
				goto cg_54
			cg_54:
				;
				aai++
			}

			goto cg_31
		cg_31:
			;
			aak++
		}

		goto cg_25
	cg_25:
		;
		aaj++
	}
	aai = 0
	for {
		if !(aai < ppint32(11)) {
			break
		}

		if ccv56 = (*tn__mpfr_struct)(iqunsafe.ppPointer(aap[aai])).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(4); ccv56 {
			pp_ = iqlibc.ppUint64FromInt64(8)
			Xmpfr_mpz_clear(cgtls, aap[aai]+32)
		}
		pp_ = ccv56 && iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		Xmpfr_clear(cgtls, aap[aai])

		goto cg_55
	cg_55:
		;
		aai++
	}

	Xmpfr_clears(cgtls, cgbp+528, iqlibc.ppVaList(cgbp+720, cgbp+560, cgbp+592, cgbp+624, iqlibc.ppUintptrFromInt32(0)))
}

// C documentation
//
//	/* Run the tests on UBF with the maximum exponent range and with a
//	   reduced exponent range. */
func sitest_ubf(cgtls *iqlibc.ppTLS) {

	var aaemax, aaemin tnmpfr_exp_t
	pp_, pp_ = aaemax, aaemin

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax

	Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
	Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
	sitest_ubf_aux(cgtls)

	Xset_emin(cgtls, ppint64(-ppint32(mvREXP)))
	Xset_emax(cgtls, ppint64(mvREXP))
	sitest_ubf_aux(cgtls)

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

/* Generic test file for functions with one or two arguments (the second being
   either mpfr_t or double or unsigned long).

Copyright 2001-2023 Free Software Foundation, Inc.
Contributed by the AriC and Caramba projects, INRIA.

This file is part of the GNU MPFR Library.

The GNU MPFR Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The GNU MPFR Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

/* Define TWO_ARGS for two-argument functions like mpfr_pow.
   Define DOUBLE_ARG1 or DOUBLE_ARG2 for function with a double operand in
   first or second place like sub_d or d_sub.
   Define ULONG_ARG1 or ULONG_ARG2 for function with an unsigned long
   operand in first or second place like sub_ui or ui_sub.
   Define THREE_ARGS for three-argument functions like mpfr_atan2u. */

/* TODO: Add support for type long and extreme integer values, as done
   in tgeneric_ui.c; then tgeneric_ui.c could probably disappear. */

/* For the random function: one number on two is negative. */

/* For the random function: one number on two is negative. */

/* If the MPFR_SUSPICIOUS_OVERFLOW test fails but this is not a bug,
   then define TGENERIC_SO_TEST with an adequate test (possibly 0) to
   omit this particular case. */

/* For some functions (for example cos), the argument reduction is too
   expensive when using mpfr_get_emax(). Then simply define REDUCE_EMAX
   to some reasonable value before including tgeneric.c. */

/* same for mpfr_get_emin() */

func sitest_generic(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aanmax ppuint32) {
	cgbp := cgtls.ppAlloc(320)
	defer cgtls.ppFree(320)

	var aa_p, aa_p1, aa_p2, aa_p3 tnmpfr_ptr
	var aa_p4 tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, aainfinite_input, aatest_of, aatest_uf, ccv10, ccv13, ccv15, ccv16, ccv17, ccv19, ccv21, ccv24, ccv26, ccv27, ccv28, ccv30, ccv32, ccv35, ccv37, ccv38, ccv39, ccv41, ccv43, ccv46, ccv48, ccv49, ccv50, ccv54, ccv8 ppint32
	var aactrn, aactrt ppuint64
	var aae, aaemax, aaemin, aaoemax, aaoemin, aaold_emax, aaold_emin tnmpfr_exp_t
	var aaex_flags, aaex_flags1, aaflags, aaoldflags tnmpfr_flags_t
	var aan, ccv3 ppuint32
	var aaprec, aaxprec, aayprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv11, ccv14, ccv18, ccv22, ccv25, ccv29, ccv33, ccv36, ccv40, ccv44, ccv47, ccv51, ccv52, ccv53, ccv55, ccv56, ccv57, ccv58, ccv7 ppbool
	var ccv4 ppfloat64
	var pp_ /* t at bp+96 */ tnmpfr_t
	var pp_ /* w at bp+128 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* x2 at bp+224 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* yd at bp+160 */ tnmpfr_t
	var pp_ /* yu at bp+192 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aa_p2, aa_p3, aa_p4, aacompare, aacompare2, aactrn, aactrt, aae, aaemax, aaemin, aaex_flags, aaex_flags1, aaflags, aainexact, aainfinite_input, aan, aaoemax, aaoemin, aaold_emax, aaold_emin, aaoldflags, aaprec, aarnd, aatest_of, aatest_uf, aaxprec, aayprec, ccv10, ccv11, ccv13, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv21, ccv22, ccv24, ccv25, ccv26, ccv27, ccv28, ccv29, ccv3, ccv30, ccv32, ccv33, ccv35, ccv36, ccv37, ccv38, ccv39, ccv4, ccv40, ccv41, ccv43, ccv44, ccv46, ccv47, ccv48, ccv49, ccv50, ccv51, ccv52, ccv53, ccv54, ccv55, ccv56, ccv57, ccv58, ccv7, ccv8
	aactrt = ppuint64(0)
	aactrn = ppuint64(0)

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	Xmpfr_inits2(cgtls, ppint64(mvMPFR_PREC_MIN), cgbp, iqlibc.ppVaList(cgbp+264, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_init2(cgtls, cgbp+224, ppint64(mvMPFR_PREC_MIN))

	/* generic tests */
	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		/* Number of overflow/underflow tests for each precision.
		   Since MPFR uses several algorithms and there may also be
		   early overflow/underflow detection, several tests may be
		   needed to detect a bug. */
		aatest_of = ppint32(3)
		aatest_uf = ppint32(3)

		Xmpfr_set_prec(cgtls, cgbp+64, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+96, aaprec)
		aayprec = aaprec + ppint64(20)
		Xmpfr_set_prec(cgtls, cgbp+32, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+160, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+192, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+128, aayprec)

		/* Note: in precision p1, we test NSPEC special cases. */
		aan = ppuint32(0)
		for {
			if aaprec == aap1 {
				ccv3 = aanmax + ppuint32(9)
			} else {
				ccv3 = aanmax
			}
			if !(aan < ccv3) {
				break
			}

			aainfinite_input = 0

			aaxprec = aaprec
			if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {

				/* In half cases, modify the precision of the inputs:
				   If the base precision (for the result) is small,
				   take a larger input precision in general, else
				   take a smaller precision. */
				if aaprec < ppint64(16) {
					ccv4 = ppfloat64(256)
				} else {
					ccv4 = ppfloat64(1)
				}
				aaxprec = tnmpfr_prec_t(ppfloat64(aaxprec) * (ccv4 * ppfloat64(Xrandlimb(cgtls)) / ppfloat64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1)))))
				if aaxprec < ppint64(mvMPFR_PREC_MIN) {
					aaxprec = ppint64(mvMPFR_PREC_MIN)
				}
			}
			Xmpfr_set_prec(cgtls, cgbp, aaxprec)
			Xmpfr_set_prec(cgtls, cgbp+224, aaxprec)

			/* Generate random arguments, even in the special cases
			   (this may not be needed, but this is simpler).
			   Note that if RAND_FUNCTION is defined, this specific
			   random function is used for all arguments; this is
			   typically mpfr_random2, which generates a positive
			   random mpfr_t with long runs of consecutive ones and
			   zeros in the binary representation. */

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_random2(cgtls, cgbp, ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt64FromInt32(1), iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%ppuint64(100)), ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_random2(cgtls, cgbp+224, ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt64FromInt32(1), iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%ppuint64(100)), ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))

			if aan < ppuint32(9) && aaprec == aap1 {

				/* Special cases tested in precision p1 if n < NSPEC. They are
				   useful really in the extended exponent range. */
				/* TODO: x2 is set even when it is associated with a double;
				   check whether this really makes sense. */
				Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
				Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
				if aan == ppuint32(0) {

					Xmpfr_set_nan(cgtls, cgbp)
				} else {
					if aan <= ppuint32(2) {

						if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(1) || aan == ppuint32(2))), ppint64(1)) != 0; !ccv7 {
							Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(290), "n == 1 || n == 2\x00")
						}
						pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
						if aan == ppuint32(1) {
							ccv8 = ppint32(1)
						} else {
							ccv8 = -ppint32(1)
						}
						pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv8), 0, ppint32(ecMPFR_RNDN))
						Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)

						if ccv11 = iqlibc.Bool(0 != 0); ccv11 {
							if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
								ccv10 = -ppint32(1)
							} else {
								ccv10 = ppint32(1)
							}
						}
						if ccv11 && ppint64(ccv10) >= 0 {
							if ccv14 = iqlibc.Bool(0 != 0); ccv14 {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv13 = -ppint32(1)
								} else {
									ccv13 = ppint32(1)
								}
							}
							if ccv14 && iqlibc.ppUint64FromInt64(ppint64(ccv13)) == ppuint64(0) {
								{
									aa_p = cgbp + 224
									(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
									(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
									pp_ = ppint32(ecMPFR_RNDN)
									ccv15 = 0
								}
								pp_ = ccv15
							} else {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv16 = -ppint32(1)
								} else {
									ccv16 = ppint32(1)
								}
								Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv16)), 0, ppint32(ecMPFR_RNDN))
							}
						} else {
							if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
								ccv17 = -ppint32(1)
							} else {
								ccv17 = ppint32(1)
							}
							Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv17), 0, ppint32(ecMPFR_RNDN))
						}
						Xmpfr_set_exp(cgtls, cgbp+224, X__gmpfr_emin)
					} else {
						if aan <= ppuint32(4) {

							if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(3) || aan == ppuint32(4))), ppint64(1)) != 0; !ccv18 {
								Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(300), "n == 3 || n == 4\x00")
							}
							pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
							if aan == ppuint32(3) {
								ccv19 = ppint32(1)
							} else {
								ccv19 = -ppint32(1)
							}
							pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv19), 0, ppint32(ecMPFR_RNDN))
							Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)

							if ccv22 = iqlibc.Bool(0 != 0); ccv22 {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv21 = -ppint32(1)
								} else {
									ccv21 = ppint32(1)
								}
							}
							if ccv22 && ppint64(ccv21) >= 0 {
								if ccv25 = iqlibc.Bool(0 != 0); ccv25 {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv24 = -ppint32(1)
									} else {
										ccv24 = ppint32(1)
									}
								}
								if ccv25 && iqlibc.ppUint64FromInt64(ppint64(ccv24)) == ppuint64(0) {
									{
										aa_p1 = cgbp + 224
										(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
										(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
										pp_ = ppint32(ecMPFR_RNDN)
										ccv26 = 0
									}
									pp_ = ccv26
								} else {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv27 = -ppint32(1)
									} else {
										ccv27 = ppint32(1)
									}
									Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv27)), 0, ppint32(ecMPFR_RNDN))
								}
							} else {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv28 = -ppint32(1)
								} else {
									ccv28 = ppint32(1)
								}
								Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv28), 0, ppint32(ecMPFR_RNDN))
							}
							Xmpfr_setmax(cgtls, cgbp+224, X__gmpfr_emax)
						} else {
							if aan <= ppuint32(6) {

								if ccv29 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(5) || aan == ppuint32(6))), ppint64(1)) != 0; !ccv29 {
									Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(311), "n == 5 || n == 6\x00")
								}
								pp_ = ccv29 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
								if aan == ppuint32(5) {
									ccv30 = ppint32(1)
								} else {
									ccv30 = -ppint32(1)
								}
								pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv30), 0, ppint32(ecMPFR_RNDN))
								Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)

								if ccv33 = iqlibc.Bool(0 != 0); ccv33 {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv32 = -ppint32(1)
									} else {
										ccv32 = ppint32(1)
									}
								}
								if ccv33 && ppint64(ccv32) >= 0 {
									if ccv36 = iqlibc.Bool(0 != 0); ccv36 {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv35 = -ppint32(1)
										} else {
											ccv35 = ppint32(1)
										}
									}
									if ccv36 && iqlibc.ppUint64FromInt64(ppint64(ccv35)) == ppuint64(0) {
										{
											aa_p2 = cgbp + 224
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_sign = ppint32(1)
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
											pp_ = ppint32(ecMPFR_RNDN)
											ccv37 = 0
										}
										pp_ = ccv37
									} else {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv38 = -ppint32(1)
										} else {
											ccv38 = ppint32(1)
										}
										Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv38)), 0, ppint32(ecMPFR_RNDN))
									}
								} else {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv39 = -ppint32(1)
									} else {
										ccv39 = ppint32(1)
									}
									Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv39), 0, ppint32(ecMPFR_RNDN))
								}
								Xmpfr_setmax(cgtls, cgbp+224, X__gmpfr_emax)
							} else {

								if ccv40 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(7) || aan == ppuint32(8))), ppint64(1)) != 0; !ccv40 {
									Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(319), "n == 7 || n == 8\x00")
								}
								pp_ = ccv40 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
								if aan == ppuint32(7) {
									ccv41 = ppint32(1)
								} else {
									ccv41 = -ppint32(1)
								}
								pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv41), 0, ppint32(ecMPFR_RNDN))
								Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)

								if ccv44 = iqlibc.Bool(0 != 0); ccv44 {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv43 = -ppint32(1)
									} else {
										ccv43 = ppint32(1)
									}
								}
								if ccv44 && ppint64(ccv43) >= 0 {
									if ccv47 = iqlibc.Bool(0 != 0); ccv47 {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv46 = -ppint32(1)
										} else {
											ccv46 = ppint32(1)
										}
									}
									if ccv47 && iqlibc.ppUint64FromInt64(ppint64(ccv46)) == ppuint64(0) {
										{
											aa_p3 = cgbp + 224
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_sign = ppint32(1)
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
											pp_ = ppint32(ecMPFR_RNDN)
											ccv48 = 0
										}
										pp_ = ccv48
									} else {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv49 = -ppint32(1)
										} else {
											ccv49 = ppint32(1)
										}
										Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv49)), 0, ppint32(ecMPFR_RNDN))
									}
								} else {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv50 = -ppint32(1)
									} else {
										ccv50 = ppint32(1)
									}
									Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv50), 0, ppint32(ecMPFR_RNDN))
								}
								Xmpfr_set_exp(cgtls, cgbp+224, X__gmpfr_emin)
							}
						}
					}
				}
			}

			/* Exponent range for the test. */
			aaoemin = X__gmpfr_emin
			aaoemax = X__gmpfr_emax

			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt32(ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)))
			Xmpfr_clear_flags(cgtls)
			aacompare = sitest_sub(cgtls, cgbp+32, cgbp, cgbp+224, aarnd)
			aaflags = X__gmpfr_flags
			if X__gmpfr_emin != aaoemin || X__gmpfr_emax != aaoemax {

				Xprintf(cgtls, "tgeneric: the exponent range has been modified by the tested function!\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
			if aarnd != ppint32(ecMPFR_RNDF) {
				if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) == iqlibc.ppInt32FromInt32(0)) != 0) {
					Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad inexact flag for test_sub\x00"))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					if cgbp+224 != ppuintptr(0) {
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+224)
					}
					if -ppint32(1) >= 0 {
						Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
					}
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}
			aactrt++

			/* If rnd = RNDF, check that we obtain the same result as
			   RNDD or RNDU. */
			if aarnd == ppint32(ecMPFR_RNDF) {

				sitest_sub(cgtls, cgbp+160, cgbp, cgbp+224, ppint32(ecMPFR_RNDD))
				sitest_sub(cgtls, cgbp+192, cgbp, cgbp+224, ppint32(ecMPFR_RNDU))
				if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+160) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_sign || ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+192) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_sign)) {

					Xprintf(cgtls, "tgeneric: error fortest_sub, RNDF; result matches neither RNDD nor RNDU\n\x00", 0)
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "x2 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+224)
					Xprintf(cgtls, "yd (RNDD) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+160)
					Xprintf(cgtls, "yu (RNDU) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+192)
					Xprintf(cgtls, "y  (RNDF) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xexit(cgtls, ppint32(1))
				}
			}

			/* Tests in a reduced exponent range. */

			aaoldflags = aaflags

			/* Determine the smallest exponent range containing the
			   exponents of the mpfr_t inputs (x, and u if TWO_ARGS)
			   and output (y). */
			aaemin = iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) - iqlibc.ppInt64FromInt32(1)
			aaemax = iqlibc.ppInt64FromInt32(1) - iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))

			if ccv51 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv51 {
			}
			if ccv51 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv52 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv52 {
			}
			if ccv52 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 224)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv53 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv53 {
			}
			if ccv53 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp /* exponent of the result */

				if aatest_of > 0 && aae-ppint64(1) >= aaemax { /* overflow test */

					/* Exponent e of the result > exponents of the inputs;
					   let's set emax to e - 1, so that one should get an
					   overflow. */
					Xset_emax(cgtls, aae-ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = sitest_sub(cgtls, cgbp+128, cgbp, cgbp+224, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emax(cgtls, aaoemax)
					aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for test_sub, reduced exponent range [%ld,%ld] (overflow test) on:\n\x00", iqlibc.ppVaList(cgbp+264, aaoemin, aae-ppint64(1)))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+224)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+264, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_of--
				}

				if aatest_uf > 0 && aae+ppint64(1) <= aaemin { /* underflow test */

					/* Exponent e of the result < exponents of the inputs;
					   let's set emin to e + 1, so that one should get an
					   underflow. */
					Xset_emin(cgtls, aae+ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = sitest_sub(cgtls, cgbp+128, cgbp, cgbp+224, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emin(cgtls, aaoemin)
					aaex_flags1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags1 && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for test_sub, reduced exponent range [%ld,%ld] (underflow test) on:\n\x00", iqlibc.ppVaList(cgbp+264, aae+ppint64(1), aaoemax))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+224)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags1)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+264, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_uf--
				}

				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			} /* MPFR_IS_PURE_FP (y) */

			if aaemin > aaemax {
				aaemin = aaemax
			} /* case where all values are singular */

			/* Consistency test in a reduced exponent range. Doing it
			   for the first 10 samples and for prec == p1 (which has
			   some special cases) should be sufficient. */
			if aactrt <= ppuint64(10) || aaprec == aap1 {

				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				Xmpfr_clear_flags(cgtls)
				aainexact = sitest_sub(cgtls, cgbp+128, cgbp, cgbp+224, aarnd)
				aaflags = X__gmpfr_flags
				Xset_emin(cgtls, aaoemin)
				Xset_emax(cgtls, aaoemax)
				/* That test makes no sense for RNDF. */
				if aarnd != ppint32(ecMPFR_RNDF) && !(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+128, cgbp+32) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign) && iqlibc.ppBoolInt32(aainexact > 0)-iqlibc.ppBoolInt32(aainexact < 0) == iqlibc.ppBoolInt32(aacompare > 0)-iqlibc.ppBoolInt32(aacompare < 0) && aaflags == aaoldflags) {

					Xprintf(cgtls, "tgeneric: error for test_sub, reduced exponent range [%ld,%ld] on:\n\x00", iqlibc.ppVaList(cgbp+264, aaemin, aaemax))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "x2 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+224)
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "Expected:\n  y = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+264, aacompare))
					Xflags_out(cgtls, aaoldflags)
					Xprintf(cgtls, "Got:\n  w = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+128)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+264, aainexact))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}

			X__gmpfr_flags = aaoldflags /* restore the flags */
			/* tests in a reduced exponent range */

			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 {
					if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad NaN flag for test_sub\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

						if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad overflow flag for test_sub\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(iqlibc.ppBoolInt32(aacompare == 0 && !(aainfinite_input != 0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad divide-by-zero flag for test_sub\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) {
							if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad underflow flag for test_sub\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+224 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+224)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						}
					}
				}
			} else {
				if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0 {

					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "both overflow and divide-by-zero for test_sub\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "both underflow and divide-by-zero for test_sub\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !(aacompare == iqlibc.ppInt32FromInt32(0)) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad compare value (divide-by-zero) for test_sub\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 {

						if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "both underflow and overflow for test_sub\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad compare value (overflow) for test_sub\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						Xmpfr_nexttoinf(cgtls, cgbp+32)
						if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "should have been max MPFR number (overflow) for test_sub\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {

							if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad compare value (underflow) for test_sub\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+224 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+224)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
							Xmpfr_nexttozero(cgtls, cgbp+32)
							if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "should have been min MPFR number (underflow) for test_sub\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+224 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+224)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						} else {
							if aacompare == 0 || aarnd == ppint32(ecMPFR_RNDF) || Xmpfr_can_round(cgtls, cgbp+32, aayprec, aarnd, aarnd, aaprec) != 0 {

								aactrn++
								{
									aa_p4 = cgbp + 32
									ccv54 = Xmpfr_set4(cgtls, cgbp+96, aa_p4, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p4)).fd_mpfr_sign)
								}
								pp_ = ccv54
								/* Risk of failures are known when some flags are already set
								   before the function call. Do not set the erange flag, as
								   it will remain set after the function call and no checks
								   are performed in such a case (see the mpfr_erangeflag_p
								   test below). */
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									X__gmpfr_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0) ^ iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE))
								}
								/* Let's increase the precision of the inputs in a random way.
								   In most cases, this doesn't make any difference, but for
								   the mpfr_fmod bug fixed in r6230, this triggers the bug. */
								Xmpfr_prec_round(cgtls, cgbp, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint64(15)), ppint32(ecMPFR_RNDN))
								Xmpfr_prec_round(cgtls, cgbp+224, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint64(15)), ppint32(ecMPFR_RNDN))
								aainexact = sitest_sub(cgtls, cgbp+64, cgbp, cgbp+224, aarnd)
								if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0 {
									goto ppnext_n
								}
								if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+64) != 0) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "tgeneric: results differ for test_sub on\n\x00", 0)
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp+224)
									Xprintf(cgtls, "prec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
									Xprintf(cgtls, "Got      \x00", 0)
									Xmpfr_dump(cgtls, cgbp+64)
									Xprintf(cgtls, "Expected \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xprintf(cgtls, "Approx   \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xexit(cgtls, ppint32(1))
								}
								aacompare2 = Xmpfr_cmp3(cgtls, cgbp+96, cgbp+32, ppint32(1))
								/* if rounding to nearest, cannot know the sign of t - f(x)
								   because of composed rounding: y = o(f(x)) and t = o(y) */
								if aacompare*aacompare2 >= 0 {
									aacompare = aacompare + aacompare2
								} else {
									aacompare = aainexact
								} /* cannot determine sign(t-f(x)) */
								if !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aacompare > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aacompare < iqlibc.ppInt32FromInt32(0))) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+264, Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare, aainexact))
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp+224)
									Xprintf(cgtls, "y = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xprintf(cgtls, "t = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xexit(cgtls, ppint32(1))
								}
							} else {
								if Xgetenv(cgtls, "MPFR_SUSPICIOUS_OVERFLOW\x00") != iqlibc.ppUintptrFromInt32(0) {

									/* For developers only! */

									if ccv55 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv55 {
									}
									if ccv56 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv55 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0))), ppint64(1)) != 0; !ccv56 {
										Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(803), "(!(((y)->_mpfr_exp) <= (((-0x7fffffffffffffffL-1))+3)) && ((! __builtin_constant_p (!!(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) || !(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0))) || (((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) ? (void) 0 : __builtin_unreachable()), 1))\x00")
									}
									pp_ = ccv56 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
									Xmpfr_nexttoinf(cgtls, cgbp+32)

									if ccv58 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3); ccv58 {
										if ccv57 = aarnd == ppint32(ecMPFR_RNDZ); !ccv57 {
										}
									}
									if ccv58 && (ccv57 || aarnd+iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)) && !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) && iqlibc.Bool(ppint32(mvTGENERIC_SO_TEST) != 0) {

										Xprintf(cgtls, "Possible bug! |y| is the maximum finite number (with yprec = %u) and has\nbeen obtained when rounding toward zero (%s). Thus there is a very\nprobable overflow, but the overflow flag is not set!\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
										Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp)
										Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp+224)
										Xexit(cgtls, ppint32(1))
									}
								}
							}
						}
					}
				}
			}

			goto ppnext_n
		ppnext_n:
			;
			/* In case the exponent range has been changed by
			   tests_default_random() or for special values... */
			Xset_emin(cgtls, aaold_emin)
			Xset_emax(cgtls, aaold_emax)

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	if Xgetenv(cgtls, "MPFR_TGENERIC_STAT\x00") != iqlibc.ppUintptrFromInt32(0) {
		Xprintf(cgtls, "tgeneric: normal cases / total = %lu / %lu\n\x00", iqlibc.ppVaList(cgbp+264, aactrn, aactrt))
	}

	if ppuint64(3)*aactrn < ppuint64(2)*aactrt {
		Xprintf(cgtls, "Warning! Too few normal cases in generic tests (%lu / %lu)\n\x00", iqlibc.ppVaList(cgbp+264, aactrn, aactrt))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+264, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_clear(cgtls, cgbp+224)
}

func Xmain(cgtls *iqlibc.ppTLS, cgargc ppint32, cgargv ppuintptr) (cgr ppint32) {

	var aai ppuint32
	var aap tnmpfr_prec_t
	pp_, pp_ = aai, aap

	Xtests_start_mpfr(cgtls)

	sitest_rndf(cgtls)
	sitestall_rndf(cgtls, ppint64(7))
	sitest_rndf_exact(cgtls, ppint64(200))
	sibug20101017(cgtls)
	sicheck_rounding(cgtls)
	sicheck_diverse(cgtls)
	sicheck_inexact(cgtls)
	sicheck_max_almosteven(cgtls)
	sibug_ddefour(cgtls)
	sibug20180215(cgtls)
	sibug20180216(cgtls)
	sibug20180217(cgtls)
	aap = ppint64(2)
	for {
		if !(aap < ppint64(200)) {
			break
		}
		aai = ppuint32(0)
		for {
			if !(aai < ppuint32(50)) {
				break
			}
			sicheck_two_sum(cgtls, aap)
			goto cg_2
		cg_2:
			;
			aai++
		}
		goto cg_1
	cg_1:
		;
		aap++
	}
	sitest_generic(cgtls, ppint64(mvMPFR_PREC_MIN), ppint64(800), ppuint32(100))
	sitest_ubf(cgtls)

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___builtin_unreachable(*iqlibc.ppTLS)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuint64

func ___gmpz_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpz_set(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpz_sub_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _mpfr_add(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_can_round(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32, ppint32, ppint64) ppint32

func _mpfr_check(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_check_range(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp3(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_cmp_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64) ppint32

func _mpfr_cmpabs(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_div_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_get_z(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_inits2(*iqlibc.ppTLS, ppint64, ppuintptr, ppuintptr)

func _mpfr_log(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_mpz_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mpz_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mul_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_mul_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttoinf(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttozero(*iqlibc.ppTLS, ppuintptr)

func _mpfr_overflow(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_prec_round(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32) ppint32

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_random2(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppuintptr)

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppint8

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_exp(*iqlibc.ppTLS, ppuintptr, ppint64) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_setmax(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_setmin(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_sgn(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_sub_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_swap(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_ubf_zexp2exp(*iqlibc.ppTLS, ppuintptr) ppint64

func _mpfr_underflow(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _putchar(*iqlibc.ppTLS, ppint32) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

var _stdout ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

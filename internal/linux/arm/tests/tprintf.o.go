// Code generated for linux/arm by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DMPFR_LONG_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/arm -UHAVE_NEARBYINT -c -o tprintf.o.go tprintf.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvE2BIG = 7
const mvEACCES = 13
const mvEADDRINUSE = 98
const mvEADDRNOTAVAIL = 99
const mvEADV = 68
const mvEAFNOSUPPORT = 97
const mvEAGAIN = 11
const mvEALREADY = 114
const mvEBADE = 52
const mvEBADF = 9
const mvEBADFD = 77
const mvEBADMSG = 74
const mvEBADR = 53
const mvEBADRQC = 56
const mvEBADSLT = 57
const mvEBFONT = 59
const mvEBUSY = 16
const mvECANCELED = 125
const mvECHILD = 10
const mvECHRNG = 44
const mvECOMM = 70
const mvECONNABORTED = 103
const mvECONNREFUSED = 111
const mvECONNRESET = 104
const mvEDEADLK = 35
const mvEDEADLOCK = "EDEADLK"
const mvEDESTADDRREQ = 89
const mvEDOM = 33
const mvEDOTDOT = 73
const mvEDQUOT = 122
const mvEEXIST = 17
const mvEFAULT = 14
const mvEFBIG = 27
const mvEHOSTDOWN = 112
const mvEHOSTUNREACH = 113
const mvEHWPOISON = 133
const mvEIDRM = 43
const mvEILSEQ = 84
const mvEINPROGRESS = 115
const mvEINTR = 4
const mvEINVAL = 22
const mvEIO = 5
const mvEISCONN = 106
const mvEISDIR = 21
const mvEISNAM = 120
const mvEKEYEXPIRED = 127
const mvEKEYREJECTED = 129
const mvEKEYREVOKED = 128
const mvEL2HLT = 51
const mvEL2NSYNC = 45
const mvEL3HLT = 46
const mvEL3RST = 47
const mvELIBACC = 79
const mvELIBBAD = 80
const mvELIBEXEC = 83
const mvELIBMAX = 82
const mvELIBSCN = 81
const mvELNRNG = 48
const mvELOOP = 40
const mvEMEDIUMTYPE = 124
const mvEMFILE = 24
const mvEMLINK = 31
const mvEMSGSIZE = 90
const mvEMULTIHOP = 72
const mvENAMETOOLONG = 36
const mvENAVAIL = 119
const mvENETDOWN = 100
const mvENETRESET = 102
const mvENETUNREACH = 101
const mvENFILE = 23
const mvENOANO = 55
const mvENOBUFS = 105
const mvENOCSI = 50
const mvENODATA = 61
const mvENODEV = 19
const mvENOENT = 2
const mvENOEXEC = 8
const mvENOKEY = 126
const mvENOLCK = 37
const mvENOLINK = 67
const mvENOMEDIUM = 123
const mvENOMEM = 12
const mvENOMSG = 42
const mvENONET = 64
const mvENOPKG = 65
const mvENOPROTOOPT = 92
const mvENOSPC = 28
const mvENOSR = 63
const mvENOSTR = 60
const mvENOSYS = 38
const mvENOTBLK = 15
const mvENOTCONN = 107
const mvENOTDIR = 20
const mvENOTEMPTY = 39
const mvENOTNAM = 118
const mvENOTRECOVERABLE = 131
const mvENOTSOCK = 88
const mvENOTSUP = "EOPNOTSUPP"
const mvENOTTY = 25
const mvENOTUNIQ = 76
const mvENXIO = 6
const mvEOPNOTSUPP = 95
const mvEOVERFLOW = 75
const mvEOWNERDEAD = 130
const mvEPERM = 1
const mvEPFNOSUPPORT = 96
const mvEPIPE = 32
const mvEPROTO = 71
const mvEPROTONOSUPPORT = 93
const mvEPROTOTYPE = 91
const mvERANGE = 34
const mvEREMCHG = 78
const mvEREMOTE = 66
const mvEREMOTEIO = 121
const mvERESTART = 85
const mvERFKILL = 132
const mvEROFS = 30
const mvESHUTDOWN = 108
const mvESOCKTNOSUPPORT = 94
const mvESPIPE = 29
const mvESRCH = 3
const mvESRMNT = 69
const mvESTALE = 116
const mvESTRPIPE = 86
const mvETIME = 62
const mvETIMEDOUT = 110
const mvETOOMANYREFS = 109
const mvETXTBSY = 26
const mvEUCLEAN = 117
const mvEUNATCH = 49
const mvEUSERS = 87
const mvEWOULDBLOCK = "EAGAIN"
const mvEXDEV = 18
const mvEXFULL = 54
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFMT_SIZE = 13
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 32
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 0x7fffffffffffffff
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT32_MAX"
const mvINTPTR_MIN = "INT32_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 2147483647
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_MAX = "__LONG_MAX"
const mvLS1 = "%Rb %512d"
const mvLS2 = "%RA %RA %Ra %Ra %512d"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 2972
const mvMPFR_AI_THRESHOLD3 = 44718
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 277
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 10666
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_FSPEC = "j"
const mvMPFR_INTMAX_MAX = "INTMAX_MAX"
const mvMPFR_INTMAX_MIN = "INTMAX_MIN"
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 5
const mvMPFR_LOG2_PREC_BITS = 5
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 22
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 32
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 28990
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 24
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/arm/mparam.h"
const mvMPFR_UINTMAX_MAX = "UINTMAX_MAX"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvN0 = 20
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_L = 1
const mvPRINTF_T = 1
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT32_MAX"
const mvPTRDIFF_MIN = "INT32_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZE_MAX = "UINT32_MAX"
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSTDOUT_FILENO = 1
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = "0xffffffffffffffffu"
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT32_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_H_HAVE_VA_LIST = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_H_HAVE_INTMAX_T = 1
const mv_MPFR_H_HAVE_VA_LIST = 1
const mv_MPFR_H_HAVE_VA_LIST_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_REDIR_TIME64 = 1
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ACCUM_EPSILON__ = "0x1P-15K"
const mv__ACCUM_FBIT__ = 15
const mv__ACCUM_IBIT__ = 16
const mv__ACCUM_MAX__ = "0X7FFFFFFFP-15K"
const mv__APCS_32__ = 1
const mv__ARMEL__ = 1
const mv__ARM_32BIT_STATE = 1
const mv__ARM_ARCH = 6
const mv__ARM_ARCH_6__ = 1
const mv__ARM_ARCH_ISA_ARM = 1
const mv__ARM_ARCH_ISA_THUMB = 1
const mv__ARM_EABI__ = 1
const mv__ARM_FEATURE_CLZ = 1
const mv__ARM_FEATURE_COPROC = 15
const mv__ARM_FEATURE_DSP = 1
const mv__ARM_FEATURE_LDREX = 4
const mv__ARM_FEATURE_QBIT = 1
const mv__ARM_FEATURE_SAT = 1
const mv__ARM_FEATURE_SIMD32 = 1
const mv__ARM_FEATURE_UNALIGNED = 1
const mv__ARM_FP = 12
const mv__ARM_PCS_VFP = 1
const mv__ARM_SIZEOF_MINIMAL_ENUM = 4
const mv__ARM_SIZEOF_WCHAR_T = 4
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 8
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DA_FBIT__ = 31
const mv__DA_IBIT__ = 32
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__DQ_FBIT__ = 63
const mv__DQ_IBIT__ = 0
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.2204460492503131e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.7976931348623157e+308
const mv__FLT32X_MIN__ = 2.2250738585072014e-308
const mv__FLT32X_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.1920928955078125e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.4028234663852886e+38
const mv__FLT32_MIN__ = 1.1754943508222875e-38
const mv__FLT32_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.2204460492503131e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.7976931348623157e+308
const mv__FLT64_MIN__ = 2.2250738585072014e-308
const mv__FLT64_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.1920928955078125e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.4028234663852886e+38
const mv__FLT_MIN__ = 1.1754943508222875e-38
const mv__FLT_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT_RADIX__ = 2
const mv__FRACT_EPSILON__ = "0x1P-15R"
const mv__FRACT_FBIT__ = 15
const mv__FRACT_IBIT__ = 0
const mv__FRACT_MAX__ = "0X7FFFP-15R"
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 1
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 1
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 1
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 1
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 1
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "arm-linux-gnueabihf-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__GXX_TYPEINFO_EQUALITY_INLINE = 0
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__HA_FBIT__ = 7
const mv__HA_IBIT__ = 8
const mv__HQ_FBIT__ = 15
const mv__HQ_IBIT__ = 0
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LACCUM_EPSILON__ = "0x1P-31LK"
const mv__LACCUM_FBIT__ = 31
const mv__LACCUM_IBIT__ = 32
const mv__LACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LK"
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.2204460492503131e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.7976931348623157e+308
const mv__LDBL_MIN__ = 2.2250738585072014e-308
const mv__LDBL_NORM_MAX__ = 1.7976931348623157e+308
const mv__LFRACT_EPSILON__ = "0x1P-31LR"
const mv__LFRACT_FBIT__ = 31
const mv__LFRACT_IBIT__ = 0
const mv__LFRACT_MAX__ = "0X7FFFFFFFP-31LR"
const mv__LITTLE_ENDIAN = 1234
const mv__LLACCUM_EPSILON__ = "0x1P-31LLK"
const mv__LLACCUM_FBIT__ = 31
const mv__LLACCUM_IBIT__ = 32
const mv__LLACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LLK"
const mv__LLFRACT_EPSILON__ = "0x1P-63LLR"
const mv__LLFRACT_FBIT__ = 63
const mv__LLFRACT_IBIT__ = 0
const mv__LLFRACT_MAX__ = "0X7FFFFFFFFFFFFFFFP-63LLR"
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 2147483647
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "ll"
const mv__PRIPTR = ""
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__QQ_FBIT__ = 7
const mv__QQ_IBIT__ = 0
const mv__SACCUM_EPSILON__ = "0x1P-7HK"
const mv__SACCUM_FBIT__ = 7
const mv__SACCUM_IBIT__ = 8
const mv__SACCUM_MAX__ = "0X7FFFP-7HK"
const mv__SA_FBIT__ = 15
const mv__SA_IBIT__ = 16
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SFRACT_EPSILON__ = "0x1P-7HR"
const mv__SFRACT_FBIT__ = 7
const mv__SFRACT_IBIT__ = 0
const mv__SFRACT_MAX__ = "0X7FP-7HR"
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__SQ_FBIT__ = 31
const mv__SQ_IBIT__ = 0
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__TA_FBIT__ = 63
const mv__TA_IBIT__ = 64
const mv__THUMB_INTERWORK__ = 1
const mv__TQ_FBIT__ = 127
const mv__TQ_IBIT__ = 0
const mv__UACCUM_EPSILON__ = "0x1P-16UK"
const mv__UACCUM_FBIT__ = 16
const mv__UACCUM_IBIT__ = 16
const mv__UACCUM_MAX__ = "0XFFFFFFFFP-16UK"
const mv__UACCUM_MIN__ = "0.0UK"
const mv__UDA_FBIT__ = 32
const mv__UDA_IBIT__ = 32
const mv__UDQ_FBIT__ = 64
const mv__UDQ_IBIT__ = 0
const mv__UFRACT_EPSILON__ = "0x1P-16UR"
const mv__UFRACT_FBIT__ = 16
const mv__UFRACT_IBIT__ = 0
const mv__UFRACT_MAX__ = "0XFFFFP-16UR"
const mv__UFRACT_MIN__ = "0.0UR"
const mv__UHA_FBIT__ = 8
const mv__UHA_IBIT__ = 8
const mv__UHQ_FBIT__ = 16
const mv__UHQ_IBIT__ = 0
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__ULACCUM_EPSILON__ = "0x1P-32ULK"
const mv__ULACCUM_FBIT__ = 32
const mv__ULACCUM_IBIT__ = 32
const mv__ULACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULK"
const mv__ULACCUM_MIN__ = "0.0ULK"
const mv__ULFRACT_EPSILON__ = "0x1P-32ULR"
const mv__ULFRACT_FBIT__ = 32
const mv__ULFRACT_IBIT__ = 0
const mv__ULFRACT_MAX__ = "0XFFFFFFFFP-32ULR"
const mv__ULFRACT_MIN__ = "0.0ULR"
const mv__ULLACCUM_EPSILON__ = "0x1P-32ULLK"
const mv__ULLACCUM_FBIT__ = 32
const mv__ULLACCUM_IBIT__ = 32
const mv__ULLACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULLK"
const mv__ULLACCUM_MIN__ = "0.0ULLK"
const mv__ULLFRACT_EPSILON__ = "0x1P-64ULLR"
const mv__ULLFRACT_FBIT__ = 64
const mv__ULLFRACT_IBIT__ = 0
const mv__ULLFRACT_MAX__ = "0XFFFFFFFFFFFFFFFFP-64ULLR"
const mv__ULLFRACT_MIN__ = "0.0ULLR"
const mv__UQQ_FBIT__ = 8
const mv__UQQ_IBIT__ = 0
const mv__USACCUM_EPSILON__ = "0x1P-8UHK"
const mv__USACCUM_FBIT__ = 8
const mv__USACCUM_IBIT__ = 8
const mv__USACCUM_MAX__ = "0XFFFFP-8UHK"
const mv__USACCUM_MIN__ = "0.0UHK"
const mv__USA_FBIT__ = 16
const mv__USA_IBIT__ = 16
const mv__USE_TIME_BITS64 = 1
const mv__USFRACT_EPSILON__ = "0x1P-8UHR"
const mv__USFRACT_FBIT__ = 8
const mv__USFRACT_IBIT__ = 0
const mv__USFRACT_MAX__ = "0XFFP-8UHR"
const mv__USFRACT_MIN__ = "0.0UHR"
const mv__USQ_FBIT__ = 32
const mv__USQ_IBIT__ = 0
const mv__UTA_FBIT__ = 64
const mv__UTA_IBIT__ = 64
const mv__UTQ_FBIT__ = 128
const mv__UTQ_IBIT__ = 0
const mv__VERSION__ = "12.2.0"
const mv__VFP_FP__ = 1
const mv__WCHAR_MAX__ = 0xffffffff
const mv__WCHAR_MIN__ = 0
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__arm__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_sj = "__gmpfr_mpfr_get_sj"
const mvmpfr_get_uj = "__gmpfr_mpfr_get_uj"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpfr_pow_sj = "__gmpfr_mpfr_pow_sj"
const mvmpfr_pow_uj = "__gmpfr_mpfr_pow_uj"
const mvmpfr_pown = "mpfr_pow_sj"
const mvmpfr_set_sj = "__gmpfr_set_sj"
const mvmpfr_set_sj_2exp = "__gmpfr_set_sj_2exp"
const mvmpfr_set_uj = "__gmpfr_set_uj"
const mvmpfr_set_uj_2exp = "__gmpfr_set_uj_2exp"
const mvmpfr_vasprintf = "__gmpfr_vasprintf"
const mvmpfr_vfprintf = "__gmpfr_vfprintf"
const mvmpfr_vprintf = "__gmpfr_vprintf"
const mvmpfr_vsnprintf = "__gmpfr_vsnprintf"
const mvmpfr_vsprintf = "__gmpfr_vsprintf"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppuint32

type tn__predefined_ptrdiff_t = ppint32

type tnva_list = ppuintptr

type tnwchar_t = ppuint32

type tnmax_align_t = struct {
	fd__ccgo_align [0]ppuint32
	fd__ll         ppint64
	fd__ld         ppfloat64
}

type tnsize_t = ppuint32

type tnptrdiff_t = ppint32

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnlocale_t = ppuintptr

type tnssize_t = ppint32

type tnoff_t = ppint64

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__ccgo_align [0]ppuint32
	fd__lldata     [0]ppint64
	fd__align      [0]ppfloat64
	fd__opaque     [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fd__ccgo_align [0]ppuint32
	fdquot         ppint64
	fdrem          ppint64
}

type tnuintptr_t = ppuint32

type tnintptr_t = ppint32

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fd__ccgo_align [0]ppuint32
	fdquot         tnintmax_t
	fdrem          tnintmax_t
}

type tnmpfr_uintmax_t = ppuint64

type tnmpfr_intmax_t = ppint64

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint32

type tnmpfr_ulong = ppuint32

type tnmpfr_size_t = ppuint32

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint32

type tnmpfr_uprec_t = ppuint32

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint32

type tnmpfr_uexp_t = ppuint32

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tsbases = struct {
	fd__ccgo_align          [0]ppuint32
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fd__ccgo_align [0]ppuint32
	fdd            [0]ppfloat64
	fds            struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint32

type tnmpfr_ueexp_t = ppuint32

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

/* unlike other tests, we print out errors to stderr because stdout might be
   redirected */

// C documentation
//
//	/* limit for random precision in random() */
var Xprec_max_printf = ppint32(5000)

// C documentation
//
//	/* boolean: is stdout redirected to a file ? */
var Xstdout_redirect ppint32

func sicheck(cgtls *iqlibc.ppTLS, aafmt ppuintptr, aax tnmpfr_ptr) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	if Xmpfr_printf(cgtls, aafmt, iqlibc.ppVaList(cgbp+8, aax)) == -ppint32(1) {

		Xfprintf(cgtls, Xstderr, "Error 1 in mpfr_printf(\"%s\", ...)\n\x00", iqlibc.ppVaList(cgbp+8, aafmt))

		Xexit(cgtls, ppint32(1))
	}
	Xputchar(cgtls, ppint32('\n'))
}

func sicheck_vprintf(cgtls *iqlibc.ppTLS, aafmt ppuintptr, cgva ppuintptr) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aaap tnva_list
	pp_ = aaap

	aaap = cgva
	if X__gmpfr_vprintf(cgtls, aafmt, aaap) == -ppint32(1) {

		Xfprintf(cgtls, Xstderr, "Error 2 in mpfr_vprintf(\"%s\", ...)\n\x00", iqlibc.ppVaList(cgbp+8, aafmt))

		pp_ = aaap
		Xexit(cgtls, ppint32(1))
	}
	Xputchar(cgtls, ppint32('\n'))
	pp_ = aaap
}

func sicheck_vprintf_failure(cgtls *iqlibc.ppTLS, aafmt ppuintptr, cgva ppuintptr) (cgr ppuint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aaap tnva_list
	var aae, aar ppint32
	pp_, pp_, pp_ = aaap, aae, aar

	aaap = cgva
	*(*ppint32)(iqunsafe.ppPointer(X__errno_location(cgtls))) = 0
	aar = X__gmpfr_vprintf(cgtls, aafmt, aaap)
	aae = *(*ppint32)(iqunsafe.ppPointer(X__errno_location(cgtls)))
	pp_ = aaap

	if aar != -ppint32(1) || aae != ppint32(mvEOVERFLOW) {

		Xputchar(cgtls, ppint32('\n'))
		Xfprintf(cgtls, Xstderr, "Error 3 in mpfr_vprintf(\"%s\", ...)\nGot r = %d, errno = %d\n\x00", iqlibc.ppVaList(cgbp+8, aafmt, aar, aae))
		return ppuint32(1)
	}

	Xputchar(cgtls, ppint32('\n'))
	return ppuint32(0)
}

// C documentation
//
//	/* The goal of this test is to check cases where more INT_MAX characters
//	   are output, in which case, it should be a failure, because like C's
//	   *printf functions, the return type is int and the returned value must
//	   be either the number of characters printed or a negative value. */
func sicheck_long_string(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaerr ppuint32
	var aalarge_prec, aan1 ppint32
	var aamin_memory_limit, aaold_memory_limit tnsize_t
	var pp_ /* n2 at bp+16 */ ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aaerr, aalarge_prec, aamin_memory_limit, aan1, aaold_memory_limit
	aalarge_prec = ppint32(2147483647)

	aaold_memory_limit = Xtests_memory_limit

	/* With a 32-bit (4GB) address space, a realloc failure has been noticed
	   with a 2G precision (though allocating up to 4GB is possible):
	     MPFR: Can't reallocate memory (old_size=4096 new_size=2147487744)
	   The implementation might be improved to use less memory and avoid
	   this problem. In the mean time, let's choose a smaller precision,
	   but this will generally have the effect to disable the test. */
	if ppuint32(4) == ppuint32(4) {

		aalarge_prec /= ppint32(2)
	}

	/* We assume that the precision won't be increased internally. */
	if aalarge_prec > iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)-iqlibc.ppUint32FromInt32(256)) {
		aalarge_prec = iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1) - iqlibc.ppUint32FromInt32(256))
	}

	/* Increase tests_memory_limit if need be in order to avoid an
	   obvious failure due to insufficient memory. Note that such an
	   increase is necessary, but is not guaranteed to be sufficient
	   in all cases (e.g. with logging activated). */
	aamin_memory_limit = iqlibc.ppUint32FromInt32(aalarge_prec / ppint32((iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))/iqlibc.ppInt32FromInt32(mvCHAR_BIT)))
	if aamin_memory_limit > iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))/iqlibc.ppUint32FromInt32(32) {
		aamin_memory_limit = iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))
	} else {

		aamin_memory_limit *= ppuint32(32)
	}
	if Xtests_memory_limit > ppuint32(0) && Xtests_memory_limit < aamin_memory_limit {
		Xtests_memory_limit = aamin_memory_limit
	}

	Xmpfr_init2(cgtls, cgbp, aalarge_prec)

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_nextabove(cgtls, cgbp)

	if aalarge_prec >= ppint32(iqlibc.ppInt32FromInt32(mvINT_MAX)-iqlibc.ppInt32FromInt32(512)) {

		aaerr = ppuint32(0)

		aaerr |= sicheck_vprintf_failure(cgtls, "%Rb %512d\x00", iqlibc.ppVaList(cgbp+32, cgbp, ppint32(1)))

		aaerr |= sicheck_vprintf_failure(cgtls, "%RA %RA %Ra %Ra %512d\x00", iqlibc.ppVaList(cgbp+32, cgbp, cgbp, cgbp, cgbp, ppint32(1)))

		if iqlibc.ppUint32FromInt64(4)*iqlibc.ppUint32FromInt32(mvCHAR_BIT) > ppuint32(40) {

			aan1 = aalarge_prec + ppint32(517)
			*(*ppint32)(iqunsafe.ppPointer(cgbp + 16)) = ppint32(-ppint32(17))

			aaerr |= sicheck_vprintf_failure(cgtls, "%Rb %512d%ln\x00", iqlibc.ppVaList(cgbp+32, cgbp, ppint32(1), cgbp+16))
			if aan1 != *(*ppint32)(iqunsafe.ppPointer(cgbp + 16)) {

				Xfprintf(cgtls, Xstderr, "Error in check_long_string(\"%s\", ...)\nExpected n = %ld\nGot      n = %ld\n\x00", iqlibc.ppVaList(cgbp+32, "%Rb %512d%ln\x00", aan1, *(*ppint32)(iqunsafe.ppPointer(cgbp + 16))))
				aaerr = ppuint32(1)
			}
			aan1 = (aalarge_prec-ppint32(2))/ppint32(4)*ppint32(4) + ppint32(548)
			*(*ppint32)(iqunsafe.ppPointer(cgbp + 16)) = ppint32(-ppint32(17))

			aaerr |= sicheck_vprintf_failure(cgtls, "%RA %RA %Ra %Ra %512d%ln\x00", iqlibc.ppVaList(cgbp+32, cgbp, cgbp, cgbp, cgbp, ppint32(1), cgbp+16))
			if aan1 != *(*ppint32)(iqunsafe.ppPointer(cgbp + 16)) {

				Xfprintf(cgtls, Xstderr, "Error in check_long_string(\"%s\", ...)\nExpected n = %ld\nGot      n = %ld\n\x00", iqlibc.ppVaList(cgbp+32, "%RA %RA %Ra %Ra %512d%ln\x00", aan1, *(*ppint32)(iqunsafe.ppPointer(cgbp + 16))))
				aaerr = ppuint32(1)
			}
		}

		if aaerr != 0 {
			Xexit(cgtls, ppint32(1))
		}
	}

	Xmpfr_clear(cgtls, cgbp)
	Xtests_memory_limit = aaold_memory_limit
}

func sicheck_special(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var pp_ /* x at bp+0 */ tnmpfr_t

	Xmpfr_init(cgtls, cgbp)

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	sicheck(cgtls, "%Ra\x00", cgbp)
	sicheck(cgtls, "%Rb\x00", cgbp)
	sicheck(cgtls, "%Re\x00", cgbp)
	sicheck(cgtls, "%Rf\x00", cgbp)
	sicheck(cgtls, "%Rg\x00", cgbp)
	sicheck_vprintf(cgtls, "%Ra\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Rb\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Re\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Rf\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Rg\x00", iqlibc.ppVaList(cgbp+24, cgbp))

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	sicheck(cgtls, "%Ra\x00", cgbp)
	sicheck(cgtls, "%Rb\x00", cgbp)
	sicheck(cgtls, "%Re\x00", cgbp)
	sicheck(cgtls, "%Rf\x00", cgbp)
	sicheck(cgtls, "%Rg\x00", cgbp)
	sicheck_vprintf(cgtls, "%Ra\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Rb\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Re\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Rf\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Rg\x00", iqlibc.ppVaList(cgbp+24, cgbp))

	Xmpfr_set_nan(cgtls, cgbp)
	sicheck(cgtls, "%Ra\x00", cgbp)
	sicheck(cgtls, "%Rb\x00", cgbp)
	sicheck(cgtls, "%Re\x00", cgbp)
	sicheck(cgtls, "%Rf\x00", cgbp)
	sicheck(cgtls, "%Rg\x00", cgbp)
	sicheck_vprintf(cgtls, "%Ra\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Rb\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Re\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Rf\x00", iqlibc.ppVaList(cgbp+24, cgbp))
	sicheck_vprintf(cgtls, "%Rg\x00", iqlibc.ppVaList(cgbp+24, cgbp))

	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_mixed(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aach, aalo ppint32
	var aad, aald ppfloat64
	var aaf ppfloat32
	var aallo ppint64
	var aaprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var aasaved_p tnptrdiff_t
	var aasch ppint8
	var aash ppint16
	var aaui ppuint32
	var aauim tnuintmax_t
	var pp_ /* i at bp+4 */ ppint32
	var pp_ /* im at bp+104 */ tnintmax_t
	var pp_ /* j at bp+8 */ ppint32
	var pp_ /* mpf at bp+60 */ tnmpf_t
	var pp_ /* mpfr at bp+76 */ tnmpfr_t
	var pp_ /* mpq at bp+36 */ tnmpq_t
	var pp_ /* mpz at bp+24 */ tnmpz_t
	var pp_ /* p at bp+16 */ tnptrdiff_t
	var pp_ /* sz at bp+20 */ tnsize_t
	var pp_ /* uch at bp+0 */ ppuint8
	var pp_ /* ullo at bp+96 */ ppuint64
	var pp_ /* ulo at bp+12 */ ppuint32
	var pp_ /* ush at bp+2 */ ppuint16
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aach, aad, aaf, aald, aallo, aalo, aaprec, aarnd, aasaved_p, aasch, aash, aaui, aauim
	aach = ppint32('a')
	aasch = ppint8(-ppint32(1))
	*(*ppuint8)(iqunsafe.ppPointer(cgbp)) = ppuint8(1)
	aash = ppint16(-ppint32(1))
	*(*ppuint16)(iqunsafe.ppPointer(cgbp + 2)) = ppuint16(1)
	*(*ppint32)(iqunsafe.ppPointer(cgbp + 4)) = -ppint32(1)
	*(*ppint32)(iqunsafe.ppPointer(cgbp + 8)) = ppint32(1)
	aaui = ppuint32(1)
	aalo = ppint32(-ppint32(1))
	*(*ppuint32)(iqunsafe.ppPointer(cgbp + 12)) = ppuint32(1)
	aaf = ppfloat32(-iqlibc.ppFloat64FromFloat64(1.25))
	aad = -iqlibc.ppFloat64FromFloat64(1.25)
	aald = ppfloat64(-iqlibc.ppFloat64FromFloat64(1.25))

	*(*tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 16)) = ppint32(1)
	*(*tnsize_t)(iqunsafe.ppPointer(cgbp + 20)) = ppuint32(1)
	aarnd = ppint32(ecMPFR_RNDN)

	Xmpfr_mpz_init(cgtls, cgbp+24)
	X__gmpz_set_ui(cgtls, cgbp+24, *(*ppuint32)(iqunsafe.ppPointer(cgbp + 12)))
	X__gmpq_init(cgtls, cgbp+36)
	X__gmpq_set_si(cgtls, cgbp+36, aalo, *(*ppuint32)(iqunsafe.ppPointer(cgbp + 12)))
	X__gmpf_init(cgtls, cgbp+60)
	X__gmpf_set_q(cgtls, cgbp+60, cgbp+36)
	Xmpfr_init(cgtls, cgbp+76)
	Xmpfr_set_f(cgtls, cgbp+76, cgbp+60, ppint32(ecMPFR_RNDN))
	aaprec = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 76)).fd_mpfr_prec

	sicheck_vprintf(cgtls, "a. %Ra, b. %u, c. %lx%n\x00", iqlibc.ppVaList(cgbp+120, cgbp+76, aaui, *(*ppuint32)(iqunsafe.ppPointer(cgbp + 12)), cgbp+8))
	if *(*ppint32)(iqunsafe.ppPointer(cgbp + 8)) != ppint32(22) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(1), *(*ppint32)(iqunsafe.ppPointer(cgbp + 8)), ppint32(22)))
		Xexit(cgtls, ppint32(1))
	}
	sicheck_vprintf(cgtls, "a. %c, b. %Rb, c. %u, d. %li%ln\x00", iqlibc.ppVaList(cgbp+120, *(*ppint32)(iqunsafe.ppPointer(cgbp + 4)), cgbp+76, *(*ppint32)(iqunsafe.ppPointer(cgbp + 4)), aalo, cgbp+12))
	if *(*ppuint32)(iqunsafe.ppPointer(cgbp + 12)) != iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(36)) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %lu characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(2), *(*ppuint32)(iqunsafe.ppPointer(cgbp + 12)), ppint32(36)))
		Xexit(cgtls, ppint32(1))
	}
	sicheck_vprintf(cgtls, "a. %hi, b. %*f, c. %Re%hn\x00", iqlibc.ppVaList(cgbp+120, iqlibc.ppInt32FromUint16(*(*ppuint16)(iqunsafe.ppPointer(cgbp + 2))), ppint32(3), ppfloat64(aaf), cgbp+76, cgbp+2))
	if iqlibc.ppInt32FromUint16(*(*ppuint16)(iqunsafe.ppPointer(cgbp + 2))) != ppint32(46) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %hu characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(3), iqlibc.ppInt32FromUint16(*(*ppuint16)(iqunsafe.ppPointer(cgbp + 2))), ppint32(46)))
		Xexit(cgtls, ppint32(1))
	}
	sicheck_vprintf(cgtls, "a. %hi, b. %f, c. %#.2Rf%n\x00", iqlibc.ppVaList(cgbp+120, ppint32(aash), aad, cgbp+76, cgbp+4))
	if *(*ppint32)(iqunsafe.ppPointer(cgbp + 4)) != ppint32(29) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(4), *(*ppint32)(iqunsafe.ppPointer(cgbp + 4)), ppint32(29)))
		Xexit(cgtls, ppint32(1))
	}
	sicheck_vprintf(cgtls, "a. %R*A, b. %Fe, c. %i%zn\x00", iqlibc.ppVaList(cgbp+120, aarnd, cgbp+76, cgbp+60, *(*tnsize_t)(iqunsafe.ppPointer(cgbp + 20)), cgbp+20))
	if ppuint32(*(*tnsize_t)(iqunsafe.ppPointer(cgbp + 20))) != iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(34)) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %lu characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(5), ppuint32(*(*tnsize_t)(iqunsafe.ppPointer(cgbp + 20))), ppint32(34)))
		Xexit(cgtls, ppint32(1))
	} /* no format specifier '%zu' in C90 */
	sicheck_vprintf(cgtls, "a. %Pu, b. %c, c. %RUG, d. %Zi%Zn\x00", iqlibc.ppVaList(cgbp+120, aaprec, aach, cgbp+76, cgbp+24, cgbp+24))
	if X__gmpz_cmp_ui(cgtls, cgbp+24, ppuint32(24)) != 0 {
		X__gmpfr_fprintf(cgtls, Xstderr, "Error in test #%d, mpfr_printf printed %Zi characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(6), cgbp+24, ppint32(24)))
		Xexit(cgtls, ppint32(1))
	}
	sicheck_vprintf(cgtls, "%% a. %#.0RNg, b. %Qx%Rn c. %p\x00", iqlibc.ppVaList(cgbp+120, cgbp+76, cgbp+36, cgbp+76, cgbp+4))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+76, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(15)), 0) != 0 {
		X__gmpfr_fprintf(cgtls, Xstderr, "Error in test #%d, mpfr_printf printed %Rg characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(7), cgbp+76, ppint32(15)))
		Xexit(cgtls, ppint32(1))
	}

	aasaved_p = *(*tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 16))
	sicheck_vprintf(cgtls, "%% a. %RNg, b. %Qx, c. %td%tn\x00", iqlibc.ppVaList(cgbp+120, cgbp+76, cgbp+36, *(*tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 16)), cgbp+16))
	if *(*tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 16)) != ppint32(20) {

		X__gmpfr_fprintf(cgtls, Xstderr, "Error in test 8, got '%% a. %RNg, b. %Qx, c. %td'\n\x00", iqlibc.ppVaList(cgbp+120, cgbp+76, cgbp+36, aasaved_p))
	}
	if ppint32(*(*tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 16))) != ppint32(iqlibc.ppInt32FromInt32(20)) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %ld characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(8), ppint32(*(*tnptrdiff_t)(iqunsafe.ppPointer(cgbp + 16))), ppint32(20)))
		Xexit(cgtls, ppint32(1))
	} /* no format specifier '%td' in C90 */

	sicheck_vprintf(cgtls, "a. %RA, b. %Lf, c. %QX%zn\x00", iqlibc.ppVaList(cgbp+120, cgbp+76, aald, cgbp+36, cgbp+20))
	if ppuint32(*(*tnsize_t)(iqunsafe.ppPointer(cgbp + 20))) != iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(30)) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %lu characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(9), ppuint32(*(*tnsize_t)(iqunsafe.ppPointer(cgbp + 20))), ppint32(30)))
		Xexit(cgtls, ppint32(1))
	} /* no format specifier '%zu' in C90 */

	sicheck_vprintf(cgtls, "a. %hhi, b. %Ra, c. %hhu%hhn\x00", iqlibc.ppVaList(cgbp+120, ppint32(aasch), cgbp+76, iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(cgbp))), cgbp))
	if ppuint32(*(*ppuint8)(iqunsafe.ppPointer(cgbp))) != iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(22)) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %u characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(10), ppuint32(*(*ppuint8)(iqunsafe.ppPointer(cgbp))), ppint32(22)))
		Xexit(cgtls, ppint32(1))
	} /* no format specifier '%hhu' in C90 */

	aallo = ppint64(-ppint32(1))
	*(*ppuint64)(iqunsafe.ppPointer(cgbp + 96)) = ppuint64(1)

	sicheck_vprintf(cgtls, "a. %Re, b. %llx%Qn\x00", iqlibc.ppVaList(cgbp+120, cgbp+76, *(*ppuint64)(iqunsafe.ppPointer(cgbp + 96)), cgbp+36))
	if X__gmpq_cmp_ui(cgtls, cgbp+36, ppuint32(31), ppuint32(1)) != 0 {
		X__gmpfr_fprintf(cgtls, Xstderr, "Error in test #%d, mpfr_printf printed %Qu characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(11), cgbp+36, ppint32(31)))
		Xexit(cgtls, ppint32(1))
	}
	sicheck_vprintf(cgtls, "a. %lli, b. %Rf%lln\x00", iqlibc.ppVaList(cgbp+120, aallo, cgbp+76, cgbp+96))
	if *(*ppuint64)(iqunsafe.ppPointer(cgbp + 96)) != iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(19)) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %llu characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(12), *(*ppuint64)(iqunsafe.ppPointer(cgbp + 96)), ppint32(19)))
		Xexit(cgtls, ppint32(1))
	}

	*(*tnintmax_t)(iqunsafe.ppPointer(cgbp + 104)) = ppint64(-ppint32(1))
	aauim = ppuint64(1)

	sicheck_vprintf(cgtls, "a. %*RA, b. %ji%Fn\x00", iqlibc.ppVaList(cgbp+120, ppint32(10), cgbp+76, *(*tnintmax_t)(iqunsafe.ppPointer(cgbp + 104)), cgbp+60))
	if X__gmpf_cmp_ui(cgtls, cgbp+60, ppuint32(20)) != 0 {
		X__gmpfr_fprintf(cgtls, Xstderr, "Error in test #%d, mpfr_printf printed %Fg characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(31), cgbp+60, ppint32(20)))
		Xexit(cgtls, ppint32(1))
	}
	sicheck_vprintf(cgtls, "a. %.*Re, b. %jx%jn\x00", iqlibc.ppVaList(cgbp+120, ppint32(10), cgbp+76, aauim, cgbp+104))
	if ppint32(*(*tnintmax_t)(iqunsafe.ppPointer(cgbp + 104))) != ppint32(iqlibc.ppInt32FromInt32(25)) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %li characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+120, ppint32(32), ppint32(*(*tnintmax_t)(iqunsafe.ppPointer(cgbp + 104))), ppint32(25)))
		Xexit(cgtls, ppint32(1))
	} /* no format specifier "%ji" in C90 */

	Xmpfr_clear(cgtls, cgbp+76)
	X__gmpf_clear(cgtls, cgbp+60)
	X__gmpq_clear(cgtls, cgbp+36)
	Xmpfr_mpz_clear(cgtls, cgbp+24)
}

func sicheck_random(cgtls *iqlibc.ppTLS, aanb_tests ppint32) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaflag [6]ppuint8
	var aai, aaj, aajmax, aaprec, aaret, aaspec, ccv2, ccv3 ppint32
	var aaold_emax, aaold_emin tnmpfr_exp_t
	var aaptr, ccv10, ccv11, ccv4, ccv6, ccv7, ccv8, ccv9 ppuintptr
	var aarnd tnmpfr_rnd_t
	var aaspecifier [5]ppuint8
	var pp_ /* fmt at bp+16 */ [13]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaflag, aai, aaj, aajmax, aaold_emax, aaold_emin, aaprec, aaptr, aaret, aarnd, aaspec, aaspecifier, ccv10, ccv11, ccv2, ccv3, ccv4, ccv6, ccv7, ccv8, ccv9
	aaflag = [6]ppuint8{
		0: ppuint8('-'),
		1: ppuint8('+'),
		2: ppuint8(' '),
		3: ppuint8('#'),
		4: ppuint8('0'),
		5: ppuint8('\''),
	}
	aaspecifier = [5]ppuint8{
		0: ppuint8('a'),
		1: ppuint8('b'),
		2: ppuint8('e'),
		3: ppuint8('f'),
		4: ppuint8('g'),
	}

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	Xmpfr_init(cgtls, cgbp)

	aai = 0
	for {
		if !(aai < aanb_tests) {
			break
		}
		/* at most something like "%-+ #0'.*R*f" */
		aaptr = cgbp + 16

		Xtests_default_random(cgtls, cgbp, ppint32(256), iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2)), iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))-iqlibc.ppInt32FromInt32(1), 0)
		aarnd = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % iqlibc.ppUint32FromInt32(ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)))

		aaspec = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % iqlibc.ppUint32FromInt32(5))
		if aaspec == ppint32(3) || aaspec == ppint32(4) {
			ccv2 = ppint32(6)
		} else {
			ccv2 = ppint32(5)
		}
		aajmax = ccv2 /* ' flag only with %f or %g */
		/* advantage small precision */
		if Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0) {
			ccv3 = ppint32(10)
		} else {
			ccv3 = Xprec_max_printf
		}
		aaprec = ccv3
		aaprec = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % iqlibc.ppUint32FromInt32(aaprec))
		if aaspec == ppint32(3) && ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp > ppint32(Xprec_max_printf) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp < ppint32(-Xprec_max_printf)) {
			/*  change style 'f' to style 'e' when number x is very large or very
			    small*/
			aaspec--
		}

		ccv4 = aaptr
		aaptr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv4)) = ppuint8('%')
		aaj = 0
		for {
			if !(aaj < aajmax) {
				break
			}

			if Xrandlimb(cgtls)%ppuint32(3) == ppuint32(0) {
				ccv6 = aaptr
				aaptr++
				*(*ppuint8)(iqunsafe.ppPointer(ccv6)) = aaflag[aaj]
			}

			goto cg_5
		cg_5:
			;
			aaj++
		}
		ccv7 = aaptr
		aaptr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv7)) = ppuint8('.')
		ccv8 = aaptr
		aaptr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv8)) = ppuint8('*')
		ccv9 = aaptr
		aaptr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv9)) = ppuint8('R')
		ccv10 = aaptr
		aaptr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv10)) = ppuint8('*')
		ccv11 = aaptr
		aaptr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv11)) = aaspecifier[aaspec]
		*(*ppuint8)(iqunsafe.ppPointer(aaptr)) = ppuint8('\000')

		Xmpfr_printf(cgtls, "mpfr_printf(\"%s\", %d, %s, %Re)\n\x00", iqlibc.ppVaList(cgbp+40, cgbp+16, aaprec, Xmpfr_print_rnd_mode(cgtls, aarnd), cgbp))
		aaret = Xmpfr_printf(cgtls, cgbp+16, iqlibc.ppVaList(cgbp+40, aaprec, aarnd, cgbp))
		if aaret == -ppint32(1) {

			if aaspec == ppint32(3) && ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp > ppint32(mvINT_MAX) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp < ppint32(-ppint32(mvINT_MAX))) {
				/* normal failure: x is too large to be output with full precision */

				Xmpfr_printf(cgtls, "too large !\x00", 0)
			} else {

				Xprintf(cgtls, "Error in mpfr_printf(\"%s\", %d, %s, ...)\x00", iqlibc.ppVaList(cgbp+40, cgbp+16, aaprec, Xmpfr_print_rnd_mode(cgtls, aarnd)))

				if Xstdout_redirect != 0 {

					if Xfflush(cgtls, Xstdout) == -ppint32(1) || Xfclose(cgtls, Xstdout) == -ppint32(1) {

						Xperror(cgtls, "check_random\x00")
						Xexit(cgtls, ppint32(1))
					}
				}
				Xexit(cgtls, ppint32(1))
			}
		}
		Xputchar(cgtls, ppint32('\n'))

		goto cg_1
	cg_1:
		;
		aai++
	}

	Xset_emin(cgtls, aaold_emin)
	Xset_emax(cgtls, aaold_emax)

	Xmpfr_clear(cgtls, cgbp)
}

func sitest_locale(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aacount, aai, aaj, aarnd, ccv3 ppint32
	var aas ppuintptr
	var ccv2, ccv4, ccv5 ppbool
	var pp_ /* s at bp+58 */ [24]ppuint8
	var pp_ /* tab_locale at bp+0 */ [4]ppuintptr
	var pp_ /* v at bp+32 */ [26]ppuint8
	var pp_ /* x at bp+16 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aacount, aai, aaj, aarnd, aas, ccv2, ccv3, ccv4, ccv5
	*(*[4]ppuintptr)(iqunsafe.ppPointer(cgbp)) = [4]ppuintptr{
		0: "en_US\x00",
		1: "en_US.iso88591\x00",
		2: "en_US.iso885915\x00",
		3: "en_US.utf8\x00",
	}
	*(*[26]ppuint8)(iqunsafe.ppPointer(cgbp + 32)) = [26]ppuint8{'9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '.', '5'}

	aai = 0
	for {
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(cgbp == cgbp)), ppint32(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tprintf.c\x00", ppint32(505), "(void *) &(tab_locale) == (void *) &(tab_locale)[0]\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if !(ppint32(aai) < iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt64(16)/iqlibc.ppUint32FromInt64(4))) {
			break
		}

		aas = Xsetlocale(cgtls, ppint32(mvLC_ALL), (*(*[4]ppuintptr)(iqunsafe.ppPointer(cgbp)))[aai])

		if ccv4 = aas != iqlibc.ppUintptrFromInt32(0); ccv4 {
			if iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer((*tslconv)(iqunsafe.ppPointer(Xlocaleconv(cgtls))).fdthousands_sep))) == ppint32('\000') || iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer((*tslconv)(iqunsafe.ppPointer(Xlocaleconv(cgtls))).fdthousands_sep + 1))) != ppint32('\000') {
				ccv3 = iqlibc.ppInt32FromUint8(iqlibc.ppUint8FromUint8('\000'))
			} else {
				ccv3 = iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer((*tslconv)(iqunsafe.ppPointer(Xlocaleconv(cgtls))).fdthousands_sep)))
			}
		}
		if ccv4 && ccv3 == ppint32(',') {
			break
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(cgbp == cgbp)), ppint32(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tprintf.c\x00", ppint32(515), "(void *) &(tab_locale) == (void *) &(tab_locale)[0]\x00")
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ppint32(aai) == iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt64(16)/iqlibc.ppUint32FromInt64(4)) {

		if Xgetenv(cgtls, "MPFR_CHECK_LOCALES\x00") == iqlibc.ppUintptrFromInt32(0) {
			return
		}

		Xfprintf(cgtls, Xstderr, "Cannot find a locale with ',' thousands separator.\nPlease install one of the en_US based locales.\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_init2(cgtls, cgbp+16, ppint32(113))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(10000)), 0, ppint32(ecMPFR_RNDN))

	aacount = Xmpfr_printf(cgtls, "(1) 10000=%'Rg \n\x00", iqlibc.ppVaList(cgbp+96, cgbp+16))
	if aacount != ppint32(18) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+96, ppint32(10000), aacount, ppint32(18)))
		Xexit(cgtls, ppint32(1))
	}
	aacount = Xmpfr_printf(cgtls, "(2) 10000=%'Rf \n\x00", iqlibc.ppVaList(cgbp+96, cgbp+16))
	if aacount != ppint32(25) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+96, ppint32(10001), aacount, ppint32(25)))
		Xexit(cgtls, ppint32(1))
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1000)), 0, ppint32(ecMPFR_RNDN))
	aacount = Xmpfr_printf(cgtls, "(3) 1000=%'Rf \n\x00", iqlibc.ppVaList(cgbp+96, cgbp+16))
	if aacount != ppint32(23) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+96, ppint32(10002), aacount, ppint32(23)))
		Xexit(cgtls, ppint32(1))
	}

	aai = ppint32(1)
	for {
		if !(iqlibc.ppUint32FromInt32(aai) <= iqlibc.ppUint32FromInt64(26)-iqlibc.ppUint32FromInt32(3)) {
			break
		}

		Xmpfr_set_str(cgtls, cgbp+16, cgbp+32+ppuintptr(26)-ppuintptr(3)-ppuintptr(aai), ppint32(10), ppint32(ecMPFR_RNDN))
		aacount = Xmpfr_printf(cgtls, "(4) 10^i=%'.0Rf \n\x00", iqlibc.ppVaList(cgbp+96, cgbp+16))
		if aacount != ppint32(12)+aai+aai/ppint32(3) {
			Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+96, ppint32(10002)+aai, aacount, ppint32(12)+aai+aai/ppint32(3)))
			Xexit(cgtls, ppint32(1))
		}

		goto cg_6
	cg_6:
		;
		aai++
	}

	aai = ppint32(1)
	for {
		if !(aai <= ppint32(mvN0)) {
			break
		}

		(*(*[24]ppuint8)(iqunsafe.ppPointer(cgbp + 58)))[0] = ppuint8('1')
		aaj = ppint32(1)
		for {
			if !(aaj <= aai) {
				break
			}
			(*(*[24]ppuint8)(iqunsafe.ppPointer(cgbp + 58)))[aaj] = ppuint8('0')
			goto cg_8
		cg_8:
			;
			aaj++
		}
		(*(*[24]ppuint8)(iqunsafe.ppPointer(cgbp + 58)))[aai+ppint32(1)] = ppuint8('\000')

		Xmpfr_set_str(cgtls, cgbp+16, cgbp+58, ppint32(10), ppint32(ecMPFR_RNDN))

		aarnd = 0
		for {
			if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}

			aacount = Xmpfr_printf(cgtls, "(5) 10^i=%'.0R*f \n\x00", iqlibc.ppVaList(cgbp+96, aarnd, cgbp+16))
			if aacount != ppint32(12)+aai+aai/ppint32(3) {
				Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+96, ppint32(11000)+ppint32(10)*aai+aarnd, aacount, ppint32(12)+aai+aai/ppint32(3)))
				Xexit(cgtls, ppint32(1))
			}

			goto cg_9
		cg_9:
			;
			aarnd++
		}

		Xstrcat(cgtls, cgbp+58+ppuintptr(aai+iqlibc.ppInt32FromInt32(1)), ".5\x00")
		aacount = Xmpfr_printf(cgtls, "(5) 10^i=%'.0Rf \n\x00", iqlibc.ppVaList(cgbp+96, cgbp+16))
		if aacount != ppint32(12)+aai+aai/ppint32(3) {
			Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+96, ppint32(11000)+ppint32(10)*aai+ppint32(9), aacount, ppint32(12)+aai+aai/ppint32(3)))
			Xexit(cgtls, ppint32(1))
		}

		goto cg_7
	cg_7:
		;
		aai++
	}

	Xmpfr_set_str(cgtls, cgbp+16, "1000\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aacount = Xmpfr_printf(cgtls, "%'012.3Rg\n\x00", iqlibc.ppVaList(cgbp+96, cgbp+16))
	if aacount != ppint32(13) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+96, ppint32(12000), aacount, ppint32(13)))
		Xexit(cgtls, ppint32(1))
	}
	aacount = Xmpfr_printf(cgtls, "%'012.4Rg\n\x00", iqlibc.ppVaList(cgbp+96, cgbp+16))
	if aacount != ppint32(13) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+96, ppint32(12001), aacount, ppint32(13)))
		Xexit(cgtls, ppint32(1))
	}
	aacount = Xmpfr_printf(cgtls, "%'013.4Rg\n\x00", iqlibc.ppVaList(cgbp+96, cgbp+16))
	if aacount != ppint32(14) {
		Xfprintf(cgtls, Xstderr, "Error in test #%d: mpfr_printf printed %d characters instead of %d\n\x00", iqlibc.ppVaList(cgbp+96, ppint32(12002), aacount, ppint32(14)))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp+16)
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	var aaN ppint32
	pp_ = aaN

	Xtests_start_mpfr(cgtls)

	/* with no argument: prints to /dev/null,
	   tprintf N: prints N tests to stdout */
	if aaargc == ppint32(1) {

		aaN = ppint32(1000)
		Xstdout_redirect = ppint32(1)
		if Xfreopen(cgtls, "/dev/null\x00", "w\x00", Xstdout) == iqlibc.ppUintptrFromInt32(0) {

			/* We failed to open this device, try with a dummy file */
			if Xfreopen(cgtls, "tprintf_out.txt\x00", "w\x00", Xstdout) == iqlibc.ppUintptrFromInt32(0) {

				/* Output the error message to stderr since it is not
				   a message about a wrong result in MPFR. Anyway the
				   standard output may have changed. */
				Xfprintf(cgtls, Xstderr, "Can't open /dev/null or a temporary file\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
		}
	} else {

		Xstdout_redirect = 0
		aaN = Xatoi(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*4)))
	}

	sicheck_special(cgtls)
	sicheck_mixed(cgtls)

	/* expensive tests */
	if Xgetenv(cgtls, "MPFR_CHECK_LARGEMEM\x00") != iqlibc.ppUintptrFromInt32(0) {
		sicheck_long_string(cgtls)
	}

	sicheck_random(cgtls, aaN)

	sitest_locale(cgtls)

	if Xstdout_redirect != 0 {

		if Xfflush(cgtls, Xstdout) == -ppint32(1) || Xfclose(cgtls, Xstdout) == -ppint32(1) {
			Xperror(cgtls, "main\x00")
		}
	}
	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

func ___errno_location(*iqlibc.ppTLS) ppuintptr

func ___gmpf_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_cmp_ui(*iqlibc.ppTLS, ppuintptr, ppuint32) ppint32

func ___gmpf_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_set_q(*iqlibc.ppTLS, ppuintptr, ppuintptr)

var ___gmpfr_emax ppint32

var ___gmpfr_emin ppint32

func ___gmpfr_fprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func ___gmpfr_vprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func ___gmpq_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_cmp_ui(*iqlibc.ppTLS, ppuintptr, ppuint32, ppuint32) ppint32

func ___gmpq_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_set_si(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32)

func ___gmpz_cmp_ui(*iqlibc.ppTLS, ppuintptr, ppuint32) ppint32

func ___gmpz_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint32)

func _atoi(*iqlibc.ppTLS, ppuintptr) ppint32

func _exit(*iqlibc.ppTLS, ppint32)

func _fclose(*iqlibc.ppTLS, ppuintptr) ppint32

func _fflush(*iqlibc.ppTLS, ppuintptr) ppint32

func _fprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _freopen(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppuintptr

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _localeconv(*iqlibc.ppTLS) ppuintptr

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_mpz_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mpz_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_set_f(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint32, ppint32, ppint32) ppint32

func _perror(*iqlibc.ppTLS, ppuintptr)

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _putchar(*iqlibc.ppTLS, ppint32) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint32

func _set_emax(*iqlibc.ppTLS, ppint32)

func _set_emin(*iqlibc.ppTLS, ppint32)

func _setlocale(*iqlibc.ppTLS, ppint32, ppuintptr) ppuintptr

var _stderr ppuintptr

var _stdout ppuintptr

func _strcat(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _tests_default_random(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32, ppint32, ppint32)

func _tests_end_mpfr(*iqlibc.ppTLS)

var _tests_memory_limit ppuint32

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

// Code generated for linux/arm by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DMPFR_LONG_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/arm -UHAVE_NEARBYINT -c -o tgmpop.o.go tgmpop.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 32
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 2972
const mvMPFR_AI_THRESHOLD3 = 44718
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 277
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 10666
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 5
const mvMPFR_LOG2_PREC_BITS = 5
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 22
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 32
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 28990
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 24
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/arm/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_L = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_REDIR_TIME64 = 1
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ACCUM_EPSILON__ = "0x1P-15K"
const mv__ACCUM_FBIT__ = 15
const mv__ACCUM_IBIT__ = 16
const mv__ACCUM_MAX__ = "0X7FFFFFFFP-15K"
const mv__APCS_32__ = 1
const mv__ARMEL__ = 1
const mv__ARM_32BIT_STATE = 1
const mv__ARM_ARCH = 6
const mv__ARM_ARCH_6__ = 1
const mv__ARM_ARCH_ISA_ARM = 1
const mv__ARM_ARCH_ISA_THUMB = 1
const mv__ARM_EABI__ = 1
const mv__ARM_FEATURE_CLZ = 1
const mv__ARM_FEATURE_COPROC = 15
const mv__ARM_FEATURE_DSP = 1
const mv__ARM_FEATURE_LDREX = 4
const mv__ARM_FEATURE_QBIT = 1
const mv__ARM_FEATURE_SAT = 1
const mv__ARM_FEATURE_SIMD32 = 1
const mv__ARM_FEATURE_UNALIGNED = 1
const mv__ARM_FP = 12
const mv__ARM_PCS_VFP = 1
const mv__ARM_SIZEOF_MINIMAL_ENUM = 4
const mv__ARM_SIZEOF_WCHAR_T = 4
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 8
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DA_FBIT__ = 31
const mv__DA_IBIT__ = 32
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__DQ_FBIT__ = 63
const mv__DQ_IBIT__ = 0
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.2204460492503131e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.7976931348623157e+308
const mv__FLT32X_MIN__ = 2.2250738585072014e-308
const mv__FLT32X_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.1920928955078125e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.4028234663852886e+38
const mv__FLT32_MIN__ = 1.1754943508222875e-38
const mv__FLT32_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.2204460492503131e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.7976931348623157e+308
const mv__FLT64_MIN__ = 2.2250738585072014e-308
const mv__FLT64_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.1920928955078125e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.4028234663852886e+38
const mv__FLT_MIN__ = 1.1754943508222875e-38
const mv__FLT_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT_RADIX__ = 2
const mv__FRACT_EPSILON__ = "0x1P-15R"
const mv__FRACT_FBIT__ = 15
const mv__FRACT_IBIT__ = 0
const mv__FRACT_MAX__ = "0X7FFFP-15R"
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 1
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 1
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 1
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 1
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 1
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "arm-linux-gnueabihf-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__GXX_TYPEINFO_EQUALITY_INLINE = 0
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__HA_FBIT__ = 7
const mv__HA_IBIT__ = 8
const mv__HQ_FBIT__ = 15
const mv__HQ_IBIT__ = 0
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LACCUM_EPSILON__ = "0x1P-31LK"
const mv__LACCUM_FBIT__ = 31
const mv__LACCUM_IBIT__ = 32
const mv__LACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LK"
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.2204460492503131e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.7976931348623157e+308
const mv__LDBL_MIN__ = 2.2250738585072014e-308
const mv__LDBL_NORM_MAX__ = 1.7976931348623157e+308
const mv__LFRACT_EPSILON__ = "0x1P-31LR"
const mv__LFRACT_FBIT__ = 31
const mv__LFRACT_IBIT__ = 0
const mv__LFRACT_MAX__ = "0X7FFFFFFFP-31LR"
const mv__LITTLE_ENDIAN = 1234
const mv__LLACCUM_EPSILON__ = "0x1P-31LLK"
const mv__LLACCUM_FBIT__ = 31
const mv__LLACCUM_IBIT__ = 32
const mv__LLACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LLK"
const mv__LLFRACT_EPSILON__ = "0x1P-63LLR"
const mv__LLFRACT_FBIT__ = 63
const mv__LLFRACT_IBIT__ = 0
const mv__LLFRACT_MAX__ = "0X7FFFFFFFFFFFFFFFP-63LLR"
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 2147483647
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__QQ_FBIT__ = 7
const mv__QQ_IBIT__ = 0
const mv__SACCUM_EPSILON__ = "0x1P-7HK"
const mv__SACCUM_FBIT__ = 7
const mv__SACCUM_IBIT__ = 8
const mv__SACCUM_MAX__ = "0X7FFFP-7HK"
const mv__SA_FBIT__ = 15
const mv__SA_IBIT__ = 16
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SFRACT_EPSILON__ = "0x1P-7HR"
const mv__SFRACT_FBIT__ = 7
const mv__SFRACT_IBIT__ = 0
const mv__SFRACT_MAX__ = "0X7FP-7HR"
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__SQ_FBIT__ = 31
const mv__SQ_IBIT__ = 0
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__TA_FBIT__ = 63
const mv__TA_IBIT__ = 64
const mv__THUMB_INTERWORK__ = 1
const mv__TQ_FBIT__ = 127
const mv__TQ_IBIT__ = 0
const mv__UACCUM_EPSILON__ = "0x1P-16UK"
const mv__UACCUM_FBIT__ = 16
const mv__UACCUM_IBIT__ = 16
const mv__UACCUM_MAX__ = "0XFFFFFFFFP-16UK"
const mv__UACCUM_MIN__ = "0.0UK"
const mv__UDA_FBIT__ = 32
const mv__UDA_IBIT__ = 32
const mv__UDQ_FBIT__ = 64
const mv__UDQ_IBIT__ = 0
const mv__UFRACT_EPSILON__ = "0x1P-16UR"
const mv__UFRACT_FBIT__ = 16
const mv__UFRACT_IBIT__ = 0
const mv__UFRACT_MAX__ = "0XFFFFP-16UR"
const mv__UFRACT_MIN__ = "0.0UR"
const mv__UHA_FBIT__ = 8
const mv__UHA_IBIT__ = 8
const mv__UHQ_FBIT__ = 16
const mv__UHQ_IBIT__ = 0
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__ULACCUM_EPSILON__ = "0x1P-32ULK"
const mv__ULACCUM_FBIT__ = 32
const mv__ULACCUM_IBIT__ = 32
const mv__ULACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULK"
const mv__ULACCUM_MIN__ = "0.0ULK"
const mv__ULFRACT_EPSILON__ = "0x1P-32ULR"
const mv__ULFRACT_FBIT__ = 32
const mv__ULFRACT_IBIT__ = 0
const mv__ULFRACT_MAX__ = "0XFFFFFFFFP-32ULR"
const mv__ULFRACT_MIN__ = "0.0ULR"
const mv__ULLACCUM_EPSILON__ = "0x1P-32ULLK"
const mv__ULLACCUM_FBIT__ = 32
const mv__ULLACCUM_IBIT__ = 32
const mv__ULLACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULLK"
const mv__ULLACCUM_MIN__ = "0.0ULLK"
const mv__ULLFRACT_EPSILON__ = "0x1P-64ULLR"
const mv__ULLFRACT_FBIT__ = 64
const mv__ULLFRACT_IBIT__ = 0
const mv__ULLFRACT_MAX__ = "0XFFFFFFFFFFFFFFFFP-64ULLR"
const mv__ULLFRACT_MIN__ = "0.0ULLR"
const mv__UQQ_FBIT__ = 8
const mv__UQQ_IBIT__ = 0
const mv__USACCUM_EPSILON__ = "0x1P-8UHK"
const mv__USACCUM_FBIT__ = 8
const mv__USACCUM_IBIT__ = 8
const mv__USACCUM_MAX__ = "0XFFFFP-8UHK"
const mv__USACCUM_MIN__ = "0.0UHK"
const mv__USA_FBIT__ = 16
const mv__USA_IBIT__ = 16
const mv__USE_TIME_BITS64 = 1
const mv__USFRACT_EPSILON__ = "0x1P-8UHR"
const mv__USFRACT_FBIT__ = 8
const mv__USFRACT_IBIT__ = 0
const mv__USFRACT_MAX__ = "0XFFP-8UHR"
const mv__USFRACT_MIN__ = "0.0UHR"
const mv__USQ_FBIT__ = 32
const mv__USQ_IBIT__ = 0
const mv__UTA_FBIT__ = 64
const mv__UTA_IBIT__ = 64
const mv__UTQ_FBIT__ = 128
const mv__UTQ_IBIT__ = 0
const mv__VERSION__ = "12.2.0"
const mv__VFP_FP__ = 1
const mv__WCHAR_MAX__ = 0xffffffff
const mv__WCHAR_MIN__ = 0
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__arm__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppuint32

type tn__predefined_ptrdiff_t = ppint32

type tnsize_t = ppuint32

type tnssize_t = ppint32

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__ccgo_align [0]ppuint32
	fd__lldata     [0]ppint64
	fd__align      [0]ppfloat64
	fd__opaque     [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppuint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fd__ccgo_align [0]ppuint32
	fdquot         ppint64
	fdrem          ppint64
}

type tnmax_align_t = struct {
	fd__ccgo_align [0]ppuint32
	fd__ll         ppint64
	fd__ld         ppfloat64
}

type tnptrdiff_t = ppint32

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint32

type tnmpfr_ulong = ppuint32

type tnmpfr_size_t = ppuint32

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint32

type tnmpfr_uprec_t = ppuint32

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint32

type tnmpfr_uexp_t = ppuint32

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tsbases = struct {
	fd__ccgo_align          [0]ppuint32
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fd__ccgo_align [0]ppuint32
	fdd            [0]ppfloat64
	fds            struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint32

type tnmpfr_ueexp_t = ppuint32

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

func sispecial(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aares ppint32
	var ccv1, ccv2, ccv3, ccv4, ccv5, ccv6 ppbool
	var pp_ /* q at bp+32 */ tnmpq_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	var pp_ /* z at bp+56 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aares, ccv1, ccv2, ccv3, ccv4, ccv5, ccv6
	aares = 0

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+16)
	X__gmpq_init(cgtls, cgbp+32)
	Xmpfr_mpz_init(cgtls, cgbp+56)

	/* cancellation in mpfr_add_q */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(60))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(20))
	X__gmpz_set_str(cgtls, cgbp+32, "-187207494\x00", ppint32(10))
	X__gmpz_set_str(cgtls, cgbp+32+12, "5721\x00", ppint32(10))
	Xmpfr_set_str_binary(cgtls, cgbp, "11111111101001011011100101100011011110010011100010000100001E-44\x00")
	Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32(Xmpfr_cmp_ui_2exp(cgtls, cgbp+16, ppuint32(256783), ppint32(-ppint32(64))) == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "cancellation in add_q\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(19))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1011110101110011100E0\x00")
	X__gmpz_set_str(cgtls, cgbp+32, "187207494\x00", ppint32(10))
	X__gmpz_set_str(cgtls, cgbp+32+12, "5721\x00", ppint32(10))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(29))
	Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDD))
	Xmpfr_set_prec(cgtls, cgbp, ppint32(29))
	Xmpfr_set_str_binary(cgtls, cgbp, "11111111101001110011010001001E-14\x00")
	if iqlibc.ppBoolInt32(Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "cancellation in add_q\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* Inf */
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	X__gmpz_set_str(cgtls, cgbp+32, "395877315\x00", ppint32(10))
	X__gmpz_set_str(cgtls, cgbp+32+12, "3508975966\x00", ppint32(10))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(118))
	Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))

	if ccv3 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv3 {
		if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv1 {
			Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(89), "! (((y)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(89), "! (((y)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if iqlibc.ppBoolInt32(ccv3 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "inf\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))

	if ccv6 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv6 {
		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(91), "! (((y)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(91), "! (((y)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if iqlibc.ppBoolInt32(ccv6 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "inf\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* Nan */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt32FromInt32(0x7fffffff) - iqlibc.ppInt32FromInt32(1) + iqlibc.ppInt32FromInt32(2)
	Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "nan\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "nan\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* Exact value */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(60))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(60))
	Xmpfr_set_str(cgtls, cgbp, "0.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	X__gmpz_set_str(cgtls, cgbp+32, "3\x00", ppint32(10))
	X__gmpz_set_str(cgtls, cgbp+32+12, "2\x00", ppint32(10))
	aares = Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	if iqlibc.ppBoolInt32(Xmpfr_cmp_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(2)), 0) == 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "0.5+3/2\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	if iqlibc.ppBoolInt32(Xmpfr_cmp_si_2exp(cgtls, cgbp+16, ppint32(-iqlibc.ppInt32FromInt32(1)), 0) == 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "0.5-3/2\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* Inf rational */
	X__gmpq_set_ui(cgtls, cgbp+32, ppuint32(1), ppuint32(0))
	Xmpfr_set_str(cgtls, cgbp, "0.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aares = Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "0.5+1/0\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign < 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "0.5-1/0\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	X__gmpq_set_si(cgtls, cgbp+32, ppint32(-ppint32(1)), ppuint32(0))
	aares = Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign < 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "0.5+ -1/0\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "0.5- -1/0\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_div_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign < 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "0.5 / (-1/0)\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	X__gmpq_set_ui(cgtls, cgbp+32, ppuint32(1), ppuint32(0))
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	aares = Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "+Inf + +Inf\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "+Inf - +Inf\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	aares = Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "-Inf + +Inf\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign < 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "-Inf - +Inf\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	X__gmpq_set_si(cgtls, cgbp+32, ppint32(-ppint32(1)), ppuint32(0))
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	aares = Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "+Inf + -Inf\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "+Inf - -Inf\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	aares = Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign < 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "-Inf + -Inf\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "-Inf - -Inf\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* 0 */
	X__gmpq_set_ui(cgtls, cgbp+32, ppuint32(0), ppuint32(1))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(42)), 0, ppint32(ecMPFR_RNDN))
	aares = Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32(Xmpfr_cmp_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(42)), 0) == 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "42+0/1\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32(Xmpfr_cmp_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(42)), 0) == 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "42-0/1\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	aares = Xmpfr_mul_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(0)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0 && aares == 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "42*0/1\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear_flags(cgtls)
	aares = Xmpfr_div_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0 && aares == 0 && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "42/(0/1)\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Q = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\n   /\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+32+12)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	X__gmpz_set_ui(cgtls, cgbp+56, ppuint32(0))
	Xmpfr_clear_flags(cgtls)
	aares = Xmpfr_div_z(cgtls, cgbp+16, cgbp, cgbp+56, ppint32(ecMPFR_RNDN))
	if iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_sign > 0 && aares == 0 && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0) == 0 {
		Xprintf(cgtls, "Special case error %s. Ternary value = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+80, "42/0\x00", aares, X__gmpfr_flags))
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "X = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Z = \x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+56)
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_mpz_clear(cgtls, cgbp+56)
	X__gmpq_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
}

func sicheck_for_zero(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aai tnmpfr_sign_t
	var aar ppint32
	var pp_ /* q at bp+0 */ tnmpq_t
	var pp_ /* x at bp+36 */ tnmpfr_t
	var pp_ /* z at bp+24 */ tnmpz_t
	pp_, pp_ = aai, aar

	Xmpfr_init(cgtls, cgbp+36)
	Xmpfr_mpz_init(cgtls, cgbp+24)
	X__gmpq_init(cgtls, cgbp)

	X__gmpz_set_ui(cgtls, cgbp+24, ppuint32(0))
	X__gmpq_set_ui(cgtls, cgbp, ppuint32(0), ppuint32(1))

	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 36)).fd_mpfr_exp = -iqlibc.ppInt32FromInt32(0x7fffffff) - iqlibc.ppInt32FromInt32(1) + iqlibc.ppInt32FromInt32(1)
	aar = 0
	for {
		if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}

		aai = -iqlibc.ppInt32FromInt32(1)
		for {
			if !(aai <= ppint32(mvMPFR_SIGN_POS)) {
				break
			}

			(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 36)).fd_mpfr_sign = aai
			Xmpfr_add_z(cgtls, cgbp+36, cgbp+36, cgbp+24, aar)
			if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+36)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1)) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+36)).fd_mpfr_sign != aai {

				Xprintf(cgtls, "GMP Zero errors for add_z & rnd=%s & s=%d\n\x00", iqlibc.ppVaList(cgbp+64, Xmpfr_print_rnd_mode(cgtls, aar), aai))
				Xmpfr_dump(cgtls, cgbp+36)
				Xexit(cgtls, ppint32(1))
			}
			Xmpfr_sub_z(cgtls, cgbp+36, cgbp+36, cgbp+24, aar)
			if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+36)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1)) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+36)).fd_mpfr_sign != aai {

				Xprintf(cgtls, "GMP Zero errors for sub_z & rnd=%s & s=%d\n\x00", iqlibc.ppVaList(cgbp+64, Xmpfr_print_rnd_mode(cgtls, aar), aai))
				Xmpfr_dump(cgtls, cgbp+36)
				Xexit(cgtls, ppint32(1))
			}
			Xmpfr_mul_z(cgtls, cgbp+36, cgbp+36, cgbp+24, aar)
			if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+36)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1)) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+36)).fd_mpfr_sign != aai {

				Xprintf(cgtls, "GMP Zero errors for mul_z & rnd=%s & s=%d\n\x00", iqlibc.ppVaList(cgbp+64, Xmpfr_print_rnd_mode(cgtls, aar), aai))
				Xmpfr_dump(cgtls, cgbp+36)
				Xexit(cgtls, ppint32(1))
			}
			Xmpfr_add_q(cgtls, cgbp+36, cgbp+36, cgbp, aar)
			if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+36)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1)) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+36)).fd_mpfr_sign != aai {

				Xprintf(cgtls, "GMP Zero errors for add_q & rnd=%s & s=%d\n\x00", iqlibc.ppVaList(cgbp+64, Xmpfr_print_rnd_mode(cgtls, aar), aai))
				Xmpfr_dump(cgtls, cgbp+36)
				Xexit(cgtls, ppint32(1))
			}
			Xmpfr_sub_q(cgtls, cgbp+36, cgbp+36, cgbp, aar)
			if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+36)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1)) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+36)).fd_mpfr_sign != aai {

				Xprintf(cgtls, "GMP Zero errors for sub_q & rnd=%s & s=%d\n\x00", iqlibc.ppVaList(cgbp+64, Xmpfr_print_rnd_mode(cgtls, aar), aai))
				Xmpfr_dump(cgtls, cgbp+36)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_2
		cg_2:
			;
			aai += iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS) - -iqlibc.ppInt32FromInt32(1)
		}

		goto cg_1
	cg_1:
		;
		aar++
	}

	X__gmpq_clear(cgtls, cgbp)
	Xmpfr_mpz_clear(cgtls, cgbp+24)
	Xmpfr_clear(cgtls, cgbp+36)
}

func sitest_cmp_z(cgtls *iqlibc.ppTLS, aapmin tnmpfr_prec_t, aapmax tnmpfr_prec_t, aanmax ppint32) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aan, aares1, aares2 ppint32
	var aap tnmpfr_prec_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpz_t
	var pp_ /* z at bp+16 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aan, aap, aares1, aares2

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init2(cgtls, cgbp+16, ppint32(mvMPFR_PREC_MIN))
	Xmpfr_mpz_init(cgtls, cgbp+32)

	/* check the erange flag when x is NaN */
	Xmpfr_set_nan(cgtls, cgbp)
	X__gmpz_set_ui(cgtls, cgbp+32, ppuint32(17))
	Xmpfr_clear_erangeflag(cgtls)
	aares1 = Xmpfr_cmp_z(cgtls, cgbp, cgbp+32)
	if aares1 != 0 || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) == 0 {

		Xprintf(cgtls, "Error for mpfr_cmp_z (NaN, 17)\n\x00", 0)
		Xprintf(cgtls, "Return value: expected 0, got %d\n\x00", iqlibc.ppVaList(cgbp+56, aares1))
		Xprintf(cgtls, "Erange flag: expected set, got %d\n\x00", iqlibc.ppVaList(cgbp+56, iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE))))
		Xexit(cgtls, ppint32(1))
	}

	aap = aapmin
	for {
		if !(aap < aapmax) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aap)
		aan = 0
		for {
			if !(aan < aanmax) {
				break
			}

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			X__gmpz_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppuint32(1024))
			if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)) {

				Xmpfr_sub_z(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
				aares1 = Xmpfr_sgn(cgtls, cgbp+16)
				aares2 = Xmpfr_cmp_z(cgtls, cgbp, cgbp+32)
				if aares1 != aares2 {

					Xprintf(cgtls, "Error for mpfr_cmp_z: res=%d sub_z gives %d\n\x00", iqlibc.ppVaList(cgbp+56, aares2, aares1))
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aap++
	}
	Xmpfr_mpz_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
}

func sitest_cmp_q(cgtls *iqlibc.ppTLS, aapmin tnmpfr_prec_t, aapmax tnmpfr_prec_t, aanmax ppint32) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aan, aares1, aares2 ppint32
	var aap tnmpfr_prec_t
	var ccv11, ccv12, ccv14, ccv16, ccv18, ccv20, ccv21, ccv23, ccv24, ccv26, ccv27, ccv29, ccv30, ccv32, ccv33, ccv5, ccv7, ccv9 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpq_t
	var pp_ /* z at bp+16 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aan, aap, aares1, aares2, ccv11, ccv12, ccv14, ccv16, ccv18, ccv20, ccv21, ccv23, ccv24, ccv26, ccv27, ccv29, ccv30, ccv32, ccv33, ccv5, ccv7, ccv9

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init2(cgtls, cgbp+16, ppint32(mvMPFR_PREC_MIN))
	X__gmpq_init(cgtls, cgbp+32)

	/* Check the flags when x is NaN: the erange flags must be set, and
	   only this one. */
	Xmpfr_set_nan(cgtls, cgbp)
	X__gmpq_set_ui(cgtls, cgbp+32, ppuint32(17), ppuint32(1))
	Xmpfr_clear_flags(cgtls)
	aares1 = Xmpfr_cmp_q(cgtls, cgbp, cgbp+32)
	if aares1 != 0 || X__gmpfr_flags != ppuint32(mvMPFR_FLAGS_ERANGE) {

		Xprintf(cgtls, "Error for mpfr_cmp_q (NaN, 17)\n\x00", 0)
		Xprintf(cgtls, "Return value: expected 0, got %d\n\x00", iqlibc.ppVaList(cgbp+64, aares1))
		Xprintf(cgtls, "Expected flags:\x00", 0)
		Xflags_out(cgtls, ppuint32(mvMPFR_FLAGS_ERANGE))
		Xprintf(cgtls, "Got flags:     \x00", 0)
		Xflags_out(cgtls, X__gmpfr_flags)
		Xexit(cgtls, ppint32(1))
	}

	/* Check the flags when y is NaN: the erange flags must be set, and
	   only this one. */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(42)), 0, ppint32(ecMPFR_RNDN))
	/* A NaN rational is represented by 0/0 (MPFR extension). */
	X__gmpz_set_ui(cgtls, cgbp+32, ppuint32(0))
	X__gmpz_set_ui(cgtls, cgbp+32+12, ppuint32(0))
	Xmpfr_clear_flags(cgtls)
	aares1 = Xmpfr_cmp_q(cgtls, cgbp, cgbp+32)
	if aares1 != 0 || X__gmpfr_flags != ppuint32(mvMPFR_FLAGS_ERANGE) {

		Xprintf(cgtls, "Error for mpfr_cmp_q (42, NaN)\n\x00", 0)
		Xprintf(cgtls, "Return value: expected 0, got %d\n\x00", iqlibc.ppVaList(cgbp+64, aares1))
		Xprintf(cgtls, "Expected flags:\x00", 0)
		Xflags_out(cgtls, ppuint32(mvMPFR_FLAGS_ERANGE))
		Xprintf(cgtls, "Got flags:     \x00", 0)
		Xflags_out(cgtls, X__gmpfr_flags)
		Xexit(cgtls, ppint32(1))
	}

	aap = aapmin
	for {
		if !(aap < aapmax) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aap)
		aan = 0
		for {
			if !(aan < aanmax) {
				break
			}

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			X__gmpq_set_ui(cgtls, cgbp+32, Xrandlimb(cgtls), Xrandlimb(cgtls))
			if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)) {

				Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
				aares1 = Xmpfr_sgn(cgtls, cgbp+16)
				aares2 = Xmpfr_cmp_q(cgtls, cgbp, cgbp+32)
				if aares1 != aares2 {

					Xprintf(cgtls, "Error for mpfr_cmp_q: res=%d sub_z gives %d\n\x00", iqlibc.ppVaList(cgbp+64, aares2, aares1))
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aap++
	}

	/* check for y = 1/0 */
	X__gmpz_set_ui(cgtls, cgbp+32, ppuint32(1))
	X__gmpz_set_ui(cgtls, cgbp+32+12, ppuint32(0))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) < iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(372), "mpfr_cmp_q (x, y) < 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))

	if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) < iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv7 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(374), "mpfr_cmp_q (x, y) < 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inf(cgtls, cgbp, +iqlibc.ppInt32FromInt32(1))

	if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv9 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(376), "mpfr_cmp_q (x, y) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_clear_erangeflag(cgtls)

	if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv11 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(379), "mpfr_cmp_q (x, y) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0)), ppint32(1)) != 0; !ccv12 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(380), "((int) (__gmpfr_flags & 16))\x00")
	}
	pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* check for y = -1/0 */
	X__gmpz_set_si(cgtls, cgbp+32, ppint32(-ppint32(1)))
	X__gmpz_set_ui(cgtls, cgbp+32+12, ppuint32(0))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))

	if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) > iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv14 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(386), "mpfr_cmp_q (x, y) > 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))

	if ccv16 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv16 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(388), "mpfr_cmp_q (x, y) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv16 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inf(cgtls, cgbp, +iqlibc.ppInt32FromInt32(1))

	if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) > iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv18 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(390), "mpfr_cmp_q (x, y) > 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_clear_erangeflag(cgtls)

	if ccv20 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv20 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(393), "mpfr_cmp_q (x, y) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv20 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv21 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0)), ppint32(1)) != 0; !ccv21 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(394), "((int) (__gmpfr_flags & 16))\x00")
	}
	pp_ = ccv21 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* check for y = 0/0 */
	X__gmpz_set_ui(cgtls, cgbp+32, ppuint32(0))
	X__gmpz_set_ui(cgtls, cgbp+32+12, ppuint32(0))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_clear_erangeflag(cgtls)

	if ccv23 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv23 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(401), "mpfr_cmp_q (x, y) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv23 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv24 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0)), ppint32(1)) != 0; !ccv24 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(402), "((int) (__gmpfr_flags & 16))\x00")
	}
	pp_ = ccv24 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_clear_erangeflag(cgtls)

	if ccv26 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv26 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(405), "mpfr_cmp_q (x, y) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv26 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv27 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0)), ppint32(1)) != 0; !ccv27 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(406), "((int) (__gmpfr_flags & 16))\x00")
	}
	pp_ = ccv27 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inf(cgtls, cgbp, +iqlibc.ppInt32FromInt32(1))
	Xmpfr_clear_erangeflag(cgtls)

	if ccv29 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv29 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(409), "mpfr_cmp_q (x, y) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv29 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv30 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0)), ppint32(1)) != 0; !ccv30 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(410), "((int) (__gmpfr_flags & 16))\x00")
	}
	pp_ = ccv30 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_clear_erangeflag(cgtls)

	if ccv32 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv32 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(413), "mpfr_cmp_q (x, y) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_q(cgtls, cgbp, cgbp+32) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv32 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv33 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0)), ppint32(1)) != 0; !ccv33 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(414), "((int) (__gmpfr_flags & 16))\x00")
	}
	pp_ = ccv33 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	X__gmpq_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
}

func sitest_cmp_f(cgtls *iqlibc.ppTLS, aapmin tnmpfr_prec_t, aapmax tnmpfr_prec_t, aanmax ppint32) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aan, aares1, aares2 ppint32
	var aap tnmpfr_prec_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpf_t
	var pp_ /* z at bp+16 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aan, aap, aares1, aares2

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init2(cgtls, cgbp+16, aapmax+ppint32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
	X__gmpf_init2(cgtls, cgbp+32, ppuint32(mvMPFR_PREC_MIN))

	/* check the erange flag when x is NaN */
	Xmpfr_set_nan(cgtls, cgbp)
	X__gmpf_set_ui(cgtls, cgbp+32, ppuint32(17))
	Xmpfr_clear_erangeflag(cgtls)
	aares1 = Xmpfr_cmp_f(cgtls, cgbp, cgbp+32)
	if aares1 != 0 || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) == 0 {

		Xprintf(cgtls, "Error for mpfr_cmp_f (NaN, 17)\n\x00", 0)
		Xprintf(cgtls, "Return value: expected 0, got %d\n\x00", iqlibc.ppVaList(cgbp+56, aares1))
		Xprintf(cgtls, "Erange flag: expected set, got %d\n\x00", iqlibc.ppVaList(cgbp+56, iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE))))
		Xexit(cgtls, ppint32(1))
	}

	aap = aapmin
	for {
		if !(aap < aapmax) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aap)
		X__gmpf_set_prec(cgtls, cgbp+32, iqlibc.ppUint32FromInt32(aap))
		aan = 0
		for {
			if !(aan < aanmax) {
				break
			}

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			X__gmpf_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), iqlibc.ppUint32FromInt32(aap))
			if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(3)) {

				Xmpfr_set_f(cgtls, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
				Xmpfr_sub(cgtls, cgbp+16, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
				aares1 = Xmpfr_sgn(cgtls, cgbp+16)
				aares2 = Xmpfr_cmp_f(cgtls, cgbp, cgbp+32)
				if aares1 != aares2 {

					Xprintf(cgtls, "Error for mpfr_cmp_f: res=%d sub gives %d\n\x00", iqlibc.ppVaList(cgbp+56, aares2, aares1))
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aap += ppint32(3)
	}
	X__gmpf_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
}

func sitest_specialz(cgtls *iqlibc.ppTLS, aampfr_func ppuintptr, aampz_func ppuintptr, aaop ppuintptr) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aares ppint32
	var pp_ /* x1 at bp+0 */ tnmpfr_t
	var pp_ /* x2 at bp+16 */ tnmpfr_t
	var pp_ /* z1 at bp+32 */ tnmpz_t
	var pp_ /* z2 at bp+44 */ tnmpz_t
	pp_ = aares

	Xmpfr_inits2(cgtls, ppint32(128), cgbp, iqlibc.ppVaList(cgbp+64, cgbp+16, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_mpz_init(cgtls, cgbp+32)
	Xmpfr_mpz_init(cgtls, cgbp+44)
	X__gmpz_fac_ui(cgtls, cgbp+32, ppuint32(19)) /* 19!+1 fits perfectly in a 128 bits mantissa */
	X__gmpz_add_ui(cgtls, cgbp+32, cgbp+32, ppuint32(1))
	X__gmpz_fac_ui(cgtls, cgbp+44, ppuint32(20)) /* 20!+1 fits perfectly in a 128 bits mantissa */
	X__gmpz_add_ui(cgtls, cgbp+44, cgbp+44, ppuint32(1))

	aares = Xmpfr_set_z(cgtls, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if aares != 0 {

		Xprintf(cgtls, "Specialz %s: set_z1 error\n\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_z(cgtls, cgbp+16, cgbp+44, ppint32(ecMPFR_RNDN))
	if aares != 0 {

		Xprintf(cgtls, "Specialz %s: set_z2 error\n\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xexit(cgtls, ppint32(1))
	}

	/* (19!+1) * (20!+1) fits in a 128 bits number */
	aares = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpz_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aampfr_func})))(cgtls, cgbp, cgbp, cgbp+44, ppint32(ecMPFR_RNDN))
	if aares != 0 {

		Xprintf(cgtls, "Specialz %s: wrong inexact flag.\n\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xexit(cgtls, ppint32(1))
	}
	(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aampz_func})))(cgtls, cgbp+32, cgbp+32, cgbp+44)
	aares = Xmpfr_set_z(cgtls, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if aares != 0 {

		Xprintf(cgtls, "Specialz %s: set_z2 error\n\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Specialz %s: results differ.\nx1=\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "x2=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "Z2=\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(2), cgbp+32)
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}

	X__gmpz_set_ui(cgtls, cgbp+32, ppuint32(1))
	X__gmpz_set_ui(cgtls, cgbp+44, ppuint32(0))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aampz_func})))(cgtls, cgbp+32, cgbp+32, cgbp+44)
	aares = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpz_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aampfr_func})))(cgtls, cgbp, cgbp, cgbp+44, ppint32(ecMPFR_RNDN))
	Xmpfr_set_z(cgtls, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Specialz %s: results differ(2).\nx1=\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "x2=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_mpz_clear(cgtls, cgbp+32)
	Xmpfr_mpz_clear(cgtls, cgbp+44)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+64, cgbp+16, iqlibc.ppUintptrFromInt32(0)))
}

func sitest_special2z(cgtls *iqlibc.ppTLS, aampfr_func ppuintptr, aampz_func ppuintptr, aaop ppuintptr) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aares ppint32
	var pp_ /* x1 at bp+0 */ tnmpfr_t
	var pp_ /* x2 at bp+16 */ tnmpfr_t
	var pp_ /* z1 at bp+32 */ tnmpz_t
	var pp_ /* z2 at bp+44 */ tnmpz_t
	pp_ = aares

	Xmpfr_inits2(cgtls, ppint32(128), cgbp, iqlibc.ppVaList(cgbp+64, cgbp+16, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_mpz_init(cgtls, cgbp+32)
	Xmpfr_mpz_init(cgtls, cgbp+44)
	X__gmpz_fac_ui(cgtls, cgbp+32, ppuint32(19)) /* 19!+1 fits perfectly in a 128 bits mantissa */
	X__gmpz_add_ui(cgtls, cgbp+32, cgbp+32, ppuint32(1))
	X__gmpz_fac_ui(cgtls, cgbp+44, ppuint32(20)) /* 20!+1 fits perfectly in a 128 bits mantissa */
	X__gmpz_add_ui(cgtls, cgbp+44, cgbp+44, ppuint32(1))

	aares = Xmpfr_set_z(cgtls, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if aares != 0 {

		Xprintf(cgtls, "Special2z %s: set_z1 error\n\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_z(cgtls, cgbp+16, cgbp+44, ppint32(ecMPFR_RNDN))
	if aares != 0 {

		Xprintf(cgtls, "Special2z %s: set_z2 error\n\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xexit(cgtls, ppint32(1))
	}

	/* (19!+1) * (20!+1) fits in a 128 bits number */
	aares = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpz_srcptr, tnmpfr_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aampfr_func})))(cgtls, cgbp, cgbp+32, cgbp+16, ppint32(ecMPFR_RNDN))
	if aares != 0 {

		Xprintf(cgtls, "Special2z %s: wrong inexact flag.\n\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xexit(cgtls, ppint32(1))
	}
	(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aampz_func})))(cgtls, cgbp+32, cgbp+32, cgbp+44)
	aares = Xmpfr_set_z(cgtls, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if aares != 0 {

		Xprintf(cgtls, "Special2z %s: set_z2 error\n\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Special2z %s: results differ.\nx1=\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "x2=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "Z2=\x00", 0)
		X__gmpz_out_str(cgtls, Xstdout, ppint32(2), cgbp+32)
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}

	X__gmpz_set_ui(cgtls, cgbp+32, ppuint32(0))
	X__gmpz_set_ui(cgtls, cgbp+44, ppuint32(1))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	aares = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpz_srcptr, tnmpfr_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aampfr_func})))(cgtls, cgbp, cgbp+32, cgbp+16, ppint32(ecMPFR_RNDN))
	(*(*func(*iqlibc.ppTLS, tnmpz_ptr, tnmpz_srcptr, tnmpz_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aampz_func})))(cgtls, cgbp+32, cgbp+32, cgbp+44)
	Xmpfr_set_z(cgtls, cgbp+16, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Special2z %s: results differ(2).\nx1=\x00", iqlibc.ppVaList(cgbp+64, aaop))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "x2=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_mpz_clear(cgtls, cgbp+32)
	Xmpfr_mpz_clear(cgtls, cgbp+44)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+64, cgbp+16, iqlibc.ppUintptrFromInt32(0)))
}

func sitest_genericz(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aaN ppuint32, aafunc ppuintptr, aaop ppuintptr) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aa_p tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, ccv5 ppint32
	var aan ppuint32
	var aaprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var pp_ /* arg1 at bp+0 */ tnmpfr_t
	var pp_ /* arg2 at bp+64 */ tnmpz_t
	var pp_ /* dst_big at bp+16 */ tnmpfr_t
	var pp_ /* dst_small at bp+32 */ tnmpfr_t
	var pp_ /* tmp at bp+48 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aacompare, aacompare2, aainexact, aan, aaprec, aarnd, ccv5

	Xmpfr_inits(cgtls, cgbp, iqlibc.ppVaList(cgbp+88, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_mpz_init(cgtls, cgbp+64)

	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+48, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+32, aaprec)

		aan = ppuint32(0)
		for {
			if !(aan < aaN) {
				break
			}

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			X__gmpz_urandomb(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppuint32(1024))
			aarnd = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % ppuint32(ecMPFR_RNDF))
			Xmpfr_set_prec(cgtls, cgbp+16, ppint32(2)*aaprec)
			aacompare = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpz_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, cgbp+16, cgbp, cgbp+64, aarnd)
			if Xmpfr_can_round(cgtls, cgbp+16, ppint32(2)*aaprec, aarnd, aarnd, aaprec) != 0 {

				{
					aa_p = cgbp + 16
					ccv5 = Xmpfr_set4(cgtls, cgbp+48, aa_p, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
				}
				pp_ = ccv5
				aainexact = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpz_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, cgbp+32, cgbp, cgbp+64, aarnd)
				if Xmpfr_cmp3(cgtls, cgbp+48, cgbp+32, ppint32(1)) != 0 {

					Xprintf(cgtls, "Results differ for prec=%u rnd_mode=%s and %s_z:\narg1=\x00", iqlibc.ppVaList(cgbp+88, iqlibc.ppUint32FromInt32(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aaop))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "arg2=\x00", 0)
					X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+64)
					Xprintf(cgtls, "\ngot      \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "expected \x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xprintf(cgtls, "approx   \x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xexit(cgtls, ppint32(1))
				}
				aacompare2 = Xmpfr_cmp3(cgtls, cgbp+48, cgbp+16, ppint32(1))
				/* if rounding to nearest, cannot know the sign of t - f(x)
				   because of composed rounding: y = o(f(x)) and t = o(y) */
				if aacompare*aacompare2 >= 0 {
					aacompare = aacompare + aacompare2
				} else {
					aacompare = aainexact
				} /* cannot determine sign(t-f(x)) */
				if aainexact == 0 && aacompare != 0 || aainexact > 0 && aacompare <= 0 || aainexact < 0 && aacompare >= 0 {

					Xprintf(cgtls, "Wrong inexact flag for rnd=%s and %s_z:\nexpected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+88, Xmpfr_print_rnd_mode(cgtls, aarnd), aaop, aacompare, aainexact))
					Xprintf(cgtls, "arg1=\x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "arg2=\x00", 0)
					X__gmpz_out_str(cgtls, Xstdout, ppint32(2), cgbp+64)
					Xprintf(cgtls, "\ndstl=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xprintf(cgtls, "dsts=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "tmp =\x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	Xmpfr_mpz_clear(cgtls, cgbp+64)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+88, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
}

func sitest_generic2z(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aaN ppuint32, aafunc ppuintptr, aaop ppuintptr) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aa_p tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, ccv5 ppint32
	var aan ppuint32
	var aaprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var pp_ /* arg1 at bp+0 */ tnmpfr_t
	var pp_ /* arg2 at bp+64 */ tnmpz_t
	var pp_ /* dst_big at bp+16 */ tnmpfr_t
	var pp_ /* dst_small at bp+32 */ tnmpfr_t
	var pp_ /* tmp at bp+48 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aacompare, aacompare2, aainexact, aan, aaprec, aarnd, ccv5

	Xmpfr_inits(cgtls, cgbp, iqlibc.ppVaList(cgbp+88, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_mpz_init(cgtls, cgbp+64)

	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+48, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+32, aaprec)

		aan = ppuint32(0)
		for {
			if !(aan < aaN) {
				break
			}

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			X__gmpz_urandomb(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppuint32(1024))
			aarnd = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % ppuint32(ecMPFR_RNDF))
			Xmpfr_set_prec(cgtls, cgbp+16, ppint32(2)*aaprec)
			aacompare = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpz_srcptr, tnmpfr_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, cgbp+16, cgbp+64, cgbp, aarnd)
			if Xmpfr_can_round(cgtls, cgbp+16, ppint32(2)*aaprec, aarnd, aarnd, aaprec) != 0 {

				{
					aa_p = cgbp + 16
					ccv5 = Xmpfr_set4(cgtls, cgbp+48, aa_p, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
				}
				pp_ = ccv5
				aainexact = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpz_srcptr, tnmpfr_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, cgbp+32, cgbp+64, cgbp, aarnd)
				if Xmpfr_cmp3(cgtls, cgbp+48, cgbp+32, ppint32(1)) != 0 {

					Xprintf(cgtls, "Results differ for prec=%u rnd_mode=%s and %s_z:\narg1=\x00", iqlibc.ppVaList(cgbp+88, iqlibc.ppUint32FromInt32(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aaop))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "arg2=\x00", 0)
					X__gmpz_out_str(cgtls, Xstdout, ppint32(10), cgbp+64)
					Xprintf(cgtls, "\ngot      \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "expected \x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xprintf(cgtls, "approx   \x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xexit(cgtls, ppint32(1))
				}
				aacompare2 = Xmpfr_cmp3(cgtls, cgbp+48, cgbp+16, ppint32(1))
				/* if rounding to nearest, cannot know the sign of t - f(x)
				   because of composed rounding: y = o(f(x)) and t = o(y) */
				if aacompare*aacompare2 >= 0 {
					aacompare = aacompare + aacompare2
				} else {
					aacompare = aainexact
				} /* cannot determine sign(t-f(x)) */
				if aainexact == 0 && aacompare != 0 || aainexact > 0 && aacompare <= 0 || aainexact < 0 && aacompare >= 0 {

					Xprintf(cgtls, "Wrong inexact flag for rnd=%s and %s_z:\nexpected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+88, Xmpfr_print_rnd_mode(cgtls, aarnd), aaop, aacompare, aainexact))
					Xprintf(cgtls, "arg1=\x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "arg2=\x00", 0)
					X__gmpz_out_str(cgtls, Xstdout, ppint32(2), cgbp+64)
					Xprintf(cgtls, "\ndstl=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xprintf(cgtls, "dsts=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "tmp =\x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	Xmpfr_mpz_clear(cgtls, cgbp+64)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+88, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
}

func sitest_genericq(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aaN ppuint32, aafunc ppuintptr, aaop ppuintptr) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aa_p tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, ccv4 ppint32
	var aan ppuint32
	var aaprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var pp_ /* arg1 at bp+0 */ tnmpfr_t
	var pp_ /* arg2 at bp+64 */ tnmpq_t
	var pp_ /* dst_big at bp+16 */ tnmpfr_t
	var pp_ /* dst_small at bp+32 */ tnmpfr_t
	var pp_ /* tmp at bp+48 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aacompare, aacompare2, aainexact, aan, aaprec, aarnd, ccv4

	Xmpfr_inits(cgtls, cgbp, iqlibc.ppVaList(cgbp+96, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
	X__gmpq_init(cgtls, cgbp+64)

	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+48, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+32, aaprec)

		aan = ppuint32(0)
		for {
			if !(aan < aaN) {
				break
			}

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			X__gmpq_set_ui(cgtls, cgbp+64, Xrandlimb(cgtls), Xrandlimb(cgtls))
			X__gmpq_canonicalize(cgtls, cgbp+64)
			aarnd = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % ppuint32(ecMPFR_RNDF))
			Xmpfr_set_prec(cgtls, cgbp+16, aaprec+ppint32(10))
			aacompare = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpq_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, cgbp+16, cgbp, cgbp+64, aarnd)
			if Xmpfr_can_round(cgtls, cgbp+16, aaprec+ppint32(10), aarnd, aarnd, aaprec) != 0 {

				{
					aa_p = cgbp + 16
					ccv4 = Xmpfr_set4(cgtls, cgbp+48, aa_p, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
				}
				pp_ = ccv4
				aainexact = (*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpq_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aafunc})))(cgtls, cgbp+32, cgbp, cgbp+64, aarnd)
				if Xmpfr_cmp3(cgtls, cgbp+48, cgbp+32, ppint32(1)) != 0 {

					Xprintf(cgtls, "Results differ for prec=%u rnd_mode=%s and %s_q:\narg1=\x00", iqlibc.ppVaList(cgbp+96, iqlibc.ppUint32FromInt32(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aaop))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "arg2=\x00", 0)
					X__gmpq_out_str(cgtls, Xstdout, ppint32(2), cgbp+64)
					Xprintf(cgtls, "\ngot      \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "expected \x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xprintf(cgtls, "approx  \x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xexit(cgtls, ppint32(1))
				}
				aacompare2 = Xmpfr_cmp3(cgtls, cgbp+48, cgbp+16, ppint32(1))
				/* if rounding to nearest, cannot know the sign of t - f(x)
				   because of composed rounding: y = o(f(x)) and t = o(y) */
				if aacompare*aacompare2 >= 0 {
					aacompare = aacompare + aacompare2
				} else {
					aacompare = aainexact
				} /* cannot determine sign(t-f(x)) */
				if aainexact == 0 && aacompare != 0 || aainexact > 0 && aacompare <= 0 || aainexact < 0 && aacompare >= 0 {

					Xprintf(cgtls, "Wrong inexact flag for rnd=%s and %s_q:\nexpected %d, got %d\x00", iqlibc.ppVaList(cgbp+96, Xmpfr_print_rnd_mode(cgtls, aarnd), aaop, aacompare, aainexact))
					Xprintf(cgtls, "arg1=\x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "arg2=\x00", 0)
					X__gmpq_out_str(cgtls, Xstdout, ppint32(2), cgbp+64)
					Xprintf(cgtls, "\ndstl=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+16)
					Xprintf(cgtls, "dsts=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "tmp =\x00", 0)
					Xmpfr_dump(cgtls, cgbp+48)
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	X__gmpq_clear(cgtls, cgbp+64)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+96, cgbp+16, cgbp+32, cgbp+48, iqlibc.ppUintptrFromInt32(0)))
}

func sitest_specialq(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aaN ppuint32, aampfr_func ppuintptr, aampq_func ppuintptr, aaop ppuintptr) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aan ppuint32
	var aaprec tnmpfr_prec_t
	var pp_ /* fra at bp+0 */ tnmpfr_t
	var pp_ /* frb at bp+16 */ tnmpfr_t
	var pp_ /* frq at bp+32 */ tnmpfr_t
	var pp_ /* q1 at bp+48 */ tnmpq_t
	var pp_ /* q2 at bp+72 */ tnmpq_t
	var pp_ /* qr at bp+96 */ tnmpq_t
	pp_, pp_ = aan, aaprec

	aaprec = aap0
	for {
		if !(aaprec < aap1) {
			break
		}

		Xmpfr_inits2(cgtls, aaprec, cgbp, iqlibc.ppVaList(cgbp+128, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
		X__gmpq_init(cgtls, cgbp+48)
		X__gmpq_init(cgtls, cgbp+72)
		X__gmpq_init(cgtls, cgbp+96)

		aan = ppuint32(0)
		for {
			if !(aan < aaN) {
				break
			}

			X__gmpq_set_ui(cgtls, cgbp+48, Xrandlimb(cgtls), Xrandlimb(cgtls))
			X__gmpq_set_ui(cgtls, cgbp+72, Xrandlimb(cgtls), Xrandlimb(cgtls))
			X__gmpq_canonicalize(cgtls, cgbp+48)
			X__gmpq_canonicalize(cgtls, cgbp+72)
			(*(*func(*iqlibc.ppTLS, tnmpq_ptr, tnmpq_srcptr, tnmpq_srcptr))(iqunsafe.ppPointer(&struct{ ppuintptr }{aampq_func})))(cgtls, cgbp+96, cgbp+48, cgbp+72)
			Xmpfr_set_q(cgtls, cgbp, cgbp+48, ppint32(ecMPFR_RNDD))
			(*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpq_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aampfr_func})))(cgtls, cgbp, cgbp, cgbp+72, ppint32(ecMPFR_RNDD))
			Xmpfr_set_q(cgtls, cgbp+16, cgbp+48, ppint32(ecMPFR_RNDU))
			(*(*func(*iqlibc.ppTLS, tnmpfr_ptr, tnmpfr_srcptr, tnmpq_srcptr, tnmpfr_rnd_t) ppint32)(iqunsafe.ppPointer(&struct{ ppuintptr }{aampfr_func})))(cgtls, cgbp+16, cgbp+16, cgbp+72, ppint32(ecMPFR_RNDU))
			Xmpfr_set_q(cgtls, cgbp+32, cgbp+96, ppint32(ecMPFR_RNDN))
			/* We should have fra <= qr <= frb */
			if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) > 0 || Xmpfr_cmp3(cgtls, cgbp+32, cgbp+16, ppint32(1)) > 0 {

				Xprintf(cgtls, "Range error for prec=%lu and %s\x00", iqlibc.ppVaList(cgbp+128, iqlibc.ppUint32FromInt32(aaprec), aaop))
				Xprintf(cgtls, "\nq1=\x00", 0)
				X__gmpq_out_str(cgtls, Xstdout, ppint32(2), cgbp+48)
				Xprintf(cgtls, "\nq2=\x00", 0)
				X__gmpq_out_str(cgtls, Xstdout, ppint32(2), cgbp+72)
				Xprintf(cgtls, "\nfr_dn=\x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "fr_q =\x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xprintf(cgtls, "fr_up=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+16)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_2
		cg_2:
			;
			aan++
		}

		X__gmpq_clear(cgtls, cgbp+48)
		X__gmpq_clear(cgtls, cgbp+72)
		X__gmpq_clear(cgtls, cgbp+96)
		Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+128, cgbp+16, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

		goto cg_1
	cg_1:
		;
		aaprec++
	}
}

func sibug_mul_q_20100810(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aainexact ppint32
	var pp_ /* q at bp+32 */ tnmpq_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	pp_ = aainexact

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+16)
	X__gmpq_init(cgtls, cgbp+32)

	/* mpfr_mul_q: the inexact value must be set in case of overflow */
	X__gmpq_set_ui(cgtls, cgbp+32, ppuint32(4096), ppuint32(3))
	Xmpfr_set_inf(cgtls, cgbp, +iqlibc.ppInt32FromInt32(1))
	Xmpfr_nextbelow(cgtls, cgbp)
	aainexact = Xmpfr_mul_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))

	if aainexact <= 0 {

		Xprintf(cgtls, "Overflow error in mpfr_mul_q. \x00", 0)
		Xprintf(cgtls, "Wrong inexact flag: got %d, should be positive.\n\x00", iqlibc.ppVaList(cgbp+64, aainexact))

		Xexit(cgtls, ppint32(1))
	}
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "Overflow error in mpfr_mul_q (y, x, q, MPFR_RNDD). \x00", 0)
		Xprintf(cgtls, "\nx = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp, ppint32(ecMPFR_RNDD))
		Xprintf(cgtls, "\nq = \x00", 0)
		X__gmpq_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\ny = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDD))
		Xprintf(cgtls, " (should be +infinity)\n\x00", 0)

		Xexit(cgtls, ppint32(1))
	}

	X__gmpq_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp)
}

func sibug_div_q_20100810(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aainexact ppint32
	var pp_ /* q at bp+32 */ tnmpq_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	pp_ = aainexact

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+16)
	X__gmpq_init(cgtls, cgbp+32)

	/* mpfr_div_q: the inexact value must be set in case of overflow */
	X__gmpq_set_ui(cgtls, cgbp+32, ppuint32(3), ppuint32(4096))
	Xmpfr_set_inf(cgtls, cgbp, +iqlibc.ppInt32FromInt32(1))
	Xmpfr_nextbelow(cgtls, cgbp)
	aainexact = Xmpfr_div_q(cgtls, cgbp+16, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))

	if aainexact <= 0 {

		Xprintf(cgtls, "Overflow error in mpfr_div_q. \x00", 0)
		Xprintf(cgtls, "Wrong inexact flag: got %d, should be positive.\n\x00", iqlibc.ppVaList(cgbp+64, aainexact))

		Xexit(cgtls, ppint32(1))
	}
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "Overflow error in mpfr_div_q (y, x, q, MPFR_RNDD). \x00", 0)
		Xprintf(cgtls, "\nx = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp, ppint32(ecMPFR_RNDD))
		Xprintf(cgtls, "\nq = \x00", 0)
		X__gmpq_out_str(cgtls, Xstdout, ppint32(10), cgbp+32)
		Xprintf(cgtls, "\ny = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp+16, ppint32(ecMPFR_RNDD))
		Xprintf(cgtls, " (should be +infinity)\n\x00", 0)

		Xexit(cgtls, ppint32(1))
	}

	X__gmpq_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp)
}

func sibug_mul_div_q_20100818(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aae, aaemax, aaemin tnmpfr_exp_t
	var aainex, aarnd ppint32
	var ccv1, ccv11, ccv12, ccv13, ccv2, ccv5, ccv6, ccv7, ccv8 ppbool
	var pp_ /* qa at bp+0 */ tnmpq_t
	var pp_ /* qb at bp+24 */ tnmpq_t
	var pp_ /* x1 at bp+48 */ tnmpfr_t
	var pp_ /* x2 at bp+64 */ tnmpfr_t
	var pp_ /* y1 at bp+80 */ tnmpfr_t
	var pp_ /* y2 at bp+96 */ tnmpfr_t
	var pp_ /* y3 at bp+112 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aae, aaemax, aaemin, aainex, aarnd, ccv1, ccv11, ccv12, ccv13, ccv2, ccv5, ccv6, ccv7, ccv8

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	Xset_emin(cgtls, iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2)))
	Xset_emax(cgtls, iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))-iqlibc.ppInt32FromInt32(1))

	X__gmpq_init(cgtls, cgbp)
	X__gmpq_init(cgtls, cgbp+24)
	Xmpfr_inits2(cgtls, ppint32(32), cgbp+48, iqlibc.ppVaList(cgbp+136, cgbp+64, cgbp+80, cgbp+96, cgbp+112, iqlibc.ppUintptrFromInt32(0)))

	X__gmpq_set_ui(cgtls, cgbp, ppuint32(3), ppuint32(17))
	X__gmpq_set_ui(cgtls, cgbp+24, ppuint32(17), ppuint32(3))
	aainex = Xmpfr_set_ui_2exp(cgtls, cgbp+48, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(7)), 0, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1011), "inex == 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aae = iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2)) - iqlibc.ppInt32FromInt32(1) - iqlibc.ppInt32FromInt32(3)
	aainex = Xmpfr_set_ui_2exp(cgtls, cgbp+64, ppuint32(7), aae, ppint32(ecMPFR_RNDN)) /* x2 = x1 * 2^e */

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1015), "inex == 0\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aarnd = 0
	for {
		if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}

		Xmpfr_mul_q(cgtls, cgbp+80, cgbp+48, cgbp, aarnd)
		Xmpfr_div_q(cgtls, cgbp+112, cgbp+48, cgbp+24, aarnd)

		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_equal_p(cgtls, cgbp+80, cgbp+112) != 0)), ppint32(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1021), "mpfr_equal_p (y1, y3)\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+80, cgbp+112) != 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aainex = Xmpfr_set_ui_2exp(cgtls, cgbp+112, ppuint32(1), aae, ppint32(ecMPFR_RNDN))

		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1023), "inex == 0\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aainex = Xmpfr_mul(cgtls, cgbp+112, cgbp+112, cgbp+80, ppint32(ecMPFR_RNDN)) /* y3 = y1 * 2^e */

		if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv7 {
			Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1025), "inex == 0\x00")
		}
		pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		Xmpfr_mul_q(cgtls, cgbp+96, cgbp+64, cgbp, aarnd)
		if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+112) != 0) {

			Xprintf(cgtls, "Error 1 in bug_mul_div_q_20100818 (rnd = %d)\n\x00", iqlibc.ppVaList(cgbp+136, aarnd))
			Xprintf(cgtls, "Expected \x00", 0)
			Xmpfr_dump(cgtls, cgbp+112)
			Xprintf(cgtls, "Got      \x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xexit(cgtls, ppint32(1))
		}
		Xmpfr_div_q(cgtls, cgbp+96, cgbp+64, cgbp+24, aarnd)
		if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+112) != 0) {

			Xprintf(cgtls, "Error 2 in bug_mul_div_q_20100818 (rnd = %d)\n\x00", iqlibc.ppVaList(cgbp+136, aarnd))
			Xprintf(cgtls, "Expected \x00", 0)
			Xmpfr_dump(cgtls, cgbp+112)
			Xprintf(cgtls, "Got      \x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xexit(cgtls, ppint32(1))
		}

		goto cg_3
	cg_3:
		;
		aarnd++
	}

	aae = iqlibc.ppInt32FromInt32(1) - iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))
	aainex = Xmpfr_set_ui_2exp(cgtls, cgbp+64, ppuint32(7), aae, ppint32(ecMPFR_RNDN)) /* x2 = x1 * 2^e */

	if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv8 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1046), "inex == 0\x00")
	}
	pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aarnd = 0
	for {
		if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}

		Xmpfr_div_q(cgtls, cgbp+80, cgbp+48, cgbp, aarnd)
		Xmpfr_mul_q(cgtls, cgbp+112, cgbp+48, cgbp+24, aarnd)

		if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_equal_p(cgtls, cgbp+80, cgbp+112) != 0)), ppint32(1)) != 0; !ccv11 {
			Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1052), "mpfr_equal_p (y1, y3)\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+80, cgbp+112) != 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aainex = Xmpfr_set_ui_2exp(cgtls, cgbp+112, ppuint32(1), aae, ppint32(ecMPFR_RNDN))

		if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv12 {
			Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1054), "inex == 0\x00")
		}
		pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aainex = Xmpfr_mul(cgtls, cgbp+112, cgbp+112, cgbp+80, ppint32(ecMPFR_RNDN)) /* y3 = y1 * 2^e */

		if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv13 {
			Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1056), "inex == 0\x00")
		}
		pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		Xmpfr_div_q(cgtls, cgbp+96, cgbp+64, cgbp, aarnd)
		if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+112) != 0) {

			Xprintf(cgtls, "Error 3 in bug_mul_div_q_20100818 (rnd = %d)\n\x00", iqlibc.ppVaList(cgbp+136, aarnd))
			Xprintf(cgtls, "Expected \x00", 0)
			Xmpfr_dump(cgtls, cgbp+112)
			Xprintf(cgtls, "Got      \x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xexit(cgtls, ppint32(1))
		}
		Xmpfr_mul_q(cgtls, cgbp+96, cgbp+64, cgbp+24, aarnd)
		if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+112) != 0) {

			Xprintf(cgtls, "Error 4 in bug_mul_div_q_20100818 (rnd = %d)\n\x00", iqlibc.ppVaList(cgbp+136, aarnd))
			Xprintf(cgtls, "Expected \x00", 0)
			Xmpfr_dump(cgtls, cgbp+112)
			Xprintf(cgtls, "Got      \x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xexit(cgtls, ppint32(1))
		}

		goto cg_9
	cg_9:
		;
		aarnd++
	}

	X__gmpq_clear(cgtls, cgbp)
	X__gmpq_clear(cgtls, cgbp+24)
	Xmpfr_clears(cgtls, cgbp+48, iqlibc.ppVaList(cgbp+136, cgbp+64, cgbp+80, cgbp+96, cgbp+112, iqlibc.ppUintptrFromInt32(0)))

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

func sireduced_expo_range(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaemin tnmpfr_exp_t
	var aainex ppint32
	var ccv1, ccv2 ppbool
	var pp_ /* q at bp+28 */ tnmpq_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* z at bp+16 */ tnmpz_t
	pp_, pp_, pp_, pp_ = aaemin, aainex, ccv1, ccv2

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint32(4))

	Xmpfr_init2(cgtls, cgbp, ppint32(32))

	Xmpfr_mpz_init(cgtls, cgbp+16)
	Xmpfr_clear_flags(cgtls)
	aainex = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(17)), 0, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1100), "inex == 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	X__gmpz_set_ui(cgtls, cgbp+16, ppuint32(3))
	aainex = Xmpfr_mul_z(cgtls, cgbp, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if aainex != 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(51)), 0) != 0 {

		Xprintf(cgtls, "Error 1 in reduce_expo_range: expected 51 with inex = 0, got\n\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "with inex = %d\n\x00", iqlibc.ppVaList(cgbp+64, aainex))
		Xexit(cgtls, ppint32(1))
	}
	aainex = Xmpfr_div_z(cgtls, cgbp, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if aainex != 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(17)), 0) != 0 {

		Xprintf(cgtls, "Error 2 in reduce_expo_range: expected 17 with inex = 0, got\n\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "with inex = %d\n\x00", iqlibc.ppVaList(cgbp+64, aainex))
		Xexit(cgtls, ppint32(1))
	}
	aainex = Xmpfr_add_z(cgtls, cgbp, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if aainex != 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(20)), 0) != 0 {

		Xprintf(cgtls, "Error 3 in reduce_expo_range: expected 20 with inex = 0, got\n\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "with inex = %d\n\x00", iqlibc.ppVaList(cgbp+64, aainex))
		Xexit(cgtls, ppint32(1))
	}
	aainex = Xmpfr_sub_z(cgtls, cgbp, cgbp, cgbp+16, ppint32(ecMPFR_RNDN))
	if aainex != 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(17)), 0) != 0 {

		Xprintf(cgtls, "Error 4 in reduce_expo_range: expected 17 with inex = 0, got\n\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "with inex = %d\n\x00", iqlibc.ppVaList(cgbp+64, aainex))
		Xexit(cgtls, ppint32(1))
	}

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint32(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1138), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if Xmpfr_cmp_z(cgtls, cgbp, cgbp+16) <= 0 {

		Xprintf(cgtls, "Error 5 in reduce_expo_range: expected a positive value.\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_mpz_clear(cgtls, cgbp+16)

	X__gmpq_init(cgtls, cgbp+28)
	X__gmpq_set_ui(cgtls, cgbp+28, ppuint32(1), ppuint32(1))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(16)), 0, ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_add_q(cgtls, cgbp, cgbp, cgbp+28, ppint32(ecMPFR_RNDN))
	if aainex != 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(17)), 0) != 0 {

		Xprintf(cgtls, "Error in reduce_expo_range for 16 + 1/1, got inex = %d and\nx = \x00", iqlibc.ppVaList(cgbp+64, aainex))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	aainex = Xmpfr_sub_q(cgtls, cgbp, cgbp, cgbp+28, ppint32(ecMPFR_RNDN))
	if aainex != 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(16)), 0) != 0 {

		Xprintf(cgtls, "Error in reduce_expo_range for 17 - 1/1, got inex = %d and\nx = \x00", iqlibc.ppVaList(cgbp+64, aainex))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	X__gmpq_clear(cgtls, cgbp+28)

	Xmpfr_clear(cgtls, cgbp)

	Xset_emin(cgtls, aaemin)
}

func siaddsubq_overflow_aux(cgtls *iqlibc.ppTLS, aae tnmpfr_exp_t) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aaemax tnmpfr_exp_t
	var aaex_flags, aaflags ppuint32
	var aainex, aainf, aarnd, aasign, aasub, ccv5, ccv7, ccv8, ccv9 ppint32
	var ccv1, ccv10, ccv6 ppbool
	var ccv11, ccv13 tnmpq_ptr
	var ccv12, ccv14 tnmpq_srcptr
	var pp_ /* q at bp+32 */ tnmpq_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaex_flags, aaflags, aainex, aainf, aarnd, aasign, aasub, ccv1, ccv10, ccv11, ccv12, ccv13, ccv14, ccv5, ccv6, ccv7, ccv8, ccv9

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aae <= iqlibc.ppInt32FromInt32(0x7fffffff))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1182), "e <= 0x7fffffffL\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaemax = X__gmpfr_emax
	Xset_emax(cgtls, aae)
	Xmpfr_inits2(cgtls, ppint32(16), cgbp, iqlibc.ppVaList(cgbp+64, cgbp+16, iqlibc.ppUintptrFromInt32(0)))
	X__gmpq_init(cgtls, cgbp+32)

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	Xmpfr_nextbelow(cgtls, cgbp)
	X__gmpq_set_ui(cgtls, cgbp+32, ppuint32(1), ppuint32(1))

	aasign = 0
	for {
		if !(aasign <= ppint32(1)) {
			break
		}

		aasub = 0
		for {
			if !(aasub <= ppint32(1)) {
				break
			}

			aarnd = 0
			for {
				if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
					break
				}

				if ccv6 = aarnd == ppint32(ecMPFR_RNDA); !ccv6 {
					if aasign != 0 {
						ccv5 = ppint32(ecMPFR_RNDD)
					} else {
						ccv5 = ppint32(ecMPFR_RNDU)
					}
				}
				aainf = iqlibc.ppBoolInt32(ccv6 || aarnd == ccv5)
				if aainf != 0 {
					ccv7 = ppint32(mvMPFR_FLAGS_OVERFLOW)
				} else {
					ccv7 = 0
				}
				aaex_flags = iqlibc.ppUint32FromInt32(ppint32(mvMPFR_FLAGS_INEXACT) | ccv7)
				Xmpfr_clear_flags(cgtls)
				if aasub != 0 {
					ccv8 = Xmpfr_sub_q(cgtls, cgbp+16, cgbp, cgbp+32, aarnd)
				} else {
					ccv8 = Xmpfr_add_q(cgtls, cgbp+16, cgbp, cgbp+32, aarnd)
				}
				aainex = ccv8
				aaflags = X__gmpfr_flags

				if ccv10 = aainex == 0 || aaflags != aaex_flags; !ccv10 {
					if aainf != 0 {
						ccv9 = iqlibc.ppBoolInt32(!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))))
					} else {
						ccv9 = iqlibc.ppBoolInt32(!(Xmpfr_equal_p(cgtls, cgbp, cgbp+16) != 0))
					}
				}
				if ccv10 || ccv9 != 0 {

					Xprintf(cgtls, "Error in addsubq_overflow_aux(%ld), sign = %d, %s\n\x00", iqlibc.ppVaList(cgbp+64, aae, aasign, Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "Got inex = %d, y = \x00", iqlibc.ppVaList(cgbp+64, aainex))
					Xmpfr_dump(cgtls, cgbp+16)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}

				goto cg_4
			cg_4:
				;
				aarnd++
			}
			ccv11 = cgbp + 32
			ccv12 = cgbp + 32
			if ccv11 != ccv12 {
				X__gmpq_set(cgtls, ccv11, ccv12)
			}
			(*tn__mpq_struct)(iqunsafe.ppPointer(ccv11)).fd_mp_num.fd_mp_size = -(*tn__mpq_struct)(iqunsafe.ppPointer(ccv11)).fd_mp_num.fd_mp_size

			goto cg_3
		cg_3:
			;
			aasub++
		}
		Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
		ccv13 = cgbp + 32
		ccv14 = cgbp + 32
		if ccv13 != ccv14 {
			X__gmpq_set(cgtls, ccv13, ccv14)
		}
		(*tn__mpq_struct)(iqunsafe.ppPointer(ccv13)).fd_mp_num.fd_mp_size = -(*tn__mpq_struct)(iqunsafe.ppPointer(ccv13)).fd_mp_num.fd_mp_size

		goto cg_2
	cg_2:
		;
		aasign++
	}

	X__gmpq_clear(cgtls, cgbp+32)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+64, cgbp+16, iqlibc.ppUintptrFromInt32(0)))
	Xset_emax(cgtls, aaemax)
}

func siaddsubq_overflow(cgtls *iqlibc.ppTLS) {

	siaddsubq_overflow_aux(cgtls, ppint32(4913))
	siaddsubq_overflow_aux(cgtls, iqlibc.ppInt32FromInt32(1)<<(iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint32FromInt64(4)/iqlibc.ppUint32FromInt64(4)-iqlibc.ppUint32FromInt32(2))-iqlibc.ppInt32FromInt32(1))
}

func sicoverage_mpfr_mul_q_20110218(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aastatus ppint32
	var pp_ /* cmp at bp+0 */ tnmpfr_t
	var pp_ /* op1 at bp+32 */ tnmpfr_t
	var pp_ /* op2 at bp+48 */ tnmpq_t
	var pp_ /* res at bp+16 */ tnmpfr_t
	pp_ = aastatus

	Xmpfr_init2(cgtls, cgbp, ppint32(mvMPFR_PREC_MIN))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(mvMPFR_PREC_MIN))
	Xmpfr_init(cgtls, cgbp+32)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint32FromInt32(ppint32(iqlibc.ppInt32FromInt32(1))), 0, ppint32(ecMPFR_RNDN))

	X__gmpq_init(cgtls, cgbp+48)
	X__gmpq_set_si(cgtls, cgbp+48, 0, ppuint32(0))
	X__gmpz_set_si(cgtls, cgbp+48+12, 0)

	aastatus = Xmpfr_mul_q(cgtls, cgbp+16, cgbp+32, cgbp+48, ppint32(ecMPFR_RNDN))

	if aastatus != 0 || Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Results differ %d.\nres=\x00", iqlibc.ppVaList(cgbp+80, aastatus))
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "cmp=\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint32FromInt32(ppint32(iqlibc.ppInt32FromInt32(1))), 0, ppint32(ecMPFR_RNDN))
	X__gmpq_set_si(cgtls, cgbp+48, ppint32(-ppint32(1)), ppuint32(0))

	aastatus = Xmpfr_mul_q(cgtls, cgbp+16, cgbp+32, cgbp+48, ppint32(ecMPFR_RNDN))

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	if aastatus != 0 || Xmpfr_cmp3(cgtls, cgbp+16, cgbp, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_mul_q 1 * (-1/0) returned a wrong value:\n\x00", 0)
		Xprintf(cgtls, "  expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "  got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xprintf(cgtls, "  ternary value is %d\n\x00", iqlibc.ppVaList(cgbp+80, aastatus))
		Xexit(cgtls, ppint32(1))
	}

	X__gmpq_clear(cgtls, cgbp+48)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_clear(cgtls, cgbp)
}

func sicoverage(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aacmp ppint32
	var aaemax, aaemin tnmpfr_exp_t
	var ccv1, ccv2, ccv3, ccv4 ppbool
	var pp_ /* x at bp+12 */ tnmpfr_t
	var pp_ /* z at bp+0 */ tnmpz_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aacmp, aaemax, aaemin, ccv1, ccv2, ccv3, ccv4

	Xmpfr_mpz_init(cgtls, cgbp)
	Xmpfr_init2(cgtls, cgbp+12, ppint32(5))

	/* coverage for mpfr_cmp_z in case of overflow */
	aaemax = X__gmpfr_emax
	Xset_emax(cgtls, ppint32(63))
	X__gmpz_set_str(cgtls, cgbp, "9223372036854775808\x00", ppint32(10)) /* 2^63 */
	Xmpfr_set_ui_2exp(cgtls, cgbp+12, ppuint32(1), X__gmpfr_emax, ppint32(ecMPFR_RNDZ))
	/* x = (1-2^(-p))*2^emax */
	Xmpfr_clear_flags(cgtls)
	aacmp = Xmpfr_cmp_z(cgtls, cgbp+12, cgbp)

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aacmp < iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1310), "cmp < 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0)), ppint32(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1311), "!((int) (__gmpfr_flags & 2))\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emax(cgtls, aaemax)

	/* coverage for mpfr_cmp_z in case of underflow */
	X__gmpz_set_str(cgtls, cgbp, "18446744073709551615\x00", ppint32(10)) /* 2^64-1 */
	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint32(65)) /* xmin = 2^64 */
	Xmpfr_set_ui_2exp(cgtls, cgbp+12, ppuint32(1), ppint32(64), ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	aacmp = Xmpfr_cmp_z(cgtls, cgbp+12, cgbp)

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aacmp > iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1321), "cmp > 0\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint32(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tgmpop.c\x00", ppint32(1322), "!((int) (__gmpfr_flags & 1))\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emin(cgtls, aaemin)

	Xmpfr_clear(cgtls, cgbp+12)
	Xmpfr_mpz_clear(cgtls, cgbp)
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	Xtests_start_mpfr(cgtls)

	sicoverage(cgtls)
	sispecial(cgtls)

	sitest_specialz(cgtls, pp__ccgo_fp(Xmpfr_add_z), pp__ccgo_fp(X__gmpz_add), "add\x00")
	sitest_specialz(cgtls, pp__ccgo_fp(Xmpfr_sub_z), pp__ccgo_fp(X__gmpz_sub), "sub\x00")
	sitest_specialz(cgtls, pp__ccgo_fp(Xmpfr_mul_z), pp__ccgo_fp(X__gmpz_mul), "mul\x00")
	sitest_genericz(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_add_z), "add\x00")
	sitest_genericz(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_sub_z), "sub\x00")
	sitest_genericz(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_mul_z), "mul\x00")
	sitest_genericz(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_div_z), "div\x00")
	sitest_special2z(cgtls, pp__ccgo_fp(Xmpfr_z_sub), pp__ccgo_fp(X__gmpz_sub), "sub\x00")
	sitest_generic2z(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_z_sub), "sub\x00")

	sitest_genericq(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_add_q), "add\x00")
	sitest_genericq(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_sub_q), "sub\x00")
	sitest_genericq(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_mul_q), "mul\x00")
	sitest_genericq(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_div_q), "div\x00")
	sitest_specialq(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_mul_q), pp__ccgo_fp(X__gmpq_mul), "mul\x00")
	sitest_specialq(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_div_q), pp__ccgo_fp(X__gmpq_div), "div\x00")
	sitest_specialq(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_add_q), pp__ccgo_fp(X__gmpq_add), "add\x00")
	sitest_specialq(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppuint32(100), pp__ccgo_fp(Xmpfr_sub_q), pp__ccgo_fp(X__gmpq_sub), "sub\x00")

	sitest_cmp_z(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppint32(100))
	sitest_cmp_q(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppint32(100))
	sitest_cmp_f(cgtls, ppint32(mvMPFR_PREC_MIN), ppint32(100), ppint32(100))

	sicheck_for_zero(cgtls)

	sibug_mul_q_20100810(cgtls)
	sibug_div_q_20100810(cgtls)
	sibug_mul_div_q_20100818(cgtls)
	sireduced_expo_range(cgtls)
	siaddsubq_overflow(cgtls)

	sicoverage_mpfr_mul_q_20110218(cgtls)

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

func ___builtin_unreachable(*iqlibc.ppTLS)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_init2(*iqlibc.ppTLS, ppuintptr, ppuint32)

func ___gmpf_set_prec(*iqlibc.ppTLS, ppuintptr, ppuint32)

func ___gmpf_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint32)

func ___gmpf_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32)

var ___gmpfr_emax ppint32

var ___gmpfr_emin ppint32

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32, ppuintptr, ppint32) ppuint32

func ___gmpq_add(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpq_canonicalize(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_div(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpq_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpq_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpq_set(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmpq_set_si(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32)

func ___gmpq_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint32, ppuint32)

func ___gmpq_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_add(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32)

func ___gmpz_fac_ui(*iqlibc.ppTLS, ppuintptr, ppuint32)

func ___gmpz_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr) ppuint32

func ___gmpz_set_si(*iqlibc.ppTLS, ppuintptr, ppint32)

func ___gmpz_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func ___gmpz_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint32)

func ___gmpz_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func ___gmpz_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint32)

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _mpfr_add_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_add_z(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_can_round(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32, ppint32, ppint32) ppint32

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_erangeflag(*iqlibc.ppTLS)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp3(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_cmp_f(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_cmp_q(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_cmp_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_cmp_z(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_div_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_div_z(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_inits(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_inits2(*iqlibc.ppTLS, ppint32, ppuintptr, ppuintptr)

func _mpfr_mpz_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mpz_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_mul_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_mul_z(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppuint8

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_f(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint32, ppint32, ppint32) ppint32

func _mpfr_set_z(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_sgn(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_sub_q(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_sub_z(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_z_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _putchar(*iqlibc.ppTLS, ppint32) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint32

func _set_emax(*iqlibc.ppTLS, ppint32)

func _set_emin(*iqlibc.ppTLS, ppint32)

var _stdout ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

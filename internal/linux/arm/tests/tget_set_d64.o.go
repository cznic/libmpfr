// Code generated for linux/arm by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DMPFR_LONG_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/arm -UHAVE_NEARBYINT -c -o tget_set_d64.o.go tget_set_d64.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvLT_OBJDIR = ".libs/"
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_LONG_WITHIN_LIMB = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_WANT_PROVEN_CODE = 1
const mvNDEBUG = 1
const mvNO_ASM = 1
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_L = 1
const mvPRINTF_T = 1
const mvSRCDIR = "."
const mv_FILE_OFFSET_BITS = 64
const mv_GNU_SOURCE = 1
const mv_STDC_PREDEF_H = 1
const mv__ACCUM_EPSILON__ = "0x1P-15K"
const mv__ACCUM_FBIT__ = 15
const mv__ACCUM_IBIT__ = 16
const mv__ACCUM_MAX__ = "0X7FFFFFFFP-15K"
const mv__APCS_32__ = 1
const mv__ARMEL__ = 1
const mv__ARM_32BIT_STATE = 1
const mv__ARM_ARCH = 6
const mv__ARM_ARCH_6__ = 1
const mv__ARM_ARCH_ISA_ARM = 1
const mv__ARM_ARCH_ISA_THUMB = 1
const mv__ARM_EABI__ = 1
const mv__ARM_FEATURE_CLZ = 1
const mv__ARM_FEATURE_COPROC = 15
const mv__ARM_FEATURE_DSP = 1
const mv__ARM_FEATURE_LDREX = 4
const mv__ARM_FEATURE_QBIT = 1
const mv__ARM_FEATURE_SAT = 1
const mv__ARM_FEATURE_SIMD32 = 1
const mv__ARM_FEATURE_UNALIGNED = 1
const mv__ARM_FP = 12
const mv__ARM_PCS_VFP = 1
const mv__ARM_SIZEOF_MINIMAL_ENUM = 4
const mv__ARM_SIZEOF_WCHAR_T = 4
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 8
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DA_FBIT__ = 31
const mv__DA_IBIT__ = 32
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__DQ_FBIT__ = 63
const mv__DQ_IBIT__ = 0
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.2204460492503131e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.7976931348623157e+308
const mv__FLT32X_MIN__ = 2.2250738585072014e-308
const mv__FLT32X_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.1920928955078125e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.4028234663852886e+38
const mv__FLT32_MIN__ = 1.1754943508222875e-38
const mv__FLT32_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.2204460492503131e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.7976931348623157e+308
const mv__FLT64_MIN__ = 2.2250738585072014e-308
const mv__FLT64_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.1920928955078125e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.4028234663852886e+38
const mv__FLT_MIN__ = 1.1754943508222875e-38
const mv__FLT_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT_RADIX__ = 2
const mv__FRACT_EPSILON__ = "0x1P-15R"
const mv__FRACT_FBIT__ = 15
const mv__FRACT_IBIT__ = 0
const mv__FRACT_MAX__ = "0X7FFFP-15R"
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 1
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 1
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 1
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 1
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 1
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GXX_ABI_VERSION = 1017
const mv__GXX_TYPEINFO_EQUALITY_INLINE = 0
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__HA_FBIT__ = 7
const mv__HA_IBIT__ = 8
const mv__HQ_FBIT__ = 15
const mv__HQ_IBIT__ = 0
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LACCUM_EPSILON__ = "0x1P-31LK"
const mv__LACCUM_FBIT__ = 31
const mv__LACCUM_IBIT__ = 32
const mv__LACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LK"
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.2204460492503131e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.7976931348623157e+308
const mv__LDBL_MIN__ = 2.2250738585072014e-308
const mv__LDBL_NORM_MAX__ = 1.7976931348623157e+308
const mv__LFRACT_EPSILON__ = "0x1P-31LR"
const mv__LFRACT_FBIT__ = 31
const mv__LFRACT_IBIT__ = 0
const mv__LFRACT_MAX__ = "0X7FFFFFFFP-31LR"
const mv__LLACCUM_EPSILON__ = "0x1P-31LLK"
const mv__LLACCUM_FBIT__ = 31
const mv__LLACCUM_IBIT__ = 32
const mv__LLACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LLK"
const mv__LLFRACT_EPSILON__ = "0x1P-63LLR"
const mv__LLFRACT_FBIT__ = 63
const mv__LLFRACT_IBIT__ = 0
const mv__LLFRACT_MAX__ = "0X7FFFFFFFFFFFFFFFP-63LLR"
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__QQ_FBIT__ = 7
const mv__QQ_IBIT__ = 0
const mv__SACCUM_EPSILON__ = "0x1P-7HK"
const mv__SACCUM_FBIT__ = 7
const mv__SACCUM_IBIT__ = 8
const mv__SACCUM_MAX__ = "0X7FFFP-7HK"
const mv__SA_FBIT__ = 15
const mv__SA_IBIT__ = 16
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SFRACT_EPSILON__ = "0x1P-7HR"
const mv__SFRACT_FBIT__ = 7
const mv__SFRACT_IBIT__ = 0
const mv__SFRACT_MAX__ = "0X7FP-7HR"
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__SQ_FBIT__ = 31
const mv__SQ_IBIT__ = 0
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__TA_FBIT__ = 63
const mv__TA_IBIT__ = 64
const mv__THUMB_INTERWORK__ = 1
const mv__TQ_FBIT__ = 127
const mv__TQ_IBIT__ = 0
const mv__UACCUM_EPSILON__ = "0x1P-16UK"
const mv__UACCUM_FBIT__ = 16
const mv__UACCUM_IBIT__ = 16
const mv__UACCUM_MAX__ = "0XFFFFFFFFP-16UK"
const mv__UACCUM_MIN__ = "0.0UK"
const mv__UDA_FBIT__ = 32
const mv__UDA_IBIT__ = 32
const mv__UDQ_FBIT__ = 64
const mv__UDQ_IBIT__ = 0
const mv__UFRACT_EPSILON__ = "0x1P-16UR"
const mv__UFRACT_FBIT__ = 16
const mv__UFRACT_IBIT__ = 0
const mv__UFRACT_MAX__ = "0XFFFFP-16UR"
const mv__UFRACT_MIN__ = "0.0UR"
const mv__UHA_FBIT__ = 8
const mv__UHA_IBIT__ = 8
const mv__UHQ_FBIT__ = 16
const mv__UHQ_IBIT__ = 0
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__ULACCUM_EPSILON__ = "0x1P-32ULK"
const mv__ULACCUM_FBIT__ = 32
const mv__ULACCUM_IBIT__ = 32
const mv__ULACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULK"
const mv__ULACCUM_MIN__ = "0.0ULK"
const mv__ULFRACT_EPSILON__ = "0x1P-32ULR"
const mv__ULFRACT_FBIT__ = 32
const mv__ULFRACT_IBIT__ = 0
const mv__ULFRACT_MAX__ = "0XFFFFFFFFP-32ULR"
const mv__ULFRACT_MIN__ = "0.0ULR"
const mv__ULLACCUM_EPSILON__ = "0x1P-32ULLK"
const mv__ULLACCUM_FBIT__ = 32
const mv__ULLACCUM_IBIT__ = 32
const mv__ULLACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULLK"
const mv__ULLACCUM_MIN__ = "0.0ULLK"
const mv__ULLFRACT_EPSILON__ = "0x1P-64ULLR"
const mv__ULLFRACT_FBIT__ = 64
const mv__ULLFRACT_IBIT__ = 0
const mv__ULLFRACT_MAX__ = "0XFFFFFFFFFFFFFFFFP-64ULLR"
const mv__ULLFRACT_MIN__ = "0.0ULLR"
const mv__UQQ_FBIT__ = 8
const mv__UQQ_IBIT__ = 0
const mv__USACCUM_EPSILON__ = "0x1P-8UHK"
const mv__USACCUM_FBIT__ = 8
const mv__USACCUM_IBIT__ = 8
const mv__USACCUM_MAX__ = "0XFFFFP-8UHK"
const mv__USACCUM_MIN__ = "0.0UHK"
const mv__USA_FBIT__ = 16
const mv__USA_IBIT__ = 16
const mv__USFRACT_EPSILON__ = "0x1P-8UHR"
const mv__USFRACT_FBIT__ = 8
const mv__USFRACT_IBIT__ = 0
const mv__USFRACT_MAX__ = "0XFFP-8UHR"
const mv__USFRACT_MIN__ = "0.0UHR"
const mv__USQ_FBIT__ = 32
const mv__USQ_IBIT__ = 0
const mv__UTA_FBIT__ = 64
const mv__UTA_IBIT__ = 64
const mv__UTQ_FBIT__ = 128
const mv__UTQ_IBIT__ = 0
const mv__VERSION__ = "12.2.0"
const mv__VFP_FP__ = 1
const mv__WCHAR_MAX__ = 0xffffffff
const mv__WCHAR_MIN__ = 0
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__arm__ = 1
const mv__gnu_linux__ = 1
const mv__linux = 1
const mv__linux__ = 1
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mvlinux = 1
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppuint32

type tn__predefined_ptrdiff_t = ppint32

/* Test file for mpfr_get_decimal64 and mpfr_set_decimal64.

Copyright 2006-2023 Free Software Foundation, Inc.
Contributed by the AriC and Caramba projects, INRIA.

This file is part of the GNU MPFR Library.

The GNU MPFR Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The GNU MPFR Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

/* Needed due to the test on MPFR_WANT_DECIMAL_FLOATS */

func Xmain(cgtls *iqlibc.ppTLS, cgargc ppint32, cgargv ppuintptr) (cgr ppint32) {

	return ppint32(77)
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

// Code generated for linux/arm by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DMPFR_LONG_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/arm -UHAVE_NEARBYINT -c -o tset_str.o.go tset_str.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 32
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 32
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 2972
const mvMPFR_AI_THRESHOLD3 = 44718
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 277
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 10666
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 5
const mvMPFR_LOG2_PREC_BITS = 5
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 22
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 32
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 28990
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 24
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/arm/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvN = 30000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNR = 50
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_L = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_FILE_OFFSET_BITS = 64
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_REDIR_TIME64 = 1
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ACCUM_EPSILON__ = "0x1P-15K"
const mv__ACCUM_FBIT__ = 15
const mv__ACCUM_IBIT__ = 16
const mv__ACCUM_MAX__ = "0X7FFFFFFFP-15K"
const mv__APCS_32__ = 1
const mv__ARMEL__ = 1
const mv__ARM_32BIT_STATE = 1
const mv__ARM_ARCH = 6
const mv__ARM_ARCH_6__ = 1
const mv__ARM_ARCH_ISA_ARM = 1
const mv__ARM_ARCH_ISA_THUMB = 1
const mv__ARM_EABI__ = 1
const mv__ARM_FEATURE_CLZ = 1
const mv__ARM_FEATURE_COPROC = 15
const mv__ARM_FEATURE_DSP = 1
const mv__ARM_FEATURE_LDREX = 4
const mv__ARM_FEATURE_QBIT = 1
const mv__ARM_FEATURE_SAT = 1
const mv__ARM_FEATURE_SIMD32 = 1
const mv__ARM_FEATURE_UNALIGNED = 1
const mv__ARM_FP = 12
const mv__ARM_PCS_VFP = 1
const mv__ARM_SIZEOF_MINIMAL_ENUM = 4
const mv__ARM_SIZEOF_WCHAR_T = 4
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 8
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DA_FBIT__ = 31
const mv__DA_IBIT__ = 32
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__DQ_FBIT__ = 63
const mv__DQ_IBIT__ = 0
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.2204460492503131e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.7976931348623157e+308
const mv__FLT32X_MIN__ = 2.2250738585072014e-308
const mv__FLT32X_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.1920928955078125e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.4028234663852886e+38
const mv__FLT32_MIN__ = 1.1754943508222875e-38
const mv__FLT32_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.9406564584124654e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.2204460492503131e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.7976931348623157e+308
const mv__FLT64_MIN__ = 2.2250738585072014e-308
const mv__FLT64_NORM_MAX__ = 1.7976931348623157e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.4012984643248171e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.1920928955078125e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.4028234663852886e+38
const mv__FLT_MIN__ = 1.1754943508222875e-38
const mv__FLT_NORM_MAX__ = 3.4028234663852886e+38
const mv__FLT_RADIX__ = 2
const mv__FRACT_EPSILON__ = "0x1P-15R"
const mv__FRACT_FBIT__ = 15
const mv__FRACT_IBIT__ = 0
const mv__FRACT_MAX__ = "0X7FFFP-15R"
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 1
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 1
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 1
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 1
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 1
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 64
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "arm-linux-gnueabihf-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__GXX_TYPEINFO_EQUALITY_INLINE = 0
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__HA_FBIT__ = 7
const mv__HA_IBIT__ = 8
const mv__HQ_FBIT__ = 15
const mv__HQ_IBIT__ = 0
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffff
const mv__INTPTR_TYPE__ = "int"
const mv__INTPTR_WIDTH__ = 32
const mv__INT_FAST16_MAX__ = 0x7fffffff
const mv__INT_FAST16_TYPE__ = "int"
const mv__INT_FAST16_WIDTH__ = 32
const mv__INT_FAST32_MAX__ = 0x7fffffff
const mv__INT_FAST32_TYPE__ = "int"
const mv__INT_FAST32_WIDTH__ = 32
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LACCUM_EPSILON__ = "0x1P-31LK"
const mv__LACCUM_FBIT__ = 31
const mv__LACCUM_IBIT__ = 32
const mv__LACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LK"
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.2204460492503131e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.7976931348623157e+308
const mv__LDBL_MIN__ = 2.2250738585072014e-308
const mv__LDBL_NORM_MAX__ = 1.7976931348623157e+308
const mv__LFRACT_EPSILON__ = "0x1P-31LR"
const mv__LFRACT_FBIT__ = 31
const mv__LFRACT_IBIT__ = 0
const mv__LFRACT_MAX__ = "0X7FFFFFFFP-31LR"
const mv__LITTLE_ENDIAN = 1234
const mv__LLACCUM_EPSILON__ = "0x1P-31LLK"
const mv__LLACCUM_FBIT__ = 31
const mv__LLACCUM_IBIT__ = 32
const mv__LLACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LLK"
const mv__LLFRACT_EPSILON__ = "0x1P-63LLR"
const mv__LLFRACT_FBIT__ = 63
const mv__LLFRACT_IBIT__ = 0
const mv__LLFRACT_MAX__ = "0X7FFFFFFFFFFFFFFFP-63LLR"
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 2147483647
const mv__LONG_MAX__ = 0x7fffffff
const mv__LONG_WIDTH__ = 32
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffff
const mv__PTRDIFF_TYPE__ = "int"
const mv__PTRDIFF_WIDTH__ = 32
const mv__QQ_FBIT__ = 7
const mv__QQ_IBIT__ = 0
const mv__SACCUM_EPSILON__ = "0x1P-7HK"
const mv__SACCUM_FBIT__ = 7
const mv__SACCUM_IBIT__ = 8
const mv__SACCUM_MAX__ = "0X7FFFP-7HK"
const mv__SA_FBIT__ = 15
const mv__SA_IBIT__ = 16
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SFRACT_EPSILON__ = "0x1P-7HR"
const mv__SFRACT_FBIT__ = 7
const mv__SFRACT_IBIT__ = 0
const mv__SFRACT_MAX__ = "0X7FP-7HR"
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 4
const mv__SIZEOF_POINTER__ = 4
const mv__SIZEOF_PTRDIFF_T__ = 4
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 4
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffff
const mv__SIZE_WIDTH__ = 32
const mv__SQ_FBIT__ = 31
const mv__SQ_IBIT__ = 0
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__TA_FBIT__ = 63
const mv__TA_IBIT__ = 64
const mv__THUMB_INTERWORK__ = 1
const mv__TQ_FBIT__ = 127
const mv__TQ_IBIT__ = 0
const mv__UACCUM_EPSILON__ = "0x1P-16UK"
const mv__UACCUM_FBIT__ = 16
const mv__UACCUM_IBIT__ = 16
const mv__UACCUM_MAX__ = "0XFFFFFFFFP-16UK"
const mv__UACCUM_MIN__ = "0.0UK"
const mv__UDA_FBIT__ = 32
const mv__UDA_IBIT__ = 32
const mv__UDQ_FBIT__ = 64
const mv__UDQ_IBIT__ = 0
const mv__UFRACT_EPSILON__ = "0x1P-16UR"
const mv__UFRACT_FBIT__ = 16
const mv__UFRACT_IBIT__ = 0
const mv__UFRACT_MAX__ = "0XFFFFP-16UR"
const mv__UFRACT_MIN__ = "0.0UR"
const mv__UHA_FBIT__ = 8
const mv__UHA_IBIT__ = 8
const mv__UHQ_FBIT__ = 16
const mv__UHQ_IBIT__ = 0
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = "0xffffffffffffffffU"
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = "0xffffffffffffffffU"
const mv__UINTPTR_MAX__ = 0xffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffff
const mv__UINT_FAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = "0xffffffffffffffffU"
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__ULACCUM_EPSILON__ = "0x1P-32ULK"
const mv__ULACCUM_FBIT__ = 32
const mv__ULACCUM_IBIT__ = 32
const mv__ULACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULK"
const mv__ULACCUM_MIN__ = "0.0ULK"
const mv__ULFRACT_EPSILON__ = "0x1P-32ULR"
const mv__ULFRACT_FBIT__ = 32
const mv__ULFRACT_IBIT__ = 0
const mv__ULFRACT_MAX__ = "0XFFFFFFFFP-32ULR"
const mv__ULFRACT_MIN__ = "0.0ULR"
const mv__ULLACCUM_EPSILON__ = "0x1P-32ULLK"
const mv__ULLACCUM_FBIT__ = 32
const mv__ULLACCUM_IBIT__ = 32
const mv__ULLACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULLK"
const mv__ULLACCUM_MIN__ = "0.0ULLK"
const mv__ULLFRACT_EPSILON__ = "0x1P-64ULLR"
const mv__ULLFRACT_FBIT__ = 64
const mv__ULLFRACT_IBIT__ = 0
const mv__ULLFRACT_MAX__ = "0XFFFFFFFFFFFFFFFFP-64ULLR"
const mv__ULLFRACT_MIN__ = "0.0ULLR"
const mv__UQQ_FBIT__ = 8
const mv__UQQ_IBIT__ = 0
const mv__USACCUM_EPSILON__ = "0x1P-8UHK"
const mv__USACCUM_FBIT__ = 8
const mv__USACCUM_IBIT__ = 8
const mv__USACCUM_MAX__ = "0XFFFFP-8UHK"
const mv__USACCUM_MIN__ = "0.0UHK"
const mv__USA_FBIT__ = 16
const mv__USA_IBIT__ = 16
const mv__USE_TIME_BITS64 = 1
const mv__USFRACT_EPSILON__ = "0x1P-8UHR"
const mv__USFRACT_FBIT__ = 8
const mv__USFRACT_IBIT__ = 0
const mv__USFRACT_MAX__ = "0XFFP-8UHR"
const mv__USFRACT_MIN__ = "0.0UHR"
const mv__USQ_FBIT__ = 32
const mv__USQ_IBIT__ = 0
const mv__UTA_FBIT__ = 64
const mv__UTA_IBIT__ = 64
const mv__UTQ_FBIT__ = 128
const mv__UTQ_IBIT__ = 0
const mv__VERSION__ = "12.2.0"
const mv__VFP_FP__ = 1
const mv__WCHAR_MAX__ = 0xffffffff
const mv__WCHAR_MIN__ = 0
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__arm__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint32

type tn__predefined_wchar_t = ppuint32

type tn__predefined_ptrdiff_t = ppint32

type tnsize_t = ppuint32

type tnssize_t = ppint32

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__ccgo_align [0]ppuint32
	fd__lldata     [0]ppint64
	fd__align      [0]ppfloat64
	fd__opaque     [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppuint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnlldiv_t = struct {
	fd__ccgo_align [0]ppuint32
	fdquot         ppint64
	fdrem          ppint64
}

type tnmax_align_t = struct {
	fd__ccgo_align [0]ppuint32
	fd__ll         ppint64
	fd__ld         ppfloat64
}

type tnptrdiff_t = ppint32

type tnmp_limb_t = ppuint32

type tnmp_limb_signed_t = ppint32

type tnmp_bitcnt_t = ppuint32

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint32

type tnmp_exp_t = ppint32

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint32

type tnmpfr_ulong = ppuint32

type tnmpfr_size_t = ppuint32

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint32

type tnmpfr_uprec_t = ppuint32

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint32

type tnmpfr_uexp_t = ppuint32

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint32

type tnUHWtype = ppuint32

type tsbases = struct {
	fd__ccgo_align          [0]ppuint32
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fd__ccgo_align [0]ppuint32
	fdd            [0]ppfloat64
	fds            struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint32

type tnmpfr_ueexp_t = ppuint32

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

func sicheck_underflow(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aaemax, aaemin tnmpfr_exp_t
	var aares ppint32
	var ccv1, ccv2, ccv3 ppbool
	var pp_ /* a at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaemin, aares, ccv1, ccv2, ccv3

	Xmpfr_init(cgtls, cgbp)

	/* Check underflow */
	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint32(-ppint32(20)))
	aares = Xmpfr_set_str(cgtls, cgbp, "0.00000000001\x00", ppint32(10), ppint32(ecMPFR_RNDZ))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1)) {

		Xprintf(cgtls, "ERROR for mpfr_set_str (a, \"0.00000000001\", 10, MPFR_RNDN)\n with emin=-20\nres=%d\n\x00", iqlibc.ppVaList(cgbp+24, aares))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xset_emin(cgtls, aaemin)

	/* check overflow */
	aaemax = X__gmpfr_emax
	Xset_emax(cgtls, ppint32(1073741821)) /* 2^30-3 */
	Xmpfr_set_str(cgtls, cgbp, "2E1000000000\x00", ppint32(10), ppint32(ecMPFR_RNDN))

	if ccv3 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv3 {
		if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv1 {
			Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(68), "! (((a)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(68), "! (((a)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv3 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "ERROR for mpfr_set_str (a, \"2E1000000000\", 10, MPFR_RNDN);\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xset_emax(cgtls, aaemax)

	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* Bug found by Christoph Lauter. */
func sibug20081028(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aas ppuintptr
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_ = aas
	aas = "0.10000000000000000000000000000001E1\x00"

	Xmpfr_init2(cgtls, cgbp, ppint32(32))
	Xmpfr_set_str(cgtls, cgbp, "1.00000000000000000006\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	if !(Xmpfr_greater_p(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&X__gmpfr_one))) != 0) {

		Xprintf(cgtls, "Error in bug20081028:\nExpected %s\nGot      \x00", iqlibc.ppVaList(cgbp+24, aas))
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear(cgtls, cgbp)
}

func sibug20180908(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var ccv2 ppbool
	var pp_ /* s at bp+35 */ [5]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	pp_ = ccv2
	*(*[5]ppuint8)(iqunsafe.ppPointer(cgbp + 35)) = [5]ppuint8{'s', 's', 'q', '4'}

	Xmpfr_init2(cgtls, cgbp, ppint32(12))
	Xmpfr_init2(cgtls, cgbp+16, ppint32(12))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.100010111010E24\x00")
	/* x = 9150464 = [4, 52, 54, 54] in base 55 */
	Xmpfr_set_str(cgtls, cgbp+16, cgbp+35, ppint32(55), ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_equal_p(cgtls, cgbp, cgbp+16) != 0)), ppint32(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(107), "mpfr_equal_p (x, y)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp, cgbp+16) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aabase, aabaseprec, aabd, aacbase, aaclimb, aacrnd, aai, aak, aalogbase, aalz, aanc, aaobase, aaprec, aaret, ccv1, ccv11, ccv2, ccv3, ccv4 ppint32
	var aabase1 [3]ppint32
	var aacheck_limb [2]tnmp_limb_t
	var aanb_digit tnsize_t
	var aarnd tnmpfr_rnd_t
	var aarnd1 [3]tnmpfr_rnd_t
	var aastr, aastr1, aastr11, aastr2, ccv10, ccv7, ccv8, cgp14 ppuintptr
	var ccv21, ccv22, ccv23, ccv24, ccv25, ccv26, ccv27, ccv29, ccv30, ccv31, ccv32, ccv33, ccv35, ccv36, ccv37, ccv38, ccv39, ccv40, ccv41, ccv42, ccv44, ccv45, ccv46, ccv47, ccv48, ccv49, ccv50, ccv51, ccv53, ccv54, ccv55, ccv56, ccv57, ccv59, ccv61 ppbool
	var pp_ /* e at bp+32 */ tnmpfr_exp_t
	var pp_ /* exp at bp+36 */ tnmpfr_exp_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+16 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aabase, aabase1, aabaseprec, aabd, aacbase, aacheck_limb, aaclimb, aacrnd, aai, aak, aalogbase, aalz, aanb_digit, aanc, aaobase, aaprec, aaret, aarnd, aarnd1, aastr, aastr1, aastr11, aastr2, ccv1, ccv10, ccv11, ccv2, ccv21, ccv22, ccv23, ccv24, ccv25, ccv26, ccv27, ccv29, ccv3, ccv30, ccv31, ccv32, ccv33, ccv35, ccv36, ccv37, ccv38, ccv39, ccv4, ccv40, ccv41, ccv42, ccv44, ccv45, ccv46, ccv47, ccv48, ccv49, ccv50, ccv51, ccv53, ccv54, ccv55, ccv56, ccv57, ccv59, ccv61, ccv7, ccv8, cgp14

	Xtests_start_mpfr(cgtls)

	if aaargc >= ppint32(2) { /* tset_str <string> [<prec>] [<ibase>] [<obase>] */

		if aaargc >= ppint32(3) {
			ccv1 = Xatoi(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 2*4)))
		} else {
			ccv1 = ppint32(53)
		}
		aaprec = ccv1
		if aaargc >= ppint32(4) {
			ccv2 = Xatoi(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 3*4)))
		} else {
			ccv2 = ppint32(2)
		}
		aabase = ccv2
		if aaargc >= ppint32(5) {
			ccv3 = Xatoi(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 4*4)))
		} else {
			ccv3 = ppint32(10)
		}
		aaobase = ccv3
		Xmpfr_init2(cgtls, cgbp, ppint32(aaprec))
		Xmpfr_set_str(cgtls, cgbp, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*4)), aabase, ppint32(ecMPFR_RNDN))
		X__gmpfr_out_str(cgtls, Xstdout, aaobase, ppuint32(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputs(cgtls, "\x00")
		Xmpfr_clear(cgtls, cgbp)
		return 0
	}

	if aaargc > ppint32(1) {
		ccv4 = Xatoi(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*4)))
	} else {
		ccv4 = ppint32(53)
	}
	aanc = ppint32(ccv4)
	if aanc < ppint32(100) {
		aanc = ppint32(100)
	}

	aastr = Xtests_allocate(cgtls, iqlibc.ppUint32FromInt32(aanc))

	Xmpfr_init2(cgtls, cgbp, aanc+ppint32(10))

	aai = 0
	for {
		if !(aai < ppint32(mvNR)) {
			break
		}

		aastr2 = aastr

		aabd = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) & ppuint32(8)) /* 0 or 8 */
		aalz = -aabd

		if aabd != 0 {

			aak = ppint32(1)
			for {
				if !(aak <= aabd) {
					break
				}

				*(*ppuint8)(iqunsafe.ppPointer(aastr2)) = iqlibc.ppUint8FromInt32(ppint32('0') + iqlibc.ppBoolInt32(Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0)))
				if aalz == -aabd && iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aastr2))) != ppint32('0') {
					aalz = aak - aabd
				} /* position of the first 1 */

				goto cg_6
			cg_6:
				;
				aak++
				aastr2++
			}
		} else {
			ccv7 = aastr2
			aastr2++
			*(*ppuint8)(iqunsafe.ppPointer(ccv7)) = ppuint8('0')
		}

		ccv8 = aastr2
		aastr2++
		*(*ppuint8)(iqunsafe.ppPointer(ccv8)) = ppuint8('.')

		aak = ppint32(1)
		for {
			if !(aak < aanc-ppint32(17)-aabd) {
				break
			}

			*(*ppuint8)(iqunsafe.ppPointer(aastr2)) = iqlibc.ppUint8FromInt32(ppint32('0') + iqlibc.ppBoolInt32(Xrandlimb(cgtls)&ppuint32(1) != ppuint32(0)))
			if aalz == -aabd && iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aastr2))) != ppint32('0') {
				aalz = aak
			} /* position of the first 1 */

			goto cg_9
		cg_9:
			;
			aak++
			aastr2++
		}

		ccv10 = aastr2
		aastr2++
		*(*ppuint8)(iqunsafe.ppPointer(ccv10)) = ppuint8('e')

		/* Half cases have an exponent around zero, the other half cases
		   have the minimum exponent for which the value is representable
		   (not a subnormal). */
		if aai < ppint32(iqlibc.ppInt32FromInt32(mvNR)/iqlibc.ppInt32FromInt32(2)) {
			ccv11 = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls)&iqlibc.ppUint32FromInt32(0xff)) - ppint32(0x80)
		} else {
			ccv11 = X__gmpfr_emin + aalz - ppint32(1)
		}
		Xsprintf(cgtls, aastr2, "%ld\x00", iqlibc.ppVaList(cgbp+48, ccv11))

		/* if (i >= NR/2) printf ("lz = %ld, str: %s\n", lz, str); */
		Xmpfr_set_str_binary(cgtls, cgbp, aastr)

		goto cg_5
	cg_5:
		;
		aai++
	}

	Xtests_free(cgtls, aastr, iqlibc.ppUint32FromInt32(aanc))

	Xmpfr_set_prec(cgtls, cgbp, ppint32(54))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.100100100110110101001010010101111000001011100100101010E-529\x00")
	Xmpfr_init2(cgtls, cgbp+16, ppint32(54))
	Xmpfr_set_str(cgtls, cgbp+16, "4.936a52bc17254@-133\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (1a):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str_binary(cgtls, cgbp, "0.111111101101110010111010100110000111011001010100001101E-529\x00")
	Xmpfr_set_str(cgtls, cgbp+16, "0.fedcba98765434P-529\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (1b):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(53))
	Xmpfr_set_str_binary(cgtls, cgbp, "+110101100.01010000101101000000100111001000101011101110E00\x00")

	Xmpfr_set_str_binary(cgtls, cgbp, "1.0\x00")
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str_binary for s=1.0\n\x00", 0)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str_binary(cgtls, cgbp, "+0000\x00")
	Xmpfr_set_str_binary(cgtls, cgbp, "+0000E0\x00")
	Xmpfr_set_str_binary(cgtls, cgbp, "0000E0\x00")
	if Xmpfr_sgn(cgtls, cgbp) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str_binary for s=0.0\n\x00", 0)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp, "+243495834958.53452345E1\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp, "9007199254740993\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp, "9007199254740992\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str(cgtls, cgbp, "9007199254740992\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str(cgtls, cgbp, "9007199254740992\x00", ppint32(10), ppint32(ecMPFR_RNDZ))

	/* check a random number printed and read is not modified */
	aaprec = ppint32(53)
	Xmpfr_set_prec(cgtls, cgbp, ppint32(aaprec))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(aaprec))
	aai = 0
	for {
		if !(aai < ppint32(mvN)) {
			break
		}

		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		aarnd = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls) % iqlibc.ppUint32FromInt32(ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)))
		aalogbase = iqlibc.ppInt32FromUint32(Xrandlimb(cgtls)%ppuint32(5) + ppuint32(1))
		aabase = ppint32(1) << aalogbase
		/* Warning: the number of bits needed to print exactly a number of
		   'prec' bits in base 2^logbase may be greater than ceil(prec/logbase),
		   for example 0.11E-1 in base 2 cannot be written exactly with only
		   one digit in base 4 */
		if aabase == ppint32(2) {
			aabaseprec = aaprec
		} else {
			aabaseprec = ppint32(1) + (aaprec-ppint32(2)+aalogbase)/aalogbase
		}
		aastr = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, aabase, iqlibc.ppUint32FromInt32(aabaseprec), cgbp, aarnd)
		Xmpfr_set_str(cgtls, cgbp+16, aastr, aabase, aarnd)
		if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+16)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1)) {

			cgp14 = cgbp + 16 + 8
			*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgp14)) = tnmpfr_exp_t(ppuint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgp14))) + iqlibc.ppUint32FromInt32(aalogbase)*(iqlibc.ppUint32FromInt32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))-ppuint32(Xstrlen(cgtls, aastr))))
		}
		if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

			Xprintf(cgtls, "mpfr_set_str o mpfr_get_str <> id for rnd_mode=%s\n\x00", iqlibc.ppVaList(cgbp+48, Xmpfr_print_rnd_mode(cgtls, aarnd)))
			Xprintf(cgtls, "x=\x00", 0)
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "s=%s, exp=%d, base=%d\n\x00", iqlibc.ppVaList(cgbp+48, aastr, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32))), aabase))
			Xprintf(cgtls, "y=\x00", 0)
			Xmpfr_dump(cgtls, cgbp+16)
			Xmpfr_clear(cgtls, cgbp)
			Xmpfr_clear(cgtls, cgbp+16)
			Xexit(cgtls, ppint32(1))
		}
		Xmpfr_free_str(cgtls, aastr)

		goto cg_12
	cg_12:
		;
		aai++
	}

	aai = ppint32(2)
	for {
		if !(aai <= ppint32(62)) {
			break
		}

		if Xmpfr_set_str(cgtls, cgbp, "@NaN@(garbage)\x00", ppint32(aai), ppint32(ecMPFR_RNDN)) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

			Xprintf(cgtls, "mpfr_set_str failed on @NaN@(garbage)\n\x00", 0)
			Xexit(cgtls, ppint32(1))
		}

		/*
		   if (mpfr_set_str (x, "@Inf@garbage", i, MPFR_RNDN) != 0 ||
		       !mpfr_inf_p(x) || MPFR_IS_NEG (x))
		     {
		       printf ("mpfr_set_str failed on @Inf@garbage\n");
		       exit (1);
		     }

		   if (mpfr_set_str (x, "-@Inf@garbage", i, MPFR_RNDN) != 0 ||
		       !mpfr_inf_p(x) || MPFR_IS_POS (x))
		     {
		       printf ("mpfr_set_str failed on -@Inf@garbage\n");
		       exit (1);
		     }

		   if (mpfr_set_str (x, "+@Inf@garbage", i, MPFR_RNDN) != 0 ||
		       !mpfr_inf_p(x) || MPFR_IS_NEG (x))
		     {
		       printf ("mpfr_set_str failed on +@Inf@garbage\n");
		       exit (1);
		     }
		*/

		if aai > ppint32(16) {
			goto cg_15
		}

		if Xmpfr_set_str(cgtls, cgbp, "NaN\x00", ppint32(aai), ppint32(ecMPFR_RNDN)) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

			Xprintf(cgtls, "mpfr_set_str failed on NaN\n\x00", 0)
			Xexit(cgtls, ppint32(1))
		}

		if Xmpfr_set_str(cgtls, cgbp, "Inf\x00", ppint32(aai), ppint32(ecMPFR_RNDN)) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 {

			Xprintf(cgtls, "mpfr_set_str failed on Inf\n\x00", 0)
			Xexit(cgtls, ppint32(1))
		}

		if Xmpfr_set_str(cgtls, cgbp, "-Inf\x00", ppint32(aai), ppint32(ecMPFR_RNDN)) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 {

			Xprintf(cgtls, "mpfr_set_str failed on -Inf\n\x00", 0)
			Xexit(cgtls, ppint32(1))
		}

		if Xmpfr_set_str(cgtls, cgbp, "+Inf\x00", ppint32(aai), ppint32(ecMPFR_RNDN)) != 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 {

			Xprintf(cgtls, "mpfr_set_str failed on +Inf\n\x00", 0)
			Xexit(cgtls, ppint32(1))
		}

		goto cg_15
	cg_15:
		;
		aai++
	}

	/* check that mpfr_set_str works for uppercase letters too */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(10))
	Xmpfr_set_str(cgtls, cgbp, "B\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(11)), 0) != 0 {

		Xprintf(cgtls, "mpfr_set_str does not work for uppercase letters\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* start of tests added by Alain Delplanque */

	/* in this example an overflow can occur */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(64))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(64))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.0E-532\x00")
	Xmpfr_set_str(cgtls, cgbp+16, "0.71128279983522479470@-160\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (2):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	/* in this example, I think there was a pb in the old function :
	   result of mpfr_set_str_old for the same number, but with more
	   precision is: 1.111111111110000000000000000111111111111111111111111110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100111000100001100000010101100111010e184
	   this result is the same as mpfr_set_str */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(64))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(64))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.111111111110000000000000000111111111111111111111111110000000001E184\x00")
	Xmpfr_set_str(cgtls, cgbp+16, "0.jo08hg31hc5mmpj5mjjmgn55p2h35g@39\x00", ppint32(27), ppint32(ecMPFR_RNDU))
	/* y = 49027884868983130654865109690613178467841148597221480052 */
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (3):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	/* not exact rounding in mpfr_set_str
	   same number with more precision is : 1.111111111111111111111111111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011011111101000001101110110010101101000010100110011101110010001110e195
	   this result is the same as mpfr_set_str */
	/* problem was : can_round was call with MPFR_RNDN round mode,
	   so can_round use an error : 1/2 * 2^err * ulp(y)
	   instead of 2^err * ulp(y)
	   I have increase err by 1 */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(64)) /* it was round down instead of up */
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(64))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.111111111111111111111111111000000000000000000000000000000000001e195\x00")
	Xmpfr_set_str(cgtls, cgbp+16, "0.6e23ekb6acgh96abk10b6c9f2ka16i@45\x00", ppint32(21), ppint32(ecMPFR_RNDU))
	/* y = 100433627392042473064661483711179345482301462325708736552078 */
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (4):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	/* may be an error in mpfr_set_str_old
	   with more precision : 1.111111100000001111110000000000011111011111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111110111101010001110111011000010111001011100110110e180 */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(64)) /* it was round down instead of up */
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(64))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.111111100000001111110000000000011111011111111111111111111111111e180\x00")
	Xmpfr_set_str(cgtls, cgbp+16, "0.10j8j2k82ehahha56390df0a1de030@41\x00", ppint32(23), ppint32(ecMPFR_RNDZ))
	/* y = 3053110535624388280648330929253842828159081875986159414 */
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (5):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(64))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(64))
	Xmpfr_set_str(cgtls, cgbp+16, "0.jrchfhpp9en7hidqm9bmcofid9q3jg@39\x00", ppint32(28), ppint32(ecMPFR_RNDU))
	/* y = 196159429139499688661464718784226062699788036696626429952 */
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1111111111111111111111111111111000000000000011100000001111100001E187\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (6):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(64))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(64))
	Xmpfr_set_str(cgtls, cgbp+16, "0.h148m5ld5cf8gk1kd70b6ege92g6ba@47\x00", ppint32(24), ppint32(ecMPFR_RNDZ))
	/* y = 52652933527468502324759448399183654588831274530295083078827114496 */
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1111111111111100000000001000000000000000000011111111111111101111E215\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (7):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	/* worst cases for rounding to nearest in double precision */
	Xmpfr_set_prec(cgtls, cgbp, ppint32(53))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(53))

	Xmpfr_set_str(cgtls, cgbp+16, "5e125\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10111101000101110110011000100000101001010000000111111E418\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (8):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "69e267\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10000101101111100101101100000110010011001010011011010E894\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (9):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "623e100\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10110010000001010011000101111001110101000001111011111E342\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (10):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "3571e263\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10110001001100100010011000110000111010100000110101010E886\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (11):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "75569e-254\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10101101001000110001011011001000111000110101010110011E-827\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (12):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "920657e-23\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10101001110101001100110000101110110111101111001101100E-56\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (13):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "9210917e80\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11101101000100011001000110100011111100110000000110010E289\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (14):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "87575437e-309\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11110000001110011001000000110000000100000010101101100E-1000\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (15):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "245540327e122\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10001101101100010001100011110000110001100010111001011E434\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (16):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "491080654e122\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10001101101100010001100011110000110001100010111001011E435\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (17):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "83356057653e193\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10101010001001110011011011010111011100010101000011000E678\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_set_str (18):\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "83356057653e193\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10101010001001110011011011010111011100010101000011000E678\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(18)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "619534293513e124\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10001000011000010000000110000001111111110000011110001e452\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(19)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "3142213164987e-294\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11101001101000000100111011111101111001010001001101111e-935\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(20)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "36167929443327e-159\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11100111001110111110000101011001100110010100011111100e-483\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(21)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "904198236083175e-161\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11100111001110111110000101011001100110010100011111100e-485\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(22)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "3743626360493413e-165\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11000100000100011101001010111101011011011111011111001e-496\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(23)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "94080055902682397e-242\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10110010010011000000111100011100111100110011011001010e-747\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(24)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "7e-303\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10011001100111001000100110001110001000110111110001011e-1003\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(25)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "7e-303\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10011001100111001000100110001110001000110111110001100e-1003\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(26)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "93e-234\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10010011110110010111001001111001000010000000001110101E-770\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(27)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "93e-234\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10010011110110010111001001111001000010000000001110110E-770\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(28)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "755e174\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10111110110010011000110010011111101111000111111000101E588\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(29)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "755e174\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10111110110010011000110010011111101111000111111000110E588\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(30)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "8699e-276\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10010110100101101111100100100011011101100110100101100E-903\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(31)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "8699e-276\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10010110100101101111100100100011011101100110100101101E-903\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(32)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "82081e41\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10111000000010000010111011111001111010100011111001011E153\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(33)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "82081e41\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10111000000010000010111011111001111010100011111001100E153\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(34)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "584169e229\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11101011001010111000001011001110111000111100110101010E780\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(35)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "584169e229\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11101011001010111000001011001110111000111100110101011E780\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(36)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "5783893e-128\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10011000111100000110011110000101100111110011101110100E-402\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(37)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "5783893e-128\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10011000111100000110011110000101100111110011101110101E-402\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(38)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "87575437e-310\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11000000001011100000110011110011010000000010001010110E-1003\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(39)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "87575437e-310\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11000000001011100000110011110011010000000010001010111E-1003\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(40)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "245540327e121\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11100010101101001111010010110100011100000100101000100E430\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(41)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "245540327e121\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11100010101101001111010010110100011100000100101000101E430\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(42)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "9078555839e-109\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11111110001010111010110000110011100110001010011101101E-329\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(43)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "9078555839e-109\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11111110001010111010110000110011100110001010011101110E-329\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(44)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "42333842451e201\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10000000110001001101000100110110111110101011101011111E704\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(45)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "42333842451e201\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10000000110001001101000100110110111110101011101100000E704\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(46)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "778380362293e218\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11001101010111000001001100001100110010000001010010010E764\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(47)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "778380362293e218\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11001101010111000001001100001100110010000001010010011E764\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(48)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "7812878489261e-179\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10010011011011010111001111011101111101101101001110100E-551\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(49)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "7812878489261e-179\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10010011011011010111001111011101111101101101001110101E-551\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(50)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "77003665618895e-73\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11000101111110111111001111111101001101111000000101001E-196\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(51)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "77003665618895e-73\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11000101111110111111001111111101001101111000000101010E-196\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(52)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "834735494917063e-300\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11111110001101100001001101111100010011001110111010001E-947\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(53)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "834735494917063e-300\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11111110001101100001001101111100010011001110111010010E-947\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(54)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "6182410494241627e-119\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10001101110010110010001011000010001000101110100000111E-342\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(55)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "6182410494241627e-119\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10001101110010110010001011000010001000101110100001000E-342\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(56)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp+16, "26153245263757307e49\x00", ppint32(10), ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10011110111100000000001011011110101100010000011011110E218\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(57)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str(cgtls, cgbp+16, "26153245263757307e49\x00", ppint32(10), ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10011110111100000000001011011110101100010000011011111E218\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {
		Xprintf(cgtls, "Error in mpfr_set_str (%d):\nexpected \x00", iqlibc.ppVaList(cgbp+48, ppint32(58)))
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+16)
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+16)
		Xexit(cgtls, ppint32(1))
	}

	/* to check this problem : I convert limb (10--0 or 101--1) into base b
	   with more than mp_bits_per_limb digits,
	   so when convert into base 2 I should have
	   the limb that I have choose */
	/* this use mpfr_get_str */

	aanb_digit = iqlibc.ppUint32FromInt32(X__gmp_bits_per_limb)
	aacheck_limb = [2]tnmp_limb_t{
		0: iqlibc.ppUint32FromInt32(1) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) - iqlibc.ppInt32FromInt32(1)),
		1: ^(iqlibc.ppUint32FromInt32(1) << (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) - iqlibc.ppInt32FromInt32(1)) >> iqlibc.ppInt32FromInt32(1)),
	}
	aabase1 = [3]ppint32{
		0: ppint32(10),
		1: ppint32(16),
		2: ppint32(19),
	}
	aarnd1 = [3]tnmpfr_rnd_t{
		0: ppint32(ecMPFR_RNDU),
		2: ppint32(ecMPFR_RNDD),
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint32(X__gmp_bits_per_limb)) /* x and y have only one limb */
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(X__gmp_bits_per_limb))

	aastr1 = Xtests_allocate(cgtls, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvN)+iqlibc.ppInt32FromInt32(20)))

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN)) /* ensures that x is not NaN or Inf */
	for {
		if !(aanb_digit < ppuint32(mvN)) {
			break
		}
		aacbase = 0
		for {
			if !(aacbase < ppint32(3)) {
				break
			}
			aaclimb = 0
			for {
				if !(aaclimb < ppint32(2)) {
					break
				}
				aacrnd = 0
				for {
					if !(aacrnd < ppint32(3)) {
						break
					}

					*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_d)) = aacheck_limb[aaclimb]
					(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = 0

					Xmpfr_get_str(cgtls, aastr1+ppuintptr(2), cgbp+36, aabase1[aacbase], aanb_digit, cgbp, aarnd1[aacrnd])
					*(*ppuint8)(iqunsafe.ppPointer(aastr1)) = ppuint8('-')
					*(*ppuint8)(iqunsafe.ppPointer(aastr1 + iqlibc.ppBoolUintptr(iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aastr1 + 2))) == iqlibc.ppInt32FromUint8('-')))) = ppuint8('0')
					*(*ppuint8)(iqunsafe.ppPointer(aastr1 + ppuintptr(iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aastr1 + 2))) == ppint32('-'))+ppint32(1)))) = ppuint8('.')

					aastr11 = aastr1
					for {
						if !(iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aastr11))) != 0) {
							break
						}

						goto cg_20
					cg_20:
						;
						aastr11++
					}
					Xsprintf(cgtls, aastr11, "@%i\x00", iqlibc.ppVaList(cgbp+48, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 36)))))

					Xmpfr_set_str(cgtls, cgbp+16, aastr1, aabase1[aacbase], aarnd1[ppint32(2)-aacrnd])

					if Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) != 0 {

						Xprintf(cgtls, "Error in mpfr_set_str for nb_digit=%u, base=%d, rnd=%s:\n\x00", iqlibc.ppVaList(cgbp+48, aanb_digit, aabase1[aacbase], Xmpfr_print_rnd_mode(cgtls, aarnd1[aacrnd])))
						Xprintf(cgtls, "instead of: \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "return    : \x00", 0)
						Xmpfr_dump(cgtls, cgbp+16)
						Xexit(cgtls, ppint32(1))
					}

					goto cg_19
				cg_19:
					;
					aacrnd++
				}
				goto cg_18
			cg_18:
				;
				aaclimb++
			}
			goto cg_17
		cg_17:
			;
			aacbase++
		}
		goto cg_16
	cg_16:
		;
		aanb_digit *= ppuint32(10)
	}

	Xtests_free(cgtls, aastr1, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvN)+iqlibc.ppInt32FromInt32(20)))

	/* end of tests added by Alain Delplanque */

	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_set_str(cgtls, cgbp, "+0.0\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "x <- +0.0 failed after x=NaN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_set_str(cgtls, cgbp, "-0.0\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp != -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 {

		Xprintf(cgtls, "x <- -0.0 failed after x=NaN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* check invalid input */
	aaret = Xmpfr_set_str(cgtls, cgbp, "1E10toto\x00", ppint32(10), ppint32(ecMPFR_RNDN))

	if ccv21 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaret == -iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv21 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(824), "ret == -1\x00")
	}
	pp_ = ccv21 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaret = Xmpfr_set_str(cgtls, cgbp, "1p10toto\x00", ppint32(16), ppint32(ecMPFR_RNDN))

	if ccv22 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaret == -iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv22 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(826), "ret == -1\x00")
	}
	pp_ = ccv22 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaret = Xmpfr_set_str(cgtls, cgbp, "\x00", ppint32(16), ppint32(ecMPFR_RNDN))

	if ccv23 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaret == -iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv23 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(828), "ret == -1\x00")
	}
	pp_ = ccv23 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaret = Xmpfr_set_str(cgtls, cgbp, "+\x00", ppint32(16), ppint32(ecMPFR_RNDN))

	if ccv24 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaret == -iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv24 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(830), "ret == -1\x00")
	}
	pp_ = ccv24 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaret = Xmpfr_set_str(cgtls, cgbp, "-\x00", ppint32(16), ppint32(ecMPFR_RNDN))

	if ccv25 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaret == -iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv25 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(832), "ret == -1\x00")
	}
	pp_ = ccv25 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaret = Xmpfr_set_str(cgtls, cgbp, "this_is_an_invalid_number_in_base_36\x00", ppint32(36), ppint32(ecMPFR_RNDN))

	if ccv26 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaret == -iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv26 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(834), "ret == -1\x00")
	}
	pp_ = ccv26 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaret = Xmpfr_set_str(cgtls, cgbp, "1.2.3\x00", ppint32(10), ppint32(ecMPFR_RNDN))

	if ccv27 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(aaret == -iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv27 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(836), "ret == -1\x00")
	}
	pp_ = ccv27 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_prec(cgtls, cgbp, ppint32(135))
	aaret = Xmpfr_set_str(cgtls, cgbp, "thisisavalidnumberinbase36\x00", ppint32(36), ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp+16, ppint32(135))
	Xmpfr_set_str(cgtls, cgbp+16, "23833565676460972739462619524519814462546\x00", ppint32(10), ppint32(ecMPFR_RNDN))

	if ccv29 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) == 0 && aaret == 0)), ppint32(1)) != 0; !ccv29 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(841), "mpfr_cmp3(x, y, 1) == 0 && ret == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp3(cgtls, cgbp, cgbp+16, ppint32(1)) == 0 && aaret == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv29 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* coverage test for set_str_binary */
	Xmpfr_set_str_binary(cgtls, cgbp, "NaN\x00")

	if ccv30 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint32(1)) != 0; !ccv30 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(845), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (1 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv30 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_str_binary(cgtls, cgbp, "Inf\x00")

	if ccv33 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv33 {
		if ccv31 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv31 {
			Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(847), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv31 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv32 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv32 {
			Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(847), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv32 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv39 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv33 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv39 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(847), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tset_str.c\", 847, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tset_str.c\", 847, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0\x00")
		if ccv38 = iqlibc.Bool(!(0 != 0)); !ccv38 {
			if ccv37 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv37 {
				if ccv35 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv35 {
					Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(847), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv35 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv36 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv36 {
					Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(847), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv36 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv38 || ccv37 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv39 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_str_binary(cgtls, cgbp, "+Inf\x00")

	if ccv42 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv42 {
		if ccv40 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv40 {
			Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(849), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv40 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv41 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv41 {
			Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(849), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv41 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv48 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv42 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0)), ppint32(1)) != 0; !ccv48 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(849), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tset_str.c\", 849, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tset_str.c\", 849, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) > 0\x00")
		if ccv47 = iqlibc.Bool(!(0 != 0)); !ccv47 {
			if ccv46 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv46 {
				if ccv44 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv44 {
					Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(849), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv44 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv45 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv45 {
					Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(849), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv45 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv47 || ccv46 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv48 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_str_binary(cgtls, cgbp, "-Inf\x00")

	if ccv51 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv51 {
		if ccv49 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv49 {
			Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(851), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
		}
		pp_ = ccv49 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv50 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv50 {
			Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(851), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
		}
		pp_ = ccv50 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv57 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(ccv51 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0)), ppint32(1)) != 0; !ccv57 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(851), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tset_str.c\", 851, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tset_str.c\", 851, \"! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) || !(! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1)))) || (! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((x)->_mpfr_sign)) < 0\x00")
		if ccv56 = iqlibc.Bool(!(0 != 0)); !ccv56 {
			if ccv55 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt32FromInt32(2)-iqlibc.ppInt32FromUint32(iqlibc.ppUint32FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv55 {
				if ccv53 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(2))), ppint32(1)) != 0; !ccv53 {
					Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(851), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+2))\x00")
				}
				pp_ = ccv53 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv54 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt32FromInt32(0x7fffffff)-iqlibc.ppInt32FromInt32(1)+iqlibc.ppInt32FromInt32(1))), ppint32(1)) != 0; !ccv54 {
					Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(851), "! (((x)->_mpfr_exp) == (((-0x7fffffffL-1))+1))\x00")
				}
				pp_ = ccv54 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv56 || ccv55 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv57 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_prec(cgtls, cgbp, ppint32(3))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.01E2\x00")

	if ccv59 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv59 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(854), "(__builtin_constant_p (1) && (mpfr_ulong) (1) == 0 ? (mpfr_sgn) (x) : mpfr_cmp_ui_2exp ((x), (1), 0)) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv59 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.01E2\x00")

	if ccv61 = X__builtin_expect(cgtls, iqlibc.ppBoolInt32(!!(Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint32(-iqlibc.ppInt32FromInt32(1)), 0) == iqlibc.ppInt32FromInt32(0))), ppint32(1)) != 0; !ccv61 {
		Xmpfr_assert_fail(cgtls, "tset_str.c\x00", ppint32(856), "(__builtin_constant_p (-1) && (mpfr_long) (-1) >= 0 ? (__builtin_constant_p ((mpfr_ulong) (mpfr_long) (-1)) && (mpfr_ulong) ((mpfr_ulong) (mpfr_long) (-1)) == 0 ? (mpfr_sgn) ((x)) : mpfr_cmp_ui_2exp (((x)), ((mpfr_ulong) (mpfr_long) (-1)), 0)) : mpfr_cmp_si_2exp ((x), (-1), 0)) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint32(-iqlibc.ppInt32FromInt32(1)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv61 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+16)

	sicheck_underflow(cgtls)
	sibug20081028(cgtls)
	sibug20180908(cgtls)

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint32, ppint32) ppint32

func ___builtin_unreachable(*iqlibc.ppTLS)

var ___gmp_bits_per_limb ppint32

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

var ___gmpfr_emax ppint32

var ___gmpfr_emin ppint32

var ___gmpfr_one [1]tn__mpfr_struct

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint32, ppuintptr, ppint32) ppuint32

func _atoi(*iqlibc.ppTLS, ppuintptr) ppint32

func _exit(*iqlibc.ppTLS, ppint32)

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_cmp3(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_cmp_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint32, ppint32) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_free_str(*iqlibc.ppTLS, ppuintptr)

func _mpfr_get_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuint32, ppuintptr, ppint32) ppuintptr

func _mpfr_greater_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppuint8

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint32, ppint32, ppint32) ppint32

func _mpfr_sgn(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _puts(*iqlibc.ppTLS, ppuintptr) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint32

func _set_emax(*iqlibc.ppTLS, ppint32)

func _set_emin(*iqlibc.ppTLS, ppint32)

func _sprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

var _stdout ppuintptr

func _strlen(*iqlibc.ppTLS, ppuintptr) ppuint32

func _tests_allocate(*iqlibc.ppTLS, ppuint32) ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_free(*iqlibc.ppTLS, ppuintptr, ppuint32)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

// Code generated for linux/ppc64le by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/ppc64le -UHAVE_NEARBYINT -mlong-double-64 -c -o tadd.o.go tadd.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMAX_PREC = 256
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1158
const mvMPFR_AI_THRESHOLD3 = 20165
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 580
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 10480
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 9
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 22904
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 14
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/powerpc64/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvN = 30000
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTGENERIC_SO_TEST = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_ARCH_PPC = 1
const mv_ARCH_PPC64 = 1
const mv_ARCH_PPCGR = 1
const mv_ARCH_PPCSQ = 1
const mv_ARCH_PWR4 = 1
const mv_ARCH_PWR5 = 1
const mv_ARCH_PWR5X = 1
const mv_ARCH_PWR6 = 1
const mv_ARCH_PWR7 = 1
const mv_ARCH_PWR8 = 1
const mv_CALL_ELF = 2
const mv_CALL_LINUX = 1
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LITTLE_ENDIAN = 1
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ALTIVEC__ = 1
const mv__APPLE_ALTIVEC__ = 1
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BUILTIN_CPU_SUPPORTS__ = 1
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__CMODEL_MEDIUM__ = 1
const mv__CRYPTO__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT128_TYPE__ = 1
const mv__FLOAT128__ = 1
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FP_FAST_FMAL = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "powerpc64le-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=/build/gmp-dldbp2/gmp-6.2.1+dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 1
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 1
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC__ = 10
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1014
const mv__HAVE_BSWAP__ = 1
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__HTM__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LITTLE_ENDIAN__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__POWER8_VECTOR__ = 1
const mv__PPC64__ = 1
const mv__PPC__ = 1
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__QUAD_MEMORY_ATOMIC__ = 1
const mv__RECIPF__ = 1
const mv__RECIP_PRECISION__ = 1
const mv__RECIP__ = 1
const mv__RSQRTEF__ = 1
const mv__RSQRTE__ = 1
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__STRUCT_PARM_ALIGN__ = 16
const mv__TM_FENCE__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VEC_ELEMENT_REG_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__VEC__ = 10206
const mv__VERSION__ = "10.2.1 20210110"
const mv__VSX__ = 1
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__builtin_copysignq = "__builtin_copysignf128"
const mv__builtin_fabsq = "__builtin_fabsf128"
const mv__builtin_huge_valq = "__builtin_huge_valf128"
const mv__builtin_infq = "__builtin_inff128"
const mv__builtin_nanq = "__builtin_nanf128"
const mv__builtin_nansq = "__builtin_nansf128"
const mv__builtin_vsx_vperm = "__builtin_vec_perm"
const mv__builtin_vsx_xvmaddadp = "__builtin_vsx_xvmadddp"
const mv__builtin_vsx_xvmaddasp = "__builtin_vsx_xvmaddsp"
const mv__builtin_vsx_xvmaddmdp = "__builtin_vsx_xvmadddp"
const mv__builtin_vsx_xvmaddmsp = "__builtin_vsx_xvmaddsp"
const mv__builtin_vsx_xvmsubadp = "__builtin_vsx_xvmsubdp"
const mv__builtin_vsx_xvmsubasp = "__builtin_vsx_xvmsubsp"
const mv__builtin_vsx_xvmsubmdp = "__builtin_vsx_xvmsubdp"
const mv__builtin_vsx_xvmsubmsp = "__builtin_vsx_xvmsubsp"
const mv__builtin_vsx_xvnmaddadp = "__builtin_vsx_xvnmadddp"
const mv__builtin_vsx_xvnmaddasp = "__builtin_vsx_xvnmaddsp"
const mv__builtin_vsx_xvnmaddmdp = "__builtin_vsx_xvnmadddp"
const mv__builtin_vsx_xvnmaddmsp = "__builtin_vsx_xvnmaddsp"
const mv__builtin_vsx_xvnmsubadp = "__builtin_vsx_xvnmsubdp"
const mv__builtin_vsx_xvnmsubasp = "__builtin_vsx_xvnmsubsp"
const mv__builtin_vsx_xvnmsubmdp = "__builtin_vsx_xvnmsubdp"
const mv__builtin_vsx_xvnmsubmsp = "__builtin_vsx_xvnmsubsp"
const mv__builtin_vsx_xxland = "__builtin_vec_and"
const mv__builtin_vsx_xxlandc = "__builtin_vec_andc"
const mv__builtin_vsx_xxlnor = "__builtin_vec_nor"
const mv__builtin_vsx_xxlor = "__builtin_vec_or"
const mv__builtin_vsx_xxlxor = "__builtin_vec_xor"
const mv__builtin_vsx_xxsel = "__builtin_vec_sel"
const mv__float128 = "__ieee128"
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__powerpc64__ = 1
const mv__powerpc__ = 1
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

/* If the precisions are the same, we want to test both mpfr_add1sp
   and mpfr_add1. */

/* FIXME: modify check() to test the ternary value and the flags. */

var siusesp ppint32

func sitest_add(cgtls *iqlibc.ppTLS, aaa tnmpfr_ptr, aab tnmpfr_srcptr, aac tnmpfr_srcptr, aarnd_mode tnmpfr_rnd_t) (cgr ppint32) {

	var aares ppint32
	pp_ = aares
	if siusesp != 0 || (X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(aab)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3))), 0) != 0 || X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(aac)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3))), 0) != 0) || (*tn__mpfr_struct)(iqunsafe.ppPointer(aab)).fd_mpfr_sign != (*tn__mpfr_struct)(iqunsafe.ppPointer(aac)).fd_mpfr_sign {
		aares = Xmpfr_add(cgtls, aaa, aab, aac, aarnd_mode)
	} else {

		if (*tn__mpfr_struct)(iqunsafe.ppPointer(aab)).fd_mpfr_exp < (*tn__mpfr_struct)(iqunsafe.ppPointer(aac)).fd_mpfr_exp {
			aares = Xmpfr_add1(cgtls, aaa, aac, aab, aarnd_mode)
		} else {
			aares = Xmpfr_add1(cgtls, aaa, aab, aac, aarnd_mode)
		}
	}
	return aares
}

// C documentation
//
//	/* checks that xs+ys gives the expected result zs */
func sicheck(cgtls *iqlibc.ppTLS, aaxs ppuintptr, aays ppuintptr, aarnd_mode tnmpfr_rnd_t, aapx ppuint32, aapy ppuint32, aapz ppuint32, aazs ppuintptr) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var pp_ /* xx at bp+0 */ tnmpfr_t
	var pp_ /* yy at bp+32 */ tnmpfr_t
	var pp_ /* zz at bp+64 */ tnmpfr_t

	Xmpfr_init2(cgtls, cgbp, iqlibc.ppInt64FromUint32(aapx))
	Xmpfr_init2(cgtls, cgbp+32, iqlibc.ppInt64FromUint32(aapy))
	Xmpfr_init2(cgtls, cgbp+64, iqlibc.ppInt64FromUint32(aapz))

	Xmpfr_set_str(cgtls, cgbp, aaxs, ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, aays, ppint32(10), ppint32(ecMPFR_RNDN))
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, aarnd_mode)
	if Xmpfr_cmp_str(cgtls, cgbp+64, aazs, ppint32(10), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "expected sum is %s, got \x00", iqlibc.ppVaList(cgbp+104, aazs))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp+64, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\nmpfr_add failed for x=%s y=%s with rnd_mode=%s\n\x00", iqlibc.ppVaList(cgbp+104, aaxs, aays, Xmpfr_print_rnd_mode(cgtls, aarnd_mode)))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

func sicheck2b(cgtls *iqlibc.ppTLS, aaxs ppuintptr, aapx ppint32, aays ppuintptr, aapy ppint32, aars ppuintptr, aapz ppint32, aarnd_mode tnmpfr_rnd_t) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var pp_ /* xx at bp+0 */ tnmpfr_t
	var pp_ /* yy at bp+32 */ tnmpfr_t
	var pp_ /* zz at bp+64 */ tnmpfr_t

	Xmpfr_init2(cgtls, cgbp, ppint64(aapx))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(aapy))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(aapz))
	Xmpfr_set_str_binary(cgtls, cgbp, aaxs)
	Xmpfr_set_str_binary(cgtls, cgbp+32, aays)
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, aarnd_mode)
	if Xmpfr_cmp_str(cgtls, cgbp+64, aars, ppint32(2), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "(2) x=%s,%d y=%s,%d pz=%d,rnd=%s\n\x00", iqlibc.ppVaList(cgbp+104, aaxs, aapx, aays, aapy, aapz, Xmpfr_print_rnd_mode(cgtls, aarnd_mode)))
		Xprintf(cgtls, "got        \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xmpfr_set_str(cgtls, cgbp+64, aars, ppint32(2), ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "instead of \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
}

func sicheck64(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aa_p, aa_p1 tnmpfr_srcptr
	var ccv1, ccv2 ppint32
	var pp_ /* t at bp+32 */ tnmpfr_t
	var pp_ /* u at bp+64 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aa_p, aa_p1, ccv1, ccv2

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)
	Xmpfr_init(cgtls, cgbp+64)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(29))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.1101001000101111011010010110e-3\x00")
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(58))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.11100010011111001001100110010111110110011000000100101E-1\x00")
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(29))
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "1.0101011100001000011100111110e-1\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_add(u, x, t) failed for prec(x)=29, prec(t)=58\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(29), cgbp+32, ppint32(ecMPFR_RNDN))
		Xputs(cgtls, "\x00")
		Xprintf(cgtls, "got      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(29), cgbp+64, ppint32(ecMPFR_RNDN))
		Xputs(cgtls, "\x00")
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(4))
	Xmpfr_set_str_binary(cgtls, cgbp, "-1.0E-2\x00")
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(2))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-1.1e-2\x00")
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(2))
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mpfr_d))<<iqlibc.ppInt32FromInt32(2) != 0 {

		Xprintf(cgtls, "result not normalized for prec=2\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-1.0e-1\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_add(u, x, t) failed for prec(x)=4, prec(t)=2\n\x00", 0)
		Xprintf(cgtls, "expected -1.0e-1\n\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(4), cgbp+64, ppint32(ecMPFR_RNDN))
		Xputs(cgtls, "\x00")
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(8))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.10011010\x00") /* -77/128 */
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(4))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-1.110e-5\x00") /* -7/128 */
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(4))
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN)) /* should give -5/8 */
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-1.010e-1\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+32, ppint32(1)) != 0 {
		Xprintf(cgtls, "mpfr_add(u, x, t) failed for prec(x)=8, prec(t)=4\n\x00", 0)
		Xprintf(cgtls, "expected -1.010e-1\n\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(4), cgbp+64, ppint32(ecMPFR_RNDN))
		Xputs(cgtls, "\x00")
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(112))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(98))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(54))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.11111100100000000011000011100000101101010001000111E-401\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10110000100100000101101100011111111011101000111000101E-464\x00")
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_add(u, x, t) failed for prec(x)=112, prec(t)=98\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(92))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(86))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(53))
	Xmpfr_set_str(cgtls, cgbp, "-5.03525136761487735093e-74\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "8.51539046314262304109e-91\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp+64, "-5.0352513676148773509283672e-74\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "mpfr_add(u, x, t) failed for prec(x)=92, prec(t)=86\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(76))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(76))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.10010010001001011011110000000000001010011011011110001E-32\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.1011000101110010000101111111011111010001110011110111100110101011110010011111\x00")
	Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1011000101110010000101111111011100111111101010011011110110101011101000000100\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "expect \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "mpfr_add failed for precisions 53-76\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(108))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(108))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.10010010001001011011110000000000001010011011011110001E-32\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.101100010111001000010111111101111101000111001111011110011010101111001001111000111011001110011000000000111111\x00")
	Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.101100010111001000010111111101110011111110101001101111011010101110100000001011000010101110011000000000111111\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "expect \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "mpfr_add failed for precisions 53-108\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_prec(cgtls, cgbp, ppint64(97))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(97))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(97))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1111101100001000000001011000110111101000001011111000100001000101010100011111110010000000000000000E-39\x00")
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1000000000000000000000000000000000000000111110110000100000000101100011011110100000101111100010001E1\x00")
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_add failed for precision 97\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_prec(cgtls, cgbp, ppint64(128))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(128))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(128))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10101011111001001010111011001000101100111101000000111111111011010100001100011101010001010111111101111010100110111111100101100010E-4\x00")
	{
		aa_p = cgbp
		ccv1 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
	}
	pp_ = ccv1
	Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(96))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(96))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(96))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.111000000001110100111100110101101001001010010011010011100111100011010100011001010011011011000010E-4\x00")
	{
		aa_p1 = cgbp
		ccv2 = Xmpfr_set4(cgtls, cgbp+32, aa_p1, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign)
	}
	pp_ = ccv2
	Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(85))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(85))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(85))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1111101110100110110110100010101011101001100010100011110110110010010011101100101111100E-4\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1111101110100110110110100010101001001000011000111000011101100101110100001110101010110E-4\x00")
	Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	Xmpfr_sub(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+64, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_sub: u=x-t and x=x-t give different results\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if !(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_d + ppuintptr(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))*8))&(iqlibc.ppUint64FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1))) != iqlibc.ppUint64FromInt32(0)) {

		Xprintf(cgtls, "Error in mpfr_sub: result is not msb-normalized (1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_prec(cgtls, cgbp, ppint64(65))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(65))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(65))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10011010101000110101010000000011001001001110001011101011111011101E623\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10011010101000110101010000000011001001001110001011101011111011100E623\x00")
	Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, ppuint64(1), ppint64(558)) != 0 {
		/* 2^558 */
		Xprintf(cgtls, "Error (1) in mpfr_sub\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(64))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(64))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(64))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1000011110101111011110111111000011101011101111101101101100000100E-220\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1000011110101111011110111111000011101011101111101101010011111101E-220\x00")
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	if *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 64)).fd_mpfr_d))&ppuint64(1) != ppuint64(1) {

		Xprintf(cgtls, "error in mpfr_add with rnd_mode=MPFR_RNDU\n\x00", 0)
		Xprintf(cgtls, "b=  \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "c=  \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "b+c=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}

	/* bug found by Norbert Mueller, 14 Sep 2000 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(56))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(83))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(10))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10001001011011001111101100110100000101111010010111010111E-7\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10001001011011001111101100110100000101111010010111010111000000000111110110110000100E-7\x00")
	Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))

	/* array bound write found by Norbert Mueller, 26 Sep 2000 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(109))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(153))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(95))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1001010000101011101100111000110001111111111111111111111111111111111111111111111111111111111111100000000000000E33\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.100101000010101110110011100011000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011100101101000000100100001100110111E33\x00")
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))

	/* array bound writes found by Norbert Mueller, 27 Sep 2000 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(106))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(53))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(23))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.1000011110101111111001010001000100001011000000000000000000000000000000000000000000000000000000000000000000E-59\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.10000111101011111110010100010001101100011100110100000E-59\x00")
	Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(177))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(217))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(160))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.111010001011010000111001001010010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000E35\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1110100010110100001110010010100100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000111011010011100001111001E35\x00")
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(214))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(278))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(207))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1000100110100110101101101101000000010000100111000001001110001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000E66\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.10001001101001101011011011010000000100001001110000010011100010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001111011111001001100011E66\x00")
	sitest_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(32))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(247))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(223))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10000000000000000000000000000000E1\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111100000110001110100000100011110000101110110011101110100110110111111011010111100100000000000000000000000000E0\x00")
	Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if !(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_d + ppuintptr(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))*8))&(iqlibc.ppUint64FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1))) != iqlibc.ppUint64FromInt32(0)) {

		Xprintf(cgtls, "Error in mpfr_sub: result is not msb-normalized (2)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* bug found by Nathalie Revol, 21 March 2001 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(65))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(65))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(65))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11100100101101001100111011111111110001101001000011101001001010010E-35\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10000000000000000000000000000000000001110010010110100110011110000E1\x00")
	Xmpfr_sub(cgtls, cgbp+64, cgbp+32, cgbp, ppint32(ecMPFR_RNDU))
	if !(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_d + ppuintptr(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))*8))&(iqlibc.ppUint64FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1))) != iqlibc.ppUint64FromInt32(0)) {

		Xprintf(cgtls, "Error in mpfr_sub: result is not msb-normalized (3)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* bug found by Fabrice Rouillier, 27 Mar 2001 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(107))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(107))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(107))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10111001001111010010001000000010111111011011011101000001001000101000000000000000000000000000000000000000000E315\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10000000000000000000000000000000000101110100100101110110000001100101011111001000011101111100100100111011000E350\x00")
	Xmpfr_sub(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	if !(*(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_d + ppuintptr(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))*8))&(iqlibc.ppUint64FromInt32(1)<<(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)-iqlibc.ppInt32FromInt32(1))) != iqlibc.ppUint64FromInt32(0)) {

		Xprintf(cgtls, "Error in mpfr_sub: result is not msb-normalized (4)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* checks that NaN flag is correctly reset */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_nan(cgtls, cgbp)
	sitest_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) != 0 {

		Xprintf(cgtls, "Error in mpfr_add: 1+1 gives \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
}

// C documentation
//
//	/* check case when c does not overlap with a, but both b and c count
//	   for rounding */
func sicheck_case_1b(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aadif, aaprec_a, aaprec_b, aaprec_c ppuint32
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aadif, aaprec_a, aaprec_b, aaprec_c

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)
	Xmpfr_init(cgtls, cgbp+64)

	aaprec_a = ppuint32(ppuint64(mvMPFR_PREC_MIN) + Xrandlimb(cgtls)%ppuint64(63))
	Xmpfr_set_prec(cgtls, cgbp, iqlibc.ppInt64FromUint32(aaprec_a))
	aaprec_b = aaprec_a + ppuint32(2)
	for {
		if !(aaprec_b <= ppuint32(64)) {
			break
		}

		aadif = aaprec_b - aaprec_a
		Xmpfr_set_prec(cgtls, cgbp+32, iqlibc.ppInt64FromUint32(aaprec_b))
		/* b = 1 - 2^(-prec_a) + 2^(-prec_b) */
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
		Xmpfr_div_2ui(cgtls, cgbp+32, cgbp+32, ppuint64(aadif), ppint32(ecMPFR_RNDN))
		Xmpfr_sub_ui(cgtls, cgbp+32, cgbp+32, ppuint64(1), ppint32(ecMPFR_RNDN))
		Xmpfr_div_2ui(cgtls, cgbp+32, cgbp+32, ppuint64(aaprec_a), ppint32(ecMPFR_RNDN))
		Xmpfr_add_ui(cgtls, cgbp+32, cgbp+32, ppuint64(1), ppint32(ecMPFR_RNDN))
		aaprec_c = aadif
		for {
			if !(aaprec_c <= ppuint32(64)) {
				break
			}

			/* c = 2^(-prec_a) - 2^(-prec_b) */
			Xmpfr_set_prec(cgtls, cgbp+64, iqlibc.ppInt64FromUint32(aaprec_c))
			pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+64, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
			Xmpfr_div_2ui(cgtls, cgbp+64, cgbp+64, ppuint64(aadif), ppint32(ecMPFR_RNDN))
			Xmpfr_add_ui(cgtls, cgbp+64, cgbp+64, ppuint64(1), ppint32(ecMPFR_RNDN))
			Xmpfr_div_2ui(cgtls, cgbp+64, cgbp+64, ppuint64(aaprec_a), ppint32(ecMPFR_RNDN))
			sitest_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))
			if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

				Xprintf(cgtls, "case (1b) failed for prec_a=%u, prec_b=%u, prec_c=%u\n\x00", iqlibc.ppVaList(cgbp+104, aaprec_a, aaprec_b, aaprec_c))
				Xprintf(cgtls, "b=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xprintf(cgtls, "c=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+64)
				Xprintf(cgtls, "a=\x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_2
		cg_2:
			;
			aaprec_c++
		}

		goto cg_1
	cg_1:
		;
		aaprec_b++
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
}

// C documentation
//
//	/* check case when c overlaps with a */
func sicheck_case_2(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t

	Xmpfr_init2(cgtls, cgbp, ppint64(300))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(800))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(500))
	Xmpfr_init2(cgtls, cgbp+96, ppint64(800))

	Xmpfr_set_str_binary(cgtls, cgbp, "1E110\x00")                     /* a = 2^110 */
	Xmpfr_set_str_binary(cgtls, cgbp+32, "1E900\x00")                  /* b = 2^900 */
	Xmpfr_set_str_binary(cgtls, cgbp+64, "1E500\x00")                  /* c = 2^500 */
	sitest_add(cgtls, cgbp+64, cgbp+64, cgbp, ppint32(ecMPFR_RNDZ))    /* c = 2^500 + 2^110 */
	Xmpfr_sub(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDZ))  /* d = 2^900 - 2^500 - 2^110 */
	sitest_add(cgtls, cgbp+32, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDZ)) /* b = 2^900 + 2^500 + 2^110 */
	sitest_add(cgtls, cgbp, cgbp+32, cgbp+96, ppint32(ecMPFR_RNDZ))    /* a = 2^901 */
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), ppint64(901)) != 0 {

		Xprintf(cgtls, "b + d fails for b=2^900+2^500+2^110, d=2^900-2^500-2^110\n\x00", 0)
		Xprintf(cgtls, "expected 1.0e901, got \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
}

// C documentation
//
//	/* checks when source and destination are equal */
func sicheck_same(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var pp_ /* x at bp+0 */ tnmpfr_t

	Xmpfr_init(cgtls, cgbp)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDZ))
	sitest_add(cgtls, cgbp, cgbp, cgbp, ppint32(ecMPFR_RNDZ))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) != 0 {

		Xprintf(cgtls, "Error when all 3 operands are equal\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_inexact(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aacmp, aainexact ppint32
	var aapu, aapx, aapy, aapz tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv5, ccv6 ppint64
	var pp_ /* u at bp+96 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aacmp, aainexact, aapu, aapx, aapy, aapz, aarnd, ccv5, ccv6

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)
	Xmpfr_init(cgtls, cgbp+64)
	Xmpfr_init(cgtls, cgbp+96)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1E-4\x00")
	Xmpfr_set_prec(cgtls, cgbp+96, ppint64(33))
	Xmpfr_set_str_binary(cgtls, cgbp+96, "0.101110100101101100000000111100000E-1\x00")
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(31))
	aainexact = sitest_add(cgtls, cgbp+32, cgbp, cgbp+96, ppint32(ecMPFR_RNDN))

	if aainexact != 0 {

		Xprintf(cgtls, "Wrong ternary value (2): expected 0, got %d\n\x00", iqlibc.ppVaList(cgbp+136, aainexact))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1E-4\x00")
	Xmpfr_set_prec(cgtls, cgbp+96, ppint64(33))
	Xmpfr_set_str_binary(cgtls, cgbp+96, "0.101110100101101100000000111100000E-1\x00")
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(28))
	aainexact = sitest_add(cgtls, cgbp+32, cgbp, cgbp+96, ppint32(ecMPFR_RNDN))

	if aainexact != 0 {

		Xprintf(cgtls, "Wrong ternary value (1): expected 0, got %d\n\x00", iqlibc.ppVaList(cgbp+136, aainexact))
		Xexit(cgtls, ppint32(1))
	}

	aapx = ppint64(2)
	for {
		if !(aapx < ppint64(mvMAX_PREC)) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aapx)

		for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp) == 0 {

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		}

		aapu = ppint64(2)
		for {
			if !(aapu < ppint64(mvMAX_PREC)) {
				break
			}

			Xmpfr_set_prec(cgtls, cgbp+96, aapu)

			for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp+96) == 0 {

				if !(Xmpfr_rands_initialized != 0) {
					Xmpfr_rands_initialized = ppuint8(1)
					X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
					pp_ = iqlibc.ppInt32FromInt32(0)
				}
				Xmpfr_urandomb(cgtls, cgbp+96, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			}

			aapy = iqlibc.ppInt64FromUint64(ppuint64(mvMPFR_PREC_MIN) + Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvMAX_PREC)-iqlibc.ppInt32FromInt32(1)))
			Xmpfr_set_prec(cgtls, cgbp+32, aapy)
			if Xmpfr_cmpabs(cgtls, cgbp, cgbp+96) >= 0 {
				ccv5 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp - (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp
			} else {
				ccv5 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp - (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			}
			aapz = ccv5
			/* x + u is exactly representable with precision
			   abs(EXP(x)-EXP(u)) + max(prec(x), prec(u)) + 1 */
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec > (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_prec {
				ccv6 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec
			} else {
				ccv6 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 96)).fd_mpfr_prec
			}
			aapz = aapz + ccv6 + ppint64(1)
			Xmpfr_set_prec(cgtls, cgbp+64, aapz)

			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % ppuint64(ecMPFR_RNDF))
			aainexact = sitest_add(cgtls, cgbp+64, cgbp, cgbp+96, aarnd)
			if aainexact != 0 {

				Xprintf(cgtls, "z <- x + u should be exact\n\x00", 0)
				Xprintf(cgtls, "x=\x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "u=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+96)
				Xprintf(cgtls, "z=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+64)
				Xexit(cgtls, ppint32(1))
			}

			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % ppuint64(ecMPFR_RNDF))
			aainexact = sitest_add(cgtls, cgbp+32, cgbp, cgbp+96, aarnd)
			aacmp = Xmpfr_cmp3(cgtls, cgbp+32, cgbp+64, ppint32(1))
			if aainexact == 0 && aacmp != 0 || aainexact > 0 && aacmp <= 0 || aainexact < 0 && aacmp >= 0 {

				Xprintf(cgtls, "Wrong ternary value for rnd=%s\n\x00", iqlibc.ppVaList(cgbp+136, Xmpfr_print_rnd_mode(cgtls, aarnd)))
				Xprintf(cgtls, "expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+136, aacmp, aainexact))
				Xprintf(cgtls, "x=\x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "u=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+96)
				Xprintf(cgtls, "y=  \x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xprintf(cgtls, "x+u=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+64)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_3
		cg_3:
			;
			aapu++
		}

		goto cg_1
	cg_1:
		;
		aapx++
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
}

func sicheck_nans(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var ccv1, ccv10, ccv11, ccv12, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv2, ccv20, ccv22, ccv23, ccv24, ccv25, ccv26, ccv27, ccv28, ccv3, ccv30, ccv31, ccv32, ccv33, ccv4, ccv6, ccv7, ccv8, ccv9 ppbool
	var pp_ /* s at bp+0 */ tnmpfr_t
	var pp_ /* x at bp+32 */ tnmpfr_t
	var pp_ /* y at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = ccv1, ccv10, ccv11, ccv12, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv2, ccv20, ccv22, ccv23, ccv24, ccv25, ccv26, ccv27, ccv28, ccv3, ccv30, ccv31, ccv32, ccv33, ccv4, ccv6, ccv7, ccv8, ccv9

	Xmpfr_init2(cgtls, cgbp+32, ppint64(8))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(8))
	Xmpfr_init2(cgtls, cgbp, ppint64(8))

	/* +inf + -inf == nan */
	Xmpfr_set_inf(cgtls, cgbp+32, ppint32(1))
	Xmpfr_set_inf(cgtls, cgbp+64, -ppint32(1))
	sitest_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(580), "(((mpfr_srcptr) (0 ? (s) : (mpfr_srcptr) (s)))->_mpfr_exp == (1 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* +inf + 1 == +inf */
	Xmpfr_set_inf(cgtls, cgbp+32, ppint32(1))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)), 0, ppint32(ecMPFR_RNDN))
	sitest_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(586), "(((mpfr_srcptr) (0 ? (s) : (mpfr_srcptr) (s)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(587), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(587), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv9 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(587), "(((void) (((__builtin_expect(!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tadd.c\", 587, \"! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tadd.c\", 587, \"! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((s)->_mpfr_sign)) > 0\x00")
		if ccv8 = iqlibc.Bool(!(0 != 0)); !ccv8 {
			if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv6 {
				Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(587), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
			}
			pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv7 {
				Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(587), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
			}
			pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv8 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* -inf + 1 == -inf */
	Xmpfr_set_inf(cgtls, cgbp+32, -ppint32(1))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)), 0, ppint32(ecMPFR_RNDN))
	sitest_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))

	if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv10 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(593), "(((mpfr_srcptr) (0 ? (s) : (mpfr_srcptr) (s)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv11 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(594), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
	}
	pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv12 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(594), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv17 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv17 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(594), "(((void) (((__builtin_expect(!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tadd.c\", 594, \"! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tadd.c\", 594, \"! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((s)->_mpfr_sign)) < 0\x00")
		if ccv16 = iqlibc.Bool(!(0 != 0)); !ccv16 {
			if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv14 {
				Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(594), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
			}
			pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if ccv15 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv15 {
				Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(594), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
			}
			pp_ = ccv15 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv16 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv17 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* 1 + +inf == +inf */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_inf(cgtls, cgbp+64, ppint32(1))
	sitest_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))

	if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv18 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(600), "(((mpfr_srcptr) (0 ? (s) : (mpfr_srcptr) (s)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv19 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv19 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(601), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
	}
	pp_ = ccv19 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv20 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv20 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(601), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv20 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv25 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv25 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(601), "(((void) (((__builtin_expect(!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tadd.c\", 601, \"! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tadd.c\", 601, \"! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((s)->_mpfr_sign)) > 0\x00")
		if ccv24 = iqlibc.Bool(!(0 != 0)); !ccv24 {
			if ccv22 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv22 {
				Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(601), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
			}
			pp_ = ccv22 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if ccv23 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv23 {
				Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(601), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
			}
			pp_ = ccv23 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv24 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv25 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* 1 + -inf == -inf */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_inf(cgtls, cgbp+64, -ppint32(1))
	sitest_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))

	if ccv26 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv26 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(607), "(((mpfr_srcptr) (0 ? (s) : (mpfr_srcptr) (s)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv26 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv27 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv27 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(608), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
	}
	pp_ = ccv27 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv28 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv28 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(608), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv28 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv33 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv33 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(608), "(((void) (((__builtin_expect(!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tadd.c\", 608, \"! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tadd.c\", 608, \"! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((s)->_mpfr_sign)) < 0\x00")
		if ccv32 = iqlibc.Bool(!(0 != 0)); !ccv32 {
			if ccv30 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv30 {
				Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(608), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
			}
			pp_ = ccv30 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if ccv31 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv31 {
				Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(608), "! (((s)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
			}
			pp_ = ccv31 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv32 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv33 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_alloc(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var pp_ /* a at bp+0 */ tnmpfr_t

	Xmpfr_init2(cgtls, cgbp, ppint64(10000))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(15236)), 0, ppint32(ecMPFR_RNDN))
	sitest_add(cgtls, cgbp, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_mul(cgtls, cgbp, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_div(cgtls, cgbp, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_sub(cgtls, cgbp, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_overflow(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaprec_a, aaprec_b, aaprec_c tnmpfr_prec_t
	var aar, aaup ppint32
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aaprec_a, aaprec_b, aaprec_c, aar, aaup

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)
	Xmpfr_init(cgtls, cgbp+64)

	aar = 0
	for {
		if !(aar < ppint32(ecMPFR_RNDF)) {
			break
		}
		aaprec_a = ppint64(2)
		for {
			if !(aaprec_a <= ppint64(128)) {
				break
			}
			aaprec_b = ppint64(2)
			for {
				if !(aaprec_b <= ppint64(128)) {
					break
				}
				aaprec_c = ppint64(2)
				for {
					if !(aaprec_c <= ppint64(128)) {
						break
					}

					Xmpfr_set_prec(cgtls, cgbp, aaprec_a)
					Xmpfr_set_prec(cgtls, cgbp+32, aaprec_b)
					Xmpfr_set_prec(cgtls, cgbp+64, aaprec_c)

					Xmpfr_setmax(cgtls, cgbp+32, X__gmpfr_emax)

					aaup = iqlibc.ppBoolInt32(aar == ppint32(ecMPFR_RNDA) || aar == ppint32(ecMPFR_RNDU) || aar == ppint32(ecMPFR_RNDN))

					/* set c with overlap with bits of b: will always overflow */
					Xmpfr_set_ui_2exp(cgtls, cgbp+64, ppuint64(1), X__gmpfr_emax-aaprec_b/ppint64(2), ppint32(ecMPFR_RNDN))
					Xmpfr_nextbelow(cgtls, cgbp+64)
					Xmpfr_clear_overflow(cgtls)
					sitest_add(cgtls, cgbp, cgbp+32, cgbp+64, aar)
					if !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) || aaup != 0 && !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

						Xprintf(cgtls, "No overflow (1) in check_overflow for rnd=%s\n\x00", iqlibc.ppVaList(cgbp+104, Xmpfr_print_rnd_mode(cgtls, aar)))
						Xprintf(cgtls, "b=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+32)
						Xprintf(cgtls, "c=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "a=\x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xexit(cgtls, ppint32(1))
					}

					if aar == ppint32(ecMPFR_RNDZ) || aar == ppint32(ecMPFR_RNDD) || aaprec_a >= aaprec_b+aaprec_c {
						goto cg_4
					}

					/* set c to 111...111 so that ufp(c) = 1/2 ulp(b): will only overflow
					   when prec_a < prec_b + prec_c, and rounding up or to nearest */
					Xmpfr_set_ui_2exp(cgtls, cgbp+64, ppuint64(1), X__gmpfr_emax-aaprec_b, ppint32(ecMPFR_RNDN))
					Xmpfr_nextbelow(cgtls, cgbp+64)
					Xmpfr_clear_overflow(cgtls)
					sitest_add(cgtls, cgbp, cgbp+32, cgbp+64, aar)
					/* b + c is exactly representable iff prec_a >= prec_b + prec_c */
					if !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

						Xprintf(cgtls, "No overflow (2) in check_overflow for rnd=%s\n\x00", iqlibc.ppVaList(cgbp+104, Xmpfr_print_rnd_mode(cgtls, aar)))
						Xprintf(cgtls, "b=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+32)
						Xprintf(cgtls, "c=\x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "a=\x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xexit(cgtls, ppint32(1))
					}

					goto cg_4
				cg_4:
					;
					aaprec_c += ppint64(2)
				}
				goto cg_3
			cg_3:
				;
				aaprec_b += ppint64(2)
			}
			goto cg_2
		cg_2:
			;
			aaprec_a += ppint64(2)
		}
		goto cg_1
	cg_1:
		;
		aar++
	}

	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(256))
	Xmpfr_setmax(cgtls, cgbp+32, X__gmpfr_emax)
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(256))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_exp(cgtls, cgbp+64, X__gmpfr_emax-ppint64(512))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(256))
	Xmpfr_clear_overflow(cgtls)
	Xmpfr_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))
	if !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) {

		Xprintf(cgtls, "No overflow (3) in check_overflow\n\x00", 0)
		Xprintf(cgtls, "b=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "c=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xprintf(cgtls, "a=\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
}

func sicheck_1111(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(208)
	defer cgtls.ppFree(208)

	var aa_p tnmpfr_srcptr
	var aadiff, aatb, aatc tnmpfr_exp_t
	var aainex_a, aainex_s, aam, aasb, aasc, ccv2 ppint32
	var aan ppint64
	var aaprec_a, aaprec_b, aaprec_c tnmpfr_prec_t
	var aarnd_mode tnmpfr_rnd_t
	var pp_ /* a at bp+32 */ tnmpfr_t
	var pp_ /* b at bp+64 */ tnmpfr_t
	var pp_ /* c at bp+96 */ tnmpfr_t
	var pp_ /* one at bp+0 */ tnmpfr_t
	var pp_ /* s at bp+128 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aadiff, aainex_a, aainex_s, aam, aan, aaprec_a, aaprec_b, aaprec_c, aarnd_mode, aasb, aasc, aatb, aatc, ccv2

	Xmpfr_init2(cgtls, cgbp, ppint64(mvMPFR_PREC_MIN))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	aan = 0
	for {
		if !(aan < ppint64(mvN)) {
			break
		}

		aatb = 0
		aam = ppint32(512)

		aaprec_a = iqlibc.ppInt64FromUint64(ppuint64(mvMPFR_PREC_MIN) + Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(aam))
		aaprec_b = iqlibc.ppInt64FromUint64(ppuint64(mvMPFR_PREC_MIN) + Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(aam))
		/* we need prec_c > 1 so that % (prec_c - 1) is well defined below */
		for cgcond := true; cgcond; cgcond = aaprec_c == ppint64(1) {
			aaprec_c = iqlibc.ppInt64FromUint64(ppuint64(mvMPFR_PREC_MIN) + Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(aam))
		}
		Xmpfr_init2(cgtls, cgbp+32, aaprec_a)
		Xmpfr_init2(cgtls, cgbp+64, aaprec_b)
		Xmpfr_init2(cgtls, cgbp+96, aaprec_c)
		/* we need prec_b - (sb != 2) > 0 below */
		for cgcond := true; cgcond; cgcond = aaprec_b-iqlibc.ppBoolInt64(aasb != iqlibc.ppInt32FromInt32(2)) == 0 {
			aasb = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % ppuint64(3))
		}
		if aasb != 0 {

			aatb = iqlibc.ppInt64FromUint64(ppuint64(1) + Xrandlimb(cgtls)%iqlibc.ppUint64FromInt64(aaprec_b-iqlibc.ppBoolInt64(aasb != iqlibc.ppInt32FromInt32(2))))
			Xmpfr_div_2ui(cgtls, cgbp+64, cgbp, iqlibc.ppUint64FromInt64(aatb), ppint32(ecMPFR_RNDN))
			if aasb == ppint32(2) {
				Xmpfr_neg(cgtls, cgbp+64, cgbp+64, ppint32(ecMPFR_RNDN))
			}
			sitest_add(cgtls, cgbp+64, cgbp+64, cgbp, ppint32(ecMPFR_RNDN))
		} else {
			{
				aa_p = cgbp
				ccv2 = Xmpfr_set4(cgtls, cgbp+64, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
			}
			pp_ = ccv2
		}
		aatc = iqlibc.ppInt64FromUint64(ppuint64(1) + Xrandlimb(cgtls)%iqlibc.ppUint64FromInt64(aaprec_c-iqlibc.ppInt64FromInt32(1)))
		Xmpfr_div_2ui(cgtls, cgbp+96, cgbp, iqlibc.ppUint64FromInt64(aatc), ppint32(ecMPFR_RNDN))
		aasc = iqlibc.ppBoolInt32(Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0))
		if aasc != 0 {
			Xmpfr_neg(cgtls, cgbp+96, cgbp+96, ppint32(ecMPFR_RNDN))
		}
		sitest_add(cgtls, cgbp+96, cgbp+96, cgbp, ppint32(ecMPFR_RNDN))
		aadiff = iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)*aam) - iqlibc.ppUint64FromInt32(aam))
		Xmpfr_mul_2si(cgtls, cgbp+96, cgbp+96, aadiff, ppint32(ecMPFR_RNDN))
		aarnd_mode = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % ppuint64(ecMPFR_RNDF))
		aainex_a = sitest_add(cgtls, cgbp+32, cgbp+64, cgbp+96, aarnd_mode)
		Xmpfr_init2(cgtls, cgbp+128, ppint64(ppint32(mvMPFR_PREC_MIN)+ppint32(2)*aam))
		aainex_s = sitest_add(cgtls, cgbp+128, cgbp+64, cgbp+96, ppint32(ecMPFR_RNDN)) /* exact */
		if aainex_s != 0 {

			Xprintf(cgtls, "check_1111: result should have been exact.\n\x00", 0)
			Xexit(cgtls, ppint32(1))
		}
		aainex_s = Xmpfr_prec_round(cgtls, cgbp+128, aaprec_a, aarnd_mode)
		if aainex_a < 0 && aainex_s >= 0 || aainex_a == 0 && aainex_s != 0 || aainex_a > 0 && aainex_s <= 0 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+128) != 0) {

			Xprintf(cgtls, "check_1111: results are different.\n\x00", 0)
			Xprintf(cgtls, "prec_a = %d, prec_b = %d, prec_c = %d\n\x00", iqlibc.ppVaList(cgbp+168, ppint32(aaprec_a), ppint32(aaprec_b), ppint32(aaprec_c)))
			Xprintf(cgtls, "tb = %d, tc = %d, diff = %d, rnd = %s\n\x00", iqlibc.ppVaList(cgbp+168, ppint32(aatb), ppint32(aatc), ppint32(aadiff), Xmpfr_print_rnd_mode(cgtls, aarnd_mode)))
			Xprintf(cgtls, "sb = %d, sc = %d\n\x00", iqlibc.ppVaList(cgbp+168, aasb, aasc))
			Xprintf(cgtls, "b = \x00", 0)
			Xmpfr_dump(cgtls, cgbp+64)
			Xprintf(cgtls, "c = \x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xprintf(cgtls, "a = \x00", 0)
			Xmpfr_dump(cgtls, cgbp+32)
			Xprintf(cgtls, "s = \x00", 0)
			Xmpfr_dump(cgtls, cgbp+128)
			Xprintf(cgtls, "inex_a = %d, inex_s = %d\n\x00", iqlibc.ppVaList(cgbp+168, aainex_a, aainex_s))
			Xexit(cgtls, ppint32(1))
		}
		Xmpfr_clear(cgtls, cgbp+32)
		Xmpfr_clear(cgtls, cgbp+64)
		Xmpfr_clear(cgtls, cgbp+96)
		Xmpfr_clear(cgtls, cgbp+128)

		goto cg_1
	cg_1:
		;
		aan++
	}
	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_1minuseps(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aaia, aaib, aaic ppuint32
	var aainex_a, aainex_s, aarnd_mode ppint32
	var aaprec_b tnmpfr_prec_t
	var ccv2, ccv4, ccv6 ppbool
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* s at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaia, aaib, aaic, aainex_a, aainex_s, aaprec_b, aarnd_mode, ccv2, ccv4, ccv6
	var snprec_a = [13]tnmpfr_prec_t{
		0:  ppint64(mvMPFR_PREC_MIN),
		1:  ppint64(30),
		2:  ppint64(31),
		3:  ppint64(32),
		4:  ppint64(33),
		5:  ppint64(62),
		6:  ppint64(63),
		7:  ppint64(64),
		8:  ppint64(65),
		9:  ppint64(126),
		10: ppint64(127),
		11: ppint64(128),
		12: ppint64(129),
	}
	var snsupp_b = [19]ppint32{
		1:  ppint32(1),
		2:  ppint32(2),
		3:  ppint32(3),
		4:  ppint32(4),
		5:  ppint32(29),
		6:  ppint32(30),
		7:  ppint32(31),
		8:  ppint32(32),
		9:  ppint32(33),
		10: ppint32(34),
		11: ppint32(35),
		12: ppint32(61),
		13: ppint32(62),
		14: ppint32(63),
		15: ppint32(64),
		16: ppint32(65),
		17: ppint32(66),
		18: ppint32(67),
	}

	Xmpfr_init2(cgtls, cgbp+64, ppint64(mvMPFR_PREC_MIN))

	aaia = ppuint32(0)
	for {
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppuintptr(iqunsafe.ppPointer(&snprec_a)) == ppuintptr(iqunsafe.ppPointer(&snprec_a)))), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(808), "(void *) &(prec_a) == (void *) &(prec_a)[0]\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if !(iqlibc.ppInt64FromUint32(aaia) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(104)/iqlibc.ppUint64FromInt64(8))) {
			break
		}
		aaib = ppuint32(0)
		for {
			if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppuintptr(iqunsafe.ppPointer(&snsupp_b)) == ppuintptr(iqunsafe.ppPointer(&snsupp_b)))), ppint64(1)) != 0; !ccv4 {
				Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(809), "(void *) &(supp_b) == (void *) &(supp_b)[0]\x00")
			}
			pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if !(iqlibc.ppInt64FromUint32(aaib) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(76)/iqlibc.ppUint64FromInt64(4))) {
				break
			}

			aaprec_b = snprec_a[aaia] + ppint64(snsupp_b[aaib])

			Xmpfr_init2(cgtls, cgbp, snprec_a[aaia])
			Xmpfr_init2(cgtls, cgbp+32, aaprec_b)

			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
			pp_ = Xmpfr_div_ui(cgtls, cgbp+32, cgbp+64, iqlibc.ppUint64FromInt64(snprec_a[aaia]), ppint32(ecMPFR_RNDN))
			Xmpfr_sub(cgtls, cgbp+32, cgbp+64, cgbp+32, ppint32(ecMPFR_RNDN)) /* b = 1 - 2^(-prec_a) */

			aaic = ppuint32(0)
			for {
				if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppuintptr(iqunsafe.ppPointer(&snsupp_b)) == ppuintptr(iqunsafe.ppPointer(&snsupp_b)))), ppint64(1)) != 0; !ccv6 {
					Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(823), "(void *) &(supp_b) == (void *) &(supp_b)[0]\x00")
				}
				pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if !(iqlibc.ppInt64FromUint32(aaic) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(76)/iqlibc.ppUint64FromInt64(4))) {
					break
				}
				aarnd_mode = 0
				for {
					if !(aarnd_mode < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
						break
					}

					if aarnd_mode == ppint32(ecMPFR_RNDF) {
						goto cg_7
					} /* inex_a, inex_s make no sense */

					pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
					pp_ = Xmpfr_div_ui(cgtls, cgbp+64, cgbp+64, iqlibc.ppUint64FromInt64(snprec_a[aaia]+ppint64(snsupp_b[aaic])), ppint32(ecMPFR_RNDN))
					aainex_a = sitest_add(cgtls, cgbp, cgbp+32, cgbp+64, aarnd_mode)
					Xmpfr_init2(cgtls, cgbp+96, ppint64(256))
					aainex_s = sitest_add(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN)) /* exact */
					if aainex_s != 0 {

						Xprintf(cgtls, "check_1minuseps: result should have been exact (ia = %u, ib = %u, ic = %u)\n\x00", iqlibc.ppVaList(cgbp+136, aaia, aaib, aaic))
						Xexit(cgtls, ppint32(1))
					}
					aainex_s = Xmpfr_prec_round(cgtls, cgbp+96, snprec_a[aaia], aarnd_mode)
					if aainex_a < 0 && aainex_s >= 0 || aainex_a == 0 && aainex_s != 0 || aainex_a > 0 && aainex_s <= 0 || !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

						Xprintf(cgtls, "check_1minuseps: results are different.\n\x00", 0)
						Xprintf(cgtls, "ia = %u, ib = %u, ic = %u\n\x00", iqlibc.ppVaList(cgbp+136, aaia, aaib, aaic))
						Xexit(cgtls, ppint32(1))
					}
					Xmpfr_clear(cgtls, cgbp+96)

					goto cg_7
				cg_7:
					;
					aarnd_mode++
				}
				goto cg_5
			cg_5:
				;
				aaic++
			}

			Xmpfr_clear(cgtls, cgbp)
			Xmpfr_clear(cgtls, cgbp+32)

			goto cg_3
		cg_3:
			;
			aaib++
		}
		goto cg_1
	cg_1:
		;
		aaia++
	}

	Xmpfr_clear(cgtls, cgbp+64)
}

// C documentation
//
//	/* Test case bk == 0 in add1.c (b has entirely been read and
//	   c hasn't been taken into account). */
func sicoverage_bk_eq_0(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aainex ppint32
	var ccv1 ppbool
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	pp_, pp_ = aainex, ccv1

	Xmpfr_init2(cgtls, cgbp, ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))

	Xmpfr_set_ui_2exp(cgtls, cgbp+32, ppuint64(1), ppint64(iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))), ppint32(ecMPFR_RNDN))
	Xmpfr_sub_ui(cgtls, cgbp+32, cgbp+32, ppuint64(1), ppint32(ecMPFR_RNDN))
	/* b = 111...111 (in base 2) where the 1's fit 2 whole limbs */

	Xmpfr_set_ui_2exp(cgtls, cgbp+64, ppuint64(1), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN)) /* c = 1/2 */

	aainex = Xmpfr_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))
	Xmpfr_set_ui_2exp(cgtls, cgbp+64, ppuint64(1), ppint64(iqlibc.ppInt32FromInt32(2)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))), ppint32(ecMPFR_RNDN))
	if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in coverage_bk_eq_0\n\x00", 0)
		Xprintf(cgtls, "Expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xprintf(cgtls, "Got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(892), "inex > 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
}

func sitests(cgtls *iqlibc.ppTLS) {

	sicheck_alloc(cgtls)
	sicheck_nans(cgtls)
	sicheck_inexact(cgtls)
	sicheck_case_1b(cgtls)
	sicheck_case_2(cgtls)
	sicheck64(cgtls)
	sicoverage_bk_eq_0(cgtls)

	sicheck(cgtls, "293607738.0\x00", "1.9967571564050541e-5\x00", ppint32(ecMPFR_RNDU), ppuint32(64), ppuint32(53), ppuint32(53), "2.9360773800002003e8\x00")
	sicheck(cgtls, "880524.0\x00", "-2.0769715792901673e-5\x00", ppint32(ecMPFR_RNDN), ppuint32(64), ppuint32(53), ppuint32(53), "8.8052399997923023e5\x00")
	sicheck(cgtls, "1196426492.0\x00", "-1.4218093058435347e-3\x00", ppint32(ecMPFR_RNDN), ppuint32(64), ppuint32(53), ppuint32(53), "1.1964264919985781e9\x00")
	sicheck(cgtls, "982013018.0\x00", "-8.941829477291838e-7\x00", ppint32(ecMPFR_RNDN), ppuint32(64), ppuint32(53), ppuint32(53), "9.8201301799999905e8\x00")
	sicheck(cgtls, "1092583421.0\x00", "1.0880649218158844e9\x00", ppint32(ecMPFR_RNDN), ppuint32(64), ppuint32(53), ppuint32(53), "2.1806483428158846e9\x00")
	sicheck(cgtls, "1.8476886419022969e-6\x00", "961494401.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(64), ppuint32(53), "9.6149440100000179e8\x00")
	sicheck(cgtls, "-2.3222118418069868e5\x00", "1229318102.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(64), ppuint32(53), "1.2290858808158193e9\x00")
	sicheck(cgtls, "-3.0399171300395734e-6\x00", "874924868.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(64), ppuint32(53), "8.749248679999969e8\x00")
	sicheck(cgtls, "9.064246624706179e1\x00", "663787413.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(64), ppuint32(53), "6.6378750364246619e8\x00")
	sicheck(cgtls, "-1.0954322421551264e2\x00", "281806592.0\x00", ppint32(ecMPFR_RNDD), ppuint32(53), ppuint32(64), ppuint32(53), "2.8180648245677572e8\x00")
	sicheck(cgtls, "5.9836930386056659e-8\x00", "1016217213.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(64), ppuint32(53), "1.0162172130000001e9\x00")
	sicheck(cgtls, "-1.2772161928500301e-7\x00", "1237734238.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(64), ppuint32(53), "1.2377342379999998e9\x00")
	sicheck(cgtls, "-4.567291988483277e8\x00", "1262857194.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(64), ppuint32(53), "8.0612799515167236e8\x00")
	sicheck(cgtls, "4.7719471752925262e7\x00", "196089880.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "2.4380935175292528e8\x00")
	sicheck(cgtls, "4.7719471752925262e7\x00", "196089880.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(64), ppuint32(53), "2.4380935175292528e8\x00")
	sicheck(cgtls, "-1.716113812768534e-140\x00", "1271212614.0\x00", ppint32(ecMPFR_RNDZ), ppuint32(53), ppuint32(64), ppuint32(53), "1.2712126139999998e9\x00")
	sicheck(cgtls, "-1.2927455200185474e-50\x00", "1675676122.0\x00", ppint32(ecMPFR_RNDD), ppuint32(53), ppuint32(64), ppuint32(53), "1.6756761219999998e9\x00")

	sicheck(cgtls, "1.22191250737771397120e+20\x00", "948002822.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "122191250738719408128.0\x00")
	sicheck(cgtls, "9966027674114492.0\x00", "1780341389094537.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "11746369063209028.0\x00")
	sicheck(cgtls, "2.99280481918991653800e+272\x00", "5.34637717585790933424e+271\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "3.5274425367757071711e272\x00")
	sicheck_same(cgtls)
	sicheck(cgtls, "6.14384195492641560499e-02\x00", "-6.14384195401037683237e-02\x00", ppint32(ecMPFR_RNDU), ppuint32(53), ppuint32(53), ppuint32(53), "9.1603877261370314499e-12\x00")
	sicheck(cgtls, "1.16809465359248765399e+196\x00", "7.92883212101990665259e+196\x00", ppint32(ecMPFR_RNDU), ppuint32(53), ppuint32(53), ppuint32(53), "9.0969267746123943065e196\x00")
	sicheck(cgtls, "3.14553393112021279444e-67\x00", "3.14553401015952024126e-67\x00", ppint32(ecMPFR_RNDU), ppuint32(53), ppuint32(53), ppuint32(53), "6.2910679412797336946e-67\x00")

	sicheck(cgtls, "5.43885304644369509058e+185\x00", "-1.87427265794105342763e-57\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "5.4388530464436950905e185\x00")
	sicheck(cgtls, "5.43885304644369509058e+185\x00", "-1.87427265794105342763e-57\x00", ppint32(ecMPFR_RNDZ), ppuint32(53), ppuint32(53), ppuint32(53), "5.4388530464436944867e185\x00")
	sicheck(cgtls, "5.43885304644369509058e+185\x00", "-1.87427265794105342763e-57\x00", ppint32(ecMPFR_RNDU), ppuint32(53), ppuint32(53), ppuint32(53), "5.4388530464436950905e185\x00")
	sicheck(cgtls, "5.43885304644369509058e+185\x00", "-1.87427265794105342763e-57\x00", ppint32(ecMPFR_RNDD), ppuint32(53), ppuint32(53), ppuint32(53), "5.4388530464436944867e185\x00")

	sicheck2b(cgtls, "1.001010101110011000000010100101110010111001010000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e358\x00", ppint32(187), "-1.11100111001101100010001111111110101101110001000000000000000000000000000000000000000000e160\x00", ppint32(87), "1.001010101110011000000010100101110010111001010000000111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e358\x00", ppint32(178), ppint32(ecMPFR_RNDD))
	sicheck2b(cgtls, "-1.111100100011100111010101010101001010100100000111001000000000000000000e481\x00", ppint32(70), "1.1111000110100011110101111110110010010000000110101000000000000000e481\x00", ppint32(65), "-1.001010111111101011010000001100011101100101000000000000000000e472\x00", ppint32(61), ppint32(ecMPFR_RNDD))
	sicheck2b(cgtls, "1.0100010111010000100101000000111110011100011001011010000000000000000000000000000000e516\x00", ppint32(83), "-1.1001111000100001011100000001001100110011110010111111000000e541\x00", ppint32(59), "-1.1001111000100001011011110111000001001011100000011110100000110001110011010011000000000000000000000000000000000000000000000000e541\x00", ppint32(125), ppint32(ecMPFR_RNDZ))
	sicheck2b(cgtls, "-1.0010111100000100110001011011010000000011000111101000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e261\x00", ppint32(155), "-1.00111110100011e239\x00", ppint32(15), "-1.00101111000001001100101010101110001100110001111010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e261\x00", ppint32(159), ppint32(ecMPFR_RNDD))
	sicheck2b(cgtls, "-1.110111000011111011000000001001111101101001010100111000000000000000000000000e880\x00", ppint32(76), "-1.1010010e-634\x00", ppint32(8), "-1.11011100001111101100000000100111110110100101010011100000000000000000000000e880\x00", ppint32(75), ppint32(ecMPFR_RNDZ))
	sicheck2b(cgtls, "1.00100100110110101001010010101111000001011100100101010000000000000000000000000000e-530\x00", ppint32(81), "-1.101101111100000111000011001010110011001011101001110100000e-908\x00", ppint32(58), "1.00100100110110101001010010101111000001011100100101010e-530\x00", ppint32(54), ppint32(ecMPFR_RNDN))
	sicheck2b(cgtls, "1.0101100010010111101000000001000010010010011000111011000000000000000000000000000000000000000000000000000000000000000000e374\x00", ppint32(119), "1.11100101100101e358\x00", ppint32(15), "1.01011000100110011000010110100100100100100110001110110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e374\x00", ppint32(150), ppint32(ecMPFR_RNDZ))
	sicheck2b(cgtls, "-1.10011001000010100000010100100110110010011111101111000000000000000000000000000000000000000000000000000000000000000000e-172\x00", ppint32(117), "1.111011100000101010110000100100110100100001001000011100000000e-173\x00", ppint32(61), "-1.0100010000001001010110011011101001001011101011110001000000000000000e-173\x00", ppint32(68), ppint32(ecMPFR_RNDZ))
	sicheck2b(cgtls, "-1.011110000111101011100001100110100011100101000011011000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e-189\x00", ppint32(175), "1.1e631\x00", ppint32(2), "1.011111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e631\x00", ppint32(115), ppint32(ecMPFR_RNDZ))
	sicheck2b(cgtls, "-1.101011101001011101100011001000001100010100001101011000000000000000000000000000000000000000000e-449\x00", ppint32(94), "-1.01111101010111000011000110011101000111001100110111100000000000000e-429\x00", ppint32(66), "-1.01111101010111000100110010000110100100101111111111101100010100001101011000000000000000000000000000000000000000e-429\x00", ppint32(111), ppint32(ecMPFR_RNDU))
	sicheck2b(cgtls, "-1.101011101001011101100011001000001100010100001101011000000000000000000000000000000000000000000e-449\x00", ppint32(94), "-1.01111101010111000011000110011101000111001100110111100000000000000e-429\x00", ppint32(66), "-1.01111101010111000100110010000110100100101111111111101100010100001101011000000000000000000000000000000000000000e-429\x00", ppint32(111), ppint32(ecMPFR_RNDD))
	sicheck2b(cgtls, "-1.1001000011101000110000111110010100100101110101111100000000000000000000000000000000000000000000000000000000e-72\x00", ppint32(107), "-1.001100011101100100010101101010101011010010111111010000000000000000000000000000e521\x00", ppint32(79), "-1.00110001110110010001010110101010101101001011111101000000000000000000000000000000000000000000000001e521\x00", ppint32(99), ppint32(ecMPFR_RNDD))
	sicheck2b(cgtls, "-1.01010001111000000101010100100100110101011011100001110000000000e498\x00", ppint32(63), "1.010000011010101111000100111100011100010101011110010100000000000e243\x00", ppint32(64), "-1.010100011110000001010101001001001101010110111000011100000000000e498\x00", ppint32(64), ppint32(ecMPFR_RNDN))
	sicheck2b(cgtls, "1.00101100010101000011010000011000111111011110010111000000000000000000000000000000000000000000000000000000000e178\x00", ppint32(108), "-1.10101101010101000110011011111001001101111111110000100000000e160\x00", ppint32(60), "1.00101100010100111100100011000011111001000010011101110010000000001111100000000000000000000000000000000000e178\x00", ppint32(105), ppint32(ecMPFR_RNDN))
	sicheck2b(cgtls, "1.00110011010100111110011010110100111101110101100100110000000000000000000000000000000000000000000000e559\x00", ppint32(99), "-1.011010110100111011100110100110011100000000111010011000000000000000e559\x00", ppint32(67), "-1.101111111101011111111111001001100100011100001001100000000000000000000000000000000000000000000e556\x00", ppint32(94), ppint32(ecMPFR_RNDU))
	sicheck2b(cgtls, "-1.100000111100101001100111011100011011000001101001111100000000000000000000000000e843\x00", ppint32(79), "-1.1101101010110000001001000100001100110011000110110111000000000000000000000000000000000000000000e414\x00", ppint32(95), "-1.1000001111001010011001110111000110110000011010100000e843\x00", ppint32(53), ppint32(ecMPFR_RNDD))
	sicheck2b(cgtls, "-1.110110010110100010100011000110111001010000010111110000000000e-415\x00", ppint32(61), "-1.0000100101100001111100110011111111110100011101101011000000000000000000e751\x00", ppint32(71), "-1.00001001011000011111001100111111111101000111011010110e751\x00", ppint32(54), ppint32(ecMPFR_RNDN))
	sicheck2b(cgtls, "-1.1011011011110001001101010101001000010100010110111101000000000000000000000e258\x00", ppint32(74), "-1.00011100010110110101001011000100100000100010101000010000000000000000000000000000000000000000000000e268\x00", ppint32(99), "-1.0001110011001001000011110001000111010110101011110010011011110100000000000000000000000000000000000000e268\x00", ppint32(101), ppint32(ecMPFR_RNDD))
	sicheck2b(cgtls, "-1.1011101010011101011000000100100110101101101110000001000000000e629\x00", ppint32(62), "1.111111100000011100100011100000011101100110111110111000000000000000000000000000000000000000000e525\x00", ppint32(94), "-1.101110101001110101100000010010011010110110111000000011111111111111111111111111111111111111111111111111101e629\x00", ppint32(106), ppint32(ecMPFR_RNDD))
	sicheck2b(cgtls, "1.111001000010001100010000001100000110001011110111011000000000000000000000000000000000000e152\x00", ppint32(88), "1.111110111001100100000100111111010111000100111111001000000000000000e152\x00", ppint32(67), "1.1110111111011110000010101001011011101010000110110100e153\x00", ppint32(53), ppint32(ecMPFR_RNDN))
	sicheck2b(cgtls, "1.000001100011110010110000110100001010101101111011110100e696\x00", ppint32(55), "-1.1011001111011100100001011110100101010101110111010101000000000000000000000000000000000000000000000000000000000000e730\x00", ppint32(113), "-1.1011001111011100100001011110100100010100010011100010e730\x00", ppint32(53), ppint32(ecMPFR_RNDN))
	sicheck2b(cgtls, "-1.11010111100001001111000001110101010010001111111001100000000000000000000000000000000000000000000000000000000000e530\x00", ppint32(111), "1.01110100010010000000010110111101011101000001111101100000000000000000000000000000000000000000000000e530\x00", ppint32(99), "-1.1000110011110011101010101101111101010011011111000000000000000e528\x00", ppint32(62), ppint32(ecMPFR_RNDD))
	sicheck2b(cgtls, "-1.0001100010010100111101101011101000100100010011100011000000000000000000000000000000000000000000000000000000000e733\x00", ppint32(110), "-1.001000000111110010100101010100110111001111011011001000000000000000000000000000000000000000000000000000000000e710\x00", ppint32(109), "-1.000110001001010011111000111110110001110110011000110110e733\x00", ppint32(55), ppint32(ecMPFR_RNDN))
	sicheck2b(cgtls, "-1.1101011110000100111100000111010101001000111111100110000000000000000000000e530\x00", ppint32(74), "1.01110100010010000000010110111101011101000001111101100000000000000000000000000000000000000000000000000000000000e530\x00", ppint32(111), "-1.10001100111100111010101011011111010100110111110000000000000000000000000000e528\x00", ppint32(75), ppint32(ecMPFR_RNDU))
	sicheck2b(cgtls, "1.00110011010100111110011010110100111101110101100100110000000000000000000000000000000000000000000000e559\x00", ppint32(99), "-1.011010110100111011100110100110011100000000111010011000000000000000e559\x00", ppint32(67), "-1.101111111101011111111111001001100100011100001001100000000000000000000000000000000000000000000e556\x00", ppint32(94), ppint32(ecMPFR_RNDU))
	sicheck2b(cgtls, "-1.100101111110110000000110111111011010011101101111100100000000000000e-624\x00", ppint32(67), "1.10111010101110100000010110101000000000010011100000100000000e-587\x00", ppint32(60), "1.1011101010111010000001011010011111110100011110001011111111001000000100101100010010000011100000000000000000000e-587\x00", ppint32(110), ppint32(ecMPFR_RNDU))
	sicheck2b(cgtls, "-1.10011001000010100000010100100110110010011111101111000000000000000000000000000000000000000000000000000000000000000000e-172\x00", ppint32(117), "1.111011100000101010110000100100110100100001001000011100000000e-173\x00", ppint32(61), "-1.0100010000001001010110011011101001001011101011110001000000000000000e-173\x00", ppint32(68), ppint32(ecMPFR_RNDZ))
	sicheck2b(cgtls, "1.1000111000110010101001010011010011101100010110001001000000000000000000000000000000000000000000000000e167\x00", ppint32(101), "1.0011110010000110000000101100100111000001110110110000000000000000000000000e167\x00", ppint32(74), "1.01100101010111000101001111111111010101110001100111001000000000000000000000000000000000000000000000000000e168\x00", ppint32(105), ppint32(ecMPFR_RNDZ))
	sicheck2b(cgtls, "1.100101111111110010100101110111100001110000100001010000000000000000000000000000000000000000000000e808\x00", ppint32(97), "-1.1110011001100000100000111111110000110010100111001011000000000000000000000000000000e807\x00", ppint32(83), "1.01001001100110001100011111000000000001011010010111010000000000000000000000000000000000000000000e807\x00", ppint32(96), ppint32(ecMPFR_RNDN))
	sicheck2b(cgtls, "1e128\x00", ppint32(128), "1e0\x00", ppint32(128), "100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001e0\x00", ppint32(256), ppint32(ecMPFR_RNDN))
	sicheck2b(cgtls, "0.10000000000000000011101111111110110110001100010110100011010000110101101101010010000110110011100110001101110010111011011110010101e1\x00", ppint32(128), "0.11110011001100100001111011100010010100011011011111111110101001101001101011100101011110101111111100010010100010100111110110011001e-63\x00", ppint32(128), "0.10000000000000000011101111111110110110001100010110100011010001000100111010000100001110100001101111011111100000111011011000111100e1\x00", ppint32(128), ppint32(ecMPFR_RNDN))
	sicheck2b(cgtls, "0.101000111001110100111100000010110010011101001011101001001101101000010100010000011100110110110011011000001110110101100111010110011101000111010011110001010001110110110011111011011100101011111100e-2\x00", ppint32(192), "0.101101100000111110011110000110001000110110010010110000111101110010111010100110000000000110010100010001000001011011101010111010101011010110011011011110110111001000111001101000010101111010010010e-130\x00", ppint32(192), "0.101000111001110100111100000010110010011101001011101001001101101000010100010000011100110110110011011000001110110101100111010110101000011111100011011000110011011001000001100000001000111011011001e-2\x00", ppint32(192), ppint32(ecMPFR_RNDN))

	/* Checking double precision (53 bits) */
	sicheck(cgtls, "-8.22183238641455905806e-19\x00", "7.42227178769761587878e-19\x00", ppint32(ecMPFR_RNDD), ppuint32(53), ppuint32(53), ppuint32(53), "-7.9956059871694317927e-20\x00")
	sicheck(cgtls, "5.82106394662028628236e+234\x00", "-5.21514064202368477230e+89\x00", ppint32(ecMPFR_RNDD), ppuint32(53), ppuint32(53), ppuint32(53), "5.8210639466202855763e234\x00")
	sicheck(cgtls, "5.72931679569871602371e+122\x00", "-5.72886070363264321230e+122\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "4.5609206607281141508e118\x00")
	sicheck(cgtls, "-5.09937369394650450820e+238\x00", "2.70203299854862982387e+250\x00", ppint32(ecMPFR_RNDD), ppuint32(53), ppuint32(53), ppuint32(53), "2.7020329985435301323e250\x00")
	sicheck(cgtls, "-2.96695924472363684394e+27\x00", "1.22842938251111500000e+16\x00", ppint32(ecMPFR_RNDD), ppuint32(53), ppuint32(53), ppuint32(53), "-2.96695924471135255027e27\x00")
	sicheck(cgtls, "1.74693641655743793422e-227\x00", "-7.71776956366861843469e-229\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "1.669758720920751867e-227\x00")
	/*  x = -7883040437021647.0; for (i=0; i<468; i++) x = x / 2.0;*/
	sicheck(cgtls, "-1.03432206392780011159e-125\x00", "1.30127034799251347548e-133\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "-1.0343220509150965661100887242027378881805094180354e-125\x00")
	sicheck(cgtls, "1.05824655795525779205e+71\x00", "-1.06022698059744327881e+71\x00", ppint32(ecMPFR_RNDZ), ppuint32(53), ppuint32(53), ppuint32(53), "-1.9804226421854867632e68\x00")
	sicheck(cgtls, "-5.84204911040921732219e+240\x00", "7.26658169050749590763e+240\x00", ppint32(ecMPFR_RNDD), ppuint32(53), ppuint32(53), ppuint32(53), "1.4245325800982785854e240\x00")
	sicheck(cgtls, "1.00944884131046636376e+221\x00", "2.33809162651471520268e+215\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "1.0094511794020929787e221\x00")
	/*x = 7045852550057985.0; for (i=0; i<986; i++) x = x / 2.0;*/
	sicheck(cgtls, "4.29232078932667367325e-278\x00", "1.0773525047389793833221116707010783793203080117586e-281\x00", ppint32(ecMPFR_RNDU), ppuint32(53), ppuint32(53), ppuint32(53), "4.2933981418314132787e-278\x00")
	sicheck(cgtls, "5.27584773801377058681e-80\x00", "8.91207657803547196421e-91\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "5.2758477381028917269e-80\x00")
	sicheck(cgtls, "2.99280481918991653800e+272\x00", "5.34637717585790933424e+271\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "3.5274425367757071711e272\x00")
	sicheck(cgtls, "4.67302514390488041733e-184\x00", "2.18321376145645689945e-190\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "4.6730273271186420541e-184\x00")
	sicheck(cgtls, "5.57294120336300389254e+71\x00", "2.60596167942024924040e+65\x00", ppint32(ecMPFR_RNDZ), ppuint32(53), ppuint32(53), ppuint32(53), "5.5729438093246831053e71\x00")
	sicheck(cgtls, "6.6052588496951015469e24\x00", "4938448004894539.0\x00", ppint32(ecMPFR_RNDU), ppuint32(53), ppuint32(53), ppuint32(53), "6.6052588546335505068e24\x00")
	sicheck(cgtls, "1.23056185051606761523e-190\x00", "1.64589756643433857138e-181\x00", ppint32(ecMPFR_RNDU), ppuint32(53), ppuint32(53), ppuint32(53), "1.6458975676649006598e-181\x00")
	sicheck(cgtls, "2.93231171510175981584e-280\x00", "3.26266919161341483877e-273\x00", ppint32(ecMPFR_RNDU), ppuint32(53), ppuint32(53), ppuint32(53), "3.2626694848445867288e-273\x00")
	sicheck(cgtls, "5.76707395945001907217e-58\x00", "4.74752971449827687074e-51\x00", ppint32(ecMPFR_RNDD), ppuint32(53), ppuint32(53), ppuint32(53), "4.747530291205672325e-51\x00")
	sicheck(cgtls, "277363943109.0\x00", "11.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "277363943120.0\x00")
	sicheck(cgtls, "1.44791789689198883921e-140\x00", "-1.90982880222349071284e-121\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "-1.90982880222349071e-121\x00")

	/* tests for particular cases (Vincent Lefevre, 22 Aug 2001) */
	sicheck(cgtls, "9007199254740992.0\x00", "1.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "9007199254740992.0\x00")
	sicheck(cgtls, "9007199254740994.0\x00", "1.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "9007199254740996.0\x00")
	sicheck(cgtls, "9007199254740992.0\x00", "-1.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "9007199254740991.0\x00")
	sicheck(cgtls, "9007199254740994.0\x00", "-1.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "9007199254740992.0\x00")
	sicheck(cgtls, "9007199254740996.0\x00", "-1.0\x00", ppint32(ecMPFR_RNDN), ppuint32(53), ppuint32(53), ppuint32(53), "9007199254740996.0\x00")

	sicheck_overflow(cgtls)
	sicheck_1111(cgtls)
	sicheck_1minuseps(cgtls)
}

func sicheck_extreme(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(208)
	defer cgtls.ppFree(208)

	var aai, aainex, aar ppint32
	var ccv3 ppbool
	var pp_ /* u at bp+0 */ tnmpfr_t
	var pp_ /* v at bp+32 */ tnmpfr_t
	var pp_ /* w at bp+64 */ tnmpfr_t
	var pp_ /* x at bp+96 */ tnmpfr_t
	var pp_ /* y at bp+128 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aai, aainex, aar, ccv3

	Xmpfr_inits2(cgtls, ppint64(32), cgbp, iqlibc.ppVaList(cgbp+168, cgbp+32, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_setmin(cgtls, cgbp, X__gmpfr_emax)
	Xmpfr_setmax(cgtls, cgbp+32, X__gmpfr_emin)
	Xmpfr_setmin(cgtls, cgbp+64, X__gmpfr_emax-ppint64(40))
	aar = 0
	for {
		if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}
		aai = 0
		for {
			if !(aai < ppint32(2)) {
				break
			}

			Xmpfr_add(cgtls, cgbp+96, cgbp, cgbp+32, aar)
			Xmpfr_set_prec(cgtls, cgbp+128, ppint64(64))
			aainex = Xmpfr_add(cgtls, cgbp+128, cgbp, cgbp+64, ppint32(ecMPFR_RNDN))

			if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv3 {
				Xmpfr_assert_fail(cgtls, "tadd.c\x00", ppint32(1173), "inex == 0\x00")
			}
			pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			Xmpfr_prec_round(cgtls, cgbp+128, ppint64(32), aar)
			/* for RNDF, the rounding directions are not necessarily consistent */
			if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+128) != 0) && aar != ppint32(ecMPFR_RNDF) {

				Xprintf(cgtls, "Error in u + v (%s)\n\x00", iqlibc.ppVaList(cgbp+168, Xmpfr_print_rnd_mode(cgtls, aar)))
				Xprintf(cgtls, "u = \x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "v = \x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xprintf(cgtls, "Expected \x00", 0)
				Xmpfr_dump(cgtls, cgbp+128)
				Xprintf(cgtls, "Got      \x00", 0)
				Xmpfr_dump(cgtls, cgbp+96)
				Xexit(cgtls, ppint32(1))
			}
			Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))
			Xmpfr_neg(cgtls, cgbp+64, cgbp+64, ppint32(ecMPFR_RNDN))

			goto cg_2
		cg_2:
			;
			aai++
		}
		goto cg_1
	cg_1:
		;
		aar++
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+168, cgbp+32, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
}

func sitestall_rndf(cgtls *iqlibc.ppTLS, aapmax tnmpfr_prec_t) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaeb tnmpfr_exp_t
	var aapa, aapb, aapc tnmpfr_prec_t
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aaeb, aapa, aapb, aapc

	aapa = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aapa <= aapmax) {
			break
		}

		Xmpfr_init2(cgtls, cgbp, aapa)
		Xmpfr_init2(cgtls, cgbp+96, aapa)
		aapb = ppint64(mvMPFR_PREC_MIN)
		for {
			if !(aapb <= aapmax) {
				break
			}

			Xmpfr_init2(cgtls, cgbp+32, aapb)
			aaeb = 0
			for {
				if !(aaeb <= aapmax+ppint64(3)) {
					break
				}

				Xmpfr_set_ui_2exp(cgtls, cgbp+32, ppuint64(1), aaeb, ppint32(ecMPFR_RNDN))
				for Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, ppuint64(1), aaeb+ppint64(1)) < 0 {

					aapc = ppint64(mvMPFR_PREC_MIN)
					for {
						if !(aapc <= aapmax) {
							break
						}

						Xmpfr_init2(cgtls, cgbp+64, aapc)
						pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
						for Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) < 0 {

							Xmpfr_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDF))
							Xmpfr_add(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD))
							if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

								Xmpfr_add(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))
								if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

									Xprintf(cgtls, "Error: mpfr_add(a,b,c,RNDF) does not match RNDD/RNDU\n\x00", 0)
									Xprintf(cgtls, "b=\x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xprintf(cgtls, "c=\x00", 0)
									Xmpfr_dump(cgtls, cgbp+64)
									Xprintf(cgtls, "a=\x00", 0)
									Xmpfr_dump(cgtls, cgbp)
									Xexit(cgtls, ppint32(1))
								}
							}
							Xmpfr_nextabove(cgtls, cgbp+64)
						}
						Xmpfr_clear(cgtls, cgbp+64)

						goto cg_4
					cg_4:
						;
						aapc++
					}
					Xmpfr_nextabove(cgtls, cgbp+32)
				}

				goto cg_3
			cg_3:
				;
				aaeb++
			}
			Xmpfr_clear(cgtls, cgbp+32)

			goto cg_2
		cg_2:
			;
			aapb++
		}
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+96)

		goto cg_1
	cg_1:
		;
		aapa++
	}
}

func sitest_rndf_exact(cgtls *iqlibc.ppTLS, aapmax tnmp_size_t) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaeb tnmpfr_exp_t
	var aapa, aapb, aapc tnmpfr_prec_t
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aaeb, aapa, aapb, aapc

	aapa = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aapa <= aapmax) {
			break
		}

		/* only check pa mod GMP_NUMB_BITS = -2, -1, 0, 1, 2 */
		if (aapa+ppint64(2))%ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)) > ppint64(4) {
			goto cg_1
		}
		Xmpfr_init2(cgtls, cgbp, aapa)
		Xmpfr_init2(cgtls, cgbp+96, aapa)
		aapb = ppint64(mvMPFR_PREC_MIN)
		for {
			if !(aapb <= aapmax) {
				break
			}

			if (aapb+ppint64(2))%ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)) > ppint64(4) {
				goto cg_2
			}
			Xmpfr_init2(cgtls, cgbp+32, aapb)
			aaeb = 0
			for {
				if !(aaeb <= aapmax+ppint64(3)) {
					break
				}

				if !(Xmpfr_rands_initialized != 0) {
					Xmpfr_rands_initialized = ppuint8(1)
					X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
					pp_ = iqlibc.ppInt32FromInt32(0)
				}
				Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				Xmpfr_mul_2ui(cgtls, cgbp+32, cgbp+32, iqlibc.ppUint64FromInt64(aaeb), ppint32(ecMPFR_RNDN))
				aapc = ppint64(mvMPFR_PREC_MIN)
				for {
					if !(aapc <= aapmax) {
						break
					}

					if (aapc+ppint64(2))%ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)) > ppint64(4) {
						goto cg_5
					}
					Xmpfr_init2(cgtls, cgbp+64, aapc)
					if !(Xmpfr_rands_initialized != 0) {
						Xmpfr_rands_initialized = ppuint8(1)
						X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
						pp_ = iqlibc.ppInt32FromInt32(0)
					}
					Xmpfr_urandomb(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
					Xmpfr_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDF))
					Xmpfr_add(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD))
					if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

						Xmpfr_add(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))
						if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

							Xprintf(cgtls, "Error: mpfr_add(a,b,c,RNDF) does not match RNDD/RNDU\n\x00", 0)
							Xprintf(cgtls, "b=\x00", 0)
							Xmpfr_dump(cgtls, cgbp+32)
							Xprintf(cgtls, "c=\x00", 0)
							Xmpfr_dump(cgtls, cgbp+64)
							Xprintf(cgtls, "a=\x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							Xexit(cgtls, ppint32(1))
						}
					}

					/* now make the low bits from c match those from b */
					Xmpfr_sub(cgtls, cgbp+64, cgbp+32, cgbp+96, ppint32(ecMPFR_RNDN))
					Xmpfr_add(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDF))
					Xmpfr_add(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD))
					if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

						Xmpfr_add(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))
						if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

							Xprintf(cgtls, "Error: mpfr_add(a,b,c,RNDF) does not match RNDD/RNDU\n\x00", 0)
							Xprintf(cgtls, "b=\x00", 0)
							Xmpfr_dump(cgtls, cgbp+32)
							Xprintf(cgtls, "c=\x00", 0)
							Xmpfr_dump(cgtls, cgbp+64)
							Xprintf(cgtls, "a=\x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							Xexit(cgtls, ppint32(1))
						}
					}

					Xmpfr_clear(cgtls, cgbp+64)

					goto cg_5
				cg_5:
					;
					aapc++
				}

				goto cg_3
			cg_3:
				;
				aaeb++
			}
			Xmpfr_clear(cgtls, cgbp+32)

			goto cg_2
		cg_2:
			;
			aapb++
		}
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+96)

		goto cg_1
	cg_1:
		;
		aapa++
	}
}

/* Generic test file for functions with one or two arguments (the second being
   either mpfr_t or double or unsigned long).

Copyright 2001-2023 Free Software Foundation, Inc.
Contributed by the AriC and Caramba projects, INRIA.

This file is part of the GNU MPFR Library.

The GNU MPFR Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The GNU MPFR Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

/* Define TWO_ARGS for two-argument functions like mpfr_pow.
   Define DOUBLE_ARG1 or DOUBLE_ARG2 for function with a double operand in
   first or second place like sub_d or d_sub.
   Define ULONG_ARG1 or ULONG_ARG2 for function with an unsigned long
   operand in first or second place like sub_ui or ui_sub.
   Define THREE_ARGS for three-argument functions like mpfr_atan2u. */

/* TODO: Add support for type long and extreme integer values, as done
   in tgeneric_ui.c; then tgeneric_ui.c could probably disappear. */

/* For the random function: one number on two is negative. */

/* For the random function: one number on two is negative. */

/* If the MPFR_SUSPICIOUS_OVERFLOW test fails but this is not a bug,
   then define TGENERIC_SO_TEST with an adequate test (possibly 0) to
   omit this particular case. */

/* For some functions (for example cos), the argument reduction is too
   expensive when using mpfr_get_emax(). Then simply define REDUCE_EMAX
   to some reasonable value before including tgeneric.c. */

/* same for mpfr_get_emin() */

func sitest_generic(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aanmax ppuint32) {
	cgbp := cgtls.ppAlloc(320)
	defer cgtls.ppFree(320)

	var aa_p, aa_p1, aa_p2, aa_p3 tnmpfr_ptr
	var aa_p4 tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, aainfinite_input, aatest_of, aatest_uf, ccv10, ccv13, ccv15, ccv16, ccv17, ccv19, ccv21, ccv24, ccv26, ccv27, ccv28, ccv30, ccv32, ccv35, ccv37, ccv38, ccv39, ccv41, ccv43, ccv46, ccv48, ccv49, ccv50, ccv54, ccv8 ppint32
	var aactrn, aactrt ppuint64
	var aae, aaemax, aaemin, aaoemax, aaoemin, aaold_emax, aaold_emin tnmpfr_exp_t
	var aaex_flags, aaex_flags1, aaflags, aaoldflags tnmpfr_flags_t
	var aan, ccv3 ppuint32
	var aaprec, aaxprec, aayprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv11, ccv14, ccv18, ccv22, ccv25, ccv29, ccv33, ccv36, ccv40, ccv44, ccv47, ccv51, ccv52, ccv53, ccv55, ccv56, ccv57, ccv58, ccv7 ppbool
	var ccv4 ppfloat64
	var pp_ /* t at bp+96 */ tnmpfr_t
	var pp_ /* w at bp+128 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* x2 at bp+224 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* yd at bp+160 */ tnmpfr_t
	var pp_ /* yu at bp+192 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aa_p2, aa_p3, aa_p4, aacompare, aacompare2, aactrn, aactrt, aae, aaemax, aaemin, aaex_flags, aaex_flags1, aaflags, aainexact, aainfinite_input, aan, aaoemax, aaoemin, aaold_emax, aaold_emin, aaoldflags, aaprec, aarnd, aatest_of, aatest_uf, aaxprec, aayprec, ccv10, ccv11, ccv13, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv21, ccv22, ccv24, ccv25, ccv26, ccv27, ccv28, ccv29, ccv3, ccv30, ccv32, ccv33, ccv35, ccv36, ccv37, ccv38, ccv39, ccv4, ccv40, ccv41, ccv43, ccv44, ccv46, ccv47, ccv48, ccv49, ccv50, ccv51, ccv52, ccv53, ccv54, ccv55, ccv56, ccv57, ccv58, ccv7, ccv8
	aactrt = ppuint64(0)
	aactrn = ppuint64(0)

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	Xmpfr_inits2(cgtls, ppint64(mvMPFR_PREC_MIN), cgbp, iqlibc.ppVaList(cgbp+264, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_init2(cgtls, cgbp+224, ppint64(mvMPFR_PREC_MIN))

	/* generic tests */
	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		/* Number of overflow/underflow tests for each precision.
		   Since MPFR uses several algorithms and there may also be
		   early overflow/underflow detection, several tests may be
		   needed to detect a bug. */
		aatest_of = ppint32(3)
		aatest_uf = ppint32(3)

		Xmpfr_set_prec(cgtls, cgbp+64, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+96, aaprec)
		aayprec = aaprec + ppint64(20)
		Xmpfr_set_prec(cgtls, cgbp+32, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+160, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+192, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+128, aayprec)

		/* Note: in precision p1, we test NSPEC special cases. */
		aan = ppuint32(0)
		for {
			if aaprec == aap1 {
				ccv3 = aanmax + ppuint32(9)
			} else {
				ccv3 = aanmax
			}
			if !(aan < ccv3) {
				break
			}

			aainfinite_input = 0

			aaxprec = aaprec
			if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {

				/* In half cases, modify the precision of the inputs:
				   If the base precision (for the result) is small,
				   take a larger input precision in general, else
				   take a smaller precision. */
				if aaprec < ppint64(16) {
					ccv4 = ppfloat64(256)
				} else {
					ccv4 = ppfloat64(1)
				}
				aaxprec = tnmpfr_prec_t(ppfloat64(aaxprec) * (ccv4 * ppfloat64(Xrandlimb(cgtls)) / ppfloat64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1)))))
				if aaxprec < ppint64(mvMPFR_PREC_MIN) {
					aaxprec = ppint64(mvMPFR_PREC_MIN)
				}
			}
			Xmpfr_set_prec(cgtls, cgbp, aaxprec)
			Xmpfr_set_prec(cgtls, cgbp+224, aaxprec)

			/* Generate random arguments, even in the special cases
			   (this may not be needed, but this is simpler).
			   Note that if RAND_FUNCTION is defined, this specific
			   random function is used for all arguments; this is
			   typically mpfr_random2, which generates a positive
			   random mpfr_t with long runs of consecutive ones and
			   zeros in the binary representation. */

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_random2(cgtls, cgbp, ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt64FromInt32(1), iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%ppuint64(100)), ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_random2(cgtls, cgbp+224, ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt64FromInt32(1), iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%ppuint64(100)), ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))

			if aan < ppuint32(9) && aaprec == aap1 {

				/* Special cases tested in precision p1 if n < NSPEC. They are
				   useful really in the extended exponent range. */
				/* TODO: x2 is set even when it is associated with a double;
				   check whether this really makes sense. */
				Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
				Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
				if aan == ppuint32(0) {

					Xmpfr_set_nan(cgtls, cgbp)
				} else {
					if aan <= ppuint32(2) {

						if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(1) || aan == ppuint32(2))), ppint64(1)) != 0; !ccv7 {
							Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(290), "n == 1 || n == 2\x00")
						}
						pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
						if aan == ppuint32(1) {
							ccv8 = ppint32(1)
						} else {
							ccv8 = -ppint32(1)
						}
						pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv8), 0, ppint32(ecMPFR_RNDN))
						Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)

						if ccv11 = iqlibc.Bool(0 != 0); ccv11 {
							if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
								ccv10 = -ppint32(1)
							} else {
								ccv10 = ppint32(1)
							}
						}
						if ccv11 && ppint64(ccv10) >= 0 {
							if ccv14 = iqlibc.Bool(0 != 0); ccv14 {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv13 = -ppint32(1)
								} else {
									ccv13 = ppint32(1)
								}
							}
							if ccv14 && iqlibc.ppUint64FromInt64(ppint64(ccv13)) == ppuint64(0) {
								{
									aa_p = cgbp + 224
									(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
									(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
									pp_ = ppint32(ecMPFR_RNDN)
									ccv15 = 0
								}
								pp_ = ccv15
							} else {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv16 = -ppint32(1)
								} else {
									ccv16 = ppint32(1)
								}
								Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv16)), 0, ppint32(ecMPFR_RNDN))
							}
						} else {
							if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
								ccv17 = -ppint32(1)
							} else {
								ccv17 = ppint32(1)
							}
							Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv17), 0, ppint32(ecMPFR_RNDN))
						}
						Xmpfr_set_exp(cgtls, cgbp+224, X__gmpfr_emin)
					} else {
						if aan <= ppuint32(4) {

							if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(3) || aan == ppuint32(4))), ppint64(1)) != 0; !ccv18 {
								Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(300), "n == 3 || n == 4\x00")
							}
							pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
							if aan == ppuint32(3) {
								ccv19 = ppint32(1)
							} else {
								ccv19 = -ppint32(1)
							}
							pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv19), 0, ppint32(ecMPFR_RNDN))
							Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)

							if ccv22 = iqlibc.Bool(0 != 0); ccv22 {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv21 = -ppint32(1)
								} else {
									ccv21 = ppint32(1)
								}
							}
							if ccv22 && ppint64(ccv21) >= 0 {
								if ccv25 = iqlibc.Bool(0 != 0); ccv25 {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv24 = -ppint32(1)
									} else {
										ccv24 = ppint32(1)
									}
								}
								if ccv25 && iqlibc.ppUint64FromInt64(ppint64(ccv24)) == ppuint64(0) {
									{
										aa_p1 = cgbp + 224
										(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
										(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
										pp_ = ppint32(ecMPFR_RNDN)
										ccv26 = 0
									}
									pp_ = ccv26
								} else {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv27 = -ppint32(1)
									} else {
										ccv27 = ppint32(1)
									}
									Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv27)), 0, ppint32(ecMPFR_RNDN))
								}
							} else {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv28 = -ppint32(1)
								} else {
									ccv28 = ppint32(1)
								}
								Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv28), 0, ppint32(ecMPFR_RNDN))
							}
							Xmpfr_setmax(cgtls, cgbp+224, X__gmpfr_emax)
						} else {
							if aan <= ppuint32(6) {

								if ccv29 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(5) || aan == ppuint32(6))), ppint64(1)) != 0; !ccv29 {
									Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(311), "n == 5 || n == 6\x00")
								}
								pp_ = ccv29 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
								if aan == ppuint32(5) {
									ccv30 = ppint32(1)
								} else {
									ccv30 = -ppint32(1)
								}
								pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv30), 0, ppint32(ecMPFR_RNDN))
								Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)

								if ccv33 = iqlibc.Bool(0 != 0); ccv33 {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv32 = -ppint32(1)
									} else {
										ccv32 = ppint32(1)
									}
								}
								if ccv33 && ppint64(ccv32) >= 0 {
									if ccv36 = iqlibc.Bool(0 != 0); ccv36 {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv35 = -ppint32(1)
										} else {
											ccv35 = ppint32(1)
										}
									}
									if ccv36 && iqlibc.ppUint64FromInt64(ppint64(ccv35)) == ppuint64(0) {
										{
											aa_p2 = cgbp + 224
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_sign = ppint32(1)
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
											pp_ = ppint32(ecMPFR_RNDN)
											ccv37 = 0
										}
										pp_ = ccv37
									} else {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv38 = -ppint32(1)
										} else {
											ccv38 = ppint32(1)
										}
										Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv38)), 0, ppint32(ecMPFR_RNDN))
									}
								} else {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv39 = -ppint32(1)
									} else {
										ccv39 = ppint32(1)
									}
									Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv39), 0, ppint32(ecMPFR_RNDN))
								}
								Xmpfr_setmax(cgtls, cgbp+224, X__gmpfr_emax)
							} else {

								if ccv40 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(7) || aan == ppuint32(8))), ppint64(1)) != 0; !ccv40 {
									Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(319), "n == 7 || n == 8\x00")
								}
								pp_ = ccv40 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
								if aan == ppuint32(7) {
									ccv41 = ppint32(1)
								} else {
									ccv41 = -ppint32(1)
								}
								pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv41), 0, ppint32(ecMPFR_RNDN))
								Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)

								if ccv44 = iqlibc.Bool(0 != 0); ccv44 {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv43 = -ppint32(1)
									} else {
										ccv43 = ppint32(1)
									}
								}
								if ccv44 && ppint64(ccv43) >= 0 {
									if ccv47 = iqlibc.Bool(0 != 0); ccv47 {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv46 = -ppint32(1)
										} else {
											ccv46 = ppint32(1)
										}
									}
									if ccv47 && iqlibc.ppUint64FromInt64(ppint64(ccv46)) == ppuint64(0) {
										{
											aa_p3 = cgbp + 224
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_sign = ppint32(1)
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
											pp_ = ppint32(ecMPFR_RNDN)
											ccv48 = 0
										}
										pp_ = ccv48
									} else {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv49 = -ppint32(1)
										} else {
											ccv49 = ppint32(1)
										}
										Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv49)), 0, ppint32(ecMPFR_RNDN))
									}
								} else {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv50 = -ppint32(1)
									} else {
										ccv50 = ppint32(1)
									}
									Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv50), 0, ppint32(ecMPFR_RNDN))
								}
								Xmpfr_set_exp(cgtls, cgbp+224, X__gmpfr_emin)
							}
						}
					}
				}
			}

			/* Exponent range for the test. */
			aaoemin = X__gmpfr_emin
			aaoemax = X__gmpfr_emax

			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt32(ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)))
			Xmpfr_clear_flags(cgtls)
			aacompare = sitest_add(cgtls, cgbp+32, cgbp, cgbp+224, aarnd)
			aaflags = X__gmpfr_flags
			if X__gmpfr_emin != aaoemin || X__gmpfr_emax != aaoemax {

				Xprintf(cgtls, "tgeneric: the exponent range has been modified by the tested function!\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
			if aarnd != ppint32(ecMPFR_RNDF) {
				if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) == iqlibc.ppInt32FromInt32(0)) != 0) {
					Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad inexact flag for test_add\x00"))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					if cgbp+224 != ppuintptr(0) {
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+224)
					}
					if -ppint32(1) >= 0 {
						Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
					}
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}
			aactrt++

			/* If rnd = RNDF, check that we obtain the same result as
			   RNDD or RNDU. */
			if aarnd == ppint32(ecMPFR_RNDF) {

				sitest_add(cgtls, cgbp+160, cgbp, cgbp+224, ppint32(ecMPFR_RNDD))
				sitest_add(cgtls, cgbp+192, cgbp, cgbp+224, ppint32(ecMPFR_RNDU))
				if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+160) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_sign || ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+192) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_sign)) {

					Xprintf(cgtls, "tgeneric: error fortest_add, RNDF; result matches neither RNDD nor RNDU\n\x00", 0)
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "x2 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+224)
					Xprintf(cgtls, "yd (RNDD) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+160)
					Xprintf(cgtls, "yu (RNDU) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+192)
					Xprintf(cgtls, "y  (RNDF) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xexit(cgtls, ppint32(1))
				}
			}

			/* Tests in a reduced exponent range. */

			aaoldflags = aaflags

			/* Determine the smallest exponent range containing the
			   exponents of the mpfr_t inputs (x, and u if TWO_ARGS)
			   and output (y). */
			aaemin = iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) - iqlibc.ppInt64FromInt32(1)
			aaemax = iqlibc.ppInt64FromInt32(1) - iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))

			if ccv51 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv51 {
			}
			if ccv51 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv52 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv52 {
			}
			if ccv52 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 224)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv53 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv53 {
			}
			if ccv53 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp /* exponent of the result */

				if aatest_of > 0 && aae-ppint64(1) >= aaemax { /* overflow test */

					/* Exponent e of the result > exponents of the inputs;
					   let's set emax to e - 1, so that one should get an
					   overflow. */
					Xset_emax(cgtls, aae-ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = sitest_add(cgtls, cgbp+128, cgbp, cgbp+224, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emax(cgtls, aaoemax)
					aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for test_add, reduced exponent range [%ld,%ld] (overflow test) on:\n\x00", iqlibc.ppVaList(cgbp+264, aaoemin, aae-ppint64(1)))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+224)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+264, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_of--
				}

				if aatest_uf > 0 && aae+ppint64(1) <= aaemin { /* underflow test */

					/* Exponent e of the result < exponents of the inputs;
					   let's set emin to e + 1, so that one should get an
					   underflow. */
					Xset_emin(cgtls, aae+ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = sitest_add(cgtls, cgbp+128, cgbp, cgbp+224, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emin(cgtls, aaoemin)
					aaex_flags1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags1 && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for test_add, reduced exponent range [%ld,%ld] (underflow test) on:\n\x00", iqlibc.ppVaList(cgbp+264, aae+ppint64(1), aaoemax))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+224)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags1)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+264, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_uf--
				}

				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			} /* MPFR_IS_PURE_FP (y) */

			if aaemin > aaemax {
				aaemin = aaemax
			} /* case where all values are singular */

			/* Consistency test in a reduced exponent range. Doing it
			   for the first 10 samples and for prec == p1 (which has
			   some special cases) should be sufficient. */
			if aactrt <= ppuint64(10) || aaprec == aap1 {

				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				Xmpfr_clear_flags(cgtls)
				aainexact = sitest_add(cgtls, cgbp+128, cgbp, cgbp+224, aarnd)
				aaflags = X__gmpfr_flags
				Xset_emin(cgtls, aaoemin)
				Xset_emax(cgtls, aaoemax)
				/* That test makes no sense for RNDF. */
				if aarnd != ppint32(ecMPFR_RNDF) && !(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+128, cgbp+32) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign) && iqlibc.ppBoolInt32(aainexact > 0)-iqlibc.ppBoolInt32(aainexact < 0) == iqlibc.ppBoolInt32(aacompare > 0)-iqlibc.ppBoolInt32(aacompare < 0) && aaflags == aaoldflags) {

					Xprintf(cgtls, "tgeneric: error for test_add, reduced exponent range [%ld,%ld] on:\n\x00", iqlibc.ppVaList(cgbp+264, aaemin, aaemax))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "x2 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+224)
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "Expected:\n  y = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+264, aacompare))
					Xflags_out(cgtls, aaoldflags)
					Xprintf(cgtls, "Got:\n  w = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+128)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+264, aainexact))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}

			X__gmpfr_flags = aaoldflags /* restore the flags */
			/* tests in a reduced exponent range */

			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 {
					if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad NaN flag for test_add\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

						if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad overflow flag for test_add\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(iqlibc.ppBoolInt32(aacompare == 0 && !(aainfinite_input != 0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad divide-by-zero flag for test_add\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) {
							if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad underflow flag for test_add\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+224 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+224)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						}
					}
				}
			} else {
				if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0 {

					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "both overflow and divide-by-zero for test_add\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "both underflow and divide-by-zero for test_add\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !(aacompare == iqlibc.ppInt32FromInt32(0)) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad compare value (divide-by-zero) for test_add\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 {

						if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "both underflow and overflow for test_add\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad compare value (overflow) for test_add\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						Xmpfr_nexttoinf(cgtls, cgbp+32)
						if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "should have been max MPFR number (overflow) for test_add\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {

							if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad compare value (underflow) for test_add\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+224 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+224)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
							Xmpfr_nexttozero(cgtls, cgbp+32)
							if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "should have been min MPFR number (underflow) for test_add\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+224 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+224)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						} else {
							if aacompare == 0 || aarnd == ppint32(ecMPFR_RNDF) || Xmpfr_can_round(cgtls, cgbp+32, aayprec, aarnd, aarnd, aaprec) != 0 {

								aactrn++
								{
									aa_p4 = cgbp + 32
									ccv54 = Xmpfr_set4(cgtls, cgbp+96, aa_p4, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p4)).fd_mpfr_sign)
								}
								pp_ = ccv54
								/* Risk of failures are known when some flags are already set
								   before the function call. Do not set the erange flag, as
								   it will remain set after the function call and no checks
								   are performed in such a case (see the mpfr_erangeflag_p
								   test below). */
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									X__gmpfr_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0) ^ iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE))
								}
								/* Let's increase the precision of the inputs in a random way.
								   In most cases, this doesn't make any difference, but for
								   the mpfr_fmod bug fixed in r6230, this triggers the bug. */
								Xmpfr_prec_round(cgtls, cgbp, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint64(15)), ppint32(ecMPFR_RNDN))
								Xmpfr_prec_round(cgtls, cgbp+224, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint64(15)), ppint32(ecMPFR_RNDN))
								aainexact = sitest_add(cgtls, cgbp+64, cgbp, cgbp+224, aarnd)
								if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0 {
									goto ppnext_n
								}
								if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+64) != 0) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "tgeneric: results differ for test_add on\n\x00", 0)
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp+224)
									Xprintf(cgtls, "prec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
									Xprintf(cgtls, "Got      \x00", 0)
									Xmpfr_dump(cgtls, cgbp+64)
									Xprintf(cgtls, "Expected \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xprintf(cgtls, "Approx   \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xexit(cgtls, ppint32(1))
								}
								aacompare2 = Xmpfr_cmp3(cgtls, cgbp+96, cgbp+32, ppint32(1))
								/* if rounding to nearest, cannot know the sign of t - f(x)
								   because of composed rounding: y = o(f(x)) and t = o(y) */
								if aacompare*aacompare2 >= 0 {
									aacompare = aacompare + aacompare2
								} else {
									aacompare = aainexact
								} /* cannot determine sign(t-f(x)) */
								if !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aacompare > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aacompare < iqlibc.ppInt32FromInt32(0))) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+264, Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare, aainexact))
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp+224)
									Xprintf(cgtls, "y = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xprintf(cgtls, "t = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xexit(cgtls, ppint32(1))
								}
							} else {
								if Xgetenv(cgtls, "MPFR_SUSPICIOUS_OVERFLOW\x00") != iqlibc.ppUintptrFromInt32(0) {

									/* For developers only! */

									if ccv55 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv55 {
									}
									if ccv56 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv55 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0))), ppint64(1)) != 0; !ccv56 {
										Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(803), "(!(((y)->_mpfr_exp) <= (((-0x7fffffffffffffffL-1))+3)) && ((! __builtin_constant_p (!!(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) || !(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0))) || (((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) ? (void) 0 : __builtin_unreachable()), 1))\x00")
									}
									pp_ = ccv56 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
									Xmpfr_nexttoinf(cgtls, cgbp+32)

									if ccv58 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3); ccv58 {
										if ccv57 = aarnd == ppint32(ecMPFR_RNDZ); !ccv57 {
										}
									}
									if ccv58 && (ccv57 || aarnd+iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)) && !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) && iqlibc.Bool(ppint32(mvTGENERIC_SO_TEST) != 0) {

										Xprintf(cgtls, "Possible bug! |y| is the maximum finite number (with yprec = %u) and has\nbeen obtained when rounding toward zero (%s). Thus there is a very\nprobable overflow, but the overflow flag is not set!\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
										Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp)
										Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp+224)
										Xexit(cgtls, ppint32(1))
									}
								}
							}
						}
					}
				}
			}

			goto ppnext_n
		ppnext_n:
			;
			/* In case the exponent range has been changed by
			   tests_default_random() or for special values... */
			Xset_emin(cgtls, aaold_emin)
			Xset_emax(cgtls, aaold_emax)

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	if Xgetenv(cgtls, "MPFR_TGENERIC_STAT\x00") != iqlibc.ppUintptrFromInt32(0) {
		Xprintf(cgtls, "tgeneric: normal cases / total = %lu / %lu\n\x00", iqlibc.ppVaList(cgbp+264, aactrn, aactrt))
	}

	if ppuint64(3)*aactrn < ppuint64(2)*aactrt {
		Xprintf(cgtls, "Warning! Too few normal cases in generic tests (%lu / %lu)\n\x00", iqlibc.ppVaList(cgbp+264, aactrn, aactrt))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+264, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_clear(cgtls, cgbp+224)
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	Xtests_start_mpfr(cgtls)

	siusesp = 0
	sitests(cgtls)

	siusesp = ppint32(1)
	sitests(cgtls)

	sitest_rndf_exact(cgtls, ppint64(200))
	sitestall_rndf(cgtls, ppint64(7))
	sicheck_extreme(cgtls)

	sitest_generic(cgtls, ppint64(mvMPFR_PREC_MIN), ppint64(1000), ppuint32(100))

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___builtin_unreachable(*iqlibc.ppTLS)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuint64

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _mpfr_add(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_add1(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_can_round(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32, ppint32, ppint64) ppint32

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clear_overflow(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp3(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_cmp_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64) ppint32

func _mpfr_cmpabs(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_div(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_div_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_div_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_inits2(*iqlibc.ppTLS, ppint64, ppuintptr, ppuintptr)

func _mpfr_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_mul_2si(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64, ppint32) ppint32

func _mpfr_mul_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttoinf(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttozero(*iqlibc.ppTLS, ppuintptr)

func _mpfr_prec_round(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32) ppint32

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_random2(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppuintptr)

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppuint8

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_exp(*iqlibc.ppTLS, ppuintptr, ppint64) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_setmax(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_setmin(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_sgn(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_sub_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _puts(*iqlibc.ppTLS, ppuintptr) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

var _stdout ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

// Code generated for linux/ppc64le by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/ppc64le -UHAVE_NEARBYINT -mlong-double-64 -c -o tsprintf.o.go tsprintf.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvBUF_SIZE = 65536
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.2250738585072014e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvE2BIG = 7
const mvEACCES = 13
const mvEADDRINUSE = 98
const mvEADDRNOTAVAIL = 99
const mvEADV = 68
const mvEAFNOSUPPORT = 97
const mvEAGAIN = 11
const mvEALREADY = 114
const mvEBADE = 52
const mvEBADF = 9
const mvEBADFD = 77
const mvEBADMSG = 74
const mvEBADR = 53
const mvEBADRQC = 56
const mvEBADSLT = 57
const mvEBFONT = 59
const mvEBUSY = 16
const mvECANCELED = 125
const mvECHILD = 10
const mvECHRNG = 44
const mvECOMM = 70
const mvECONNABORTED = 103
const mvECONNREFUSED = 111
const mvECONNRESET = 104
const mvEDEADLK = 35
const mvEDEADLOCK = 58
const mvEDESTADDRREQ = 89
const mvEDOM = 33
const mvEDOTDOT = 73
const mvEDQUOT = 122
const mvEEXIST = 17
const mvEFAULT = 14
const mvEFBIG = 27
const mvEHOSTDOWN = 112
const mvEHOSTUNREACH = 113
const mvEHWPOISON = 133
const mvEIDRM = 43
const mvEILSEQ = 84
const mvEINPROGRESS = 115
const mvEINTR = 4
const mvEINVAL = 22
const mvEIO = 5
const mvEISCONN = 106
const mvEISDIR = 21
const mvEISNAM = 120
const mvEKEYEXPIRED = 127
const mvEKEYREJECTED = 129
const mvEKEYREVOKED = 128
const mvEL2HLT = 51
const mvEL2NSYNC = 45
const mvEL3HLT = 46
const mvEL3RST = 47
const mvELIBACC = 79
const mvELIBBAD = 80
const mvELIBEXEC = 83
const mvELIBMAX = 82
const mvELIBSCN = 81
const mvELNRNG = 48
const mvELOOP = 40
const mvEMEDIUMTYPE = 124
const mvEMFILE = 24
const mvEMLINK = 31
const mvEMSGSIZE = 90
const mvEMULTIHOP = 72
const mvENAMETOOLONG = 36
const mvENAVAIL = 119
const mvENETDOWN = 100
const mvENETRESET = 102
const mvENETUNREACH = 101
const mvENFILE = 23
const mvENOANO = 55
const mvENOBUFS = 105
const mvENOCSI = 50
const mvENODATA = 61
const mvENODEV = 19
const mvENOENT = 2
const mvENOEXEC = 8
const mvENOKEY = 126
const mvENOLCK = 37
const mvENOLINK = 67
const mvENOMEDIUM = 123
const mvENOMEM = 12
const mvENOMSG = 42
const mvENONET = 64
const mvENOPKG = 65
const mvENOPROTOOPT = 92
const mvENOSPC = 28
const mvENOSR = 63
const mvENOSTR = 60
const mvENOSYS = 38
const mvENOTBLK = 15
const mvENOTCONN = 107
const mvENOTDIR = 20
const mvENOTEMPTY = 39
const mvENOTNAM = 118
const mvENOTRECOVERABLE = 131
const mvENOTSOCK = 88
const mvENOTSUP = "EOPNOTSUPP"
const mvENOTTY = 25
const mvENOTUNIQ = 76
const mvENXIO = 6
const mvEOPNOTSUPP = 95
const mvEOVERFLOW = 75
const mvEOWNERDEAD = 130
const mvEPERM = 1
const mvEPFNOSUPPORT = 96
const mvEPIPE = 32
const mvEPROTO = 71
const mvEPROTONOSUPPORT = 93
const mvEPROTOTYPE = 91
const mvERANGE = 34
const mvEREMCHG = 78
const mvEREMOTE = 66
const mvEREMOTEIO = 121
const mvERESTART = 85
const mvERFKILL = 132
const mvEROFS = 30
const mvESHUTDOWN = 108
const mvESOCKTNOSUPPORT = 94
const mvESPIPE = 29
const mvESRCH = 3
const mvESRMNT = 69
const mvESTALE = 116
const mvESTRPIPE = 86
const mvETIME = 62
const mvETIMEDOUT = 110
const mvETOOMANYREFS = 109
const mvETXTBSY = 26
const mvEUCLEAN = 117
const mvEUNATCH = 49
const mvEUSERS = 87
const mvEWOULDBLOCK = "EAGAIN"
const mvEXDEV = 18
const mvEXFULL = 54
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFMT_MPFR_SIZE = 12
const mvFMT_SIZE = 11
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 2147483647
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1158
const mvMPFR_AI_THRESHOLD3 = 20165
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 580
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 10480
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 9
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 22904
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 14
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/powerpc64/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvN0 = 20
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvT1 = "000"
const mvT2 = ".000"
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_ARCH_PPC = 1
const mv_ARCH_PPC64 = 1
const mv_ARCH_PPCGR = 1
const mv_ARCH_PPCSQ = 1
const mv_ARCH_PWR4 = 1
const mv_ARCH_PWR5 = 1
const mv_ARCH_PWR5X = 1
const mv_ARCH_PWR6 = 1
const mv_ARCH_PWR7 = 1
const mv_ARCH_PWR8 = 1
const mv_CALL_ELF = 2
const mv_CALL_LINUX = 1
const mv_GMP_H_HAVE_FILE = 1
const mv_GMP_H_HAVE_VA_LIST = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LITTLE_ENDIAN = 1
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_H_HAVE_VA_LIST = 1
const mv_MPFR_H_HAVE_VA_LIST_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ALTIVEC__ = 1
const mv__APPLE_ALTIVEC__ = 1
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BUILTIN_CPU_SUPPORTS__ = 1
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__CMODEL_MEDIUM__ = 1
const mv__CRYPTO__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT128_TYPE__ = 1
const mv__FLOAT128__ = 1
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FP_FAST_FMAL = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "powerpc64le-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=/build/gmp-dldbp2/gmp-6.2.1+dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 1
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 1
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC__ = 10
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1014
const mv__HAVE_BSWAP__ = 1
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__HTM__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LITTLE_ENDIAN__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__POWER8_VECTOR__ = 1
const mv__PPC64__ = 1
const mv__PPC__ = 1
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__QUAD_MEMORY_ATOMIC__ = 1
const mv__RECIPF__ = 1
const mv__RECIP_PRECISION__ = 1
const mv__RECIP__ = 1
const mv__RSQRTEF__ = 1
const mv__RSQRTE__ = 1
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__STRUCT_PARM_ALIGN__ = 16
const mv__TM_FENCE__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VEC_ELEMENT_REG_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__VEC__ = 10206
const mv__VERSION__ = "10.2.1 20210110"
const mv__VSX__ = 1
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__builtin_copysignq = "__builtin_copysignf128"
const mv__builtin_fabsq = "__builtin_fabsf128"
const mv__builtin_huge_valq = "__builtin_huge_valf128"
const mv__builtin_infq = "__builtin_inff128"
const mv__builtin_nanq = "__builtin_nanf128"
const mv__builtin_nansq = "__builtin_nansf128"
const mv__builtin_vsx_vperm = "__builtin_vec_perm"
const mv__builtin_vsx_xvmaddadp = "__builtin_vsx_xvmadddp"
const mv__builtin_vsx_xvmaddasp = "__builtin_vsx_xvmaddsp"
const mv__builtin_vsx_xvmaddmdp = "__builtin_vsx_xvmadddp"
const mv__builtin_vsx_xvmaddmsp = "__builtin_vsx_xvmaddsp"
const mv__builtin_vsx_xvmsubadp = "__builtin_vsx_xvmsubdp"
const mv__builtin_vsx_xvmsubasp = "__builtin_vsx_xvmsubsp"
const mv__builtin_vsx_xvmsubmdp = "__builtin_vsx_xvmsubdp"
const mv__builtin_vsx_xvmsubmsp = "__builtin_vsx_xvmsubsp"
const mv__builtin_vsx_xvnmaddadp = "__builtin_vsx_xvnmadddp"
const mv__builtin_vsx_xvnmaddasp = "__builtin_vsx_xvnmaddsp"
const mv__builtin_vsx_xvnmaddmdp = "__builtin_vsx_xvnmadddp"
const mv__builtin_vsx_xvnmaddmsp = "__builtin_vsx_xvnmaddsp"
const mv__builtin_vsx_xvnmsubadp = "__builtin_vsx_xvnmsubdp"
const mv__builtin_vsx_xvnmsubasp = "__builtin_vsx_xvnmsubsp"
const mv__builtin_vsx_xvnmsubmdp = "__builtin_vsx_xvnmsubdp"
const mv__builtin_vsx_xvnmsubmsp = "__builtin_vsx_xvnmsubsp"
const mv__builtin_vsx_xxland = "__builtin_vec_and"
const mv__builtin_vsx_xxlandc = "__builtin_vec_andc"
const mv__builtin_vsx_xxlnor = "__builtin_vec_nor"
const mv__builtin_vsx_xxlor = "__builtin_vec_or"
const mv__builtin_vsx_xxlxor = "__builtin_vec_xor"
const mv__builtin_vsx_xxsel = "__builtin_vec_sel"
const mv__float128 = "__ieee128"
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__powerpc64__ = 1
const mv__powerpc__ = 1
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpfr_vasprintf = "__gmpfr_vasprintf"
const mvmpfr_vfprintf = "__gmpfr_vfprintf"
const mvmpfr_vprintf = "__gmpfr_vprintf"
const mvmpfr_vsnprintf = "__gmpfr_vsnprintf"
const mvmpfr_vsprintf = "__gmpfr_vsprintf"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnva_list = ppuintptr

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnlocale_t = ppuintptr

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

var Xprec_max_printf = ppint32(5000) /* limit for random precision in
   random_double() */

var Xpinf_str = [4]ppuint8{'i', 'n', 'f'}
var Xpinf_uc_str = [4]ppuint8{'I', 'N', 'F'}
var Xminf_str = [5]ppuint8{'-', 'i', 'n', 'f'}
var Xminf_uc_str = [5]ppuint8{'-', 'I', 'N', 'F'}
var Xnan_str = [4]ppuint8{'n', 'a', 'n'}
var Xnan_uc_str = [4]ppuint8{'N', 'A', 'N'}

var Xrandsize ppint32

// C documentation
//
//	/* 1. compare expected string with the string BUFFER returned by
//	   mpfr_sprintf(buffer, fmt, x)
//	   2. then test mpfr_snprintf (buffer, p, fmt, x) with a random p. */
func sicheck_sprintf(cgtls *iqlibc.ppTLS, aaexpected ppuintptr, aafmt ppuintptr, aax tnmpfr_srcptr) {
	cgbp := cgtls.ppAlloc(131104)
	defer cgtls.ppFree(131104)

	var aan0, aan1 ppint32
	var pp_ /* buffer at bp+0 */ [65536]ppuint8
	var pp_ /* part_expected at bp+65536 */ [65536]ppuint8
	pp_, pp_ = aan0, aan1

	/* test mpfr_sprintf */
	aan0 = Xmpfr_sprintf(cgtls, cgbp, aafmt, iqlibc.ppVaList(cgbp+131080, aax))
	if Xstrcmp(cgtls, cgbp, aaexpected) != 0 {

		Xprintf(cgtls, "Error in mpfr_sprintf (s, \"%s\", x);\n\x00", iqlibc.ppVaList(cgbp+131080, aafmt))
		Xprintf(cgtls, "expected: \"%s\"\ngot:      \"%s\"\n\x00", iqlibc.ppVaList(cgbp+131080, aaexpected, cgbp))

		Xexit(cgtls, ppint32(1))
	}

	/* test mpfr_snprintf */
	Xrandsize = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(aan0+iqlibc.ppInt32FromInt32(3))) - ppint32(3) /* between -3 and n0 - 1 */
	if Xrandsize < 0 {

		aan1 = Xmpfr_snprintf(cgtls, iqlibc.ppUintptrFromInt32(0), ppuint64(0), aafmt, iqlibc.ppVaList(cgbp+131080, aax))
	} else {

		(*(*[65536]ppuint8)(iqunsafe.ppPointer(cgbp)))[Xrandsize] = ppuint8(17)
		aan1 = Xmpfr_snprintf(cgtls, cgbp, iqlibc.ppUint64FromInt32(Xrandsize), aafmt, iqlibc.ppVaList(cgbp+131080, aax))
		if iqlibc.ppInt32FromUint8((*(*[65536]ppuint8)(iqunsafe.ppPointer(cgbp)))[Xrandsize]) != ppint32(17) {

			Xprintf(cgtls, "Buffer overflow in mpfr_snprintf for randsize = %d!\n\x00", iqlibc.ppVaList(cgbp+131080, Xrandsize))
			Xexit(cgtls, ppint32(1))
		}
	}
	if aan0 != aan1 {

		Xprintf(cgtls, "Error in mpfr_snprintf (s, %d, \"%s\", x) return value\n\x00", iqlibc.ppVaList(cgbp+131080, Xrandsize, aafmt))
		Xprintf(cgtls, "expected: %d\ngot:      %d\nx='\x00", iqlibc.ppVaList(cgbp+131080, aan0, aan1))
		Xmpfr_printf(cgtls, aafmt, iqlibc.ppVaList(cgbp+131080, aax))
		Xprintf(cgtls, "'\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if Xrandsize > ppint32(1) && Xstrncmp(cgtls, aaexpected, cgbp, iqlibc.ppUint64FromInt32(Xrandsize-ppint32(1))) != 0 || Xrandsize == ppint32(1) && iqlibc.ppInt32FromUint8((*(*[65536]ppuint8)(iqunsafe.ppPointer(cgbp)))[0]) != ppint32('\000') {

		Xstrncpy(cgtls, cgbp+65536, aaexpected, iqlibc.ppUint64FromInt32(Xrandsize))
		(*(*[65536]ppuint8)(iqunsafe.ppPointer(cgbp + 65536)))[Xrandsize-ppint32(1)] = ppuint8('\000')
		Xprintf(cgtls, "Error in mpfr_vsnprintf (s, %d, \"%s\", ...);\n\x00", iqlibc.ppVaList(cgbp+131080, Xrandsize, aafmt))
		Xprintf(cgtls, "expected: \"%s\"\ngot:      \"%s\"\n\x00", iqlibc.ppVaList(cgbp+131080, cgbp+65536, cgbp))
		Xexit(cgtls, ppint32(1))
	}
}

// C documentation
//
//	/* 1. compare expected string with the string BUFFER returned by
//	   mpfr_vsprintf(buffer, fmt, ...)
//	   2. then, test mpfr_vsnprintf. */
func sicheck_vsprintf(cgtls *iqlibc.ppTLS, aaexpected ppuintptr, aafmt ppuintptr, cgva ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(131104)
	defer cgtls.ppFree(131104)

	var aaap0, aaap1 tnva_list
	var aan0, aan1 ppint32
	var pp_ /* buffer at bp+0 */ [65536]ppuint8
	var pp_ /* part_expected at bp+65536 */ [65536]ppuint8
	pp_, pp_, pp_, pp_ = aaap0, aaap1, aan0, aan1

	aaap0 = cgva
	aan0 = X__gmpfr_vsprintf(cgtls, cgbp, aafmt, aaap0)
	pp_ = aaap0

	if Xstrcmp(cgtls, cgbp, aaexpected) != 0 {

		Xprintf(cgtls, "Error in mpfr_vsprintf (s, \"%s\", ...);\n\x00", iqlibc.ppVaList(cgbp+131080, aafmt))
		Xprintf(cgtls, "expected: \"%s\"\ngot:      \"%s\"\n\x00", iqlibc.ppVaList(cgbp+131080, aaexpected, cgbp))
		Xexit(cgtls, ppint32(1))
	}

	aaap1 = cgva

	/* test mpfr_snprintf */
	Xrandsize = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(aan0+iqlibc.ppInt32FromInt32(3))) - ppint32(3) /* between -3 and n0 - 1 */
	if Xrandsize < 0 {

		aan1 = X__gmpfr_vsnprintf(cgtls, iqlibc.ppUintptrFromInt32(0), ppuint64(0), aafmt, aaap1)
	} else {

		(*(*[65536]ppuint8)(iqunsafe.ppPointer(cgbp)))[Xrandsize] = ppuint8(17)
		aan1 = X__gmpfr_vsnprintf(cgtls, cgbp, iqlibc.ppUint64FromInt32(Xrandsize), aafmt, aaap1)
		if iqlibc.ppInt32FromUint8((*(*[65536]ppuint8)(iqunsafe.ppPointer(cgbp)))[Xrandsize]) != ppint32(17) {

			Xprintf(cgtls, "Buffer overflow in mpfr_vsnprintf for randsize = %d!\n\x00", iqlibc.ppVaList(cgbp+131080, Xrandsize))
			Xexit(cgtls, ppint32(1))
		}
	}

	pp_ = aaap1

	if aan0 != aan1 {

		Xprintf(cgtls, "Error in mpfr_vsnprintf (s, %d, \"%s\", ...) return value\n\x00", iqlibc.ppVaList(cgbp+131080, Xrandsize, aafmt))
		Xprintf(cgtls, "expected: %d\ngot:      %d\n\x00", iqlibc.ppVaList(cgbp+131080, aan0, aan1))
		Xexit(cgtls, ppint32(1))
	}
	if Xrandsize > ppint32(1) && Xstrncmp(cgtls, aaexpected, cgbp, iqlibc.ppUint64FromInt32(Xrandsize-ppint32(1))) != 0 || Xrandsize == ppint32(1) && iqlibc.ppInt32FromUint8((*(*[65536]ppuint8)(iqunsafe.ppPointer(cgbp)))[0]) != ppint32('\000') {

		Xstrncpy(cgtls, cgbp+65536, aaexpected, iqlibc.ppUint64FromInt32(Xrandsize))
		(*(*[65536]ppuint8)(iqunsafe.ppPointer(cgbp + 65536)))[Xrandsize-ppint32(1)] = ppuint8('\000')
		Xprintf(cgtls, "Error in mpfr_vsnprintf (s, %d, \"%s\", ...);\n\x00", iqlibc.ppVaList(cgbp+131080, Xrandsize, aafmt))
		Xprintf(cgtls, "expected: \"%s\"\ngot:      \"%s\"\n\x00", iqlibc.ppVaList(cgbp+131080, cgbp+65536, cgbp))
		Xexit(cgtls, ppint32(1))
	}

	return aan0
}

func sinative_types(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(304)
	defer cgtls.ppFree(304)

	var aac, aai, aak ppint32
	var aaui ppuint32
	var ccv2 ppbool
	var pp_ /* buf at bp+25 */ [255]ppuint8
	var pp_ /* d at bp+0 */ [2]ppfloat64
	var pp_ /* s at bp+20 */ [5]ppuint8
	pp_, pp_, pp_, pp_, pp_ = aac, aai, aak, aaui, ccv2
	aac = ppint32('a')
	aai = -ppint32(1)
	aaui = ppuint32(1)
	*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)) = [2]ppfloat64{
		0: -iqlibc.ppFloat64FromFloat64(1.25),
		1: ppfloat64(7.62939453125e-06),
	}
	*(*[5]ppuint8)(iqunsafe.ppPointer(cgbp + 20)) = [5]ppuint8{'t', 'e', 's', 't'}

	Xsprintf(cgtls, cgbp+25, "%c\x00", iqlibc.ppVaList(cgbp+288, aac))
	sicheck_vsprintf(cgtls, cgbp+25, "%c\x00", iqlibc.ppVaList(cgbp+288, aac))

	Xsprintf(cgtls, cgbp+25, "%d\x00", iqlibc.ppVaList(cgbp+288, aai))
	sicheck_vsprintf(cgtls, cgbp+25, "%d\x00", iqlibc.ppVaList(cgbp+288, aai))

	sicheck_vsprintf(cgtls, "0\x00", "%d\x00", iqlibc.ppVaList(cgbp+288, 0))
	sicheck_vsprintf(cgtls, "\x00", "%.d\x00", iqlibc.ppVaList(cgbp+288, 0))
	sicheck_vsprintf(cgtls, "\x00", "%.0d\x00", iqlibc.ppVaList(cgbp+288, 0))

	Xsprintf(cgtls, cgbp+25, "%i\x00", iqlibc.ppVaList(cgbp+288, aai))
	sicheck_vsprintf(cgtls, cgbp+25, "%i\x00", iqlibc.ppVaList(cgbp+288, aai))

	sicheck_vsprintf(cgtls, "0\x00", "%i\x00", iqlibc.ppVaList(cgbp+288, 0))
	sicheck_vsprintf(cgtls, "\x00", "%.i\x00", iqlibc.ppVaList(cgbp+288, 0))
	sicheck_vsprintf(cgtls, "\x00", "%.0i\x00", iqlibc.ppVaList(cgbp+288, 0))

	aak = 0
	for {
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(cgbp == cgbp)), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(220), "(void *) &(d) == (void *) &(d)[0]\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if !(ppint64(aak) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(16)/iqlibc.ppUint64FromInt64(8))) {
			break
		}

		Xsprintf(cgtls, cgbp+25, "%e\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%e\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		Xsprintf(cgtls, cgbp+25, "%E\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%E\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		Xsprintf(cgtls, cgbp+25, "%f\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%f\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		Xsprintf(cgtls, cgbp+25, "%g\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%g\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		Xsprintf(cgtls, cgbp+25, "%G\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%G\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		X__gmp_sprintf(cgtls, cgbp+25, "%a\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%a\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		X__gmp_sprintf(cgtls, cgbp+25, "%A\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%A\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		X__gmp_sprintf(cgtls, cgbp+25, "%la\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%la\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		X__gmp_sprintf(cgtls, cgbp+25, "%lA\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%lA\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		Xsprintf(cgtls, cgbp+25, "%le\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%le\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		Xsprintf(cgtls, cgbp+25, "%lE\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%lE\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		Xsprintf(cgtls, cgbp+25, "%lf\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%lf\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		Xsprintf(cgtls, cgbp+25, "%lg\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%lg\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		Xsprintf(cgtls, cgbp+25, "%lG\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))
		sicheck_vsprintf(cgtls, cgbp+25, "%lG\x00", iqlibc.ppVaList(cgbp+288, (*(*[2]ppfloat64)(iqunsafe.ppPointer(cgbp)))[aak]))

		goto cg_1
	cg_1:
		;
		aak++
	}

	Xsprintf(cgtls, cgbp+25, "%o\x00", iqlibc.ppVaList(cgbp+288, aai))
	sicheck_vsprintf(cgtls, cgbp+25, "%o\x00", iqlibc.ppVaList(cgbp+288, aai))

	Xsprintf(cgtls, cgbp+25, "%s\x00", iqlibc.ppVaList(cgbp+288, cgbp+20))
	sicheck_vsprintf(cgtls, cgbp+25, "%s\x00", iqlibc.ppVaList(cgbp+288, cgbp+20))

	Xsprintf(cgtls, cgbp+25, "--%s++\x00", iqlibc.ppVaList(cgbp+288, "\x00"))
	sicheck_vsprintf(cgtls, cgbp+25, "--%s++\x00", iqlibc.ppVaList(cgbp+288, "\x00"))

	Xsprintf(cgtls, cgbp+25, "%u\x00", iqlibc.ppVaList(cgbp+288, aaui))
	sicheck_vsprintf(cgtls, cgbp+25, "%u\x00", iqlibc.ppVaList(cgbp+288, aaui))

	Xsprintf(cgtls, cgbp+25, "%x\x00", iqlibc.ppVaList(cgbp+288, aaui))
	sicheck_vsprintf(cgtls, cgbp+25, "%x\x00", iqlibc.ppVaList(cgbp+288, aaui))
}

func sidecimal(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(144)
	defer cgtls.ppFree(144)

	var aa_p, aa_p2 tnmpfr_srcptr
	var aa_p1, aa_p3 tnmpfr_ptr
	var aap tnmpfr_prec_t
	var ccv1, ccv2, ccv3, ccv4 ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aa_p2, aa_p3, aap, ccv1, ccv2, ccv3, ccv4
	aap = ppint64(128)

	/* specifier 'P' for precision */
	sicheck_vsprintf(cgtls, "128\x00", "%Pu\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "00128\x00", "%.5Pu\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "  128\x00", "%5Pu\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "000128\x00", "%06Pu\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "128    :\x00", "%-7Pu:\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "000128:\x00", "%-2.6Pd:\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "  000128:\x00", "%8.6Pd:\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "000128  :\x00", "%-8.6Pd:\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "+128:\x00", "%+Pd:\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, " 128:\x00", "% Pd:\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "80:\x00", "% Px:\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "0x80:\x00", "% #Px:\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "0x80:\x00", "%0#+ -Px:\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "0200:\x00", "%0#+ -Po:\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "+0000128 :\x00", "%0+ *.*Pd:\x00", iqlibc.ppVaList(cgbp+104, -ppint32(9), ppint32(7), aap))
	sicheck_vsprintf(cgtls, "+12345   :\x00", "%0+ -*.*Pd:\x00", iqlibc.ppVaList(cgbp+104, -ppint32(9), -ppint32(3), iqlibc.ppInt64FromInt32(12345)))
	sicheck_vsprintf(cgtls, "0\x00", "%Pu\x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppInt64FromInt32(0)))
	/* Do not add a test like "%05.1Pd" as MS Windows is buggy: when
	   a precision is given, the '0' flag must be ignored. */

	/* specifier 'P' with precision field 0 */
	sicheck_vsprintf(cgtls, "128\x00", "%.Pu\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "128\x00", "%.0Pd\x00", iqlibc.ppVaList(cgbp+104, aap))
	sicheck_vsprintf(cgtls, "\x00", "%.Pu\x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppInt64FromInt32(0)))
	sicheck_vsprintf(cgtls, "\x00", "%.0Pd\x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppInt64FromInt32(0)))

	Xmpfr_init(cgtls, cgbp+64)
	Xmpfr_init2(cgtls, cgbp, ppint64(128))

	/* special numbers */
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_str)), "%Re\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_str)), "%RUe\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_uc_str)), "%RE\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_uc_str)), "%RDE\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_str)), "%Rf\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_str)), "%RYf\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_uc_str)), "%RF\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_uc_str)), "%RZF\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_str)), "%Rg\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_str)), "%RNg\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_uc_str)), "%RG\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_uc_str)), "%RUG\x00", cgbp)
	sicheck_sprintf(cgtls, "       inf\x00", "%010Re\x00", cgbp)
	sicheck_sprintf(cgtls, "       inf\x00", "%010RDe\x00", cgbp)

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%Re\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%RYe\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_uc_str)), "%RE\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_uc_str)), "%RZE\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%Rf\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%RNf\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_uc_str)), "%RF\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_uc_str)), "%RUF\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%Rg\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%RDg\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_uc_str)), "%RG\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_uc_str)), "%RYG\x00", cgbp)
	sicheck_sprintf(cgtls, "      -inf\x00", "%010Re\x00", cgbp)
	sicheck_sprintf(cgtls, "      -inf\x00", "%010RZe\x00", cgbp)

	Xmpfr_set_nan(cgtls, cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_str)), "%Re\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_str)), "%RNe\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_uc_str)), "%RE\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_uc_str)), "%RUE\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_str)), "%Rf\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_str)), "%RDf\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_uc_str)), "%RF\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_uc_str)), "%RYF\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_str)), "%Rg\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_str)), "%RZg\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_uc_str)), "%RG\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_uc_str)), "%RNG\x00", cgbp)
	sicheck_sprintf(cgtls, "       nan\x00", "%010Re\x00", cgbp)

	/* positive numbers */
	Xmpfr_set_str(cgtls, cgbp, "18993474.61279296875\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(59))
	{
		aa_p = cgbp
		ccv1 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
	}
	pp_ = ccv1
	{
		aa_p1 = cgbp + 64
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDD)
		ccv2 = 0
	}
	pp_ = ccv2

	/* simplest case right justified */
	sicheck_sprintf(cgtls, "1.899347461279296875000000000000000000000e+07\x00", "%30Re\x00", cgbp)
	sicheck_sprintf(cgtls, "      1.899347461279296875e+07\x00", "%30Re\x00", cgbp+32)
	sicheck_sprintf(cgtls, "                         2e+07\x00", "%30.0Re\x00", cgbp)
	sicheck_sprintf(cgtls, "               18993474.612793\x00", "%30Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "              18993474.6127930\x00", "%30.7Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "                   1.89935e+07\x00", "%30Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "                         2e+07\x00", "%30.0Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "          18993474.61279296875\x00", "%30.19Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "        0.0000000000000000e+00\x00", "%30Re\x00", cgbp+64)
	sicheck_sprintf(cgtls, "                      0.000000\x00", "%30Rf\x00", cgbp+64)
	sicheck_sprintf(cgtls, "                             0\x00", "%30Rg\x00", cgbp+64)
	sicheck_sprintf(cgtls, "                       0.00000\x00", "%#30Rg\x00", cgbp+64)
	sicheck_sprintf(cgtls, "                         0e+00\x00", "%30.0Re\x00", cgbp+64)
	sicheck_sprintf(cgtls, "                             0\x00", "%30.0Rf\x00", cgbp+64)
	sicheck_sprintf(cgtls, "                        0.0000\x00", "%30.4Rf\x00", cgbp+64)
	sicheck_sprintf(cgtls, "                             0\x00", "%30.0Rg\x00", cgbp+64)
	sicheck_sprintf(cgtls, "                             0\x00", "%30.4Rg\x00", cgbp+64)
	/* sign or space, pad with leading zeros */
	sicheck_sprintf(cgtls, " 1.899347461279296875000000000000000000000E+07\x00", "% 030RE\x00", cgbp)
	sicheck_sprintf(cgtls, " 000001.899347461279296875E+07\x00", "% 030RE\x00", cgbp+32)
	sicheck_sprintf(cgtls, " 0000000000000000001.89935E+07\x00", "% 030RG\x00", cgbp)
	sicheck_sprintf(cgtls, " 0000000000000000000000002E+07\x00", "% 030.0RE\x00", cgbp)
	sicheck_sprintf(cgtls, " 0000000000000000000000000E+00\x00", "% 030.0RE\x00", cgbp+64)
	sicheck_sprintf(cgtls, " 00000000000000000000000000000\x00", "% 030.0RF\x00", cgbp+64)
	/* sign + or -, left justified */
	sicheck_sprintf(cgtls, "+1.899347461279296875000000000000000000000e+07\x00", "%+-30Re\x00", cgbp)
	sicheck_sprintf(cgtls, "+1.899347461279296875e+07     \x00", "%+-30Re\x00", cgbp+32)
	sicheck_sprintf(cgtls, "+2e+07                        \x00", "%+-30.0Re\x00", cgbp)
	sicheck_sprintf(cgtls, "+0e+00                        \x00", "%+-30.0Re\x00", cgbp+64)
	sicheck_sprintf(cgtls, "+0                            \x00", "%+-30.0Rf\x00", cgbp+64)
	/* decimal point, left justified, precision and rounding parameter */
	sicheck_vsprintf(cgtls, "1.9E+07   \x00", "%#-10.*R*E\x00", iqlibc.ppVaList(cgbp+104, ppint32(1), ppint32(ecMPFR_RNDN), cgbp))
	sicheck_vsprintf(cgtls, "2.E+07    \x00", "%#*.*R*E\x00", iqlibc.ppVaList(cgbp+104, -ppint32(10), 0, ppint32(ecMPFR_RNDN), cgbp))
	sicheck_vsprintf(cgtls, "2.E+07    \x00", "%#-10.*R*G\x00", iqlibc.ppVaList(cgbp+104, 0, ppint32(ecMPFR_RNDN), cgbp))
	sicheck_vsprintf(cgtls, "0.E+00    \x00", "%#-10.*R*E\x00", iqlibc.ppVaList(cgbp+104, 0, ppint32(ecMPFR_RNDN), cgbp+64))
	sicheck_vsprintf(cgtls, "0.        \x00", "%#-10.*R*F\x00", iqlibc.ppVaList(cgbp+104, 0, ppint32(ecMPFR_RNDN), cgbp+64))
	sicheck_vsprintf(cgtls, "0.        \x00", "%#-10.*R*G\x00", iqlibc.ppVaList(cgbp+104, 0, ppint32(ecMPFR_RNDN), cgbp+64))
	/* sign or space */
	sicheck_sprintf(cgtls, " 1.899e+07\x00", "% .3RNe\x00", cgbp)
	sicheck_sprintf(cgtls, " 2e+07\x00", "% .0RNe\x00", cgbp)
	/* sign + or -, decimal point, pad with leading zeros */
	sicheck_sprintf(cgtls, "+0001.8E+07\x00", "%0+#11.1RZE\x00", cgbp)
	sicheck_sprintf(cgtls, "+00001.E+07\x00", "%0+#11.0RZE\x00", cgbp)
	sicheck_sprintf(cgtls, "+0000.0E+00\x00", "%0+#11.1RZE\x00", cgbp+64)
	sicheck_sprintf(cgtls, "+00000000.0\x00", "%0+#11.1RZF\x00", cgbp+64)
	/* pad with leading zero */
	sicheck_sprintf(cgtls, "1.899347461279296875000000000000000000000e+07\x00", "%030RDe\x00", cgbp)
	sicheck_sprintf(cgtls, "0000001.899347461279296875e+07\x00", "%030RDe\x00", cgbp+32)
	sicheck_sprintf(cgtls, "00000000000000000000000001e+07\x00", "%030.0RDe\x00", cgbp)
	/* sign or space, decimal point, left justified */
	sicheck_sprintf(cgtls, " 1.8E+07   \x00", "%- #11.1RDE\x00", cgbp)
	sicheck_sprintf(cgtls, " 1.E+07    \x00", "%- #11.0RDE\x00", cgbp)
	/* large requested precision */
	sicheck_sprintf(cgtls, "18993474.61279296875\x00", "%.2147483647Rg\x00", cgbp)

	/* negative numbers */
	pp_ = Xmpfr_mul_si(cgtls, cgbp, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), ppint32(ecMPFR_RNDD))
	pp_ = Xmpfr_mul_si(cgtls, cgbp+64, cgbp+64, ppint64(-iqlibc.ppInt32FromInt32(1)), ppint32(ecMPFR_RNDD))

	/* sign + or - */
	sicheck_sprintf(cgtls, "  -1.8e+07\x00", "%+10.1RUe\x00", cgbp)
	sicheck_sprintf(cgtls, "    -1e+07\x00", "%+10.0RUe\x00", cgbp)
	sicheck_sprintf(cgtls, "    -0e+00\x00", "%+10.0RUe\x00", cgbp+64)
	sicheck_sprintf(cgtls, "        -0\x00", "%+10.0RUf\x00", cgbp+64)

	/* neighborhood of 1 */
	Xmpfr_set_str(cgtls, cgbp, "0.99993896484375\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(43))
	{
		aa_p2 = cgbp
		ccv3 = Xmpfr_set4(cgtls, cgbp+32, aa_p2, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_sign)
	}
	pp_ = ccv3
	sicheck_sprintf(cgtls, "9.999389648437500000000000000000000000000E-01\x00", "%-20RE\x00", cgbp)
	sicheck_sprintf(cgtls, "9.9993896484375E-01 \x00", "%-20RE\x00", cgbp+32)
	sicheck_sprintf(cgtls, "1E+00               \x00", "%-20.RE\x00", cgbp)
	sicheck_sprintf(cgtls, "1E+00               \x00", "%-20.RE\x00", cgbp+32)
	sicheck_sprintf(cgtls, "1E+00               \x00", "%-20.0RE\x00", cgbp)
	sicheck_sprintf(cgtls, "1.0E+00             \x00", "%-20.1RE\x00", cgbp)
	sicheck_sprintf(cgtls, "1.00E+00            \x00", "%-20.2RE\x00", cgbp)
	sicheck_sprintf(cgtls, "9.999E-01           \x00", "%-20.3RE\x00", cgbp)
	sicheck_sprintf(cgtls, "9.9994E-01          \x00", "%-20.4RE\x00", cgbp)
	sicheck_sprintf(cgtls, "0.999939            \x00", "%-20RF\x00", cgbp)
	sicheck_sprintf(cgtls, "1                   \x00", "%-20.RF\x00", cgbp)
	sicheck_sprintf(cgtls, "1                   \x00", "%-20.0RF\x00", cgbp)
	sicheck_sprintf(cgtls, "1.0                 \x00", "%-20.1RF\x00", cgbp)
	sicheck_sprintf(cgtls, "1.00                \x00", "%-20.2RF\x00", cgbp)
	sicheck_sprintf(cgtls, "1.000               \x00", "%-20.3RF\x00", cgbp)
	sicheck_sprintf(cgtls, "0.9999              \x00", "%-20.4RF\x00", cgbp)
	sicheck_sprintf(cgtls, "0.999939            \x00", "%-#20RF\x00", cgbp)
	sicheck_sprintf(cgtls, "1.                  \x00", "%-#20.RF\x00", cgbp)
	sicheck_sprintf(cgtls, "1.                  \x00", "%-#20.0RF\x00", cgbp)
	sicheck_sprintf(cgtls, "1.0                 \x00", "%-#20.1RF\x00", cgbp)
	sicheck_sprintf(cgtls, "1.00                \x00", "%-#20.2RF\x00", cgbp)
	sicheck_sprintf(cgtls, "1.000               \x00", "%-#20.3RF\x00", cgbp)
	sicheck_sprintf(cgtls, "0.9999              \x00", "%-#20.4RF\x00", cgbp)
	sicheck_sprintf(cgtls, "0.999939            \x00", "%-20RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1                   \x00", "%-20.RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1                   \x00", "%-20.0RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1                   \x00", "%-20.1RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1                   \x00", "%-20.2RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1                   \x00", "%-20.3RG\x00", cgbp)
	sicheck_sprintf(cgtls, "0.9999              \x00", "%-20.4RG\x00", cgbp)
	sicheck_sprintf(cgtls, "0.999939            \x00", "%-#20RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1.                  \x00", "%-#20.RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1.                  \x00", "%-#20.0RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1.                  \x00", "%-#20.1RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1.0                 \x00", "%-#20.2RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1.00                \x00", "%-#20.3RG\x00", cgbp)
	sicheck_sprintf(cgtls, "0.9999              \x00", "%-#20.4RG\x00", cgbp)

	/* powers of 10 */
	Xmpfr_set_str(cgtls, cgbp, "1e17\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1.000000000000000000000000000000000000000e+17\x00", "%Re\x00", cgbp)
	sicheck_sprintf(cgtls, "1.000e+17\x00", "%.3Re\x00", cgbp)
	sicheck_sprintf(cgtls, "100000000000000000\x00", "%.Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "100000000000000000\x00", "%.0Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "100000000000000000.0\x00", "%.1Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "100000000000000000.000000\x00", "%'Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "100000000000000000.0\x00", "%'.1Rf\x00", cgbp)

	Xmpfr_ui_div(cgtls, cgbp, ppuint64(1), cgbp, ppint32(ecMPFR_RNDN)) /* x=1e-17 */
	sicheck_sprintf(cgtls, "1.000000000000000000000000000000000000000e-17\x00", "%Re\x00", cgbp)
	sicheck_sprintf(cgtls, "0.000000\x00", "%Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "1e-17\x00", "%Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.0\x00", "%.1RDf\x00", cgbp)
	sicheck_sprintf(cgtls, "0.0\x00", "%.1RZf\x00", cgbp)
	sicheck_sprintf(cgtls, "0.1\x00", "%.1RUf\x00", cgbp)
	sicheck_sprintf(cgtls, "0.1\x00", "%.1RYf\x00", cgbp)
	sicheck_sprintf(cgtls, "0\x00", "%.0RDf\x00", cgbp)
	sicheck_sprintf(cgtls, "0\x00", "%.0RZf\x00", cgbp)
	sicheck_sprintf(cgtls, "1\x00", "%.0RUf\x00", cgbp)
	sicheck_sprintf(cgtls, "1\x00", "%.0RYf\x00", cgbp)

	/* powers of 10 with 'g' style */
	Xmpfr_set_str(cgtls, cgbp, "10\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "10\x00", "%Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1e+01\x00", "%.0Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1e+01\x00", "%.1Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "10\x00", "%.2Rg\x00", cgbp)

	Xmpfr_ui_div(cgtls, cgbp, ppuint64(1), cgbp, ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0.1\x00", "%Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.1\x00", "%.0Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.1\x00", "%.1Rg\x00", cgbp)

	Xmpfr_set_str(cgtls, cgbp, "1000\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1000\x00", "%Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1e+03\x00", "%.0Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1e+03\x00", "%.3Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1000\x00", "%.4Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1e+03\x00", "%.3Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1000\x00", "%.4Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "    1e+03\x00", "%9.3Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "     1000\x00", "%9.4Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "00001e+03\x00", "%09.3Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "000001000\x00", "%09.4Rg\x00", cgbp)

	Xmpfr_ui_div(cgtls, cgbp, ppuint64(1), cgbp, ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0.001\x00", "%Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.001\x00", "%.0Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.001\x00", "%.1Rg\x00", cgbp)

	Xmpfr_set_str(cgtls, cgbp, "100000\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "100000\x00", "%Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1e+05\x00", "%.0Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1e+05\x00", "%.5Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "100000\x00", "%.6Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "            1e+05\x00", "%17.5Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "           100000\x00", "%17.6Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0000000000001e+05\x00", "%017.5Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "00000000000100000\x00", "%017.6Rg\x00", cgbp)

	Xmpfr_ui_div(cgtls, cgbp, ppuint64(1), cgbp, ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1e-05\x00", "%Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1e-05\x00", "%.0Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1e-05\x00", "%.1Rg\x00", cgbp)

	/* check rounding mode */
	Xmpfr_set_str(cgtls, cgbp, "0.0076\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0.007\x00", "%.3RDF\x00", cgbp)
	sicheck_sprintf(cgtls, "0.007\x00", "%.3RZF\x00", cgbp)
	sicheck_sprintf(cgtls, "0.008\x00", "%.3RF\x00", cgbp)
	sicheck_sprintf(cgtls, "0.008\x00", "%.3RUF\x00", cgbp)
	sicheck_sprintf(cgtls, "0.008\x00", "%.3RYF\x00", cgbp)
	sicheck_vsprintf(cgtls, "0.008\x00", "%.3R*F\x00", iqlibc.ppVaList(cgbp+104, ppint32(ecMPFR_RNDA), cgbp))

	/* check limit between %f-style and %g-style */
	Xmpfr_set_str(cgtls, cgbp, "0.0000999\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0.0001\x00", "%.0Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "9e-05\x00", "%.0RDg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.0001\x00", "%.1Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.0001\x00", "%.2Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "9.99e-05\x00", "%.3Rg\x00", cgbp)

	/* trailing zeros */
	Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-ppint32(1)), ppint64(-ppint32(15)), ppint32(ecMPFR_RNDN)) /* x=-2^-15 */
	sicheck_sprintf(cgtls, "-3.0517578125e-05\x00", "%.30Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "-3.051757812500000000000000000000e-05\x00", "%.30Re\x00", cgbp)
	sicheck_sprintf(cgtls, "-3.05175781250000000000000000000e-05\x00", "%#.30Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "-0.000030517578125000000000000000\x00", "%.30Rf\x00", cgbp)

	/* bug 20081023 */
	sicheck_sprintf(cgtls, "-3.0517578125e-05\x00", "%.30Rg\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "1.9999\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1.999900  \x00", "%-#10.7RG\x00", cgbp)
	sicheck_sprintf(cgtls, "1.9999    \x00", "%-10.7RG\x00", cgbp)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1.\x00", "%#.1Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1.   \x00", "%-#5.1Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "  1.0\x00", "%#5.2Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1.00000000000000000000000000000\x00", "%#.30Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "1\x00", "%.30Rg\x00", cgbp)
	{
		aa_p3 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv4 = 0
	}
	pp_ = ccv4
	sicheck_sprintf(cgtls, "0.\x00", "%#.1Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.   \x00", "%-#5.1Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "  0.0\x00", "%#5.2Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.00000000000000000000000000000\x00", "%#.30Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0\x00", "%.30Rg\x00", cgbp)

	/* following tests with precision 53 bits */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))

	/* Exponent zero has a plus sign */
	Xmpfr_set_str(cgtls, cgbp, "-9.95645044213728791504536275169812142849e-01\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "-1.0e+00\x00", "%- #0.1Re\x00", cgbp)

	/* Decimal point and no figure after it with '#' flag and 'G' style */
	Xmpfr_set_str(cgtls, cgbp, "-9.90597761233942053494e-01\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "-1.\x00", "%- #0.1RG\x00", cgbp)

	/* precision zero */
	Xmpfr_set_d(cgtls, cgbp, ppfloat64(9.5), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "9\x00", "%.0RDf\x00", cgbp)
	sicheck_sprintf(cgtls, "10\x00", "%.0RUf\x00", cgbp)

	Xmpfr_set_d(cgtls, cgbp, ppfloat64(19.5), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "19\x00", "%.0RDf\x00", cgbp)
	sicheck_sprintf(cgtls, "20\x00", "%.0RUf\x00", cgbp)

	Xmpfr_set_d(cgtls, cgbp, ppfloat64(99.5), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "99\x00", "%.0RDf\x00", cgbp)
	sicheck_sprintf(cgtls, "100\x00", "%.0RUf\x00", cgbp)

	Xmpfr_set_d(cgtls, cgbp, -iqlibc.ppFloat64FromFloat64(9.5), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "-10\x00", "%.0RDf\x00", cgbp)
	sicheck_sprintf(cgtls, "-10\x00", "%.0RYf\x00", cgbp)
	sicheck_sprintf(cgtls, "-10\x00", "%.0Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "-1e+01\x00", "%.0Re\x00", cgbp)
	sicheck_sprintf(cgtls, "-1e+01\x00", "%.0Rg\x00", cgbp)
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(1), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0\x00", "%.0Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "5e-01\x00", "%.0Re\x00", cgbp)
	sicheck_sprintf(cgtls, "0.5\x00", "%.0Rg\x00", cgbp)
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(3), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "2\x00", "%.0Rf\x00", cgbp)
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(5), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "2\x00", "%.0Rf\x00", cgbp)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(0x1f)), 0, ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0x1p+5\x00", "%.0Ra\x00", cgbp)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(3)), 0, ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1p+2\x00", "%.0Rb\x00", cgbp)

	/* round to next ten power with %f but not with %g */
	Xmpfr_set_str(cgtls, cgbp, "-6.64464380544039223686e-02\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "-0.1\x00", "%.1Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "-0.0\x00", "%.1RZf\x00", cgbp)
	sicheck_sprintf(cgtls, "-0.07\x00", "%.1Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "-0.06\x00", "%.1RZg\x00", cgbp)

	/* round to next ten power and do not remove trailing zeros */
	Xmpfr_set_str(cgtls, cgbp, "9.98429393291486722006e-02\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0.1\x00", "%#.1Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.10\x00", "%#.2Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "0.099\x00", "%#.2RZg\x00", cgbp)

	/* Halfway cases */
	Xmpfr_set_str(cgtls, cgbp, "1.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "2e+00\x00", "%.0Re\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "2.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "2e+00\x00", "%.0Re\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "9.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1e+01\x00", "%.0Re\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "1.25\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1.2e+00\x00", "%.1Re\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "1.75\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1.8e+00\x00", "%.1Re\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "-0.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "-0\x00", "%.0Rf\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "1.25\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1.2\x00", "%.1Rf\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "1.75\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1.8\x00", "%.1Rf\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "1.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "2\x00", "%.1Rg\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "2.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "2\x00", "%.1Rg\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "9.25\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "9.2\x00", "%.2Rg\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "9.75\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "9.8\x00", "%.2Rg\x00", cgbp)

	/* assertion failure in r6320 */
	Xmpfr_set_str(cgtls, cgbp, "-9.996\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "-10.0\x00", "%.1Rf\x00", cgbp)

	/* regression in MPFR 3.1.0 (bug introduced in r7761, fixed in r7931) */
	sicheck_sprintf(cgtls, "-10\x00", "%.2Rg\x00", cgbp)

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

func sihexadecimal(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aa_p tnmpfr_ptr
	var ccv1 ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_ = aa_p, ccv1

	Xmpfr_inits2(cgtls, ppint64(64), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

	/* special */
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_str)), "%Ra\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_str)), "%RUa\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_str)), "%RDa\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_uc_str)), "%RA\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_uc_str)), "%RYA\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_uc_str)), "%RZA\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_uc_str)), "%RNA\x00", cgbp)

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%Ra\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%RYa\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%RZa\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%RNa\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_uc_str)), "%RA\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_uc_str)), "%RUA\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_uc_str)), "%RDA\x00", cgbp)

	Xmpfr_set_nan(cgtls, cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_str)), "%Ra\x00", cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_uc_str)), "%RA\x00", cgbp)

	/* regular numbers */
	Xmpfr_set_str(cgtls, cgbp, "FEDCBA9.87654321\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	{
		aa_p = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDZ)
		ccv1 = 0
	}
	pp_ = ccv1

	/* simplest case right justified */
	sicheck_sprintf(cgtls, "   0xf.edcba987654321p+24\x00", "%25Ra\x00", cgbp)
	sicheck_sprintf(cgtls, "   0xf.edcba987654321p+24\x00", "%25RUa\x00", cgbp)
	sicheck_sprintf(cgtls, "   0xf.edcba987654321p+24\x00", "%25RDa\x00", cgbp)
	sicheck_sprintf(cgtls, "   0xf.edcba987654321p+24\x00", "%25RYa\x00", cgbp)
	sicheck_sprintf(cgtls, "   0xf.edcba987654321p+24\x00", "%25RZa\x00", cgbp)
	sicheck_sprintf(cgtls, "   0xf.edcba987654321p+24\x00", "%25RNa\x00", cgbp)
	sicheck_sprintf(cgtls, "                  0x1p+28\x00", "%25.0Ra\x00", cgbp)
	sicheck_sprintf(cgtls, "                   0x0p+0\x00", "%25.0Ra\x00", cgbp+32)
	sicheck_sprintf(cgtls, "                   0x0p+0\x00", "%25Ra\x00", cgbp+32)
	sicheck_sprintf(cgtls, "                  0x0.p+0\x00", "%#25Ra\x00", cgbp+32)
	/* sign or space, pad with leading zeros */
	sicheck_sprintf(cgtls, " 0X00F.EDCBA987654321P+24\x00", "% 025RA\x00", cgbp)
	sicheck_sprintf(cgtls, " 0X000000000000000001P+28\x00", "% 025.0RA\x00", cgbp)
	sicheck_sprintf(cgtls, " 0X0000000000000000000P+0\x00", "% 025.0RA\x00", cgbp+32)
	/* sign + or -, left justified */
	sicheck_sprintf(cgtls, "+0xf.edcba987654321p+24  \x00", "%+-25Ra\x00", cgbp)
	sicheck_sprintf(cgtls, "+0x1p+28                 \x00", "%+-25.0Ra\x00", cgbp)
	sicheck_sprintf(cgtls, "+0x0p+0                  \x00", "%+-25.0Ra\x00", cgbp+32)
	/* decimal point, left justified, precision and rounding parameter */
	sicheck_vsprintf(cgtls, "0XF.FP+24 \x00", "%#-10.*R*A\x00", iqlibc.ppVaList(cgbp+72, ppint32(1), ppint32(ecMPFR_RNDN), cgbp))
	sicheck_vsprintf(cgtls, "0X1.P+28  \x00", "%#-10.*R*A\x00", iqlibc.ppVaList(cgbp+72, 0, ppint32(ecMPFR_RNDN), cgbp))
	sicheck_vsprintf(cgtls, "0X0.P+0   \x00", "%#-10.*R*A\x00", iqlibc.ppVaList(cgbp+72, 0, ppint32(ecMPFR_RNDN), cgbp+32))
	/* sign or space */
	sicheck_sprintf(cgtls, " 0xf.eddp+24\x00", "% .3RNa\x00", cgbp)
	sicheck_sprintf(cgtls, " 0x1p+28\x00", "% .0RNa\x00", cgbp)
	/* sign + or -, decimal point, pad with leading zeros */
	sicheck_sprintf(cgtls, "+0X0F.EP+24\x00", "%0+#11.1RZA\x00", cgbp)
	sicheck_sprintf(cgtls, "+0X00F.P+24\x00", "%0+#11.0RZA\x00", cgbp)
	sicheck_sprintf(cgtls, "+0X000.0P+0\x00", "%0+#11.1RZA\x00", cgbp+32)
	/* pad with leading zero */
	sicheck_sprintf(cgtls, "0x0000f.edcba987654321p+24\x00", "%026RDa\x00", cgbp)
	sicheck_sprintf(cgtls, "0x0000000000000000000fp+24\x00", "%026.0RDa\x00", cgbp)
	/* sign or space, decimal point, left justified */
	sicheck_sprintf(cgtls, " 0XF.EP+24 \x00", "%- #11.1RDA\x00", cgbp)
	sicheck_sprintf(cgtls, " 0XF.P+24  \x00", "%- #11.0RDA\x00", cgbp)

	pp_ = Xmpfr_mul_si(cgtls, cgbp, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), ppint32(ecMPFR_RNDD))
	pp_ = Xmpfr_mul_si(cgtls, cgbp+32, cgbp+32, ppint64(-iqlibc.ppInt32FromInt32(1)), ppint32(ecMPFR_RNDD))

	/* sign + or - */
	sicheck_sprintf(cgtls, "-0xf.ep+24\x00", "%+10.1RUa\x00", cgbp)
	sicheck_sprintf(cgtls, "  -0xfp+24\x00", "%+10.0RUa\x00", cgbp)
	sicheck_sprintf(cgtls, "   -0x0p+0\x00", "%+10.0RUa\x00", cgbp+32)

	/* rounding bit is zero */
	Xmpfr_set_str(cgtls, cgbp, "0xF.7\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0XFP+0\x00", "%.0RNA\x00", cgbp)
	/* tie case in round to nearest mode */
	Xmpfr_set_str(cgtls, cgbp, "0x0.8800000000000000p+3\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0x9.p-1\x00", "%#.0RNa\x00", cgbp)
	Xmpfr_set_str(cgtls, cgbp, "-0x0.9800000000000000p+3\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "-0xap-1\x00", "%.0RNa\x00", cgbp)
	/* trailing zeros in fractional part */
	sicheck_sprintf(cgtls, "-0X4.C0000000000000000000P+0\x00", "%.20RNA\x00", cgbp)
	/* rounding bit is one and the first non zero bit is far away */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(1024))
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(29), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN))
	Xmpfr_nextabove(cgtls, cgbp)
	sicheck_sprintf(cgtls, "0XFP+0\x00", "%.0RNA\x00", cgbp)

	/* with more than one limb */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(300))
	Xmpfr_set_str(cgtls, cgbp, "0xf.fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0x1p+4 [300]\x00", "%.0RNa [300]\x00", cgbp)
	sicheck_sprintf(cgtls, "0xfp+0 [300]\x00", "%.0RZa [300]\x00", cgbp)
	sicheck_sprintf(cgtls, "0x1p+4 [300]\x00", "%.0RYa [300]\x00", cgbp)
	sicheck_sprintf(cgtls, "0xfp+0 [300]\x00", "%.0RDa [300]\x00", cgbp)
	sicheck_sprintf(cgtls, "0x1p+4 [300]\x00", "%.0RUa [300]\x00", cgbp)
	sicheck_sprintf(cgtls, "0x1.0000000000000000000000000000000000000000p+4\x00", "%.40RNa\x00", cgbp)
	sicheck_sprintf(cgtls, "0xf.ffffffffffffffffffffffffffffffffffffffffp+0\x00", "%.40RZa\x00", cgbp)
	sicheck_sprintf(cgtls, "0x1.0000000000000000000000000000000000000000p+4\x00", "%.40RYa\x00", cgbp)
	sicheck_sprintf(cgtls, "0xf.ffffffffffffffffffffffffffffffffffffffffp+0\x00", "%.40RDa\x00", cgbp)
	sicheck_sprintf(cgtls, "0x1.0000000000000000000000000000000000000000p+4\x00", "%.40RUa\x00", cgbp)

	Xmpfr_set_str(cgtls, cgbp, "0xf.7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0XFP+0\x00", "%.0RNA\x00", cgbp)
	sicheck_sprintf(cgtls, "0XFP+0\x00", "%.0RZA\x00", cgbp)
	sicheck_sprintf(cgtls, "0X1P+4\x00", "%.0RYA\x00", cgbp)
	sicheck_sprintf(cgtls, "0XFP+0\x00", "%.0RDA\x00", cgbp)
	sicheck_sprintf(cgtls, "0X1P+4\x00", "%.0RUA\x00", cgbp)
	sicheck_sprintf(cgtls, "0XF.8P+0\x00", "%.1RNA\x00", cgbp)
	sicheck_sprintf(cgtls, "0XF.7P+0\x00", "%.1RZA\x00", cgbp)
	sicheck_sprintf(cgtls, "0XF.8P+0\x00", "%.1RYA\x00", cgbp)
	sicheck_sprintf(cgtls, "0XF.7P+0\x00", "%.1RDA\x00", cgbp)
	sicheck_sprintf(cgtls, "0XF.8P+0\x00", "%.1RUA\x00", cgbp)

	/* do not round up to the next power of the base */
	Xmpfr_set_str(cgtls, cgbp, "0xf.fffffffffffffffffffffffffffffffffffffeffffffffffffffffffffffffffffffff\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "0xf.ffffffffffffffffffffffffffffffffffffff00p+0\x00", "%.40RNa\x00", cgbp)
	sicheck_sprintf(cgtls, "0xf.fffffffffffffffffffffffffffffffffffffeffp+0\x00", "%.40RZa\x00", cgbp)
	sicheck_sprintf(cgtls, "0xf.ffffffffffffffffffffffffffffffffffffff00p+0\x00", "%.40RYa\x00", cgbp)
	sicheck_sprintf(cgtls, "0xf.fffffffffffffffffffffffffffffffffffffeffp+0\x00", "%.40RDa\x00", cgbp)
	sicheck_sprintf(cgtls, "0xf.ffffffffffffffffffffffffffffffffffffff00p+0\x00", "%.40RUa\x00", cgbp)

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
}

func sibinary(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aa_p tnmpfr_ptr
	var ccv1 ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* z at bp+32 */ tnmpfr_t
	pp_, pp_ = aa_p, ccv1

	Xmpfr_inits2(cgtls, ppint64(64), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

	/* special */
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xpinf_str)), "%Rb\x00", cgbp)

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xminf_str)), "%Rb\x00", cgbp)

	Xmpfr_set_nan(cgtls, cgbp)
	sicheck_sprintf(cgtls, ppuintptr(iqunsafe.ppPointer(&Xnan_str)), "%Rb\x00", cgbp)

	/* regular numbers */
	Xmpfr_set_str(cgtls, cgbp, "1110010101.1001101\x00", ppint32(2), ppint32(ecMPFR_RNDN))
	{
		aa_p = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1

	/* simplest case: right justified */
	sicheck_sprintf(cgtls, "    1.1100101011001101p+9\x00", "%25Rb\x00", cgbp)
	sicheck_sprintf(cgtls, "                     0p+0\x00", "%25Rb\x00", cgbp+32)
	sicheck_sprintf(cgtls, "                    0.p+0\x00", "%#25Rb\x00", cgbp+32)
	/* sign or space, pad with leading zeros */
	sicheck_sprintf(cgtls, " 0001.1100101011001101p+9\x00", "% 025Rb\x00", cgbp)
	sicheck_sprintf(cgtls, " 000000000000000000000p+0\x00", "% 025Rb\x00", cgbp+32)
	/* sign + or -, left justified */
	sicheck_sprintf(cgtls, "+1.1100101011001101p+9   \x00", "%+-25Rb\x00", cgbp)
	sicheck_sprintf(cgtls, "+0p+0                    \x00", "%+-25Rb\x00", cgbp+32)
	/* sign or space */
	sicheck_sprintf(cgtls, " 1.110p+9\x00", "% .3RNb\x00", cgbp)
	sicheck_sprintf(cgtls, " 1.1101p+9\x00", "% .4RNb\x00", cgbp)
	sicheck_sprintf(cgtls, " 0.0000p+0\x00", "% .4RNb\x00", cgbp+32)
	/* sign + or -, decimal point, pad with leading zeros */
	sicheck_sprintf(cgtls, "+00001.1p+9\x00", "%0+#11.1RZb\x00", cgbp)
	sicheck_sprintf(cgtls, "+0001.0p+10\x00", "%0+#11.1RNb\x00", cgbp)
	sicheck_sprintf(cgtls, "+000000.p+0\x00", "%0+#11.0RNb\x00", cgbp+32)
	/* pad with leading zero */
	sicheck_sprintf(cgtls, "00001.1100101011001101p+9\x00", "%025RDb\x00", cgbp)
	/* sign or space, decimal point (unused), left justified */
	sicheck_sprintf(cgtls, " 1.1p+9    \x00", "%- #11.1RDb\x00", cgbp)
	sicheck_sprintf(cgtls, " 1.p+9     \x00", "%- #11.0RDb\x00", cgbp)
	sicheck_sprintf(cgtls, " 1.p+10    \x00", "%- #11.0RUb\x00", cgbp)
	sicheck_sprintf(cgtls, " 1.p+9     \x00", "%- #11.0RZb\x00", cgbp)
	sicheck_sprintf(cgtls, " 1.p+10    \x00", "%- #11.0RYb\x00", cgbp)
	sicheck_sprintf(cgtls, " 1.p+10    \x00", "%- #11.0RNb\x00", cgbp)

	pp_ = Xmpfr_mul_si(cgtls, cgbp, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), ppint32(ecMPFR_RNDD))
	pp_ = Xmpfr_mul_si(cgtls, cgbp+32, cgbp+32, ppint64(-iqlibc.ppInt32FromInt32(1)), ppint32(ecMPFR_RNDD))

	/* sign + or - */
	sicheck_sprintf(cgtls, "   -1.1p+9\x00", "%+10.1RUb\x00", cgbp)
	sicheck_sprintf(cgtls, "   -0.0p+0\x00", "%+10.1RUb\x00", cgbp+32)

	/* precision 0 */
	sicheck_sprintf(cgtls, "-1p+10\x00", "%.0RNb\x00", cgbp)
	sicheck_sprintf(cgtls, "-1p+10\x00", "%.0RDb\x00", cgbp)
	sicheck_sprintf(cgtls, "-1p+9\x00", "%.0RUb\x00", cgbp)
	sicheck_sprintf(cgtls, "-1p+9\x00", "%.0RZb\x00", cgbp)
	sicheck_sprintf(cgtls, "-1p+10\x00", "%.0RYb\x00", cgbp)
	/* round to next base power */
	sicheck_sprintf(cgtls, "-1.0p+10\x00", "%.1RNb\x00", cgbp)
	sicheck_sprintf(cgtls, "-1.0p+10\x00", "%.1RDb\x00", cgbp)
	sicheck_sprintf(cgtls, "-1.0p+10\x00", "%.1RYb\x00", cgbp)
	/* do not round to next base power */
	sicheck_sprintf(cgtls, "-1.1p+9\x00", "%.1RUb\x00", cgbp)
	sicheck_sprintf(cgtls, "-1.1p+9\x00", "%.1RZb\x00", cgbp)
	/* rounding bit is zero */
	sicheck_sprintf(cgtls, "-1.11p+9\x00", "%.2RNb\x00", cgbp)
	/* tie case in round to nearest mode */
	sicheck_sprintf(cgtls, "-1.1100101011001101p+9\x00", "%.16RNb\x00", cgbp)
	/* trailing zeros in fractional part */
	sicheck_sprintf(cgtls, "-1.110010101100110100000000000000p+9\x00", "%.30RNb\x00", cgbp)

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
}

func simixed(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aai, aak, aan1 ppint32
	var aarnd tnmpfr_rnd_t
	var pp_ /* mpf at bp+8 */ tnmpf_t
	var pp_ /* mpq at bp+32 */ tnmpq_t
	var pp_ /* mpz at bp+64 */ tnmpz_t
	var pp_ /* n2 at bp+0 */ ppint32
	var pp_ /* x at bp+80 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aai, aak, aan1, aarnd
	aai = ppint32(121)

	X__gmpf_init(cgtls, cgbp+8)
	X__gmpf_set_ui(cgtls, cgbp+8, ppuint64(40))
	X__gmpf_div_ui(cgtls, cgbp+8, cgbp+8, ppuint64(31)) /* mpf = 40.0 / 31.0 */
	X__gmpq_init(cgtls, cgbp+32)
	X__gmpq_set_ui(cgtls, cgbp+32, ppuint64(123456), ppuint64(4567890))
	Xmpfr_mpz_init(cgtls, cgbp+64)
	X__gmpz_fib_ui(cgtls, cgbp+64, ppuint64(64))
	Xmpfr_init(cgtls, cgbp+80)
	Xmpfr_set_str(cgtls, cgbp+80, "-12345678.875\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aarnd = ppint32(ecMPFR_RNDD)

	sicheck_vsprintf(cgtls, "121%\x00", "%i%%\x00", iqlibc.ppVaList(cgbp+120, aai))
	sicheck_vsprintf(cgtls, "121% -1.2345678875000000E+07\x00", "%i%% %RNE\x00", iqlibc.ppVaList(cgbp+120, aai, cgbp+80))
	sicheck_vsprintf(cgtls, "121, -12345679\x00", "%i, %.0Rf\x00", iqlibc.ppVaList(cgbp+120, aai, cgbp+80))
	sicheck_vsprintf(cgtls, "10610209857723, -1.2345678875000000e+07\x00", "%Zi, %R*e\x00", iqlibc.ppVaList(cgbp+120, cgbp+64, aarnd, cgbp+80))
	sicheck_vsprintf(cgtls, "-12345678.9, 121\x00", "%.1Rf, %i\x00", iqlibc.ppVaList(cgbp+120, cgbp+80, aai))
	sicheck_vsprintf(cgtls, "-12345678, 1e240/45b352\x00", "%.0R*f, %Qx\x00", iqlibc.ppVaList(cgbp+120, ppint32(ecMPFR_RNDZ), cgbp+80, cgbp+32))

	/* TODO: Systematically test with and without %n in check_vsprintf? */
	/* Do the test several times due to random parameters in check_vsprintf
	   and the use of %n. In r11501, n2 is incorrect (seems random) when
	   randsize <= 0, i.e. when the size argument of mpfr_vsnprintf is 0. */
	aak = 0
	for {
		if !(aak < ppint32(30)) {
			break
		}

		*(*ppint32)(iqunsafe.ppPointer(cgbp)) = -ppint32(17)
		/* If this value is obtained for n2 after the check_vsprintf call below,
		   this probably means that n2 has not been written as expected. */
		aan1 = sicheck_vsprintf(cgtls, "121, -12345678.875000000000, 1.290323\x00", "%i, %.*Rf, %Ff%n\x00", iqlibc.ppVaList(cgbp+120, aai, ppint32(12), cgbp+80, cgbp+8, cgbp))
		if aan1 != *(*ppint32)(iqunsafe.ppPointer(cgbp)) {

			Xprintf(cgtls, "error in number of characters written by mpfr_vsprintf for k = %d, randsize = %d\n\x00", iqlibc.ppVaList(cgbp+120, aak, Xrandsize))
			Xprintf(cgtls, "expected: %d\n\x00", iqlibc.ppVaList(cgbp+120, *(*ppint32)(iqunsafe.ppPointer(cgbp))))
			Xprintf(cgtls, "     got: %d\n\x00", iqlibc.ppVaList(cgbp+120, aan1))
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aak++
	}

	/* check invalid spec.spec */
	sicheck_vsprintf(cgtls, "%,\x00", "%,\x00", 0)
	sicheck_vsprintf(cgtls, "%3*Rg\x00", "%3*Rg\x00", 0)

	/* check empty format */
	sicheck_vsprintf(cgtls, "%\x00", "%\x00", 0)

	X__gmpf_clear(cgtls, cgbp+8)
	X__gmpq_clear(cgtls, cgbp+32)
	Xmpfr_mpz_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+80)
}

// C documentation
//
//	/* Check with locale "da_DK.utf8" or "da_DK".
//	   On most platforms, decimal point is ',' and thousands separator is '.';
//	   if this is not the case or if the locale does not exist, the test is not
//	   performed (and if the MPFR_CHECK_LOCALES environment variable is set,
//	   the program fails). */
func silocale_da_DK(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aa_p tnmpfr_srcptr
	var aap tnmpfr_prec_t
	var ccv1 ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_ = aa_p, aap, ccv1
	aap = ppint64(128)

	if Xsetlocale(cgtls, ppint32(mvLC_ALL), "da_DK.utf8\x00") == ppuintptr(0) && Xsetlocale(cgtls, ppint32(mvLC_ALL), "da_DK\x00") == ppuintptr(0) || iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer((*tslconv)(iqunsafe.ppPointer(Xlocaleconv(cgtls))).fddecimal_point))) != ppint32(',') || iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer((*tslconv)(iqunsafe.ppPointer(Xlocaleconv(cgtls))).fdthousands_sep))) != ppint32('.') {

		Xsetlocale(cgtls, ppint32(mvLC_ALL), "C\x00")

		if Xgetenv(cgtls, "MPFR_CHECK_LOCALES\x00") == iqlibc.ppUintptrFromInt32(0) {
			return
		}

		Xfprintf(cgtls, Xstderr, "Cannot test the da_DK locale (not found or inconsistent).\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_init2(cgtls, cgbp, aap)

	/* positive numbers */
	Xmpfr_set_str(cgtls, cgbp, "18993474.61279296875\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(59))
	{
		aa_p = cgbp
		ccv1 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
	}
	pp_ = ccv1

	/* simplest case right justified with thousands separator */
	sicheck_sprintf(cgtls, "1,899347461279296875000000000000000000000e+07\x00", "%'30Re\x00", cgbp)
	sicheck_sprintf(cgtls, "      1,899347461279296875e+07\x00", "%'30Re\x00", cgbp+32)
	sicheck_sprintf(cgtls, "                   1,89935e+07\x00", "%'30Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "        18.993.474,61279296875\x00", "%'30.19Rg\x00", cgbp)
	sicheck_sprintf(cgtls, "             18.993.474,612793\x00", "%'30Rf\x00", cgbp)

	/* sign or space, pad, thousands separator with leading zeros */
	sicheck_sprintf(cgtls, " 1,899347461279296875000000000000000000000E+07\x00", "%' 030RE\x00", cgbp)
	sicheck_sprintf(cgtls, " 000001,899347461279296875E+07\x00", "%' 030RE\x00", cgbp+32)
	sicheck_sprintf(cgtls, " 0000000000000000001,89935E+07\x00", "%' 030RG\x00", cgbp)
	sicheck_sprintf(cgtls, " 000000018.993.474,61279296875\x00", "%' 030.19RG\x00", cgbp)
	sicheck_sprintf(cgtls, " 00000000000018.993.474,612793\x00", "%' 030RF\x00", cgbp)

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(48)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_exp10(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "1000000000000000000000000000000000000000000000000\x00", "%.0Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "1.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000,\x00", "%'#.0Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "1.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000,0000\x00", "%'.4Rf\x00", cgbp)
	pp_ = Xmpfr_mul_ui(cgtls, cgbp, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(10)), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "10000000000000000000000000000000000000000000000000\x00", "%.0Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "10.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000,\x00", "%'#.0Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "10.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000,0000\x00", "%'.4Rf\x00", cgbp)
	pp_ = Xmpfr_mul_ui(cgtls, cgbp, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(10)), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "100000000000000000000000000000000000000000000000000\x00", "%.0Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "100.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000,\x00", "%'#.0Rf\x00", cgbp)
	sicheck_sprintf(cgtls, "100.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000.000,0000\x00", "%'.4Rf\x00", cgbp)

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)

	Xsetlocale(cgtls, ppint32(mvLC_ALL), "C\x00")
}

// C documentation
//
//	/* check concordance between mpfr_asprintf result with a regular mpfr float
//	   and with a regular double float */
func sirandom_double(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaflag, aaspecifier [6]ppuint8
	var aai, aaj, aajmax, aaprec, aaspec, aaxi, aayi, ccv24 ppint32
	var aaptr, aaptr_mpfr, ccv10, ccv11, ccv13, ccv14, ccv16, ccv17, ccv18, ccv20, ccv4, ccv6, ccv8 ppuintptr
	var aay, ccv2 ppfloat64
	var ccv12, ccv15, ccv19, ccv21, ccv5, ccv9 ppuint8
	var ccv22, ccv23 ppbool
	var pp_ /* fmt at bp+44 */ [11]ppuint8
	var pp_ /* fmt_mpfr at bp+32 */ [12]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* xs at bp+56 */ ppuintptr
	var pp_ /* ys at bp+64 */ ppuintptr
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaflag, aai, aaj, aajmax, aaprec, aaptr, aaptr_mpfr, aaspec, aaspecifier, aaxi, aay, aayi, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv2, ccv20, ccv21, ccv22, ccv23, ccv24, ccv4, ccv5, ccv6, ccv8, ccv9 /* regular double float (equal to x) */

	aaflag = [6]ppuint8{
		0: ppuint8('-'),
		1: ppuint8('+'),
		2: ppuint8(' '),
		3: ppuint8('#'),
		4: ppuint8('0'),
		5: ppuint8('\''),
	}
	/* no 'a': mpfr and glibc do not have the same semantic */
	aaspecifier = [6]ppuint8{
		0: ppuint8('e'),
		1: ppuint8('f'),
		2: ppuint8('g'),
		3: ppuint8('E'),
		4: ppuint8('f'),
		5: ppuint8('G'),
	}

	Xmpfr_init2(cgtls, cgbp, ppint64(mvLDBL_MANT_DIG))

	aai = 0
	for {
		if !(aai < ppint32(1000)) {
			break
		}

		/* 1. random double */
		for {

			aay = ppfloat64(Xrandlimb(cgtls)) / ppfloat64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1)))

			goto cg_3
		cg_3:
			;
			if aay > iqlibc.ppFloat64FromInt32(0) {
				ccv2 = aay
			} else {
				ccv2 = -aay
			}
			if !(ccv2 < ppfloat64(2.2250738585072014e-308)) {
				break
			}
		}

		if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
			aay = -aay
		}

		Xmpfr_set_d(cgtls, cgbp, aay, ppint32(ecMPFR_RNDN))
		if aay != Xmpfr_get_d(cgtls, cgbp, ppint32(ecMPFR_RNDN)) {
			/* conversion error: skip this one */
			goto cg_1
		}

		/* 2. build random format strings fmt_mpfr and fmt */
		aaptr_mpfr = cgbp + 32
		aaptr = cgbp + 44
		ccv4 = aaptr_mpfr
		aaptr_mpfr++
		ccv5 = iqlibc.ppUint8FromUint8('%')
		ccv6 = aaptr
		aaptr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv6)) = ccv5
		*(*ppuint8)(iqunsafe.ppPointer(ccv4)) = ccv5
		/* random specifier 'e', 'f', 'g', 'E', 'F', or 'G' */
		aaspec = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt32(6))
		/* random flags, but no ' flag with %e or with non-glibc */
		aajmax = ppint32(5)
		aaj = 0
		for {
			if !(aaj < aajmax) {
				break
			}

			if Xrandlimb(cgtls)%ppuint64(3) == ppuint64(0) {
				ccv8 = aaptr_mpfr
				aaptr_mpfr++
				ccv9 = aaflag[aaj]
				ccv10 = aaptr
				aaptr++
				*(*ppuint8)(iqunsafe.ppPointer(ccv10)) = ccv9
				*(*ppuint8)(iqunsafe.ppPointer(ccv8)) = ccv9
			}

			goto cg_7
		cg_7:
			;
			aaj++
		}
		ccv11 = aaptr_mpfr
		aaptr_mpfr++
		ccv12 = iqlibc.ppUint8FromUint8('.')
		ccv13 = aaptr
		aaptr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv13)) = ccv12
		*(*ppuint8)(iqunsafe.ppPointer(ccv11)) = ccv12
		ccv14 = aaptr_mpfr
		aaptr_mpfr++
		ccv15 = iqlibc.ppUint8FromUint8('*')
		ccv16 = aaptr
		aaptr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv16)) = ccv15
		*(*ppuint8)(iqunsafe.ppPointer(ccv14)) = ccv15
		ccv17 = aaptr_mpfr
		aaptr_mpfr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv17)) = ppuint8('R')
		ccv18 = aaptr_mpfr
		aaptr_mpfr++
		ccv19 = aaspecifier[aaspec]
		ccv20 = aaptr
		aaptr++
		*(*ppuint8)(iqunsafe.ppPointer(ccv20)) = ccv19
		*(*ppuint8)(iqunsafe.ppPointer(ccv18)) = ccv19
		ccv21 = iqlibc.ppUint8FromUint8('\000')
		*(*ppuint8)(iqunsafe.ppPointer(aaptr)) = ccv21
		*(*ppuint8)(iqunsafe.ppPointer(aaptr_mpfr)) = ccv21

		if ccv22 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppint64(aaptr)-tn__predefined_ptrdiff_t(cgbp+44) < iqlibc.ppInt64FromInt32(mvFMT_SIZE))), ppint64(1)) != 0; !ccv22 {
			Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1153), "ptr - fmt < 11\x00")
		}
		pp_ = ccv22 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv23 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ppint64(aaptr_mpfr)-tn__predefined_ptrdiff_t(cgbp+32) < iqlibc.ppInt64FromInt32(mvFMT_MPFR_SIZE))), ppint64(1)) != 0; !ccv23 {
			Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1154), "ptr_mpfr - fmt_mpfr < 12\x00")
		}
		pp_ = ccv23 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		/* advantage small precision */
		if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
			ccv24 = ppint32(10)
		} else {
			ccv24 = Xprec_max_printf
		}
		aaprec = ccv24
		aaprec = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt32(aaprec))

		/* 3. calls and checks */
		/* the double float case is handled by the libc asprintf through
		   gmp_asprintf */
		aaxi = Xmpfr_asprintf(cgtls, cgbp+56, cgbp+32, iqlibc.ppVaList(cgbp+80, aaprec, cgbp))
		aayi = Xmpfr_asprintf(cgtls, cgbp+64, cgbp+44, iqlibc.ppVaList(cgbp+80, aaprec, aay))

		/* test if XS and YS differ, beware that ISO C99 doesn't specify
		   the sign of a zero exponent (the C99 rationale says: "The sign
		   of a zero exponent in %e format is unspecified.  The committee
		   knows of different implementations and choose not to require
		   implementations to document their behavior in this case
		   (by making this be implementation defined behaviour).  Most
		   implementations use a "+" sign, e.g., 1.2e+00; but there is at
		   least one implementation that uses the sign of the unlimited
		   precision result, e.g., the 0.987 would be 9.87e-01, so could
		   end up as 1e-00 after rounding to one digit of precision."),
		   while mpfr always uses '+' */
		if aaxi != aayi || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 56)), *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))) != 0 && (aaspec == ppint32(1) || aaspec == ppint32(4) || (Xstrstr(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 56)), "e+00\x00") == iqlibc.ppUintptrFromInt32(0) || Xstrstr(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "e-00\x00") == iqlibc.ppUintptrFromInt32(0)) && (Xstrstr(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 56)), "E+00\x00") == iqlibc.ppUintptrFromInt32(0) || Xstrstr(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)), "E-00\x00") == iqlibc.ppUintptrFromInt32(0))) {

			Xmpfr_printf(cgtls, "Error in mpfr_asprintf(\"%s\", %d, %Re)\n\x00", iqlibc.ppVaList(cgbp+80, cgbp+32, aaprec, cgbp))
			Xprintf(cgtls, "expected: %s\n\x00", iqlibc.ppVaList(cgbp+80, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64))))
			Xprintf(cgtls, "     got: %s\n\x00", iqlibc.ppVaList(cgbp+80, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 56))))
			Xprintf(cgtls, "xi=%d yi=%d spec=%d\n\x00", iqlibc.ppVaList(cgbp+80, aaxi, aayi, aaspec))

			Xexit(cgtls, ppint32(1))
		}

		Xmpfr_free_str(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 56)))
		Xmpfr_free_str(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 64)))

		goto cg_1
	cg_1:
		;
		aai++
	}

	Xmpfr_clear(cgtls, cgbp)
}

func sibug20080610(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaxi, aayi ppint32
	var aay ppfloat64
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* xs at bp+32 */ ppuintptr
	var pp_ /* ys at bp+40 */ ppuintptr
	pp_, pp_, pp_ = aaxi, aay, aayi

	Xmpfr_init2(cgtls, cgbp, ppint64(mvLDBL_MANT_DIG))

	aay = -iqlibc.ppFloat64FromFloat64(0.9956450442137288)
	Xmpfr_set_d(cgtls, cgbp, aay, ppint32(ecMPFR_RNDN))

	aaxi = Xmpfr_asprintf(cgtls, cgbp+32, "%- #0.*Re\x00", iqlibc.ppVaList(cgbp+56, ppint32(1), cgbp))
	aayi = Xmpfr_asprintf(cgtls, cgbp+40, "%- #0.*e\x00", iqlibc.ppVaList(cgbp+56, ppint32(1), aay))

	if aaxi != aayi || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 40))) != 0 {

		Xprintf(cgtls, "Error in bug20080610\n\x00", 0)
		Xprintf(cgtls, "expected: %s\n\x00", iqlibc.ppVaList(cgbp+56, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 40))))
		Xprintf(cgtls, "     got: %s\n\x00", iqlibc.ppVaList(cgbp+56, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xprintf(cgtls, "xi=%d yi=%d\n\x00", iqlibc.ppVaList(cgbp+56, aaxi, aayi))

		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_free_str(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)))
	Xmpfr_free_str(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 40)))
	Xmpfr_clear(cgtls, cgbp)
}

func sibug20081214(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaxi, aayi ppint32
	var aay ppfloat64
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* xs at bp+32 */ ppuintptr
	var pp_ /* ys at bp+40 */ ppuintptr
	pp_, pp_, pp_ = aaxi, aay, aayi

	Xmpfr_init2(cgtls, cgbp, ppint64(mvLDBL_MANT_DIG))

	aay = -iqlibc.ppFloat64FromFloat64(0.990597761233942)
	Xmpfr_set_d(cgtls, cgbp, aay, ppint32(ecMPFR_RNDN))

	aaxi = Xmpfr_asprintf(cgtls, cgbp+32, "%- #0.*RG\x00", iqlibc.ppVaList(cgbp+56, ppint32(1), cgbp))
	aayi = Xmpfr_asprintf(cgtls, cgbp+40, "%- #0.*G\x00", iqlibc.ppVaList(cgbp+56, ppint32(1), aay))

	if aaxi != aayi || Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 40))) != 0 {

		Xmpfr_printf(cgtls, "Error in bug20081214\nmpfr_asprintf(\"%- #0.*Re\", 1, %Re)\n\x00", iqlibc.ppVaList(cgbp+56, cgbp))
		Xprintf(cgtls, "expected: %s\n\x00", iqlibc.ppVaList(cgbp+56, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 40))))
		Xprintf(cgtls, "     got: %s\n\x00", iqlibc.ppVaList(cgbp+56, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xprintf(cgtls, "xi=%d yi=%d\n\x00", iqlibc.ppVaList(cgbp+56, aaxi, aayi))

		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_free_str(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)))
	Xmpfr_free_str(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 40)))
	Xmpfr_clear(cgtls, cgbp)
}

func sibug20111102(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var pp_ /* s at bp+32 */ [100]ppuint8
	var pp_ /* t at bp+0 */ tnmpfr_t

	Xmpfr_init2(cgtls, cgbp, ppint64(84))
	Xmpfr_set_str(cgtls, cgbp, "999.99999999999999999999\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_sprintf(cgtls, cgbp+32, "%.20RNg\x00", iqlibc.ppVaList(cgbp+144, cgbp))
	if Xstrcmp(cgtls, cgbp+32, "1000\x00") != 0 {

		Xprintf(cgtls, "Error in bug20111102, expected 1000, got %s\n\x00", iqlibc.ppVaList(cgbp+144, cgbp+32))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* In particular, the following test makes sure that the rounding
//	 * for %Ra and %Rb is not done on the MPFR number itself (as it
//	 * would overflow). Note: it has been reported on comp.std.c that
//	 * some C libraries behave differently on %a, but this is a bug.
//	 */
func sicheck_emax_aux(cgtls *iqlibc.ppTLS, aae tnmpfr_exp_t) {
	cgbp := cgtls.ppAlloc(320)
	defer cgtls.ppFree(320)

	var aaemax tnmpfr_exp_t
	var aai ppint32
	var ccv1, ccv2, ccv3 ppbool
	var pp_ /* s1 at bp+32 */ ppuintptr
	var pp_ /* s2 at bp+40 */ [256]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aaemax, aai, ccv1, ccv2, ccv3

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aae <= iqlibc.ppInt64FromInt64(0x7fffffffffffffff))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1303), "e <= 0x7fffffffffffffffL\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaemax = X__gmpfr_emax
	Xset_emax(cgtls, aae)

	Xmpfr_init2(cgtls, cgbp, ppint64(16))

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	Xmpfr_nextbelow(cgtls, cgbp)

	aai = Xmpfr_asprintf(cgtls, cgbp+32, "%Ra %.2Ra\x00", iqlibc.ppVaList(cgbp+304, cgbp, cgbp))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aai > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1313), "i > 0\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_snprintf(cgtls, cgbp+40, ppuint64(256), "0x7.fff8p+%ld 0x8.00p+%ld\x00", iqlibc.ppVaList(cgbp+304, aae-ppint64(3), aae-ppint64(3)))

	if Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), cgbp+40) != 0 {

		Xprintf(cgtls, "Error in check_emax_aux for emax = \x00", 0)
		if aae > ppint64(0x7fffffffffffffff) {
			Xprintf(cgtls, "(>LONG_MAX)\n\x00", 0)
		} else {
			Xprintf(cgtls, "%ld\n\x00", iqlibc.ppVaList(cgbp+304, aae))
		}
		Xprintf(cgtls, "Expected '%s'\n\x00", iqlibc.ppVaList(cgbp+304, cgbp+40))
		Xprintf(cgtls, "Got      '%s'\n\x00", iqlibc.ppVaList(cgbp+304, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_free_str(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)))

	aai = Xmpfr_asprintf(cgtls, cgbp+32, "%Rb %.2Rb\x00", iqlibc.ppVaList(cgbp+304, cgbp, cgbp))

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aai > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1332), "i > 0\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_snprintf(cgtls, cgbp+40, ppuint64(256), "1.111111111111111p+%ld 1.00p+%ld\x00", iqlibc.ppVaList(cgbp+304, aae-ppint64(1), aae))

	if Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), cgbp+40) != 0 {

		Xprintf(cgtls, "Error in check_emax_aux for emax = \x00", 0)
		if aae > ppint64(0x7fffffffffffffff) {
			Xprintf(cgtls, "(>LONG_MAX)\n\x00", 0)
		} else {
			Xprintf(cgtls, "%ld\n\x00", iqlibc.ppVaList(cgbp+304, aae))
		}
		Xprintf(cgtls, "Expected %s\n\x00", iqlibc.ppVaList(cgbp+304, cgbp+40))
		Xprintf(cgtls, "Got      %s\n\x00", iqlibc.ppVaList(cgbp+304, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_free_str(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)))

	Xmpfr_clear(cgtls, cgbp)
	Xset_emax(cgtls, aaemax)
}

func sicheck_emax(cgtls *iqlibc.ppTLS) {

	sicheck_emax_aux(cgtls, ppint64(15))
	sicheck_emax_aux(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
}

func sicheck_emin_aux(cgtls *iqlibc.ppTLS, aae tnmpfr_exp_t) {
	cgbp := cgtls.ppAlloc(336)
	defer cgtls.ppFree(336)

	var aaemin tnmpfr_exp_t
	var aai ppint32
	var ccv1, ccv2, ccv3 ppbool
	var pp_ /* ee at bp+296 */ tnmpz_t
	var pp_ /* s1 at bp+32 */ ppuintptr
	var pp_ /* s2 at bp+40 */ [256]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aaemin, aai, ccv1, ccv2, ccv3

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aae >= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1370), "e >= (-0x7fffffffffffffffL-1)\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, aae)

	Xmpfr_init2(cgtls, cgbp, ppint64(16))
	Xmpfr_mpz_init(cgtls, cgbp+296)

	Xmpfr_setmin(cgtls, cgbp, aae)
	X__gmpz_set_si(cgtls, cgbp+296, aae)
	X__gmpz_sub_ui(cgtls, cgbp+296, cgbp+296, ppuint64(1))

	aai = Xmpfr_asprintf(cgtls, cgbp+32, "%Ra\x00", iqlibc.ppVaList(cgbp+320, cgbp))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aai > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1382), "i > 0\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	X__gmp_snprintf(cgtls, cgbp+40, ppuint64(256), "0x1p%Zd\x00", iqlibc.ppVaList(cgbp+320, cgbp+296))

	if Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), cgbp+40) != 0 {

		Xprintf(cgtls, "Error in check_emin_aux for emin = %ld\n\x00", iqlibc.ppVaList(cgbp+320, aae))
		Xprintf(cgtls, "Expected %s\n\x00", iqlibc.ppVaList(cgbp+320, cgbp+40))
		Xprintf(cgtls, "Got      %s\n\x00", iqlibc.ppVaList(cgbp+320, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_free_str(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)))

	aai = Xmpfr_asprintf(cgtls, cgbp+32, "%Rb\x00", iqlibc.ppVaList(cgbp+320, cgbp))

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aai > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1397), "i > 0\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	X__gmp_snprintf(cgtls, cgbp+40, ppuint64(256), "1p%Zd\x00", iqlibc.ppVaList(cgbp+320, cgbp+296))

	if Xstrcmp(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)), cgbp+40) != 0 {

		Xprintf(cgtls, "Error in check_emin_aux for emin = %ld\n\x00", iqlibc.ppVaList(cgbp+320, aae))
		Xprintf(cgtls, "Expected %s\n\x00", iqlibc.ppVaList(cgbp+320, cgbp+40))
		Xprintf(cgtls, "Got      %s\n\x00", iqlibc.ppVaList(cgbp+320, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_free_str(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(cgbp + 32)))

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_mpz_clear(cgtls, cgbp+296)
	Xset_emin(cgtls, aaemin)
}

func sicheck_emin(cgtls *iqlibc.ppTLS) {

	sicheck_emin_aux(cgtls, ppint64(-ppint32(15)))
	sicheck_emin_aux(cgtls, X__gmpfr_emin)
	sicheck_emin_aux(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
}

func sitest20161214(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aar ppint32
	var ccv1, ccv2, ccv3 ppbool
	var pp_ /* buf at bp+32 */ [32]ppuint8
	var pp_ /* s at bp+64 */ [25]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aar, ccv1, ccv2, ccv3
	*(*[25]ppuint8)(iqunsafe.ppPointer(cgbp + 64)) = [25]ppuint8{'0', 'x', '0', '.', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', '8', 'p', '+', '1', '0', '2', '4'}

	Xmpfr_init2(cgtls, cgbp, ppint64(64))
	Xmpfr_set_str(cgtls, cgbp, cgbp+64, ppint32(16), ppint32(ecMPFR_RNDN))
	aar = Xmpfr_snprintf(cgtls, cgbp+32, ppuint64(32), "%.*RDf\x00", iqlibc.ppVaList(cgbp+104, -ppint32(2), cgbp))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aar == iqlibc.ppInt32FromInt32(316))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1435), "r == 316\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aar = Xmpfr_snprintf(cgtls, cgbp+32, ppuint64(32), "%.*RDf\x00", iqlibc.ppVaList(cgbp+104, -iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fffffff)+iqlibc.ppInt32FromInt32(1), cgbp))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aar == iqlibc.ppInt32FromInt32(316))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1437), "r == 316\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aar = Xmpfr_snprintf(cgtls, cgbp+32, ppuint64(32), "%.*RDf\x00", iqlibc.ppVaList(cgbp+104, -iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fffffff), cgbp))

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aar == iqlibc.ppInt32FromInt32(316))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1439), "r == 316\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* http://gforge.inria.fr/tracker/index.php?func=detail&aid=21056 */
func sibug21056(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aandigits, aar ppint32
	var ccv1, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7 ppbool
	var pp_ /* s at bp+32 */ [25]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aandigits, aar, ccv1, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7
	*(*[25]ppuint8)(iqunsafe.ppPointer(cgbp + 32)) = [25]ppuint8{'0', 'x', '0', '.', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', 'f', '8', 'p', '+', '1', '0', '2', '4'}

	Xmpfr_init2(cgtls, cgbp, ppint64(64))

	Xmpfr_set_str(cgtls, cgbp, cgbp+32, ppint32(16), ppint32(ecMPFR_RNDN))

	aandigits = ppint32(1000)
	aar = Xmpfr_snprintf(cgtls, ppuintptr(0), ppuint64(0), "%.*RDf\x00", iqlibc.ppVaList(cgbp+72, aandigits, cgbp))
	/* the return value should be ndigits + 310 */

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aar == aandigits+iqlibc.ppInt32FromInt32(310))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1458), "r == ndigits + 310\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aandigits = iqlibc.ppInt32FromInt32(mvINT_MAX) - iqlibc.ppInt32FromInt32(310)
	aar = Xmpfr_snprintf(cgtls, ppuintptr(0), ppuint64(0), "%.*RDf\x00", iqlibc.ppVaList(cgbp+72, aandigits, cgbp))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aar == iqlibc.ppInt32FromInt32(mvINT_MAX))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1462), "r == 0x7fffffff\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aandigits = iqlibc.ppInt32FromInt32(mvINT_MAX) - iqlibc.ppInt32FromInt32(10)
	aar = Xmpfr_snprintf(cgtls, ppuintptr(0), ppuint64(0), "%.*RDa\x00", iqlibc.ppVaList(cgbp+72, aandigits, cgbp))

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aar == iqlibc.ppInt32FromInt32(mvINT_MAX))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1466), "r == 0x7fffffff\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aandigits = iqlibc.ppInt32FromInt32(mvINT_MAX) - iqlibc.ppInt32FromInt32(7)
	aar = Xmpfr_snprintf(cgtls, ppuintptr(0), ppuint64(0), "%.*RDe\x00", iqlibc.ppVaList(cgbp+72, aandigits, cgbp))

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aar == iqlibc.ppInt32FromInt32(mvINT_MAX))), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1470), "r == 0x7fffffff\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aandigits = ppint32(1000)
	aar = Xmpfr_snprintf(cgtls, ppuintptr(0), ppuint64(0), "%.*RDg\x00", iqlibc.ppVaList(cgbp+72, aandigits, cgbp))
	/* since trailing zeros are removed with %g, we get less digits */

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aar == iqlibc.ppInt32FromInt32(309))), ppint64(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1475), "r == 309\x00")
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aandigits = ppint32(mvINT_MAX)
	aar = Xmpfr_snprintf(cgtls, ppuintptr(0), ppuint64(0), "%.*RDg\x00", iqlibc.ppVaList(cgbp+72, aandigits, cgbp))
	/* since trailing zeros are removed with %g, we get less digits */

	if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aar == iqlibc.ppInt32FromInt32(309))), ppint64(1)) != 0; !ccv6 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1480), "r == 309\x00")
	}
	pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	aandigits = iqlibc.ppInt32FromInt32(mvINT_MAX) - iqlibc.ppInt32FromInt32(1)
	aar = Xmpfr_snprintf(cgtls, ppuintptr(0), ppuint64(0), "%#.*RDg\x00", iqlibc.ppVaList(cgbp+72, aandigits, cgbp))

	if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aar == aandigits+iqlibc.ppInt32FromInt32(1))), ppint64(1)) != 0; !ccv7 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1484), "r == ndigits + 1\x00")
	}
	pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* Fails for i = 5, i.e. t[i] = (size_t) UINT_MAX + 1,
//	   with r11427 on 64-bit machines (4-byte int, 8-byte size_t).
//	   On such machines, t[5] converted to int typically gives 0.
//	   Note: the assumed behavior corresponds to the snprintf behavior
//	   in ISO C, but this conflicts with POSIX:
//	     https://sourceware.org/bugzilla/show_bug.cgi?id=14771#c2
//	     https://austingroupbugs.net/view.php?id=761
//	     https://austingroupbugs.net/view.php?id=1219
//	     https://gcc.gnu.org/bugzilla/show_bug.cgi?id=87096
//	   Fixed in r11429.
//	*/
func sisnprintf_size(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aai, aar ppint32
	var aat [8]tnsize_t
	var pp_ /* buf at bp+32 */ [12]ppuint8
	var pp_ /* s at bp+44 */ [12]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_ = aai, aar, aat
	*(*[12]ppuint8)(iqunsafe.ppPointer(cgbp + 44)) = [12]ppuint8{'1', '7', '.', '0', '0', '0', '0', '0', '0', '0', '0'}
	aat = [8]tnsize_t{
		0: ppuint64(11),
		1: ppuint64(12),
		2: ppuint64(64),
		3: ppuint64(mvINT_MAX),
		4: iqlibc.ppUint64FromInt32(mvINT_MAX) + iqlibc.ppUint64FromInt32(1),
		5: iqlibc.ppUint64FromUint32(0xffffffff) + iqlibc.ppUint64FromInt32(1),
		6: iqlibc.ppUint64FromUint32(0xffffffff) + iqlibc.ppUint64FromInt32(2),
		7: iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1)),
	}

	Xmpfr_init2(cgtls, cgbp, ppint64(64))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(17)), 0, ppint32(ecMPFR_RNDN))

	aai = 0
	for {
		if !(iqlibc.ppUint64FromInt32(aai) < iqlibc.ppUint64FromInt64(64)/iqlibc.ppUint64FromInt64(8)) {
			break
		}

		Xmemset(cgtls, cgbp+32, 0, ppuint64(12))
		/* r = snprintf (buf, t[i], "%.8f", 17.0); */
		aar = Xmpfr_snprintf(cgtls, cgbp+32, aat[aai], "%.8Rf\x00", iqlibc.ppVaList(cgbp+64, cgbp))
		if aar != ppint32(11) || aat[aai] > ppuint64(11) && Xstrcmp(cgtls, cgbp+32, cgbp+44) != 0 {

			Xprintf(cgtls, "Error in snprintf_size for i = %d:\n\x00", iqlibc.ppVaList(cgbp+64, aai))
			Xprintf(cgtls, "expected r = 11, \"%s\"\n\x00", iqlibc.ppVaList(cgbp+64, cgbp+44))
			Xprintf(cgtls, "got      r = %d, \"%s\"\n\x00", iqlibc.ppVaList(cgbp+64, aar, cgbp+32))
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* With r11516, n2 gets a random value for i = 0 only!
//	   valgrind detects a problem for "nchar = buf.curr - buf.start;"
//	   in the spec.spec == 'n' case. Indeed, there is no buffer when
//	   size is 0. */
func sipercent_n(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaerr, aai, aaj, aan1 ppint32
	var pp_ /* buffer at bp+4 */ [64]ppuint8
	var pp_ /* n2 at bp+0 */ ppint32
	pp_, pp_, pp_, pp_ = aaerr, aai, aaj, aan1
	aaerr = 0

	aai = 0
	for {
		if !(aai < ppint32(24)) {
			break
		}
		aaj = 0
		for {
			if !(aaj < ppint32(3)) {
				break
			}

			Xmemset(cgtls, cgbp+4, 0, ppuint64(64))
			*(*ppint32)(iqunsafe.ppPointer(cgbp)) = -ppint32(17)
			aan1 = Xmpfr_snprintf(cgtls, cgbp+4, iqlibc.ppUint64FromInt32(aai%ppint32(8)), "%d%n\x00", iqlibc.ppVaList(cgbp+80, ppint32(123), cgbp))
			if aan1 != ppint32(3) || *(*ppint32)(iqunsafe.ppPointer(cgbp)) != ppint32(3) {

				Xprintf(cgtls, "Error 1 in percent_n: i = %d, n1 = %d, n2 = %d\n\x00", iqlibc.ppVaList(cgbp+80, aai, aan1, *(*ppint32)(iqunsafe.ppPointer(cgbp))))
				aaerr = ppint32(1)
			}

			goto cg_2
		cg_2:
			;
			aaj++
		}
		goto cg_1
	cg_1:
		;
		aai++
	}

	if aaerr != 0 {
		Xexit(cgtls, ppint32(1))
	}
}

type tsclo = struct {
	fdfmt   ppuintptr
	fdwidth ppint32
	fdr     ppint32
	fde     ppint32
}

func sicheck_length_overflow(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(384)
	defer cgtls.ppFree(384)

	var aa_p tnmpfr_ptr
	var aae, aai, aar, ccv1, ccv4, ccv5 ppint32
	var ccv3 ppbool
	var pp_ /* t at bp+32 */ [13]tsclo
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aae, aai, aar, ccv1, ccv3, ccv4, ccv5
	*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)) = [13]tsclo{
		0: {
			fdfmt: "%Rg\x00",
			fdr:   ppint32(1),
		},
		1: {
			fdfmt:   "%*Rg\x00",
			fdwidth: ppint32(1),
			fdr:     ppint32(1),
		},
		2: {
			fdfmt:   "%*Rg\x00",
			fdwidth: -ppint32(1),
			fdr:     ppint32(1),
		},
		3: {
			fdfmt: "%5Rg\x00",
			fdr:   ppint32(5),
		},
		4: {
			fdfmt:   "%*Rg\x00",
			fdwidth: ppint32(5),
			fdr:     ppint32(5),
		},
		5: {
			fdfmt:   "%*Rg\x00",
			fdwidth: -ppint32(5),
			fdr:     ppint32(5),
		},
		6: {
			fdfmt: "%2147483647Rg\x00",
			fdr:   ppint32(2147483647),
		},
		7: {
			fdfmt: "%2147483647Rg \x00",
			fdr:   -ppint32(1),
			fde:   ppint32(1),
		},
		8: {
			fdfmt: "%2147483648Rg\x00",
			fdr:   -ppint32(1),
			fde:   ppint32(1),
		},
		9: {
			fdfmt: "%18446744073709551616Rg\x00",
			fdr:   -ppint32(1),
			fde:   ppint32(1),
		},
		10: {
			fdfmt:   "%*Rg\x00",
			fdwidth: ppint32(2147483647),
			fdr:     ppint32(2147483647),
		},
		11: {
			fdfmt:   "%*Rg\x00",
			fdwidth: -ppint32(2147483647),
			fdr:     ppint32(2147483647),
		},
		12: {
			fdfmt:   "%*Rg\x00",
			fdwidth: -iqlibc.ppInt32FromInt32(1) - iqlibc.ppInt32FromInt32(0x7fffffff),
			fdr:     -ppint32(1),
			fde:     ppint32(1),
		},
	}

	Xmpfr_init2(cgtls, cgbp, ppint64(mvMPFR_PREC_MIN))
	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1

	aai = 0
	for {
		if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(cgbp+32 == cgbp+32)), ppint64(1)) != 0; !ccv3 {
			Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1595), "(void *) &(t) == (void *) &(t)[0]\x00")
		}
		pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if !(ppint64(aai) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(312)/iqlibc.ppUint64FromInt64(24))) {
			break
		}

		*(*ppint32)(iqunsafe.ppPointer(X__errno_location(cgtls))) = 0
		if (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fdwidth == 0 {
			ccv4 = Xmpfr_snprintf(cgtls, iqlibc.ppUintptrFromInt32(0), ppuint64(0), (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fdfmt, iqlibc.ppVaList(cgbp+352, cgbp))
		} else {
			ccv4 = Xmpfr_snprintf(cgtls, iqlibc.ppUintptrFromInt32(0), ppuint64(0), (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fdfmt, iqlibc.ppVaList(cgbp+352, (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fdwidth, cgbp))
		}
		aar = ccv4
		aae = *(*ppint32)(iqunsafe.ppPointer(X__errno_location(cgtls)))
		if (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fdr < 0 {
			ccv5 = iqlibc.ppBoolInt32(aar >= 0)
		} else {
			ccv5 = iqlibc.ppBoolInt32(aar != (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fdr)
		}
		if ccv5 != 0 || (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fde != 0 && aae != ppint32(mvEOVERFLOW) {

			Xprintf(cgtls, "Error in check_length_overflow for i=%d (%s %d)\n\x00", iqlibc.ppVaList(cgbp+352, aai, (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fdfmt, (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fdwidth))
			Xprintf(cgtls, "Expected r=%d, got r=%d\n\x00", iqlibc.ppVaList(cgbp+352, (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fdr, aar))
			if (*(*[13]tsclo)(iqunsafe.ppPointer(cgbp + 32)))[aai].fde != 0 && aae != ppint32(mvEOVERFLOW) {
				Xprintf(cgtls, "Expected errno=EOVERFLOW=%d, got errno=%d\n\x00", iqlibc.ppVaList(cgbp+352, ppint32(mvEOVERFLOW), aae))
			}
			Xexit(cgtls, ppint32(1))
		}

		goto cg_2
	cg_2:
		;
		aai++
	}

	Xmpfr_clear(cgtls, cgbp)
}

/* The following tests should be equivalent to those from test_locale()
   in tprintf.c (remove the \n at the end of the test strings). */

func sitest_locale(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(272)
	defer cgtls.ppFree(272)

	var aai, aaj, aaj1, ccv3 ppint32
	var aas, ccv12, ccv8 ppuintptr
	var ccv2, ccv4, ccv5 ppbool
	var pp_ /* buf at bp+178 */ [64]ppuint8
	var pp_ /* buf at bp+90 */ [64]ppuint8
	var pp_ /* s at bp+154 */ [24]ppuint8
	var pp_ /* tab_locale at bp+0 */ [4]ppuintptr
	var pp_ /* v at bp+64 */ [26]ppuint8
	var pp_ /* x at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aai, aaj, aaj1, aas, ccv12, ccv2, ccv3, ccv4, ccv5, ccv8
	*(*[4]ppuintptr)(iqunsafe.ppPointer(cgbp)) = [4]ppuintptr{
		0: "en_US\x00",
		1: "en_US.iso88591\x00",
		2: "en_US.iso885915\x00",
		3: "en_US.utf8\x00",
	}
	*(*[26]ppuint8)(iqunsafe.ppPointer(cgbp + 64)) = [26]ppuint8{'9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', '.', '5'}

	aai = 0
	for {
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(cgbp == cgbp)), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1641), "(void *) &(tab_locale) == (void *) &(tab_locale)[0]\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if !(ppint64(aai) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(32)/iqlibc.ppUint64FromInt64(8))) {
			break
		}

		aas = Xsetlocale(cgtls, ppint32(mvLC_ALL), (*(*[4]ppuintptr)(iqunsafe.ppPointer(cgbp)))[aai])

		if ccv4 = aas != iqlibc.ppUintptrFromInt32(0); ccv4 {
			if iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer((*tslconv)(iqunsafe.ppPointer(Xlocaleconv(cgtls))).fdthousands_sep))) == ppint32('\000') || iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer((*tslconv)(iqunsafe.ppPointer(Xlocaleconv(cgtls))).fdthousands_sep + 1))) != ppint32('\000') {
				ccv3 = iqlibc.ppInt32FromUint8(iqlibc.ppUint8FromUint8('\000'))
			} else {
				ccv3 = iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer((*tslconv)(iqunsafe.ppPointer(Xlocaleconv(cgtls))).fdthousands_sep)))
			}
		}
		if ccv4 && ccv3 == ppint32(',') {
			break
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(cgbp == cgbp)), ppint64(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tsprintf.c\x00", ppint32(1651), "(void *) &(tab_locale) == (void *) &(tab_locale)[0]\x00")
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ppint64(aai) == iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(32)/iqlibc.ppUint64FromInt64(8)) {

		if Xgetenv(cgtls, "MPFR_CHECK_LOCALES\x00") == iqlibc.ppUintptrFromInt32(0) {
			return
		}

		Xfprintf(cgtls, Xstderr, "Cannot find a locale with ',' thousands separator.\nPlease install one of the en_US based locales.\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_init2(cgtls, cgbp+32, ppint64(113))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(10000)), 0, ppint32(ecMPFR_RNDN))

	sicheck_sprintf(cgtls, "(1) 10000=10,000 \x00", "(1) 10000=%'Rg \x00", cgbp+32)
	sicheck_sprintf(cgtls, "(2) 10000=10,000.000000 \x00", "(2) 10000=%'Rf \x00", cgbp+32)

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1000)), 0, ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "(3) 1000=1,000.000000 \x00", "(3) 1000=%'Rf \x00", cgbp+32)

	aai = ppint32(1)
	for {
		if !(iqlibc.ppUint64FromInt32(aai) <= iqlibc.ppUint64FromInt64(26)-iqlibc.ppUint64FromInt32(3)) {
			break
		}

		Xstrcpy(cgtls, cgbp+90, "(4) 10^i=1\x00")
		aaj = aai
		for {
			if !(aaj > 0) {
				break
			}
			if aaj%ppint32(3) == 0 {
				ccv8 = ",0\x00"
			} else {
				ccv8 = "0\x00"
			}
			Xstrcat(cgtls, cgbp+90, ccv8)
			goto cg_7
		cg_7:
			;
			aaj--
		}
		Xstrcat(cgtls, cgbp+90, " \x00")
		Xmpfr_set_str(cgtls, cgbp+32, cgbp+64+ppuintptr(26)-ppuintptr(3)-ppuintptr(aai), ppint32(10), ppint32(ecMPFR_RNDN))
		sicheck_sprintf(cgtls, cgbp+90, "(4) 10^i=%'.0Rf \x00", cgbp+32)

		goto cg_6
	cg_6:
		;
		aai++
	}

	aai = ppint32(1)
	for {
		if !(aai <= ppint32(mvN0)) {
			break
		}

		(*(*[24]ppuint8)(iqunsafe.ppPointer(cgbp + 154)))[0] = ppuint8('1')
		aaj1 = ppint32(1)
		for {
			if !(aaj1 <= aai) {
				break
			}
			(*(*[24]ppuint8)(iqunsafe.ppPointer(cgbp + 154)))[aaj1] = ppuint8('0')
			goto cg_10
		cg_10:
			;
			aaj1++
		}
		(*(*[24]ppuint8)(iqunsafe.ppPointer(cgbp + 154)))[aai+ppint32(1)] = ppuint8('\000')

		Xstrcpy(cgtls, cgbp+178, "(5) 10^i=1\x00")
		aaj1 = aai
		for {
			if !(aaj1 > 0) {
				break
			}
			if aaj1%ppint32(3) == 0 {
				ccv12 = ",0\x00"
			} else {
				ccv12 = "0\x00"
			}
			Xstrcat(cgtls, cgbp+178, ccv12)
			goto cg_11
		cg_11:
			;
			aaj1--
		}
		Xstrcat(cgtls, cgbp+178, " \x00")

		Xmpfr_set_str(cgtls, cgbp+32, cgbp+154, ppint32(10), ppint32(ecMPFR_RNDN))

		sicheck_sprintf(cgtls, cgbp+178, "(5) 10^i=%'.0RNf \x00", cgbp+32)
		sicheck_sprintf(cgtls, cgbp+178, "(5) 10^i=%'.0RZf \x00", cgbp+32)
		sicheck_sprintf(cgtls, cgbp+178, "(5) 10^i=%'.0RUf \x00", cgbp+32)
		sicheck_sprintf(cgtls, cgbp+178, "(5) 10^i=%'.0RDf \x00", cgbp+32)
		sicheck_sprintf(cgtls, cgbp+178, "(5) 10^i=%'.0RYf \x00", cgbp+32)

		Xstrcat(cgtls, cgbp+154+ppuintptr(aai+iqlibc.ppInt32FromInt32(1)), ".5\x00")
		sicheck_sprintf(cgtls, cgbp+178, "(5) 10^i=%'.0Rf \x00", cgbp+32)

		goto cg_9
	cg_9:
		;
		aai++
	}

	Xmpfr_set_str(cgtls, cgbp+32, "1000\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	sicheck_sprintf(cgtls, "00000001e+03\x00", "%'012.3Rg\x00", cgbp+32)
	sicheck_sprintf(cgtls, "00000001,000\x00", "%'012.4Rg\x00", cgbp+32)
	sicheck_sprintf(cgtls, "000000001,000\x00", "%'013.4Rg\x00", cgbp+32)

	sicheck_vsprintf(cgtls, "+01,234,567  :\x00", "%0+ -'13.10Pd:\x00", iqlibc.ppVaList(cgbp+256, iqlibc.ppInt64FromInt32(1234567)))

	Xmpfr_clear(cgtls, cgbp+32)
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	var aak ppint32
	pp_ = aak

	Xtests_start_mpfr(cgtls)

	/* currently, we just check with 'C' locale */
	Xsetlocale(cgtls, ppint32(mvLC_ALL), "C\x00")

	sibug20111102(cgtls)

	aak = 0
	for {
		if !(aak < ppint32(40)) {
			break
		}

		sinative_types(cgtls)
		sihexadecimal(cgtls)
		sibinary(cgtls)
		sidecimal(cgtls)

		silocale_da_DK(cgtls)

		goto cg_1
	cg_1:
		;
		aak++
	}

	sicheck_emax(cgtls)
	sicheck_emin(cgtls)
	sitest20161214(cgtls)
	sibug21056(cgtls)
	sisnprintf_size(cgtls)
	sipercent_n(cgtls)
	simixed(cgtls)
	sicheck_length_overflow(cgtls)
	sitest_locale(cgtls)

	if Xgetenv(cgtls, "MPFR_CHECK_LIBC_PRINTF\x00") != 0 {

		/* check against libc */
		sirandom_double(cgtls)
		sibug20081214(cgtls)
		sibug20080610(cgtls)
	}

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___errno_location(*iqlibc.ppTLS) ppuintptr

func ___gmp_snprintf(*iqlibc.ppTLS, ppuintptr, ppuint64, ppuintptr, ppuintptr) ppint32

func ___gmp_sprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func ___gmpf_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_div_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func ___gmpf_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpf_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint64)

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

func ___gmpfr_vsnprintf(*iqlibc.ppTLS, ppuintptr, ppuint64, ppuintptr, ppuintptr) ppint32

func ___gmpfr_vsprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func ___gmpq_clear(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_init(*iqlibc.ppTLS, ppuintptr)

func ___gmpq_set_ui(*iqlibc.ppTLS, ppuintptr, ppuint64, ppuint64)

func ___gmpz_fib_ui(*iqlibc.ppTLS, ppuintptr, ppuint64)

func ___gmpz_set_si(*iqlibc.ppTLS, ppuintptr, ppint64)

func ___gmpz_sub_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64)

func _exit(*iqlibc.ppTLS, ppint32)

func _fprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _localeconv(*iqlibc.ppTLS) ppuintptr

func _memset(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64) ppuintptr

func _mpfr_asprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_exp10(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_free_str(*iqlibc.ppTLS, ppuintptr)

func _mpfr_get_d(*iqlibc.ppTLS, ppuintptr, ppint32) ppfloat64

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_inits2(*iqlibc.ppTLS, ppint64, ppuintptr, ppuintptr)

func _mpfr_mpz_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mpz_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mul_si(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64, ppint32) ppint32

func _mpfr_mul_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_d(*iqlibc.ppTLS, ppuintptr, ppfloat64, ppint32) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_setmin(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_snprintf(*iqlibc.ppTLS, ppuintptr, ppuint64, ppuintptr, ppuintptr) ppint32

func _mpfr_sprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

func _mpfr_ui_div(*iqlibc.ppTLS, ppuintptr, ppuint64, ppuintptr, ppint32) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

func _setlocale(*iqlibc.ppTLS, ppint32, ppuintptr) ppuintptr

func _sprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

var _stderr ppuintptr

func _strcat(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _strcmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _strcpy(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _strncmp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppint32

func _strncpy(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppuintptr

func _strstr(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

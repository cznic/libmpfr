// Code generated for linux/ppc64le by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/ppc64le -UHAVE_NEARBYINT -mlong-double-64 -c -o turandom.o.go turandom.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvC1 = "0.8488312"
const mvC2 = "0.8156509"
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1158
const mvMPFR_AI_THRESHOLD3 = 20165
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 580
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 10480
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 9
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 22904
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 14
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/powerpc64/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_ARCH_PPC = 1
const mv_ARCH_PPC64 = 1
const mv_ARCH_PPCGR = 1
const mv_ARCH_PPCSQ = 1
const mv_ARCH_PWR4 = 1
const mv_ARCH_PWR5 = 1
const mv_ARCH_PWR5X = 1
const mv_ARCH_PWR6 = 1
const mv_ARCH_PWR7 = 1
const mv_ARCH_PWR8 = 1
const mv_CALL_ELF = 2
const mv_CALL_LINUX = 1
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LITTLE_ENDIAN = 1
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ALTIVEC__ = 1
const mv__APPLE_ALTIVEC__ = 1
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BUILTIN_CPU_SUPPORTS__ = 1
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__CMODEL_MEDIUM__ = 1
const mv__CRYPTO__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT128_TYPE__ = 1
const mv__FLOAT128__ = 1
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FP_FAST_FMAL = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "powerpc64le-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=/build/gmp-dldbp2/gmp-6.2.1+dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 1
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 1
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC__ = 10
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1014
const mv__HAVE_BSWAP__ = 1
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__HTM__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LITTLE_ENDIAN__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__POWER8_VECTOR__ = 1
const mv__PPC64__ = 1
const mv__PPC__ = 1
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__QUAD_MEMORY_ATOMIC__ = 1
const mv__RECIPF__ = 1
const mv__RECIP_PRECISION__ = 1
const mv__RECIP__ = 1
const mv__RSQRTEF__ = 1
const mv__RSQRTE__ = 1
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__STRUCT_PARM_ALIGN__ = 16
const mv__TM_FENCE__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VEC_ELEMENT_REG_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__VEC__ = 10206
const mv__VERSION__ = "10.2.1 20210110"
const mv__VSX__ = 1
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__builtin_copysignq = "__builtin_copysignf128"
const mv__builtin_fabsq = "__builtin_fabsf128"
const mv__builtin_huge_valq = "__builtin_huge_valf128"
const mv__builtin_infq = "__builtin_inff128"
const mv__builtin_nanq = "__builtin_nanf128"
const mv__builtin_nansq = "__builtin_nansf128"
const mv__builtin_vsx_vperm = "__builtin_vec_perm"
const mv__builtin_vsx_xvmaddadp = "__builtin_vsx_xvmadddp"
const mv__builtin_vsx_xvmaddasp = "__builtin_vsx_xvmaddsp"
const mv__builtin_vsx_xvmaddmdp = "__builtin_vsx_xvmadddp"
const mv__builtin_vsx_xvmaddmsp = "__builtin_vsx_xvmaddsp"
const mv__builtin_vsx_xvmsubadp = "__builtin_vsx_xvmsubdp"
const mv__builtin_vsx_xvmsubasp = "__builtin_vsx_xvmsubsp"
const mv__builtin_vsx_xvmsubmdp = "__builtin_vsx_xvmsubdp"
const mv__builtin_vsx_xvmsubmsp = "__builtin_vsx_xvmsubsp"
const mv__builtin_vsx_xvnmaddadp = "__builtin_vsx_xvnmadddp"
const mv__builtin_vsx_xvnmaddasp = "__builtin_vsx_xvnmaddsp"
const mv__builtin_vsx_xvnmaddmdp = "__builtin_vsx_xvnmadddp"
const mv__builtin_vsx_xvnmaddmsp = "__builtin_vsx_xvnmaddsp"
const mv__builtin_vsx_xvnmsubadp = "__builtin_vsx_xvnmsubdp"
const mv__builtin_vsx_xvnmsubasp = "__builtin_vsx_xvnmsubsp"
const mv__builtin_vsx_xvnmsubmdp = "__builtin_vsx_xvnmsubdp"
const mv__builtin_vsx_xvnmsubmsp = "__builtin_vsx_xvnmsubsp"
const mv__builtin_vsx_xxland = "__builtin_vec_and"
const mv__builtin_vsx_xxlandc = "__builtin_vec_andc"
const mv__builtin_vsx_xxlnor = "__builtin_vec_nor"
const mv__builtin_vsx_xxlor = "__builtin_vec_or"
const mv__builtin_vsx_xxlxor = "__builtin_vec_xor"
const mv__builtin_vsx_xxsel = "__builtin_vec_sel"
const mv__float128 = "__ieee128"
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__powerpc64__ = 1
const mv__powerpc__ = 1
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

func sitest_urandom(cgtls *iqlibc.ppTLS, aanbtests ppint64, aaprec tnmpfr_prec_t, aarnd tnmpfr_rnd_t, aabit_index ppint64, aaverbose ppint32) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaav, aachi2, aad, aath, aavar ppfloat64
	var aacount, ccv1 ppint64
	var aaemin tnmpfr_exp_t
	var aaex_flags, aaflags tnmpfr_flags_t
	var aai, aainex, aak, aash, aasize_tab, aaxn ppint32
	var aalimb_index tnmp_size_t
	var aalimb_mask tnmp_limb_t
	var aatab ppuintptr
	var ccv5 ppuint64
	var ccv6 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaav, aachi2, aacount, aad, aaemin, aaex_flags, aaflags, aai, aainex, aak, aalimb_index, aalimb_mask, aash, aasize_tab, aatab, aath, aavar, aaxn, ccv1, ccv5, ccv6
	aaav = iqlibc.ppFloat64FromInt32(0)
	aavar = iqlibc.ppFloat64FromInt32(0)
	aachi2 = iqlibc.ppFloat64FromInt32(0)
	aalimb_index = 0
	aalimb_mask = ppuint64(0)
	aacount = 0
	aainex = ppint32(1)

	if aanbtests >= ppint64(1000) {
		ccv1 = aanbtests / ppint64(50)
	} else {
		ccv1 = ppint64(20)
	}
	aasize_tab = ppint32(ccv1)
	aatab = Xtests_allocate(cgtls, iqlibc.ppUint64FromInt32(aasize_tab)*ppuint64(4))
	aak = 0
	for {
		if !(aak < aasize_tab) {
			break
		}
		*(*ppint32)(iqunsafe.ppPointer(aatab + ppuintptr(aak)*4)) = 0
		goto cg_2
	cg_2:
		;
		aak++
	}

	Xmpfr_init2(cgtls, cgbp, aaprec)
	aaxn = ppint32(ppint64(1) + (aaprec-ppint64(1))/ppint64(X__gmp_bits_per_limb))
	aash = ppint32(ppint64(aaxn*X__gmp_bits_per_limb) - aaprec)
	if aabit_index >= 0 && aabit_index < aaprec {

		/* compute the limb index and limb mask to fetch the bit #bit_index */
		aalimb_index = (aaprec - aabit_index) / ppint64(X__gmp_bits_per_limb)
		aai = ppint32(ppint64(1) + aabit_index - aabit_index/ppint64(X__gmp_bits_per_limb)*ppint64(X__gmp_bits_per_limb))
		aalimb_mask = iqlibc.ppUint64FromInt32(1) << (X__gmp_bits_per_limb - aai)
	}

	aak = 0
	for {
		if !(ppint64(aak) < aanbtests) {
			break
		}

		Xmpfr_clear_flags(cgtls)
		aaex_flags = ppuint32(mvMPFR_FLAGS_INEXACT)
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		aai = Xmpfr_urandom(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), aarnd)
		aaflags = X__gmpfr_flags
		aainex = iqlibc.ppBoolInt32(aai != 0 && aainex != 0)
		/* check that lower bits are zero */
		if aash == iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS) {
			ccv5 = iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))
		} else {
			ccv5 = iqlibc.ppUint64FromInt32(1)<<aash - iqlibc.ppUint64FromInt32(1)
		}
		if *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_d))&ccv5 != 0 && !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) {

			Xprintf(cgtls, "Error: mpfr_urandom() returns invalid numbers:\n\x00", 0)
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		/* check that the value is in [0,1] */
		if Xmpfr_sgn(cgtls, cgbp) < 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) > 0 {

			Xprintf(cgtls, "Error: mpfr_urandom() returns number outside [0, 1]:\n\x00", 0)
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		/* check the flags (an underflow is theoretically possible, but
		   impossible in practice due to the huge exponent range) */
		if aaflags != aaex_flags {

			Xprintf(cgtls, "Error: mpfr_urandom() returns incorrect flags.\n\x00", 0)
			Xprintf(cgtls, "Expected \x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got      \x00", 0)
			Xflags_out(cgtls, aaflags)
			Xexit(cgtls, ppint32(1))
		}

		aad = Xmpfr_get_d(cgtls, cgbp, X__gmpfr_default_rounding_mode)

		aaav += aad

		aavar += aad * aad
		aai = ppint32(ppfloat64(aasize_tab) * aad)
		if aad == ppfloat64(1) {
			aai--
		}

		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aai < aasize_tab)), ppint64(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "turandom.c\x00", ppint32(95), "i < size_tab\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		*(*ppint32)(iqunsafe.ppPointer(aatab + ppuintptr(aai)*4))++

		if aalimb_mask != 0 && *(*tnmp_limb_t)(iqunsafe.ppPointer((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_d + ppuintptr(aalimb_index)*8))&aalimb_mask != 0 {
			aacount++
		}

		goto cg_3
	cg_3:
		;
		aak++
	}

	if aainex == 0 {

		/* one call in the loop pretended to return an exact number! */
		Xprintf(cgtls, "Error: mpfr_urandom() returns a zero ternary value.\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* coverage test */
	aaemin = X__gmpfr_emin
	aak = 0
	for {
		if !(aak < ppint32(5)) {
			break
		}

		Xset_emin(cgtls, ppint64(aak+ppint32(1)))
		aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
		aai = 0
		for {
			if !(aai < ppint32(5)) {
				break
			}

			Xmpfr_clear_flags(cgtls)
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			aainex = Xmpfr_urandom(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), aarnd)
			aaflags = X__gmpfr_flags
			if aak > 0 && aaflags != aaex_flags {

				Xprintf(cgtls, "Error: mpfr_urandom() returns incorrect flags for emin = %d (i = %d).\n\x00", iqlibc.ppVaList(cgbp+40, aak+ppint32(1), aai))
				Xprintf(cgtls, "Expected \x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got      \x00", 0)
				Xflags_out(cgtls, aaflags)
				Xexit(cgtls, ppint32(1))
			}
			if (aarnd == ppint32(ecMPFR_RNDZ) || aarnd == ppint32(ecMPFR_RNDD)) && (!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) || aainex != -ppint32(1)) || (aarnd == ppint32(ecMPFR_RNDU) || aarnd == ppint32(ecMPFR_RNDA)) && (Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)<<aak), 0) != 0 || aainex != +iqlibc.ppInt32FromInt32(1)) || aarnd == ppint32(ecMPFR_RNDN) && (aak > 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)<<aak), 0) != 0 || aainex != +iqlibc.ppInt32FromInt32(1)) && (!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) || aainex != -ppint32(1)) {

				Xprintf(cgtls, "Error: mpfr_urandom() does not handle correctly a restricted exponent range.\nemin = %d\nrounding mode: %s\nternary value: %d\nrandom value: \x00", iqlibc.ppVaList(cgbp+40, aak+ppint32(1), Xmpfr_print_rnd_mode(cgtls, aarnd), aainex))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_8
		cg_8:
			;
			aai++
		}

		goto cg_7
	cg_7:
		;
		aak++
	}
	Xset_emin(cgtls, aaemin)

	Xmpfr_clear(cgtls, cgbp)

	if aaverbose != 0 {

		aaav /= ppfloat64(aanbtests)
		aavar = aavar/ppfloat64(aanbtests) - aaav*aaav

		aath = ppfloat64(aanbtests) / ppfloat64(aasize_tab)
		Xprintf(cgtls, "Average = %.5f\nVariance = %.5f\n\x00", iqlibc.ppVaList(cgbp+40, aaav, aavar))
		Xprintf(cgtls, "Repartition for urandom with rounding mode %s. Each integer should be close to %d.\n\x00", iqlibc.ppVaList(cgbp+40, Xmpfr_print_rnd_mode(cgtls, aarnd), ppint32(aath)))

		aak = 0
		for {
			if !(aak < aasize_tab) {
				break
			}

			aachi2 += (ppfloat64(*(*ppint32)(iqunsafe.ppPointer(aatab + ppuintptr(aak)*4))) - aath) * (ppfloat64(*(*ppint32)(iqunsafe.ppPointer(aatab + ppuintptr(aak)*4))) - aath) / aath
			Xprintf(cgtls, "%d \x00", iqlibc.ppVaList(cgbp+40, *(*ppint32)(iqunsafe.ppPointer(aatab + ppuintptr(aak)*4))))
			if iqlibc.ppUint32FromInt32(aak+iqlibc.ppInt32FromInt32(1))&ppuint32(7) == ppuint32(0) {
				Xprintf(cgtls, "\n\x00", 0)
			}

			goto cg_10
		cg_10:
			;
			aak++
		}

		Xprintf(cgtls, "\nChi2 statistics value (with %d degrees of freedom) : %.5f\n\x00", iqlibc.ppVaList(cgbp+40, aasize_tab-ppint32(1), aachi2))

		if aalimb_mask != 0 {
			Xprintf(cgtls, "Bit #%ld is set %ld/%ld = %.1f %% of time\n\x00", iqlibc.ppVaList(cgbp+40, aabit_index, aacount, aanbtests, ppfloat64(aacount)*ppfloat64(100)/ppfloat64(aanbtests)))
		}

		Xputs(cgtls, "\x00")
	}

	Xtests_free(cgtls, aatab, iqlibc.ppUint64FromInt32(aasize_tab)*ppuint64(4))
	return
}

func siunderflow_tests(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaemin tnmpfr_exp_t
	var aaex_flags, aaflags tnmpfr_flags_t
	var aai, aainex, aak, aarnd ppint32
	var ccv4 ppbool
	var pp_ /* s at bp+32 */ tngmp_randstate_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemin, aaex_flags, aaflags, aai, aainex, aak, aarnd, ccv4

	aaemin = X__gmpfr_emin
	Xmpfr_init2(cgtls, cgbp, ppint64(4))

	aai = ppint32(2)
	for {
		if !(aai >= -ppint32(4)) {
			break
		}
		aarnd = 0
		for {
			if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}
			aak = 0
			for {
				if !(aak < ppint32(100)) {
					break
				}

				if aai >= ppint32(2) {

					/* Always underflow when emin >= 2, i.e. when the minimum
					   representable positive number is >= 2. */
					aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
				} else {

					/* Since the unrounded random number does not depend on
					   the current exponent range, we can detect underflow
					   in a range larger than the one that will be tested. */
					X__gmp_randinit_set(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
					Xmpfr_clear_flags(cgtls)
					Xmpfr_urandom(cgtls, cgbp, cgbp+32, aarnd)
					X__gmp_randclear(cgtls, cgbp+32)
					aaex_flags = ppuint32(mvMPFR_FLAGS_INEXACT)
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp < ppint64(aai) {

						aaex_flags |= ppuint32(mvMPFR_FLAGS_UNDERFLOW)
					}
				}

				Xset_emin(cgtls, ppint64(aai))
				Xmpfr_clear_flags(cgtls)
				aainex = Xmpfr_urandom(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), aarnd)
				aaflags = X__gmpfr_flags

				if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv4 {
					Xmpfr_assert_fail(cgtls, "turandom.c\x00", ppint32(233), "((int) (__gmpfr_flags & 8))\x00")
				}
				pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				Xset_emin(cgtls, aaemin)

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 {

					Xprintf(cgtls, "Error in underflow_tests: got a negative sign for i=%d rnd=%s k=%d.\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), aak))
					Xexit(cgtls, ppint32(1))
				}

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) {

					if aarnd == ppint32(ecMPFR_RNDU) || aarnd == ppint32(ecMPFR_RNDA) {

						Xprintf(cgtls, "Error in underflow_tests: the value cannot be 0 for i=%d rnd=%s k=%d.\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), aak))
						Xexit(cgtls, ppint32(1))
					}
				}

				if aainex == 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) && aainex > 0 {

					Xprintf(cgtls, "Error in underflow_tests: incorrect inex (%d) for i=%d rnd=%s k=%d.\n\x00", iqlibc.ppVaList(cgbp+72, aainex, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), aak))
					Xexit(cgtls, ppint32(1))
				}

				if aaex_flags != ppuint32(0) && aaflags != aaex_flags {

					Xprintf(cgtls, "Error in underflow_tests: incorrect flags for i=%d rnd=%s k=%d.\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), aak))
					Xprintf(cgtls, "Expected \x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got      \x00", 0)
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}

				goto cg_3
			cg_3:
				;
				aak++
			}
			goto cg_2
		cg_2:
			;
			aarnd++
		}
		goto cg_1
	cg_1:
		;
		aai--
	}

	Xmpfr_clear(cgtls, cgbp)
}

func sitest_underflow(cgtls *iqlibc.ppTLS, aaverbose ppint32) {
	cgbp := cgtls.ppAlloc(144)
	defer cgtls.ppFree(144)

	var aaemin tnmpfr_exp_t
	var aaexp [6]ppint64
	var aai ppint64
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_ = aaemin, aaexp, aai
	aaemin = X__gmpfr_emin
	aaexp = [6]ppint64{}

	Xmpfr_init2(cgtls, cgbp, ppint64(2))
	Xset_emin(cgtls, ppint64(-ppint32(3)))
	aai = 0
	for {
		if !(aai < ppint64(1000000)) {
			break
		}

		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandom(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppint32(ecMPFR_RNDN))
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
			aaexp[ppint32(5)]++
		} else {
			/* exp=1 is possible if the generated number is 0.111111... */
			aaexp[ppint64(1)-(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp]++
		}

		goto cg_1
	cg_1:
		;
		aai++
	}
	if aaverbose != 0 {
		Xprintf(cgtls, "exp=1:%.3f(%.3f) 0:%.3f(%.3f) -1:%.3f(%.3f) -2:%.3f(%.3f) -3:%.3f(%.3f) zero:%.3f(%.3f)\n\x00", iqlibc.ppVaList(cgbp+40, ppfloat64(100)*ppfloat64(aaexp[0])/iqlibc.ppFloat64FromInt32(1000000), ppfloat64(12.5), ppfloat64(100)*ppfloat64(aaexp[ppint32(1)])/iqlibc.ppFloat64FromInt32(1000000), ppfloat64(43.75), ppfloat64(100)*ppfloat64(aaexp[ppint32(2)])/iqlibc.ppFloat64FromInt32(1000000), ppfloat64(21.875), ppfloat64(100)*ppfloat64(aaexp[ppint32(3)])/iqlibc.ppFloat64FromInt32(1000000), ppfloat64(10.9375), ppfloat64(100)*ppfloat64(aaexp[ppint32(4)])/iqlibc.ppFloat64FromInt32(1000000), ppfloat64(7.8125), ppfloat64(100)*ppfloat64(aaexp[ppint32(5)])/iqlibc.ppFloat64FromInt32(1000000), ppfloat64(3.125)))
	}
	Xmpfr_clear(cgtls, cgbp)
	Xset_emin(cgtls, aaemin)
}

func sioverflow_tests(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaemax tnmpfr_exp_t
	var aaex_flags, aaflags tnmpfr_flags_t
	var aai, aainex, aak, aarnd ppint32
	var ccv4 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaex_flags, aaflags, aai, aainex, aak, aarnd, ccv4

	aaemax = X__gmpfr_emax
	Xmpfr_init2(cgtls, cgbp, ppint64(4))
	aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)) /* if overflow */
	aai = -ppint32(4)
	for {
		if !(aai <= 0) {
			break
		}

		Xset_emax(cgtls, ppint64(aai))
		aarnd = 0
		for {
			if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}
			aak = 0
			for {
				if !(aak < ppint32(100)) {
					break
				}

				Xmpfr_clear_flags(cgtls)
				aainex = Xmpfr_urandom(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), aarnd)
				aaflags = X__gmpfr_flags

				if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv4 {
					Xmpfr_assert_fail(cgtls, "turandom.c\x00", ppint32(334), "((int) (__gmpfr_flags & 8))\x00")
				}
				pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 {

					Xprintf(cgtls, "Error in overflow_tests: got a negative sign for i=%d rnd=%s k=%d.\n\x00", iqlibc.ppVaList(cgbp+40, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), aak))
					Xexit(cgtls, ppint32(1))
				}
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

					if aarnd == ppint32(ecMPFR_RNDD) || aarnd == ppint32(ecMPFR_RNDZ) {

						Xprintf(cgtls, "Error in overflow_tests: the value cannot be +inf for i=%d rnd=%s k=%d.\n\x00", iqlibc.ppVaList(cgbp+40, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), aak))
						Xexit(cgtls, ppint32(1))
					}
					if aaflags != aaex_flags {

						Xprintf(cgtls, "Error in overflow_tests: incorrect flags for i=%d rnd=%s k=%d.\n\x00", iqlibc.ppVaList(cgbp+40, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), aak))
						Xprintf(cgtls, "Expected \x00", 0)
						Xflags_out(cgtls, aaex_flags)
						Xprintf(cgtls, "Got      \x00", 0)
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				}
				if aainex == 0 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) && aainex < 0 {

					Xprintf(cgtls, "Error in overflow_tests: incorrect inex (%d) for i=%d rnd=%s k=%d.\n\x00", iqlibc.ppVaList(cgbp+40, aainex, aai, Xmpfr_print_rnd_mode(cgtls, aarnd), aak))
					Xexit(cgtls, ppint32(1))
				}

				goto cg_3
			cg_3:
				;
				aak++
			}
			goto cg_2
		cg_2:
			;
			aarnd++
		}

		goto cg_1
	cg_1:
		;
		aai++
	}
	Xmpfr_clear(cgtls, cgbp)
	Xset_emax(cgtls, aaemax)
}

// C documentation
//
//	/* Problem reported by Carl Witty. This test assumes the random generator
//	   used by GMP is deterministic (for a given seed). We need to distinguish
//	   two cases since the random generator changed in GMP 4.2.0. */
func sibug20100914(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var pp_ /* s at bp+32 */ tngmp_randstate_t
	var pp_ /* x at bp+0 */ tnmpfr_t

	X__gmp_randinit_default(cgtls, cgbp+32)
	X__gmp_randseed_ui(cgtls, cgbp+32, ppuint64(42))
	Xmpfr_init2(cgtls, cgbp, ppint64(17))
	Xmpfr_urandom(cgtls, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "0.8488312\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "Error in bug20100914, expected 0.8488312, got \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_urandom(cgtls, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_str(cgtls, cgbp, "0.8156509\x00", ppint32(10), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "Error in bug20100914, expected 0.8156509, got \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear(cgtls, cgbp)
	X__gmp_randclear(cgtls, cgbp+32)
}

// C documentation
//
//	/* non-regression test for bug reported by Trevor Spiteri
//	   https://sympa.inria.fr/sympa/arc/mpfr/2017-01/msg00020.html */
func sibug20170123(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aaemin tnmpfr_exp_t
	var ccv2 ppbool
	var pp_ /* s at bp+32 */ tngmp_randstate_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_ = aaemin, ccv2

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint64(-ppint32(7)))
	Xmpfr_init2(cgtls, cgbp, ppint64(53))
	X__gmp_randinit_default(cgtls, cgbp+32)
	X__gmp_randseed_ui(cgtls, cgbp+32, ppuint64(398))
	Xmpfr_urandom(cgtls, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), ppint64(-ppint32(8))) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "turandom.c\x00", ppint32(434), "mpfr_cmp_ui_2exp (x, 1, -8) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), ppint64(-ppint32(8))) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp)
	X__gmp_randclear(cgtls, cgbp+32)
	Xset_emin(cgtls, aaemin)
}

// C documentation
//
//	/* Reproducibility test with several rounding modes and exponent ranges. */
func sireprod_rnd_exp(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(208)
	defer cgtls.ppFree(208)

	var aa_p tnmpfr_srcptr
	var aai, aak, aank, aar, ccv2, ccv5, ccv7 ppint32
	var aaprec tnmpfr_prec_t
	var aarr tnmpfr_rnd_t
	var aat [2]tnmpfr_ptr
	var ccv4, ccv6, ccv8 ppbool
	var ccv9 ppuintptr
	var pp_ /* s1 at bp+0 */ tngmp_randstate_t
	var pp_ /* s2 at bp+136 */ tngmp_randstate_t
	var pp_ /* v at bp+128 */ tnmp_limb_t
	var pp_ /* w at bp+168 */ tnmp_limb_t
	var pp_ /* x1 at bp+32 */ tnmpfr_t
	var pp_ /* x2 at bp+64 */ tnmpfr_t
	var pp_ /* y at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aai, aak, aank, aaprec, aar, aarr, aat, ccv2, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9

	aai = 0
	for {
		if !(aai < ppint32(10)) {
			break
		}

		aaprec = iqlibc.ppInt64FromUint64(ppuint64(mvMPFR_PREC_MIN) + Xrandlimb(cgtls)%ppuint64(200))
		Xmpfr_inits2(cgtls, aaprec, cgbp+32, iqlibc.ppVaList(cgbp+184, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))

		X__gmp_randinit_set(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		Xmpfr_urandom(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppint32(ecMPFR_RNDZ))
		Xmpfr_rand_raw(cgtls, cgbp+128, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)), ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
		{
			aa_p = cgbp + 32
			ccv2 = Xmpfr_set4(cgtls, cgbp+64, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
		}
		pp_ = ccv2
		Xmpfr_nextabove(cgtls, cgbp+64)
		/* The real number is between x1 and x2. */

		aar = 0
		for {
			if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}

			aarr = aar
			aank = 0

			X__gmp_randinit_set(cgtls, cgbp+136, cgbp)
			Xmpfr_urandom(cgtls, cgbp+96, cgbp+136, aarr)
			Xmpfr_rand_raw(cgtls, cgbp+168, cgbp+136, ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
			if *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 168)) != *(*tnmp_limb_t)(iqunsafe.ppPointer(cgbp + 128)) {

				Xprintf(cgtls, "Error in reprod_rnd_exp for i=%d rnd=%s: different PRNG state\n\x00", iqlibc.ppVaList(cgbp+184, aai, Xmpfr_print_rnd_mode(cgtls, aarr)))
				Xexit(cgtls, ppint32(1))
			}

			if ccv4 = aarr == ppint32(ecMPFR_RNDA); !ccv4 {
			}
			if !(ccv4 || aarr+iqlibc.ppBoolInt32(true) == ppint32(ecMPFR_RNDD)) {
				ccv5 = aank
				aank++
				aat[ccv5] = cgbp + 32
			}

			if ccv6 = aarr == ppint32(ecMPFR_RNDZ); !ccv6 {
			}
			if !(ccv6 || aarr+iqlibc.ppInt32FromInt32(0) == ppint32(ecMPFR_RNDD)) {
				ccv7 = aank
				aank++
				aat[ccv7] = cgbp + 64
			}

			if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aank == ppint32(1) || aank == ppint32(2))), ppint64(1)) != 0; !ccv8 {
				Xmpfr_assert_fail(cgtls, "turandom.c\x00", ppint32(487), "nk == 1 || nk == 2\x00")
			}
			pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

			if !(Xmpfr_equal_p(cgtls, cgbp+96, aat[0]) != 0 || aank > ppint32(1) && Xmpfr_equal_p(cgtls, cgbp+96, aat[ppint32(1)]) != 0) {

				Xprintf(cgtls, "Error in reprod_rnd_exp for i=%d rnd=%s:\n\x00", iqlibc.ppVaList(cgbp+184, aai, Xmpfr_print_rnd_mode(cgtls, aarr)))
				if aank > ppint32(1) {
					ccv9 = " one of\x00"
				} else {
					ccv9 = "\x00"
				}
				Xprintf(cgtls, "Expected%s\n\x00", iqlibc.ppVaList(cgbp+184, ccv9))
				aak = 0
				for {
					if !(aak < aank) {
						break
					}

					Xprintf(cgtls, "  \x00", 0)
					Xmpfr_dump(cgtls, aat[aak])

					goto cg_10
				cg_10:
					;
					aak++
				}
				Xprintf(cgtls, "Got\n  \x00", 0)
				Xmpfr_dump(cgtls, cgbp+96)
				Xexit(cgtls, ppint32(1))
			}

			X__gmp_randclear(cgtls, cgbp+136)

			goto cg_3
		cg_3:
			;
			aar++
		}

		Xmpfr_clears(cgtls, cgbp+32, iqlibc.ppVaList(cgbp+184, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
		X__gmp_randclear(cgtls, cgbp)

		goto cg_1
	cg_1:
		;
		aai++
	}
}

// C documentation
//
//	/* Reproducibility test: check that the behavior does not depend on
//	   the platform ABI or MPFR version (new, incompatible MPFR versions
//	   may introduce changes, in which case the hardcoded values should
//	   depend on MPFR_VERSION).
//	   It is not necessary to test with different rounding modes and
//	   exponent ranges as this has already been done in reprod_rnd_exp.
//	   We do not need to check the status of the PRNG after mpfr_urandom
//	   since this is done implicitly by comparing the next value, except
//	   for the last itaration.
//	*/
func sireprod_abi(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aagenerate, aai, ccv2 ppint32
	var aaprec tnmpfr_prec_t
	var aat [30]ppuintptr
	var ccv3 ppuintptr
	var pp_ /* s at bp+0 */ tngmp_randstate_t
	var pp_ /* x at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_ = aagenerate, aai, aaprec, aat, ccv2, ccv3
	/* Run this program with the MPFR_REPROD_ABI_OUTPUT environment variable
	   set to get the array of strings. */
	aat = [30]ppuintptr{
		0:  "1.0@-1\x00",
		1:  "3.0@-1\x00",
		2:  "7.0@-1\x00",
		3:  "9.0@-1\x00",
		4:  "c.0@-1\x00",
		5:  "4.385434c0@-1\x00",
		6:  "1.9a018734@-1\x00",
		7:  "8.26547780@-1\x00",
		8:  "a.fd334198@-1\x00",
		9:  "9.aa11d5f00@-1\x00",
		10: "d.aa9a32fd0a801ac0@-1\x00",
		11: "c.eb47074368ec6340@-1\x00",
		12: "9.7dbe2ced88ae4c30@-1\x00",
		13: "d.03218ea6704a42c0@-1\x00",
		14: "7.1530156aac800d980@-1\x00",
		15: "e.270121b1d74aea9029ccc740@-1\x00",
		16: "5.614fc2d9ca3917107609e2e0@-1\x00",
		17: "5.15417c51af272232181d6a40@-1\x00",
		18: "f.dfb431dd6533c004b6d3c590@-1\x00",
		19: "4.345f96fd2929d41eb278a4f40@-1\x00",
		20: "a.804590c6449cd8c83bae31f31f7a4100@-1\x00",
		21: "a.0a2b318d3c99911a45e4cf33847d3680@-1\x00",
		22: "2.89f6127c19092d7a1808b1842b296400@-1\x00",
		23: "2.1db4fc00348ca1531983fe4bd4cdf6d2@-1\x00",
		24: "5.2d90f11ed710425ebe549a95decbb6540@-1\x00",
		25: "8.ca35d1302cf369e03c2a58bf2ce5cff8307f0bc0@-1\x00",
		26: "3.a22bae632c32f2a7a67a1fa78a93f5e84f9caa40@-1\x00",
		27: "f.370a36febed972dbb47f2503f7e08a651edbf120@-1\x00",
		28: "d.0764d7a38c206eeba6ffe8cf39d777194f5c9200@-1\x00",
		29: "a.1a312f0bb16db20c4783c6438725ed5d6dff6af80@-1\x00",
	}

	/* We must hardcode the seed to be able to compare with hardcoded values. */
	X__gmp_randinit_default(cgtls, cgbp)
	X__gmp_randseed_ui(cgtls, cgbp, ppuint64(17))

	aagenerate = iqlibc.ppBoolInt32(Xgetenv(cgtls, "MPFR_REPROD_ABI_OUTPUT\x00") != iqlibc.ppUintptrFromInt32(0))

	aai = 0
	for {
		if !(aai < iqlibc.ppInt32FromInt32(5)*iqlibc.ppInt32FromInt32(6)) {
			break
		}

		if aai < ppint32(5) {
			ccv2 = ppint32(mvMPFR_PREC_MIN) + aai
		} else {
			ccv2 = aai/ppint32(5)*ppint32(32) + aai%ppint32(5) - ppint32(2)
		}
		aaprec = ppint64(ccv2)
		Xmpfr_init2(cgtls, cgbp+32, aaprec)
		Xmpfr_urandom(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
		if aagenerate != 0 {

			Xprintf(cgtls, "    \"\x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDZ))
			if aai < iqlibc.ppInt32FromInt32(5)*iqlibc.ppInt32FromInt32(6)-iqlibc.ppInt32FromInt32(1) {
				ccv3 = "\",\n\x00"
			} else {
				ccv3 = "\"\n\x00"
			}
			Xprintf(cgtls, ccv3, 0)
		} else {
			if Xmpfr_cmp_str(cgtls, cgbp+32, aat[aai], ppint32(16), ppint32(ecMPFR_RNDN)) != 0 {

				Xprintf(cgtls, "Error in reprod_abi for i=%d\n\x00", iqlibc.ppVaList(cgbp+72, aai))
				Xprintf(cgtls, "Expected %s\n\x00", iqlibc.ppVaList(cgbp+72, aat[aai]))
				Xprintf(cgtls, "Got      \x00", 0)
				X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDZ))
				Xprintf(cgtls, "\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
		}
		Xmpfr_clear(cgtls, cgbp+32)

		goto cg_1
	cg_1:
		;
		aai++
	}

	X__gmp_randclear(cgtls, cgbp)
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aaa, aabit_index, aanbtests ppint64
	var aaprec tnmpfr_prec_t
	var aarnd, aaverbose ppint32
	pp_, pp_, pp_, pp_, pp_, pp_ = aaa, aabit_index, aanbtests, aaprec, aarnd, aaverbose
	aaverbose = 0

	Xtests_start_mpfr(cgtls)

	if aaargc > ppint32(1) {
		aaverbose = ppint32(1)
	}

	aanbtests = ppint64(10000)
	if aaargc > ppint32(1) {

		aaa = Xatol(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*8)))
		if aaa != 0 {
			aanbtests = aaa
		}
	}

	if aaargc <= ppint32(2) {
		aaprec = ppint64(1000)
	} else {
		aaprec = Xatol(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 2*8)))
	}

	if aaargc <= ppint32(3) {
		aabit_index = ppint64(-ppint32(1))
	} else {

		aabit_index = Xatol(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 3*8)))
		if aabit_index >= aaprec {

			Xprintf(cgtls, "Warning. Cannot compute the bit frequency: the given bit index (= %ld) is not less than the precision (= %ld).\n\x00", iqlibc.ppVaList(cgbp+8, aabit_index, aaprec))
			aabit_index = ppint64(-ppint32(1))
		}
	}

	aarnd = 0
	for {
		if !(aarnd < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}

		sitest_urandom(cgtls, aanbtests, aaprec, aarnd, aabit_index, aaverbose)

		if aaargc == ppint32(1) { /* check also small precision */

			sitest_urandom(cgtls, aanbtests, ppint64(mvMPFR_PREC_MIN), aarnd, ppint64(-ppint32(1)), 0)
		}

		goto cg_1
	cg_1:
		;
		aarnd++
	}

	siunderflow_tests(cgtls)
	sioverflow_tests(cgtls)

	/* Since these tests assume a deterministic random generator, and
	   this is not implemented in mini-gmp, we omit it with mini-gmp. */
	sibug20100914(cgtls)
	sibug20170123(cgtls)
	sireprod_rnd_exp(cgtls)
	sireprod_abi(cgtls)

	sitest_underflow(cgtls, aaverbose)

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___builtin_unreachable(*iqlibc.ppTLS)

var ___gmp_bits_per_limb ppint32

func ___gmp_randclear(*iqlibc.ppTLS, ppuintptr)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

func ___gmp_randinit_set(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func ___gmp_randseed_ui(*iqlibc.ppTLS, ppuintptr, ppuint64)

var ___gmpfr_default_rounding_mode ppint32

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuint64

func _atol(*iqlibc.ppTLS, ppuintptr) ppint64

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_get_d(*iqlibc.ppTLS, ppuintptr, ppint32) ppfloat64

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_inits2(*iqlibc.ppTLS, ppint64, ppuintptr, ppuintptr)

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_rand_raw(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64)

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppuint8

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_sgn(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_urandom(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _puts(*iqlibc.ppTLS, ppuintptr) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

var _stdout ppuintptr

func _tests_allocate(*iqlibc.ppTLS, ppuint64) ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_free(*iqlibc.ppTLS, ppuintptr, ppuint64)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

// Code generated for linux/ppc64le by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/ppc64le -UHAVE_NEARBYINT -mlong-double-64 -c -o tgamma.o.go tgamma.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCHECK_X1 = "1.0762904832837976166"
const mvCHECK_X2 = "9.23709516716202383435e-01"
const mvCHECK_Y1 = "0.96134843256452096050"
const mvCHECK_Y2 = "1.0502315560291053398"
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1158
const mvMPFR_AI_THRESHOLD3 = 20165
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 580
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 10480
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 9
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 22904
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 14
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/powerpc64/mparam.h"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTGENERIC_SO_TEST = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_ARCH_PPC = 1
const mv_ARCH_PPC64 = 1
const mv_ARCH_PPCGR = 1
const mv_ARCH_PPCSQ = 1
const mv_ARCH_PWR4 = 1
const mv_ARCH_PWR5 = 1
const mv_ARCH_PWR5X = 1
const mv_ARCH_PWR6 = 1
const mv_ARCH_PWR7 = 1
const mv_ARCH_PWR8 = 1
const mv_CALL_ELF = 2
const mv_CALL_LINUX = 1
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LITTLE_ENDIAN = 1
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ALTIVEC__ = 1
const mv__APPLE_ALTIVEC__ = 1
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BUILTIN_CPU_SUPPORTS__ = 1
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__CMODEL_MEDIUM__ = 1
const mv__CRYPTO__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT128_TYPE__ = 1
const mv__FLOAT128__ = 1
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FP_FAST_FMAL = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "powerpc64le-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=/build/gmp-dldbp2/gmp-6.2.1+dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 1
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 1
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC__ = 10
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1014
const mv__HAVE_BSWAP__ = 1
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__HTM__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LITTLE_ENDIAN__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__POWER8_VECTOR__ = 1
const mv__PPC64__ = 1
const mv__PPC__ = 1
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__QUAD_MEMORY_ATOMIC__ = 1
const mv__RECIPF__ = 1
const mv__RECIP_PRECISION__ = 1
const mv__RECIP__ = 1
const mv__RSQRTEF__ = 1
const mv__RSQRTE__ = 1
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__STRUCT_PARM_ALIGN__ = 16
const mv__TM_FENCE__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VEC_ELEMENT_REG_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__VEC__ = 10206
const mv__VERSION__ = "10.2.1 20210110"
const mv__VSX__ = 1
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__builtin_copysignq = "__builtin_copysignf128"
const mv__builtin_fabsq = "__builtin_fabsf128"
const mv__builtin_huge_valq = "__builtin_huge_valf128"
const mv__builtin_infq = "__builtin_inff128"
const mv__builtin_nanq = "__builtin_nanf128"
const mv__builtin_nansq = "__builtin_nansf128"
const mv__builtin_vsx_vperm = "__builtin_vec_perm"
const mv__builtin_vsx_xvmaddadp = "__builtin_vsx_xvmadddp"
const mv__builtin_vsx_xvmaddasp = "__builtin_vsx_xvmaddsp"
const mv__builtin_vsx_xvmaddmdp = "__builtin_vsx_xvmadddp"
const mv__builtin_vsx_xvmaddmsp = "__builtin_vsx_xvmaddsp"
const mv__builtin_vsx_xvmsubadp = "__builtin_vsx_xvmsubdp"
const mv__builtin_vsx_xvmsubasp = "__builtin_vsx_xvmsubsp"
const mv__builtin_vsx_xvmsubmdp = "__builtin_vsx_xvmsubdp"
const mv__builtin_vsx_xvmsubmsp = "__builtin_vsx_xvmsubsp"
const mv__builtin_vsx_xvnmaddadp = "__builtin_vsx_xvnmadddp"
const mv__builtin_vsx_xvnmaddasp = "__builtin_vsx_xvnmaddsp"
const mv__builtin_vsx_xvnmaddmdp = "__builtin_vsx_xvnmadddp"
const mv__builtin_vsx_xvnmaddmsp = "__builtin_vsx_xvnmaddsp"
const mv__builtin_vsx_xvnmsubadp = "__builtin_vsx_xvnmsubdp"
const mv__builtin_vsx_xvnmsubasp = "__builtin_vsx_xvnmsubsp"
const mv__builtin_vsx_xvnmsubmdp = "__builtin_vsx_xvnmsubdp"
const mv__builtin_vsx_xvnmsubmsp = "__builtin_vsx_xvnmsubsp"
const mv__builtin_vsx_xxland = "__builtin_vec_and"
const mv__builtin_vsx_xxlandc = "__builtin_vec_andc"
const mv__builtin_vsx_xxlnor = "__builtin_vec_nor"
const mv__builtin_vsx_xxlor = "__builtin_vec_or"
const mv__builtin_vsx_xxlxor = "__builtin_vec_xor"
const mv__builtin_vsx_xxsel = "__builtin_vec_sel"
const mv__float128 = "__ieee128"
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__powerpc64__ = 1
const mv__powerpc__ = 1
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

/* Note: there could be an incorrect test about suspicious overflow
   (MPFR_SUSPICIOUS_OVERFLOW) for x = 2^(-emax) = 0.5 * 2^(emin+1) in
   RNDZ or RNDD, but this case is never tested in the generic tests. */
/* Generic test file for functions with one or two arguments (the second being
   either mpfr_t or double or unsigned long).

Copyright 2001-2023 Free Software Foundation, Inc.
Contributed by the AriC and Caramba projects, INRIA.

This file is part of the GNU MPFR Library.

The GNU MPFR Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The GNU MPFR Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

/* Define TWO_ARGS for two-argument functions like mpfr_pow.
   Define DOUBLE_ARG1 or DOUBLE_ARG2 for function with a double operand in
   first or second place like sub_d or d_sub.
   Define ULONG_ARG1 or ULONG_ARG2 for function with an unsigned long
   operand in first or second place like sub_ui or ui_sub.
   Define THREE_ARGS for three-argument functions like mpfr_atan2u. */

/* TODO: Add support for type long and extreme integer values, as done
   in tgeneric_ui.c; then tgeneric_ui.c could probably disappear. */

/* For the random function: one number on two is negative. */

/* For the random function: one number on two is negative. */

/* If the MPFR_SUSPICIOUS_OVERFLOW test fails but this is not a bug,
   then define TGENERIC_SO_TEST with an adequate test (possibly 0) to
   omit this particular case. */

/* For some functions (for example cos), the argument reduction is too
   expensive when using mpfr_get_emax(). Then simply define REDUCE_EMAX
   to some reasonable value before including tgeneric.c. */

/* same for mpfr_get_emin() */

func sitest_generic(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aanmax ppuint32) {
	cgbp := cgtls.ppAlloc(288)
	defer cgtls.ppFree(288)

	var aa_p tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, aainfinite_input, aatest_of, aatest_uf, ccv12, ccv7, ccv9 ppint32
	var aactrn, aactrt ppuint64
	var aae, aaemax, aaemin, aaoemax, aaoemin, aaold_emax, aaold_emin tnmpfr_exp_t
	var aaex_flags, aaex_flags1, aaflags, aaoldflags tnmpfr_flags_t
	var aan, ccv3 ppuint32
	var aaprec, aaxprec, aayprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv10, ccv11, ccv13, ccv14, ccv15, ccv16, ccv6, ccv8 ppbool
	var ccv4 ppfloat64
	var pp_ /* t at bp+96 */ tnmpfr_t
	var pp_ /* w at bp+128 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* yd at bp+160 */ tnmpfr_t
	var pp_ /* yu at bp+192 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aacompare, aacompare2, aactrn, aactrt, aae, aaemax, aaemin, aaex_flags, aaex_flags1, aaflags, aainexact, aainfinite_input, aan, aaoemax, aaoemin, aaold_emax, aaold_emin, aaoldflags, aaprec, aarnd, aatest_of, aatest_uf, aaxprec, aayprec, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv3, ccv4, ccv6, ccv7, ccv8, ccv9
	aactrt = ppuint64(0)
	aactrn = ppuint64(0)

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	Xmpfr_inits2(cgtls, ppint64(mvMPFR_PREC_MIN), cgbp, iqlibc.ppVaList(cgbp+232, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))

	/* generic tests */
	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		/* Number of overflow/underflow tests for each precision.
		   Since MPFR uses several algorithms and there may also be
		   early overflow/underflow detection, several tests may be
		   needed to detect a bug. */
		aatest_of = ppint32(3)
		aatest_uf = ppint32(3)

		Xmpfr_set_prec(cgtls, cgbp+64, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+96, aaprec)
		aayprec = aaprec + ppint64(20)
		Xmpfr_set_prec(cgtls, cgbp+32, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+160, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+192, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+128, aayprec)

		/* Note: in precision p1, we test NSPEC special cases. */
		aan = ppuint32(0)
		for {
			if aaprec == aap1 {
				ccv3 = aanmax + ppuint32(5)
			} else {
				ccv3 = aanmax
			}
			if !(aan < ccv3) {
				break
			}

			aainfinite_input = 0

			aaxprec = aaprec
			if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {

				/* In half cases, modify the precision of the inputs:
				   If the base precision (for the result) is small,
				   take a larger input precision in general, else
				   take a smaller precision. */
				if aaprec < ppint64(16) {
					ccv4 = ppfloat64(256)
				} else {
					ccv4 = ppfloat64(1)
				}
				aaxprec = tnmpfr_prec_t(ppfloat64(aaxprec) * (ccv4 * ppfloat64(Xrandlimb(cgtls)) / ppfloat64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1)))))
				if aaxprec < ppint64(mvMPFR_PREC_MIN) {
					aaxprec = ppint64(mvMPFR_PREC_MIN)
				}
			}
			Xmpfr_set_prec(cgtls, cgbp, aaxprec)

			/* Generate random arguments, even in the special cases
			   (this may not be needed, but this is simpler).
			   Note that if RAND_FUNCTION is defined, this specific
			   random function is used for all arguments; this is
			   typically mpfr_random2, which generates a positive
			   random mpfr_t with long runs of consecutive ones and
			   zeros in the binary representation. */

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_random2(cgtls, cgbp, ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt64FromInt32(1), ppint64(1), ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))

			if aan < ppuint32(5) && aaprec == aap1 {

				/* Special cases tested in precision p1 if n < NSPEC. They are
				   useful really in the extended exponent range. */
				/* TODO: x2 is set even when it is associated with a double;
				   check whether this really makes sense. */
				Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
				Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
				if aan == ppuint32(0) {

					Xmpfr_set_nan(cgtls, cgbp)
				} else {
					if aan <= ppuint32(2) {

						if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(1) || aan == ppuint32(2))), ppint64(1)) != 0; !ccv6 {
							Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(290), "n == 1 || n == 2\x00")
						}
						pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
						if aan == ppuint32(1) {
							ccv7 = ppint32(1)
						} else {
							ccv7 = -ppint32(1)
						}
						pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv7), 0, ppint32(ecMPFR_RNDN))
						Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)
					} else {
						if aan <= ppuint32(4) {

							if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(3) || aan == ppuint32(4))), ppint64(1)) != 0; !ccv8 {
								Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(300), "n == 3 || n == 4\x00")
							}
							pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
							if aan == ppuint32(3) {
								ccv9 = ppint32(1)
							} else {
								ccv9 = -ppint32(1)
							}
							pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv9), 0, ppint32(ecMPFR_RNDN))
							Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)
						}
					}
				}
			}

			/* Exponent range for the test. */
			aaoemin = X__gmpfr_emin
			aaoemax = X__gmpfr_emax

			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt32(ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)))
			Xmpfr_clear_flags(cgtls)
			aacompare = Xmpfr_gamma(cgtls, cgbp+32, cgbp, aarnd)
			aaflags = X__gmpfr_flags
			if X__gmpfr_emin != aaoemin || X__gmpfr_emax != aaoemax {

				Xprintf(cgtls, "tgeneric: the exponent range has been modified by the tested function!\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
			if aarnd != ppint32(ecMPFR_RNDF) {
				if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) == iqlibc.ppInt32FromInt32(0)) != 0) {
					Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad inexact flag for mpfr_gamma\x00"))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					if 0 != 0 {
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, ppuintptr(0))
					}
					if -ppint32(1) >= 0 {
						Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
					}
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}
			aactrt++

			/* If rnd = RNDF, check that we obtain the same result as
			   RNDD or RNDU. */
			if aarnd == ppint32(ecMPFR_RNDF) {

				Xmpfr_gamma(cgtls, cgbp+160, cgbp, ppint32(ecMPFR_RNDD))
				Xmpfr_gamma(cgtls, cgbp+192, cgbp, ppint32(ecMPFR_RNDU))
				if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+160) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_sign || ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+192) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_sign)) {

					Xprintf(cgtls, "tgeneric: error formpfr_gamma, RNDF; result matches neither RNDD nor RNDU\n\x00", 0)
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "yd (RNDD) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+160)
					Xprintf(cgtls, "yu (RNDU) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+192)
					Xprintf(cgtls, "y  (RNDF) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xexit(cgtls, ppint32(1))
				}
			}

			/* Tests in a reduced exponent range. */

			aaoldflags = aaflags

			/* Determine the smallest exponent range containing the
			   exponents of the mpfr_t inputs (x, and u if TWO_ARGS)
			   and output (y). */
			aaemin = iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) - iqlibc.ppInt64FromInt32(1)
			aaemax = iqlibc.ppInt64FromInt32(1) - iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))

			if ccv10 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv10 {
			}
			if ccv10 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv11 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv11 {
			}
			if ccv11 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp /* exponent of the result */

				if aatest_of > 0 && aae-ppint64(1) >= aaemax { /* overflow test */

					/* Exponent e of the result > exponents of the inputs;
					   let's set emax to e - 1, so that one should get an
					   overflow. */
					Xset_emax(cgtls, aae-ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = Xmpfr_gamma(cgtls, cgbp+128, cgbp, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emax(cgtls, aaoemax)
					aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for mpfr_gamma, reduced exponent range [%ld,%ld] (overflow test) on:\n\x00", iqlibc.ppVaList(cgbp+232, aaoemin, aae-ppint64(1)))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+232, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_of--
				}

				if aatest_uf > 0 && aae+ppint64(1) <= aaemin { /* underflow test */

					/* Exponent e of the result < exponents of the inputs;
					   let's set emin to e + 1, so that one should get an
					   underflow. */
					Xset_emin(cgtls, aae+ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = Xmpfr_gamma(cgtls, cgbp+128, cgbp, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emin(cgtls, aaoemin)
					aaex_flags1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags1 && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for mpfr_gamma, reduced exponent range [%ld,%ld] (underflow test) on:\n\x00", iqlibc.ppVaList(cgbp+232, aae+ppint64(1), aaoemax))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags1)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+232, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_uf--
				}

				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			} /* MPFR_IS_PURE_FP (y) */

			if aaemin > aaemax {
				aaemin = aaemax
			} /* case where all values are singular */

			/* Consistency test in a reduced exponent range. Doing it
			   for the first 10 samples and for prec == p1 (which has
			   some special cases) should be sufficient. */
			if aactrt <= ppuint64(10) || aaprec == aap1 {

				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				Xmpfr_clear_flags(cgtls)
				aainexact = Xmpfr_gamma(cgtls, cgbp+128, cgbp, aarnd)
				aaflags = X__gmpfr_flags
				Xset_emin(cgtls, aaoemin)
				Xset_emax(cgtls, aaoemax)
				/* That test makes no sense for RNDF. */
				if aarnd != ppint32(ecMPFR_RNDF) && !(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+128, cgbp+32) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign) && iqlibc.ppBoolInt32(aainexact > 0)-iqlibc.ppBoolInt32(aainexact < 0) == iqlibc.ppBoolInt32(aacompare > 0)-iqlibc.ppBoolInt32(aacompare < 0) && aaflags == aaoldflags) {

					Xprintf(cgtls, "tgeneric: error for mpfr_gamma, reduced exponent range [%ld,%ld] on:\n\x00", iqlibc.ppVaList(cgbp+232, aaemin, aaemax))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "Expected:\n  y = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+232, aacompare))
					Xflags_out(cgtls, aaoldflags)
					Xprintf(cgtls, "Got:\n  w = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+128)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+232, aainexact))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}

			X__gmpfr_flags = aaoldflags /* restore the flags */
			/* tests in a reduced exponent range */

			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 {
					if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad NaN flag for mpfr_gamma\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

						if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad overflow flag for mpfr_gamma\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(iqlibc.ppBoolInt32(aacompare == 0 && !(aainfinite_input != 0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad divide-by-zero flag for mpfr_gamma\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) {
							if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad underflow flag for mpfr_gamma\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if 0 != 0 {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, ppuintptr(0))
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						}
					}
				}
			} else {
				if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0 {

					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "both overflow and divide-by-zero for mpfr_gamma\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "both underflow and divide-by-zero for mpfr_gamma\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !(aacompare == iqlibc.ppInt32FromInt32(0)) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad compare value (divide-by-zero) for mpfr_gamma\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 {

						if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "both underflow and overflow for mpfr_gamma\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad compare value (overflow) for mpfr_gamma\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						Xmpfr_nexttoinf(cgtls, cgbp+32)
						if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "should have been max MPFR number (overflow) for mpfr_gamma\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {

							if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad compare value (underflow) for mpfr_gamma\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if 0 != 0 {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, ppuintptr(0))
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
							Xmpfr_nexttozero(cgtls, cgbp+32)
							if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "should have been min MPFR number (underflow) for mpfr_gamma\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if 0 != 0 {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, ppuintptr(0))
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						} else {
							if aacompare == 0 || aarnd == ppint32(ecMPFR_RNDF) || Xmpfr_can_round(cgtls, cgbp+32, aayprec, aarnd, aarnd, aaprec) != 0 {

								aactrn++
								{
									aa_p = cgbp + 32
									ccv12 = Xmpfr_set4(cgtls, cgbp+96, aa_p, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
								}
								pp_ = ccv12
								/* Risk of failures are known when some flags are already set
								   before the function call. Do not set the erange flag, as
								   it will remain set after the function call and no checks
								   are performed in such a case (see the mpfr_erangeflag_p
								   test below). */
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									X__gmpfr_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0) ^ iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE))
								}
								/* Let's increase the precision of the inputs in a random way.
								   In most cases, this doesn't make any difference, but for
								   the mpfr_fmod bug fixed in r6230, this triggers the bug. */
								Xmpfr_prec_round(cgtls, cgbp, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint64(15)), ppint32(ecMPFR_RNDN))
								aainexact = Xmpfr_gamma(cgtls, cgbp+64, cgbp, aarnd)
								if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0 {
									goto ppnext_n
								}
								if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+64) != 0) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "tgeneric: results differ for mpfr_gamma on\n\x00", 0)
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "prec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
									Xprintf(cgtls, "Got      \x00", 0)
									Xmpfr_dump(cgtls, cgbp+64)
									Xprintf(cgtls, "Expected \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xprintf(cgtls, "Approx   \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xexit(cgtls, ppint32(1))
								}
								aacompare2 = Xmpfr_cmp3(cgtls, cgbp+96, cgbp+32, ppint32(1))
								/* if rounding to nearest, cannot know the sign of t - f(x)
								   because of composed rounding: y = o(f(x)) and t = o(y) */
								if aacompare*aacompare2 >= 0 {
									aacompare = aacompare + aacompare2
								} else {
									aacompare = aainexact
								} /* cannot determine sign(t-f(x)) */
								if !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aacompare > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aacompare < iqlibc.ppInt32FromInt32(0))) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+232, Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare, aainexact))
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "y = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xprintf(cgtls, "t = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xexit(cgtls, ppint32(1))
								}
							} else {
								if Xgetenv(cgtls, "MPFR_SUSPICIOUS_OVERFLOW\x00") != iqlibc.ppUintptrFromInt32(0) {

									/* For developers only! */

									if ccv13 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv13 {
									}
									if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv13 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0))), ppint64(1)) != 0; !ccv14 {
										Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(803), "(!(((y)->_mpfr_exp) <= (((-0x7fffffffffffffffL-1))+3)) && ((! __builtin_constant_p (!!(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) || !(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0))) || (((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) ? (void) 0 : __builtin_unreachable()), 1))\x00")
									}
									pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
									Xmpfr_nexttoinf(cgtls, cgbp+32)

									if ccv16 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3); ccv16 {
										if ccv15 = aarnd == ppint32(ecMPFR_RNDZ); !ccv15 {
										}
									}
									if ccv16 && (ccv15 || aarnd+iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)) && !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) && iqlibc.Bool(ppint32(mvTGENERIC_SO_TEST) != 0) {

										Xprintf(cgtls, "Possible bug! |y| is the maximum finite number (with yprec = %u) and has\nbeen obtained when rounding toward zero (%s). Thus there is a very\nprobable overflow, but the overflow flag is not set!\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
										Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp)
										Xexit(cgtls, ppint32(1))
									}
								}
							}
						}
					}
				}
			}

			goto ppnext_n
		ppnext_n:
			;
			/* In case the exponent range has been changed by
			   tests_default_random() or for special values... */
			Xset_emin(cgtls, aaold_emin)
			Xset_emax(cgtls, aaold_emax)

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	if Xgetenv(cgtls, "MPFR_TGENERIC_STAT\x00") != iqlibc.ppUintptrFromInt32(0) {
		Xprintf(cgtls, "tgeneric: normal cases / total = %lu / %lu\n\x00", iqlibc.ppVaList(cgbp+232, aactrn, aactrt))
	}

	if ppuint64(3)*aactrn < ppuint64(2)*aactrt {
		Xprintf(cgtls, "Warning! Too few normal cases in generic tests (%lu / %lu)\n\x00", iqlibc.ppVaList(cgbp+232, aactrn, aactrt))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+232, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
}

func sispecial(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aa_p, aa_p1 tnmpfr_ptr
	var aainex, ccv4, ccv8 ppint32
	var ccv1, ccv10, ccv11, ccv2, ccv3, ccv5, ccv6, ccv7, ccv9 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aainex, ccv1, ccv10, ccv11, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)

	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "Error for gamma(NaN)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "Error for gamma(-Inf)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))

	if ccv3 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv3 {
		if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv1 {
			Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(59), "! (((y)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(59), "! (((y)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv3 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error for gamma(+Inf)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv4 = 0
	}
	pp_ = ccv4
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))

	if ccv7 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv7 {
		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(67), "! (((y)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(67), "! (((y)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv7 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error for gamma(+0)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	{
		aa_p1 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv8 = 0
	}
	pp_ = ccv8
	Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))

	if ccv11 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv11 {
		if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv9 {
			Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(76), "! (((y)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv10 {
			Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(76), "! (((y)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv11 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0 {

		Xprintf(cgtls, "Error for gamma(-0)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error for gamma(1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "Error for gamma(-1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(53))

	Xmpfr_set_str(cgtls, cgbp, "1.0762904832837976166\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp, "0.96134843256452096050\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp+32, cgbp, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_lngamma(1.0762904832837976166) is wrong:\nexpected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp, "9.23709516716202383435e-01\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp, "1.0502315560291053398\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp+32, cgbp, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_lngamma(9.23709516716202383435e-01) is wrong:\nexpected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(8))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(175))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(33)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDU))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(175))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.110010101011010101101000010101010111000110011101001000101011000001100010111001101001011E118\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_gamma (1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(21))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(8))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(120)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(21))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.101111101110100110110E654\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_gamma (120)\n\x00", 0)
		Xprintf(cgtls, "Expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(3))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(206))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.110e10\x00")
	aainex = Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(206))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.110111100001000001101010010001000111000100000100111000010011100011011111001100011110101000111101101100110001001100110100001001111110000101010000100100011100010011101110000001000010001100010000101001111E6250\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_gamma (768)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if aainex <= 0 {

		Xprintf(cgtls, "Wrong flag for mpfr_gamma (768)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* worst case to exercise retry */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(1000))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(869))
	Xmpfr_cache(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&X__gmpfr_cache_const_pi)), ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))

	Xmpfr_set_prec(cgtls, cgbp, ppint64(4))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(4))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.1100E-66\x00")
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.1011E67\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for gamma(-0.1100E-66)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(2))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_clear_inexflag(cgtls)
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0 {

		Xprintf(cgtls, "Wrong inexact flag for gamma(2)\n\x00", 0)
		Xprintf(cgtls, "expected 0, got 1\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
}

func sispecial_overflow(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aaemax, aaemin tnmpfr_exp_t
	var aainex ppint32
	var ccv1, ccv2, ccv3, ccv4 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaemin, aainex, ccv1, ccv2, ccv3, ccv4

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax

	Xset_emin(cgtls, ppint64(-ppint32(125)))
	Xset_emax(cgtls, ppint64(128))

	Xmpfr_init2(cgtls, cgbp, ppint64(24))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(24))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.101100100000000000110100E7\x00")
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "Overflow error.\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	/* problem mentioned by Kenneth Wilder, 18 Aug 2005 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(29))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(29))
	Xmpfr_set_str(cgtls, cgbp, "-200000000.5\x00", ppint32(10), ppint32(ecMPFR_RNDN)) /* exact */
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0) {

		Xprintf(cgtls, "Error for gamma(-200000000.5)\n\x00", 0)
		Xprintf(cgtls, "expected -0\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(53))
	Xmpfr_set_str(cgtls, cgbp, "-200000000.1\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0) {

		Xprintf(cgtls, "Error for gamma(-200000000.1), prec=53\n\x00", 0)
		Xprintf(cgtls, "expected -0\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	/* another problem mentioned by Kenneth Wilder, 29 Aug 2005 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(333))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(14))
	Xmpfr_set_str(cgtls, cgbp, "-2.0000000000000000000000005\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(14))
	Xmpfr_set_str_binary(cgtls, cgbp, "-11010011110001E66\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for gamma(-2.0000000000000000000000005)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	/* another tests from Kenneth Wilder, 31 Aug 2005 */
	Xset_emax(cgtls, ppint64(200))
	Xset_emin(cgtls, ppint64(-ppint32(200)))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(38))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(54))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11101111011100111101001001010110101001E-166\x00")
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(54))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.100010001101100001110110001010111111010000100101011E167\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for gamma (test 1)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xset_emax(cgtls, ppint64(1000))
	Xset_emin(cgtls, ppint64(-ppint32(2000)))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(38))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(71))
	Xmpfr_set_str_binary(cgtls, cgbp, "10101011011100001111111000010111110010E-1034\x00")
	/* 184083777010*2^(-1034) */
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(71))
	Xmpfr_set_str_binary(cgtls, cgbp, "10111111001000011110010001000000000000110011110000000011101011111111100E926\x00")
	/* 1762885132679550982140*2^926 */
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for gamma (test 2)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(38))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(88))
	Xmpfr_set_str_binary(cgtls, cgbp, "10111100111001010000100001100100100101E-104\x00")
	/* 202824096037*2^(-104) */
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(88))
	Xmpfr_set_str_binary(cgtls, cgbp, "1010110101111000111010111100010110101010100110111000001011000111000011101100001101110010E-21\x00")
	/* 209715199999500283894743922*2^(-21) */
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for gamma (test 3)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(171))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(38))
	Xmpfr_set_str(cgtls, cgbp, "-2993155353253689176481146537402947624254601559176535\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(170), ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(38))
	Xmpfr_set_str(cgtls, cgbp, "201948391737\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(92), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for gamma (test 5)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xset_emin(cgtls, ppint64(-ppint32(500000)))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(337))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(38))
	Xmpfr_set_str(cgtls, cgbp, "-30000.000000000000000000000000000000000000000000001\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(38))
	Xmpfr_set_str(cgtls, cgbp, "-3.623795987425E-121243\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for gamma (test 7)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	/* was producing infinite loop */
	Xset_emin(cgtls, aaemin)
	Xmpfr_set_prec(cgtls, cgbp, ppint64(71))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(71))
	Xmpfr_set_str(cgtls, cgbp, "-200000000.1\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0) {

		Xprintf(cgtls, "Error for gamma (test 8)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xset_emax(cgtls, ppint64(1073741821))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(29))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(29))
	Xmpfr_set_str(cgtls, cgbp, "423786866\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))

	if ccv3 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv3 {
		if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv1 {
			Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(377), "! (((y)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(377), "! (((y)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv3 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "Error for gamma(423786866)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* check exact result */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(3)), 0, ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_gamma(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if aainex != 0 || Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) != 0 {

		Xprintf(cgtls, "Error for gamma(3)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xset_emax(cgtls, ppint64(1024))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(53))
	Xmpfr_set_str_binary(cgtls, cgbp, "101010110100110011111010000110001000111100000110101E-43\x00")
	Xmpfr_gamma(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "110000011110001000111110110101011110000100001111111E971\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error for gamma(4)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)

	/* bug found by Kevin Rauch, 26 Oct 2007 */
	Xmpfr_set_str(cgtls, cgbp, "1e19\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_gamma(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && aainex > 0)), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(413), "(((mpfr_srcptr) (0 ? (x) : (mpfr_srcptr) (x)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && inex > 0\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* test gamma on some integral values (from Christopher Creutzig). */
func sigamma_integer(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aai ppuint32
	var pp_ /* n at bp+0 */ tnmpz_t
	var pp_ /* x at bp+16 */ tnmpfr_t
	var pp_ /* y at bp+48 */ tnmpfr_t
	pp_ = aai

	Xmpfr_mpz_init(cgtls, cgbp)
	Xmpfr_init2(cgtls, cgbp+16, ppint64(149))
	Xmpfr_init2(cgtls, cgbp+48, ppint64(149))

	aai = ppuint32(0)
	for {
		if !(aai < ppuint32(100)) {
			break
		}

		X__gmpz_fac_ui(cgtls, cgbp, ppuint64(aai))
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+16, ppuint64(aai+iqlibc.ppUint32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
		Xmpfr_gamma(cgtls, cgbp+48, cgbp+16, ppint32(ecMPFR_RNDN))
		Xmpfr_set_z(cgtls, cgbp+16, cgbp, ppint32(ecMPFR_RNDN))
		if !(Xmpfr_equal_p(cgtls, cgbp+16, cgbp+48) != 0) {

			Xprintf(cgtls, "Error for gamma(%u)\n\x00", iqlibc.ppVaList(cgbp+88, aai+ppuint32(1)))
			Xprintf(cgtls, "expected \x00", 0)
			Xmpfr_dump(cgtls, cgbp+16)
			Xprintf(cgtls, "got      \x00", 0)
			Xmpfr_dump(cgtls, cgbp+48)
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aai++
	}
	Xmpfr_clear(cgtls, cgbp+48)
	Xmpfr_clear(cgtls, cgbp+16)
	Xmpfr_mpz_clear(cgtls, cgbp)
}

// C documentation
//
//	/* bug found by Kevin Rauch */
func sitest20071231(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aaemin tnmpfr_exp_t
	var aainex ppint32
	var ccv1, ccv2 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aaemin, aainex, ccv1, ccv2

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint64(-ppint32(1000000)))

	Xmpfr_init2(cgtls, cgbp, ppint64(21))
	Xmpfr_set_str(cgtls, cgbp, "-1000001.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_gamma(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 && aainex < 0)), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(464), "(((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)) && (((x)->_mpfr_sign) > 0) && inex < 0\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp)

	Xset_emin(cgtls, aaemin)

	Xmpfr_init2(cgtls, cgbp, ppint64(53))
	Xmpfr_set_str(cgtls, cgbp, "-1000000001.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_gamma(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 && aainex < 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(472), "(((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)) && (((x)->_mpfr_sign) > 0) && inex < 0\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* bug found by Stathis in mpfr_gamma, only occurs on 32-bit machines;
//	   the second test is for 64-bit machines. This bug reappeared due to
//	   r8159. */
func sitest20100709(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aa_p, aa_p1 tnmpfr_ptr
	var aaemin tnmpfr_exp_t
	var aainex, ccv1, ccv6 ppint32
	var ccv10, ccv2, ccv4, ccv5, ccv7, ccv9 ppbool
	var pp_ /* sign at bp+96 */ ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aaemin, aainex, ccv1, ccv10, ccv2, ccv4, ccv5, ccv6, ccv7, ccv9

	Xmpfr_init2(cgtls, cgbp, ppint64(100))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(32))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(32))
	Xmpfr_set_str(cgtls, cgbp, "-4.6308260837372266e+07\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	{
		aa_p = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1
	Xmpfr_nextabove(cgtls, cgbp+32)
	Xmpfr_log(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDD))
	Xmpfr_cache(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&X__gmpfr_cache_const_log2)), ppint32(ecMPFR_RNDU))
	Xmpfr_sub(cgtls, cgbp+32, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD)) /* log(MIN/2) = log(MIN) - log(2) */
	Xmpfr_lgamma(cgtls, cgbp+64, cgbp+96, cgbp, ppint32(ecMPFR_RNDU))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 96)) == -iqlibc.ppInt32FromInt32(1))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(497), "sign == -1\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_less_p(cgtls, cgbp+64, cgbp+32) != 0)), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(498), "mpfr_less_p (z, y)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_less_p(cgtls, cgbp+64, cgbp+32) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* thus underflow */
	aainex = Xmpfr_gamma(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 && aainex > 0)), ppint64(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(500), "(((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)) && (((x)->_mpfr_sign) < 0) && inex > 0\x00")
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)

	/* Similar test for 64-bit machines (also valid with a 32-bit exponent,
	   but will not trigger the bug). */
	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
	Xmpfr_init2(cgtls, cgbp, ppint64(100))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(32))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(32))
	Xmpfr_set_str(cgtls, cgbp, "-90.6308260837372266e+15\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	{
		aa_p1 = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv6 = 0
	}
	pp_ = ccv6
	Xmpfr_nextabove(cgtls, cgbp+32)
	Xmpfr_log(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDD))
	Xmpfr_cache(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&X__gmpfr_cache_const_log2)), ppint32(ecMPFR_RNDU))
	Xmpfr_sub(cgtls, cgbp+32, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD)) /* log(MIN/2) = log(MIN) - log(2) */
	Xmpfr_lgamma(cgtls, cgbp+64, cgbp+96, cgbp, ppint32(ecMPFR_RNDU))

	if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(*(*ppint32)(iqunsafe.ppPointer(cgbp + 96)) == -iqlibc.ppInt32FromInt32(1))), ppint64(1)) != 0; !ccv7 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(519), "sign == -1\x00")
	}
	pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_less_p(cgtls, cgbp+64, cgbp+32) != 0)), ppint64(1)) != 0; !ccv9 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(520), "mpfr_less_p (z, y)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_less_p(cgtls, cgbp+64, cgbp+32) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* thus underflow */
	aainex = Xmpfr_gamma(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0 && aainex > 0)), ppint64(1)) != 0; !ccv10 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(522), "(((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)) && (((x)->_mpfr_sign) < 0) && inex > 0\x00")
	}
	pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xset_emin(cgtls, aaemin)
}

// C documentation
//
//	/* bug found by Giridhar Tammana */
func sitest20120426(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaemin tnmpfr_exp_t
	var aai ppint32
	var pp_ /* xa at bp+0 */ tnmpfr_t
	var pp_ /* xb at bp+32 */ tnmpfr_t
	pp_, pp_ = aaemin, aai

	Xmpfr_init2(cgtls, cgbp, ppint64(53))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(53))
	Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(-ppint32(337)), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN))
	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint64(-ppint32(1073)))
	aai = Xmpfr_gamma(cgtls, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	aai = Xmpfr_subnormalize(cgtls, cgbp, aai, ppint32(ecMPFR_RNDN)) /* new ternary value */
	Xmpfr_set_str(cgtls, cgbp+32, "-9.5737343987585366746184749943e-304\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	if !(aai > 0 && Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) == 0) {

		Xprintf(cgtls, "Error in test20120426, i=%d\n\x00", iqlibc.ppVaList(cgbp+72, aai))
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xset_emin(cgtls, aaemin)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
}

func siexprange(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aa_p tnmpfr_ptr
	var aaemax, aaemin tnmpfr_exp_t
	var aaflags1, aaflags2 ppuint32
	var aainex1, aainex2, ccv12, ccv5, ccv6 ppint32
	var ccv1, ccv10, ccv11, ccv13, ccv14, ccv15, ccv16, ccv17, ccv18, ccv2, ccv3, ccv4, ccv7, ccv8, ccv9 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aaemax, aaemin, aaflags1, aaflags2, aainex1, aainex2, ccv1, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv16, ccv17, ccv18, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax

	Xmpfr_init2(cgtls, cgbp, ppint64(16))
	Xmpfr_inits2(cgtls, ppint64(8), cgbp+32, iqlibc.ppVaList(cgbp+104, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(5), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	aainex1 = Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	aaflags1 = X__gmpfr_flags

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(577), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emin(cgtls, 0)
	Xmpfr_clear_flags(cgtls)
	aainex2 = Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDN))
	aaflags2 = X__gmpfr_flags

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(582), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emin(cgtls, aaemin)
	if aainex1 != aainex2 || aaflags1 != aaflags2 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in exprange (test1)\n\x00", 0)
		Xprintf(cgtls, "x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected inex1 = %d, flags1 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0), aaflags1))
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      inex2 = %d, flags2 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0), aaflags2))
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(32769), ppint64(-ppint32(60)), ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	aainex1 = Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDD))
	aaflags1 = X__gmpfr_flags

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(600), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emax(cgtls, ppint64(45))
	Xmpfr_clear_flags(cgtls)
	aainex2 = Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDD))
	aaflags2 = X__gmpfr_flags

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(605), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emax(cgtls, aaemax)
	if aainex1 != aainex2 || aaflags1 != aaflags2 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in exprange (test2)\n\x00", 0)
		Xprintf(cgtls, "x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected inex1 = %d, flags1 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0), aaflags1))
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      inex2 = %d, flags2 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0), aaflags2))
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}

	Xset_emax(cgtls, ppint64(44))
	Xmpfr_clear_flags(cgtls)
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp >= X__gmpfr_emin && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= X__gmpfr_emax)), ppint64(1)) != 0 {
		if aainex1 != 0 {
			X__gmpfr_flags |= ppuint32(mvMPFR_FLAGS_INEXACT)
			ccv6 = aainex1
		} else {
			ccv6 = 0
		}
		ccv5 = ccv6
	} else {
		ccv5 = Xmpfr_check_range(cgtls, cgbp+32, aainex1, ppint32(ecMPFR_RNDD))
	}
	aainex1 = ccv5
	aaflags1 = X__gmpfr_flags

	if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv7 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(623), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_flags(cgtls)
	aainex2 = Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDD))
	aaflags2 = X__gmpfr_flags

	if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv8 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(627), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emax(cgtls, aaemax)
	if aainex1 != aainex2 || aaflags1 != aaflags2 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in exprange (test3)\n\x00", 0)
		Xprintf(cgtls, "x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected inex1 = %d, flags1 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0), aaflags1))
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      inex2 = %d, flags2 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0), aaflags2))
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(1), ppint64(-ppint32(60)), ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	aainex1 = Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDD))
	aaflags1 = X__gmpfr_flags

	if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv9 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(645), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emax(cgtls, ppint64(60))
	Xmpfr_clear_flags(cgtls)
	aainex2 = Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDD))
	aaflags2 = X__gmpfr_flags

	if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv10 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(650), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emax(cgtls, aaemax)
	if aainex1 != aainex2 || aaflags1 != aaflags2 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in exprange (test4)\n\x00", 0)
		Xprintf(cgtls, "x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected inex1 = %d, flags1 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0), aaflags1))
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      inex2 = %d, flags2 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0), aaflags2))
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}

	if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) == -(iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1)))), ppint64(1)) != 0; !ccv11 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(664), "(1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) == - (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1)\x00")
	}
	pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
	Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv12 = 0
	}
	pp_ = ccv12
	Xmpfr_nextabove(cgtls, cgbp) /* x = 2^(emin - 1) */
	Xmpfr_set_inf(cgtls, cgbp+32, ppint32(1))
	aainex1 = ppint32(1)
	aaflags1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW))
	Xmpfr_clear_flags(cgtls)
	/* MPFR_RNDU: overflow, infinity since 1/x = 2^(emax + 1) */
	aainex2 = Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDU))
	aaflags2 = X__gmpfr_flags

	if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv13 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(676), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if aainex1 != aainex2 || aaflags1 != aaflags2 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in exprange (test5)\n\x00", 0)
		Xprintf(cgtls, "x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected inex1 = %d, flags1 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0), aaflags1))
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      inex2 = %d, flags2 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0), aaflags2))
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear_flags(cgtls)
	/* MPFR_RNDN: overflow, infinity since 1/x = 2^(emax + 1) */
	aainex2 = Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDN))
	aaflags2 = X__gmpfr_flags

	if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv14 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(692), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if aainex1 != aainex2 || aaflags1 != aaflags2 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in exprange (test6)\n\x00", 0)
		Xprintf(cgtls, "x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected inex1 = %d, flags1 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0), aaflags1))
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      inex2 = %d, flags2 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0), aaflags2))
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_nextbelow(cgtls, cgbp+32)
	aainex1 = -ppint32(1)
	Xmpfr_clear_flags(cgtls)
	/* MPFR_RNDD: overflow, maxnum since 1/x = 2^(emax + 1) */
	aainex2 = Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDD))
	aaflags2 = X__gmpfr_flags

	if ccv15 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv15 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(710), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv15 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if aainex1 != aainex2 || aaflags1 != aaflags2 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in exprange (test7)\n\x00", 0)
		Xprintf(cgtls, "x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected inex1 = %d, flags1 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0), aaflags1))
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      inex2 = %d, flags2 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0), aaflags2))
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(1), ppint32(ecMPFR_RNDN)) /* x = 2^emin */
	Xmpfr_set_inf(cgtls, cgbp+32, ppint32(1))
	aainex1 = ppint32(1)
	aaflags1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW))
	Xmpfr_clear_flags(cgtls)
	/* MPFR_RNDU: overflow, infinity since 1/x = 2^emax */
	aainex2 = Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDU))
	aaflags2 = X__gmpfr_flags

	if ccv16 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv16 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(730), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv16 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if aainex1 != aainex2 || aaflags1 != aaflags2 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in exprange (test8)\n\x00", 0)
		Xprintf(cgtls, "x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected inex1 = %d, flags1 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0), aaflags1))
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      inex2 = %d, flags2 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0), aaflags2))
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear_flags(cgtls)
	/* MPFR_RNDN: overflow, infinity since 1/x = 2^emax */
	aainex2 = Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDN))
	aaflags2 = X__gmpfr_flags

	if ccv17 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv17 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(746), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv17 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if aainex1 != aainex2 || aaflags1 != aaflags2 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in exprange (test9)\n\x00", 0)
		Xprintf(cgtls, "x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected inex1 = %d, flags1 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0), aaflags1))
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      inex2 = %d, flags2 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0), aaflags2))
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_nextbelow(cgtls, cgbp+32)
	aainex1 = -ppint32(1)
	aaflags1 = ppuint32(mvMPFR_FLAGS_INEXACT)
	Xmpfr_clear_flags(cgtls)
	/* MPFR_RNDD: no overflow, maxnum since 1/x = 2^emax and euler > 0 */
	aainex2 = Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDD))
	aaflags2 = X__gmpfr_flags

	if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv18 {
		Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(765), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if aainex1 != aainex2 || aaflags1 != aaflags2 || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in exprange (test10)\n\x00", 0)
		Xprintf(cgtls, "x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected inex1 = %d, flags1 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex1 > 0)-iqlibc.ppBoolInt32(aainex1 < 0), aaflags1))
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      inex2 = %d, flags2 = %u, \x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex2 > 0)-iqlibc.ppBoolInt32(aainex2 < 0), aaflags2))
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}
	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

func sitiny_aux(cgtls *iqlibc.ppTLS, aastop ppint32, aae tnmpfr_exp_t) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aaemax, aaexponent, aasaved_emax tnmpfr_exp_t
	var aaerr, aaexpected_inex, aainex, aaoverflow, aar, aas, aaspm ppint32
	var aaexpected_flags, aaflags ppuint32
	var aarr tnmpfr_rnd_t
	var ccv3 ppint64
	var ccv5 ppuintptr
	var pp_ /* expected_dir at bp+96 */ [2][5]ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaerr, aaexpected_flags, aaexpected_inex, aaexponent, aaflags, aainex, aaoverflow, aar, aarr, aas, aasaved_emax, aaspm, ccv3, ccv5
	aaerr = 0
	*(*[2][5]ppint32)(iqunsafe.ppPointer(cgbp + 96)) = [2][5]ppint32{
		0: {
			0: ppint32(1),
			1: -ppint32(1),
			2: ppint32(1),
			3: -ppint32(1),
			4: ppint32(1),
		},
		1: {
			0: ppint32(1),
			1: ppint32(1),
			2: ppint32(1),
			3: -ppint32(1),
			4: -ppint32(1),
		},
	}

	aasaved_emax = X__gmpfr_emax

	Xmpfr_init2(cgtls, cgbp, ppint64(32))
	Xmpfr_inits2(cgtls, ppint64(8), cgbp+32, iqlibc.ppVaList(cgbp+144, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(1), aae, ppint32(ecMPFR_RNDN))
	aaspm = ppint32(1)
	aas = 0
	for {
		if !(aas < ppint32(2)) {
			break
		}

		aar = 0
		for {
			if !(aar < ppint32(ecMPFR_RNDF)) {
				break
			}

			aarr = aar

			/* Exponent of the rounded value in unbounded exponent range. */
			if *(*ppint32)(iqunsafe.ppPointer(cgbp + 96 + ppuintptr(aas)*20 + ppuintptr(aar)*4)) < 0 && aas == 0 {
				ccv3 = -aae
			} else {
				ccv3 = ppint64(1) - aae
			}
			aaexponent = ccv3

			aaemax = aaexponent - ppint64(1)
			for {
				if !(aaemax <= aaexponent) {
					break
				}
				aaexpected_flags = ppuint32(mvMPFR_FLAGS_INEXACT)
				aaexpected_inex = *(*ppint32)(iqunsafe.ppPointer(cgbp + 96 + ppuintptr(aas)*20 + ppuintptr(aar)*4))

				if aaemax > iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1) {
					break
				}
				Xset_emax(cgtls, aaemax)

				Xmpfr_clear_flags(cgtls)
				aainex = Xmpfr_gamma(cgtls, cgbp+32, cgbp, aarr)
				aaflags = X__gmpfr_flags
				Xmpfr_clear_flags(cgtls)
				Xmpfr_set_si_2exp(cgtls, cgbp+64, ppint64(aaspm), -aae, ppint32(ecMPFR_RNDU))
				aaoverflow = iqlibc.ppInt32FromUint32(X__gmpfr_flags & iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW))
				/* z is 1/x - euler rounded toward +inf */

				if aaoverflow != 0 && aarr == ppint32(ecMPFR_RNDN) && aas == ppint32(1) {
					aaexpected_inex = -ppint32(1)
				}

				if aaexpected_inex < 0 {
					Xmpfr_nextbelow(cgtls, cgbp+64)
				} /* 1/x - euler rounded toward -inf */

				if aaexponent > aaemax {

					aaexpected_flags |= ppuint32(mvMPFR_FLAGS_OVERFLOW)
				}

				if !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0 && aaflags == aaexpected_flags && iqlibc.ppBoolInt32(aainex > 0)-iqlibc.ppBoolInt32(aainex < 0) == iqlibc.ppBoolInt32(aaexpected_inex > 0)-iqlibc.ppBoolInt32(aaexpected_inex < 0)) {

					if aaexponent > aaemax {
						ccv5 = " (overflow)\x00"
					} else {
						ccv5 = "\x00"
					}
					Xprintf(cgtls, "Error in tiny for s = %d, r = %s, emax = %ld%s\n  on \x00", iqlibc.ppVaList(cgbp+144, aas, Xmpfr_print_rnd_mode(cgtls, aarr), aaemax, ccv5))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "  expected inex = %2d, \x00", iqlibc.ppVaList(cgbp+144, aaexpected_inex))
					Xmpfr_dump(cgtls, cgbp+64)
					Xprintf(cgtls, "  got      inex = %2d, \x00", iqlibc.ppVaList(cgbp+144, iqlibc.ppBoolInt32(aainex > 0)-iqlibc.ppBoolInt32(aainex < 0)))
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "  expected flags = %u, got %u\n\x00", iqlibc.ppVaList(cgbp+144, aaexpected_flags, aaflags))
					if aastop != 0 {
						Xexit(cgtls, ppint32(1))
					}
					aaerr = ppint32(1)
				}

				goto cg_4
			cg_4:
				;
				aaemax++
			}

			goto cg_2
		cg_2:
			;
			aar++
		}
		Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
		aaspm = -aaspm

		goto cg_1
	cg_1:
		;
		aas++
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+144, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xset_emax(cgtls, aasaved_emax)
	return aaerr
}

func sitiny(cgtls *iqlibc.ppTLS, aastop ppint32) {

	var aaemin tnmpfr_exp_t
	var aaerr ppint32
	pp_, pp_ = aaemin, aaerr
	aaerr = 0

	aaemin = X__gmpfr_emin

	/* Note: in r7499, exponent -17 will select the generic code (in
	   tiny_aux, x has precision 32), while the other exponent values
	   will select special code for tiny values. */

	aaerr |= sitiny_aux(cgtls, aastop, ppint64(-ppint32(17)))

	aaerr |= sitiny_aux(cgtls, aastop, ppint64(-ppint32(999)))

	aaerr |= sitiny_aux(cgtls, aastop, X__gmpfr_emin)

	if aaemin != iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) {

		Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))

		aaerr |= sitiny_aux(cgtls, aastop, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
		Xset_emin(cgtls, aaemin)
	}

	if aaerr != 0 {
		Xexit(cgtls, ppint32(1))
	}
}

// C documentation
//
//	/* Test mpfr_gamma in precision p1 by comparing it with exp(lgamma(x))
//	   computing with a working precision p2. Assume that x is not an
//	   integer <= 2. */
func siexp_lgamma(cgtls *iqlibc.ppTLS, aax tnmpfr_ptr, aap1 tnmpfr_prec_t, aap2 tnmpfr_prec_t) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aa_p tnmpfr_srcptr
	var aagot_overflow, aagot_underflow, aainexd, aainexu, aaoverflow, aaunderflow, ccv1, ccv2, ccv3, ccv4, ccv5 ppint32
	var ccv6, ccv7 ppbool
	var pp_ /* sign at bp+128 */ ppint32
	var pp_ /* yd at bp+0 */ tnmpfr_t
	var pp_ /* yu at bp+32 */ tnmpfr_t
	var pp_ /* zd at bp+64 */ tnmpfr_t
	var pp_ /* zu at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aagot_overflow, aagot_underflow, aainexd, aainexu, aaoverflow, aaunderflow, ccv1, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7
	aaunderflow = -ppint32(1)
	aaoverflow = -ppint32(1)

	if Xmpfr_integer_p(cgtls, aax) != 0 && Xmpfr_cmp_ui_2exp(cgtls, aax, iqlibc.ppUint64FromInt64(ppint64(iqlibc.ppInt32FromInt32(2))), 0) <= 0 {

		Xprintf(cgtls, "Warning! x is an integer <= 2 in exp_lgamma: \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), aax, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		return
	}
	Xmpfr_inits2(cgtls, aap2, cgbp, iqlibc.ppVaList(cgbp+144, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
	aainexd = Xmpfr_lgamma(cgtls, cgbp, cgbp+128, aax, ppint32(ecMPFR_RNDD))
	{
		aa_p = cgbp
		ccv1 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
	}
	pp_ = ccv1 /* exact */
	if aainexd != 0 {
		Xmpfr_nextabove(cgtls, cgbp+32)
	}
	Xmpfr_clear_flags(cgtls)
	Xmpfr_exp(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDD))
	if !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
		aaunderflow = 0
	}
	if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 {
		aaoverflow = ppint32(1)
	}
	Xmpfr_clear_flags(cgtls)
	Xmpfr_exp(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDU))
	if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {
		aaunderflow = ppint32(1)
	}
	if !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) {
		aaoverflow = 0
	}
	if *(*ppint32)(iqunsafe.ppPointer(cgbp + 128)) < 0 {

		Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))       /* exact */
		Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN)) /* exact */
		Xmpfr_swap(cgtls, cgbp, cgbp+32)
	}
	/* yd < Gamma(x) < yu (strict inequalities since x != 1 and x != 2) */
	Xmpfr_inits2(cgtls, aap1, cgbp+64, iqlibc.ppVaList(cgbp+144, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_clear_flags(cgtls)
	aainexd = Xmpfr_gamma(cgtls, cgbp+64, aax, ppint32(ecMPFR_RNDD)) /* zd <= Gamma(x) < yu */
	if aaunderflow == -ppint32(1) {
		ccv2 = -ppint32(1)
	} else {
		ccv2 = iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0))
	}
	aagot_underflow = ccv2
	if aaoverflow == -ppint32(1) {
		ccv3 = -ppint32(1)
	} else {
		ccv3 = iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0))
	}
	aagot_overflow = ccv3
	if !(Xmpfr_less_p(cgtls, cgbp+64, cgbp+32) != 0) || aainexd > 0 || aagot_underflow != aaunderflow || aagot_overflow != aaoverflow {

		Xprintf(cgtls, "Error in exp_lgamma on x = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), aax, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xprintf(cgtls, "yu = \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "zd = \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xprintf(cgtls, "got inexd = %d, expected <= 0\n\x00", iqlibc.ppVaList(cgbp+144, aainexd))
		Xprintf(cgtls, "got underflow = %d, expected %d\n\x00", iqlibc.ppVaList(cgbp+144, aagot_underflow, aaunderflow))
		Xprintf(cgtls, "got overflow = %d, expected %d\n\x00", iqlibc.ppVaList(cgbp+144, aagot_overflow, aaoverflow))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear_flags(cgtls)
	aainexu = Xmpfr_gamma(cgtls, cgbp+96, aax, ppint32(ecMPFR_RNDU)) /* zu >= Gamma(x) > yd */
	if aaunderflow == -ppint32(1) {
		ccv4 = -ppint32(1)
	} else {
		ccv4 = iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0))
	}
	aagot_underflow = ccv4
	if aaoverflow == -ppint32(1) {
		ccv5 = -ppint32(1)
	} else {
		ccv5 = iqlibc.ppBoolInt32(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0))
	}
	aagot_overflow = ccv5
	if !(Xmpfr_greater_p(cgtls, cgbp+96, cgbp) != 0) || aainexu < 0 || aagot_underflow != aaunderflow || aagot_overflow != aaoverflow {

		Xprintf(cgtls, "Error in exp_lgamma on x = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), aax, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xprintf(cgtls, "yd = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "zu = \x00", 0)
		Xmpfr_dump(cgtls, cgbp+96)
		Xprintf(cgtls, "got inexu = %d, expected >= 0\n\x00", iqlibc.ppVaList(cgbp+144, aainexu))
		Xprintf(cgtls, "got underflow = %d, expected %d\n\x00", iqlibc.ppVaList(cgbp+144, aagot_underflow, aaunderflow))
		Xprintf(cgtls, "got overflow = %d, expected %d\n\x00", iqlibc.ppVaList(cgbp+144, aagot_overflow, aaoverflow))
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_equal_p(cgtls, cgbp+64, cgbp+96) != 0 {

		if aainexd != 0 || aainexu != 0 {

			Xprintf(cgtls, "Error in exp_lgamma on x = \x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), aax, ppint32(ecMPFR_RNDN))
			Xputchar(cgtls, ppint32('\n'))
			Xprintf(cgtls, "zd = zu, thus exact, but inexd = %d and inexu = %d\n\x00", iqlibc.ppVaList(cgbp+144, aainexd, aainexu))
			Xexit(cgtls, ppint32(1))
		}

		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aagot_underflow == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(979), "got_underflow == 0\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aagot_overflow == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv7 {
			Xmpfr_assert_fail(cgtls, "tgamma.c\x00", ppint32(980), "got_overflow == 0\x00")
		}
		pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	} else {
		if aainexd == 0 || aainexu == 0 {

			Xprintf(cgtls, "Error in exp_lgamma on x = \x00", 0)
			X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), aax, ppint32(ecMPFR_RNDN))
			Xputchar(cgtls, ppint32('\n'))
			Xprintf(cgtls, "zd != zu, thus inexact, but inexd = %d and inexu = %d\n\x00", iqlibc.ppVaList(cgbp+144, aainexd, aainexu))
			Xexit(cgtls, ppint32(1))
		}
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+144, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
}

func siexp_lgamma_tests(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aaemax, aaemin tnmpfr_exp_t
	var aai ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_ = aaemax, aaemin, aai

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
	Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))

	Xmpfr_init2(cgtls, cgbp, ppint64(96))
	aai = ppint32(3)
	for {
		if !(aai <= ppint32(8)) {
			break
		}

		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(aai), 0, ppint32(ecMPFR_RNDN))
		siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))
		Xmpfr_nextbelow(cgtls, cgbp)
		siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))
		Xmpfr_nextabove(cgtls, cgbp)
		Xmpfr_nextabove(cgtls, cgbp)
		siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))

		goto cg_1
	cg_1:
		;
		aai++
	}
	Xmpfr_set_str(cgtls, cgbp, "1.7\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))
	Xmpfr_set_str(cgtls, cgbp, "-4.6308260837372266e+07\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))
	Xmpfr_set_str(cgtls, cgbp, "-90.6308260837372266e+15\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))
	/* The following test gives a large positive result < +Inf */
	Xmpfr_set_str(cgtls, cgbp, "1.2b13fc45a92dea1@14\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))
	/* Idem for a large negative result > -Inf */
	Xmpfr_set_str(cgtls, cgbp, "-1.2b13fc45a92de81@14\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))
	/* The following two tests trigger an endless loop in r8186
	   on 64-bit machines (64-bit exponent). The second one (due
	   to undetected overflow) is a direct consequence of the
	   first one, due to the call of Gamma(2-x) if x < 1. */
	Xmpfr_set_str(cgtls, cgbp, "1.2b13fc45a92dec8@14\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))
	Xmpfr_set_str(cgtls, cgbp, "-1.2b13fc45a92dea8@14\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))
	/* Similar tests (overflow threshold) for 32-bit machines. */
	Xmpfr_set_str(cgtls, cgbp, "2ab68d8.657542f855111c61\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(12), ppint64(64))
	Xmpfr_set_str(cgtls, cgbp, "-2ab68d6.657542f855111c61\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(12), ppint64(64))
	/* The following test is an overflow on 32-bit and 64-bit machines.
	   Revision r8189 fails on 64-bit machines as the flag is unset. */
	Xmpfr_set_str(cgtls, cgbp, "1.2b13fc45a92ded8@14\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(53), ppint64(64))
	/* On the following tests, with r8196, one gets an underflow on
	   32-bit machines, while a normal result is expected (see FIXME
	   in gamma.c:382). */
	Xmpfr_set_str(cgtls, cgbp, "-2ab68d6.657542f855111c6104\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(12), ppint64(64)) /* failure on 32-bit machines */
	Xmpfr_set_str(cgtls, cgbp, "-12b13fc45a92deb.1c6c5bc964\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	siexp_lgamma(cgtls, cgbp, ppint64(12), ppint64(64)) /* failure on 64-bit machines */
	Xmpfr_clear(cgtls, cgbp)

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

// C documentation
//
//	/* Bug reported by Frithjof Blomquist on May 19, 2020.
//	   For the record, this bug was present since r8981
//	   (in mpfr_bernoulli_internal, den was wrongly reset to 1 in case
//	   of failure in Ziv's loop). The bug only occurred up from r8986
//	   where the initial precision was reduced, but was potentially
//	   present in any case of failure of Ziv's loop. */
func sibug20200519(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(144)
	defer cgtls.ppFree(144)

	var aadd ppfloat64
	var aamin_memory_limit, aaold_memory_limit tnsize_t
	var aaprec tnmpfr_prec_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aadd, aamin_memory_limit, aaold_memory_limit, aaprec
	aaprec = ppint64(25093)

	aaold_memory_limit = Xtests_memory_limit
	aamin_memory_limit = ppuint64(24000000)
	if Xtests_memory_limit > ppuint64(0) && Xtests_memory_limit < aamin_memory_limit {
		Xtests_memory_limit = aamin_memory_limit
	}

	Xmpfr_init2(cgtls, cgbp, aaprec)
	Xmpfr_init2(cgtls, cgbp+32, aaprec)
	Xmpfr_init2(cgtls, cgbp+64, aaprec+ppint64(100))
	Xmpfr_init2(cgtls, cgbp+96, ppint64(24))
	Xmpfr_set_d(cgtls, cgbp, ppfloat64(2.5), ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_gamma(cgtls, cgbp+64, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_sub(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))
	Xmpfr_mul_2si(cgtls, cgbp+96, cgbp+96, aaprec-(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp, ppint32(ecMPFR_RNDN))
	aadd = Xmpfr_get_d(cgtls, cgbp+96, ppint32(ecMPFR_RNDN))
	if aadd < -iqlibc.ppFloat64FromFloat64(0.5) || ppfloat64(0.5) < aadd {

		Xprintf(cgtls, "Error in bug20200519: dd=%f\n\x00", iqlibc.ppVaList(cgbp+136, aadd))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)

	Xtests_memory_limit = aaold_memory_limit
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aap tnmpfr_prec_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_ = aap
	Xtests_start_mpfr(cgtls)

	if aaargc == ppint32(3) { /* tgamma x prec: print gamma(x) to prec bits */

		aap = ppint64(Xatoi(cgtls, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 2*8))))
		Xmpfr_init2(cgtls, cgbp, aap)
		Xmpfr_set_str(cgtls, cgbp, *(*ppuintptr)(iqunsafe.ppPointer(aaargv + 1*8)), ppint32(10), ppint32(ecMPFR_RNDN))
		Xmpfr_gamma(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xmpfr_clear(cgtls, cgbp)
		return 0
	}

	sispecial(cgtls)
	sispecial_overflow(cgtls)
	siexprange(cgtls)
	sitiny(cgtls, iqlibc.ppBoolInt32(aaargc == ppint32(1)))
	sitest_generic(cgtls, ppint64(mvMPFR_PREC_MIN), ppint64(100), ppuint32(2))
	sigamma_integer(cgtls)
	sitest20071231(cgtls)
	sitest20100709(cgtls)
	sitest20120426(cgtls)
	siexp_lgamma_tests(cgtls)

	Xdata_check(cgtls, "data/gamma\x00", pp__ccgo_fp(Xmpfr_gamma), "mpfr_gamma\x00")

	/* this test takes about one minute */
	if Xgetenv(cgtls, "MPFR_CHECK_EXPENSIVE\x00") != iqlibc.ppUintptrFromInt32(0) && Xgetenv(cgtls, "MPFR_CHECK_LARGEMEM\x00") != iqlibc.ppUintptrFromInt32(0) {
		sibug20200519(cgtls)
	}

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___builtin_unreachable(*iqlibc.ppTLS)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

var ___gmpfr_cache_const_log2 [1]ts__gmpfr_cache_s

var ___gmpfr_cache_const_pi [1]ts__gmpfr_cache_s

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuint64

func ___gmpz_fac_ui(*iqlibc.ppTLS, ppuintptr, ppuint64)

func _atoi(*iqlibc.ppTLS, ppuintptr) ppint32

func _data_check(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_cache(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_can_round(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32, ppint32, ppint64) ppint32

func _mpfr_check_range(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clear_inexflag(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp3(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64) ppint32

func _mpfr_div_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_exp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_gamma(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_get_d(*iqlibc.ppTLS, ppuintptr, ppint32) ppfloat64

func _mpfr_greater_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_inits2(*iqlibc.ppTLS, ppint64, ppuintptr, ppuintptr)

func _mpfr_integer_p(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_less_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_lgamma(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_log(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_mpz_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mpz_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_mul_2si(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64, ppint32) ppint32

func _mpfr_mul_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttoinf(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttozero(*iqlibc.ppTLS, ppuintptr)

func _mpfr_prec_round(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32) ppint32

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_random2(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppuintptr)

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppuint8

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_d(*iqlibc.ppTLS, ppuintptr, ppfloat64, ppint32) ppint32

func _mpfr_set_exp(*iqlibc.ppTLS, ppuintptr, ppint64) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_set_z(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_setmax(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_subnormalize(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_swap(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _putchar(*iqlibc.ppTLS, ppint32) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

var _stdout ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

var _tests_memory_limit ppuint64

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

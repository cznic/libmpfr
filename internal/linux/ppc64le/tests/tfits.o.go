// Code generated for linux/ppc64le by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IS_DOUBLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DNPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/ppc64le -UHAVE_NEARBYINT -mlong-double-64 -c -o tfits.o.go tfits.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IS_DOUBLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT16_MAX = 0x7fff
const mvINT32_MAX = 0x7fffffff
const mvINT64_MAX = 9223372036854775807
const mvINT8_MAX = 0x7f
const mvINTMAX_MAX = "INT64_MAX"
const mvINTMAX_MIN = "INT64_MIN"
const mvINTPTR_MAX = "INT64_MAX"
const mvINTPTR_MIN = "INT64_MIN"
const mvINT_FAST16_MAX = "INT32_MAX"
const mvINT_FAST16_MIN = "INT32_MIN"
const mvINT_FAST32_MAX = "INT32_MAX"
const mvINT_FAST32_MIN = "INT32_MIN"
const mvINT_FAST64_MAX = "INT64_MAX"
const mvINT_FAST64_MIN = "INT64_MIN"
const mvINT_FAST8_MAX = "INT8_MAX"
const mvINT_FAST8_MIN = "INT8_MIN"
const mvINT_LEAST16_MAX = "INT16_MAX"
const mvINT_LEAST16_MIN = "INT16_MIN"
const mvINT_LEAST32_MAX = "INT32_MAX"
const mvINT_LEAST32_MIN = "INT32_MIN"
const mvINT_LEAST64_MAX = "INT64_MAX"
const mvINT_LEAST64_MIN = "INT64_MIN"
const mvINT_LEAST8_MAX = "INT8_MAX"
const mvINT_LEAST8_MIN = "INT8_MIN"
const mvINT_MAX = 2147483647
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1158
const mvMPFR_AI_THRESHOLD3 = 20165
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 3
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 580
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 10480
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_FSPEC = "j"
const mvMPFR_INTMAX_MAX = "INTMAX_MAX"
const mvMPFR_INTMAX_MIN = "INTMAX_MIN"
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 9
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 22904
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 14
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "src/powerpc64/mparam.h"
const mvMPFR_UINTMAX_MAX = "UINTMAX_MAX"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNPRINTF_L = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_T = 1
const mvPRIX16 = "X"
const mvPRIX32 = "X"
const mvPRIX8 = "X"
const mvPRIXFAST16 = "X"
const mvPRIXFAST32 = "X"
const mvPRIXFAST8 = "X"
const mvPRIXLEAST16 = "X"
const mvPRIXLEAST32 = "X"
const mvPRIXLEAST8 = "X"
const mvPRId16 = "d"
const mvPRId32 = "d"
const mvPRId8 = "d"
const mvPRIdFAST16 = "d"
const mvPRIdFAST32 = "d"
const mvPRIdFAST8 = "d"
const mvPRIdLEAST16 = "d"
const mvPRIdLEAST32 = "d"
const mvPRIdLEAST8 = "d"
const mvPRIi16 = "i"
const mvPRIi32 = "i"
const mvPRIi8 = "i"
const mvPRIiFAST16 = "i"
const mvPRIiFAST32 = "i"
const mvPRIiFAST8 = "i"
const mvPRIiLEAST16 = "i"
const mvPRIiLEAST32 = "i"
const mvPRIiLEAST8 = "i"
const mvPRIo16 = "o"
const mvPRIo32 = "o"
const mvPRIo8 = "o"
const mvPRIoFAST16 = "o"
const mvPRIoFAST32 = "o"
const mvPRIoFAST8 = "o"
const mvPRIoLEAST16 = "o"
const mvPRIoLEAST32 = "o"
const mvPRIoLEAST8 = "o"
const mvPRIu16 = "u"
const mvPRIu32 = "u"
const mvPRIu8 = "u"
const mvPRIuFAST16 = "u"
const mvPRIuFAST32 = "u"
const mvPRIuFAST8 = "u"
const mvPRIuLEAST16 = "u"
const mvPRIuLEAST32 = "u"
const mvPRIuLEAST8 = "u"
const mvPRIx16 = "x"
const mvPRIx32 = "x"
const mvPRIx8 = "x"
const mvPRIxFAST16 = "x"
const mvPRIxFAST32 = "x"
const mvPRIxFAST8 = "x"
const mvPRIxLEAST16 = "x"
const mvPRIxLEAST32 = "x"
const mvPRIxLEAST8 = "x"
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvPTRDIFF_MAX = "INT64_MAX"
const mvPTRDIFF_MIN = "INT64_MIN"
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSCNd16 = "hd"
const mvSCNd32 = "d"
const mvSCNd8 = "hhd"
const mvSCNdFAST16 = "d"
const mvSCNdFAST32 = "d"
const mvSCNdFAST8 = "hhd"
const mvSCNdLEAST16 = "hd"
const mvSCNdLEAST32 = "d"
const mvSCNdLEAST8 = "hhd"
const mvSCNi16 = "hi"
const mvSCNi32 = "i"
const mvSCNi8 = "hhi"
const mvSCNiFAST16 = "i"
const mvSCNiFAST32 = "i"
const mvSCNiFAST8 = "hhi"
const mvSCNiLEAST16 = "hi"
const mvSCNiLEAST32 = "i"
const mvSCNiLEAST8 = "hhi"
const mvSCNo16 = "ho"
const mvSCNo32 = "o"
const mvSCNo8 = "hho"
const mvSCNoFAST16 = "o"
const mvSCNoFAST32 = "o"
const mvSCNoFAST8 = "hho"
const mvSCNoLEAST16 = "ho"
const mvSCNoLEAST32 = "o"
const mvSCNoLEAST8 = "hho"
const mvSCNu16 = "hu"
const mvSCNu32 = "u"
const mvSCNu8 = "hhu"
const mvSCNuFAST16 = "u"
const mvSCNuFAST32 = "u"
const mvSCNuFAST8 = "hhu"
const mvSCNuLEAST16 = "hu"
const mvSCNuLEAST32 = "u"
const mvSCNuLEAST8 = "hhu"
const mvSCNx16 = "hx"
const mvSCNx32 = "x"
const mvSCNx8 = "hhx"
const mvSCNxFAST16 = "x"
const mvSCNxFAST32 = "x"
const mvSCNxFAST8 = "hhx"
const mvSCNxLEAST16 = "hx"
const mvSCNxLEAST32 = "x"
const mvSCNxLEAST8 = "hhx"
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 32767
const mvSIG_ATOMIC_MAX = "INT32_MAX"
const mvSIG_ATOMIC_MIN = "INT32_MIN"
const mvSIZE_MAX = "UINT64_MAX"
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT16_MAX = 0xffff
const mvUINT32_MAX = "0xffffffffu"
const mvUINT64_MAX = 18446744073709551615
const mvUINT8_MAX = 0xff
const mvUINTMAX_MAX = "UINT64_MAX"
const mvUINTPTR_MAX = "UINT64_MAX"
const mvUINT_FAST16_MAX = "UINT32_MAX"
const mvUINT_FAST32_MAX = "UINT32_MAX"
const mvUINT_FAST64_MAX = "UINT64_MAX"
const mvUINT_FAST8_MAX = "UINT8_MAX"
const mvUINT_LEAST16_MAX = "UINT16_MAX"
const mvUINT_LEAST32_MAX = "UINT32_MAX"
const mvUINT_LEAST64_MAX = "UINT64_MAX"
const mvUINT_LEAST8_MAX = "UINT8_MAX"
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWINT_MAX = "UINT32_MAX"
const mvWINT_MIN = 0
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_ARCH_PPC = 1
const mv_ARCH_PPC64 = 1
const mv_ARCH_PPCGR = 1
const mv_ARCH_PPCSQ = 1
const mv_ARCH_PWR4 = 1
const mv_ARCH_PWR5 = 1
const mv_ARCH_PWR5X = 1
const mv_ARCH_PWR6 = 1
const mv_ARCH_PWR7 = 1
const mv_ARCH_PWR8 = 1
const mv_CALL_ELF = 2
const mv_CALL_LINUX = 1
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LITTLE_ENDIAN = 1
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_H_HAVE_INTMAX_T = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__ALTIVEC__ = 1
const mv__APPLE_ALTIVEC__ = 1
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BUILTIN_CPU_SUPPORTS__ = 1
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__CMODEL_MEDIUM__ = 1
const mv__CRYPTO__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DEC128_EPSILON__ = 1e-33
const mv__DEC128_MANT_DIG__ = 34
const mv__DEC128_MAX_EXP__ = 6145
const mv__DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const mv__DEC128_MIN__ = 1e-6143
const mv__DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const mv__DEC32_EPSILON__ = 1e-6
const mv__DEC32_MANT_DIG__ = 7
const mv__DEC32_MAX_EXP__ = 97
const mv__DEC32_MAX__ = 9.999999e96
const mv__DEC32_MIN__ = 1e-95
const mv__DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const mv__DEC64_EPSILON__ = 1e-15
const mv__DEC64_MANT_DIG__ = 16
const mv__DEC64_MAX_EXP__ = 385
const mv__DEC64_MAX__ = "9.999999999999999E384"
const mv__DEC64_MIN__ = 1e-383
const mv__DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const mv__DECIMAL_DIG__ = 17
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT128_TYPE__ = 1
const mv__FLOAT128__ = 1
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FP_FAST_FMAL = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "powerpc64le-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=/build/gmp-dldbp2/gmp-6.2.1+dfsg=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 1
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 1
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC__ = 10
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1014
const mv__HAVE_BSWAP__ = 1
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__HTM__ = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 17
const mv__LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__LDBL_DIG__ = 15
const mv__LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_MANT_DIG__ = 53
const mv__LDBL_MAX_10_EXP__ = 308
const mv__LDBL_MAX_EXP__ = 1024
const mv__LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__LITTLE_ENDIAN = 1234
const mv__LITTLE_ENDIAN__ = 1
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__POWER8_VECTOR__ = 1
const mv__PPC64__ = 1
const mv__PPC__ = 1
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PRI64 = "l"
const mv__PRIPTR = "l"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__QUAD_MEMORY_ATOMIC__ = 1
const mv__RECIPF__ = 1
const mv__RECIP_PRECISION__ = 1
const mv__RECIP__ = 1
const mv__RSQRTEF__ = 1
const mv__RSQRTE__ = 1
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__STRUCT_PARM_ALIGN__ = 16
const mv__TM_FENCE__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VEC_ELEMENT_REG_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__VEC__ = 10206
const mv__VERSION__ = "10.2.1 20210110"
const mv__VSX__ = 1
const mv__WCHAR_MAX__ = 0x7fffffff
const mv__WCHAR_TYPE__ = "int"
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__builtin_copysignq = "__builtin_copysignf128"
const mv__builtin_fabsq = "__builtin_fabsf128"
const mv__builtin_huge_valq = "__builtin_huge_valf128"
const mv__builtin_infq = "__builtin_inff128"
const mv__builtin_nanq = "__builtin_nanf128"
const mv__builtin_nansq = "__builtin_nansf128"
const mv__builtin_vsx_vperm = "__builtin_vec_perm"
const mv__builtin_vsx_xvmaddadp = "__builtin_vsx_xvmadddp"
const mv__builtin_vsx_xvmaddasp = "__builtin_vsx_xvmaddsp"
const mv__builtin_vsx_xvmaddmdp = "__builtin_vsx_xvmadddp"
const mv__builtin_vsx_xvmaddmsp = "__builtin_vsx_xvmaddsp"
const mv__builtin_vsx_xvmsubadp = "__builtin_vsx_xvmsubdp"
const mv__builtin_vsx_xvmsubasp = "__builtin_vsx_xvmsubsp"
const mv__builtin_vsx_xvmsubmdp = "__builtin_vsx_xvmsubdp"
const mv__builtin_vsx_xvmsubmsp = "__builtin_vsx_xvmsubsp"
const mv__builtin_vsx_xvnmaddadp = "__builtin_vsx_xvnmadddp"
const mv__builtin_vsx_xvnmaddasp = "__builtin_vsx_xvnmaddsp"
const mv__builtin_vsx_xvnmaddmdp = "__builtin_vsx_xvnmadddp"
const mv__builtin_vsx_xvnmaddmsp = "__builtin_vsx_xvnmaddsp"
const mv__builtin_vsx_xvnmsubadp = "__builtin_vsx_xvnmsubdp"
const mv__builtin_vsx_xvnmsubasp = "__builtin_vsx_xvnmsubsp"
const mv__builtin_vsx_xvnmsubmdp = "__builtin_vsx_xvnmsubdp"
const mv__builtin_vsx_xvnmsubmsp = "__builtin_vsx_xvnmsubsp"
const mv__builtin_vsx_xxland = "__builtin_vec_and"
const mv__builtin_vsx_xxlandc = "__builtin_vec_andc"
const mv__builtin_vsx_xxlnor = "__builtin_vec_nor"
const mv__builtin_vsx_xxlor = "__builtin_vec_or"
const mv__builtin_vsx_xxlxor = "__builtin_vec_xor"
const mv__builtin_vsx_xxsel = "__builtin_vec_sel"
const mv__float128 = "__ieee128"
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__powerpc64__ = 1
const mv__powerpc__ = 1
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_sj = "__gmpfr_mpfr_get_sj"
const mvmpfr_get_uj = "__gmpfr_mpfr_get_uj"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpfr_pow_sj = "__gmpfr_mpfr_pow_sj"
const mvmpfr_pow_uj = "__gmpfr_mpfr_pow_uj"
const mvmpfr_pown = "mpfr_pow_sj"
const mvmpfr_set_sj = "__gmpfr_set_sj"
const mvmpfr_set_sj_2exp = "__gmpfr_set_sj_2exp"
const mvmpfr_set_uj = "__gmpfr_set_uj"
const mvmpfr_set_uj_2exp = "__gmpfr_set_uj_2exp"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnuintptr_t = ppuint64

type tnintptr_t = ppint64

type tnint8_t = ppint8

type tnint16_t = ppint16

type tnint32_t = ppint32

type tnint64_t = ppint64

type tnintmax_t = ppint64

type tnuint8_t = ppuint8

type tnuint16_t = ppuint16

type tnuint32_t = ppuint32

type tnuint64_t = ppuint64

type tnuintmax_t = ppuint64

type tnint_fast8_t = ppint8

type tnint_fast64_t = ppint64

type tnint_least8_t = ppint8

type tnint_least16_t = ppint16

type tnint_least32_t = ppint32

type tnint_least64_t = ppint64

type tnuint_fast8_t = ppuint8

type tnuint_fast64_t = ppuint64

type tnuint_least8_t = ppuint8

type tnuint_least16_t = ppuint16

type tnuint_least32_t = ppuint32

type tnuint_least64_t = ppuint64

type tnint_fast16_t = ppint32

type tnint_fast32_t = ppint32

type tnuint_fast16_t = ppuint32

type tnuint_fast32_t = ppuint32

type tnimaxdiv_t = struct {
	fdquot tnintmax_t
	fdrem  tnintmax_t
}

type tnmpfr_uintmax_t = ppuint64

type tnmpfr_intmax_t = ppint64

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

/* V is a non-zero limit for the type (*_MIN for a signed type or *_MAX).
 * If V is positive, then test V, V + 1/4, V + 3/4 and V + 1.
 * If V is negative, then test V, V - 1/4, V - 3/4 and V - 1.
 */

func Xmain(cgtls *iqlibc.ppTLS, cgargc ppint32, cgargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aae, aae1, aae10, aae100, aae101, aae102, aae103, aae104, aae105, aae106, aae107, aae108, aae109, aae11, aae110, aae111, aae112, aae113, aae114, aae115, aae116, aae117, aae118, aae119, aae12, aae120, aae121, aae122, aae123, aae124, aae125, aae126, aae127, aae128, aae129, aae13, aae130, aae131, aae132, aae133, aae134, aae14, aae15, aae16, aae17, aae18, aae19, aae2, aae20, aae21, aae22, aae23, aae24, aae25, aae26, aae27, aae28, aae29, aae3, aae30, aae31, aae32, aae33, aae34, aae35, aae36, aae37, aae38, aae39, aae4, aae40, aae41, aae42, aae43, aae44, aae45, aae46, aae47, aae48, aae49, aae5, aae50, aae51, aae52, aae53, aae54, aae55, aae56, aae57, aae58, aae59, aae6, aae60, aae61, aae62, aae63, aae64, aae65, aae66, aae67, aae68, aae69, aae7, aae70, aae71, aae72, aae73, aae74, aae75, aae76, aae77, aae78, aae79, aae8, aae80, aae81, aae82, aae83, aae84, aae85, aae86, aae87, aae88, aae89, aae9, aae90, aae91, aae92, aae93, aae94, aae95, aae96, aae97, aae98, aae99, aaemax, aaemin tnmpfr_exp_t
	var aaex_flags, aaflags1, aaflags10, aaflags100, aaflags101, aaflags102, aaflags103, aaflags104, aaflags105, aaflags106, aaflags107, aaflags108, aaflags109, aaflags11, aaflags110, aaflags111, aaflags112, aaflags113, aaflags114, aaflags115, aaflags116, aaflags117, aaflags118, aaflags119, aaflags12, aaflags120, aaflags121, aaflags122, aaflags123, aaflags124, aaflags125, aaflags126, aaflags127, aaflags128, aaflags129, aaflags13, aaflags130, aaflags131, aaflags132, aaflags133, aaflags134, aaflags135, aaflags136, aaflags137, aaflags138, aaflags139, aaflags14, aaflags140, aaflags141, aaflags142, aaflags143, aaflags144, aaflags145, aaflags146, aaflags147, aaflags148, aaflags149, aaflags15, aaflags150, aaflags151, aaflags152, aaflags153, aaflags154, aaflags155, aaflags156, aaflags157, aaflags158, aaflags159, aaflags16, aaflags160, aaflags161, aaflags162, aaflags163, aaflags164, aaflags165, aaflags166, aaflags167, aaflags168, aaflags169, aaflags17, aaflags170, aaflags171, aaflags172, aaflags173, aaflags174, aaflags175, aaflags176, aaflags177, aaflags178, aaflags179, aaflags18, aaflags180, aaflags181, aaflags182, aaflags183, aaflags184, aaflags185, aaflags186, aaflags187, aaflags188, aaflags189, aaflags19, aaflags190, aaflags191, aaflags192, aaflags193, aaflags194, aaflags195, aaflags196, aaflags197, aaflags198, aaflags199, aaflags2, aaflags20, aaflags200, aaflags201, aaflags202, aaflags203, aaflags204, aaflags205, aaflags206, aaflags207, aaflags208, aaflags209, aaflags21, aaflags210, aaflags211, aaflags212, aaflags213, aaflags214, aaflags215, aaflags216, aaflags217, aaflags218, aaflags219, aaflags22, aaflags220, aaflags221, aaflags222, aaflags223, aaflags224, aaflags225, aaflags226, aaflags227, aaflags228, aaflags229, aaflags23, aaflags230, aaflags231, aaflags232, aaflags233, aaflags234, aaflags235, aaflags236, aaflags237, aaflags238, aaflags239, aaflags24, aaflags240, aaflags241, aaflags242, aaflags243, aaflags244, aaflags245, aaflags246, aaflags247, aaflags248, aaflags249, aaflags25, aaflags250, aaflags251, aaflags252, aaflags253, aaflags254, aaflags255, aaflags256, aaflags257, aaflags258, aaflags259, aaflags26, aaflags260, aaflags261, aaflags262, aaflags263, aaflags264, aaflags265, aaflags266, aaflags267, aaflags268, aaflags269, aaflags27, aaflags270, aaflags28, aaflags29, aaflags3, aaflags30, aaflags31, aaflags32, aaflags33, aaflags34, aaflags35, aaflags36, aaflags37, aaflags38, aaflags39, aaflags4, aaflags40, aaflags41, aaflags42, aaflags43, aaflags44, aaflags45, aaflags46, aaflags47, aaflags48, aaflags49, aaflags5, aaflags50, aaflags51, aaflags52, aaflags53, aaflags54, aaflags55, aaflags56, aaflags57, aaflags58, aaflags59, aaflags6, aaflags60, aaflags61, aaflags62, aaflags63, aaflags64, aaflags65, aaflags66, aaflags67, aaflags68, aaflags69, aaflags7, aaflags70, aaflags71, aaflags72, aaflags73, aaflags74, aaflags75, aaflags76, aaflags77, aaflags78, aaflags79, aaflags8, aaflags80, aaflags81, aaflags82, aaflags83, aaflags84, aaflags85, aaflags86, aaflags87, aaflags88, aaflags89, aaflags9, aaflags90, aaflags91, aaflags92, aaflags93, aaflags94, aaflags95, aaflags96, aaflags97, aaflags98, aaflags99 tnmpfr_flags_t
	var aafi, aai, aainv, aainv1, aar, ccv251 ppint32
	var ccv128, ccv129, ccv130, ccv131, ccv134, ccv135, ccv142, ccv143, ccv144, ccv145, ccv148, ccv149, ccv156, ccv157, ccv158, ccv159, ccv162, ccv163, ccv170, ccv171, ccv172, ccv173, ccv176, ccv177, ccv184, ccv185, ccv186, ccv187, ccv190, ccv191, ccv198, ccv199, ccv200, ccv201, ccv204, ccv205, ccv212, ccv213, ccv214, ccv215, ccv218, ccv219, ccv226, ccv227, ccv228, ccv229, ccv232, ccv233, ccv240, ccv241, ccv242, ccv243, ccv246, ccv247, ccv3, ccv305, ccv306, ccv307, ccv308, ccv311, ccv312, ccv319, ccv320, ccv321, ccv322, ccv325, ccv326, ccv333, ccv334, ccv335, ccv336, ccv339, ccv340 ppbool
	var pp_ /* flags at bp+64 */ [2]tnmpfr_flags_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aae, aae1, aae10, aae100, aae101, aae102, aae103, aae104, aae105, aae106, aae107, aae108, aae109, aae11, aae110, aae111, aae112, aae113, aae114, aae115, aae116, aae117, aae118, aae119, aae12, aae120, aae121, aae122, aae123, aae124, aae125, aae126, aae127, aae128, aae129, aae13, aae130, aae131, aae132, aae133, aae134, aae14, aae15, aae16, aae17, aae18, aae19, aae2, aae20, aae21, aae22, aae23, aae24, aae25, aae26, aae27, aae28, aae29, aae3, aae30, aae31, aae32, aae33, aae34, aae35, aae36, aae37, aae38, aae39, aae4, aae40, aae41, aae42, aae43, aae44, aae45, aae46, aae47, aae48, aae49, aae5, aae50, aae51, aae52, aae53, aae54, aae55, aae56, aae57, aae58, aae59, aae6, aae60, aae61, aae62, aae63, aae64, aae65, aae66, aae67, aae68, aae69, aae7, aae70, aae71, aae72, aae73, aae74, aae75, aae76, aae77, aae78, aae79, aae8, aae80, aae81, aae82, aae83, aae84, aae85, aae86, aae87, aae88, aae89, aae9, aae90, aae91, aae92, aae93, aae94, aae95, aae96, aae97, aae98, aae99, aaemax, aaemin, aaex_flags, aafi, aaflags1, aaflags10, aaflags100, aaflags101, aaflags102, aaflags103, aaflags104, aaflags105, aaflags106, aaflags107, aaflags108, aaflags109, aaflags11, aaflags110, aaflags111, aaflags112, aaflags113, aaflags114, aaflags115, aaflags116, aaflags117, aaflags118, aaflags119, aaflags12, aaflags120, aaflags121, aaflags122, aaflags123, aaflags124, aaflags125, aaflags126, aaflags127, aaflags128, aaflags129, aaflags13, aaflags130, aaflags131, aaflags132, aaflags133, aaflags134, aaflags135, aaflags136, aaflags137, aaflags138, aaflags139, aaflags14, aaflags140, aaflags141, aaflags142, aaflags143, aaflags144, aaflags145, aaflags146, aaflags147, aaflags148, aaflags149, aaflags15, aaflags150, aaflags151, aaflags152, aaflags153, aaflags154, aaflags155, aaflags156, aaflags157, aaflags158, aaflags159, aaflags16, aaflags160, aaflags161, aaflags162, aaflags163, aaflags164, aaflags165, aaflags166, aaflags167, aaflags168, aaflags169, aaflags17, aaflags170, aaflags171, aaflags172, aaflags173, aaflags174, aaflags175, aaflags176, aaflags177, aaflags178, aaflags179, aaflags18, aaflags180, aaflags181, aaflags182, aaflags183, aaflags184, aaflags185, aaflags186, aaflags187, aaflags188, aaflags189, aaflags19, aaflags190, aaflags191, aaflags192, aaflags193, aaflags194, aaflags195, aaflags196, aaflags197, aaflags198, aaflags199, aaflags2, aaflags20, aaflags200, aaflags201, aaflags202, aaflags203, aaflags204, aaflags205, aaflags206, aaflags207, aaflags208, aaflags209, aaflags21, aaflags210, aaflags211, aaflags212, aaflags213, aaflags214, aaflags215, aaflags216, aaflags217, aaflags218, aaflags219, aaflags22, aaflags220, aaflags221, aaflags222, aaflags223, aaflags224, aaflags225, aaflags226, aaflags227, aaflags228, aaflags229, aaflags23, aaflags230, aaflags231, aaflags232, aaflags233, aaflags234, aaflags235, aaflags236, aaflags237, aaflags238, aaflags239, aaflags24, aaflags240, aaflags241, aaflags242, aaflags243, aaflags244, aaflags245, aaflags246, aaflags247, aaflags248, aaflags249, aaflags25, aaflags250, aaflags251, aaflags252, aaflags253, aaflags254, aaflags255, aaflags256, aaflags257, aaflags258, aaflags259, aaflags26, aaflags260, aaflags261, aaflags262, aaflags263, aaflags264, aaflags265, aaflags266, aaflags267, aaflags268, aaflags269, aaflags27, aaflags270, aaflags28, aaflags29, aaflags3, aaflags30, aaflags31, aaflags32, aaflags33, aaflags34, aaflags35, aaflags36, aaflags37, aaflags38, aaflags39, aaflags4, aaflags40, aaflags41, aaflags42, aaflags43, aaflags44, aaflags45, aaflags46, aaflags47, aaflags48, aaflags49, aaflags5, aaflags50, aaflags51, aaflags52, aaflags53, aaflags54, aaflags55, aaflags56, aaflags57, aaflags58, aaflags59, aaflags6, aaflags60, aaflags61, aaflags62, aaflags63, aaflags64, aaflags65, aaflags66, aaflags67, aaflags68, aaflags69, aaflags7, aaflags70, aaflags71, aaflags72, aaflags73, aaflags74, aaflags75, aaflags76, aaflags77, aaflags78, aaflags79, aaflags8, aaflags80, aaflags81, aaflags82, aaflags83, aaflags84, aaflags85, aaflags86, aaflags87, aaflags88, aaflags89, aaflags9, aaflags90, aaflags91, aaflags92, aaflags93, aaflags94, aaflags95, aaflags96, aaflags97, aaflags98, aaflags99, aai, aainv, aainv1, aar, ccv128, ccv129, ccv130, ccv131, ccv134, ccv135, ccv142, ccv143, ccv144, ccv145, ccv148, ccv149, ccv156, ccv157, ccv158, ccv159, ccv162, ccv163, ccv170, ccv171, ccv172, ccv173, ccv176, ccv177, ccv184, ccv185, ccv186, ccv187, ccv190, ccv191, ccv198, ccv199, ccv200, ccv201, ccv204, ccv205, ccv212, ccv213, ccv214, ccv215, ccv218, ccv219, ccv226, ccv227, ccv228, ccv229, ccv232, ccv233, ccv240, ccv241, ccv242, ccv243, ccv246, ccv247, ccv251, ccv3, ccv305, ccv306, ccv307, ccv308, ccv311, ccv312, ccv319, ccv320, ccv321, ccv322, ccv325, ccv326, ccv333, ccv334, ccv335, ccv336, ccv339, ccv340
	*(*[2]tnmpfr_flags_t)(iqunsafe.ppPointer(cgbp + 64)) = [2]tnmpfr_flags_t{
		1: iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)),
	}

	Xtests_start_mpfr(cgtls)

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax

	Xmpfr_init2(cgtls, cgbp, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(8)*iqlibc.ppUint64FromInt32(mvCHAR_BIT)+iqlibc.ppUint64FromInt32(2)))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(8))

	aar = 0
	for {
		if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}
		aafi = 0
		for {
			if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(cgbp+64 == cgbp+64)), ppint64(1)) != 0; !ccv3 {
				Xmpfr_assert_fail(cgtls, "tfits.c\x00", ppint32(130), "(void *) &(flags) == (void *) &(flags)[0]\x00")
			}
			pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if !(ppint64(aafi) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(4))) {
				break
			}

			aaex_flags = (*(*[2]tnmpfr_flags_t)(iqunsafe.ppPointer(cgbp + 64)))[aafi]

			/* Check NaN */
			Xmpfr_set_nan(cgtls, cgbp)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags1 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags1)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_4
			}
			aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae)
			Xset_emax(cgtls, aae)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags2 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags2)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_5
		cg_5:
			;
			goto cg_4
		cg_4: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags3 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags3)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_6
			}
			aae1 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae1)
			Xset_emax(cgtls, aae1)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags4 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags4)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_7
		cg_7:
			;
			goto cg_6
		cg_6: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags5 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags5)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_8
			}
			aae2 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae2)
			Xset_emax(cgtls, aae2)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags6 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags6)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_9
		cg_9:
			;
			goto cg_8
		cg_8: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags7 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags7)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_10
			}
			aae3 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae3)
			Xset_emax(cgtls, aae3)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags8 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags8)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_11
		cg_11:
			;
			goto cg_10
		cg_10: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags9 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags9)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_12
			}
			aae4 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae4)
			Xset_emax(cgtls, aae4)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags10 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags10)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_13
		cg_13:
			;
			goto cg_12
		cg_12: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags11 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags11)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_14
			}
			aae5 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae5)
			Xset_emax(cgtls, aae5)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags12 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags12)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_15
		cg_15:
			;
			goto cg_14
		cg_14: /**/
			; //

			/* Check +Inf */
			Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags13 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags13)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_16
			}
			aae6 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae6)
			Xset_emax(cgtls, aae6)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags14 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags14)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_17
		cg_17:
			;
			goto cg_16
		cg_16: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags15 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags15)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_18
			}
			aae7 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae7)
			Xset_emax(cgtls, aae7)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags16 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags16)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_19
		cg_19:
			;
			goto cg_18
		cg_18: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags17 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags17)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_20
			}
			aae8 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae8)
			Xset_emax(cgtls, aae8)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags18 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags18)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_21
		cg_21:
			;
			goto cg_20
		cg_20: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags19 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags19)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_22
			}
			aae9 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae9)
			Xset_emax(cgtls, aae9)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags20 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags20)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_23
		cg_23:
			;
			goto cg_22
		cg_22: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags21 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags21)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_24
			}
			aae10 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae10)
			Xset_emax(cgtls, aae10)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags22 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags22)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_25
		cg_25:
			;
			goto cg_24
		cg_24: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags23 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags23)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_26
			}
			aae11 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae11)
			Xset_emax(cgtls, aae11)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags24 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags24)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_27
		cg_27:
			;
			goto cg_26
		cg_26: /**/
			; //

			/* Check -Inf */
			Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags25 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags25)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_28
			}
			aae12 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae12)
			Xset_emax(cgtls, aae12)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags26 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags26)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_29
		cg_29:
			;
			goto cg_28
		cg_28: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags27 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags27)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_30
			}
			aae13 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae13)
			Xset_emax(cgtls, aae13)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags28 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags28)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_31
		cg_31:
			;
			goto cg_30
		cg_30: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags29 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags29)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_32
			}
			aae14 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae14)
			Xset_emax(cgtls, aae14)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags30 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags30)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_33
		cg_33:
			;
			goto cg_32
		cg_32: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags31 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags31)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_34
			}
			aae15 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae15)
			Xset_emax(cgtls, aae15)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags32 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags32)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_35
		cg_35:
			;
			goto cg_34
		cg_34: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags33 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags33)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_36
			}
			aae16 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae16)
			Xset_emax(cgtls, aae16)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags34 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags34)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_37
		cg_37:
			;
			goto cg_36
		cg_36: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags35 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags35)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_38
			}
			aae17 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae17)
			Xset_emax(cgtls, aae17)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags36 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags36)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_39
		cg_39:
			;
			goto cg_38
		cg_38: /**/
			; //

			/* Check +0 */
			Xmpfr_set_zero(cgtls, cgbp, ppint32(1))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags37 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags37)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_40
			}
			aae18 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae18)
			Xset_emax(cgtls, aae18)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags38 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags38)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_41
		cg_41:
			;
			goto cg_40
		cg_40: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags39 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags39)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_42
			}
			aae19 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae19)
			Xset_emax(cgtls, aae19)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags40 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags40)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_43
		cg_43:
			;
			goto cg_42
		cg_42: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags41 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags41)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_44
			}
			aae20 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae20)
			Xset_emax(cgtls, aae20)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags42 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags42)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_45
		cg_45:
			;
			goto cg_44
		cg_44: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags43 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags43)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_46
			}
			aae21 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae21)
			Xset_emax(cgtls, aae21)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags44 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags44)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_47
		cg_47:
			;
			goto cg_46
		cg_46: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags45 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags45)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_48
			}
			aae22 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae22)
			Xset_emax(cgtls, aae22)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags46 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags46)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_49
		cg_49:
			;
			goto cg_48
		cg_48: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags47 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags47)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_50
			}
			aae23 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae23)
			Xset_emax(cgtls, aae23)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags48 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags48)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_51
		cg_51:
			;
			goto cg_50
		cg_50: /**/
			; //

			/* Check -0 */
			Xmpfr_set_zero(cgtls, cgbp, -ppint32(1))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags49 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags49)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_52
			}
			aae24 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae24)
			Xset_emax(cgtls, aae24)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags50 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags50)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_53
		cg_53:
			;
			goto cg_52
		cg_52: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags51 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags51)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_54
			}
			aae25 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae25)
			Xset_emax(cgtls, aae25)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags52 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags52)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_55
		cg_55:
			;
			goto cg_54
		cg_54: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags53 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags53)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_56
			}
			aae26 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae26)
			Xset_emax(cgtls, aae26)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags54 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags54)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_57
		cg_57:
			;
			goto cg_56
		cg_56: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags55 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags55)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_58
			}
			aae27 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae27)
			Xset_emax(cgtls, aae27)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags56 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags56)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_59
		cg_59:
			;
			goto cg_58
		cg_58: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags57 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags57)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_60
			}
			aae28 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae28)
			Xset_emax(cgtls, aae28)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags58 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags58)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_61
		cg_61:
			;
			goto cg_60
		cg_60: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags59 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags59)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_62
			}
			aae29 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae29)
			Xset_emax(cgtls, aae29)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags60 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags60)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_63
		cg_63:
			;
			goto cg_62
		cg_62: /**/
			; //

			/* Check small positive op */
			Xmpfr_set_str(cgtls, cgbp, "1@-1\x00", ppint32(10), ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags61 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags61)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_64
			}
			aae30 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae30)
			Xset_emax(cgtls, aae30)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags62 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags62)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_65
		cg_65:
			;
			goto cg_64
		cg_64: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags63 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags63)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_66
			}
			aae31 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae31)
			Xset_emax(cgtls, aae31)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags64 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags64)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_67
		cg_67:
			;
			goto cg_66
		cg_66: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags65 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags65)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_68
			}
			aae32 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae32)
			Xset_emax(cgtls, aae32)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags66 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags66)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_69
		cg_69:
			;
			goto cg_68
		cg_68: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags67 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags67)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_70
			}
			aae33 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae33)
			Xset_emax(cgtls, aae33)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags68 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags68)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_71
		cg_71:
			;
			goto cg_70
		cg_70: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags69 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags69)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_72
			}
			aae34 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae34)
			Xset_emax(cgtls, aae34)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags70 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags70)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_73
		cg_73:
			;
			goto cg_72
		cg_72: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags71 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags71)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_74
			}
			aae35 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae35)
			Xset_emax(cgtls, aae35)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags72 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags72)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_75
		cg_75:
			;
			goto cg_74
		cg_74: /**/
			; //

			/* Check 17 */
			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(17)), 0, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags73 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags73)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_76
			}
			aae36 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae36)
			Xset_emax(cgtls, aae36)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags74 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags74)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_77
		cg_77:
			;
			goto cg_76
		cg_76: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags75 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags75)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_78
			}
			aae37 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae37)
			Xset_emax(cgtls, aae37)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags76 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags76)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_79
		cg_79:
			;
			goto cg_78
		cg_78: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags77 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags77)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_80
			}
			aae38 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae38)
			Xset_emax(cgtls, aae38)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags78 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags78)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_81
		cg_81:
			;
			goto cg_80
		cg_80: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags79 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags79)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_82
			}
			aae39 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae39)
			Xset_emax(cgtls, aae39)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags80 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags80)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_83
		cg_83:
			;
			goto cg_82
		cg_82: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags81 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags81)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_84
			}
			aae40 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae40)
			Xset_emax(cgtls, aae40)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags82 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags82)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_85
		cg_85:
			;
			goto cg_84
		cg_84: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags83 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags83)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_86
			}
			aae41 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae41)
			Xset_emax(cgtls, aae41)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags84 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags84)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_87
		cg_87:
			;
			goto cg_86
		cg_86: /**/
			; //

			/* Check large values (no fit) */
			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromUint64(2)*iqlibc.ppUint64FromInt64(0x7fffffffffffffff)+iqlibc.ppUint64FromInt32(1), 0, ppint32(ecMPFR_RNDN))
			Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(1), ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags85 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags85)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_88
			}
			aae42 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae42)
			Xset_emax(cgtls, aae42)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags86 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags86)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_89
		cg_89:
			;
			goto cg_88
		cg_88: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags87 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags87)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_90
			}
			aae43 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae43)
			Xset_emax(cgtls, aae43)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags88 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags88)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_91
		cg_91:
			;
			goto cg_90
		cg_90: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags89 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags89)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_92
			}
			aae44 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae44)
			Xset_emax(cgtls, aae44)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags90 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags90)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_93
		cg_93:
			;
			goto cg_92
		cg_92: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags91 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags91)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_94
			}
			aae45 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae45)
			Xset_emax(cgtls, aae45)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags92 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags92)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_95
		cg_95:
			;
			goto cg_94
		cg_94: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags93 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags93)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_96
			}
			aae46 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae46)
			Xset_emax(cgtls, aae46)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags94 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags94)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_97
		cg_97:
			;
			goto cg_96
		cg_96: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags95 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags95)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_98
			}
			aae47 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae47)
			Xset_emax(cgtls, aae47)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags96 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags96)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_99
		cg_99:
			;
			goto cg_98
		cg_98: /**/
			; //

			Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(40), ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags97 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags97)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_100
			}
			aae48 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae48)
			Xset_emax(cgtls, aae48)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags98 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags98)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_101
		cg_101:
			;
			goto cg_100
		cg_100: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags99 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags99)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_102
			}
			aae49 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae49)
			Xset_emax(cgtls, aae49)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags100 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags100)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_103
		cg_103:
			;
			goto cg_102
		cg_102: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags101 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags101)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_104
			}
			aae50 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae50)
			Xset_emax(cgtls, aae50)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags102 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags102)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_105
		cg_105:
			;
			goto cg_104
		cg_104: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags103 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags103)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_106
			}
			aae51 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae51)
			Xset_emax(cgtls, aae51)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags104 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags104)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_107
		cg_107:
			;
			goto cg_106
		cg_106: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags105 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags105)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_108
			}
			aae52 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae52)
			Xset_emax(cgtls, aae52)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags106 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags106)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_109
		cg_109:
			;
			goto cg_108
		cg_108: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags107 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags107)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_110
			}
			aae53 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae53)
			Xset_emax(cgtls, aae53)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags108 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(9), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags108)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_111
		cg_111:
			;
			goto cg_110
		cg_110: /**/
			; //

			/* Check a non-integer number just below a power of two. */
			Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(255), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags109 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags109)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_112
			}
			aae54 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae54)
			Xset_emax(cgtls, aae54)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags110 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags110)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_113
		cg_113:
			;
			goto cg_112
		cg_112: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags111 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags111)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_114
			}
			aae55 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae55)
			Xset_emax(cgtls, aae55)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags112 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags112)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_115
		cg_115:
			;
			goto cg_114
		cg_114: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags113 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags113)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_116
			}
			aae56 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae56)
			Xset_emax(cgtls, aae56)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags114 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags114)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_117
		cg_117:
			;
			goto cg_116
		cg_116: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags115 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags115)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_118
			}
			aae57 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae57)
			Xset_emax(cgtls, aae57)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags116 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags116)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_119
		cg_119:
			;
			goto cg_118
		cg_118: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags117 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags117)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_120
			}
			aae58 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae58)
			Xset_emax(cgtls, aae58)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags118 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags118)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_121
		cg_121:
			;
			goto cg_120
		cg_120: /**/
			; //
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags119 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags119)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_122
			}
			aae59 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae59)
			Xset_emax(cgtls, aae59)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags120 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags120)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_123
		cg_123:
			;
			goto cg_122
		cg_122: /**/
			; //

			/* Check the limits of the types (except 0 for unsigned types) */
			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromUint64(2)*iqlibc.ppUint64FromInt64(0x7fffffffffffffff)+iqlibc.ppUint64FromInt32(1), 0, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(20), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags121 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(20), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags121)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_124
			}
			aae60 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae60)
			Xset_emax(cgtls, aae60)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(20), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags122 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(20), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags122)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_125
		cg_125:
			;
			goto cg_124
		cg_124: /**/
			; //
			Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(1), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv129 = aar == ppint32(ecMPFR_RNDN); !ccv129 {
				if ccv128 = aar == ppint32(ecMPFR_RNDZ); !ccv128 {
				}
			}
			if iqlibc.ppBoolInt32(ccv129 || (ccv128 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint64FromUint64(2)*iqlibc.ppUint64FromInt64(0x7fffffffffffffff)+iqlibc.ppUint64FromInt32(1) < iqlibc.ppUint64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags123 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags123)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_126
			}
			aae61 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae61)
			Xset_emax(cgtls, aae61)
			X__gmpfr_flags = aaex_flags
			if ccv131 = aar == ppint32(ecMPFR_RNDN); !ccv131 {
				if ccv130 = aar == ppint32(ecMPFR_RNDZ); !ccv130 {
				}
			}
			if iqlibc.ppBoolInt32(ccv131 || (ccv130 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint64FromUint64(2)*iqlibc.ppUint64FromInt64(0x7fffffffffffffff)+iqlibc.ppUint64FromInt32(1) < iqlibc.ppUint64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags124 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags124)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_127
		cg_127:
			;
			goto cg_126
		cg_126: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv134 = aar == ppint32(ecMPFR_RNDZ); !ccv134 {
			}
			if iqlibc.ppBoolInt32(ccv134 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint64FromUint64(2)*iqlibc.ppUint64FromInt64(0x7fffffffffffffff)+iqlibc.ppUint64FromInt32(1) < iqlibc.ppUint64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags125 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags125)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_132
			}
			aae62 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae62)
			Xset_emax(cgtls, aae62)
			X__gmpfr_flags = aaex_flags
			if ccv135 = aar == ppint32(ecMPFR_RNDZ); !ccv135 {
			}
			if iqlibc.ppBoolInt32(ccv135 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint64FromUint64(2)*iqlibc.ppUint64FromInt64(0x7fffffffffffffff)+iqlibc.ppUint64FromInt32(1) < iqlibc.ppUint64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags126 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags126)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_133
		cg_133:
			;
			goto cg_132
		cg_132: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags127 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags127)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_136
			}
			aae63 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae63)
			Xset_emax(cgtls, aae63)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags128 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags128)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_137
		cg_137:
			;
			goto cg_136
		cg_136: /**/
			; //

			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(0x7fffffffffffffff)), 0, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(30), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags129 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(30), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags129)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_138
			}
			aae64 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae64)
			Xset_emax(cgtls, aae64)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(30), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags130 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(30), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags130)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_139
		cg_139:
			;
			goto cg_138
		cg_138: /**/
			; //
			Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(1), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv143 = aar == ppint32(ecMPFR_RNDN); !ccv143 {
				if ccv142 = aar == ppint32(ecMPFR_RNDZ); !ccv142 {
				}
			}
			if iqlibc.ppBoolInt32(ccv143 || (ccv142 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt64FromInt64(0x7fffffffffffffff) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags131 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags131)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_140
			}
			aae65 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae65)
			Xset_emax(cgtls, aae65)
			X__gmpfr_flags = aaex_flags
			if ccv145 = aar == ppint32(ecMPFR_RNDN); !ccv145 {
				if ccv144 = aar == ppint32(ecMPFR_RNDZ); !ccv144 {
				}
			}
			if iqlibc.ppBoolInt32(ccv145 || (ccv144 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt64FromInt64(0x7fffffffffffffff) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags132 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags132)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_141
		cg_141:
			;
			goto cg_140
		cg_140: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv148 = aar == ppint32(ecMPFR_RNDZ); !ccv148 {
			}
			if iqlibc.ppBoolInt32(ccv148 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt64FromInt64(0x7fffffffffffffff) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags133 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags133)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_146
			}
			aae66 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae66)
			Xset_emax(cgtls, aae66)
			X__gmpfr_flags = aaex_flags
			if ccv149 = aar == ppint32(ecMPFR_RNDZ); !ccv149 {
			}
			if iqlibc.ppBoolInt32(ccv149 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt64FromInt64(0x7fffffffffffffff) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags134 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags134)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_147
		cg_147:
			;
			goto cg_146
		cg_146: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags135 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags135)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_150
			}
			aae67 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae67)
			Xset_emax(cgtls, aae67)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags136 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags136)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_151
		cg_151:
			;
			goto cg_150
		cg_150: /**/
			; //

			pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1), 0, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(35), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags137 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(35), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags137)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_152
			}
			aae68 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae68)
			Xset_emax(cgtls, aae68)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(35), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags138 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(35), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags138)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_153
		cg_153:
			;
			goto cg_152
		cg_152: /**/
			; //
			Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(-ppint32(1)), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv157 = aar == ppint32(ecMPFR_RNDN); !ccv157 {
				if ccv156 = aar == ppint32(ecMPFR_RNDZ); !ccv156 {
				}
			}
			if iqlibc.ppBoolInt32(ccv157 || (ccv156 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags139 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags139)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_154
			}
			aae69 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae69)
			Xset_emax(cgtls, aae69)
			X__gmpfr_flags = aaex_flags
			if ccv159 = aar == ppint32(ecMPFR_RNDN); !ccv159 {
				if ccv158 = aar == ppint32(ecMPFR_RNDZ); !ccv158 {
				}
			}
			if iqlibc.ppBoolInt32(ccv159 || (ccv158 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags140 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags140)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_155
		cg_155:
			;
			goto cg_154
		cg_154: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv162 = aar == ppint32(ecMPFR_RNDZ); !ccv162 {
			}
			if iqlibc.ppBoolInt32(ccv162 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags141 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags141)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_160
			}
			aae70 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae70)
			Xset_emax(cgtls, aae70)
			X__gmpfr_flags = aaex_flags
			if ccv163 = aar == ppint32(ecMPFR_RNDZ); !ccv163 {
			}
			if iqlibc.ppBoolInt32(ccv163 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags142 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags142)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_161
		cg_161:
			;
			goto cg_160
		cg_160: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags143 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags143)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_164
			}
			aae71 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae71)
			Xset_emax(cgtls, aae71)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags144 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags144)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_165
		cg_165:
			;
			goto cg_164
		cg_164: /**/
			; //

			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(iqlibc.ppUint32FromUint32(0xffffffff)), 0, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(40), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags145 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(40), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags145)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_166
			}
			aae72 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae72)
			Xset_emax(cgtls, aae72)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(40), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags146 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(40), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags146)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_167
		cg_167:
			;
			goto cg_166
		cg_166: /**/
			; //
			Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(1), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv171 = aar == ppint32(ecMPFR_RNDN); !ccv171 {
				if ccv170 = aar == ppint32(ecMPFR_RNDZ); !ccv170 {
				}
			}
			if iqlibc.ppBoolInt32(ccv171 || (ccv170 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint32FromUint32(0xffffffff) < iqlibc.ppUint32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags147 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags147)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_168
			}
			aae73 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae73)
			Xset_emax(cgtls, aae73)
			X__gmpfr_flags = aaex_flags
			if ccv173 = aar == ppint32(ecMPFR_RNDN); !ccv173 {
				if ccv172 = aar == ppint32(ecMPFR_RNDZ); !ccv172 {
				}
			}
			if iqlibc.ppBoolInt32(ccv173 || (ccv172 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint32FromUint32(0xffffffff) < iqlibc.ppUint32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags148 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags148)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_169
		cg_169:
			;
			goto cg_168
		cg_168: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv176 = aar == ppint32(ecMPFR_RNDZ); !ccv176 {
			}
			if iqlibc.ppBoolInt32(ccv176 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint32FromUint32(0xffffffff) < iqlibc.ppUint32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags149 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags149)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_174
			}
			aae74 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae74)
			Xset_emax(cgtls, aae74)
			X__gmpfr_flags = aaex_flags
			if ccv177 = aar == ppint32(ecMPFR_RNDZ); !ccv177 {
			}
			if iqlibc.ppBoolInt32(ccv177 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint32FromUint32(0xffffffff) < iqlibc.ppUint32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags150 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags150)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_175
		cg_175:
			;
			goto cg_174
		cg_174: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags151 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags151)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_178
			}
			aae75 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae75)
			Xset_emax(cgtls, aae75)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags152 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(40)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags152)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_179
		cg_179:
			;
			goto cg_178
		cg_178: /**/
			; //

			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt64(ppint64(iqlibc.ppInt32FromInt32(mvINT_MAX))), 0, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(50), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags153 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(50), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags153)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_180
			}
			aae76 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae76)
			Xset_emax(cgtls, aae76)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(50), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags154 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(50), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags154)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_181
		cg_181:
			;
			goto cg_180
		cg_180: /**/
			; //
			Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(1), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv185 = aar == ppint32(ecMPFR_RNDN); !ccv185 {
				if ccv184 = aar == ppint32(ecMPFR_RNDZ); !ccv184 {
				}
			}
			if iqlibc.ppBoolInt32(ccv185 || (ccv184 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvINT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags155 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags155)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_182
			}
			aae77 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae77)
			Xset_emax(cgtls, aae77)
			X__gmpfr_flags = aaex_flags
			if ccv187 = aar == ppint32(ecMPFR_RNDN); !ccv187 {
				if ccv186 = aar == ppint32(ecMPFR_RNDZ); !ccv186 {
				}
			}
			if iqlibc.ppBoolInt32(ccv187 || (ccv186 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvINT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags156 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags156)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_183
		cg_183:
			;
			goto cg_182
		cg_182: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv190 = aar == ppint32(ecMPFR_RNDZ); !ccv190 {
			}
			if iqlibc.ppBoolInt32(ccv190 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvINT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags157 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags157)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_188
			}
			aae78 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae78)
			Xset_emax(cgtls, aae78)
			X__gmpfr_flags = aaex_flags
			if ccv191 = aar == ppint32(ecMPFR_RNDZ); !ccv191 {
			}
			if iqlibc.ppBoolInt32(ccv191 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvINT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags158 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags158)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_189
		cg_189:
			;
			goto cg_188
		cg_188: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags159 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags159)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_192
			}
			aae79 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae79)
			Xset_emax(cgtls, aae79)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags160 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(50)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags160)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_193
		cg_193:
			;
			goto cg_192
		cg_192: /**/
			; //

			pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fffffff)), 0, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(55), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags161 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(55), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags161)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_194
			}
			aae80 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae80)
			Xset_emax(cgtls, aae80)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(55), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags162 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(55), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags162)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_195
		cg_195:
			;
			goto cg_194
		cg_194: /**/
			; //
			Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(-ppint32(1)), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv199 = aar == ppint32(ecMPFR_RNDN); !ccv199 {
				if ccv198 = aar == ppint32(ecMPFR_RNDZ); !ccv198 {
				}
			}
			if iqlibc.ppBoolInt32(ccv199 || (ccv198 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fffffff) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags163 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags163)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_196
			}
			aae81 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae81)
			Xset_emax(cgtls, aae81)
			X__gmpfr_flags = aaex_flags
			if ccv201 = aar == ppint32(ecMPFR_RNDN); !ccv201 {
				if ccv200 = aar == ppint32(ecMPFR_RNDZ); !ccv200 {
				}
			}
			if iqlibc.ppBoolInt32(ccv201 || (ccv200 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fffffff) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags164 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags164)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_197
		cg_197:
			;
			goto cg_196
		cg_196: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv204 = aar == ppint32(ecMPFR_RNDZ); !ccv204 {
			}
			if iqlibc.ppBoolInt32(ccv204 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fffffff) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags165 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags165)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_202
			}
			aae82 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae82)
			Xset_emax(cgtls, aae82)
			X__gmpfr_flags = aaex_flags
			if ccv205 = aar == ppint32(ecMPFR_RNDZ); !ccv205 {
			}
			if iqlibc.ppBoolInt32(ccv205 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fffffff) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags166 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags166)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_203
		cg_203:
			;
			goto cg_202
		cg_202: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags167 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags167)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_206
			}
			aae83 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae83)
			Xset_emax(cgtls, aae83)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags168 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(55)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags168)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_207
		cg_207:
			;
			goto cg_206
		cg_206: /**/
			; //

			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvUSHRT_MAX)), 0, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(60), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags169 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(60), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags169)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_208
			}
			aae84 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae84)
			Xset_emax(cgtls, aae84)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(60), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags170 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(60), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags170)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_209
		cg_209:
			;
			goto cg_208
		cg_208: /**/
			; //
			Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(1), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv213 = aar == ppint32(ecMPFR_RNDN); !ccv213 {
				if ccv212 = aar == ppint32(ecMPFR_RNDZ); !ccv212 {
				}
			}
			if iqlibc.ppBoolInt32(ccv213 || (ccv212 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvUSHRT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags171 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags171)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_210
			}
			aae85 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae85)
			Xset_emax(cgtls, aae85)
			X__gmpfr_flags = aaex_flags
			if ccv215 = aar == ppint32(ecMPFR_RNDN); !ccv215 {
				if ccv214 = aar == ppint32(ecMPFR_RNDZ); !ccv214 {
				}
			}
			if iqlibc.ppBoolInt32(ccv215 || (ccv214 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvUSHRT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags172 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags172)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_211
		cg_211:
			;
			goto cg_210
		cg_210: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv218 = aar == ppint32(ecMPFR_RNDZ); !ccv218 {
			}
			if iqlibc.ppBoolInt32(ccv218 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvUSHRT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags173 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags173)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_216
			}
			aae86 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae86)
			Xset_emax(cgtls, aae86)
			X__gmpfr_flags = aaex_flags
			if ccv219 = aar == ppint32(ecMPFR_RNDZ); !ccv219 {
			}
			if iqlibc.ppBoolInt32(ccv219 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvUSHRT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags174 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags174)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_217
		cg_217:
			;
			goto cg_216
		cg_216: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags175 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags175)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_220
			}
			aae87 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae87)
			Xset_emax(cgtls, aae87)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags176 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(60)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags176)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_221
		cg_221:
			;
			goto cg_220
		cg_220: /**/
			; //

			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt64(ppint64(iqlibc.ppInt32FromInt32(mvSHRT_MAX))), 0, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(70), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags177 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(70), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags177)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_222
			}
			aae88 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae88)
			Xset_emax(cgtls, aae88)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(70), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags178 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(70), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags178)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_223
		cg_223:
			;
			goto cg_222
		cg_222: /**/
			; //
			Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(1), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv227 = aar == ppint32(ecMPFR_RNDN); !ccv227 {
				if ccv226 = aar == ppint32(ecMPFR_RNDZ); !ccv226 {
				}
			}
			if iqlibc.ppBoolInt32(ccv227 || (ccv226 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvSHRT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags179 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags179)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_224
			}
			aae89 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae89)
			Xset_emax(cgtls, aae89)
			X__gmpfr_flags = aaex_flags
			if ccv229 = aar == ppint32(ecMPFR_RNDN); !ccv229 {
				if ccv228 = aar == ppint32(ecMPFR_RNDZ); !ccv228 {
				}
			}
			if iqlibc.ppBoolInt32(ccv229 || (ccv228 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvSHRT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags180 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags180)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_225
		cg_225:
			;
			goto cg_224
		cg_224: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv232 = aar == ppint32(ecMPFR_RNDZ); !ccv232 {
			}
			if iqlibc.ppBoolInt32(ccv232 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvSHRT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags181 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags181)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_230
			}
			aae90 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae90)
			Xset_emax(cgtls, aae90)
			X__gmpfr_flags = aaex_flags
			if ccv233 = aar == ppint32(ecMPFR_RNDZ); !ccv233 {
			}
			if iqlibc.ppBoolInt32(ccv233 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt32FromInt32(mvSHRT_MAX) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags182 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags182)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_231
		cg_231:
			;
			goto cg_230
		cg_230: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags183 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags183)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_234
			}
			aae91 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae91)
			Xset_emax(cgtls, aae91)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags184 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(70)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags184)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_235
		cg_235:
			;
			goto cg_234
		cg_234: /**/
			; //

			pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fff)), 0, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(75), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags185 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(75), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags185)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_236
			}
			aae92 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae92)
			Xset_emax(cgtls, aae92)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(75), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags186 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(75), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags186)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_237
		cg_237:
			;
			goto cg_236
		cg_236: /**/
			; //
			Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(-ppint32(1)), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv241 = aar == ppint32(ecMPFR_RNDN); !ccv241 {
				if ccv240 = aar == ppint32(ecMPFR_RNDZ); !ccv240 {
				}
			}
			if iqlibc.ppBoolInt32(ccv241 || (ccv240 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fff) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags187 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags187)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_238
			}
			aae93 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae93)
			Xset_emax(cgtls, aae93)
			X__gmpfr_flags = aaex_flags
			if ccv243 = aar == ppint32(ecMPFR_RNDN); !ccv243 {
				if ccv242 = aar == ppint32(ecMPFR_RNDZ); !ccv242 {
				}
			}
			if iqlibc.ppBoolInt32(ccv243 || (ccv242 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fff) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags188 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags188)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_239
		cg_239:
			;
			goto cg_238
		cg_238: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if ccv246 = aar == ppint32(ecMPFR_RNDZ); !ccv246 {
			}
			if iqlibc.ppBoolInt32(ccv246 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fff) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags189 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags189)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_244
			}
			aae94 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae94)
			Xset_emax(cgtls, aae94)
			X__gmpfr_flags = aaex_flags
			if ccv247 = aar == ppint32(ecMPFR_RNDZ); !ccv247 {
			}
			if iqlibc.ppBoolInt32(ccv247 || aar+iqlibc.ppBoolInt32(-iqlibc.ppInt32FromInt32(1)-iqlibc.ppInt32FromInt32(0x7fff) < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0)) != 0 {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags190 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags190)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_245
		cg_245:
			;
			goto cg_244
		cg_244: /**/
			; //
			Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags191 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags191)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_248
			}
			aae95 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae95)
			Xset_emax(cgtls, aae95)
			X__gmpfr_flags = aaex_flags
			if !!(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags192 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(75)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags192)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_249
		cg_249:
			;
			goto cg_248
		cg_248: /**/
			; //

			/* Check negative op */
			aai = ppint32(1)
			for {
				if !(aai <= ppint32(4)) {
					break
				}

				Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-aai), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
				/* for RNDF, it fits if it fits when rounding away from zero */
				if aar != ppint32(ecMPFR_RNDF) {
					ccv251 = aar
				} else {
					ccv251 = ppint32(ecMPFR_RNDA)
				}
				Xmpfr_rint(cgtls, cgbp+32, cgbp, ccv251)
				aainv = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp != -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))
				X__gmpfr_flags = aaex_flags
				if aainv^iqlibc.ppBoolInt32(!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0)) != 0 {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags193 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags193)
					Xexit(cgtls, ppint32(1))
				}
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
					goto cg_252
				}
				aae96 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				Xset_emin(cgtls, aae96)
				Xset_emax(cgtls, aae96)
				X__gmpfr_flags = aaex_flags
				if aainv^iqlibc.ppBoolInt32(!(Xmpfr_fits_ulong_p(cgtls, cgbp, aar) != 0)) != 0 {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags194 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_ulong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags194)
					Xexit(cgtls, ppint32(1))
				}
				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				goto cg_253
			cg_253:
				;
				goto cg_252
			cg_252: /**/
				; //

				X__gmpfr_flags = aaex_flags
				if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(81), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags195 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(81), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags195)
					Xexit(cgtls, ppint32(1))
				}
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
					goto cg_254
				}
				aae97 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				Xset_emin(cgtls, aae97)
				Xset_emax(cgtls, aae97)
				X__gmpfr_flags = aaex_flags
				if !(Xmpfr_fits_slong_p(cgtls, cgbp, aar) != 0) {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(81), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags196 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(81), "mpfr_fits_slong_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags196)
					Xexit(cgtls, ppint32(1))
				}
				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				goto cg_255
			cg_255:
				;
				goto cg_254
			cg_254: /**/
				; //

				X__gmpfr_flags = aaex_flags
				if aainv^iqlibc.ppBoolInt32(!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0)) != 0 {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(82), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags197 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(82), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags197)
					Xexit(cgtls, ppint32(1))
				}
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
					goto cg_256
				}
				aae98 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				Xset_emin(cgtls, aae98)
				Xset_emax(cgtls, aae98)
				X__gmpfr_flags = aaex_flags
				if aainv^iqlibc.ppBoolInt32(!(Xmpfr_fits_uint_p(cgtls, cgbp, aar) != 0)) != 0 {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(82), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags198 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(82), "mpfr_fits_uint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags198)
					Xexit(cgtls, ppint32(1))
				}
				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				goto cg_257
			cg_257:
				;
				goto cg_256
			cg_256: /**/
				; //

				X__gmpfr_flags = aaex_flags
				if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(83), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags199 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(83), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags199)
					Xexit(cgtls, ppint32(1))
				}
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
					goto cg_258
				}
				aae99 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				Xset_emin(cgtls, aae99)
				Xset_emax(cgtls, aae99)
				X__gmpfr_flags = aaex_flags
				if !(Xmpfr_fits_sint_p(cgtls, cgbp, aar) != 0) {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(83), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags200 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(83), "mpfr_fits_sint_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags200)
					Xexit(cgtls, ppint32(1))
				}
				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				goto cg_259
			cg_259:
				;
				goto cg_258
			cg_258: /**/
				; //

				X__gmpfr_flags = aaex_flags
				if aainv^iqlibc.ppBoolInt32(!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0)) != 0 {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(84), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags201 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(84), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags201)
					Xexit(cgtls, ppint32(1))
				}
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
					goto cg_260
				}
				aae100 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				Xset_emin(cgtls, aae100)
				Xset_emax(cgtls, aae100)
				X__gmpfr_flags = aaex_flags
				if aainv^iqlibc.ppBoolInt32(!(Xmpfr_fits_ushort_p(cgtls, cgbp, aar) != 0)) != 0 {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(84), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags202 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(84), "mpfr_fits_ushort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags202)
					Xexit(cgtls, ppint32(1))
				}
				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				goto cg_261
			cg_261:
				;
				goto cg_260
			cg_260: /**/
				; //

				X__gmpfr_flags = aaex_flags
				if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(85), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags203 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(85), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags203)
					Xexit(cgtls, ppint32(1))
				}
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
					goto cg_262
				}
				aae101 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				Xset_emin(cgtls, aae101)
				Xset_emax(cgtls, aae101)
				X__gmpfr_flags = aaex_flags
				if !(Xmpfr_fits_sshort_p(cgtls, cgbp, aar) != 0) {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(85), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags204 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(85), "mpfr_fits_sshort_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags204)
					Xexit(cgtls, ppint32(1))
				}
				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				goto cg_263
			cg_263:
				;
				goto cg_262
			cg_262: /**/
				; //

				goto cg_250
			cg_250:
				;
				aai++
			}

			goto cg_2
		cg_2:
			;
			aafi++
		}
		goto cg_1
	cg_1:
		;
		aar++
	}

	Xmpfr_set_prec(cgtls, cgbp, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(8)*iqlibc.ppUint64FromInt32(mvCHAR_BIT)+iqlibc.ppUint64FromInt32(2)))

	aar = 0
	for {
		if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}

		/* Check NaN */
		Xmpfr_set_nan(cgtls, cgbp)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags205 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags205)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_265
		}
		aae102 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae102)
		Xset_emax(cgtls, aae102)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags206 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags206)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_266
	cg_266:
		;
		goto cg_265
	cg_265: /**/
		; //
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags207 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags207)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_267
		}
		aae103 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae103)
		Xset_emax(cgtls, aae103)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags208 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags208)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_268
	cg_268:
		;
		goto cg_267
	cg_267: /**/
		; //

		/* Check +Inf */
		Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags209 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags209)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_269
		}
		aae104 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae104)
		Xset_emax(cgtls, aae104)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags210 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags210)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_270
	cg_270:
		;
		goto cg_269
	cg_269: /**/
		; //
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags211 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags211)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_271
		}
		aae105 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae105)
		Xset_emax(cgtls, aae105)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags212 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(2), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags212)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_272
	cg_272:
		;
		goto cg_271
	cg_271: /**/
		; //

		/* Check -Inf */
		Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags213 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags213)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_273
		}
		aae106 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae106)
		Xset_emax(cgtls, aae106)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags214 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags214)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_274
	cg_274:
		;
		goto cg_273
	cg_273: /**/
		; //
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags215 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags215)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_275
		}
		aae107 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae107)
		Xset_emax(cgtls, aae107)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags216 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags216)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_276
	cg_276:
		;
		goto cg_275
	cg_275: /**/
		; //

		/* Check +0 */
		Xmpfr_set_zero(cgtls, cgbp, ppint32(1))
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags217 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags217)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_277
		}
		aae108 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae108)
		Xset_emax(cgtls, aae108)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags218 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags218)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_278
	cg_278:
		;
		goto cg_277
	cg_277: /**/
		; //
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags219 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags219)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_279
		}
		aae109 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae109)
		Xset_emax(cgtls, aae109)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags220 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags220)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_280
	cg_280:
		;
		goto cg_279
	cg_279: /**/
		; //

		/* Check -0 */
		Xmpfr_set_zero(cgtls, cgbp, -ppint32(1))
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags221 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags221)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_281
		}
		aae110 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae110)
		Xset_emax(cgtls, aae110)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags222 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags222)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_282
	cg_282:
		;
		goto cg_281
	cg_281: /**/
		; //
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags223 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags223)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_283
		}
		aae111 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae111)
		Xset_emax(cgtls, aae111)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags224 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(5), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags224)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_284
	cg_284:
		;
		goto cg_283
	cg_283: /**/
		; //

		/* Check small positive op */
		Xmpfr_set_str(cgtls, cgbp, "1@-1\x00", ppint32(10), ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags225 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags225)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_285
		}
		aae112 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae112)
		Xset_emax(cgtls, aae112)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags226 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags226)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_286
	cg_286:
		;
		goto cg_285
	cg_285: /**/
		; //
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags227 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags227)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_287
		}
		aae113 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae113)
		Xset_emax(cgtls, aae113)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags228 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(6), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags228)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_288
	cg_288:
		;
		goto cg_287
	cg_287: /**/
		; //

		/* Check 17 */
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(17)), 0, ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags229 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags229)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_289
		}
		aae114 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae114)
		Xset_emax(cgtls, aae114)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags230 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags230)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_290
	cg_290:
		;
		goto cg_289
	cg_289: /**/
		; //
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags231 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags231)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_291
		}
		aae115 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae115)
		Xset_emax(cgtls, aae115)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags232 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(7), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags232)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_292
	cg_292:
		;
		goto cg_291
	cg_291: /**/
		; //

		/* Check hugest */
		Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(42), iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(8)*iqlibc.ppUint64FromInt32(32)), ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags233 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags233)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_293
		}
		aae116 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae116)
		Xset_emax(cgtls, aae116)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags234 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags234)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_294
	cg_294:
		;
		goto cg_293
	cg_293: /**/
		; //
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags235 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags235)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_295
		}
		aae117 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae117)
		Xset_emax(cgtls, aae117)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags236 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(8), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags236)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_296
	cg_296:
		;
		goto cg_295
	cg_295: /**/
		; //

		/* Check a non-integer number just below a power of two. */
		Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(255), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags237 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags237)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_297
		}
		aae118 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae118)
		Xset_emax(cgtls, aae118)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags238 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags238)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_298
	cg_298:
		;
		goto cg_297
	cg_297: /**/
		; //
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags239 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags239)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_299
		}
		aae119 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae119)
		Xset_emax(cgtls, aae119)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags240 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(10), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags240)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_300
	cg_300:
		;
		goto cg_299
	cg_299: /**/
		; //

		/* Check the limits of the types (except 0 for uintmax_t) */
		X__gmpfr_set_uj(cgtls, cgbp, iqlibc.ppUint64FromUint64(0xffffffffffffffff), ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(20), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags241 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(20), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags241)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_301
		}
		aae120 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae120)
		Xset_emax(cgtls, aae120)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(20), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags242 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(20), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags242)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_302
	cg_302:
		;
		goto cg_301
	cg_301: /**/
		; //
		Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(1), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if ccv306 = aar == ppint32(ecMPFR_RNDN); !ccv306 {
			if ccv305 = aar == ppint32(ecMPFR_RNDZ); !ccv305 {
			}
		}
		if iqlibc.ppBoolInt32(ccv306 || (ccv305 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint64FromUint64(0xffffffffffffffff) < iqlibc.ppUint64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags243 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags243)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_303
		}
		aae121 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae121)
		Xset_emax(cgtls, aae121)
		X__gmpfr_flags = aaex_flags
		if ccv308 = aar == ppint32(ecMPFR_RNDN); !ccv308 {
			if ccv307 = aar == ppint32(ecMPFR_RNDZ); !ccv307 {
			}
		}
		if iqlibc.ppBoolInt32(ccv308 || (ccv307 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint64FromUint64(0xffffffffffffffff) < iqlibc.ppUint64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags244 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags244)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_304
	cg_304:
		;
		goto cg_303
	cg_303: /**/
		; //
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if ccv311 = aar == ppint32(ecMPFR_RNDZ); !ccv311 {
		}
		if iqlibc.ppBoolInt32(ccv311 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint64FromUint64(0xffffffffffffffff) < iqlibc.ppUint64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags245 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags245)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_309
		}
		aae122 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae122)
		Xset_emax(cgtls, aae122)
		X__gmpfr_flags = aaex_flags
		if ccv312 = aar == ppint32(ecMPFR_RNDZ); !ccv312 {
		}
		if iqlibc.ppBoolInt32(ccv312 || aar+iqlibc.ppBoolInt32(iqlibc.ppUint64FromUint64(0xffffffffffffffff) < iqlibc.ppUint64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags246 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags246)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_310
	cg_310:
		;
		goto cg_309
	cg_309: /**/
		; //
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags247 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags247)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_313
		}
		aae123 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae123)
		Xset_emax(cgtls, aae123)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags248 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(20)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags248)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_314
	cg_314:
		;
		goto cg_313
	cg_313: /**/
		; //

		X__gmpfr_set_sj(cgtls, cgbp, iqlibc.ppInt64FromInt64(mvINT64_MAX), ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(30), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags249 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(30), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags249)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_315
		}
		aae124 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae124)
		Xset_emax(cgtls, aae124)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(30), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags250 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(30), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags250)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_316
	cg_316:
		;
		goto cg_315
	cg_315: /**/
		; //
		Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(1), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if ccv320 = aar == ppint32(ecMPFR_RNDN); !ccv320 {
			if ccv319 = aar == ppint32(ecMPFR_RNDZ); !ccv319 {
			}
		}
		if iqlibc.ppBoolInt32(ccv320 || (ccv319 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt64FromInt64(mvINT64_MAX) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags251 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags251)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_317
		}
		aae125 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae125)
		Xset_emax(cgtls, aae125)
		X__gmpfr_flags = aaex_flags
		if ccv322 = aar == ppint32(ecMPFR_RNDN); !ccv322 {
			if ccv321 = aar == ppint32(ecMPFR_RNDZ); !ccv321 {
			}
		}
		if iqlibc.ppBoolInt32(ccv322 || (ccv321 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt64FromInt64(mvINT64_MAX) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags252 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags252)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_318
	cg_318:
		;
		goto cg_317
	cg_317: /**/
		; //
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if ccv325 = aar == ppint32(ecMPFR_RNDZ); !ccv325 {
		}
		if iqlibc.ppBoolInt32(ccv325 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt64FromInt64(mvINT64_MAX) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags253 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags253)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_323
		}
		aae126 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae126)
		Xset_emax(cgtls, aae126)
		X__gmpfr_flags = aaex_flags
		if ccv326 = aar == ppint32(ecMPFR_RNDZ); !ccv326 {
		}
		if iqlibc.ppBoolInt32(ccv326 || aar+iqlibc.ppBoolInt32(iqlibc.ppInt64FromInt64(mvINT64_MAX) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags254 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags254)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_324
	cg_324:
		;
		goto cg_323
	cg_323: /**/
		; //
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags255 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags255)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_327
		}
		aae127 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae127)
		Xset_emax(cgtls, aae127)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags256 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(30)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags256)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_328
	cg_328:
		;
		goto cg_327
	cg_327: /**/
		; //

		X__gmpfr_set_sj(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1))-iqlibc.ppInt64FromInt64(0x7fffffffffffffff), ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(35), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags257 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(35), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags257)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_329
		}
		aae128 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae128)
		Xset_emax(cgtls, aae128)
		X__gmpfr_flags = aaex_flags
		if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(35), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags258 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(35), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags258)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_330
	cg_330:
		;
		goto cg_329
	cg_329: /**/
		; //
		Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(-ppint32(1)), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if ccv334 = aar == ppint32(ecMPFR_RNDN); !ccv334 {
			if ccv333 = aar == ppint32(ecMPFR_RNDZ); !ccv333 {
			}
		}
		if iqlibc.ppBoolInt32(ccv334 || (ccv333 || aar+iqlibc.ppBoolInt32(ppint64(-iqlibc.ppInt32FromInt32(1))-iqlibc.ppInt64FromInt64(0x7fffffffffffffff) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags259 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags259)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_331
		}
		aae129 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae129)
		Xset_emax(cgtls, aae129)
		X__gmpfr_flags = aaex_flags
		if ccv336 = aar == ppint32(ecMPFR_RNDN); !ccv336 {
			if ccv335 = aar == ppint32(ecMPFR_RNDZ); !ccv335 {
			}
		}
		if iqlibc.ppBoolInt32(ccv336 || (ccv335 || aar+iqlibc.ppBoolInt32(ppint64(-iqlibc.ppInt32FromInt32(1))-iqlibc.ppInt64FromInt64(0x7fffffffffffffff) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD)))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags260 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(1), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags260)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_332
	cg_332:
		;
		goto cg_331
	cg_331: /**/
		; //
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if ccv339 = aar == ppint32(ecMPFR_RNDZ); !ccv339 {
		}
		if iqlibc.ppBoolInt32(ccv339 || aar+iqlibc.ppBoolInt32(ppint64(-iqlibc.ppInt32FromInt32(1))-iqlibc.ppInt64FromInt64(0x7fffffffffffffff) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags261 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags261)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_337
		}
		aae130 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae130)
		Xset_emax(cgtls, aae130)
		X__gmpfr_flags = aaex_flags
		if ccv340 = aar == ppint32(ecMPFR_RNDZ); !ccv340 {
		}
		if iqlibc.ppBoolInt32(ccv340 || aar+iqlibc.ppBoolInt32(ppint64(-iqlibc.ppInt32FromInt32(1))-iqlibc.ppInt64FromInt64(0x7fffffffffffffff) < iqlibc.ppInt64FromInt32(0)) == ppint32(ecMPFR_RNDD))^iqlibc.ppBoolInt32(!!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0)) != 0 {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags262 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(3), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags262)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_338
	cg_338:
		;
		goto cg_337
	cg_337: /**/
		; //
		Xmpfr_add(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags263 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags263)
			Xexit(cgtls, ppint32(1))
		}
		if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
			goto cg_341
		}
		aae131 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
		Xset_emin(cgtls, aae131)
		Xset_emax(cgtls, aae131)
		X__gmpfr_flags = aaex_flags
		if !!(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
			Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xexit(cgtls, ppint32(1))
		}
		if X__gmpfr_flags != aaex_flags {
			aaflags264 = X__gmpfr_flags
			Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, iqlibc.ppInt32FromInt32(35)+iqlibc.ppInt32FromInt32(4), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "Expected flags:\x00", 0)
			Xflags_out(cgtls, aaex_flags)
			Xprintf(cgtls, "Got flags:     \x00", 0)
			Xflags_out(cgtls, aaflags264)
			Xexit(cgtls, ppint32(1))
		}
		Xset_emin(cgtls, aaemin)
		Xset_emax(cgtls, aaemax)
		goto cg_342
	cg_342:
		;
		goto cg_341
	cg_341: /**/
		; //

		/* Check negative op */
		aai = ppint32(1)
		for {
			if !(aai <= ppint32(4)) {
				break
			}

			Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-aai), ppint64(-ppint32(2)), ppint32(ecMPFR_RNDN))
			Xmpfr_rint(cgtls, cgbp+32, cgbp, aar)
			aainv1 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp != -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))
			if aar != ppint32(ecMPFR_RNDF) {
				X__gmpfr_flags = aaex_flags
				if aainv1^iqlibc.ppBoolInt32(!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0)) != 0 {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags265 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags265)
					Xexit(cgtls, ppint32(1))
				}
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
					goto cg_344
				}
				aae132 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				Xset_emin(cgtls, aae132)
				Xset_emax(cgtls, aae132)
				X__gmpfr_flags = aaex_flags
				if aainv1^iqlibc.ppBoolInt32(!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0)) != 0 {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags266 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags266)
					Xexit(cgtls, ppint32(1))
				}
				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				goto cg_345
			cg_345:
				;
				goto cg_344
			cg_344: /**/
				//
			} else {
				X__gmpfr_flags = aaex_flags
				if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags267 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags267)
					Xexit(cgtls, ppint32(1))
				}
				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
					goto cg_346
				}
				aae133 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				Xset_emin(cgtls, aae133)
				Xset_emax(cgtls, aae133)
				X__gmpfr_flags = aaex_flags
				if !!(Xmpfr_fits_uintmax_p(cgtls, cgbp, aar) != 0) {
					Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xexit(cgtls, ppint32(1))
				}
				if X__gmpfr_flags != aaex_flags {
					aaflags268 = X__gmpfr_flags
					Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(80), "mpfr_fits_uintmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "Expected flags:\x00", 0)
					Xflags_out(cgtls, aaex_flags)
					Xprintf(cgtls, "Got flags:     \x00", 0)
					Xflags_out(cgtls, aaflags268)
					Xexit(cgtls, ppint32(1))
				}
				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				goto cg_347
			cg_347:
				;
				goto cg_346
			cg_346: /**/
				//
			}
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(81), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags269 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(81), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags269)
				Xexit(cgtls, ppint32(1))
			}
			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {
				goto cg_348
			}
			aae134 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
			Xset_emin(cgtls, aae134)
			Xset_emax(cgtls, aae134)
			X__gmpfr_flags = aaex_flags
			if !(Xmpfr_fits_intmax_p(cgtls, cgbp, aar) != 0) {
				Xprintf(cgtls, "Error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(81), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aaex_flags {
				aaflags270 = X__gmpfr_flags
				Xprintf(cgtls, "Flags error %d for %s, rnd = %s and x = \x00", iqlibc.ppVaList(cgbp+80, ppint32(81), "mpfr_fits_intmax_p\x00", Xmpfr_print_rnd_mode(cgtls, aar)))
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "Expected flags:\x00", 0)
				Xflags_out(cgtls, aaex_flags)
				Xprintf(cgtls, "Got flags:     \x00", 0)
				Xflags_out(cgtls, aaflags270)
				Xexit(cgtls, ppint32(1))
			}
			Xset_emin(cgtls, aaemin)
			Xset_emax(cgtls, aaemax)
			goto cg_349
		cg_349:
			;
			goto cg_348
		cg_348: /**/
			; //

			goto cg_343
		cg_343:
			;
			aai++
		}

		goto cg_264
	cg_264:
		;
		aar++
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

var ___gmpfr_flags ppuint32

func ___gmpfr_set_sj(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32) ppint32

func ___gmpfr_set_uj(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint32) ppint32

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _mpfr_add(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_fits_intmax_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_fits_sint_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_fits_slong_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_fits_sshort_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_fits_uint_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_fits_uintmax_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_fits_ulong_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_fits_ushort_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_mul_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_rint(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_set_zero(*iqlibc.ppTLS, ppuintptr, ppint32)

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

// Code generated for linux/arm64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IEEE_QUAD_LITTLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/arm64 -UHAVE_NEARBYINT -c -o tdiv.o.go tdiv.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IEEE_QUAD_LITTLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvKMAX = 10000
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMAX_PREC = 128
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1311
const mvMPFR_AI_THRESHOLD3 = 19661
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 25
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 100
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 25000
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 20
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 30000
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 20
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "default"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_L = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTGENERIC_SO_TEST = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__AARCH64EL__ = 1
const mv__AARCH64_CMODEL_SMALL__ = 1
const mv__ARM_64BIT_STATE = 1
const mv__ARM_ALIGN_MAX_PWR = 28
const mv__ARM_ALIGN_MAX_STACK_PWR = 16
const mv__ARM_ARCH = 8
const mv__ARM_ARCH_8A = 1
const mv__ARM_ARCH_ISA_A64 = 1
const mv__ARM_ARCH_PROFILE = 65
const mv__ARM_FEATURE_CLZ = 1
const mv__ARM_FEATURE_FMA = 1
const mv__ARM_FEATURE_IDIV = 1
const mv__ARM_FEATURE_NUMERIC_MAXMIN = 1
const mv__ARM_FEATURE_UNALIGNED = 1
const mv__ARM_FP = 14
const mv__ARM_FP16_ARGS = 1
const mv__ARM_FP16_FORMAT_IEEE = 1
const mv__ARM_NEON = 1
const mv__ARM_PCS_AAPCS64 = 1
const mv__ARM_SIZEOF_MINIMAL_ENUM = 4
const mv__ARM_SIZEOF_WCHAR_T = 4
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 36
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 2
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_C99__ = 0
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 256
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "aarch64-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 36
const mv__LDBL_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__LDBL_DIG__ = 33
const mv__LDBL_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 113
const mv__LDBL_MAX_10_EXP__ = 4932
const mv__LDBL_MAX_EXP__ = 16384
const mv__LDBL_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LDBL_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__LDBL_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0xffffffff
const mv__WCHAR_MIN__ = 0
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__aarch64__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvtest_div = "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppuint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppuint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

func sicheck_equal(cgtls *iqlibc.ppTLS, aaa tnmpfr_srcptr, aaa2 tnmpfr_srcptr, aas ppuintptr, aab tnmpfr_srcptr, aac tnmpfr_srcptr, aar tnmpfr_rnd_t) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	if (*tn__mpfr_struct)(iqunsafe.ppPointer(aaa)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(aaa2)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, aaa, aaa2) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(aaa)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(aaa2)).fd_mpfr_sign {
		return
	}
	if aar == ppint32(ecMPFR_RNDF) { /* RNDF might return different values */
		return
	}
	Xprintf(cgtls, "Error in %s\n\x00", iqlibc.ppVaList(cgbp+8, Xmpfr_print_rnd_mode(cgtls, aar)))
	Xprintf(cgtls, "b  = \x00", 0)
	Xmpfr_dump(cgtls, aab)
	Xprintf(cgtls, "c  = \x00", 0)
	Xmpfr_dump(cgtls, aac)
	Xprintf(cgtls, "mpfr_div    result: \x00", 0)
	Xmpfr_dump(cgtls, aaa)
	Xprintf(cgtls, "%s result: \x00", iqlibc.ppVaList(cgbp+8, aas))
	Xmpfr_dump(cgtls, aaa2)
	Xexit(cgtls, ppint32(1))
}

func simpfr_all_div(cgtls *iqlibc.ppTLS, aaa tnmpfr_ptr, aab tnmpfr_srcptr, aac tnmpfr_srcptr, aar tnmpfr_rnd_t) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aainex, aainex2, ccv3, ccv6, ccv7 ppint32
	var aanewflags, aaoldflags ppuint32
	var ccv1, ccv2, ccv4, ccv5, ccv8, ccv9 ppbool
	var pp_ /* a2 at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aainex, aainex2, aanewflags, aaoldflags, ccv1, ccv2, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9

	aaoldflags = X__gmpfr_flags
	aainex = Xmpfr_div(cgtls, aaa, aab, aac, aar)

	/* this test makes no sense for RNDF, since it compares the ternary value
	   and the flags */
	if aaa == aab || aaa == aac || aar == ppint32(ecMPFR_RNDF) {
		return aainex
	}

	aanewflags = X__gmpfr_flags

	Xmpfr_init2(cgtls, cgbp, (*tn__mpfr_struct)(iqunsafe.ppPointer(aaa)).fd_mpfr_prec)

	if Xmpfr_integer_p(cgtls, aab) != 0 && !((*tn__mpfr_struct)(iqunsafe.ppPointer(aab)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(aab)).fd_mpfr_sign < 0) {

		/* b is an integer, but not -0 (-0 is rejected as
		   it becomes +0 when converted to an integer). */
		if Xmpfr_fits_ulong_p(cgtls, aab, ppint32(ecMPFR_RNDA)) != 0 {

			X__gmpfr_flags = aaoldflags
			aainex2 = Xmpfr_ui_div(cgtls, cgbp, Xmpfr_get_ui(cgtls, aab, ppint32(ecMPFR_RNDN)), aac, aar)
			if !(iqlibc.ppBoolInt32(aainex2 > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex2 < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex < iqlibc.ppInt32FromInt32(0))) {

				Xprintf(cgtls, "Error for ternary value (rnd=%s), mpfr_div %d, mpfr_ui_div %d\n\x00", iqlibc.ppVaList(cgbp+40, Xmpfr_print_rnd_mode(cgtls, aar), aainex, aainex2))
				Xexit(cgtls, ppint32(1))
			}
			if X__gmpfr_flags != aanewflags {

				Xprintf(cgtls, "Error for flags, mpfr_div %d, mpfr_ui_div %d\n\x00", iqlibc.ppVaList(cgbp+40, aanewflags, X__gmpfr_flags))
				Xexit(cgtls, ppint32(1))
			}
			sicheck_equal(cgtls, aaa, cgbp, "mpfr_ui_div\x00", aab, aac, aar)
		}
		if Xmpfr_fits_slong_p(cgtls, aab, ppint32(ecMPFR_RNDA)) != 0 {

			X__gmpfr_flags = aaoldflags
			aainex2 = Xmpfr_si_div(cgtls, cgbp, Xmpfr_get_si(cgtls, aab, ppint32(ecMPFR_RNDN)), aac, aar)

			if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppBoolInt32(aainex2 > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex2 < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex < iqlibc.ppInt32FromInt32(0)))), ppint64(1)) != 0; !ccv1 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(90), "((((inex2) > 0) - ((inex2) < 0)) == (((inex) > 0) - ((inex) < 0)))\x00")
			}
			pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

			if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == aanewflags)), ppint64(1)) != 0; !ccv2 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(91), "__gmpfr_flags == newflags\x00")
			}
			pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			sicheck_equal(cgtls, aaa, cgbp, "mpfr_si_div\x00", aab, aac, aar)
		}
	}

	if Xmpfr_integer_p(cgtls, aac) != 0 && !((*tn__mpfr_struct)(iqunsafe.ppPointer(aac)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) && (*tn__mpfr_struct)(iqunsafe.ppPointer(aac)).fd_mpfr_sign < 0) {

		/* c is an integer, but not -0 (-0 is rejected as
		   it becomes +0 when converted to an integer). */
		if Xmpfr_fits_ulong_p(cgtls, aac, ppint32(ecMPFR_RNDA)) != 0 {

			X__gmpfr_flags = aaoldflags
			if iqlibc.Bool(iqlibc.Bool(0 != 0) && Xmpfr_get_ui(cgtls, aac, ppint32(ecMPFR_RNDN)) >= ppuint64(1)) && Xmpfr_get_ui(cgtls, aac, ppint32(ecMPFR_RNDN))&(Xmpfr_get_ui(cgtls, aac, ppint32(ecMPFR_RNDN))-ppuint64(1)) == ppuint64(0) {
				ccv3 = Xmpfr_mul_2si(cgtls, cgbp, aab, ppint64(-X__builtin_ctzl(cgtls, Xmpfr_get_ui(cgtls, aac, ppint32(ecMPFR_RNDN)))), aar)
			} else {
				ccv3 = Xmpfr_div_ui(cgtls, cgbp, aab, Xmpfr_get_ui(cgtls, aac, ppint32(ecMPFR_RNDN)), aar)
			}
			aainex2 = ccv3

			if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppBoolInt32(aainex2 > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex2 < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex < iqlibc.ppInt32FromInt32(0)))), ppint64(1)) != 0; !ccv4 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(104), "((((inex2) > 0) - ((inex2) < 0)) == (((inex) > 0) - ((inex) < 0)))\x00")
			}
			pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

			if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == aanewflags)), ppint64(1)) != 0; !ccv5 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(105), "__gmpfr_flags == newflags\x00")
			}
			pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			sicheck_equal(cgtls, aaa, cgbp, "mpfr_div_ui\x00", aab, aac, aar)
		}
		if Xmpfr_fits_slong_p(cgtls, aac, ppint32(ecMPFR_RNDA)) != 0 {

			X__gmpfr_flags = aaoldflags
			if iqlibc.Bool(0 != 0) && Xmpfr_get_si(cgtls, aac, ppint32(ecMPFR_RNDN)) >= 0 {
				if iqlibc.Bool(iqlibc.Bool(0 != 0) && iqlibc.ppUint64FromInt64(Xmpfr_get_si(cgtls, aac, ppint32(ecMPFR_RNDN))) >= ppuint64(1)) && iqlibc.ppUint64FromInt64(Xmpfr_get_si(cgtls, aac, ppint32(ecMPFR_RNDN)))&(iqlibc.ppUint64FromInt64(Xmpfr_get_si(cgtls, aac, ppint32(ecMPFR_RNDN)))-ppuint64(1)) == ppuint64(0) {
					ccv7 = Xmpfr_mul_2si(cgtls, cgbp, aab, ppint64(-X__builtin_ctzl(cgtls, iqlibc.ppUint64FromInt64(Xmpfr_get_si(cgtls, aac, ppint32(ecMPFR_RNDN))))), aar)
				} else {
					ccv7 = Xmpfr_div_ui(cgtls, cgbp, aab, iqlibc.ppUint64FromInt64(Xmpfr_get_si(cgtls, aac, ppint32(ecMPFR_RNDN))), aar)
				}
				ccv6 = ccv7
			} else {
				ccv6 = Xmpfr_div_si(cgtls, cgbp, aab, Xmpfr_get_si(cgtls, aac, ppint32(ecMPFR_RNDN)), aar)
			}
			aainex2 = ccv6

			if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppBoolInt32(aainex2 > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex2 < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex < iqlibc.ppInt32FromInt32(0)))), ppint64(1)) != 0; !ccv8 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(112), "((((inex2) > 0) - ((inex2) < 0)) == (((inex) > 0) - ((inex) < 0)))\x00")
			}
			pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

			if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == aanewflags)), ppint64(1)) != 0; !ccv9 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(113), "__gmpfr_flags == newflags\x00")
			}
			pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			sicheck_equal(cgtls, aaa, cgbp, "mpfr_div_si\x00", aab, aac, aar)
		}
	}

	Xmpfr_clear(cgtls, cgbp)

	return aainex
}

// C documentation
//
//	/* return 0 iff a and b are of the same sign */
func siinex_cmp(cgtls *iqlibc.ppTLS, aaa ppint32, aab ppint32) (cgr ppint32) {

	var ccv1, ccv2, ccv3 ppint32
	pp_, pp_, pp_ = ccv1, ccv2, ccv3
	if aaa > 0 {
		if aab > 0 {
			ccv1 = 0
		} else {
			ccv1 = ppint32(1)
		}
		return ccv1
	} else {
		if aaa == 0 {
			if aab == 0 {
				ccv2 = 0
			} else {
				ccv2 = ppint32(1)
			}
			return ccv2
		} else {
			if aab < 0 {
				ccv3 = 0
			} else {
				ccv3 = ppint32(1)
			}
			return ccv3
		}
	}
	return cgr
}

func sicheck4(cgtls *iqlibc.ppTLS, aaNs ppuintptr, aaDs ppuintptr, aarnd_mode tnmpfr_rnd_t, aap ppint32, aaQs ppuintptr) {
	cgbp := cgtls.ppAlloc(144)
	defer cgtls.ppFree(144)

	var ccv1, ccv2 ppint32
	var pp_ /* d at bp+64 */ tnmpfr_t
	var pp_ /* n at bp+32 */ tnmpfr_t
	var pp_ /* q at bp+0 */ tnmpfr_t
	pp_, pp_ = ccv1, ccv2

	Xmpfr_inits2(cgtls, ppint64(aap), cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_str(cgtls, cgbp+32, aaNs, ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+64, aaDs, ppint32(10), ppint32(ecMPFR_RNDN))
	simpfr_all_div(cgtls, cgbp, cgbp+32, cgbp+64, aarnd_mode)
	if aap == ppint32(53) {
		ccv1 = ppint32(10)
	} else {
		ccv1 = ppint32(2)
	}
	if Xmpfr_cmp_str(cgtls, cgbp, aaQs, ccv1, ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "mpfr_div failed for n=%s, d=%s, p=%d, rnd_mode=%s\n\x00", iqlibc.ppVaList(cgbp+104, aaNs, aaDs, aap, Xmpfr_print_rnd_mode(cgtls, aarnd_mode)))
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		if aap == ppint32(53) {
			ccv2 = ppint32(10)
		} else {
			ccv2 = ppint32(2)
		}
		Xmpfr_set_str(cgtls, cgbp, aaQs, ccv2, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

func sicheck24(cgtls *iqlibc.ppTLS, aaNs ppuintptr, aaDs ppuintptr, aarnd_mode tnmpfr_rnd_t, aaQs ppuintptr) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var pp_ /* d at bp+64 */ tnmpfr_t
	var pp_ /* n at bp+32 */ tnmpfr_t
	var pp_ /* q at bp+0 */ tnmpfr_t

	Xmpfr_inits2(cgtls, ppint64(24), cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

	Xmpfr_set_str(cgtls, cgbp+32, aaNs, ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+64, aaDs, ppint32(10), ppint32(ecMPFR_RNDN))
	simpfr_all_div(cgtls, cgbp, cgbp+32, cgbp+64, aarnd_mode)
	if Xmpfr_cmp_str(cgtls, cgbp, aaQs, ppint32(10), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "mpfr_div failed for n=%s, d=%s, prec=24, rnd_mode=%s\n\x00", iqlibc.ppVaList(cgbp+104, aaNs, aaDs, Xmpfr_print_rnd_mode(cgtls, aarnd_mode)))
		Xprintf(cgtls, "expected quotient is %s, got \x00", iqlibc.ppVaList(cgbp+104, aaQs))
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xputchar(cgtls, ppint32('\n'))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

// C documentation
//
//	/* the following examples come from the paper "Number-theoretic Test
//	   Generation for Directed Rounding" from Michael Parks, Table 2 */
func sicheck_float(cgtls *iqlibc.ppTLS) {

	sicheck24(cgtls, "70368760954880.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDN), "8.388609e6\x00")
	sicheck24(cgtls, "140737479966720.0\x00", "16777213.0\x00", ppint32(ecMPFR_RNDN), "8.388609e6\x00")
	sicheck24(cgtls, "70368777732096.0\x00", "8388611.0\x00", ppint32(ecMPFR_RNDN), "8.388609e6\x00")
	sicheck24(cgtls, "105553133043712.0\x00", "12582911.0\x00", ppint32(ecMPFR_RNDN), "8.38861e6\x00")
	/* the exponent for the following example was forgotten in
	   the Arith'14 version of Parks' paper */
	sicheck24(cgtls, "12582913.0\x00", "12582910.0\x00", ppint32(ecMPFR_RNDN), "1.000000238\x00")
	sicheck24(cgtls, "105553124655104.0\x00", "12582910.0\x00", ppint32(ecMPFR_RNDN), "8388610.0\x00")
	sicheck24(cgtls, "140737479966720.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDN), "1.6777213e7\x00")
	sicheck24(cgtls, "70368777732096.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDN), "8.388611e6\x00")
	sicheck24(cgtls, "105553133043712.0\x00", "8388610.0\x00", ppint32(ecMPFR_RNDN), "1.2582911e7\x00")
	sicheck24(cgtls, "105553124655104.0\x00", "8388610.0\x00", ppint32(ecMPFR_RNDN), "1.258291e7\x00")

	sicheck24(cgtls, "70368760954880.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDZ), "8.388608e6\x00")
	sicheck24(cgtls, "140737479966720.0\x00", "16777213.0\x00", ppint32(ecMPFR_RNDZ), "8.388609e6\x00")
	sicheck24(cgtls, "70368777732096.0\x00", "8388611.0\x00", ppint32(ecMPFR_RNDZ), "8.388608e6\x00")
	sicheck24(cgtls, "105553133043712.0\x00", "12582911.0\x00", ppint32(ecMPFR_RNDZ), "8.38861e6\x00")
	sicheck24(cgtls, "12582913.0\x00", "12582910.0\x00", ppint32(ecMPFR_RNDZ), "1.000000238\x00")
	sicheck24(cgtls, "105553124655104.0\x00", "12582910.0\x00", ppint32(ecMPFR_RNDZ), "8388610.0\x00")
	sicheck24(cgtls, "140737479966720.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDZ), "1.6777213e7\x00")
	sicheck24(cgtls, "70368777732096.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDZ), "8.38861e6\x00")
	sicheck24(cgtls, "105553133043712.0\x00", "8388610.0\x00", ppint32(ecMPFR_RNDZ), "1.2582911e7\x00")
	sicheck24(cgtls, "105553124655104.0\x00", "8388610.0\x00", ppint32(ecMPFR_RNDZ), "1.258291e7\x00")

	sicheck24(cgtls, "70368760954880.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDU), "8.388609e6\x00")
	sicheck24(cgtls, "140737479966720.0\x00", "16777213.0\x00", ppint32(ecMPFR_RNDU), "8.38861e6\x00")
	sicheck24(cgtls, "70368777732096.0\x00", "8388611.0\x00", ppint32(ecMPFR_RNDU), "8.388609e6\x00")
	sicheck24(cgtls, "105553133043712.0\x00", "12582911.0\x00", ppint32(ecMPFR_RNDU), "8.388611e6\x00")
	sicheck24(cgtls, "12582913.0\x00", "12582910.0\x00", ppint32(ecMPFR_RNDU), "1.000000357\x00")
	sicheck24(cgtls, "105553124655104.0\x00", "12582910.0\x00", ppint32(ecMPFR_RNDU), "8388611.0\x00")
	sicheck24(cgtls, "140737479966720.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDU), "1.6777214e7\x00")
	sicheck24(cgtls, "70368777732096.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDU), "8.388611e6\x00")
	sicheck24(cgtls, "105553133043712.0\x00", "8388610.0\x00", ppint32(ecMPFR_RNDU), "1.2582912e7\x00")
	sicheck24(cgtls, "105553124655104.0\x00", "8388610.0\x00", ppint32(ecMPFR_RNDU), "1.2582911e7\x00")

	sicheck24(cgtls, "70368760954880.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDD), "8.388608e6\x00")
	sicheck24(cgtls, "140737479966720.0\x00", "16777213.0\x00", ppint32(ecMPFR_RNDD), "8.388609e6\x00")
	sicheck24(cgtls, "70368777732096.0\x00", "8388611.0\x00", ppint32(ecMPFR_RNDD), "8.388608e6\x00")
	sicheck24(cgtls, "105553133043712.0\x00", "12582911.0\x00", ppint32(ecMPFR_RNDD), "8.38861e6\x00")
	sicheck24(cgtls, "12582913.0\x00", "12582910.0\x00", ppint32(ecMPFR_RNDD), "1.000000238\x00")
	sicheck24(cgtls, "105553124655104.0\x00", "12582910.0\x00", ppint32(ecMPFR_RNDD), "8388610.0\x00")
	sicheck24(cgtls, "140737479966720.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDD), "1.6777213e7\x00")
	sicheck24(cgtls, "70368777732096.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDD), "8.38861e6\x00")
	sicheck24(cgtls, "105553133043712.0\x00", "8388610.0\x00", ppint32(ecMPFR_RNDD), "1.2582911e7\x00")
	sicheck24(cgtls, "105553124655104.0\x00", "8388610.0\x00", ppint32(ecMPFR_RNDD), "1.258291e7\x00")

	sicheck24(cgtls, "70368760954880.0\x00", "8388609.0\x00", ppint32(ecMPFR_RNDA), "8.388609e6\x00")
}

func sicheck_double(cgtls *iqlibc.ppTLS) {

	sicheck4(cgtls, "0.0\x00", "1.0\x00", ppint32(ecMPFR_RNDZ), ppint32(53), "0.0\x00")
	sicheck4(cgtls, "-7.4988969224688591e63\x00", "4.8816866450288732e306\x00", ppint32(ecMPFR_RNDD), ppint32(53), "-1.5361282826510687291e-243\x00")
	sicheck4(cgtls, "-1.33225773037748601769e+199\x00", "3.63449540676937123913e+79\x00", ppint32(ecMPFR_RNDZ), ppint32(53), "-3.6655920045905428978e119\x00")
	sicheck4(cgtls, "9.89438396044940256501e-134\x00", "5.93472984109987421717e-67\x00", ppint32(ecMPFR_RNDU), ppint32(53), "1.6672003992376663654e-67\x00")
	sicheck4(cgtls, "9.89438396044940256501e-134\x00", "5.93472984109987421717e-67\x00", ppint32(ecMPFR_RNDA), ppint32(53), "1.6672003992376663654e-67\x00")
	sicheck4(cgtls, "9.89438396044940256501e-134\x00", "-5.93472984109987421717e-67\x00", ppint32(ecMPFR_RNDU), ppint32(53), "-1.6672003992376663654e-67\x00")
	sicheck4(cgtls, "-4.53063926135729747564e-308\x00", "7.02293374921793516813e-84\x00", ppint32(ecMPFR_RNDD), ppint32(53), "-6.4512060388748850857e-225\x00")
	sicheck4(cgtls, "6.25089225176473806123e-01\x00", "-2.35527154824420243364e-230\x00", ppint32(ecMPFR_RNDD), ppint32(53), "-2.6540006635008291192e229\x00")
	sicheck4(cgtls, "6.25089225176473806123e-01\x00", "-2.35527154824420243364e-230\x00", ppint32(ecMPFR_RNDA), ppint32(53), "-2.6540006635008291192e229\x00")
	sicheck4(cgtls, "6.52308934689126e15\x00", "-1.62063546601505417497e273\x00", ppint32(ecMPFR_RNDN), ppint32(53), "-4.0250194961676020848e-258\x00")
	sicheck4(cgtls, "1.04636807108079349236e-189\x00", "3.72295730823253012954e-292\x00", ppint32(ecMPFR_RNDZ), ppint32(53), "2.810583051186143125e102\x00")
	/* problems found by Kevin under HP-PA */
	sicheck4(cgtls, "2.861044553323177e-136\x00", "-1.1120354257068143e+45\x00", ppint32(ecMPFR_RNDZ), ppint32(53), "-2.5727998292003016e-181\x00")
	sicheck4(cgtls, "-4.0559157245809205e-127\x00", "-1.1237723844524865e+77\x00", ppint32(ecMPFR_RNDN), ppint32(53), "3.6091968273068081e-204\x00")
	sicheck4(cgtls, "-1.8177943561493235e-93\x00", "-8.51233984260364e-104\x00", ppint32(ecMPFR_RNDU), ppint32(53), "2.1354814184595821e+10\x00")
}

func sicheck_64(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t

	Xmpfr_inits2(cgtls, ppint64(64), cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

	Xmpfr_set_str_binary(cgtls, cgbp, "1.00100100110110101001010010101111000001011100100101010000000000E54\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "1.00000000000000000000000000000000000000000000000000000000000000E584\x00")
	simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp_str(cgtls, cgbp+64, "0.1001001001101101010010100101011110000010111001001010100000000000E-529\x00", ppint32(2), ppint32(ecMPFR_RNDN)) != 0 {

		Xprintf(cgtls, "Error for tdiv for MPFR_RNDU and p=64\nx=\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "y=\x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xprintf(cgtls, "expected 0.1001001001101101010010100101011110000010111001001010100000000000E-529\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

func sicheck_convergence(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aai, aaj ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_ = aai, aaj

	Xmpfr_init2(cgtls, cgbp, ppint64(130))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1011111101011010101000001010011111101000011100011101010011111011000011001010000000111100100111110011001010110100100001001000111001E6944\x00")
	Xmpfr_init2(cgtls, cgbp+32, ppint64(130))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(5)), 0, ppint32(ecMPFR_RNDN))
	simpfr_all_div(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDD)) /* exact division */

	Xmpfr_set_prec(cgtls, cgbp, ppint64(64))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(64))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10010010011011010100101001010111100000101110010010101E55\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1E585\x00")
	simpfr_all_div(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10010010011011010100101001010111100000101110010010101E-529\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_div for prec=64, rnd=MPFR_RNDN\n\x00", 0)
		Xprintf(cgtls, "got        \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "instead of \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	aai = ppint32(32)
	for {
		if !(aai <= ppint32(64)) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, ppint64(aai))
		Xmpfr_set_prec(cgtls, cgbp+32, ppint64(aai))
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
		aaj = 0
		for {
			if !(aaj < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}

			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
			simpfr_all_div(cgtls, cgbp+32, cgbp, cgbp+32, aaj)
			if Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

				Xprintf(cgtls, "mpfr_div failed for x=1.0, y=1.0, prec=%d rnd=%s\n\x00", iqlibc.ppVaList(cgbp+72, aai, Xmpfr_print_rnd_mode(cgtls, aaj)))
				Xprintf(cgtls, "got \x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_2
		cg_2:
			;
			aaj++
		}

		goto cg_1
	cg_1:
		;
		aai += ppint32(32)
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
}

// C documentation
//
//	/* given y = o(x/u), x, u, find the inexact flag by
//	   multiplying y by u */
func siget_inexact(cgtls *iqlibc.ppTLS, aay tnmpfr_ptr, aax tnmpfr_ptr, aau tnmpfr_ptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aainex ppint32
	var pp_ /* xx at bp+0 */ tnmpfr_t
	pp_ = aainex
	Xmpfr_init2(cgtls, cgbp, (*tn__mpfr_struct)(iqunsafe.ppPointer(aay)).fd_mpfr_prec+(*tn__mpfr_struct)(iqunsafe.ppPointer(aau)).fd_mpfr_prec)
	Xmpfr_mul(cgtls, cgbp, aay, aau, ppint32(ecMPFR_RNDN)) /* exact */
	aainex = Xmpfr_cmp3(cgtls, cgbp, aax, ppint32(1))
	Xmpfr_clear(cgtls, cgbp)
	return aainex
}

func sicheck_hard(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aai, aainex, aainex2, aaj, aarnd ppint32
	var aaprecq, aaprecu, aaprecv tnmpfr_prec_t
	var pp_ /* q at bp+64 */ tnmpfr_t
	var pp_ /* q2 at bp+96 */ tnmpfr_t
	var pp_ /* u at bp+0 */ tnmpfr_t
	var pp_ /* v at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aai, aainex, aainex2, aaj, aaprecq, aaprecu, aaprecv, aarnd

	Xmpfr_init(cgtls, cgbp+64)
	Xmpfr_init(cgtls, cgbp+96)
	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)

	aaprecq = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aaprecq <= ppint64(64)) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp+64, aaprecq)
		Xmpfr_set_prec(cgtls, cgbp+96, aaprecq+ppint64(1))
		aaj = 0
		for {
			if !(aaj < ppint32(2)) {
				break
			}

			if aaj == 0 {

				for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp+96) == 0 {

					if !(Xmpfr_rands_initialized != 0) {
						Xmpfr_rands_initialized = ppuint8(1)
						X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
						pp_ = iqlibc.ppInt32FromInt32(0)
					}
					Xmpfr_urandomb(cgtls, cgbp+96, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				}
			} else { /* use q2=1 */
				pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+96, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
			}
			aaprecv = aaprecq
			for {
				if !(aaprecv <= ppint64(10)*aaprecq) {
					break
				}

				Xmpfr_set_prec(cgtls, cgbp+32, aaprecv)
				for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp+32) == 0 {

					if !(Xmpfr_rands_initialized != 0) {
						Xmpfr_rands_initialized = ppuint8(1)
						X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
						pp_ = iqlibc.ppInt32FromInt32(0)
					}
					Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				}
				aaprecu = aaprecq
				for {
					if !(aaprecu <= ppint64(10)*aaprecq) {
						break
					}

					Xmpfr_set_prec(cgtls, cgbp, aaprecu)
					Xmpfr_mul(cgtls, cgbp, cgbp+32, cgbp+96, ppint32(ecMPFR_RNDN))
					Xmpfr_nextbelow(cgtls, cgbp)
					aai = 0
					for {
						if !(aai <= ppint32(2)) {
							break
						}

						aarnd = 0
						for {
							if !(aarnd < ppint32(ecMPFR_RNDF)) {
								break
							}

							aainex = simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, aarnd)
							aainex2 = siget_inexact(cgtls, cgbp+64, cgbp, cgbp+32)
							if siinex_cmp(cgtls, aainex, aainex2) != 0 {

								Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+136, Xmpfr_print_rnd_mode(cgtls, aarnd), aainex2, aainex))
								Xprintf(cgtls, "u=  \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								Xprintf(cgtls, "v=  \x00", 0)
								Xmpfr_dump(cgtls, cgbp+32)
								Xprintf(cgtls, "q=  \x00", 0)
								Xmpfr_dump(cgtls, cgbp+64)
								Xmpfr_set_prec(cgtls, cgbp+96, aaprecq+aaprecv)
								Xmpfr_mul(cgtls, cgbp+96, cgbp+64, cgbp+32, ppint32(ecMPFR_RNDN))
								Xprintf(cgtls, "q*v=\x00", 0)
								Xmpfr_dump(cgtls, cgbp+96)
								Xexit(cgtls, ppint32(1))
							}

							goto cg_8
						cg_8:
							;
							aarnd++
						}
						Xmpfr_nextabove(cgtls, cgbp)

						goto cg_7
					cg_7:
						;
						aai++
					}

					goto cg_6
				cg_6:
					;
					aaprecu += aaprecq
				}

				goto cg_4
			cg_4:
				;
				aaprecv += aaprecq
			}

			goto cg_2
		cg_2:
			;
			aaj++
		}

		goto cg_1
	cg_1:
		;
		aaprecq++
	}

	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
}

func sicheck_lowr(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(224)
	defer cgtls.ppFree(224)

	var aa_p, aa_p1, aa_p2, aa_p3 tnmpfr_srcptr
	var aac, aac2, aak, ccv10, ccv11, ccv12, ccv13 ppint32
	var pp_ /* tmp at bp+160 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	var pp_ /* z2 at bp+96 */ tnmpfr_t
	var pp_ /* z3 at bp+128 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aa_p2, aa_p3, aac, aac2, aak, ccv10, ccv11, ccv12, ccv13

	Xmpfr_init2(cgtls, cgbp, ppint64(1000))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(100))
	Xmpfr_init2(cgtls, cgbp+160, ppint64(850))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(10))
	Xmpfr_init2(cgtls, cgbp+96, ppint64(10))
	Xmpfr_init2(cgtls, cgbp+128, ppint64(50))

	aak = ppint32(1)
	for {
		if !(aak < ppint32(mvKMAX)) {
			break
		}

		for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp+64) == 0 {

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		}
		for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp+160) == 0 {

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp+160, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		}
		Xmpfr_mul(cgtls, cgbp, cgbp+64, cgbp+160, ppint32(ecMPFR_RNDN)) /* exact */
		aac = simpfr_all_div(cgtls, cgbp+96, cgbp, cgbp+160, ppint32(ecMPFR_RNDN))

		if aac != 0 || Xmpfr_cmp3(cgtls, cgbp+96, cgbp+64, ppint32(1)) != 0 {

			Xprintf(cgtls, "Error in mpfr_div rnd=MPFR_RNDN\n\x00", 0)
			Xprintf(cgtls, "got        \x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xprintf(cgtls, "instead of \x00", 0)
			Xmpfr_dump(cgtls, cgbp+64)
			Xprintf(cgtls, "inex flag = %d, expected 0\n\x00", iqlibc.ppVaList(cgbp+200, aac))
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aak++
	}

	/* x has still precision 1000, z precision 10, and tmp prec 850 */
	Xmpfr_set_prec(cgtls, cgbp+96, ppint64(9))
	aak = ppint32(1)
	for {
		if !(aak < ppint32(mvKMAX)) {
			break
		}

		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp+160) == 0 {

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp+160, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		}
		Xmpfr_mul(cgtls, cgbp, cgbp+64, cgbp+160, ppint32(ecMPFR_RNDN)) /* exact */
		aac = simpfr_all_div(cgtls, cgbp+96, cgbp, cgbp+160, ppint32(ecMPFR_RNDN))
		/* since z2 has one less bit that z, either the division is exact
		   if z is representable on 9 bits, or we have an even round case */

		aac2 = siget_inexact(cgtls, cgbp+96, cgbp, cgbp+160)
		if Xmpfr_cmp3(cgtls, cgbp+96, cgbp+64, ppint32(1)) == 0 && aac != 0 || siinex_cmp(cgtls, aac, aac2) != 0 {

			Xprintf(cgtls, "Error in mpfr_div rnd=MPFR_RNDN\n\x00", 0)
			Xprintf(cgtls, "got        \x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xprintf(cgtls, "instead of \x00", 0)
			Xmpfr_dump(cgtls, cgbp+64)
			Xprintf(cgtls, "inex flag = %d, expected %d\n\x00", iqlibc.ppVaList(cgbp+200, aac, aac2))
			Xexit(cgtls, ppint32(1))
		} else {
			if aac == ppint32(2) {

				Xmpfr_nexttoinf(cgtls, cgbp+64)
				if Xmpfr_cmp3(cgtls, cgbp+96, cgbp+64, ppint32(1)) != 0 {

					Xprintf(cgtls, "Error in mpfr_div [even rnd?] rnd=MPFR_RNDN\n\x00", 0)
					Xprintf(cgtls, "Dividing \x00", 0)
					Xprintf(cgtls, "got        \x00", 0)
					Xmpfr_dump(cgtls, cgbp+96)
					Xprintf(cgtls, "instead of \x00", 0)
					Xmpfr_dump(cgtls, cgbp+64)
					Xprintf(cgtls, "inex flag = %d\n\x00", iqlibc.ppVaList(cgbp+200, ppint32(1)))
					Xexit(cgtls, ppint32(1))
				}
			} else {
				if aac == -ppint32(2) {

					Xmpfr_nexttozero(cgtls, cgbp+64)
					if Xmpfr_cmp3(cgtls, cgbp+96, cgbp+64, ppint32(1)) != 0 {

						Xprintf(cgtls, "Error in mpfr_div [even rnd?] rnd=MPFR_RNDN\n\x00", 0)
						Xprintf(cgtls, "Dividing \x00", 0)
						Xprintf(cgtls, "got        \x00", 0)
						Xmpfr_dump(cgtls, cgbp+96)
						Xprintf(cgtls, "instead of \x00", 0)
						Xmpfr_dump(cgtls, cgbp+64)
						Xprintf(cgtls, "inex flag = %d\n\x00", iqlibc.ppVaList(cgbp+200, ppint32(1)))
						Xexit(cgtls, ppint32(1))
					}
				}
			}
		}

		goto cg_4
	cg_4:
		;
		aak++
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(1000))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(100))
	Xmpfr_set_prec(cgtls, cgbp+160, ppint64(850))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(10))
	Xmpfr_set_prec(cgtls, cgbp+96, ppint64(10))

	/* almost exact divisions */
	aak = ppint32(1)
	for {
		if !(aak < ppint32(mvKMAX)) {
			break
		}

		for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp+64) == 0 {

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		}
		for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp+160) == 0 {

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp+160, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		}
		Xmpfr_mul(cgtls, cgbp, cgbp+64, cgbp+160, ppint32(ecMPFR_RNDN))
		{
			aa_p = cgbp + 160
			ccv10 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDD), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
		}
		pp_ = ccv10
		Xmpfr_nexttoinf(cgtls, cgbp)

		aac = simpfr_all_div(cgtls, cgbp+96, cgbp, cgbp+32, ppint32(ecMPFR_RNDD))
		simpfr_all_div(cgtls, cgbp+128, cgbp, cgbp+32, ppint32(ecMPFR_RNDD))
		{
			aa_p1 = cgbp + 128
			ccv11 = Xmpfr_set4(cgtls, cgbp+64, aa_p1, ppint32(ecMPFR_RNDD), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign)
		}
		pp_ = ccv11

		if aac != -ppint32(1) || Xmpfr_cmp3(cgtls, cgbp+96, cgbp+64, ppint32(1)) != 0 {

			Xprintf(cgtls, "Error in mpfr_div rnd=MPFR_RNDD\n\x00", 0)
			Xprintf(cgtls, "got        \x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xprintf(cgtls, "instead of \x00", 0)
			Xmpfr_dump(cgtls, cgbp+64)
			Xprintf(cgtls, "inex flag = %d\n\x00", iqlibc.ppVaList(cgbp+200, aac))
			Xexit(cgtls, ppint32(1))
		}

		{
			aa_p2 = cgbp + 160
			ccv12 = Xmpfr_set4(cgtls, cgbp+32, aa_p2, ppint32(ecMPFR_RNDU), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_sign)
		}
		pp_ = ccv12
		simpfr_all_div(cgtls, cgbp+128, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
		{
			aa_p3 = cgbp + 128
			ccv13 = Xmpfr_set4(cgtls, cgbp+64, aa_p3, ppint32(ecMPFR_RNDU), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_sign)
		}
		pp_ = ccv13
		aac = simpfr_all_div(cgtls, cgbp+96, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
		if aac != ppint32(1) || Xmpfr_cmp3(cgtls, cgbp+96, cgbp+64, ppint32(1)) != 0 {

			Xprintf(cgtls, "Error in mpfr_div rnd=MPFR_RNDU\n\x00", 0)
			Xprintf(cgtls, "u=\x00", 0)
			Xmpfr_dump(cgtls, cgbp)
			Xprintf(cgtls, "v=\x00", 0)
			Xmpfr_dump(cgtls, cgbp+32)
			Xprintf(cgtls, "got        \x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xprintf(cgtls, "instead of \x00", 0)
			Xmpfr_dump(cgtls, cgbp+64)
			Xprintf(cgtls, "inex flag = %d\n\x00", iqlibc.ppVaList(cgbp+200, aac))
			Xexit(cgtls, ppint32(1))
		}

		goto cg_7
	cg_7:
		;
		aak++
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
	Xmpfr_clear(cgtls, cgbp+128)
	Xmpfr_clear(cgtls, cgbp+160)
}

func sicheck_inexact(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aacmp, aainexact, ccv1 ppint32
	var aapu, aapx, aapy tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var pp_ /* u at bp+96 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aacmp, aainexact, aapu, aapx, aapy, aarnd, ccv1

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)
	Xmpfr_init(cgtls, cgbp+64)
	Xmpfr_init(cgtls, cgbp+96)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(28))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(28))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(1023))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1000001001101101111100010011E0\x00")
	Xmpfr_set_str(cgtls, cgbp+64, "48284762641021308813686974720835219181653367326353400027913400579340343320519877153813133510034402932651132854764198688352364361009429039801248971901380781746767119334993621199563870113045276395603170432175354501451429471578325545278975153148347684600400321033502982713296919861760382863826626093689036010394\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_div(cgtls, cgbp, cgbp, cgbp+64, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1111001011001101001001111100E-1023\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in mpfr_div for prec=28, RNDN\n\x00", 0)
		Xprintf(cgtls, "Expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "Got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11101100110010100011011000000100001111011111110010101E0\x00")
	Xmpfr_set_prec(cgtls, cgbp+96, ppint64(127))
	Xmpfr_set_str_binary(cgtls, cgbp+96, "0.1000001100110110110101110110101101111000110000001111111110000000011111001010110100110010111111111101000001011011101011101101000E-2\x00")
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(95))
	aainexact = simpfr_all_div(cgtls, cgbp+32, cgbp, cgbp+96, ppint32(ecMPFR_RNDN))
	ccv1 = siget_inexact(cgtls, cgbp+32, cgbp, cgbp+96)
	aacmp = ccv1
	if aainexact != ccv1 {

		Xprintf(cgtls, "Wrong inexact flag (0): expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+136, aacmp, aainexact))
		Xprintf(cgtls, "x=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(99), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xprintf(cgtls, "u=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(99), cgbp+96, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xprintf(cgtls, "y=\x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(10), ppuint64(99), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(33))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.101111100011011101010011101100001E0\x00")
	Xmpfr_set_prec(cgtls, cgbp+96, ppint64(2))
	Xmpfr_set_str_binary(cgtls, cgbp+96, "0.1E0\x00")
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(28))
	aainexact = simpfr_all_div(cgtls, cgbp+32, cgbp, cgbp+96, ppint32(ecMPFR_RNDN))
	if aainexact >= 0 {

		Xprintf(cgtls, "Wrong inexact flag (1): expected -1, got %d\n\x00", iqlibc.ppVaList(cgbp+136, aainexact))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(129))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.111110101111001100000101011100101100110011011101010001000110110101100101000010000001110110100001101010001010100010001111001101010E-2\x00")
	Xmpfr_set_prec(cgtls, cgbp+96, ppint64(15))
	Xmpfr_set_str_binary(cgtls, cgbp+96, "0.101101000001100E-1\x00")
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(92))
	aainexact = simpfr_all_div(cgtls, cgbp+32, cgbp, cgbp+96, ppint32(ecMPFR_RNDN))
	if aainexact <= 0 {

		Xprintf(cgtls, "Wrong inexact flag for rnd=MPFR_RNDN(1): expected 1, got %d\n\x00", iqlibc.ppVaList(cgbp+136, aainexact))
		Xmpfr_dump(cgtls, cgbp)
		Xmpfr_dump(cgtls, cgbp+96)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	aapx = ppint64(2)
	for {
		if !(aapx < ppint64(mvMAX_PREC)) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aapx)
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		aapu = ppint64(2)
		for {
			if !(aapu <= ppint64(mvMAX_PREC)) {
				break
			}

			Xmpfr_set_prec(cgtls, cgbp+96, aapu)
			for cgcond := true; cgcond; cgcond = Xmpfr_sgn(cgtls, cgbp+96) == 0 {
				if !(Xmpfr_rands_initialized != 0) {
					Xmpfr_rands_initialized = ppuint8(1)
					X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
					pp_ = iqlibc.ppInt32FromInt32(0)
				}
				Xmpfr_urandomb(cgtls, cgbp+96, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			}

			aapy = iqlibc.ppInt64FromUint64(ppuint64(mvMPFR_PREC_MIN) + Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvMAX_PREC)-iqlibc.ppInt32FromInt32(mvMPFR_PREC_MIN)))
			Xmpfr_set_prec(cgtls, cgbp+32, aapy)
			Xmpfr_set_prec(cgtls, cgbp+64, aapy+aapu)

			/* inexact is undefined for RNDF */
			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % ppuint64(ecMPFR_RNDF))
			aainexact = simpfr_all_div(cgtls, cgbp+32, cgbp, cgbp+96, aarnd)
			if Xmpfr_mul(cgtls, cgbp+64, cgbp+32, cgbp+96, aarnd) != 0 {

				Xprintf(cgtls, "z <- y * u should be exact\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
			aacmp = Xmpfr_cmp3(cgtls, cgbp+64, cgbp, ppint32(1))
			if aainexact == 0 && aacmp != 0 || aainexact > 0 && aacmp <= 0 || aainexact < 0 && aacmp >= 0 {

				Xprintf(cgtls, "Wrong inexact flag for rnd=%s\n\x00", iqlibc.ppVaList(cgbp+136, Xmpfr_print_rnd_mode(cgtls, aarnd)))
				Xprintf(cgtls, "expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+136, aacmp, aainexact))
				Xprintf(cgtls, "x=\x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "u=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+96)
				Xprintf(cgtls, "y=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xprintf(cgtls, "y*u=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+64)
				Xexit(cgtls, ppint32(1))
			}

			goto cg_4
		cg_4:
			;
			aapu++
		}

		goto cg_2
	cg_2:
		;
		aapx++
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
}

func sicheck_special(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aa_p, aa_p1, aa_p2, aa_p3, aa_p4, aa_p5, aa_p6, aa_p7, aa_p8, aa_p9 tnmpfr_ptr
	var aaemax, aaemin tnmpfr_exp_t
	var aai, aasign, ccv105, ccv118, ccv131, ccv144, ccv157, ccv170, ccv196, ccv200, ccv71, ccv72, ccv79, ccv92 ppint32
	var ccv10, ccv100, ccv101, ccv102, ccv103, ccv104, ccv107, ccv108, ccv109, ccv11, ccv110, ccv112, ccv113, ccv114, ccv115, ccv116, ccv117, ccv120, ccv121, ccv122, ccv123, ccv125, ccv126, ccv127, ccv128, ccv129, ccv13, ccv130, ccv133, ccv134, ccv135, ccv136, ccv138, ccv139, ccv14, ccv140, ccv141, ccv142, ccv143, ccv146, ccv147, ccv148, ccv149, ccv15, ccv151, ccv152, ccv153, ccv154, ccv155, ccv156, ccv159, ccv16, ccv160, ccv161, ccv162, ccv164, ccv165, ccv166, ccv167, ccv168, ccv169, ccv17, ccv172, ccv173, ccv174, ccv175, ccv177, ccv178, ccv179, ccv180, ccv181, ccv182, ccv183, ccv184, ccv185, ccv187, ccv188, ccv189, ccv19, ccv190, ccv191, ccv192, ccv195, ccv197, ccv198, ccv199, ccv2, ccv20, ccv201, ccv202, ccv21, ccv22, ccv24, ccv25, ccv26, ccv27, ccv28, ccv3, ccv30, ccv31, ccv32, ccv33, ccv35, ccv36, ccv37, ccv38, ccv39, ccv41, ccv42, ccv43, ccv44, ccv46, ccv47, ccv48, ccv49, ccv5, ccv50, ccv52, ccv53, ccv54, ccv55, ccv57, ccv58, ccv59, ccv6, ccv60, ccv62, ccv63, ccv64, ccv65, ccv67, ccv68, ccv69, ccv70, ccv74, ccv75, ccv77, ccv78, ccv8, ccv81, ccv82, ccv83, ccv84, ccv86, ccv87, ccv88, ccv89, ccv9, ccv90, ccv91, ccv94, ccv95, ccv96, ccv97, ccv99 ppbool
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* d at bp+32 */ tnmpfr_t
	var pp_ /* q at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aa_p2, aa_p3, aa_p4, aa_p5, aa_p6, aa_p7, aa_p8, aa_p9, aaemax, aaemin, aai, aasign, ccv10, ccv100, ccv101, ccv102, ccv103, ccv104, ccv105, ccv107, ccv108, ccv109, ccv11, ccv110, ccv112, ccv113, ccv114, ccv115, ccv116, ccv117, ccv118, ccv120, ccv121, ccv122, ccv123, ccv125, ccv126, ccv127, ccv128, ccv129, ccv13, ccv130, ccv131, ccv133, ccv134, ccv135, ccv136, ccv138, ccv139, ccv14, ccv140, ccv141, ccv142, ccv143, ccv144, ccv146, ccv147, ccv148, ccv149, ccv15, ccv151, ccv152, ccv153, ccv154, ccv155, ccv156, ccv157, ccv159, ccv16, ccv160, ccv161, ccv162, ccv164, ccv165, ccv166, ccv167, ccv168, ccv169, ccv17, ccv170, ccv172, ccv173, ccv174, ccv175, ccv177, ccv178, ccv179, ccv180, ccv181, ccv182, ccv183, ccv184, ccv185, ccv187, ccv188, ccv189, ccv19, ccv190, ccv191, ccv192, ccv195, ccv196, ccv197, ccv198, ccv199, ccv2, ccv20, ccv200, ccv201, ccv202, ccv21, ccv22, ccv24, ccv25, ccv26, ccv27, ccv28, ccv3, ccv30, ccv31, ccv32, ccv33, ccv35, ccv36, ccv37, ccv38, ccv39, ccv41, ccv42, ccv43, ccv44, ccv46, ccv47, ccv48, ccv49, ccv5, ccv50, ccv52, ccv53, ccv54, ccv55, ccv57, ccv58, ccv59, ccv6, ccv60, ccv62, ccv63, ccv64, ccv65, ccv67, ccv68, ccv69, ccv70, ccv71, ccv72, ccv74, ccv75, ccv77, ccv78, ccv79, ccv8, ccv81, ccv82, ccv83, ccv84, ccv86, ccv87, ccv88, ccv89, ccv9, ccv90, ccv91, ccv92, ccv94, ccv95, ccv96, ccv97, ccv99

	Xmpfr_init2(cgtls, cgbp, ppint64(100))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(100))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(100))

	/* 1/nan == nan */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)), 0, ppint32(ecMPFR_RNDN))
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(2)
	Xmpfr_clear_flags(cgtls)

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(743), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(744), "__gmpfr_flags == 4\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* nan/1 == nan */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(2)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(750), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN))), ppint64(1)) != 0; !ccv6 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(751), "__gmpfr_flags == 4\x00")
	}
	pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* +inf/1 == +inf */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)

	if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv8 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(758), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv9 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(759), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv10 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(760), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
	}
	pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv11 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(760), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv16 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv16 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(760), "(((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 760, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 760, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) > 0\x00")
		if ccv15 = iqlibc.Bool(!(0 != 0)); !ccv15 {
			if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv13 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(760), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
			}
			pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv14 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(760), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
			}
			pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv15 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv16 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv17 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv17 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(761), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv17 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* +inf/-1 == -inf */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)

	if ccv19 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv19 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(768), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv19 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv20 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv20 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(769), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv20 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv21 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv21 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(770), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
	}
	pp_ = ccv21 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv22 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv22 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(770), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv22 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv27 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv27 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(770), "(((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 770, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 770, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) < 0\x00")
		if ccv26 = iqlibc.Bool(!(0 != 0)); !ccv26 {
			if ccv24 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv24 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(770), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
			}
			pp_ = ccv24 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if ccv25 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv25 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(770), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
			}
			pp_ = ccv25 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv26 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv27 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv28 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv28 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(771), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv28 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* -inf/1 == -inf */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = -iqlibc.ppInt32FromInt32(1)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)

	if ccv30 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv30 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(778), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv30 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv31 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv31 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(779), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv31 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv32 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv32 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(780), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
	}
	pp_ = ccv32 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv33 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv33 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(780), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv33 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv38 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv38 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(780), "(((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 780, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 780, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) < 0\x00")
		if ccv37 = iqlibc.Bool(!(0 != 0)); !ccv37 {
			if ccv35 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv35 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(780), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
			}
			pp_ = ccv35 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if ccv36 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv36 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(780), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
			}
			pp_ = ccv36 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv37 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv38 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv39 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv39 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(781), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv39 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* -inf/-1 == +inf */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = -iqlibc.ppInt32FromInt32(1)
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)

	if ccv41 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv41 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(788), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv41 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv42 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)))), ppint64(1)) != 0; !ccv42 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(789), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1))))\x00")
	}
	pp_ = ccv42 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv43 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv43 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(790), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
	}
	pp_ = ccv43 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv44 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv44 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(790), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv44 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ccv49 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv49 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(790), "(((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 790, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 790, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) > 0\x00")
		if ccv48 = iqlibc.Bool(!(0 != 0)); !ccv48 {
			if ccv46 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv46 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(790), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
			}
			pp_ = ccv46 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if ccv47 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv47 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(790), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
			}
			pp_ = ccv47 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
		if !(ccv48 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv49 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv50 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv50 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(791), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv50 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* 1/+inf == +0 */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)), 0, ppint32(ecMPFR_RNDN))
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	Xmpfr_clear_flags(cgtls)

	if ccv52 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv52 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(798), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv52 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv53 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv53 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(799), "(((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv53 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv54 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv54 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(800), "(((q)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv54 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv55 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv55 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(801), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv55 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* 1/-inf == -0 */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromInt64(1)), 0, ppint32(ecMPFR_RNDN))
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_sign = -iqlibc.ppInt32FromInt32(1)
	Xmpfr_clear_flags(cgtls)

	if ccv57 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv57 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(808), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv57 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv58 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv58 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(809), "(((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv58 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv59 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv59 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(810), "(((q)->_mpfr_sign) < 0)\x00")
	}
	pp_ = ccv59 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv60 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv60 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(811), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv60 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* -1/+inf == -0 */
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	Xmpfr_clear_flags(cgtls)

	if ccv62 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv62 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(818), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv62 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv63 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv63 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(819), "(((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv63 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv64 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv64 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(820), "(((q)->_mpfr_sign) < 0)\x00")
	}
	pp_ = ccv64 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv65 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv65 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(821), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv65 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* -1/-inf == +0 */
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_sign = -iqlibc.ppInt32FromInt32(1)
	Xmpfr_clear_flags(cgtls)

	if ccv67 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv67 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(828), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv67 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv68 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv68 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(829), "(((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
	}
	pp_ = ccv68 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv69 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv69 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(830), "(((q)->_mpfr_sign) > 0)\x00")
	}
	pp_ = ccv69 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv70 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv70 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(831), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv70 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* 0/0 == nan */
	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv71 = 0
	}
	pp_ = ccv71
	{
		aa_p1 = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv72 = 0
	}
	pp_ = ccv72
	Xmpfr_clear_flags(cgtls)

	if ccv74 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv74 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(837), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv74 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv75 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN))), ppint64(1)) != 0; !ccv75 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(838), "__gmpfr_flags == 4\x00")
	}
	pp_ = ccv75 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* +inf/+inf == nan */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	Xmpfr_clear_flags(cgtls)

	if ccv77 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv77 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(846), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv77 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv78 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN))), ppint64(1)) != 0; !ccv78 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(847), "__gmpfr_flags == 4\x00")
	}
	pp_ = ccv78 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* 1/+0 = +inf */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDZ))
	{
		aa_p2 = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDZ)
		ccv79 = 0
	}
	pp_ = ccv79
	Xmpfr_clear_flags(cgtls)

	if ccv81 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv81 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(853), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv81 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv84 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv84 {
		if ccv82 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv82 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(854), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv82 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv83 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv83 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(854), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv83 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv90 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv84 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0)), ppint64(1)) != 0; !ccv90 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(854), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 854, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 854, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) > 0\x00")
		if ccv89 = iqlibc.Bool(!(0 != 0)); !ccv89 {
			if ccv88 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv88 {
				if ccv86 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv86 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(854), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv86 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv87 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv87 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(854), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv87 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv89 || ccv88 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv90 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv91 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0))), ppint64(1)) != 0; !ccv91 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(855), "__gmpfr_flags == 32\x00")
	}
	pp_ = ccv91 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* 1/-0 = -inf */
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDZ))
	{
		aa_p3 = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDZ)
		ccv92 = 0
	}
	pp_ = ccv92
	Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDZ))
	Xmpfr_clear_flags(cgtls)

	if ccv94 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv94 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(862), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv94 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv97 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv97 {
		if ccv95 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv95 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(863), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv95 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv96 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv96 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(863), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv96 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv103 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv97 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0)), ppint64(1)) != 0; !ccv103 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(863), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 863, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 863, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) < 0\x00")
		if ccv102 = iqlibc.Bool(!(0 != 0)); !ccv102 {
			if ccv101 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv101 {
				if ccv99 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv99 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(863), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv99 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv100 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv100 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(863), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv100 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv102 || ccv101 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv103 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv104 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0))), ppint64(1)) != 0; !ccv104 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(864), "__gmpfr_flags == 32\x00")
	}
	pp_ = ccv104 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* -1/+0 = -inf */
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDZ))
	{
		aa_p4 = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p4)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p4)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDZ)
		ccv105 = 0
	}
	pp_ = ccv105
	Xmpfr_clear_flags(cgtls)

	if ccv107 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv107 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(870), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv107 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv110 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv110 {
		if ccv108 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv108 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(871), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv108 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv109 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv109 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(871), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv109 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv116 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv110 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0)), ppint64(1)) != 0; !ccv116 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(871), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 871, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 871, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) < 0\x00")
		if ccv115 = iqlibc.Bool(!(0 != 0)); !ccv115 {
			if ccv114 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv114 {
				if ccv112 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv112 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(871), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv112 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv113 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv113 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(871), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv113 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv115 || ccv114 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv116 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv117 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0))), ppint64(1)) != 0; !ccv117 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(872), "__gmpfr_flags == 32\x00")
	}
	pp_ = ccv117 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* -1/-0 = +inf */
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDZ))
	{
		aa_p5 = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p5)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p5)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDZ)
		ccv118 = 0
	}
	pp_ = ccv118
	Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDZ))
	Xmpfr_clear_flags(cgtls)

	if ccv120 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv120 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(879), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv120 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv123 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv123 {
		if ccv121 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv121 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(880), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv121 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv122 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv122 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(880), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv122 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv129 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv123 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0)), ppint64(1)) != 0; !ccv129 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(880), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 880, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 880, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) > 0\x00")
		if ccv128 = iqlibc.Bool(!(0 != 0)); !ccv128 {
			if ccv127 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv127 {
				if ccv125 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv125 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(880), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv125 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv126 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv126 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(880), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv126 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv128 || ccv127 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv129 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv130 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0))), ppint64(1)) != 0; !ccv130 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(881), "__gmpfr_flags == 32\x00")
	}
	pp_ = ccv130 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* +inf/+0 = +inf */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	{
		aa_p6 = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p6)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p6)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDZ)
		ccv131 = 0
	}
	pp_ = ccv131
	Xmpfr_clear_flags(cgtls)

	if ccv133 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv133 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(888), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv133 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv136 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv136 {
		if ccv134 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv134 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(889), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv134 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv135 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv135 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(889), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv135 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv142 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv136 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0)), ppint64(1)) != 0; !ccv142 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(889), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 889, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 889, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) > 0\x00")
		if ccv141 = iqlibc.Bool(!(0 != 0)); !ccv141 {
			if ccv140 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv140 {
				if ccv138 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv138 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(889), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv138 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv139 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv139 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(889), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv139 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv141 || ccv140 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv142 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv143 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv143 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(890), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv143 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* +inf/-0 = -inf */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = iqlibc.ppInt32FromInt32(mvMPFR_SIGN_POS)
	{
		aa_p7 = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p7)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p7)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDZ)
		ccv144 = 0
	}
	pp_ = ccv144
	Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDZ))
	Xmpfr_clear_flags(cgtls)

	if ccv146 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv146 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(898), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv146 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv149 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv149 {
		if ccv147 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv147 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(899), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv147 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv148 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv148 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(899), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv148 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv155 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv149 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0)), ppint64(1)) != 0; !ccv155 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(899), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 899, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 899, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) < 0\x00")
		if ccv154 = iqlibc.Bool(!(0 != 0)); !ccv154 {
			if ccv153 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv153 {
				if ccv151 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv151 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(899), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv151 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv152 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv152 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(899), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv152 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv154 || ccv153 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv155 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv156 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv156 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(900), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv156 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* -inf/+0 = -inf */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = -iqlibc.ppInt32FromInt32(1)
	{
		aa_p8 = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p8)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p8)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDZ)
		ccv157 = 0
	}
	pp_ = ccv157
	Xmpfr_clear_flags(cgtls)

	if ccv159 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv159 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(907), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv159 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv162 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv162 {
		if ccv160 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv160 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(908), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv160 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv161 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv161 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(908), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv161 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv168 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv162 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0)), ppint64(1)) != 0; !ccv168 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(908), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 908, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 908, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) < 0\x00")
		if ccv167 = iqlibc.Bool(!(0 != 0)); !ccv167 {
			if ccv166 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv166 {
				if ccv164 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv164 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(908), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv164 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv165 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv165 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(908), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv165 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv167 || ccv166 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv168 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv169 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv169 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(909), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv169 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* -inf/-0 = +inf */
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(3)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign = -iqlibc.ppInt32FromInt32(1)
	{
		aa_p9 = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p9)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p9)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDZ)
		ccv170 = 0
	}
	pp_ = ccv170
	Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDZ))
	Xmpfr_clear_flags(cgtls)

	if ccv172 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv172 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(917), "mpfr_all_div (q, a, d, MPFR_RNDZ) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv172 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0) /* exact */

	if ccv175 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv175 {
		if ccv173 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv173 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(918), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv173 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv174 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv174 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(918), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv174 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv181 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv175 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0)), ppint64(1)) != 0; !ccv181 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(918), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 918, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 918, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) > 0\x00")
		if ccv180 = iqlibc.Bool(!(0 != 0)); !ccv180 {
			if ccv179 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv179 {
				if ccv177 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv177 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(918), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv177 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv178 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv178 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(918), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv178 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv180 || ccv179 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv181 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv182 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv182 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(919), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv182 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* check overflow */
	aaemax = X__gmpfr_emax
	Xset_emax(cgtls, ppint64(1))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDZ))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDZ))
	Xmpfr_div_2ui(cgtls, cgbp+32, cgbp+32, ppuint64(1), ppint32(ecMPFR_RNDZ))
	Xmpfr_clear_flags(cgtls)
	simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU)) /* 1 / 0.5 = 2 -> overflow */

	if ccv185 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv185 {
		if ccv183 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv183 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(929), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv183 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv184 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv184 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(929), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv184 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv191 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv185 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0)), ppint64(1)) != 0; !ccv191 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(929), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (2 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && (((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 929, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))) ? (void) 0 : __builtin_unreachable()),0))), ((void) (((__builtin_expect(!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))), 1))) || (mpfr_assert_fail (\"tdiv.c\", 929, \"! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\"),(! __builtin_constant_p (!!(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) || !(! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1)))) || (! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))) ? (void) 0 : __builtin_unreachable()),0))), ((q)->_mpfr_sign)) > 0\x00")
		if ccv190 = iqlibc.Bool(!(0 != 0)); !ccv190 {
			if ccv189 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)); ccv189 {
				if ccv187 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv187 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(929), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
				}
				pp_ = ccv187 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if ccv188 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv188 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(929), "! (((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
				}
				pp_ = ccv188 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			}
		}
		if !(ccv190 || ccv189 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv191 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv192 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)))), ppint64(1)) != 0; !ccv192 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(930), "__gmpfr_flags == (2 | 8)\x00")
	}
	pp_ = ccv192 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xset_emax(cgtls, aaemax)

	/* check underflow */
	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint64(-ppint32(1)))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDZ))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(2), ppint32(ecMPFR_RNDZ))
	Xmpfr_set_prec(cgtls, cgbp+32, (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_prec+ppint64(8))
	aai = -ppint32(1)
	for {
		if !(aai <= ppint32(1)) {
			break
		}

		/* Test 2^(-2) / (+/- (2 + eps)), with eps < 0, eps = 0, eps > 0.
		   -> underflow.
		   With div.c r5513, this test fails for eps > 0 in MPFR_RNDN. */
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDZ))
		if aai < 0 {
			Xmpfr_nextbelow(cgtls, cgbp+32)
		}
		if aai > 0 {
			Xmpfr_nextabove(cgtls, cgbp+32)
		}
		aasign = 0
		for {
			if !(aasign <= ppint32(1)) {
				break
			}

			Xmpfr_clear_flags(cgtls)
			simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) /* result = 0 */

			if ccv195 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)))), ppint64(1)) != 0; !ccv195 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(955), "__gmpfr_flags == (1 | 8)\x00")
			}
			pp_ = ccv195 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if aasign != 0 {
				ccv196 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0)
			} else {
				ccv196 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0)
			}
			if ccv197 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv196 != 0)), ppint64(1)) != 0; !ccv197 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(957), "sign ? (((q)->_mpfr_sign) < 0) : (((q)->_mpfr_sign) > 0)\x00")
			}
			pp_ = ccv197 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

			if ccv198 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv198 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(958), "(((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
			}
			pp_ = ccv198 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			Xmpfr_clear_flags(cgtls)
			simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN)) /* result = 0 iff eps >= 0 */

			if ccv199 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)))), ppint64(1)) != 0; !ccv199 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(961), "__gmpfr_flags == (1 | 8)\x00")
			}
			pp_ = ccv199 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if aasign != 0 {
				ccv200 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign < 0)
			} else {
				ccv200 = iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_sign > 0)
			}
			if ccv201 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv200 != 0)), ppint64(1)) != 0; !ccv201 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(963), "sign ? (((q)->_mpfr_sign) < 0) : (((q)->_mpfr_sign) > 0)\x00")
			}
			pp_ = ccv201 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			if aai < 0 {
				Xmpfr_nexttozero(cgtls, cgbp+64)
			}

			if ccv202 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv202 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(966), "(((q)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
			}
			pp_ = ccv202 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))

			goto cg_194
		cg_194:
			;
			aasign++
		}

		goto cg_193
	cg_193:
		;
		aai++
	}
	Xset_emin(cgtls, aaemin)

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
}

func siconsistency(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var aai, aainex1, aainex2 ppint32
	var aap, aapx, aapy, aapz tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv4, ccv8 ppbool
	var ccv5, ccv6, ccv7 ppint64
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z1 at bp+64 */ tnmpfr_t
	var pp_ /* z2 at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aai, aainex1, aainex2, aap, aapx, aapy, aapz, aarnd, ccv4, ccv5, ccv6, ccv7, ccv8

	Xmpfr_inits(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))

	aai = 0
	for {
		if !(aai < ppint32(10000)) {
			break
		}

		/* inex is undefined for RNDF */
		aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % ppuint64(ecMPFR_RNDF))
		aapx = iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%ppuint64(256) + ppuint64(2))
		aapy = iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%ppuint64(128) + ppuint64(2))
		aapz = iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%ppuint64(256) + ppuint64(2))
		Xmpfr_set_prec(cgtls, cgbp, aapx)
		Xmpfr_set_prec(cgtls, cgbp+32, aapy)
		Xmpfr_set_prec(cgtls, cgbp+64, aapz)
		Xmpfr_set_prec(cgtls, cgbp+96, aapz)
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		for cgcond := true; cgcond; cgcond = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		}
		aainex1 = Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, aarnd)

		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1005), "!(((z1)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if aapx > aapy {
			ccv6 = aapx
		} else {
			ccv6 = aapy
		}
		if ccv6 > aapz {
			if aapx > aapy {
				ccv7 = aapx
			} else {
				ccv7 = aapy
			}
			ccv5 = ccv7
		} else {
			ccv5 = aapz
		}
		aap = ccv5
		if Xmpfr_prec_round(cgtls, cgbp, aap, ppint32(ecMPFR_RNDN)) != 0 || Xmpfr_prec_round(cgtls, cgbp+32, aap, ppint32(ecMPFR_RNDN)) != 0 {

			Xprintf(cgtls, "mpfr_prec_round error for i = %d\n\x00", iqlibc.ppVaList(cgbp+136, aai))
			Xexit(cgtls, ppint32(1))
		}
		aainex2 = Xmpfr_div(cgtls, cgbp+96, cgbp, cgbp+32, aarnd)

		if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv8 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1014), "!(((z2)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if aainex1 != aainex2 || Xmpfr_cmp3(cgtls, cgbp+64, cgbp+96, ppint32(1)) != 0 {

			Xprintf(cgtls, "Consistency error for i = %d, rnd = %s\n\x00", iqlibc.ppVaList(cgbp+136, aai, Xmpfr_print_rnd_mode(cgtls, aarnd)))
			Xprintf(cgtls, "inex1=%d inex2=%d\n\x00", iqlibc.ppVaList(cgbp+136, aainex1, aainex2))
			Xprintf(cgtls, "z1=\x00", 0)
			Xmpfr_dump(cgtls, cgbp+64)
			Xprintf(cgtls, "z2=\x00", 0)
			Xmpfr_dump(cgtls, cgbp+96)
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aai++
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
}

// C documentation
//
//	/* Reported by Carl Witty on 2007-06-03 */
func sitest_20070603(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var pp_ /* c at bp+96 */ tnmpfr_t
	var pp_ /* d at bp+32 */ tnmpfr_t
	var pp_ /* n at bp+0 */ tnmpfr_t
	var pp_ /* q at bp+64 */ tnmpfr_t

	Xmpfr_init2(cgtls, cgbp, ppint64(128))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(128))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(31))
	Xmpfr_init2(cgtls, cgbp+96, ppint64(31))

	Xmpfr_set_str(cgtls, cgbp, "10384593717069655257060992206846485\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "10384593717069655257060992206847132\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+96, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+96, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in test_20070603\nGot        \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xprintf(cgtls, "instead of \x00", 0)
		Xmpfr_dump(cgtls, cgbp+96)
		Xexit(cgtls, ppint32(1))
	}

	/* same for 64-bit machines */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(256))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(256))
	Xmpfr_set_prec(cgtls, cgbp+64, ppint64(63))
	Xmpfr_set_str(cgtls, cgbp, "822752278660603021077484591278675252491367930877209729029898240\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "822752278660603021077484591278675252491367930877212507873738752\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp3(cgtls, cgbp+64, cgbp+96, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error in test_20070603\nGot        \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xprintf(cgtls, "instead of \x00", 0)
		Xmpfr_dump(cgtls, cgbp+96)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
}

// C documentation
//
//	/* Bug found while adding tests for mpfr_cot */
func sitest_20070628(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aaerr, aainex ppint32
	var aaold_emax tnmpfr_exp_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_ = aaerr, aainex, aaold_emax
	aaerr = 0

	aaold_emax = X__gmpfr_emax
	Xset_emax(cgtls, ppint64(256))

	Xmpfr_inits2(cgtls, ppint64(53), cgbp, iqlibc.ppVaList(cgbp+72, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_si_2exp(cgtls, cgbp+32, ppint64(1), ppint64(-ppint32(256)), ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	aainex = Xmpfr_div(cgtls, cgbp, cgbp, cgbp+32, ppint32(ecMPFR_RNDD))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign > 0 || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "Error in test_20070628: expected -Inf, got\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		aaerr++
	}
	if aainex >= 0 {

		Xprintf(cgtls, "Error in test_20070628: expected inex < 0, got %d\n\x00", iqlibc.ppVaList(cgbp+72, aainex))
		aaerr++
	}
	if !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) {

		Xprintf(cgtls, "Error in test_20070628: overflow flag is not set\n\x00", 0)
		aaerr++
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
	Xset_emax(cgtls, aaold_emax)
}

// C documentation
//
//	/* Bug in mpfr_divhigh_n_basecase when all limbs of q (except the most
//	   significant one) are B-1 where B=2^GMP_NUMB_BITS. Since we truncate
//	   the divisor at each step, it might happen at some point that
//	   (np[n-1],np[n-2]) > (d1,d0), and not only the equality.
//	   Reported by Ricky Farr
//	   <https://sympa.inria.fr/sympa/arc/mpfr/2015-10/msg00023.html>
//	   To get a failure, a MPFR_DIVHIGH_TAB entry below the MPFR_DIV_THRESHOLD
//	   limit must have a value 0. With most mparam.h files, this cannot occur. To
//	   make the bug appear, one can configure MPFR with -DMPFR_TUNE_COVERAGE. */
func sitest_20151023(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aai, aainex ppint32
	var aap tnmpfr_prec_t
	var ccv4, ccv6, ccv7, ccv9 ppbool
	var pp_ /* d at bp+32 */ tnmpfr_t
	var pp_ /* n at bp+0 */ tnmpfr_t
	var pp_ /* q at bp+64 */ tnmpfr_t
	var pp_ /* q0 at bp+96 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aai, aainex, aap, ccv4, ccv6, ccv7, ccv9

	aap = ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))
	for {
		if !(aap <= ppint64(2000)) {
			break
		}

		Xmpfr_init2(cgtls, cgbp, ppint64(2)*aap)
		Xmpfr_init2(cgtls, cgbp+32, aap)
		Xmpfr_init2(cgtls, cgbp+64, aap)
		Xmpfr_init2(cgtls, cgbp+96, ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))

		/* generate a random divisor of p bits */
		for cgcond := true; cgcond; cgcond = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		}
		/* generate a random non-zero quotient of GMP_NUMB_BITS bits */
		for cgcond := true; cgcond; cgcond = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_urandomb(cgtls, cgbp+96, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		}
		/* zero-pad the quotient to p bits */
		aainex = Xmpfr_prec_round(cgtls, cgbp+96, aap, ppint32(ecMPFR_RNDN))

		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1145), "inex == 0\x00")
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		aai = 0
		for {
			if !(aai < ppint32(3)) {
				break
			}

			/* i=0: try with the original quotient xxx000...000
			   i=1: try with the original quotient minus one ulp
			   i=2: try with the original quotient plus one ulp */
			if aai == ppint32(1) {
				Xmpfr_nextbelow(cgtls, cgbp+96)
			} else {
				if aai == ppint32(2) {

					Xmpfr_nextabove(cgtls, cgbp+96)
					Xmpfr_nextabove(cgtls, cgbp+96)
				}
			}

			aainex = Xmpfr_mul(cgtls, cgbp, cgbp+32, cgbp+96, ppint32(ecMPFR_RNDN))

			if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv6 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1161), "inex == 0\x00")
			}
			pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			Xmpfr_nextabove(cgtls, cgbp)
			Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
			if !(Xmpfr_equal_p(cgtls, cgbp+64, cgbp+96) != 0) {

				Xprintf(cgtls, "Error in test_20151023 for p=%ld, rnd=RNDN, i=%d\n\x00", iqlibc.ppVaList(cgbp+136, aap, aai))
				Xprintf(cgtls, "n=\x00", 0)
				Xmpfr_dump(cgtls, cgbp)
				Xprintf(cgtls, "d=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+32)
				Xprintf(cgtls, "expected q0=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+96)
				Xprintf(cgtls, "got       q=\x00", 0)
				Xmpfr_dump(cgtls, cgbp+64)
				Xexit(cgtls, ppint32(1))
			}

			aainex = Xmpfr_mul(cgtls, cgbp, cgbp+32, cgbp+96, ppint32(ecMPFR_RNDN))

			if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv7 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1176), "inex == 0\x00")
			}
			pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			Xmpfr_nextbelow(cgtls, cgbp)
			Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))

			if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp3(cgtls, cgbp+64, cgbp+96, ppint32(1)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv9 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1179), "mpfr_cmp3(q, q0, 1) == 0\x00")
				if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp3(cgtls, cgbp+64, cgbp+96, ppint32(1)) == 0) {
					X__builtin_unreachable(cgtls)
				}
			}
			pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

			goto cg_5
		cg_5:
			;
			aai++
		}

		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+32)
		Xmpfr_clear(cgtls, cgbp+64)
		Xmpfr_clear(cgtls, cgbp+96)

		goto cg_1
	cg_1:
		;
		aap++
	}
}

// C documentation
//
//	/* test a random division of p+extra bits divided by p+extra bits,
//	   with quotient of p bits only, where the p+extra bit approximation
//	   of the quotient is very near a rounding frontier. */
func sitest_bad_aux(cgtls *iqlibc.ppTLS, aap tnmpfr_prec_t, aaextra tnmpfr_prec_t) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aa_p, aa_p1 tnmpfr_srcptr
	var ccv10, ccv12, ccv5, ccv7 ppbool
	var ccv3, ccv8 ppint32
	var pp_ /* q at bp+128 */ tnmpfr_t
	var pp_ /* q0 at bp+96 */ tnmpfr_t
	var pp_ /* u at bp+0 */ tnmpfr_t
	var pp_ /* v at bp+32 */ tnmpfr_t
	var pp_ /* w at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, ccv10, ccv12, ccv3, ccv5, ccv7, ccv8

	Xmpfr_init2(cgtls, cgbp, aap+aaextra)
	Xmpfr_init2(cgtls, cgbp+32, aap+aaextra)
	Xmpfr_init2(cgtls, cgbp+64, aap+aaextra)
	Xmpfr_init2(cgtls, cgbp+96, aap)
	Xmpfr_init2(cgtls, cgbp+128, aap)
	for cgcond := true; cgcond; cgcond = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+96)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp+96, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	}
	for cgcond := true; cgcond; cgcond = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	}

	{
		aa_p = cgbp + 96
		ccv3 = Xmpfr_set4(cgtls, cgbp+64, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
	}
	pp_ = ccv3                                                      /* exact */
	Xmpfr_nextabove(cgtls, cgbp+64)                                 /* now w > q0 */
	Xmpfr_mul(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))  /* thus u > v*q0 */
	Xmpfr_div(cgtls, cgbp+128, cgbp, cgbp+32, ppint32(ecMPFR_RNDU)) /* should have q > q0 */

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp3(cgtls, cgbp+128, cgbp+96, ppint32(1)) > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1209), "mpfr_cmp3(q, q0, 1) > 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp3(cgtls, cgbp+128, cgbp+96, ppint32(1)) > 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_div(cgtls, cgbp+128, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) /* should have q = q0 */

	if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp3(cgtls, cgbp+128, cgbp+96, ppint32(1)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv7 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1211), "mpfr_cmp3(q, q0, 1) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp3(cgtls, cgbp+128, cgbp+96, ppint32(1)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	{
		aa_p1 = cgbp + 96
		ccv8 = Xmpfr_set4(cgtls, cgbp+64, aa_p1, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign)
	}
	pp_ = ccv8                                                      /* exact */
	Xmpfr_nextbelow(cgtls, cgbp+64)                                 /* now w < q0 */
	Xmpfr_mul(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDZ))  /* thus u < v*q0 */
	Xmpfr_div(cgtls, cgbp+128, cgbp, cgbp+32, ppint32(ecMPFR_RNDZ)) /* should have q < q0 */

	if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp3(cgtls, cgbp+128, cgbp+96, ppint32(1)) < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv10 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1217), "mpfr_cmp3(q, q0, 1) < 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp3(cgtls, cgbp+128, cgbp+96, ppint32(1)) < 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_div(cgtls, cgbp+128, cgbp, cgbp+32, ppint32(ecMPFR_RNDU)) /* should have q = q0 */

	if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp3(cgtls, cgbp+128, cgbp+96, ppint32(1)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv12 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1219), "mpfr_cmp3(q, q0, 1) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp3(cgtls, cgbp+128, cgbp+96, ppint32(1)) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
	Xmpfr_clear(cgtls, cgbp+128)
}

func sitest_bad(cgtls *iqlibc.ppTLS) {

	var aaextra, aap tnmpfr_prec_t
	pp_, pp_ = aaextra, aap

	aap = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aap <= ppint64(1024)) {
			break
		}
		aaextra = ppint64(2)
		for {
			if !(aaextra <= ppint64(64)) {
				break
			}
			sitest_bad_aux(cgtls, aap, aaextra)
			goto cg_2
		cg_2:
			;
			aaextra++
		}
		goto cg_1
	cg_1:
		;
		aap += ppint64(17)
	}
}

/* Generic test file for functions with one or two arguments (the second being
   either mpfr_t or double or unsigned long).

Copyright 2001-2023 Free Software Foundation, Inc.
Contributed by the AriC and Caramba projects, INRIA.

This file is part of the GNU MPFR Library.

The GNU MPFR Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The GNU MPFR Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

/* Define TWO_ARGS for two-argument functions like mpfr_pow.
   Define DOUBLE_ARG1 or DOUBLE_ARG2 for function with a double operand in
   first or second place like sub_d or d_sub.
   Define ULONG_ARG1 or ULONG_ARG2 for function with an unsigned long
   operand in first or second place like sub_ui or ui_sub.
   Define THREE_ARGS for three-argument functions like mpfr_atan2u. */

/* TODO: Add support for type long and extreme integer values, as done
   in tgeneric_ui.c; then tgeneric_ui.c could probably disappear. */

/* For the random function: one number on two is negative. */

/* For the random function: one number on two is negative. */

/* If the MPFR_SUSPICIOUS_OVERFLOW test fails but this is not a bug,
   then define TGENERIC_SO_TEST with an adequate test (possibly 0) to
   omit this particular case. */

/* For some functions (for example cos), the argument reduction is too
   expensive when using mpfr_get_emax(). Then simply define REDUCE_EMAX
   to some reasonable value before including tgeneric.c. */

/* same for mpfr_get_emin() */

func sitest_generic(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aanmax ppuint32) {
	cgbp := cgtls.ppAlloc(320)
	defer cgtls.ppFree(320)

	var aa_p, aa_p1, aa_p2, aa_p3 tnmpfr_ptr
	var aa_p4 tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, aainfinite_input, aatest_of, aatest_uf, ccv10, ccv13, ccv15, ccv16, ccv17, ccv19, ccv21, ccv24, ccv26, ccv27, ccv28, ccv30, ccv32, ccv35, ccv37, ccv38, ccv39, ccv41, ccv43, ccv46, ccv48, ccv49, ccv50, ccv54, ccv8 ppint32
	var aactrn, aactrt ppuint64
	var aae, aaemax, aaemin, aaoemax, aaoemin, aaold_emax, aaold_emin tnmpfr_exp_t
	var aaex_flags, aaex_flags1, aaflags, aaoldflags tnmpfr_flags_t
	var aan, ccv3 ppuint32
	var aaprec, aaxprec, aayprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv11, ccv14, ccv18, ccv22, ccv25, ccv29, ccv33, ccv36, ccv40, ccv44, ccv47, ccv51, ccv52, ccv53, ccv55, ccv56, ccv57, ccv58, ccv7 ppbool
	var ccv4 ppfloat64
	var pp_ /* t at bp+96 */ tnmpfr_t
	var pp_ /* w at bp+128 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* x2 at bp+224 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* yd at bp+160 */ tnmpfr_t
	var pp_ /* yu at bp+192 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aa_p2, aa_p3, aa_p4, aacompare, aacompare2, aactrn, aactrt, aae, aaemax, aaemin, aaex_flags, aaex_flags1, aaflags, aainexact, aainfinite_input, aan, aaoemax, aaoemin, aaold_emax, aaold_emin, aaoldflags, aaprec, aarnd, aatest_of, aatest_uf, aaxprec, aayprec, ccv10, ccv11, ccv13, ccv14, ccv15, ccv16, ccv17, ccv18, ccv19, ccv21, ccv22, ccv24, ccv25, ccv26, ccv27, ccv28, ccv29, ccv3, ccv30, ccv32, ccv33, ccv35, ccv36, ccv37, ccv38, ccv39, ccv4, ccv40, ccv41, ccv43, ccv44, ccv46, ccv47, ccv48, ccv49, ccv50, ccv51, ccv52, ccv53, ccv54, ccv55, ccv56, ccv57, ccv58, ccv7, ccv8
	aactrt = ppuint64(0)
	aactrn = ppuint64(0)

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	Xmpfr_inits2(cgtls, ppint64(mvMPFR_PREC_MIN), cgbp, iqlibc.ppVaList(cgbp+264, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_init2(cgtls, cgbp+224, ppint64(mvMPFR_PREC_MIN))

	/* generic tests */
	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		/* Number of overflow/underflow tests for each precision.
		   Since MPFR uses several algorithms and there may also be
		   early overflow/underflow detection, several tests may be
		   needed to detect a bug. */
		aatest_of = ppint32(3)
		aatest_uf = ppint32(3)

		Xmpfr_set_prec(cgtls, cgbp+64, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+96, aaprec)
		aayprec = aaprec + ppint64(20)
		Xmpfr_set_prec(cgtls, cgbp+32, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+160, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+192, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+128, aayprec)

		/* Note: in precision p1, we test NSPEC special cases. */
		aan = ppuint32(0)
		for {
			if aaprec == aap1 {
				ccv3 = aanmax + ppuint32(9)
			} else {
				ccv3 = aanmax
			}
			if !(aan < ccv3) {
				break
			}

			aainfinite_input = 0

			aaxprec = aaprec
			if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {

				/* In half cases, modify the precision of the inputs:
				   If the base precision (for the result) is small,
				   take a larger input precision in general, else
				   take a smaller precision. */
				if aaprec < ppint64(16) {
					ccv4 = ppfloat64(256)
				} else {
					ccv4 = ppfloat64(1)
				}
				aaxprec = tnmpfr_prec_t(ppfloat64(aaxprec) * (ccv4 * ppfloat64(Xrandlimb(cgtls)) / ppfloat64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1)))))
				if aaxprec < ppint64(mvMPFR_PREC_MIN) {
					aaxprec = ppint64(mvMPFR_PREC_MIN)
				}
			}
			Xmpfr_set_prec(cgtls, cgbp, aaxprec)
			Xmpfr_set_prec(cgtls, cgbp+224, aaxprec)

			/* Generate random arguments, even in the special cases
			   (this may not be needed, but this is simpler).
			   Note that if RAND_FUNCTION is defined, this specific
			   random function is used for all arguments; this is
			   typically mpfr_random2, which generates a positive
			   random mpfr_t with long runs of consecutive ones and
			   zeros in the binary representation. */

			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_random2(cgtls, cgbp, ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt64FromInt32(1), iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%ppuint64(100)), ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			if !(Xmpfr_rands_initialized != 0) {
				Xmpfr_rands_initialized = ppuint8(1)
				X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
				pp_ = iqlibc.ppInt32FromInt32(0)
			}
			Xmpfr_random2(cgtls, cgbp+224, ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec-iqlibc.ppInt64FromInt32(1))/ppint64(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))+iqlibc.ppInt64FromInt32(1), iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%ppuint64(100)), ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))

			if aan < ppuint32(9) && aaprec == aap1 {

				/* Special cases tested in precision p1 if n < NSPEC. They are
				   useful really in the extended exponent range. */
				/* TODO: x2 is set even when it is associated with a double;
				   check whether this really makes sense. */
				Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
				Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
				if aan == ppuint32(0) {

					Xmpfr_set_nan(cgtls, cgbp)
				} else {
					if aan <= ppuint32(2) {

						if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(1) || aan == ppuint32(2))), ppint64(1)) != 0; !ccv7 {
							Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(290), "n == 1 || n == 2\x00")
						}
						pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
						if aan == ppuint32(1) {
							ccv8 = ppint32(1)
						} else {
							ccv8 = -ppint32(1)
						}
						pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv8), 0, ppint32(ecMPFR_RNDN))
						Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)

						if ccv11 = iqlibc.Bool(0 != 0); ccv11 {
							if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
								ccv10 = -ppint32(1)
							} else {
								ccv10 = ppint32(1)
							}
						}
						if ccv11 && ppint64(ccv10) >= 0 {
							if ccv14 = iqlibc.Bool(0 != 0); ccv14 {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv13 = -ppint32(1)
								} else {
									ccv13 = ppint32(1)
								}
							}
							if ccv14 && iqlibc.ppUint64FromInt64(ppint64(ccv13)) == ppuint64(0) {
								{
									aa_p = cgbp + 224
									(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
									(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
									pp_ = ppint32(ecMPFR_RNDN)
									ccv15 = 0
								}
								pp_ = ccv15
							} else {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv16 = -ppint32(1)
								} else {
									ccv16 = ppint32(1)
								}
								Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv16)), 0, ppint32(ecMPFR_RNDN))
							}
						} else {
							if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
								ccv17 = -ppint32(1)
							} else {
								ccv17 = ppint32(1)
							}
							Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv17), 0, ppint32(ecMPFR_RNDN))
						}
						Xmpfr_set_exp(cgtls, cgbp+224, X__gmpfr_emin)
					} else {
						if aan <= ppuint32(4) {

							if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(3) || aan == ppuint32(4))), ppint64(1)) != 0; !ccv18 {
								Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(300), "n == 3 || n == 4\x00")
							}
							pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
							if aan == ppuint32(3) {
								ccv19 = ppint32(1)
							} else {
								ccv19 = -ppint32(1)
							}
							pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv19), 0, ppint32(ecMPFR_RNDN))
							Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)

							if ccv22 = iqlibc.Bool(0 != 0); ccv22 {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv21 = -ppint32(1)
								} else {
									ccv21 = ppint32(1)
								}
							}
							if ccv22 && ppint64(ccv21) >= 0 {
								if ccv25 = iqlibc.Bool(0 != 0); ccv25 {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv24 = -ppint32(1)
									} else {
										ccv24 = ppint32(1)
									}
								}
								if ccv25 && iqlibc.ppUint64FromInt64(ppint64(ccv24)) == ppuint64(0) {
									{
										aa_p1 = cgbp + 224
										(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
										(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
										pp_ = ppint32(ecMPFR_RNDN)
										ccv26 = 0
									}
									pp_ = ccv26
								} else {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv27 = -ppint32(1)
									} else {
										ccv27 = ppint32(1)
									}
									Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv27)), 0, ppint32(ecMPFR_RNDN))
								}
							} else {
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									ccv28 = -ppint32(1)
								} else {
									ccv28 = ppint32(1)
								}
								Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv28), 0, ppint32(ecMPFR_RNDN))
							}
							Xmpfr_setmax(cgtls, cgbp+224, X__gmpfr_emax)
						} else {
							if aan <= ppuint32(6) {

								if ccv29 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(5) || aan == ppuint32(6))), ppint64(1)) != 0; !ccv29 {
									Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(311), "n == 5 || n == 6\x00")
								}
								pp_ = ccv29 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
								if aan == ppuint32(5) {
									ccv30 = ppint32(1)
								} else {
									ccv30 = -ppint32(1)
								}
								pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv30), 0, ppint32(ecMPFR_RNDN))
								Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)

								if ccv33 = iqlibc.Bool(0 != 0); ccv33 {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv32 = -ppint32(1)
									} else {
										ccv32 = ppint32(1)
									}
								}
								if ccv33 && ppint64(ccv32) >= 0 {
									if ccv36 = iqlibc.Bool(0 != 0); ccv36 {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv35 = -ppint32(1)
										} else {
											ccv35 = ppint32(1)
										}
									}
									if ccv36 && iqlibc.ppUint64FromInt64(ppint64(ccv35)) == ppuint64(0) {
										{
											aa_p2 = cgbp + 224
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_sign = ppint32(1)
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p2)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
											pp_ = ppint32(ecMPFR_RNDN)
											ccv37 = 0
										}
										pp_ = ccv37
									} else {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv38 = -ppint32(1)
										} else {
											ccv38 = ppint32(1)
										}
										Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv38)), 0, ppint32(ecMPFR_RNDN))
									}
								} else {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv39 = -ppint32(1)
									} else {
										ccv39 = ppint32(1)
									}
									Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv39), 0, ppint32(ecMPFR_RNDN))
								}
								Xmpfr_setmax(cgtls, cgbp+224, X__gmpfr_emax)
							} else {

								if ccv40 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(7) || aan == ppuint32(8))), ppint64(1)) != 0; !ccv40 {
									Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(319), "n == 7 || n == 8\x00")
								}
								pp_ = ccv40 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
								if aan == ppuint32(7) {
									ccv41 = ppint32(1)
								} else {
									ccv41 = -ppint32(1)
								}
								pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv41), 0, ppint32(ecMPFR_RNDN))
								Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)

								if ccv44 = iqlibc.Bool(0 != 0); ccv44 {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv43 = -ppint32(1)
									} else {
										ccv43 = ppint32(1)
									}
								}
								if ccv44 && ppint64(ccv43) >= 0 {
									if ccv47 = iqlibc.Bool(0 != 0); ccv47 {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv46 = -ppint32(1)
										} else {
											ccv46 = ppint32(1)
										}
									}
									if ccv47 && iqlibc.ppUint64FromInt64(ppint64(ccv46)) == ppuint64(0) {
										{
											aa_p3 = cgbp + 224
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_sign = ppint32(1)
											(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p3)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
											pp_ = ppint32(ecMPFR_RNDN)
											ccv48 = 0
										}
										pp_ = ccv48
									} else {
										if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
											ccv49 = -ppint32(1)
										} else {
											ccv49 = ppint32(1)
										}
										Xmpfr_set_ui_2exp(cgtls, cgbp+224, iqlibc.ppUint64FromInt64(ppint64(ccv49)), 0, ppint32(ecMPFR_RNDN))
									}
								} else {
									if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
										ccv50 = -ppint32(1)
									} else {
										ccv50 = ppint32(1)
									}
									Xmpfr_set_si_2exp(cgtls, cgbp+224, ppint64(ccv50), 0, ppint32(ecMPFR_RNDN))
								}
								Xmpfr_set_exp(cgtls, cgbp+224, X__gmpfr_emin)
							}
						}
					}
				}
			}

			/* Exponent range for the test. */
			aaoemin = X__gmpfr_emin
			aaoemax = X__gmpfr_emax

			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt32(ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)))
			Xmpfr_clear_flags(cgtls)
			aacompare = simpfr_all_div(cgtls, cgbp+32, cgbp, cgbp+224, aarnd)
			aaflags = X__gmpfr_flags
			if X__gmpfr_emin != aaoemin || X__gmpfr_emax != aaoemax {

				Xprintf(cgtls, "tgeneric: the exponent range has been modified by the tested function!\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
			if aarnd != ppint32(ecMPFR_RNDF) {
				if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) == iqlibc.ppInt32FromInt32(0)) != 0) {
					Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad inexact flag for mpfr_all_div\x00"))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					if cgbp+224 != ppuintptr(0) {
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+224)
					}
					if -ppint32(1) >= 0 {
						Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
					}
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}
			aactrt++

			/* If rnd = RNDF, check that we obtain the same result as
			   RNDD or RNDU. */
			if aarnd == ppint32(ecMPFR_RNDF) {

				simpfr_all_div(cgtls, cgbp+160, cgbp, cgbp+224, ppint32(ecMPFR_RNDD))
				simpfr_all_div(cgtls, cgbp+192, cgbp, cgbp+224, ppint32(ecMPFR_RNDU))
				if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+160) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_sign || ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+192) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_sign)) {

					Xprintf(cgtls, "tgeneric: error formpfr_all_div, RNDF; result matches neither RNDD nor RNDU\n\x00", 0)
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "x2 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+224)
					Xprintf(cgtls, "yd (RNDD) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+160)
					Xprintf(cgtls, "yu (RNDU) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+192)
					Xprintf(cgtls, "y  (RNDF) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xexit(cgtls, ppint32(1))
				}
			}

			/* Tests in a reduced exponent range. */

			aaoldflags = aaflags

			/* Determine the smallest exponent range containing the
			   exponents of the mpfr_t inputs (x, and u if TWO_ARGS)
			   and output (y). */
			aaemin = iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) - iqlibc.ppInt64FromInt32(1)
			aaemax = iqlibc.ppInt64FromInt32(1) - iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))

			if ccv51 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv51 {
			}
			if ccv51 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv52 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv52 {
			}
			if ccv52 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 224)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv53 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv53 {
			}
			if ccv53 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp /* exponent of the result */

				if aatest_of > 0 && aae-ppint64(1) >= aaemax { /* overflow test */

					/* Exponent e of the result > exponents of the inputs;
					   let's set emax to e - 1, so that one should get an
					   overflow. */
					Xset_emax(cgtls, aae-ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = simpfr_all_div(cgtls, cgbp+128, cgbp, cgbp+224, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emax(cgtls, aaoemax)
					aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for mpfr_all_div, reduced exponent range [%ld,%ld] (overflow test) on:\n\x00", iqlibc.ppVaList(cgbp+264, aaoemin, aae-ppint64(1)))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+224)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+264, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_of--
				}

				if aatest_uf > 0 && aae+ppint64(1) <= aaemin { /* underflow test */

					/* Exponent e of the result < exponents of the inputs;
					   let's set emin to e + 1, so that one should get an
					   underflow. */
					Xset_emin(cgtls, aae+ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = simpfr_all_div(cgtls, cgbp+128, cgbp, cgbp+224, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emin(cgtls, aaoemin)
					aaex_flags1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags1 && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for mpfr_all_div, reduced exponent range [%ld,%ld] (underflow test) on:\n\x00", iqlibc.ppVaList(cgbp+264, aae+ppint64(1), aaoemax))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp+224)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags1)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+264, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_uf--
				}

				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			} /* MPFR_IS_PURE_FP (y) */

			if aaemin > aaemax {
				aaemin = aaemax
			} /* case where all values are singular */

			/* Consistency test in a reduced exponent range. Doing it
			   for the first 10 samples and for prec == p1 (which has
			   some special cases) should be sufficient. */
			if aactrt <= ppuint64(10) || aaprec == aap1 {

				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				Xmpfr_clear_flags(cgtls)
				aainexact = simpfr_all_div(cgtls, cgbp+128, cgbp, cgbp+224, aarnd)
				aaflags = X__gmpfr_flags
				Xset_emin(cgtls, aaoemin)
				Xset_emax(cgtls, aaoemax)
				/* That test makes no sense for RNDF. */
				if aarnd != ppint32(ecMPFR_RNDF) && !(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+128, cgbp+32) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign) && iqlibc.ppBoolInt32(aainexact > 0)-iqlibc.ppBoolInt32(aainexact < 0) == iqlibc.ppBoolInt32(aacompare > 0)-iqlibc.ppBoolInt32(aacompare < 0) && aaflags == aaoldflags) {

					Xprintf(cgtls, "tgeneric: error for mpfr_all_div, reduced exponent range [%ld,%ld] on:\n\x00", iqlibc.ppVaList(cgbp+264, aaemin, aaemax))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "x2 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+224)
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "Expected:\n  y = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+264, aacompare))
					Xflags_out(cgtls, aaoldflags)
					Xprintf(cgtls, "Got:\n  w = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+128)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+264, aainexact))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}

			X__gmpfr_flags = aaoldflags /* restore the flags */
			/* tests in a reduced exponent range */

			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 {
					if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad NaN flag for mpfr_all_div\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

						if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad overflow flag for mpfr_all_div\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(iqlibc.ppBoolInt32(aacompare == 0 && !(aainfinite_input != 0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad divide-by-zero flag for mpfr_all_div\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) {
							if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad underflow flag for mpfr_all_div\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+224 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+224)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						}
					}
				}
			} else {
				if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0 {

					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "both overflow and divide-by-zero for mpfr_all_div\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "both underflow and divide-by-zero for mpfr_all_div\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !(aacompare == iqlibc.ppInt32FromInt32(0)) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad compare value (divide-by-zero) for mpfr_all_div\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if cgbp+224 != ppuintptr(0) {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+224)
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 {

						if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "both underflow and overflow for mpfr_all_div\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad compare value (overflow) for mpfr_all_div\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						Xmpfr_nexttoinf(cgtls, cgbp+32)
						if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "should have been max MPFR number (overflow) for mpfr_all_div\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if cgbp+224 != ppuintptr(0) {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp+224)
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {

							if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "bad compare value (underflow) for mpfr_all_div\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+224 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+224)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
							Xmpfr_nexttozero(cgtls, cgbp+32)
							if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+264, "should have been min MPFR number (underflow) for mpfr_all_div\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if cgbp+224 != ppuintptr(0) {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+224)
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						} else {
							if aacompare == 0 || aarnd == ppint32(ecMPFR_RNDF) || Xmpfr_can_round(cgtls, cgbp+32, aayprec, aarnd, aarnd, aaprec) != 0 {

								aactrn++
								{
									aa_p4 = cgbp + 32
									ccv54 = Xmpfr_set4(cgtls, cgbp+96, aa_p4, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p4)).fd_mpfr_sign)
								}
								pp_ = ccv54
								/* Risk of failures are known when some flags are already set
								   before the function call. Do not set the erange flag, as
								   it will remain set after the function call and no checks
								   are performed in such a case (see the mpfr_erangeflag_p
								   test below). */
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									X__gmpfr_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0) ^ iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE))
								}
								/* Let's increase the precision of the inputs in a random way.
								   In most cases, this doesn't make any difference, but for
								   the mpfr_fmod bug fixed in r6230, this triggers the bug. */
								Xmpfr_prec_round(cgtls, cgbp, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint64(15)), ppint32(ecMPFR_RNDN))
								Xmpfr_prec_round(cgtls, cgbp+224, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint64(15)), ppint32(ecMPFR_RNDN))
								aainexact = simpfr_all_div(cgtls, cgbp+64, cgbp, cgbp+224, aarnd)
								if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0 {
									goto ppnext_n
								}
								if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+64) != 0) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "tgeneric: results differ for mpfr_all_div on\n\x00", 0)
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp+224)
									Xprintf(cgtls, "prec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
									Xprintf(cgtls, "Got      \x00", 0)
									Xmpfr_dump(cgtls, cgbp+64)
									Xprintf(cgtls, "Expected \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xprintf(cgtls, "Approx   \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xexit(cgtls, ppint32(1))
								}
								aacompare2 = Xmpfr_cmp3(cgtls, cgbp+96, cgbp+32, ppint32(1))
								/* if rounding to nearest, cannot know the sign of t - f(x)
								   because of composed rounding: y = o(f(x)) and t = o(y) */
								if aacompare*aacompare2 >= 0 {
									aacompare = aacompare + aacompare2
								} else {
									aacompare = aainexact
								} /* cannot determine sign(t-f(x)) */
								if !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aacompare > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aacompare < iqlibc.ppInt32FromInt32(0))) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+264, Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare, aainexact))
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp+224)
									Xprintf(cgtls, "y = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xprintf(cgtls, "t = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xexit(cgtls, ppint32(1))
								}
							} else {
								if Xgetenv(cgtls, "MPFR_SUSPICIOUS_OVERFLOW\x00") != iqlibc.ppUintptrFromInt32(0) {

									/* For developers only! */

									if ccv55 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv55 {
									}
									if ccv56 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv55 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0))), ppint64(1)) != 0; !ccv56 {
										Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(803), "(!(((y)->_mpfr_exp) <= (((-0x7fffffffffffffffL-1))+3)) && ((! __builtin_constant_p (!!(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) || !(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0))) || (((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) ? (void) 0 : __builtin_unreachable()), 1))\x00")
									}
									pp_ = ccv56 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
									Xmpfr_nexttoinf(cgtls, cgbp+32)

									if ccv58 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3); ccv58 {
										if ccv57 = aarnd == ppint32(ecMPFR_RNDZ); !ccv57 {
										}
									}
									if ccv58 && (ccv57 || aarnd+iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)) && !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) && iqlibc.Bool(ppint32(mvTGENERIC_SO_TEST) != 0) {

										Xprintf(cgtls, "Possible bug! |y| is the maximum finite number (with yprec = %u) and has\nbeen obtained when rounding toward zero (%s). Thus there is a very\nprobable overflow, but the overflow flag is not set!\n\x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
										Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp)
										Xprintf(cgtls, "x2[%u] = \x00", iqlibc.ppVaList(cgbp+264, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+224)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp+224)
										Xexit(cgtls, ppint32(1))
									}
								}
							}
						}
					}
				}
			}

			goto ppnext_n
		ppnext_n:
			;
			/* In case the exponent range has been changed by
			   tests_default_random() or for special values... */
			Xset_emin(cgtls, aaold_emin)
			Xset_emax(cgtls, aaold_emax)

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	if Xgetenv(cgtls, "MPFR_TGENERIC_STAT\x00") != iqlibc.ppUintptrFromInt32(0) {
		Xprintf(cgtls, "tgeneric: normal cases / total = %lu / %lu\n\x00", iqlibc.ppVaList(cgbp+264, aactrn, aactrt))
	}

	if ppuint64(3)*aactrn < ppuint64(2)*aactrt {
		Xprintf(cgtls, "Warning! Too few normal cases in generic tests (%lu / %lu)\n\x00", iqlibc.ppVaList(cgbp+264, aactrn, aactrt))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+264, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_clear(cgtls, cgbp+224)
}

func sitest_extreme(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(112)
	defer cgtls.ppFree(112)

	var aaemax, aaemin tnmpfr_exp_t
	var aaex_flags, aaflags ppuint32
	var aaj, aar, aaxi, aayi, aazi ppint32
	var aap [4]tnmpfr_prec_t
	var ccv11, ccv13, ccv3, ccv7 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaemin, aaex_flags, aaflags, aaj, aap, aar, aaxi, aayi, aazi, ccv11, ccv13, ccv3, ccv7
	aap = [4]tnmpfr_prec_t{
		0: ppint64(8),
		1: ppint64(32),
		2: ppint64(64),
		3: ppint64(256),
	}

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax

	Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
	Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))

	aaxi = 0
	for {
		if !(aaxi < ppint32(4)) {
			break
		}

		Xmpfr_init2(cgtls, cgbp, aap[aaxi])
		Xmpfr_setmax(cgtls, cgbp, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))

		if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_check(cgtls, cgbp) != 0)), ppint64(1)) != 0; !ccv3 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1262), "mpfr_check (x)\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_check(cgtls, cgbp) != 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aayi = 0
		for {
			if !(aayi < ppint32(4)) {
				break
			}

			Xmpfr_init2(cgtls, cgbp+32, aap[aayi])
			Xmpfr_setmin(cgtls, cgbp+32, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
			aaj = 0
			for {
				if !(aaj < ppint32(2)) {
					break
				}

				if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_check(cgtls, cgbp+32) != 0)), ppint64(1)) != 0; !ccv7 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1269), "mpfr_check (y)\x00")
					if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_check(cgtls, cgbp+32) != 0) {
						X__builtin_unreachable(cgtls)
					}
				}
				pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				aazi = 0
				for {
					if !(aazi < ppint32(4)) {
						break
					}

					Xmpfr_init2(cgtls, cgbp+64, aap[aazi])
					aar = 0
					for {
						if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
							break
						}

						Xmpfr_clear_flags(cgtls)
						Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, aar)
						aaflags = X__gmpfr_flags

						if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_check(cgtls, cgbp+64) != 0)), ppint64(1)) != 0; !ccv11 {
							Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1278), "mpfr_check (z)\x00")
							if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_check(cgtls, cgbp+64) != 0) {
								X__builtin_unreachable(cgtls)
							}
						}
						pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
						aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
						if aaflags != aaex_flags {

							Xprintf(cgtls, "Bad flags in test_extreme on z = a/b with %s and\n\x00", iqlibc.ppVaList(cgbp+104, Xmpfr_print_rnd_mode(cgtls, aar)))
							Xprintf(cgtls, "a = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							Xprintf(cgtls, "b = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+32)
							Xprintf(cgtls, "Expected flags:\x00", 0)
							Xflags_out(cgtls, aaex_flags)
							Xprintf(cgtls, "Got flags:     \x00", 0)
							Xflags_out(cgtls, aaflags)
							Xprintf(cgtls, "z = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+64)
							Xexit(cgtls, ppint32(1))
						}
						Xmpfr_clear_flags(cgtls)
						Xmpfr_div(cgtls, cgbp+64, cgbp+32, cgbp, aar)
						aaflags = X__gmpfr_flags

						if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_check(cgtls, cgbp+64) != 0)), ppint64(1)) != 0; !ccv13 {
							Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1300), "mpfr_check (z)\x00")
							if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_check(cgtls, cgbp+64) != 0) {
								X__builtin_unreachable(cgtls)
							}
						}
						pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
						aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
						if aaflags != aaex_flags {

							Xprintf(cgtls, "Bad flags in test_extreme on z = a/b with %s and\n\x00", iqlibc.ppVaList(cgbp+104, Xmpfr_print_rnd_mode(cgtls, aar)))
							Xprintf(cgtls, "a = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+32)
							Xprintf(cgtls, "b = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							Xprintf(cgtls, "Expected flags:\x00", 0)
							Xflags_out(cgtls, aaex_flags)
							Xprintf(cgtls, "Got flags:     \x00", 0)
							Xflags_out(cgtls, aaflags)
							Xprintf(cgtls, "z = \x00", 0)
							Xmpfr_dump(cgtls, cgbp+64)
							Xexit(cgtls, ppint32(1))
						}

						goto cg_9
					cg_9:
						;
						aar++
					}
					Xmpfr_clear(cgtls, cgbp+64)

					goto cg_8
				cg_8:
					;
					aazi++
				} /* zi */
				Xmpfr_nextabove(cgtls, cgbp+32)

				goto cg_5
			cg_5:
				;
				aaj++
			} /* j */
			Xmpfr_clear(cgtls, cgbp+32)

			goto cg_4
		cg_4:
			;
			aayi++
		} /* yi */
		Xmpfr_clear(cgtls, cgbp)

		goto cg_1
	cg_1:
		;
		aaxi++
	} /* xi */

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)
}

func sitestall_rndf(cgtls *iqlibc.ppTLS, aapmax tnmpfr_prec_t) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aapa, aapb, aapc tnmpfr_prec_t
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b at bp+32 */ tnmpfr_t
	var pp_ /* c at bp+64 */ tnmpfr_t
	var pp_ /* d at bp+96 */ tnmpfr_t
	pp_, pp_, pp_ = aapa, aapb, aapc

	aapa = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aapa <= aapmax) {
			break
		}

		Xmpfr_init2(cgtls, cgbp, aapa)
		Xmpfr_init2(cgtls, cgbp+96, aapa)
		aapb = ppint64(mvMPFR_PREC_MIN)
		for {
			if !(aapb <= aapmax) {
				break
			}

			Xmpfr_init2(cgtls, cgbp+32, aapb)
			pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
			for Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) < 0 {

				aapc = ppint64(mvMPFR_PREC_MIN)
				for {
					if !(aapc <= aapmax) {
						break
					}

					Xmpfr_init2(cgtls, cgbp+64, aapc)
					pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
					for Xmpfr_cmp_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) < 0 {

						Xmpfr_div(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDF))
						Xmpfr_div(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDD))
						if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

							Xmpfr_div(cgtls, cgbp+96, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))
							if !(Xmpfr_equal_p(cgtls, cgbp, cgbp+96) != 0) {

								Xprintf(cgtls, "Error: mpfr_div(a,b,c,RNDF) does not match RNDD/RNDU\n\x00", 0)
								Xprintf(cgtls, "b=\x00", 0)
								Xmpfr_dump(cgtls, cgbp+32)
								Xprintf(cgtls, "c=\x00", 0)
								Xmpfr_dump(cgtls, cgbp+64)
								Xprintf(cgtls, "a=\x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								Xexit(cgtls, ppint32(1))
							}
						}
						Xmpfr_nextabove(cgtls, cgbp+64)
					}
					Xmpfr_clear(cgtls, cgbp+64)

					goto cg_3
				cg_3:
					;
					aapc++
				}
				Xmpfr_nextabove(cgtls, cgbp+32)
			}
			Xmpfr_clear(cgtls, cgbp+32)

			goto cg_2
		cg_2:
			;
			aapb++
		}
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+96)

		goto cg_1
	cg_1:
		;
		aapa++
	}
}

func sitest_mpfr_divsp2(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var ccv2 ppbool
	var pp_ /* q at bp+64 */ tnmpfr_t
	var pp_ /* u at bp+0 */ tnmpfr_t
	var pp_ /* v at bp+32 */ tnmpfr_t
	pp_ = ccv2

	/* test to exercise r2 = v1 in mpfr_divsp2 */
	Xmpfr_init2(cgtls, cgbp, ppint64(128))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(128))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(83))

	Xmpfr_set_str(cgtls, cgbp, "286677858044426991425771529092412636160\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "241810647971575979588130185988987264768\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp, "5732952910203749289426944\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(82), ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp+64, cgbp) != 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1398), "mpfr_equal_p (q, u)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+64, cgbp) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
}

// C documentation
//
//	/* Assertion failure in r10769 with --enable-assert --enable-gmp-internals
//	   (same failure in tatan on a similar example). */
func sitest_20160831(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var ccv2, ccv4 ppbool
	var pp_ /* q at bp+64 */ tnmpfr_t
	var pp_ /* u at bp+0 */ tnmpfr_t
	var pp_ /* v at bp+32 */ tnmpfr_t
	pp_, pp_ = ccv2, ccv4

	Xmpfr_inits2(cgtls, ppint64(124), cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "0x40000000000000005\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp, "0xfffffffffffffffecp-134\x00", ppint32(16), ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp+64, cgbp) != 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1418), "mpfr_equal_p (q, u)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+64, cgbp) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(128))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(128))
	Xmpfr_set_str(cgtls, cgbp, "186127091671619245460026015088243485690\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "205987256581218236405412302590110119580\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp, "19217137613667309953639458782352244736\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(124), ppint32(ecMPFR_RNDN))

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp+64, cgbp) != 0)), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1427), "mpfr_equal_p (q, u)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+64, cgbp) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

// C documentation
//
//	/* With r11138, on a 64-bit machine:
//	   div.c:128: MPFR assertion failed: qx >= __gmpfr_emin
//	*/
func sitest_20170104(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaemin tnmpfr_exp_t
	var pp_ /* q at bp+64 */ tnmpfr_t
	var pp_ /* u at bp+0 */ tnmpfr_t
	var pp_ /* v at bp+32 */ tnmpfr_t
	pp_ = aaemin

	aaemin = X__gmpfr_emin
	Xset_emin(cgtls, ppint64(-ppint32(42)))

	Xmpfr_init2(cgtls, cgbp, ppint64(12))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(12))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(11))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.111111111110E-29\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.111111111111E14\x00")
	Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

	Xset_emin(cgtls, aaemin)
}

// C documentation
//
//	/* With r11140, on a 64-bit machine with GMP_CHECK_RANDOMIZE=1484406128:
//	   Consistency error for i = 2577
//	*/
func sitest_20170105(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var ccv2 ppbool
	var pp_ /* t at bp+96 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_ = ccv2

	Xmpfr_init2(cgtls, cgbp, ppint64(138))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(6))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(128))
	Xmpfr_init2(cgtls, cgbp+96, ppint64(128))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.100110111001001000101111010010011101111110111111110001110100000001110111010100111010100011101010110000010100000011100100110101101011000000E-6\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.100100E-2\x00")
	/* up to exponents, x/y is exactly 367625553447399614694201910705139062483,
	   which has 129 bits, thus we are in the round-to-nearest-even case, and since
	   the penultimate bit of x/y is 1, we should round upwards */
	Xmpfr_set_str_binary(cgtls, cgbp+96, "0.10001010010010010000110110010110111111111100011011101010000000000110101000010001011110011011010000111010000000001100101101101010E-3\x00")
	Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp+64, cgbp+96) != 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1474), "mpfr_equal_p (z, t)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+64, cgbp+96) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
}

// C documentation
//
//	/* The real cause of the mpfr_ttanh failure from the non-regression test
//	   added in tests/ttanh.c@11993 was actually due to a bug in mpfr_div, as
//	   this can be seen by comparing the logs of the 3.1 branch and the trunk
//	   r11992 with MPFR_LOG_ALL=1 MPFR_LOG_PREC=50 on this particular test
//	   (this was noticed because adding this test to the 3.1 branch did not
//	   yield a failure like in the trunk, though the mpfr_ttanh code did not
//	   change until r11993). This was the bug actually fixed in r12002.
//	*/
func sitest_20171219(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(176)
	defer cgtls.ppFree(176)

	var ccv2 ppbool
	var pp_ /* t at bp+96 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_ = ccv2

	Xmpfr_inits2(cgtls, ppint64(126), cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.111000000000000111100000000000011110000000000001111000000000000111100000000000011110000000000001111000000000000111100000000000E1\x00")
	/* x = 36347266450315671364380109803814927 / 2^114 */
	Xmpfr_add_ui(cgtls, cgbp+32, cgbp, ppuint64(2), ppint32(ecMPFR_RNDN))
	/* y = 77885641318594292392624080437575695 / 2^114 */
	Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_ui_2exp(cgtls, cgbp+96, ppuint64(3823), ppint64(-ppint32(13)), ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp+64, cgbp+96) != 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1498), "mpfr_equal_p (z, t)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+64, cgbp+96) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+136, cgbp+32, cgbp+64, cgbp+96, iqlibc.ppUintptrFromInt32(0)))
}

// C documentation
//
//	/* exercise mpfr_div2_approx */
func sitest_mpfr_div2_approx(cgtls *iqlibc.ppTLS, aan ppuint64) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var ccv1 ppuint64
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_ = ccv1

	Xmpfr_init2(cgtls, cgbp, ppint64(113))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(113))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(113))
	for {
		ccv1 = aan
		aan--
		if !(ccv1 != 0) {
			break
		}

		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		Xmpfr_div(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))

	}
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
}

// C documentation
//
//	/* bug found in ttan with GMP_CHECK_RANDOMIZE=1514257254 */
func sibug20171218(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var ccv2 ppbool
	var pp_ /* c at bp+32 */ tnmpfr_t
	var pp_ /* s at bp+0 */ tnmpfr_t
	pp_ = ccv2
	Xmpfr_init2(cgtls, cgbp, ppint64(124))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(124))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.1110000111100001111000011110000111100001111000011110000111100001111000011110000111100001111000011110000111100001111000011110E0\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1111000011110000111100001111000011110000111100001111000011110000111100001111000011110000111100001111000011110000111100001111E-1\x00")
	Xmpfr_div(cgtls, cgbp+32, cgbp, cgbp+32, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "-1.111000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\x00")

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp+32, cgbp) != 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1535), "mpfr_equal_p (c, s)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
}

// C documentation
//
//	/* Extended test based on a bug found with flint-arb test suite with a
//	   32-bit ABI: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=888459
//	   Division of the form: (1 - 2^(-pa)) / (1 - 2^(-pb)).
//	   The result is compared to the one obtained by increasing the precision of
//	   the divisor (without changing its value, so that both results should be
//	   equal). For all of these tests, a failure may occur in r12126 only with
//	   pb=GMP_NUMB_BITS and MPFR_RNDN (and some particular values of pa and pc).
//	   This bug was introduced by r9086, where mpfr_div uses mpfr_div_ui when
//	   the divisor has only one limb.
//	*/
func sibug20180126(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(224)
	defer cgtls.ppFree(224)

	var aa_p tnmpfr_srcptr
	var aai, aainex1, aainex2, aaj, aapa, aapb, aapc, aar, aasa, aasb, ccv4 ppint32
	var ccv11, ccv5 ppbool
	var pp_ /* a at bp+0 */ tnmpfr_t
	var pp_ /* b1 at bp+32 */ tnmpfr_t
	var pp_ /* b2 at bp+64 */ tnmpfr_t
	var pp_ /* c1 at bp+96 */ tnmpfr_t
	var pp_ /* c2 at bp+128 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aai, aainex1, aainex2, aaj, aapa, aapb, aapc, aar, aasa, aasb, ccv11, ccv4, ccv5

	aapa = ppint32(100)
	for {
		if !(aapa < ppint32(800)) {
			break
		}
		aai = ppint32(1)
		for {
			if !(aai <= ppint32(4)) {
				break
			}
			aaj = -ppint32(2)
			for {
				if !(aaj <= ppint32(2)) {
					break
				}

				aapb = (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*aai + aaj

				if aapb > aapa {
					goto cg_3
				}

				Xmpfr_inits2(cgtls, ppint64(aapa), cgbp, iqlibc.ppVaList(cgbp+168, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
				Xmpfr_inits2(cgtls, ppint64(aapb), cgbp+64, iqlibc.ppVaList(cgbp+168, iqlibc.ppUintptrFromInt32(0)))

				pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
				Xmpfr_nextbelow(cgtls, cgbp) /* 1 - 2^(-pa) */
				pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
				Xmpfr_nextbelow(cgtls, cgbp+64) /* 1 - 2^(-pb) */
				{
					aa_p = cgbp + 64
					ccv4 = Xmpfr_set4(cgtls, cgbp+32, aa_p, ppint32(ecMPFR_RNDN), (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
				}
				aainex1 = ccv4

				if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex1 == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv5 {
					Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1573), "inex1 == 0\x00")
				}
				pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

				aapc = ppint32(32)
				for {
					if !(aapc <= ppint32(320)) {
						break
					}

					Xmpfr_inits2(cgtls, ppint64(aapc), cgbp+96, iqlibc.ppVaList(cgbp+168, cgbp+128, iqlibc.ppUintptrFromInt32(0)))

					aasa = 0
					for {
						if !(aasa < ppint32(2)) {
							break
						}

						aasb = 0
						for {
							if !(aasb < ppint32(2)) {
								break
							}

							aar = 0
							for {
								if !(aar < ppint32(ecMPFR_RNDF)) {
									break
								}

								if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0)), ppint64(1)) != 0; !ccv11 {
									Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1585), "mpfr_equal_p (b1, b2)\x00")
									if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {
										X__builtin_unreachable(cgtls)
									}
								}
								pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
								aainex1 = Xmpfr_div(cgtls, cgbp+96, cgbp, cgbp+32, aar)
								aainex2 = Xmpfr_div(cgtls, cgbp+128, cgbp, cgbp+64, aar)

								if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+128) != 0) || !(iqlibc.ppBoolInt32(aainex1 > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex1 < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aainex2 > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainex2 < iqlibc.ppInt32FromInt32(0))) {

									Xprintf(cgtls, "Error in bug20180126 for pa=%d pb=%d pc=%d sa=%d sb=%d %s\n\x00", iqlibc.ppVaList(cgbp+168, aapa, aapb, aapc, aasa, aasb, Xmpfr_print_rnd_mode(cgtls, aar)))
									Xprintf(cgtls, "inex1 = %d, c1 = \x00", iqlibc.ppVaList(cgbp+168, aainex1))
									Xmpfr_dump(cgtls, cgbp+96)
									Xprintf(cgtls, "inex2 = %d, c2 = \x00", iqlibc.ppVaList(cgbp+168, aainex2))
									Xmpfr_dump(cgtls, cgbp+128)
									Xexit(cgtls, ppint32(1))
								}

								goto cg_9
							cg_9:
								;
								aar++
							}

							Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))
							Xmpfr_neg(cgtls, cgbp+64, cgbp+64, ppint32(ecMPFR_RNDN))

							goto cg_8
						cg_8:
							;
							aasb++
						} /* sb */

						Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))

						goto cg_7
					cg_7:
						;
						aasa++
					} /* sa */

					Xmpfr_clears(cgtls, cgbp+96, iqlibc.ppVaList(cgbp+168, cgbp+128, iqlibc.ppUintptrFromInt32(0)))

					goto cg_6
				cg_6:
					;
					aapc += ppint32(32)
				} /* pc */

				Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+168, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

				goto cg_3
			cg_3:
				;
				aaj++
			}
			goto cg_2
		cg_2:
			;
			aai++
		}
		goto cg_1
	cg_1:
		;
		aapa += ppint32(11)
	} /* j */
}

func sicoverage(cgtls *iqlibc.ppTLS, aapmax tnmpfr_prec_t) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aainex ppint32
	var aap tnmpfr_prec_t
	var ccv10, ccv11, ccv12, ccv14, ccv15, ccv16, ccv18, ccv19, ccv2, ccv20, ccv21, ccv22, ccv3, ccv4, ccv5, ccv7, ccv8, ccv9 ppbool
	var pp_ /* q at bp+0 */ tnmpfr_t
	var pp_ /* u at bp+32 */ tnmpfr_t
	var pp_ /* v at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aainex, aap, ccv10, ccv11, ccv12, ccv14, ccv15, ccv16, ccv18, ccv19, ccv2, ccv20, ccv21, ccv22, ccv3, ccv4, ccv5, ccv7, ccv8, ccv9

	aap = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aap <= aapmax) {
			break
		}

		Xmpfr_init2(cgtls, cgbp, aap)
		Xmpfr_init2(cgtls, cgbp+32, aap)
		Xmpfr_init2(cgtls, cgbp+64, aap)

		/* exercise case qx < emin */
		Xmpfr_set_ui_2exp(cgtls, cgbp+32, ppuint64(1), X__gmpfr_emin, ppint32(ecMPFR_RNDN))
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(4)), 0, ppint32(ecMPFR_RNDN))

		Xmpfr_clear_flags(cgtls)
		/* u/v = 2^(emin-2), should be rounded to +0 for RNDN */
		aainex = Xmpfr_div(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))

		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1639), "inex < 0\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) == 0)), ppint64(1)) != 0; !ccv3 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1640), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q))))->_mpfr_sign))) < 0) == 0\x00")
		}
		pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1641), "((int) (__gmpfr_flags & 1))\x00")
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		Xmpfr_clear_flags(cgtls)
		/* u/v = 2^(emin-2), should be rounded to 2^(emin-1) for RNDU */
		aainex = Xmpfr_div(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))

		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1646), "inex > 0\x00")
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), X__gmpfr_emin-ppint64(1)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv7 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1647), "mpfr_cmp_ui_2exp (q, 1, ((mpfr_exp_t) __gmpfr_emin) - 1) == 0\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), X__gmpfr_emin-ppint64(1)) == 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv8 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1648), "((int) (__gmpfr_flags & 1))\x00")
		}
		pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		Xmpfr_clear_flags(cgtls)
		/* u/v = 2^(emin-2), should be rounded to +0 for RNDZ */
		aainex = Xmpfr_div(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDZ))

		if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv9 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1653), "inex < 0\x00")
		}
		pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) == 0)), ppint64(1)) != 0; !ccv10 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1654), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q))))->_mpfr_sign))) < 0) == 0\x00")
		}
		pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv11 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1655), "((int) (__gmpfr_flags & 1))\x00")
		}
		pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if aap == ppint64(1) {
			goto ppend_of_loop
		}

		Xmpfr_set_ui_2exp(cgtls, cgbp+32, ppuint64(1), X__gmpfr_emin, ppint32(ecMPFR_RNDN))
		Xmpfr_nextbelow(cgtls, cgbp+32) /* u = (1-2^(-p))*2^emin */
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp+64, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))

		Xmpfr_clear_flags(cgtls)
		/* u/v = (1-2^(-p))*2^(emin-1), will round to 2^(emin-1) for RNDN */
		aainex = Xmpfr_div(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))

		if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv12 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1667), "inex > 0\x00")
		}
		pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv14 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), X__gmpfr_emin-ppint64(1)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv14 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1668), "mpfr_cmp_ui_2exp (q, 1, ((mpfr_exp_t) __gmpfr_emin) - 1) == 0\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), X__gmpfr_emin-ppint64(1)) == 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv14 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv15 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv15 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1669), "((int) (__gmpfr_flags & 1))\x00")
		}
		pp_ = ccv15 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		Xmpfr_clear_flags(cgtls)
		/* u/v should round to 2^(emin-1) for RNDU */
		aainex = Xmpfr_div(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDU))

		if ccv16 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex > iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv16 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1674), "inex > 0\x00")
		}
		pp_ = ccv16 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv18 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), X__gmpfr_emin-ppint64(1)) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv18 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1675), "mpfr_cmp_ui_2exp (q, 1, ((mpfr_exp_t) __gmpfr_emin) - 1) == 0\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmp_ui_2exp(cgtls, cgbp, ppuint64(1), X__gmpfr_emin-ppint64(1)) == 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv18 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv19 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv19 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1676), "((int) (__gmpfr_flags & 1))\x00")
		}
		pp_ = ccv19 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		Xmpfr_clear_flags(cgtls)
		/* u/v should round to +0 for RNDZ */
		aainex = Xmpfr_div(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDZ))

		if ccv20 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex < iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv20 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1681), "inex < 0\x00")
		}
		pp_ = ccv20 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv21 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) && iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign < 0) == 0)), ppint64(1)) != 0; !ccv21 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1682), "(((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_exp == (0 - ((mpfr_exp_t) (((mpfr_uexp_t) -1) >> 1)))) && ((0 ? (((((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q))))->_mpfr_sign)) : (((((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q))))->_mpfr_sign))) < 0) == 0\x00")
		}
		pp_ = ccv21 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv22 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv22 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1683), "((int) (__gmpfr_flags & 1))\x00")
		}
		pp_ = ccv22 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		goto ppend_of_loop
	ppend_of_loop:
		;
		Xmpfr_clear(cgtls, cgbp)
		Xmpfr_clear(cgtls, cgbp+32)
		Xmpfr_clear(cgtls, cgbp+64)

		goto cg_1
	cg_1:
		;
		aap++
	}
}

// C documentation
//
//	/* coverage for case usize >= n + n in Mulders' algorithm */
func sicoverage2(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(160)
	defer cgtls.ppFree(160)

	var aainex, aainex2 ppint32
	var aap tnmpfr_prec_t
	var ccv10, ccv12, ccv3, ccv5, ccv6, ccv7, ccv8 ppbool
	var pp_ /* q at bp+0 */ tnmpfr_t
	var pp_ /* t at bp+96 */ tnmpfr_t
	var pp_ /* u at bp+32 */ tnmpfr_t
	var pp_ /* v at bp+64 */ tnmpfr_t
	var pp_ /* w at bp+128 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aainex, aainex2, aap, ccv10, ccv12, ccv3, ccv5, ccv6, ccv7, ccv8

	aap = ppint64(iqlibc.ppInt32FromInt32(mvMPFR_DIV_THRESHOLD) * (iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS) - iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS)))
	Xmpfr_init2(cgtls, cgbp, aap)
	Xmpfr_init2(cgtls, cgbp+32, ppint64(2)*aap+ppint64(iqlibc.ppInt32FromInt32(3)*(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))))
	Xmpfr_init2(cgtls, cgbp+64, aap)
	for cgcond := true; cgcond; cgcond = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp+32, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	}
	for cgcond := true; cgcond; cgcond = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+64)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(0)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1)) {
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp+64, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
	}
	aainex = Xmpfr_div(cgtls, cgbp, cgbp+32, cgbp+64, ppint32(ecMPFR_RNDN))
	Xmpfr_init2(cgtls, cgbp+96, (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_prec)
	Xmpfr_init2(cgtls, cgbp+128, (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_prec)
	aainex2 = Xmpfr_mul(cgtls, cgbp+96, cgbp, cgbp+64, ppint32(ecMPFR_RNDN))

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex2 == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1710), "inex2 == 0\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if aainex == 0 { /* check q*v = u */

		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+96) != 0)), ppint64(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1712), "mpfr_equal_p (u, t)\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+96) != 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	} else {

		if aainex > 0 {
			Xmpfr_nextbelow(cgtls, cgbp)
		} else {
			Xmpfr_nextabove(cgtls, cgbp)
		}
		aainex2 = Xmpfr_mul(cgtls, cgbp+128, cgbp, cgbp+64, ppint32(ecMPFR_RNDN))

		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex2 == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1720), "inex2 == 0\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aainex2 = Xmpfr_sub(cgtls, cgbp+96, cgbp+96, cgbp+32, ppint32(ecMPFR_RNDN))

		if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex2 == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv7 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1722), "inex2 == 0\x00")
		}
		pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aainex2 = Xmpfr_sub(cgtls, cgbp+128, cgbp+128, cgbp+32, ppint32(ecMPFR_RNDN))

		if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex2 == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv8 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1724), "inex2 == 0\x00")
		}
		pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_cmpabs(cgtls, cgbp+96, cgbp+128) <= iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv10 {
			Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1725), "mpfr_cmpabs (t, w) <= 0\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_cmpabs(cgtls, cgbp+96, cgbp+128) <= 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if Xmpfr_cmpabs(cgtls, cgbp+96, cgbp+128) == 0 { /* even rule: significand of q should now
			   be odd */

			if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_min_prec(cgtls, cgbp) == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)), ppint64(1)) != 0; !ccv12 {
				Xmpfr_assert_fail(cgtls, "tdiv.c\x00", ppint32(1728), "mpfr_min_prec (q) == (0 ? (((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_prec) : (((mpfr_srcptr) (0 ? (q) : (mpfr_srcptr) (q)))->_mpfr_prec))\x00")
				if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_min_prec(cgtls, cgbp) == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec) {
					X__builtin_unreachable(cgtls)
				}
			}
			pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		}
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
	Xmpfr_clear(cgtls, cgbp+64)
	Xmpfr_clear(cgtls, cgbp+96)
	Xmpfr_clear(cgtls, cgbp+128)
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	Xtests_start_mpfr(cgtls)

	sicoverage(cgtls, ppint64(1024))
	sicoverage2(cgtls)
	sibug20180126(cgtls)
	sibug20171218(cgtls)
	sitestall_rndf(cgtls, ppint64(9))
	sitest_20170105(cgtls)
	sicheck_inexact(cgtls)
	sicheck_hard(cgtls)
	sicheck_special(cgtls)
	sicheck_lowr(cgtls)
	sicheck_float(cgtls) /* checks single precision */
	sicheck_double(cgtls)
	sicheck_convergence(cgtls)
	sicheck_64(cgtls)

	sicheck4(cgtls, "4.0\x00", "4.503599627370496e15\x00", ppint32(ecMPFR_RNDZ), ppint32(62), "0.10000000000000000000000000000000000000000000000000000000000000E-49\x00")
	sicheck4(cgtls, "1.0\x00", "2.10263340267725788209e+187\x00", ppint32(ecMPFR_RNDU), ppint32(65), "0.11010011111001101011111001100111110100000001101001111100111000000E-622\x00")
	sicheck4(cgtls, "2.44394909079968374564e-150\x00", "2.10263340267725788209e+187\x00", ppint32(ecMPFR_RNDU), ppint32(65), "0.11010011111001101011111001100111110100000001101001111100111000000E-1119\x00")

	siconsistency(cgtls)
	sitest_20070603(cgtls)
	sitest_20070628(cgtls)
	sitest_20151023(cgtls)
	sitest_20160831(cgtls)
	sitest_20170104(cgtls)
	sitest_20171219(cgtls)
	sitest_generic(cgtls, ppint64(mvMPFR_PREC_MIN), ppint64(800), ppuint32(50))
	sitest_bad(cgtls)
	sitest_extreme(cgtls)
	sitest_mpfr_divsp2(cgtls)
	sitest_mpfr_div2_approx(cgtls, ppuint64(1000000))

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_ctzl(*iqlibc.ppTLS, ppuint64) ppint32

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___builtin_unreachable(*iqlibc.ppTLS)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuint64

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _mpfr_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_can_round(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32, ppint32, ppint64) ppint32

func _mpfr_check(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp3(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_cmp_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64) ppint32

func _mpfr_cmpabs(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_div(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_div_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_div_si(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64, ppint32) ppint32

func _mpfr_div_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_fits_slong_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_fits_ulong_p(*iqlibc.ppTLS, ppuintptr, ppint32) ppint32

func _mpfr_get_si(*iqlibc.ppTLS, ppuintptr, ppint32) ppint64

func _mpfr_get_ui(*iqlibc.ppTLS, ppuintptr, ppint32) ppuint64

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_inits(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_inits2(*iqlibc.ppTLS, ppint64, ppuintptr, ppuintptr)

func _mpfr_integer_p(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_min_prec(*iqlibc.ppTLS, ppuintptr) ppint64

func _mpfr_mul(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_mul_2si(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint64, ppint32) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttoinf(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttozero(*iqlibc.ppTLS, ppuintptr)

func _mpfr_prec_round(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32) ppint32

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_random2(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppuintptr)

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppuint8

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_exp(*iqlibc.ppTLS, ppuintptr, ppint64) ppint32

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_setmax(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_setmin(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_sgn(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_si_div(*iqlibc.ppTLS, ppuintptr, ppint64, ppuintptr, ppint32) ppint32

func _mpfr_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_ui_div(*iqlibc.ppTLS, ppuintptr, ppuint64, ppuintptr, ppint32) ppint32

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _putchar(*iqlibc.ppTLS, ppint32) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

var _stdout ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

// Code generated for linux/arm64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IEEE_QUAD_LITTLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/arm64 -UHAVE_NEARBYINT -c -o tget_str.o.go tget_str.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IEEE_QUAD_LITTLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvITER = 1000
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMAX_DIGITS = 100
const mvMAX_OPREC = 100
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1311
const mvMPFR_AI_THRESHOLD3 = 19661
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 25
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 100
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 25000
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 20
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 30000
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 20
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "default"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_L = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__AARCH64EL__ = 1
const mv__AARCH64_CMODEL_SMALL__ = 1
const mv__ARM_64BIT_STATE = 1
const mv__ARM_ALIGN_MAX_PWR = 28
const mv__ARM_ALIGN_MAX_STACK_PWR = 16
const mv__ARM_ARCH = 8
const mv__ARM_ARCH_8A = 1
const mv__ARM_ARCH_ISA_A64 = 1
const mv__ARM_ARCH_PROFILE = 65
const mv__ARM_FEATURE_CLZ = 1
const mv__ARM_FEATURE_FMA = 1
const mv__ARM_FEATURE_IDIV = 1
const mv__ARM_FEATURE_NUMERIC_MAXMIN = 1
const mv__ARM_FEATURE_UNALIGNED = 1
const mv__ARM_FP = 14
const mv__ARM_FP16_ARGS = 1
const mv__ARM_FP16_FORMAT_IEEE = 1
const mv__ARM_NEON = 1
const mv__ARM_PCS_AAPCS64 = 1
const mv__ARM_SIZEOF_MINIMAL_ENUM = 4
const mv__ARM_SIZEOF_WCHAR_T = 4
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 36
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 2
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_C99__ = 0
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 256
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "aarch64-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 36
const mv__LDBL_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__LDBL_DIG__ = 33
const mv__LDBL_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 113
const mv__LDBL_MAX_10_EXP__ = 4932
const mv__LDBL_MAX_EXP__ = 16384
const mv__LDBL_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LDBL_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__LDBL_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0xffffffff
const mv__WCHAR_MIN__ = 0
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__aarch64__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppuint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppuint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

func sicheck3(cgtls *iqlibc.ppTLS, aad ppuintptr, aarnd tnmpfr_rnd_t, aares ppuintptr) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aastr ppuintptr
	var pp_ /* e at bp+32 */ tnmpfr_exp_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_ = aastr

	Xmpfr_init2(cgtls, cgbp, ppint64(53))
	Xmpfr_set_str(cgtls, cgbp, aad, ppint32(10), aarnd)
	aastr = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(5), cgbp, aarnd)
	if Xstrcmp(cgtls, aastr, aares) != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str for x=%s\n\x00", iqlibc.ppVaList(cgbp+48, aad))
		Xprintf(cgtls, "got %s instead of %s\n\x00", iqlibc.ppVaList(cgbp+48, aastr, aares))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_free_str(cgtls, aastr)
}

func sicheck_small(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aap tnmpfr_prec_t
	var aas ppuintptr
	var pp_ /* e at bp+32 */ tnmpfr_exp_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_ = aap, aas

	Xmpfr_init(cgtls, cgbp)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(20))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_nexttozero(cgtls, cgbp)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(4), ppuint64(2), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "20\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(1) {

		Xprintf(cgtls, "Error in mpfr_get_str: 2- rounded up with 2 digits in base 4\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* check n_digits=0 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(5))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(17)), 0, ppint32(ecMPFR_RNDN))
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(3), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(36), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(62), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(64))
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(63), ppint32(ecMPFR_RNDN)) /* x = -2^(-63) */
	Xmpfr_add_ui(cgtls, cgbp, cgbp, ppuint64(1), ppint32(ecMPFR_RNDN))   /* x = 1 - 2^(-63) */
	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(32), ppint32(ecMPFR_RNDN)) /* x = 2^32 - 2^(-31) */
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(3), ppuint64(21), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "102002022201221111211\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(21) {

		Xprintf(cgtls, "Error in mpfr_get_str: 2^32-2^(-31) rounded up with 21 digits in base 3\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(3), ppuint64(20), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "10200202220122111122\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(21) {

		Xprintf(cgtls, "Error in mpfr_get_str: 2^32-2^(-31) rounded up with 20 digits in base 3\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* check corner case ret!=0, j0!=0 in mpfr_get_str_aux */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(100))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1001011111010001101110010101010101111001010111111101101101100110100011110110000101110110001011110000E-9\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(3), ppuint64(2), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "22\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(6)) {

		Xprintf(cgtls, "Error in mpfr_get_str: 100-bit number rounded up with 2 digits in base 3\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* check corner case exact=0 in mpfr_get_str_aux */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(100))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1001001111101101111000101000110111111010101100000110010001111111011001101011101100001100110000000000E8\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDZ))
	if Xstrcmp(cgtls, aas, "14\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(3) {

		Xprintf(cgtls, "Error in mpfr_get_str: 100-bit number rounded to zero with 2 digits in base 10\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	aap = ppint64(4)
	for {
		if !(aap <= ppint64(200)) {
			break
		}

		Xmpfr_set_prec(cgtls, cgbp, aap)
		Xmpfr_set_str(cgtls, cgbp, "6.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))

		aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(6), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
		if Xstrcmp(cgtls, aas, "10\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(2) {

			Xprintf(cgtls, "Error in mpfr_get_str: 6.5 rounded to nearest with 2 digits in base 6\n\x00", 0)
			Xexit(cgtls, ppint32(1))
		}
		Xmpfr_free_str(cgtls, aas)

		Xmpfr_nexttoinf(cgtls, cgbp)
		aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(6), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
		if Xstrcmp(cgtls, aas, "11\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(2) {

			Xprintf(cgtls, "Error in mpfr_get_str: 6.5+ rounded to nearest with 2 digits in base 6\ngot %se%d instead of 11e2\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
			Xexit(cgtls, ppint32(1))
		}
		Xmpfr_free_str(cgtls, aas)

		Xmpfr_set_str(cgtls, cgbp, "6.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
		Xmpfr_nexttozero(cgtls, cgbp)
		aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(6), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
		if Xstrcmp(cgtls, aas, "10\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(2) {

			Xprintf(cgtls, "Error in mpfr_get_str: 6.5- rounded to nearest with 2 digits in base 6\n\x00", 0)
			Xexit(cgtls, ppint32(1))
		}
		Xmpfr_free_str(cgtls, aas)

		goto cg_1
	cg_1:
		;
		aap++
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(3))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(7)), 0, ppint32(ecMPFR_RNDN))
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(2), ppuint64(2), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "10\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(4) {

		Xprintf(cgtls, "Error in mpfr_get_str: 7 rounded up with 2 bits should give 0.10e3 instead of 0.%s*2^%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* problem found by Fabrice Rouillier */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(63))
	Xmpfr_set_str(cgtls, cgbp, "5e14\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(18), cgbp, ppint32(ecMPFR_RNDU))
	Xmpfr_free_str(cgtls, aas)

	/* bug found by Johan Vervloet */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(6))
	Xmpfr_set_str(cgtls, cgbp, "688.0\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(2), ppuint64(4), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "1011\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(10) {

		Xprintf(cgtls, "Error in mpfr_get_str: 688 printed up to 4 bits should give 1.011e9\ninstead of \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(4), cgbp, ppint32(ecMPFR_RNDU))
		Xputs(cgtls, "\x00")
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(38))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.0001110111110100011010100010010100110e-6\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(8), ppuint64(10), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "1073721522\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(1)) {

		Xprintf(cgtls, "Error in mpfr_get_str (3): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11010111011101100010000100010101110001000000010111001E454\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(19), ppuint64(12), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "b1cgfa4gha0h\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(107) {

		Xprintf(cgtls, "Error in mpfr_get_str (4): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(145))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.1000110011000001011000010101101010110110101100101110100011111100011110011001001001010000100001000011000011000000010111011001000111101001110100110e6\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(4), ppuint64(53), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "-20303001120111222312230232203330132121021100201003003\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(3) {

		Xprintf(cgtls, "Error in mpfr_get_str (5): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(45))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.00100111010110010001011001110111010001010010010\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(32), ppuint64(9), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "-4tchctq54\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str (6): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* worst case found by Vincent Lefe`vre */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_str_binary(cgtls, cgbp, "10011110111100000000001011011110101100010000011011111E164\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(17), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "13076622631878654\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(66) {

		Xprintf(cgtls, "Error in mpfr_get_str (7): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "10000001001001001100011101010011011011111000011000100E93\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "46\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(44) {

		Xprintf(cgtls, "Error in mpfr_get_str (8): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "10010001111100000111001111010101001010000010111010101E55\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "19\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(33) {

		Xprintf(cgtls, "Error in mpfr_get_str (9): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "11011001010010111110010101101100111110111000010110110E44\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(3), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "135\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(30) {

		Xprintf(cgtls, "Error in mpfr_get_str (10): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "11101111101000001011100001111000011111101111011001100E72\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(4), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "3981\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(38) {

		Xprintf(cgtls, "Error in mpfr_get_str (11): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "10011001001100100010111100001101110101001001111110000E46\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(5), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "37930\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(30) {

		Xprintf(cgtls, "Error in mpfr_get_str (12): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "10001100110111001011011110011011011101100011010001011E-72\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(6), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "104950\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(5)) {

		Xprintf(cgtls, "Error in mpfr_get_str (13): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "10100100001011001000011001101101000110100110000010111E89\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(7), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "3575392\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(43) {

		Xprintf(cgtls, "Error in mpfr_get_str (14): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "11000011011110110010100110001010000001010011001011001E-73\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(8), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "72822386\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(6)) {

		Xprintf(cgtls, "Error in mpfr_get_str (15): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "10101010001101000111001100001000100011100010010001010E78\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(9), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "180992873\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(40) {

		Xprintf(cgtls, "Error in mpfr_get_str (16): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "10110111001000100000001101111001100101101110011011101E91\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(10), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "1595312255\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(44) {

		Xprintf(cgtls, "Error in mpfr_get_str (17): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "10011101010111101111000100111011101011110100110110101E93\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(11), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "54835744350\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(44) {

		Xprintf(cgtls, "Error in mpfr_get_str (18): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "10011101010111101111000100111011101011110100110110101E92\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(12), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "274178721752\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(44) {

		Xprintf(cgtls, "Error in mpfr_get_str (19): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "10011101010111101111000100111011101011110100110110101E91\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(13), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "1370893608762\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(44) {

		Xprintf(cgtls, "Error in mpfr_get_str (20): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "10010011010110011100010010100101100011101000011111111E92\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(14), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "25672105101864\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(44) {

		Xprintf(cgtls, "Error in mpfr_get_str (21): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "100110111110110001000101110100100101101000011111001E87\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(15), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "212231308858721\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(42) {

		Xprintf(cgtls, "Error in mpfr_get_str (22): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "10111010110000111000101100101111001011011100101001111E-128\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(15), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "193109287087290\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(22)) {

		Xprintf(cgtls, "Error in mpfr_get_str (22b): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "10001101101011010001111110000111010111010000110101010E80\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(16), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "6026241735727920\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(40) {

		Xprintf(cgtls, "Error in mpfr_get_str (23): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "100010001011101001110101000110011001001000110001001E-81\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(17), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "49741483709103481\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(9)) {

		Xprintf(cgtls, "Error in mpfr_get_str (24): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "11000100001001001110111010011001111001001010110101111E-101\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(7), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "2722049\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(14)) {

		Xprintf(cgtls, "Error in mpfr_get_str (25): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "11111001010011100101000001111111110001001001110110001E-135\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(8), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "20138772\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(24)) {

		Xprintf(cgtls, "Error in mpfr_get_str (26): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "11111001010011100101000001111111110001001001110110001E-136\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(9), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "100693858\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(24)) {

		Xprintf(cgtls, "Error in mpfr_get_str (27): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "10001000001110010110001011111011111011011010000110001E-110\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(14), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "36923634350619\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(17)) {

		Xprintf(cgtls, "Error in mpfr_get_str (28): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "11001100010111000111100010000110011101110001000101111E-87\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(16), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "4646636036100804\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(10)) {

		Xprintf(cgtls, "Error in mpfr_get_str (29): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "10011111001111110100001001010111111011010101111111000E-99\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(17), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "88399901882446712\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(14)) {

		Xprintf(cgtls, "Error in mpfr_get_str (30): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 8116315218207718*2^(-293) ~ 0.5100000000000000000015*10^(-72) */
	Xmpfr_set_str_binary(cgtls, cgbp, "11100110101011011111011100101011101110110001111100110E-293\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "52\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(72)) {

		Xprintf(cgtls, "Error in mpfr_get_str (31u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "51\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(72)) {

		Xprintf(cgtls, "Error in mpfr_get_str (31d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 6712731423444934*2^536 ~ .151000000000000000000067*10^178 */
	Xmpfr_set_str_binary(cgtls, cgbp, "10111110110010011000110010011111101111000111111000110E536\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(3), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "152\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(178) {

		Xprintf(cgtls, "Error in mpfr_get_str (32u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(3), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "151\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(178) {

		Xprintf(cgtls, "Error in mpfr_get_str (32d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 3356365711722467*2^540 ~ .120800000000000000000054*10^179 */
	Xmpfr_set_str_binary(cgtls, cgbp, "1011111011001001100011001001111110111100011111100011E540\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(4), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "1209\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(179) {

		Xprintf(cgtls, "Error in mpfr_get_str (33u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(4), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "1208\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(179) {

		Xprintf(cgtls, "Error in mpfr_get_str (33d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 6475049196144587*2^100 ~ .8208099999999999999999988*10^46 */
	Xmpfr_set_str_binary(cgtls, cgbp, "10111000000010000010111011111001111010100011111001011E100\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(5), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "82081\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(46) {

		Xprintf(cgtls, "Error in mpfr_get_str (34u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(5), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "82080\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(46) {

		Xprintf(cgtls, "Error in mpfr_get_str (34d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 6722280709661868*2^364 ~ .25260100000000000000000012*10^126 */
	Xmpfr_set_str_binary(cgtls, cgbp, "10111111000011110000011110001110001111010010010101100E364\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(6), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "252602\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(126) {

		Xprintf(cgtls, "Error in mpfr_get_str (35u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(6), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "252601\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(126) {

		Xprintf(cgtls, "Error in mpfr_get_str (35d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 5381065484265332*2^(-455) ~ .578389299999999999999999982*10^(-121) */
	Xmpfr_set_str_binary(cgtls, cgbp, "10011000111100000110011110000101100111110011101110100E-455\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(7), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "5783893\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(121)) {

		Xprintf(cgtls, "Error in mpfr_get_str (36u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(7), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "5783892\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(121)) {

		Xprintf(cgtls, "Error in mpfr_get_str (36d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 8369123604277281*2^(-852) ~ .27869147000000000000000000056*10^(-240) */
	Xmpfr_set_str_binary(cgtls, cgbp, "11101101110111010110001101111100000111010100000100001E-852\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(8), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "27869148\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(240)) {

		Xprintf(cgtls, "Error in mpfr_get_str (37u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(8), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "27869147\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(240)) {

		Xprintf(cgtls, "Error in mpfr_get_str (37d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 7976538478610756*2^377 ~ .245540326999999999999999999982*10^130 */
	Xmpfr_set_str_binary(cgtls, cgbp, "11100010101101001111010010110100011100000100101000100E377\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(9), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "245540327\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(130) {

		Xprintf(cgtls, "Error in mpfr_get_str (38u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(9), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "245540326\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(130) {

		Xprintf(cgtls, "Error in mpfr_get_str (38d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 8942832835564782*2^(-382) ~ .9078555839000000000000000000038*10^(-99) */
	Xmpfr_set_str_binary(cgtls, cgbp, "11111110001010111010110000110011100110001010011101110E-382\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(10), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "9078555840\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(99)) {

		Xprintf(cgtls, "Error in mpfr_get_str (39u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(10), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "9078555839\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(99)) {

		Xprintf(cgtls, "Error in mpfr_get_str (39d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 4471416417782391*2^(-380) ~ .18157111678000000000000000000077*10^(-98) */
	Xmpfr_set_str_binary(cgtls, cgbp, "1111111000101011101011000011001110011000101001110111E-380\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(11), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "18157111679\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(98)) {

		Xprintf(cgtls, "Error in mpfr_get_str (40u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(11), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "18157111678\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(98)) {

		Xprintf(cgtls, "Error in mpfr_get_str (40d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 7225450889282194*2^711 ~ .778380362292999999999999999999971*10^230 */
	Xmpfr_set_str_binary(cgtls, cgbp, "11001101010111000001001100001100110010000001010010010E711\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(12), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "778380362293\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(230) {

		Xprintf(cgtls, "Error in mpfr_get_str (41u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(12), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "778380362292\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(230) {

		Xprintf(cgtls, "Error in mpfr_get_str (41d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 3612725444641097*2^713 ~ .1556760724585999999999999999999942*10^231 */
	Xmpfr_set_str_binary(cgtls, cgbp, "1100110101011100000100110000110011001000000101001001E713\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(13), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "1556760724586\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(231) {

		Xprintf(cgtls, "Error in mpfr_get_str (42u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(13), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "1556760724585\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(231) {

		Xprintf(cgtls, "Error in mpfr_get_str (42d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 6965949469487146*2^(-248) ~ .15400733123779000000000000000000016*10^(-58) */
	Xmpfr_set_str_binary(cgtls, cgbp, "11000101111110111111001111111101001101111000000101010E-248\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(14), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "15400733123780\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(58)) {

		Xprintf(cgtls, "Error in mpfr_get_str (43u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(14), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "15400733123779\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(58)) {

		Xprintf(cgtls, "Error in mpfr_get_str (43d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 3482974734743573*2^(-244) ~ .12320586499023200000000000000000013*10^(-57) */
	Xmpfr_set_str_binary(cgtls, cgbp, "1100010111111011111100111111110100110111100000010101E-244\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(15), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "123205864990233\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(57)) {

		Xprintf(cgtls, "Error in mpfr_get_str (44u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(15), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "123205864990232\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(57)) {

		Xprintf(cgtls, "Error in mpfr_get_str (44d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 7542952370752766*2^(-919) ~ .170206189963739699999999999999999974*10^(-260) */
	Xmpfr_set_str_binary(cgtls, cgbp, "11010110011000100011001110100100111011100110011111110E-919\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(16), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "1702061899637397\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(260)) {

		Xprintf(cgtls, "Error in mpfr_get_str (45u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(16), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "1702061899637396\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(-ppint32(260)) {

		Xprintf(cgtls, "Error in mpfr_get_str (45d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* 5592117679628511*2^165 ~ .26153245263757307000000000000000000074*10^66 */
	Xmpfr_set_str_binary(cgtls, cgbp, "10011110111100000000001011011110101100010000011011111E165\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(17), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "26153245263757308\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(66) {

		Xprintf(cgtls, "Error in mpfr_get_str (46u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(17), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "26153245263757307\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(66) {

		Xprintf(cgtls, "Error in mpfr_get_str (46d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "11010010110111100001011010000110010000100001011011101E1223\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(17), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "10716284017294180\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(385) {

		Xprintf(cgtls, "Error in mpfr_get_str (47n): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(18), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "107162840172941805\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(385) {

		Xprintf(cgtls, "Error in mpfr_get_str (47u): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(18), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "107162840172941804\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(385) {

		Xprintf(cgtls, "Error in mpfr_get_str (47d): s=%s e=%d\n\x00", iqlibc.ppVaList(cgbp+48, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_str_binary(cgtls, cgbp, "11111101111011000001010100001101101000010010001111E122620\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(17), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "22183435284042374\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(36928) {

		Xprintf(cgtls, "Error in mpfr_get_str (48n): s=%s e=%ld\n\x00", iqlibc.ppVaList(cgbp+48, aas, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(18), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "221834352840423736\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(36928) {

		Xprintf(cgtls, "Error in mpfr_get_str (48u): s=%s e=%ld\n\x00", iqlibc.ppVaList(cgbp+48, aas, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(18), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "221834352840423735\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(36928) {

		Xprintf(cgtls, "Error in mpfr_get_str (48d): s=%s e=%ld\n\x00", iqlibc.ppVaList(cgbp+48, aas, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(45))
	Xmpfr_set_str_binary(cgtls, cgbp, "1E45\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(32), ppuint64(9), cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(7))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1010101E10\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDU))
	Xmpfr_free_str(cgtls, aas)

	/* checks rounding of negative numbers */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(7))
	Xmpfr_set_str(cgtls, cgbp, "-11.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDD))
	if Xstrcmp(cgtls, aas, "-12\x00") != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str for x=-11.5 and rnd=MPFR_RNDD\ngot %s instead of -12\n\x00", iqlibc.ppVaList(cgbp+48, aas))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDU))
	if Xstrcmp(cgtls, aas, "-11\x00") != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str for x=-11.5 and rnd=MPFR_RNDU\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	/* bug found by Jean-Pierre Merlet, produced error in mpfr_get_str */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(128))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.10111001100110011001100110011001100110011001100110011001100110011001100110011001100110011001100110011001100110011001100110011010E3\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDU))
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(381))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.111111111111111111111111111111111111111111111111111111111111111111101110110000100110011101101101001010111000101111000100100011110101010110101110100000010100001000110100000100011111001000010010000010001010111001011110000001110010111101100001111000101101100000010110000101100100000101010110010110001010100111001111100011100101100000100100111001100010010011110011011010110000001000010\x00")
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), ppuint64(0), cgbp, ppint32(ecMPFR_RNDD))
	if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str for x=0.999999..., exponent is %d instead of 0\n\x00", iqlibc.ppVaList(cgbp+48, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(5))
	Xmpfr_set_str_binary(cgtls, cgbp, "1101.1\x00") /* 13.5, or (16)_7 + 1/2 */
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(7), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
	/* we are in the tie case: both surrounding numbers are (16)_7 and
	   (20)_7: since (16)_7 = 13 is odd and (20)_7 = 14 is even,
	   we should have s = "20" and e = 2 */
	if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(2) || Xstrcmp(cgtls, aas, "20\x00") != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str for x=13.5, base 7\n\x00", 0)
		Xprintf(cgtls, "Expected s=20, e=2, got s=%s, e=%ld\n\x00", iqlibc.ppVaList(cgbp+48, aas, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	/* try the same example, with input just below or above 13.5 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(1000))
	Xmpfr_set_str_binary(cgtls, cgbp, "1101.1\x00")
	Xmpfr_nextabove(cgtls, cgbp)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(7), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
	if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(2) || Xstrcmp(cgtls, aas, "20\x00") != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str for x=13.5+tiny, base 7\n\x00", 0)
		Xprintf(cgtls, "Expected s=20, e=2, got s=%s, e=%ld\n\x00", iqlibc.ppVaList(cgbp+48, aas, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "1101.1\x00")
	Xmpfr_nextbelow(cgtls, cgbp)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(7), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
	if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(2) || Xstrcmp(cgtls, aas, "16\x00") != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str for x=13.5-tiny, base 7\n\x00", 0)
		Xprintf(cgtls, "Expected s=16, e=2, got s=%s, e=%ld\n\x00", iqlibc.ppVaList(cgbp+48, aas, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_prec(cgtls, cgbp, ppint64(7))
	Xmpfr_set_str_binary(cgtls, cgbp, "110000.1\x00") /* 48.5, or (66)_7 + 1/2 */
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(7), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
	/* we are in the tie case: both surrounding numbers are (66)_7 and
	   (100)_7: since (66)_7 = 48 is even and (100)_7 is odd,
	   we should hase s = "66" and e = 2 */
	if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(2) || Xstrcmp(cgtls, aas, "66\x00") != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str for x=48.5, base 7\n\x00", 0)
		Xprintf(cgtls, "Expected s=66, e=2, got s=%s, e=%ld\n\x00", iqlibc.ppVaList(cgbp+48, aas, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	/* try the same example, with input just below or above 48.5 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(1000))
	Xmpfr_set_str_binary(cgtls, cgbp, "110000.1\x00")
	Xmpfr_nextabove(cgtls, cgbp)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(7), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
	if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(3) || Xstrcmp(cgtls, aas, "10\x00") != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str for x=48.5+tiny, base 7\n\x00", 0)
		Xprintf(cgtls, "Expected s=10, e=3, got s=%s, e=%ld\n\x00", iqlibc.ppVaList(cgbp+48, aas, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_set_str_binary(cgtls, cgbp, "110000.1\x00")
	Xmpfr_nextbelow(cgtls, cgbp)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(7), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
	if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) != ppint64(2) || Xstrcmp(cgtls, aas, "66\x00") != 0 {

		Xprintf(cgtls, "Error in mpfr_get_str for x=48.5-tiny, base 7\n\x00", 0)
		Xprintf(cgtls, "Expected s=66, e=2, got s=%s, e=%ld\n\x00", iqlibc.ppVaList(cgbp+48, aas, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* bugs found by Alain Delplanque */
func sicheck_large(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(1088)
	defer cgtls.ppFree(1088)

	var aa_p tnmpfr_ptr
	var aas ppuintptr
	var ccv1 ppint32
	var pp_ /* e at bp+1048 */ tnmpfr_exp_t
	var pp_ /* s1 at bp+35 */ [7]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* xm at bp+42 */ [1001]ppuint8
	pp_, pp_, pp_ = aa_p, aas, ccv1
	*(*[1001]ppuint8)(iqunsafe.ppPointer(cgbp + 42)) = [1001]ppuint8{
		0:   ppuint8('1'),
		1:   ppuint8('1'),
		2:   ppuint8('9'),
		3:   ppuint8('1'),
		4:   ppuint8('3'),
		5:   ppuint8('2'),
		6:   ppuint8('9'),
		7:   ppuint8('3'),
		8:   ppuint8('7'),
		9:   ppuint8('3'),
		10:  ppuint8('5'),
		11:  ppuint8('8'),
		12:  ppuint8('4'),
		13:  ppuint8('4'),
		14:  ppuint8('5'),
		15:  ppuint8('4'),
		16:  ppuint8('9'),
		17:  ppuint8('0'),
		18:  ppuint8('2'),
		19:  ppuint8('9'),
		20:  ppuint8('6'),
		21:  ppuint8('3'),
		22:  ppuint8('4'),
		23:  ppuint8('4'),
		24:  ppuint8('6'),
		25:  ppuint8('9'),
		26:  ppuint8('9'),
		27:  ppuint8('1'),
		28:  ppuint8('9'),
		29:  ppuint8('5'),
		30:  ppuint8('5'),
		31:  ppuint8('7'),
		32:  ppuint8('2'),
		33:  ppuint8('0'),
		34:  ppuint8('1'),
		35:  ppuint8('7'),
		36:  ppuint8('5'),
		37:  ppuint8('2'),
		38:  ppuint8('8'),
		39:  ppuint8('6'),
		40:  ppuint8('1'),
		41:  ppuint8('2'),
		42:  ppuint8('5'),
		43:  ppuint8('2'),
		44:  ppuint8('5'),
		45:  ppuint8('2'),
		46:  ppuint8('7'),
		47:  ppuint8('4'),
		48:  ppuint8('0'),
		49:  ppuint8('2'),
		50:  ppuint8('7'),
		51:  ppuint8('9'),
		52:  ppuint8('1'),
		53:  ppuint8('1'),
		54:  ppuint8('7'),
		55:  ppuint8('4'),
		56:  ppuint8('5'),
		57:  ppuint8('6'),
		58:  ppuint8('7'),
		59:  ppuint8('5'),
		60:  ppuint8('9'),
		61:  ppuint8('3'),
		62:  ppuint8('1'),
		63:  ppuint8('4'),
		64:  ppuint8('2'),
		65:  ppuint8('5'),
		66:  ppuint8('5'),
		67:  ppuint8('6'),
		68:  ppuint8('6'),
		69:  ppuint8('6'),
		70:  ppuint8('1'),
		71:  ppuint8('6'),
		72:  ppuint8('4'),
		73:  ppuint8('3'),
		74:  ppuint8('8'),
		75:  ppuint8('1'),
		76:  ppuint8('2'),
		77:  ppuint8('8'),
		78:  ppuint8('7'),
		79:  ppuint8('6'),
		80:  ppuint8('2'),
		81:  ppuint8('9'),
		82:  ppuint8('2'),
		83:  ppuint8('0'),
		84:  ppuint8('8'),
		85:  ppuint8('8'),
		86:  ppuint8('9'),
		87:  ppuint8('4'),
		88:  ppuint8('3'),
		89:  ppuint8('9'),
		90:  ppuint8('6'),
		91:  ppuint8('2'),
		92:  ppuint8('8'),
		93:  ppuint8('4'),
		94:  ppuint8('1'),
		95:  ppuint8('1'),
		96:  ppuint8('8'),
		97:  ppuint8('1'),
		98:  ppuint8('0'),
		99:  ppuint8('6'),
		100: ppuint8('2'),
		101: ppuint8('3'),
		102: ppuint8('7'),
		103: ppuint8('6'),
		104: ppuint8('3'),
		105: ppuint8('8'),
		106: ppuint8('1'),
		107: ppuint8('5'),
		108: ppuint8('1'),
		109: ppuint8('7'),
		110: ppuint8('3'),
		111: ppuint8('4'),
		112: ppuint8('6'),
		113: ppuint8('1'),
		114: ppuint8('2'),
		115: ppuint8('4'),
		116: ppuint8('0'),
		117: ppuint8('1'),
		118: ppuint8('3'),
		119: ppuint8('0'),
		120: ppuint8('8'),
		121: ppuint8('4'),
		122: ppuint8('1'),
		123: ppuint8('3'),
		124: ppuint8('9'),
		125: ppuint8('3'),
		126: ppuint8('2'),
		127: ppuint8('0'),
		128: ppuint8('1'),
		129: ppuint8('6'),
		130: ppuint8('3'),
		131: ppuint8('6'),
		132: ppuint8('7'),
		133: ppuint8('1'),
		134: ppuint8('5'),
		135: ppuint8('1'),
		136: ppuint8('7'),
		137: ppuint8('5'),
		138: ppuint8('0'),
		139: ppuint8('1'),
		140: ppuint8('9'),
		141: ppuint8('8'),
		142: ppuint8('4'),
		143: ppuint8('0'),
		144: ppuint8('8'),
		145: ppuint8('2'),
		146: ppuint8('7'),
		147: ppuint8('9'),
		148: ppuint8('1'),
		149: ppuint8('3'),
		150: ppuint8('2'),
		151: ppuint8('2'),
		152: ppuint8('8'),
		153: ppuint8('3'),
		154: ppuint8('4'),
		155: ppuint8('1'),
		156: ppuint8('6'),
		157: ppuint8('2'),
		158: ppuint8('3'),
		159: ppuint8('9'),
		160: ppuint8('6'),
		161: ppuint8('2'),
		162: ppuint8('0'),
		163: ppuint8('7'),
		164: ppuint8('3'),
		165: ppuint8('5'),
		166: ppuint8('5'),
		167: ppuint8('5'),
		168: ppuint8('3'),
		169: ppuint8('4'),
		170: ppuint8('2'),
		171: ppuint8('1'),
		172: ppuint8('7'),
		173: ppuint8('0'),
		174: ppuint8('9'),
		175: ppuint8('7'),
		176: ppuint8('6'),
		177: ppuint8('2'),
		178: ppuint8('1'),
		179: ppuint8('0'),
		180: ppuint8('3'),
		181: ppuint8('3'),
		182: ppuint8('5'),
		183: ppuint8('4'),
		184: ppuint8('7'),
		185: ppuint8('6'),
		186: ppuint8('0'),
		187: ppuint8('9'),
		188: ppuint8('7'),
		189: ppuint8('6'),
		190: ppuint8('9'),
		191: ppuint8('3'),
		192: ppuint8('5'),
		193: ppuint8('1'),
		194: ppuint8('7'),
		195: ppuint8('8'),
		196: ppuint8('6'),
		197: ppuint8('8'),
		198: ppuint8('8'),
		199: ppuint8('2'),
		200: ppuint8('8'),
		201: ppuint8('1'),
		202: ppuint8('4'),
		203: ppuint8('3'),
		204: ppuint8('7'),
		205: ppuint8('4'),
		206: ppuint8('3'),
		207: ppuint8('3'),
		208: ppuint8('2'),
		209: ppuint8('4'),
		210: ppuint8('1'),
		211: ppuint8('5'),
		212: ppuint8('4'),
		213: ppuint8('7'),
		214: ppuint8('8'),
		215: ppuint8('1'),
		216: ppuint8('1'),
		217: ppuint8('4'),
		218: ppuint8('2'),
		219: ppuint8('1'),
		220: ppuint8('2'),
		221: ppuint8('4'),
		222: ppuint8('2'),
		223: ppuint8('7'),
		224: ppuint8('6'),
		225: ppuint8('5'),
		226: ppuint8('9'),
		227: ppuint8('5'),
		228: ppuint8('4'),
		229: ppuint8('5'),
		230: ppuint8('2'),
		231: ppuint8('6'),
		232: ppuint8('7'),
		233: ppuint8('3'),
		234: ppuint8('0'),
		235: ppuint8('3'),
		236: ppuint8('4'),
		237: ppuint8('0'),
		238: ppuint8('6'),
		239: ppuint8('9'),
		240: ppuint8('1'),
		241: ppuint8('8'),
		242: ppuint8('9'),
		243: ppuint8('9'),
		244: ppuint8('9'),
		245: ppuint8('8'),
		246: ppuint8('0'),
		247: ppuint8('5'),
		248: ppuint8('7'),
		249: ppuint8('0'),
		250: ppuint8('9'),
		251: ppuint8('3'),
		252: ppuint8('8'),
		253: ppuint8('7'),
		254: ppuint8('6'),
		255: ppuint8('2'),
		256: ppuint8('4'),
		257: ppuint8('6'),
		258: ppuint8('1'),
		259: ppuint8('6'),
		260: ppuint8('7'),
		261: ppuint8('2'),
		262: ppuint8('0'),
		263: ppuint8('3'),
		264: ppuint8('5'),
		265: ppuint8('9'),
		266: ppuint8('3'),
		267: ppuint8('5'),
		268: ppuint8('8'),
		269: ppuint8('8'),
		270: ppuint8('9'),
		271: ppuint8('7'),
		272: ppuint8('7'),
		273: ppuint8('9'),
		274: ppuint8('2'),
		275: ppuint8('7'),
		276: ppuint8('0'),
		277: ppuint8('8'),
		278: ppuint8('1'),
		279: ppuint8('6'),
		280: ppuint8('8'),
		281: ppuint8('7'),
		282: ppuint8('4'),
		283: ppuint8('8'),
		284: ppuint8('5'),
		285: ppuint8('3'),
		286: ppuint8('0'),
		287: ppuint8('8'),
		288: ppuint8('4'),
		289: ppuint8('3'),
		290: ppuint8('5'),
		291: ppuint8('6'),
		292: ppuint8('5'),
		293: ppuint8('1'),
		294: ppuint8('6'),
		295: ppuint8('6'),
		296: ppuint8('0'),
		297: ppuint8('9'),
		298: ppuint8('7'),
		299: ppuint8('9'),
		300: ppuint8('8'),
		301: ppuint8('9'),
		302: ppuint8('2'),
		303: ppuint8('7'),
		304: ppuint8('2'),
		305: ppuint8('6'),
		306: ppuint8('8'),
		307: ppuint8('5'),
		308: ppuint8('9'),
		309: ppuint8('4'),
		310: ppuint8('5'),
		311: ppuint8('8'),
		312: ppuint8('1'),
		313: ppuint8('3'),
		314: ppuint8('7'),
		315: ppuint8('2'),
		316: ppuint8('9'),
		317: ppuint8('3'),
		318: ppuint8('8'),
		319: ppuint8('3'),
		320: ppuint8('7'),
		321: ppuint8('9'),
		322: ppuint8('1'),
		323: ppuint8('7'),
		324: ppuint8('9'),
		325: ppuint8('9'),
		326: ppuint8('7'),
		327: ppuint8('7'),
		328: ppuint8('2'),
		329: ppuint8('8'),
		330: ppuint8('4'),
		331: ppuint8('6'),
		332: ppuint8('5'),
		333: ppuint8('5'),
		334: ppuint8('7'),
		335: ppuint8('3'),
		336: ppuint8('3'),
		337: ppuint8('8'),
		338: ppuint8('3'),
		339: ppuint8('6'),
		340: ppuint8('6'),
		341: ppuint8('9'),
		342: ppuint8('7'),
		343: ppuint8('1'),
		344: ppuint8('4'),
		345: ppuint8('3'),
		346: ppuint8('3'),
		347: ppuint8('7'),
		348: ppuint8('1'),
		349: ppuint8('4'),
		350: ppuint8('9'),
		351: ppuint8('4'),
		352: ppuint8('1'),
		353: ppuint8('2'),
		354: ppuint8('4'),
		355: ppuint8('9'),
		356: ppuint8('5'),
		357: ppuint8('1'),
		358: ppuint8('4'),
		359: ppuint8('7'),
		360: ppuint8('2'),
		361: ppuint8('6'),
		362: ppuint8('4'),
		363: ppuint8('4'),
		364: ppuint8('8'),
		365: ppuint8('0'),
		366: ppuint8('6'),
		367: ppuint8('2'),
		368: ppuint8('6'),
		369: ppuint8('0'),
		370: ppuint8('6'),
		371: ppuint8('9'),
		372: ppuint8('8'),
		373: ppuint8('1'),
		374: ppuint8('1'),
		375: ppuint8('7'),
		376: ppuint8('9'),
		377: ppuint8('9'),
		378: ppuint8('3'),
		379: ppuint8('9'),
		380: ppuint8('3'),
		381: ppuint8('8'),
		382: ppuint8('4'),
		383: ppuint8('7'),
		384: ppuint8('3'),
		385: ppuint8('1'),
		386: ppuint8('9'),
		387: ppuint8('0'),
		388: ppuint8('2'),
		389: ppuint8('3'),
		390: ppuint8('5'),
		391: ppuint8('3'),
		392: ppuint8('5'),
		393: ppuint8('4'),
		394: ppuint8('2'),
		395: ppuint8('1'),
		396: ppuint8('1'),
		397: ppuint8('7'),
		398: ppuint8('6'),
		399: ppuint8('7'),
		400: ppuint8('4'),
		401: ppuint8('3'),
		402: ppuint8('2'),
		403: ppuint8('2'),
		404: ppuint8('0'),
		405: ppuint8('6'),
		406: ppuint8('5'),
		407: ppuint8('9'),
		408: ppuint8('9'),
		409: ppuint8('3'),
		410: ppuint8('2'),
		411: ppuint8('6'),
		412: ppuint8('7'),
		413: ppuint8('1'),
		414: ppuint8('2'),
		415: ppuint8('0'),
		416: ppuint8('0'),
		417: ppuint8('3'),
		418: ppuint8('7'),
		419: ppuint8('3'),
		420: ppuint8('8'),
		421: ppuint8('7'),
		422: ppuint8('4'),
		423: ppuint8('3'),
		424: ppuint8('3'),
		425: ppuint8('3'),
		426: ppuint8('3'),
		427: ppuint8('3'),
		428: ppuint8('2'),
		429: ppuint8('3'),
		430: ppuint8('8'),
		431: ppuint8('2'),
		432: ppuint8('8'),
		433: ppuint8('6'),
		434: ppuint8('3'),
		435: ppuint8('1'),
		436: ppuint8('5'),
		437: ppuint8('5'),
		438: ppuint8('2'),
		439: ppuint8('2'),
		440: ppuint8('5'),
		441: ppuint8('9'),
		442: ppuint8('3'),
		443: ppuint8('3'),
		444: ppuint8('7'),
		445: ppuint8('0'),
		446: ppuint8('6'),
		447: ppuint8('2'),
		448: ppuint8('8'),
		449: ppuint8('1'),
		450: ppuint8('0'),
		451: ppuint8('3'),
		452: ppuint8('6'),
		453: ppuint8('7'),
		454: ppuint8('6'),
		455: ppuint8('9'),
		456: ppuint8('6'),
		457: ppuint8('5'),
		458: ppuint8('9'),
		459: ppuint8('0'),
		460: ppuint8('6'),
		461: ppuint8('6'),
		462: ppuint8('6'),
		463: ppuint8('3'),
		464: ppuint8('6'),
		465: ppuint8('9'),
		466: ppuint8('9'),
		467: ppuint8('3'),
		468: ppuint8('8'),
		469: ppuint8('7'),
		470: ppuint8('6'),
		471: ppuint8('5'),
		472: ppuint8('4'),
		473: ppuint8('5'),
		474: ppuint8('3'),
		475: ppuint8('5'),
		476: ppuint8('9'),
		477: ppuint8('4'),
		478: ppuint8('0'),
		479: ppuint8('0'),
		480: ppuint8('7'),
		481: ppuint8('5'),
		482: ppuint8('8'),
		483: ppuint8('5'),
		484: ppuint8('4'),
		485: ppuint8('1'),
		486: ppuint8('4'),
		487: ppuint8('3'),
		488: ppuint8('1'),
		489: ppuint8('5'),
		490: ppuint8('7'),
		491: ppuint8('6'),
		492: ppuint8('6'),
		493: ppuint8('3'),
		494: ppuint8('4'),
		495: ppuint8('4'),
		496: ppuint8('5'),
		497: ppuint8('0'),
		498: ppuint8('8'),
		499: ppuint8('7'),
		500: ppuint8('5'),
		501: ppuint8('7'),
		502: ppuint8('5'),
		503: ppuint8('0'),
		504: ppuint8('1'),
		505: ppuint8('0'),
		506: ppuint8('1'),
		507: ppuint8('8'),
		508: ppuint8('4'),
		509: ppuint8('7'),
		510: ppuint8('3'),
		511: ppuint8('1'),
		512: ppuint8('9'),
		513: ppuint8('9'),
		514: ppuint8('2'),
		515: ppuint8('7'),
		516: ppuint8('1'),
		517: ppuint8('1'),
		518: ppuint8('1'),
		519: ppuint8('2'),
		520: ppuint8('3'),
		521: ppuint8('9'),
		522: ppuint8('9'),
		523: ppuint8('6'),
		524: ppuint8('5'),
		525: ppuint8('9'),
		526: ppuint8('2'),
		527: ppuint8('3'),
		528: ppuint8('2'),
		529: ppuint8('8'),
		530: ppuint8('1'),
		531: ppuint8('5'),
		532: ppuint8('5'),
		533: ppuint8('1'),
		534: ppuint8('2'),
		535: ppuint8('6'),
		536: ppuint8('4'),
		537: ppuint8('9'),
		538: ppuint8('6'),
		539: ppuint8('6'),
		540: ppuint8('4'),
		541: ppuint8('5'),
		542: ppuint8('1'),
		543: ppuint8('1'),
		544: ppuint8('6'),
		545: ppuint8('0'),
		546: ppuint8('0'),
		547: ppuint8('3'),
		548: ppuint8('2'),
		549: ppuint8('8'),
		550: ppuint8('4'),
		551: ppuint8('8'),
		552: ppuint8('7'),
		553: ppuint8('1'),
		554: ppuint8('4'),
		555: ppuint8('9'),
		556: ppuint8('6'),
		557: ppuint8('8'),
		558: ppuint8('1'),
		559: ppuint8('6'),
		560: ppuint8('5'),
		561: ppuint8('9'),
		562: ppuint8('8'),
		563: ppuint8('3'),
		564: ppuint8('4'),
		565: ppuint8('2'),
		566: ppuint8('9'),
		567: ppuint8('7'),
		568: ppuint8('0'),
		569: ppuint8('1'),
		570: ppuint8('9'),
		571: ppuint8('2'),
		572: ppuint8('6'),
		573: ppuint8('6'),
		574: ppuint8('9'),
		575: ppuint8('1'),
		576: ppuint8('3'),
		577: ppuint8('5'),
		578: ppuint8('9'),
		579: ppuint8('3'),
		580: ppuint8('2'),
		581: ppuint8('9'),
		582: ppuint8('6'),
		583: ppuint8('2'),
		584: ppuint8('3'),
		585: ppuint8('0'),
		586: ppuint8('6'),
		587: ppuint8('0'),
		588: ppuint8('1'),
		589: ppuint8('1'),
		590: ppuint8('6'),
		591: ppuint8('5'),
		592: ppuint8('1'),
		593: ppuint8('7'),
		594: ppuint8('9'),
		595: ppuint8('0'),
		596: ppuint8('7'),
		597: ppuint8('5'),
		598: ppuint8('8'),
		599: ppuint8('6'),
		600: ppuint8('8'),
		601: ppuint8('4'),
		602: ppuint8('2'),
		603: ppuint8('1'),
		604: ppuint8('0'),
		605: ppuint8('3'),
		606: ppuint8('8'),
		607: ppuint8('6'),
		608: ppuint8('6'),
		609: ppuint8('4'),
		610: ppuint8('4'),
		611: ppuint8('9'),
		612: ppuint8('9'),
		613: ppuint8('7'),
		614: ppuint8('5'),
		615: ppuint8('8'),
		616: ppuint8('1'),
		617: ppuint8('7'),
		618: ppuint8('5'),
		619: ppuint8('7'),
		620: ppuint8('9'),
		621: ppuint8('6'),
		622: ppuint8('6'),
		623: ppuint8('8'),
		624: ppuint8('8'),
		625: ppuint8('5'),
		626: ppuint8('8'),
		627: ppuint8('6'),
		628: ppuint8('7'),
		629: ppuint8('4'),
		630: ppuint8('0'),
		631: ppuint8('7'),
		632: ppuint8('2'),
		633: ppuint8('0'),
		634: ppuint8('2'),
		635: ppuint8('9'),
		636: ppuint8('9'),
		637: ppuint8('4'),
		638: ppuint8('4'),
		639: ppuint8('1'),
		640: ppuint8('9'),
		641: ppuint8('5'),
		642: ppuint8('8'),
		643: ppuint8('6'),
		644: ppuint8('5'),
		645: ppuint8('0'),
		646: ppuint8('6'),
		647: ppuint8('7'),
		648: ppuint8('4'),
		649: ppuint8('2'),
		650: ppuint8('7'),
		651: ppuint8('3'),
		652: ppuint8('2'),
		653: ppuint8('3'),
		654: ppuint8('2'),
		655: ppuint8('7'),
		656: ppuint8('0'),
		657: ppuint8('2'),
		658: ppuint8('1'),
		659: ppuint8('3'),
		660: ppuint8('0'),
		661: ppuint8('5'),
		662: ppuint8('9'),
		663: ppuint8('0'),
		664: ppuint8('3'),
		665: ppuint8('9'),
		666: ppuint8('1'),
		667: ppuint8('4'),
		668: ppuint8('5'),
		669: ppuint8('3'),
		670: ppuint8('7'),
		671: ppuint8('2'),
		672: ppuint8('7'),
		673: ppuint8('0'),
		674: ppuint8('8'),
		675: ppuint8('5'),
		676: ppuint8('5'),
		677: ppuint8('4'),
		678: ppuint8('6'),
		679: ppuint8('1'),
		680: ppuint8('1'),
		681: ppuint8('0'),
		682: ppuint8('0'),
		683: ppuint8('9'),
		684: ppuint8('2'),
		685: ppuint8('0'),
		686: ppuint8('4'),
		687: ppuint8('1'),
		688: ppuint8('6'),
		689: ppuint8('6'),
		690: ppuint8('4'),
		691: ppuint8('6'),
		692: ppuint8('9'),
		693: ppuint8('1'),
		694: ppuint8('3'),
		695: ppuint8('2'),
		696: ppuint8('8'),
		697: ppuint8('5'),
		698: ppuint8('0'),
		699: ppuint8('3'),
		700: ppuint8('3'),
		701: ppuint8('8'),
		702: ppuint8('9'),
		703: ppuint8('8'),
		704: ppuint8('7'),
		705: ppuint8('8'),
		706: ppuint8('5'),
		707: ppuint8('9'),
		708: ppuint8('5'),
		709: ppuint8('5'),
		710: ppuint8('9'),
		711: ppuint8('1'),
		712: ppuint8('9'),
		713: ppuint8('3'),
		714: ppuint8('6'),
		715: ppuint8('5'),
		716: ppuint8('4'),
		717: ppuint8('1'),
		718: ppuint8('7'),
		719: ppuint8('4'),
		720: ppuint8('0'),
		721: ppuint8('2'),
		722: ppuint8('4'),
		723: ppuint8('7'),
		724: ppuint8('2'),
		725: ppuint8('9'),
		726: ppuint8('7'),
		727: ppuint8('1'),
		728: ppuint8('2'),
		729: ppuint8('4'),
		730: ppuint8('5'),
		731: ppuint8('8'),
		732: ppuint8('1'),
		733: ppuint8('4'),
		734: ppuint8('4'),
		735: ppuint8('6'),
		736: ppuint8('1'),
		737: ppuint8('8'),
		738: ppuint8('5'),
		739: ppuint8('8'),
		740: ppuint8('7'),
		741: ppuint8('6'),
		742: ppuint8('9'),
		743: ppuint8('7'),
		744: ppuint8('2'),
		745: ppuint8('1'),
		746: ppuint8('2'),
		747: ppuint8('0'),
		748: ppuint8('8'),
		749: ppuint8('9'),
		750: ppuint8('5'),
		751: ppuint8('9'),
		752: ppuint8('5'),
		753: ppuint8('5'),
		754: ppuint8('3'),
		755: ppuint8('8'),
		756: ppuint8('1'),
		757: ppuint8('2'),
		758: ppuint8('5'),
		759: ppuint8('4'),
		760: ppuint8('3'),
		761: ppuint8('0'),
		762: ppuint8('7'),
		763: ppuint8('6'),
		764: ppuint8('5'),
		765: ppuint8('1'),
		766: ppuint8('7'),
		767: ppuint8('8'),
		768: ppuint8('2'),
		769: ppuint8('0'),
		770: ppuint8('0'),
		771: ppuint8('7'),
		772: ppuint8('6'),
		773: ppuint8('7'),
		774: ppuint8('4'),
		775: ppuint8('8'),
		776: ppuint8('1'),
		777: ppuint8('0'),
		778: ppuint8('6'),
		779: ppuint8('3'),
		780: ppuint8('2'),
		781: ppuint8('3'),
		782: ppuint8('0'),
		783: ppuint8('5'),
		784: ppuint8('2'),
		785: ppuint8('5'),
		786: ppuint8('0'),
		787: ppuint8('1'),
		788: ppuint8('1'),
		789: ppuint8('4'),
		790: ppuint8('3'),
		791: ppuint8('8'),
		792: ppuint8('4'),
		793: ppuint8('5'),
		794: ppuint8('2'),
		795: ppuint8('3'),
		796: ppuint8('9'),
		797: ppuint8('5'),
		798: ppuint8('0'),
		799: ppuint8('9'),
		800: ppuint8('8'),
		801: ppuint8('2'),
		802: ppuint8('6'),
		803: ppuint8('4'),
		804: ppuint8('7'),
		805: ppuint8('4'),
		806: ppuint8('8'),
		807: ppuint8('0'),
		808: ppuint8('1'),
		809: ppuint8('1'),
		810: ppuint8('7'),
		811: ppuint8('1'),
		812: ppuint8('5'),
		813: ppuint8('4'),
		814: ppuint8('9'),
		815: ppuint8('0'),
		816: ppuint8('9'),
		817: ppuint8('2'),
		818: ppuint8('2'),
		819: ppuint8('3'),
		820: ppuint8('8'),
		821: ppuint8('1'),
		822: ppuint8('6'),
		823: ppuint8('9'),
		824: ppuint8('0'),
		825: ppuint8('4'),
		826: ppuint8('6'),
		827: ppuint8('4'),
		828: ppuint8('5'),
		829: ppuint8('4'),
		830: ppuint8('6'),
		831: ppuint8('3'),
		832: ppuint8('8'),
		833: ppuint8('7'),
		834: ppuint8('3'),
		835: ppuint8('6'),
		836: ppuint8('1'),
		837: ppuint8('7'),
		838: ppuint8('2'),
		839: ppuint8('3'),
		840: ppuint8('4'),
		841: ppuint8('5'),
		842: ppuint8('5'),
		843: ppuint8('2'),
		844: ppuint8('0'),
		845: ppuint8('2'),
		846: ppuint8('5'),
		847: ppuint8('8'),
		848: ppuint8('1'),
		849: ppuint8('4'),
		850: ppuint8('9'),
		851: ppuint8('3'),
		852: ppuint8('0'),
		853: ppuint8('7'),
		854: ppuint8('4'),
		855: ppuint8('1'),
		856: ppuint8('6'),
		857: ppuint8('8'),
		858: ppuint8('7'),
		859: ppuint8('8'),
		860: ppuint8('2'),
		861: ppuint8('6'),
		862: ppuint8('2'),
		863: ppuint8('5'),
		864: ppuint8('1'),
		865: ppuint8('0'),
		866: ppuint8('7'),
		867: ppuint8('4'),
		868: ppuint8('7'),
		869: ppuint8('3'),
		870: ppuint8('6'),
		871: ppuint8('6'),
		872: ppuint8('4'),
		873: ppuint8('5'),
		874: ppuint8('6'),
		875: ppuint8('6'),
		876: ppuint8('6'),
		877: ppuint8('6'),
		878: ppuint8('8'),
		879: ppuint8('5'),
		880: ppuint8('1'),
		881: ppuint8('3'),
		882: ppuint8('5'),
		883: ppuint8('7'),
		884: ppuint8('1'),
		885: ppuint8('6'),
		886: ppuint8('2'),
		887: ppuint8('0'),
		888: ppuint8('9'),
		889: ppuint8('2'),
		890: ppuint8('3'),
		891: ppuint8('2'),
		892: ppuint8('6'),
		893: ppuint8('0'),
		894: ppuint8('7'),
		895: ppuint8('9'),
		896: ppuint8('8'),
		897: ppuint8('1'),
		898: ppuint8('6'),
		899: ppuint8('2'),
		900: ppuint8('0'),
		901: ppuint8('3'),
		902: ppuint8('8'),
		903: ppuint8('8'),
		904: ppuint8('0'),
		905: ppuint8('2'),
		906: ppuint8('8'),
		907: ppuint8('7'),
		908: ppuint8('7'),
		909: ppuint8('5'),
		910: ppuint8('9'),
		911: ppuint8('3'),
		912: ppuint8('1'),
		913: ppuint8('0'),
		914: ppuint8('6'),
		915: ppuint8('7'),
		916: ppuint8('5'),
		917: ppuint8('7'),
		918: ppuint8('3'),
		919: ppuint8('1'),
		920: ppuint8('2'),
		921: ppuint8('7'),
		922: ppuint8('7'),
		923: ppuint8('2'),
		924: ppuint8('0'),
		925: ppuint8('0'),
		926: ppuint8('4'),
		927: ppuint8('1'),
		928: ppuint8('2'),
		929: ppuint8('8'),
		930: ppuint8('2'),
		931: ppuint8('0'),
		932: ppuint8('8'),
		933: ppuint8('4'),
		934: ppuint8('0'),
		935: ppuint8('5'),
		936: ppuint8('0'),
		937: ppuint8('5'),
		938: ppuint8('0'),
		939: ppuint8('1'),
		940: ppuint8('9'),
		941: ppuint8('3'),
		942: ppuint8('3'),
		943: ppuint8('6'),
		944: ppuint8('3'),
		945: ppuint8('6'),
		946: ppuint8('9'),
		947: ppuint8('6'),
		948: ppuint8('2'),
		949: ppuint8('8'),
		950: ppuint8('2'),
		951: ppuint8('9'),
		952: ppuint8('7'),
		953: ppuint8('5'),
		954: ppuint8('3'),
		955: ppuint8('8'),
		956: ppuint8('8'),
		957: ppuint8('9'),
		958: ppuint8('1'),
		959: ppuint8('1'),
		960: ppuint8('4'),
		961: ppuint8('5'),
		962: ppuint8('7'),
		963: ppuint8('7'),
		964: ppuint8('5'),
		965: ppuint8('6'),
		966: ppuint8('0'),
		967: ppuint8('2'),
		968: ppuint8('7'),
		969: ppuint8('9'),
		970: ppuint8('7'),
		971: ppuint8('2'),
		972: ppuint8('1'),
		973: ppuint8('7'),
		974: ppuint8('4'),
		975: ppuint8('3'),
		976: ppuint8('0'),
		977: ppuint8('3'),
		978: ppuint8('6'),
		979: ppuint8('7'),
		980: ppuint8('3'),
		981: ppuint8('7'),
		982: ppuint8('2'),
		983: ppuint8('2'),
		984: ppuint8('7'),
		985: ppuint8('5'),
		986: ppuint8('6'),
		987: ppuint8('2'),
		988: ppuint8('3'),
		989: ppuint8('1'),
		990: ppuint8('2'),
		991: ppuint8('1'),
		992: ppuint8('3'),
		993: ppuint8('1'),
		994: ppuint8('4'),
		995: ppuint8('2'),
		996: ppuint8('6'),
		997: ppuint8('9'),
		998: ppuint8('2'),
		999: ppuint8('3'),
	}

	Xmpfr_init2(cgtls, cgbp, ppint64(3322))
	Xmpfr_set_str(cgtls, cgbp, cgbp+42, ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(4343), ppint32(ecMPFR_RNDN))
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+1048, ppint32(10), ppuint64(1000), cgbp, ppint32(ecMPFR_RNDN))
	if iqlibc.ppInt32FromUint8(*(*ppuint8)(iqunsafe.ppPointer(aas + 999))) != ppint32('1') { /* s must be 5.04383...689071e-309 */

		Xprintf(cgtls, "Error in check_large: expected '689071', got '%s'\n\x00", iqlibc.ppVaList(cgbp+1064, aas+ppuintptr(994)))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(4343), ppint32(ecMPFR_RNDN))
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+1048, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "12\x00") != 0 || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 1048)) != ppint64(1000) {

		Xprintf(cgtls, "Error in check_large: expected 0.12e1000\n\x00", 0)
		Xprintf(cgtls, "got %se%d\n\x00", iqlibc.ppVaList(cgbp+1064, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 1048)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_clear_flags(cgtls)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+1048, ppint32(10), ppuint64(1000), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "@NaN@\x00") != 0 {

		Xprintf(cgtls, "Error for NaN (incorrect string)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if X__gmpfr_flags != ppuint32(mvMPFR_FLAGS_NAN) {

		Xprintf(cgtls, "Error for NaN (incorrect flags)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_get_str(cgtls, cgbp+35, cgbp+1048, ppint32(10), ppuint64(1000), cgbp, ppint32(ecMPFR_RNDN))

	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+1048, ppint32(10), ppuint64(1000), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "@Inf@\x00") != 0 {

		Xprintf(cgtls, "Error for Inf\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_get_str(cgtls, cgbp+35, cgbp+1048, ppint32(10), ppuint64(1000), cgbp, ppint32(ecMPFR_RNDN))

	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+1048, ppint32(10), ppuint64(1000), cgbp, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "-@Inf@\x00") != 0 {

		Xprintf(cgtls, "Error for -Inf\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)

	Xmpfr_get_str(cgtls, cgbp+35, cgbp+1048, ppint32(10), ppuint64(1000), cgbp, ppint32(ecMPFR_RNDN))

	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+1048, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
	if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 1048)) != 0 || Xstrcmp(cgtls, aas, "00\x00") != 0 {

		Xprintf(cgtls, "Error for 0.0\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_get_str(cgtls, cgbp+35, cgbp+1048, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))

	Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN)) /* -0.0 */
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+1048, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))
	if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 1048)) != 0 || Xstrcmp(cgtls, aas, "-00\x00") != 0 {

		Xprintf(cgtls, "Error for -0.0\ngot %se%d\n\x00", iqlibc.ppVaList(cgbp+1064, aas, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 1048)))))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_get_str(cgtls, cgbp+35, cgbp+1048, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDN))

	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_special(cgtls *iqlibc.ppTLS, aab ppint32, aap tnmpfr_prec_t) {
	cgbp := cgtls.ppAlloc(272)
	defer cgtls.ppFree(272)

	var aac ppuint8
	var aai, aaj, aar, ccv5, ccv7 ppint32
	var aam tnsize_t
	var pp_ /* e at bp+240 */ tnmpfr_exp_t
	var pp_ /* s at bp+32 */ [102]ppuint8
	var pp_ /* s2 at bp+134 */ [102]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aac, aai, aaj, aam, aar, ccv5, ccv7

	Xmpfr_init2(cgtls, cgbp, aap)

	/* check for invalid base */
	if Xmpfr_get_str(cgtls, cgbp+32, cgbp+240, ppint32(1), ppuint64(10), cgbp, ppint32(ecMPFR_RNDN)) != iqlibc.ppUintptrFromInt32(0) {

		Xprintf(cgtls, "Error: mpfr_get_str should not accept base = 1\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_get_str(cgtls, cgbp+32, cgbp+240, ppint32(63), ppuint64(10), cgbp, ppint32(ecMPFR_RNDN)) != iqlibc.ppUintptrFromInt32(0) {

		Xprintf(cgtls, "Error: mpfr_get_str should not accept base = 63\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	(*(*[102]ppuint8)(iqunsafe.ppPointer(cgbp + 134)))[0] = ppuint8('1')
	aai = ppint32(1)
	for {
		if !(aai < iqlibc.ppInt32FromInt32(mvMAX_DIGITS)+iqlibc.ppInt32FromInt32(2)) {
			break
		}
		(*(*[102]ppuint8)(iqunsafe.ppPointer(cgbp + 134)))[aai] = ppuint8('0')
		goto cg_1
	cg_1:
		;
		aai++
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	aai = ppint32(1)
	for {
		if !(aai < ppint32(mvMAX_DIGITS) && Xmpfr_mul_ui(cgtls, cgbp, cgbp, iqlibc.ppUint64FromInt32(aab), ppint32(ecMPFR_RNDN)) == 0) {
			break
		}

		/* x = b^i (exact) */
		aar = 0
		for {
			if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}
			if aai < ppint32(3) {
				ccv5 = ppint32(2)
			} else {
				ccv5 = aai - ppint32(1)
			}
			aam = iqlibc.ppUint64FromInt32(ccv5)
			for {
				if !(iqlibc.ppInt32FromUint64(aam) <= aai+ppint32(1)) {
					break
				}

				Xmpfr_get_str(cgtls, cgbp+32, cgbp+240, aab, aam, cgbp, aar)
				/* s should be 1 followed by (m-1) zeros, and e should be i+1 */
				if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 240)) != ppint64(aai+ppint32(1)) || Xstrncmp(cgtls, cgbp+32, cgbp+134, aam) != 0 {

					Xprintf(cgtls, "Error in mpfr_get_str for %d^%d\n\x00", iqlibc.ppVaList(cgbp+256, aab, aai))
					Xexit(cgtls, ppint32(1))
				}

				goto cg_4
			cg_4:
				;
				aam++
			}
			goto cg_3
		cg_3:
			;
			aar++
		}
		if Xmpfr_sub_ui(cgtls, cgbp, cgbp, ppuint64(1), ppint32(ecMPFR_RNDN)) != 0 {
			break
		}
		/* now x = b^i-1 (exact) */
		aar = 0
		for {
			if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}
			if aai >= ppint32(2) {

				Xmpfr_get_str(cgtls, cgbp+32, cgbp+240, aab, iqlibc.ppUint64FromInt32(aai), cgbp, aar)
				/* should be i times (b-1) */
				if aab <= ppint32(10) {
					ccv7 = ppint32('0') + aab - ppint32(1)
				} else {
					ccv7 = ppint32('a') + (aab - ppint32(11))
				}
				aac = iqlibc.ppUint8FromInt32(ccv7)
				aaj = 0
				for {
					if !(aaj < aai && iqlibc.ppInt32FromUint8((*(*[102]ppuint8)(iqunsafe.ppPointer(cgbp + 32)))[aaj]) == iqlibc.ppInt32FromUint8(aac)) {
						break
					}
					goto cg_8
				cg_8:
					;
					aaj++
				}
				if aaj < aai || *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 240)) != ppint64(aai) {

					Xprintf(cgtls, "Error in mpfr_get_str for %d^%d-1\n\x00", iqlibc.ppVaList(cgbp+256, aab, aai))
					Xprintf(cgtls, "got 0.%s*2^%d\n\x00", iqlibc.ppVaList(cgbp+256, cgbp+32, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 240)))))
					Xexit(cgtls, ppint32(1))
				}
			}
			goto cg_6
		cg_6:
			;
			aar++
		}
		if aai >= ppint32(3) {

			Xmpfr_get_str(cgtls, cgbp+32, cgbp+240, aab, iqlibc.ppUint64FromInt32(aai-ppint32(1)), cgbp, ppint32(ecMPFR_RNDU))
			/* should be b^i */
			if *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 240)) != ppint64(aai+ppint32(1)) || Xstrncmp(cgtls, cgbp+32, cgbp+134, iqlibc.ppUint64FromInt32(aai-ppint32(1))) != 0 {

				Xprintf(cgtls, "Error in mpfr_get_str for %d^%d-1\n\x00", iqlibc.ppVaList(cgbp+256, aab, aai))
				Xprintf(cgtls, "got 0.%s*2^%d\n\x00", iqlibc.ppVaList(cgbp+256, cgbp+32, ppint32(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 240)))))
				Xexit(cgtls, ppint32(1))
			}
		}

		Xmpfr_add_ui(cgtls, cgbp, cgbp, ppuint64(1), ppint32(ecMPFR_RNDN))

		goto cg_2
	cg_2:
		;
		aai++
	}
	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_bug_base2k(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(144)
	defer cgtls.ppFree(144)

	var aas ppuintptr
	var pp_ /* e at bp+96 */ tnmpfr_exp_t
	var pp_ /* xx at bp+0 */ tnmpfr_t
	var pp_ /* yy at bp+32 */ tnmpfr_t
	var pp_ /* zz at bp+64 */ tnmpfr_t
	pp_ = aas

	Xmpfr_init2(cgtls, cgbp, ppint64(107))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(79))
	Xmpfr_init2(cgtls, cgbp+64, ppint64(99))

	Xmpfr_set_str(cgtls, cgbp, "-1.90e8c3e525d7c0000000000000@-18\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str(cgtls, cgbp+32, "-2.63b22b55697e8000000@130\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	Xmpfr_add(cgtls, cgbp+64, cgbp, cgbp+32, ppint32(ecMPFR_RNDD))
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+96, ppint32(16), ppuint64(0), cgbp+64, ppint32(ecMPFR_RNDN))
	if Xstrcmp(cgtls, aas, "-263b22b55697e8000000000008\x00") != 0 {

		Xprintf(cgtls, "Error for get_str base 16\nGot %s expected -263b22b55697e8000000000008\n\x00", iqlibc.ppVaList(cgbp+112, aas))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+112, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

func sicheck_reduced_exprange(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aaemax tnmpfr_exp_t
	var aas ppuintptr
	var pp_ /* e at bp+32 */ tnmpfr_exp_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_ = aaemax, aas

	aaemax = X__gmpfr_emax
	Xmpfr_init2(cgtls, cgbp, ppint64(8))
	Xmpfr_set_str(cgtls, cgbp, "0.11111111E0\x00", ppint32(2), ppint32(ecMPFR_RNDN))
	Xset_emax(cgtls, 0)
	aas = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(16), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
	Xset_emax(cgtls, aaemax)
	if Xstrcmp(cgtls, aas, "ff0\x00") != 0 {

		Xprintf(cgtls, "Error for mpfr_get_str on 0.11111111E0 in base 16:\nGot \"%s\" instead of \"ff0\".\n\x00", iqlibc.ppVaList(cgbp+48, aas))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_inex(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(64)
	defer cgtls.ppFree(64)

	var aaflags tnmpfr_flags_t
	var aai, aainex, aar ppint32
	var aas [2]ppuintptr
	var ccv3, ccv4 ppbool
	var pp_ /* e at bp+32 */ tnmpfr_exp_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaflags, aai, aainex, aar, aas, ccv3, ccv4

	Xmpfr_init2(cgtls, cgbp, ppint64(8))
	Xmpfr_set_str(cgtls, cgbp, "0.11111111E-17\x00", ppint32(2), ppint32(ecMPFR_RNDN))
	aai = ppint32(1)
	for {
		if !(aai <= ppint32(25)) {
			break
		}
		aainex = 0

		aar = 0
		for {
			if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}

			Xmpfr_clear_flags(cgtls)
			aas[iqlibc.ppBoolInt32(aar != 0)] = Xmpfr_get_str(cgtls, iqlibc.ppUintptrFromInt32(0), cgbp+32, ppint32(10), iqlibc.ppUint64FromInt32(aai), cgbp, aar)

			if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 32)) == ppint64(-iqlibc.ppInt32FromInt32(5)))), ppint64(1)) != 0; !ccv3 {
				Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1227), "e == -5\x00")
			}
			pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
			aaflags = X__gmpfr_flags
			if iqlibc.ppBoolInt32(aai >= ppint32(20))^iqlibc.ppBoolInt32(!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)) != 0 {

				Xprintf(cgtls, "Error in check_inex on i=%d and %s\n\x00", iqlibc.ppVaList(cgbp+48, aai, Xmpfr_print_rnd_mode(cgtls, aar)))
				Xprintf(cgtls, "Got %s\n\x00", iqlibc.ppVaList(cgbp+48, aas[iqlibc.ppBoolInt32(aar != 0)]))
				Xprintf(cgtls, "Flags:\x00", 0)
				Xflags_out(cgtls, aaflags)
				Xexit(cgtls, ppint32(1))
			}
			if aar != 0 {

				aainex |= iqlibc.ppBoolInt32(Xstrcmp(cgtls, aas[0], aas[ppint32(1)]) != 0)
				Xmpfr_free_str(cgtls, aas[ppint32(1)])
			}

			goto cg_2
		cg_2:
			;
			aar++
		}

		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppBoolInt32(aai >= iqlibc.ppInt32FromInt32(20))^aainex != 0)), ppint64(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1245), "(i >= 20) ^ inex\x00")
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		Xmpfr_free_str(cgtls, aas[0])

		goto cg_1
	cg_1:
		;
		aai++
	}
	Xmpfr_clear(cgtls, cgbp)
}

func sicheck_negative_base(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aas ppuintptr
	var ccv2, ccv4 ppbool
	var pp_ /* e at bp+32 */ tnmpfr_exp_t
	var pp_ /* f at bp+0 */ tnmpfr_t
	var pp_ /* s2 at bp+40 */ [16]ppuint8
	var pp_ /* s3 at bp+56 */ [16]ppuint8
	pp_, pp_, pp_ = aas, ccv2, ccv4
	*(*[16]ppuint8)(iqunsafe.ppPointer(cgbp + 40)) = [16]ppuint8{'7', 'B', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'}
	*(*[16]ppuint8)(iqunsafe.ppPointer(cgbp + 56)) = [16]ppuint8{'7', '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'}

	Xmpfr_init(cgtls, cgbp)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(123)), 0, ppint32(ecMPFR_RNDN))
	aas = Xmpfr_get_str(cgtls, ppuintptr(0), cgbp+32, -ppint32(16), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xstrcmp(cgtls, aas, cgbp+40) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1260), "strcmp (s, s2) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xstrcmp(cgtls, aas, cgbp+40) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_free_str(cgtls, aas)
	aas = Xmpfr_get_str(cgtls, ppuintptr(0), cgbp+32, -ppint32(17), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xstrcmp(cgtls, aas, cgbp+56) == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1263), "strcmp (s, s3) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xstrcmp(cgtls, aas, cgbp+56) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_free_str(cgtls, aas)
	Xmpfr_clear(cgtls, cgbp)
}

func sicoverage(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aab ppint32
	var aam tnsize_t
	var ccv11, ccv12, ccv2, ccv3, ccv5, ccv6, ccv8, ccv9 ppbool
	var pp_ /* e at bp+80 */ tnmpfr_exp_t
	var pp_ /* s at bp+32 */ [42]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aab, aam, ccv11, ccv12, ccv2, ccv3, ccv5, ccv6, ccv8, ccv9
	aab = ppint32(3)
	aam = ppuint64(40)

	Xmpfr_init2(cgtls, cgbp, ppint64(128))

	/* exercise corner case in mpfr_get_str_aux: exact case (e < 0), where r
	   rounds to a power of 2, and f is a multiple of GMP_NUMB_BITS */
	Xmpfr_set_ui_2exp(cgtls, cgbp, ppuint64(1), ppint64(64), ppint32(ecMPFR_RNDU))
	Xmpfr_nextbelow(cgtls, cgbp)
	/* x = 2^64 - 2^(-64) */
	Xmpfr_get_str(cgtls, cgbp+32, cgbp+80, aab, aam, cgbp, ppint32(ecMPFR_RNDU))
	/* s is the base-3 string for 6148914691236517206 (in base 10) */

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xstrcmp(cgtls, cgbp+32, "1111222002212212010121102012021021021200\x00") == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1288), "strcmp (s, \"1111222002212212010121102012021021021200\") == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xstrcmp(cgtls, cgbp+32, "1111222002212212010121102012021021021200\x00") == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 80)) == iqlibc.ppInt64FromInt32(41))), ppint64(1)) != 0; !ccv3 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1289), "e == 41\x00")
	}
	pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* exercise corner case in mpfr_get_str: input is m=0, then it is changed
	   to m=1 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(1))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_get_str(cgtls, cgbp+32, cgbp+80, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xstrcmp(cgtls, cgbp+32, "1\x00") == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1296), "strcmp (s, \"1\") == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xstrcmp(cgtls, cgbp+32, "1\x00") == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 80)) == iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv6 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1297), "e == 1\x00")
	}
	pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_get_str(cgtls, cgbp+32, cgbp+80, ppint32(2), ppuint64(1), cgbp, ppint32(ecMPFR_RNDN))

	if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xstrcmp(cgtls, cgbp+32, "1\x00") == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv8 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1299), "strcmp (s, \"1\") == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xstrcmp(cgtls, cgbp+32, "1\x00") == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 80)) == iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv9 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1300), "e == 1\x00")
	}
	pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* exercise corner case in mpfr_get_str: case m < g, exact <> 0,
	   nx > 2 * n, and low nx-2n limbs from xp not identically zero */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(129))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.100100111110110111100010100011011111101010110000011001000111111101100110101110110000110011E8\x00")
	Xmpfr_nextabove(cgtls, cgbp)
	Xmpfr_get_str(cgtls, cgbp+32, cgbp+80, ppint32(10), ppuint64(2), cgbp, ppint32(ecMPFR_RNDZ))

	if ccv11 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xstrcmp(cgtls, cgbp+32, "14\x00") == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv11 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1308), "strcmp (s, \"14\") == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xstrcmp(cgtls, cgbp+32, "14\x00") == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv11 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(*(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp + 80)) == iqlibc.ppInt64FromInt32(3))), ppint64(1)) != 0; !ccv12 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1309), "e == 3\x00")
	}
	pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear(cgtls, cgbp)
}

func sitest_ndigits_aux(cgtls *iqlibc.ppTLS, aab ppint32, aap tnmpfr_prec_t, aaexpected_m tnsize_t) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aaflags, ccv3 tnmpfr_flags_t
	var aai ppint32
	var aam tnsize_t
	var aaold_emax, aaold_emin tnmpfr_exp_t
	var ccv1, ccv2, ccv4, ccv5 ppbool
	var pp_ /* e at bp+0 */ [3]tnmpfr_exp_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaflags, aai, aam, aaold_emax, aaold_emin, ccv1, ccv2, ccv3, ccv4, ccv5
	*(*[3]tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp)) = [3]tnmpfr_exp_t{
		0: iqlibc.ppInt64FromInt32(1) - iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)),
		2: iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) - iqlibc.ppInt64FromInt32(1),
	}

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(cgbp == cgbp)), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1325), "(void *) &(e) == (void *) &(e)[0]\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	aai = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt64(iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(24)/iqlibc.ppUint64FromInt64(8))+iqlibc.ppInt64FromInt32(1)))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(cgbp == cgbp)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1326), "(void *) &(e) == (void *) &(e)[0]\x00")
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	if ppint64(aai) < iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64(24)/iqlibc.ppUint64FromInt64(8)) {

		Xset_emin(cgtls, (*(*[3]tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp)))[aai])
		Xset_emax(cgtls, (*(*[3]tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp)))[aai])
	}

	ccv3 = ppuint32(Xrandlimb(cgtls) & iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)))
	aaflags = ccv3
	X__gmpfr_flags = ccv3
	aam = Xmpfr_get_str_ndigits(cgtls, aab, aap)

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aam == aaexpected_m)), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1334), "m == expected_m\x00")
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == aaflags)), ppint64(1)) != 0; !ccv5 {
		Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1335), "__gmpfr_flags == flags\x00")
	}
	pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xset_emin(cgtls, aaold_emin)
	Xset_emax(cgtls, aaold_emax)
}

// C documentation
//
//	/* test of mpfr_get_str_ndigits */
func sitest_ndigits(cgtls *iqlibc.ppTLS) {

	var aap tnmpfr_prec_t
	pp_ = aap

	/* for b=2, we have 1 + ceil((p-1)*log(2)/log(b)) = p */
	aap = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aap <= ppint64(1024)) {
			break
		}
		sitest_ndigits_aux(cgtls, ppint32(2), aap, iqlibc.ppUint64FromInt64(aap))
		goto cg_1
	cg_1:
		;
		aap++
	}

	/* for b=4, we have 1 + ceil((p-1)*log(2)/log(b)) = 1 + ceil((p-1)/2)
	   = 1 + floor(p/2) */
	aap = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aap <= ppint64(1024)) {
			break
		}
		sitest_ndigits_aux(cgtls, ppint32(4), aap, iqlibc.ppUint64FromInt64(ppint64(1)+aap/ppint64(2)))
		goto cg_2
	cg_2:
		;
		aap++
	}

	/* for b=8, we have 1 + ceil((p-1)*log(2)/log(b)) = 1 + ceil((p-1)/3)
	   = 1 + floor((p+1)/3) */
	aap = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aap <= ppint64(1024)) {
			break
		}
		sitest_ndigits_aux(cgtls, ppint32(8), aap, iqlibc.ppUint64FromInt64(ppint64(1)+(aap+ppint64(1))/ppint64(3)))
		goto cg_3
	cg_3:
		;
		aap++
	}

	/* for b=16, we have 1 + ceil((p-1)*log(2)/log(b)) = 1 + ceil((p-1)/4)
	   = 1 + floor((p+2)/4) */
	aap = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aap <= ppint64(1024)) {
			break
		}
		sitest_ndigits_aux(cgtls, ppint32(16), aap, iqlibc.ppUint64FromInt64(ppint64(1)+(aap+ppint64(2))/ppint64(4)))
		goto cg_4
	cg_4:
		;
		aap++
	}

	/* for b=32, we have 1 + ceil((p-1)*log(2)/log(b)) = 1 + ceil((p-1)/5)
	   = 1 + floor((p+3)/5) */
	aap = ppint64(mvMPFR_PREC_MIN)
	for {
		if !(aap <= ppint64(1024)) {
			break
		}
		sitest_ndigits_aux(cgtls, ppint32(32), aap, iqlibc.ppUint64FromInt64(ppint64(1)+(aap+ppint64(3))/ppint64(5)))
		goto cg_5
	cg_5:
		;
		aap++
	}

	/* error < 1e-3 */
	sitest_ndigits_aux(cgtls, ppint32(57), ppint64(35), ppuint64(8))

	/* error < 1e-4 */
	sitest_ndigits_aux(cgtls, ppint32(31), ppint64(649), ppuint64(133))

	/* error < 1e-5 */
	sitest_ndigits_aux(cgtls, ppint32(43), ppint64(5041), ppuint64(931))

	/* error < 1e-6 */
	sitest_ndigits_aux(cgtls, ppint32(41), ppint64(17771), ppuint64(3319))

	/* 20th convergent of log(2)/log(3) */
	sitest_ndigits_aux(cgtls, ppint32(3), ppint64(630138897), ppuint64(397573381))

	/* 21st convergent of log(2)/log(3) */
	sitest_ndigits_aux(cgtls, ppint32(3), ppint64(9809721694), ppuint64(6189245292))

	/* 22nd convergent of log(2)/log(3) */
	sitest_ndigits_aux(cgtls, ppint32(3), ppint64(10439860591), ppuint64(6586818672))

	/* 23rd convergent of log(2)/log(3) */
	sitest_ndigits_aux(cgtls, ppint32(3), ppint64(103768467013), ppuint64(65470613322))

	/* 24th convergent of log(2)/log(3) */
	sitest_ndigits_aux(cgtls, ppint32(3), ppint64(217976794617), ppuint64(137528045314))

	sitest_ndigits_aux(cgtls, ppint32(3), ppint64(1193652440098), ppuint64(753110839882))

	sitest_ndigits_aux(cgtls, ppint32(3), ppint64(683381996816440), ppuint64(431166034846569))

	sitest_ndigits_aux(cgtls, ppint32(7), ppint64(186564318007), ppuint64(66455550933))
}

// C documentation
//
//	/* check corner cases: radix-b number near from b^e converted to mpfr_t
//	   and then back to radix b */
func sicheck_corner(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aab, aai, aaret ppint32
	var aae tnmpfr_exp_t
	var aaiprec, aaoprec tnmpfr_prec_t
	var aas, aat ppuintptr
	var ccv10, ccv5, ccv6, ccv8, ccv9 ppbool
	var pp_ /* f at bp+0 */ tnmpfr_exp_t
	var pp_ /* x at bp+8 */ tnmpfr_t
	var pp_ /* y at bp+40 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aab, aae, aai, aaiprec, aaoprec, aaret, aas, aat, ccv10, ccv5, ccv6, ccv8, ccv9

	aab = ppint32(2)
	for {
		if !(aab <= ppint32(62)) {
			break
		}
		aae = ppint64(-ppint32(100))
		for {
			if !(aae <= ppint64(100)) {
				break
			}
			aaiprec = ppint64(mvMPFR_PREC_MIN)
			for {
				if !(aaiprec <= ppint64(100)) {
					break
				}

				aaoprec = iqlibc.ppInt64FromUint64(Xmpfr_get_str_ndigits(cgtls, aab, aaiprec))
				aas = Xtests_allocate(cgtls, iqlibc.ppUint64FromInt64(aaoprec+ppint64(6)))
				/* oprec characters for the significand, 1 for the '@' sign,
				   at most 4 for the exponent (-100), and 1 for the terminating
				   '\0'. */
				aat = Xtests_allocate(cgtls, iqlibc.ppUint64FromInt64(aaoprec+ppint64(6)))
				Xmpfr_init2(cgtls, cgbp+8, aaiprec)
				Xmpfr_init2(cgtls, cgbp+40, aaiprec)
				/* set s to 1000...000Ee */
				*(*ppuint8)(iqunsafe.ppPointer(aas)) = ppuint8('1')
				aai = ppint32(1)
				for {
					if !(ppint64(aai) < aaoprec) {
						break
					}
					*(*ppuint8)(iqunsafe.ppPointer(aas + ppuintptr(aai))) = ppuint8('0')
					goto cg_4
				cg_4:
					;
					aai++
				}
				*(*ppuint8)(iqunsafe.ppPointer(aas + ppuintptr(aaoprec))) = ppuint8('@')
				aaret = Xsprintf(cgtls, aas+ppuintptr(aaoprec)+ppuintptr(1), "%ld\x00", iqlibc.ppVaList(cgbp+80, aae))

				if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aaret <= iqlibc.ppInt32FromInt32(4))), ppint64(1)) != 0; !ccv5 {
					Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1440), "ret <= 4\x00")
				}
				pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				/* sprintf prints the terminating null byte */
				aaret = Xmpfr_set_str(cgtls, cgbp+8, aas, aab, ppint32(ecMPFR_RNDN))

				if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aaret == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv6 {
					Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1443), "ret == 0\x00")
				}
				pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				Xmpfr_get_str(cgtls, aat, cgbp, aab, ppuint64(0), cgbp+8, ppint32(ecMPFR_RNDN))

				if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xstrlen(cgtls, aat) == iqlibc.ppUint64FromInt64(aaoprec))), ppint64(1)) != 0; !ccv8 {
					Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1445), "strlen (t) == oprec\x00")
					if !(iqlibc.Bool(!(0 != 0)) || Xstrlen(cgtls, aat) == iqlibc.ppUint64FromInt64(aaoprec)) {
						X__builtin_unreachable(cgtls)
					}
				}
				pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				*(*ppuint8)(iqunsafe.ppPointer(aat + ppuintptr(aaoprec))) = ppuint8('@')
				aaret = Xsprintf(cgtls, aat+ppuintptr(aaoprec)+ppuintptr(1), "%ld\x00", iqlibc.ppVaList(cgbp+80, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp))-aaoprec))

				if ccv9 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aaret <= iqlibc.ppInt32FromInt32(4))), ppint64(1)) != 0; !ccv9 {
					Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1448), "ret <= 4\x00")
				}
				pp_ = ccv9 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				aaret = Xmpfr_set_str(cgtls, cgbp+40, aat, aab, ppint32(ecMPFR_RNDN))

				if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aaret == iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv10 {
					Xmpfr_assert_fail(cgtls, "tget_str.c\x00", ppint32(1450), "ret == 0\x00")
				}
				pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
				if !(Xmpfr_equal_p(cgtls, cgbp+8, cgbp+40) != 0) {

					Xprintf(cgtls, "mpfr_set_str o mpfr_get_str <> Id for b=%d\n\x00", iqlibc.ppVaList(cgbp+80, aab))
					Xprintf(cgtls, "x=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+8)
					*(*ppuint8)(iqunsafe.ppPointer(aat + ppuintptr(aaoprec))) = ppuint8('\000')
					Xprintf(cgtls, "mpfr_get_str converted to 0.%s@%ld\n\x00", iqlibc.ppVaList(cgbp+80, aat, *(*tnmpfr_exp_t)(iqunsafe.ppPointer(cgbp))))
					Xprintf(cgtls, "mpfr_set_str converted to:\n\x00", 0)
					Xprintf(cgtls, "y=\x00", 0)
					Xmpfr_dump(cgtls, cgbp+40)
					Xexit(cgtls, ppint32(1))
				}
				Xmpfr_clear(cgtls, cgbp+8)
				Xmpfr_clear(cgtls, cgbp+40)
				Xtests_free(cgtls, aas, iqlibc.ppUint64FromInt64(aaoprec+ppint64(6)))
				Xtests_free(cgtls, aat, iqlibc.ppUint64FromInt64(aaoprec+ppint64(6)))

				goto cg_3
			cg_3:
				;
				aaiprec++
			}
			goto cg_2
		cg_2:
			;
			aae++
		}
		goto cg_1
	cg_1:
		;
		aab++
	}
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(144)
	defer cgtls.ppFree(144)

	var aab, aai ppint32
	var aae tnmpfr_exp_t
	var aam tnsize_t
	var aap tnmpfr_prec_t
	var aar tnmpfr_rnd_t
	var ccv4, ccv5 ppint64
	var pp_ /* f at bp+136 */ tnmpfr_exp_t
	var pp_ /* s at bp+32 */ [102]ppuint8
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aab, aae, aai, aam, aap, aar, ccv4, ccv5

	Xtests_start_mpfr(cgtls)

	sicheck_corner(cgtls)
	sitest_ndigits(cgtls)
	sicoverage(cgtls)
	sicheck_small(cgtls)

	sicheck_special(cgtls, ppint32(2), ppint64(2))
	aai = 0
	for {
		if !(aai < ppint32(mvITER)) {
			break
		}

		aap = iqlibc.ppInt64FromUint64(ppuint64(2) + Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvMAX_DIGITS)-iqlibc.ppInt32FromInt32(1)))
		aab = iqlibc.ppInt32FromUint64(ppuint64(2) + Xrandlimb(cgtls)%ppuint64(35))
		sicheck_special(cgtls, aab, aap)

		goto cg_1
	cg_1:
		;
		aai++
	}

	Xmpfr_init2(cgtls, cgbp, ppint64(mvMAX_DIGITS))
	aai = 0
	for {
		if !(aai < ppint32(mvITER)) {
			break
		}

		aam = ppuint64(2) + Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvMAX_DIGITS)-iqlibc.ppInt32FromInt32(1))
		if !(Xmpfr_rands_initialized != 0) {
			Xmpfr_rands_initialized = ppuint8(1)
			X__gmp_randinit_default(cgtls, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
			pp_ = iqlibc.ppInt32FromInt32(0)
		}
		Xmpfr_urandomb(cgtls, cgbp, ppuintptr(iqunsafe.ppPointer(&Xmpfr_rands)))
		aae = iqlibc.ppInt64FromUint64(Xrandlimb(cgtls)%iqlibc.ppUint64FromInt32(21)) - ppint64(10)
		if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) {
			if aae == ppint64(-ppint32(10)) {
				ccv4 = X__gmpfr_emin
			} else {
				if aae == ppint64(10) {
					ccv5 = X__gmpfr_emax
				} else {
					ccv5 = aae
				}
				ccv4 = ccv5
			}
			Xmpfr_set_exp(cgtls, cgbp, ccv4)
		}
		aab = iqlibc.ppInt32FromUint64(ppuint64(2) + Xrandlimb(cgtls)%ppuint64(35))
		aar = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt32(ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)))
		Xmpfr_get_str(cgtls, cgbp+32, cgbp+136, aab, aam, cgbp, aar)

		goto cg_2
	cg_2:
		;
		aai++
	}
	Xmpfr_clear(cgtls, cgbp)

	sicheck_large(cgtls)
	sicheck3(cgtls, "4.059650008e-83\x00", ppint32(ecMPFR_RNDN), "40597\x00")
	sicheck3(cgtls, "-6.606499965302424244461355e233\x00", ppint32(ecMPFR_RNDN), "-66065\x00")
	sicheck3(cgtls, "-7.4\x00", ppint32(ecMPFR_RNDN), "-74000\x00")
	sicheck3(cgtls, "0.997\x00", ppint32(ecMPFR_RNDN), "99700\x00")
	sicheck3(cgtls, "-4.53063926135729747564e-308\x00", ppint32(ecMPFR_RNDN), "-45306\x00")
	sicheck3(cgtls, "2.14478198760196000000e+16\x00", ppint32(ecMPFR_RNDN), "21448\x00")
	sicheck3(cgtls, "7.02293374921793516813e-84\x00", ppint32(ecMPFR_RNDN), "70229\x00")

	sicheck3(cgtls, "-6.7274500420134077e-87\x00", ppint32(ecMPFR_RNDN), "-67275\x00")
	sicheck3(cgtls, "-6.7274500420134077e-87\x00", ppint32(ecMPFR_RNDZ), "-67274\x00")
	sicheck3(cgtls, "-6.7274500420134077e-87\x00", ppint32(ecMPFR_RNDU), "-67274\x00")
	sicheck3(cgtls, "-6.7274500420134077e-87\x00", ppint32(ecMPFR_RNDD), "-67275\x00")
	sicheck3(cgtls, "-6.7274500420134077e-87\x00", ppint32(ecMPFR_RNDA), "-67275\x00")

	sicheck3(cgtls, "6.7274500420134077e-87\x00", ppint32(ecMPFR_RNDN), "67275\x00")
	sicheck3(cgtls, "6.7274500420134077e-87\x00", ppint32(ecMPFR_RNDZ), "67274\x00")
	sicheck3(cgtls, "6.7274500420134077e-87\x00", ppint32(ecMPFR_RNDU), "67275\x00")
	sicheck3(cgtls, "6.7274500420134077e-87\x00", ppint32(ecMPFR_RNDD), "67274\x00")
	sicheck3(cgtls, "6.7274500420134077e-87\x00", ppint32(ecMPFR_RNDA), "67275\x00")

	sicheck_bug_base2k(cgtls)
	sicheck_reduced_exprange(cgtls)
	sicheck_inex(cgtls)
	sicheck_negative_base(cgtls)

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___builtin_unreachable(*iqlibc.ppTLS)

func ___gmp_randinit_default(*iqlibc.ppTLS, ppuintptr)

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuint64

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _mpfr_add(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_add_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_div_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_free_str(*iqlibc.ppTLS, ppuintptr)

func _mpfr_get_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuintptr

func _mpfr_get_str_ndigits(*iqlibc.ppTLS, ppint32, ppint64) ppuint64

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_mul_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_mul_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttoinf(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttozero(*iqlibc.ppTLS, ppuintptr)

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

var _mpfr_rands [1]tn__gmp_randstate_struct

var _mpfr_rands_initialized ppuint8

func _mpfr_set_exp(*iqlibc.ppTLS, ppuintptr, ppint64) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_sub_ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_urandomb(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _puts(*iqlibc.ppTLS, ppuintptr) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

func _sprintf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr) ppint32

var _stdout ppuintptr

func _strcmp(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _strlen(*iqlibc.ppTLS, ppuintptr) ppuint64

func _strncmp(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64) ppint32

func _tests_allocate(*iqlibc.ppTLS, ppuint64) ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_free(*iqlibc.ppTLS, ppuintptr, ppuint64)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

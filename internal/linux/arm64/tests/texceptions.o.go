// Code generated for linux/arm64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IEEE_QUAD_LITTLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/arm64 -UHAVE_NEARBYINT -c -o texceptions.o.go texceptions.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IEEE_QUAD_LITTLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1311
const mvMPFR_AI_THRESHOLD3 = 19661
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 25
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 100
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 25000
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 20
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 30000
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 20
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "default"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_L = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__AARCH64EL__ = 1
const mv__AARCH64_CMODEL_SMALL__ = 1
const mv__ARM_64BIT_STATE = 1
const mv__ARM_ALIGN_MAX_PWR = 28
const mv__ARM_ALIGN_MAX_STACK_PWR = 16
const mv__ARM_ARCH = 8
const mv__ARM_ARCH_8A = 1
const mv__ARM_ARCH_ISA_A64 = 1
const mv__ARM_ARCH_PROFILE = 65
const mv__ARM_FEATURE_CLZ = 1
const mv__ARM_FEATURE_FMA = 1
const mv__ARM_FEATURE_IDIV = 1
const mv__ARM_FEATURE_NUMERIC_MAXMIN = 1
const mv__ARM_FEATURE_UNALIGNED = 1
const mv__ARM_FP = 14
const mv__ARM_FP16_ARGS = 1
const mv__ARM_FP16_FORMAT_IEEE = 1
const mv__ARM_NEON = 1
const mv__ARM_PCS_AAPCS64 = 1
const mv__ARM_SIZEOF_MINIMAL_ENUM = 4
const mv__ARM_SIZEOF_WCHAR_T = 4
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 36
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 2
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_C99__ = 0
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 256
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "aarch64-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 36
const mv__LDBL_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__LDBL_DIG__ = 33
const mv__LDBL_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 113
const mv__LDBL_MAX_10_EXP__ = 4932
const mv__LDBL_MAX_EXP__ = 16384
const mv__LDBL_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LDBL_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__LDBL_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0xffffffff
const mv__WCHAR_MIN__ = 0
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__aarch64__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppuint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppuint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

// C documentation
//
//	/* Test powerof2 */
func sicheck_powerof2(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var ccv2, ccv4 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_ = ccv2, ccv4

	Xmpfr_init(cgtls, cgbp)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_powerof2_raw(cgtls, cgbp) != 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(35), "mpfr_powerof2_raw (x)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_powerof2_raw(cgtls, cgbp) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(3)), 0, ppint32(ecMPFR_RNDN))

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(Xmpfr_powerof2_raw(cgtls, cgbp) != 0)), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(37), "!mpfr_powerof2_raw (x)\x00")
		if !(iqlibc.Bool(!(0 != 0)) || !(Xmpfr_powerof2_raw(cgtls, cgbp) != 0)) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp)
}

// C documentation
//
//	/* Test default rounding mode */
func sicheck_default_rnd(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var aar ppint32
	var aat tnmpfr_rnd_t
	pp_, pp_ = aar, aat
	aar = 0
	for {
		if !(aar < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
			break
		}

		Xmpfr_set_default_rounding_mode(cgtls, aar)
		aat = Xmpfr_get_default_rounding_mode(cgtls)
		if aar != aat {

			Xprintf(cgtls, "%s %s\n\x00", iqlibc.ppVaList(cgbp+8, Xmpfr_print_rnd_mode(cgtls, aar), Xmpfr_print_rnd_mode(cgtls, aat)))
			Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "ERROR in setting / getting default rounding mode (1)\x00"))
			Xexit(cgtls, ppint32(1))
		}

		goto cg_1
	cg_1:
		;
		aar++
	}
	Xmpfr_set_default_rounding_mode(cgtls, ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1))
	if X__gmpfr_default_rounding_mode != ppint32(ecMPFR_RNDF) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "ERROR in setting / getting default rounding mode (2)\x00"))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_default_rounding_mode(cgtls, -iqlibc.ppInt32FromInt32(1))
	if X__gmpfr_default_rounding_mode != ppint32(ecMPFR_RNDF) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "ERROR in setting / getting default rounding mode (3)\x00"))
		Xexit(cgtls, ppint32(1))
	}
}

func sicheck_emin_emax(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	var aaold_emax, aaold_emin tnmpfr_exp_t
	pp_, pp_ = aaold_emax, aaold_emin

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	/* Check the functions not the macros ! */
	if Xmpfr_set_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))) != 0 {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "set_emin failed!\x00"))
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_get_emin(cgtls) != iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "get_emin FAILED!\x00"))
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_set_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1)) == 0 {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "set_emin failed! (2)\x00"))
		Xexit(cgtls, ppint32(1))
	}

	if Xmpfr_set_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1)) != 0 {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "set_emax failed!\x00"))
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_get_emax(cgtls) != iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "get_emax FAILED!\x00"))
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_set_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) == 0 {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "set_emax failed! (2)\x00"))
		Xexit(cgtls, ppint32(1))
	}

	if Xmpfr_get_emin_min(cgtls) != iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "get_emin_min\x00"))
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_get_emin_max(cgtls) != iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "get_emin_max\x00"))
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_get_emax_min(cgtls) != iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "get_emax_min\x00"))
		Xexit(cgtls, ppint32(1))
	}
	if Xmpfr_get_emax_max(cgtls) != iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "get_emax_max\x00"))
		Xexit(cgtls, ppint32(1))
	}

	Xset_emin(cgtls, aaold_emin)
	Xset_emax(cgtls, aaold_emax)
}

func sicheck_get_prec(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aai ppint32
	var ccv1 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_ = aai, ccv1
	aai = 0

	Xmpfr_init2(cgtls, cgbp, ppint64(17))

	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec != ppint64(17) || Xmpfr_get_prec(cgtls, cgbp) != ppint64(17) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+40, "mpfr_get_prec\x00"))
		Xexit(cgtls, ppint32(1))
	}

	aai++
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec != ppint64(17) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+40, "mpfr_get_prec (2)\x00"))
		Xexit(cgtls, ppint32(1))
	}

	if ccv1 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aai == iqlibc.ppInt32FromInt32(1))), ppint64(1)) != 0; !ccv1 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(125), "i == 1\x00")
	}
	pp_ = ccv1 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear(cgtls, cgbp)
}

func simpfr_set_double_range(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(16)
	defer cgtls.ppFree(16)

	Xmpfr_set_default_prec(cgtls, ppint64(54))
	if X__gmpfr_default_fp_bit_precision != ppint64(54) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "get_default_prec failed (1)\x00"))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_default_prec(cgtls, ppint64(53))
	if Xmpfr_get_default_prec(cgtls) != ppint64(53) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+8, "get_default_prec failed (2)\x00"))
		Xexit(cgtls, ppint32(1))
	}

	/* in double precision format, the unbiased exponent is between 0 and
	   2047, where 0 is used for subnormal numbers, and 2047 for special
	   numbers (infinities, NaN), and the bias is 1023, thus "normal" numbers
	   have an exponent between -1022 and 1023, corresponding to numbers
	   between 2^(-1022) and previous(2^(1024)).
	   (The smallest subnormal number is 0.(0^51)1*2^(-1022)= 2^(-1074).)

	   The smallest normal power of two is 1.0*2^(-1022).
	   The largest normal power of two is 2^1023.
	   (We have to add one for mpfr since mantissa are between 1/2 and 1.)
	*/

	Xset_emin(cgtls, ppint64(-ppint32(1021)))
	Xset_emax(cgtls, ppint64(1024))
}

func sicheck_flags(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aaold_emax, aaold_emin tnmpfr_exp_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_ = aaold_emax, aaold_emin

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax
	Xmpfr_init(cgtls, cgbp)

	/* Check the functions not the macros ! */
	Xmpfr_clear_flags(cgtls)
	simpfr_set_double_range(cgtls)

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_clear_overflow(cgtls)
	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(1024), ppint32(ecMPFR_RNDN))
	if !(Xmpfr_overflow_p(cgtls) != 0) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+40, "ERROR: No overflow detected!\n\x00"))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear_underflow(cgtls)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(1025), ppint32(ecMPFR_RNDN))
	if !(Xmpfr_underflow_p(cgtls) != 0) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+40, "ERROR: No underflow detected!\n\x00"))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear_nanflag(cgtls)
	(*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp = -iqlibc.ppInt64FromInt64(0x7fffffffffffffff) - iqlibc.ppInt64FromInt32(1) + iqlibc.ppInt64FromInt32(2)
	Xmpfr_add(cgtls, cgbp, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if !(Xmpfr_nanflag_p(cgtls) != 0) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+40, "ERROR: No NaN flag!\n\x00"))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear_inexflag(cgtls)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_cos(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if !(Xmpfr_inexflag_p(cgtls) != 0) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+40, "ERROR: No inexact flag!\n\x00"))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear_erangeflag(cgtls)
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(1024), ppint32(ecMPFR_RNDN))
	Xmpfr_get_ui(cgtls, cgbp, ppint32(ecMPFR_RNDN))
	if !(Xmpfr_erangeflag_p(cgtls) != 0) {
		Xprintf(cgtls, "%s\n\x00", iqlibc.ppVaList(cgbp+40, "ERROR: No erange flag!\n\x00"))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xset_emin(cgtls, aaold_emin)
	Xset_emax(cgtls, aaold_emax)
}

func sitest_set_underflow(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aa_p, aa_p1, ccv3, ccv4, ccv5 tnmpfr_ptr
	var aai, aainex, aaj, aas, ccv1, ccv2, ccv8 ppint32
	var aar [6]tnmpfr_ptr
	var aat [6]ppint32
	var pp_ /* min at bp+64 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* zero at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aai, aainex, aaj, aar, aas, aat, ccv1, ccv2, ccv3, ccv4, ccv5, ccv8
	aat = [6]ppint32{
		0: ppint32(1),
		1: -ppint32(1),
		2: ppint32(1),
		3: -ppint32(1),
		4: ppint32(1),
	}

	Xmpfr_inits(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	{
		aa_p = cgbp + 32
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1
	{
		aa_p1 = cgbp + 64
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv2 = 0
	}
	pp_ = ccv2
	Xmpfr_nextabove(cgtls, cgbp+64)
	ccv4 = cgbp + 64
	aar[ppint32(4)] = ccv4
	ccv3 = ccv4
	aar[ppint32(2)] = ccv3
	aar[0] = ccv3 /* RNDN, RNDU, RNDA */
	ccv5 = cgbp + 32
	aar[ppint32(3)] = ccv5
	aar[ppint32(1)] = ccv5 /* RNDZ, RNDD */
	aas = ppint32(1)
	for {
		if !(aas > 0) {
			break
		}

		aai = 0
		for {
			if !(aai < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}

			if aas < 0 && aai > ppint32(1) {
				ccv8 = ppint32(5) - aai
			} else {
				ccv8 = aai
			}
			aaj = ccv8
			aainex = Xmpfr_underflow(cgtls, cgbp, aai, aas)
			/* for RNDF, inex has no meaning, just check that x is either
			   min or zero */
			if aai == ppint32(ecMPFR_RNDF) {

				if Xmpfr_cmp3(cgtls, cgbp, cgbp+64, ppint32(1)) != 0 && Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

					Xprintf(cgtls, "Error in test_set_underflow, sign = %d, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+104, aas, Xmpfr_print_rnd_mode(cgtls, aai)))
					Xprintf(cgtls, "Got\n\x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\ninstead of\n\x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\nor\n\x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+64, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\n\x00", 0)
					Xexit(cgtls, ppint32(1))
				}
			} else {
				if Xmpfr_cmp3(cgtls, cgbp, aar[aaj], ppint32(1)) != 0 || aainex*aat[aaj] <= 0 {

					Xprintf(cgtls, "Error in test_set_underflow, sign = %d, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+104, aas, Xmpfr_print_rnd_mode(cgtls, aai)))
					Xprintf(cgtls, "Got\n\x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, ", inex = %d\ninstead of\n\x00", iqlibc.ppVaList(cgbp+104, aainex))
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), aar[aaj], ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, ", inex = %d\n\x00", iqlibc.ppVaList(cgbp+104, aat[aaj]))
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_7
		cg_7:
			;
			aai++
		}
		Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))
		Xmpfr_neg(cgtls, cgbp+64, cgbp+64, ppint32(ecMPFR_RNDN))

		goto cg_6
	cg_6:
		;
		aas = -ppint32(1)
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

func sitest_set_overflow(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aai, aainex, aaj, aas, ccv6 ppint32
	var aar [6]tnmpfr_ptr
	var aat [6]ppint32
	var ccv1, ccv2, ccv3 tnmpfr_ptr
	var pp_ /* inf at bp+32 */ tnmpfr_t
	var pp_ /* max at bp+64 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aai, aainex, aaj, aar, aas, aat, ccv1, ccv2, ccv3, ccv6
	aat = [6]ppint32{
		0: ppint32(1),
		1: -ppint32(1),
		2: ppint32(1),
		3: -ppint32(1),
		4: ppint32(1),
	}

	Xmpfr_inits2(cgtls, ppint64(32), cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_inf(cgtls, cgbp+32, ppint32(1))
	Xmpfr_set_inf(cgtls, cgbp+64, ppint32(1))
	Xmpfr_nextbelow(cgtls, cgbp+64)
	ccv2 = cgbp + 32
	aar[ppint32(4)] = ccv2
	ccv1 = ccv2
	aar[ppint32(2)] = ccv1
	aar[0] = ccv1 /* RNDN, RNDU, RNDA */
	ccv3 = cgbp + 64
	aar[ppint32(3)] = ccv3
	aar[ppint32(1)] = ccv3 /* RNDZ, RNDD */
	aas = ppint32(1)
	for {
		if !(aas > 0) {
			break
		}

		aai = 0
		for {
			if !(aai < ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)) {
				break
			}

			if aas < 0 && aai > ppint32(1) {
				ccv6 = ppint32(5) - aai
			} else {
				ccv6 = aai
			}
			aaj = ccv6
			aainex = Xmpfr_overflow(cgtls, cgbp, aai, aas)
			/* for RNDF, inex has no meaning, just check that x is either
			   max or inf */
			if aai == ppint32(ecMPFR_RNDF) {

				if Xmpfr_cmp3(cgtls, cgbp, cgbp+64, ppint32(1)) != 0 && Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

					Xprintf(cgtls, "Error in test_set_overflow, sign = %d, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+104, aas, Xmpfr_print_rnd_mode(cgtls, aai)))
					Xprintf(cgtls, "Got\n\x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\ninstead of\n\x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+64, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\nor\n\x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, "\n\x00", 0)
					Xexit(cgtls, ppint32(1))
				}
			} else {
				if Xmpfr_cmp3(cgtls, cgbp, aar[aaj], ppint32(1)) != 0 || aainex*aat[aaj] <= 0 {

					Xprintf(cgtls, "Error in test_set_overflow, sign = %d, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+104, aas, Xmpfr_print_rnd_mode(cgtls, aai)))
					Xprintf(cgtls, "Got\n\x00", 0)
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, ", inex = %d\ninstead of\n\x00", iqlibc.ppVaList(cgbp+104, aainex))
					X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), aar[aaj], ppint32(ecMPFR_RNDN))
					Xprintf(cgtls, ", inex = %d\n\x00", iqlibc.ppVaList(cgbp+104, aat[aaj]))
					Xexit(cgtls, ppint32(1))
				}
			}

			goto cg_5
		cg_5:
			;
			aai++
		}
		Xmpfr_neg(cgtls, cgbp+32, cgbp+32, ppint32(ecMPFR_RNDN))
		Xmpfr_neg(cgtls, cgbp+64, cgbp+64, ppint32(ecMPFR_RNDN))

		goto cg_4
	cg_4:
		;
		aas = -ppint32(1)
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

func sicheck_set(cgtls *iqlibc.ppTLS) {

	var ccv10, ccv12, ccv13, ccv15, ccv17, ccv19, ccv2, ccv21, ccv23, ccv25, ccv26, ccv27, ccv28, ccv29, ccv30, ccv31, ccv32, ccv33, ccv34, ccv35, ccv36, ccv37, ccv38, ccv39, ccv4, ccv40, ccv6, ccv8 ppbool
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = ccv10, ccv12, ccv13, ccv15, ccv17, ccv19, ccv2, ccv21, ccv23, ccv25, ccv26, ccv27, ccv28, ccv29, ccv30, ccv31, ccv32, ccv33, ccv34, ccv35, ccv36, ccv37, ccv38, ccv39, ccv4, ccv40, ccv6, ccv8
	Xmpfr_clear_flags(cgtls)

	Xmpfr_set_overflow(cgtls)

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_overflow_p(cgtls) != 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(331), "(mpfr_overflow_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_overflow_p(cgtls) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_underflow(cgtls)

	if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_underflow_p(cgtls) != 0)), ppint64(1)) != 0; !ccv4 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(333), "(mpfr_underflow_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_underflow_p(cgtls) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_divby0(cgtls)

	if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_divby0_p(cgtls) != 0)), ppint64(1)) != 0; !ccv6 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(335), "(mpfr_divby0_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_divby0_p(cgtls) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_nanflag(cgtls)

	if ccv8 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_nanflag_p(cgtls) != 0)), ppint64(1)) != 0; !ccv8 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(337), "(mpfr_nanflag_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_nanflag_p(cgtls) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv8 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inexflag(cgtls)

	if ccv10 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_inexflag_p(cgtls) != 0)), ppint64(1)) != 0; !ccv10 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(339), "(mpfr_inexflag_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_inexflag_p(cgtls) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv10 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_erangeflag(cgtls)

	if ccv12 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_erangeflag_p(cgtls) != 0)), ppint64(1)) != 0; !ccv12 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(341), "(mpfr_erangeflag_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_erangeflag_p(cgtls) != 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv12 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)))), ppint64(1)) != 0; !ccv13 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(343), "__gmpfr_flags == (1 | 2 | 4 | 8 | 16 | 32)\x00")
	}
	pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear_overflow(cgtls)

	if ccv15 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(Xmpfr_overflow_p(cgtls) != 0)), ppint64(1)) != 0; !ccv15 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(346), "! (mpfr_overflow_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || !(Xmpfr_overflow_p(cgtls) != 0)) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv15 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_underflow(cgtls)

	if ccv17 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(Xmpfr_underflow_p(cgtls) != 0)), ppint64(1)) != 0; !ccv17 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(348), "! (mpfr_underflow_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || !(Xmpfr_underflow_p(cgtls) != 0)) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv17 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_divby0(cgtls)

	if ccv19 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(Xmpfr_divby0_p(cgtls) != 0)), ppint64(1)) != 0; !ccv19 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(350), "! (mpfr_divby0_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || !(Xmpfr_divby0_p(cgtls) != 0)) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv19 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_nanflag(cgtls)

	if ccv21 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(Xmpfr_nanflag_p(cgtls) != 0)), ppint64(1)) != 0; !ccv21 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(352), "! (mpfr_nanflag_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || !(Xmpfr_nanflag_p(cgtls) != 0)) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv21 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_inexflag(cgtls)

	if ccv23 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(Xmpfr_inexflag_p(cgtls) != 0)), ppint64(1)) != 0; !ccv23 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(354), "! (mpfr_inexflag_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || !(Xmpfr_inexflag_p(cgtls) != 0)) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv23 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_erangeflag(cgtls)

	if ccv25 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(Xmpfr_erangeflag_p(cgtls) != 0)), ppint64(1)) != 0; !ccv25 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(356), "! (mpfr_erangeflag_p) ()\x00")
		if !(iqlibc.Bool(!(0 != 0)) || !(Xmpfr_erangeflag_p(cgtls) != 0)) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv25 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv26 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv26 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(358), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv26 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_set_overflow(cgtls)

	if ccv27 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0)), ppint64(1)) != 0; !ccv27 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(361), "((int) (__gmpfr_flags & 2))\x00")
	}
	pp_ = ccv27 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_underflow(cgtls)

	if ccv28 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv28 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(363), "((int) (__gmpfr_flags & 1))\x00")
	}
	pp_ = ccv28 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_divby0(cgtls)

	if ccv29 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0)), ppint64(1)) != 0; !ccv29 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(365), "((int) (__gmpfr_flags & 32))\x00")
	}
	pp_ = ccv29 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_nanflag(cgtls)

	if ccv30 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0)), ppint64(1)) != 0; !ccv30 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(367), "((int) (__gmpfr_flags & 4))\x00")
	}
	pp_ = ccv30 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_inexflag(cgtls)

	if ccv31 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv31 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(369), "((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv31 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_set_erangeflag(cgtls)

	if ccv32 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0)), ppint64(1)) != 0; !ccv32 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(371), "((int) (__gmpfr_flags & 16))\x00")
	}
	pp_ = ccv32 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv33 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)))), ppint64(1)) != 0; !ccv33 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(373), "__gmpfr_flags == (1 | 2 | 4 | 8 | 16 | 32)\x00")
	}
	pp_ = ccv33 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	Xmpfr_clear_overflow(cgtls)

	if ccv34 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0)), ppint64(1)) != 0; !ccv34 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(376), "! ((int) (__gmpfr_flags & 2))\x00")
	}
	pp_ = ccv34 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_underflow(cgtls)

	if ccv35 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0)), ppint64(1)) != 0; !ccv35 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(378), "! ((int) (__gmpfr_flags & 1))\x00")
	}
	pp_ = ccv35 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_divby0(cgtls)

	if ccv36 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0)), ppint64(1)) != 0; !ccv36 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(380), "! ((int) (__gmpfr_flags & 32))\x00")
	}
	pp_ = ccv36 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_nanflag(cgtls)

	if ccv37 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0)), ppint64(1)) != 0; !ccv37 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(382), "! ((int) (__gmpfr_flags & 4))\x00")
	}
	pp_ = ccv37 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_inexflag(cgtls)

	if ccv38 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) != 0)), ppint64(1)) != 0; !ccv38 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(384), "! ((int) (__gmpfr_flags & 8))\x00")
	}
	pp_ = ccv38 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	Xmpfr_clear_erangeflag(cgtls)

	if ccv39 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0)), ppint64(1)) != 0; !ccv39 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(386), "! ((int) (__gmpfr_flags & 16))\x00")
	}
	pp_ = ccv39 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	if ccv40 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == iqlibc.ppUint32FromInt32(0))), ppint64(1)) != 0; !ccv40 {
		Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(388), "__gmpfr_flags == 0\x00")
	}
	pp_ = ccv40 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
}

// C documentation
//
//	/* Note: this test assumes that mpfr_flags_* can be implemented as both
//	   a function and a macro. Thus in such a case, both implementations are
//	   tested. */
func sicheck_groups(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(48)
	defer cgtls.ppFree(48)

	var aaf1, aaf2, aamask tnmpfr_flags_t
	var aai, aaj ppint32
	var ccv10, ccv11, ccv13, ccv15, ccv8 ppuint32
	var ccv3, ccv5, ccv6 ppbool
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaf1, aaf2, aai, aaj, aamask, ccv10, ccv11, ccv13, ccv15, ccv3, ccv5, ccv6, ccv8
	aai = 0
	for {
		if !(aai < ppint32(200)) {
			break
		}

		aaf1 = X__gmpfr_flags

		if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_flags_save(cgtls) == aaf1)), ppint64(1)) != 0; !ccv3 {
			Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(403), "mpfr_flags_save () == f1\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_flags_save(cgtls) == aaf1) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_flags_save(cgtls) == aaf1)), ppint64(1)) != 0; !ccv5 {
			Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(404), "(mpfr_flags_save) () == f1\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_flags_save(cgtls) == aaf1) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(X__gmpfr_flags == aaf1)), ppint64(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(405), "__gmpfr_flags == f1\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		aamask = ppuint32(Xrandlimb(cgtls) & iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)))
		if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
			Xmpfr_flags_set(cgtls, aamask)
		} else {
			Xmpfr_flags_set(cgtls, aamask)
		}
		aaj = ppint32(1)
		for {
			if !(aaj <= iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)) {
				break
			}
			if aamask&iqlibc.ppUint32FromInt32(aaj) != ppuint32(0) {
				ccv8 = iqlibc.ppUint32FromInt32(aaj)
			} else {
				ccv8 = aaf1 & iqlibc.ppUint32FromInt32(aaj)
			}
			if X__gmpfr_flags&iqlibc.ppUint32FromInt32(aaj) != ccv8 {

				Xprintf(cgtls, "mpfr_flags_set error: old = 0x%lx, group = 0x%lx, new = 0x%lx, j = 0x%lx\n\x00", iqlibc.ppVaList(cgbp+8, ppuint64(aaf1), ppuint64(aamask), ppuint64(X__gmpfr_flags), iqlibc.ppUint64FromInt32(aaj)))
				Xexit(cgtls, ppint32(1))
			}
			goto cg_7
		cg_7:
			;
			aaj <<= ppint32(1)
		}

		aaf2 = X__gmpfr_flags
		aamask = ppuint32(Xrandlimb(cgtls) & iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)))
		if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
			Xmpfr_flags_clear(cgtls, aamask)
		} else {
			Xmpfr_flags_clear(cgtls, aamask)
		}
		aaj = ppint32(1)
		for {
			if !(aaj <= iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)) {
				break
			}
			if aamask&iqlibc.ppUint32FromInt32(aaj) != ppuint32(0) {
				ccv10 = ppuint32(0)
			} else {
				ccv10 = aaf2 & iqlibc.ppUint32FromInt32(aaj)
			}
			if X__gmpfr_flags&iqlibc.ppUint32FromInt32(aaj) != ccv10 {

				Xprintf(cgtls, "mpfr_flags_clear error: old = 0x%lx, group = 0x%lx, new = 0x%lx, j = 0x%lx\n\x00", iqlibc.ppVaList(cgbp+8, ppuint64(aaf2), ppuint64(aamask), ppuint64(X__gmpfr_flags), iqlibc.ppUint64FromInt32(aaj)))
				Xexit(cgtls, ppint32(1))
			}
			goto cg_9
		cg_9:
			;
			aaj <<= ppint32(1)
		}

		aamask = ppuint32(Xrandlimb(cgtls) & iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)))
		if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
			ccv11 = X__gmpfr_flags & aamask
		} else {
			ccv11 = Xmpfr_flags_test(cgtls, aamask)
		}
		aaf2 = ccv11
		aaj = ppint32(1)
		for {
			if !(aaj <= iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)) {
				break
			}
			if aamask&iqlibc.ppUint32FromInt32(aaj) != ppuint32(0) {
				ccv13 = X__gmpfr_flags & iqlibc.ppUint32FromInt32(aaj)
			} else {
				ccv13 = ppuint32(0)
			}
			if aaf2&iqlibc.ppUint32FromInt32(aaj) != ccv13 {

				Xprintf(cgtls, "mpfr_flags_test error: current = 0x%lx, mask = 0x%lx, res = 0x%lx, j = 0x%lx\n\x00", iqlibc.ppVaList(cgbp+8, ppuint64(X__gmpfr_flags), ppuint64(aamask), ppuint64(aaf2), iqlibc.ppUint64FromInt32(aaj)))
				Xexit(cgtls, ppint32(1))
			}
			goto cg_12
		cg_12:
			;
			aaj <<= ppint32(1)
		}

		aaf2 = X__gmpfr_flags
		if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
			Xmpfr_flags_restore(cgtls, aaf1, aamask)
		} else {
			Xmpfr_flags_restore(cgtls, aaf1, aamask)
		}
		aaj = ppint32(1)
		for {
			if !(aaj <= iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE)|iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0)) {
				break
			}
			if aamask&iqlibc.ppUint32FromInt32(aaj) != ppuint32(0) {
				ccv15 = aaf1
			} else {
				ccv15 = aaf2
			}
			if X__gmpfr_flags&iqlibc.ppUint32FromInt32(aaj) != ccv15&iqlibc.ppUint32FromInt32(aaj) {

				Xprintf(cgtls, "mpfr_flags_restore error: old = 0x%lx, flags = 0x%lx, mask = 0x%lx, new = 0x%lx, j = 0x%lx\n\x00", iqlibc.ppVaList(cgbp+8, ppuint64(aaf2), ppuint64(aaf1), ppuint64(aamask), ppuint64(X__gmpfr_flags), iqlibc.ppUint64FromInt32(aaj)))
				Xexit(cgtls, ppint32(1))
			}
			goto cg_14
		cg_14:
			;
			aaj <<= ppint32(1)
		}

		goto cg_1
	cg_1:
		;
		aai++
	}
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aaemax, aaemin tnmpfr_exp_t
	var ccv2, ccv3, ccv4, ccv6, ccv7, ccv8 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aaemax, aaemin, ccv2, ccv3, ccv4, ccv6, ccv7, ccv8

	Xtests_start_mpfr(cgtls)

	sitest_set_underflow(cgtls)
	sitest_set_overflow(cgtls)
	sicheck_default_rnd(cgtls)

	Xmpfr_init(cgtls, cgbp)
	Xmpfr_init(cgtls, cgbp+32)

	aaemin = X__gmpfr_emin
	aaemax = X__gmpfr_emax
	if aaemin >= aaemax {

		Xprintf(cgtls, "Error: emin >= emax\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(1024), ppint32(ecMPFR_RNDN))
	simpfr_set_double_range(cgtls)
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp >= X__gmpfr_emin && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= X__gmpfr_emax)), ppint64(1)) != 0 {
	} else {
		Xmpfr_check_range(cgtls, cgbp, 0, ppint32(ecMPFR_RNDN))
	}

	if ccv4 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv4 {
		if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv2 {
			Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(494), "! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv3 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv3 {
			Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(494), "! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv3 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv4 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign <= 0 {

		Xprintf(cgtls, "Error: 2^1024 rounded to nearest should give +Inf\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xset_emax(cgtls, ppint64(1025))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(1024), ppint32(ecMPFR_RNDN))
	simpfr_set_double_range(cgtls)
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp >= X__gmpfr_emin && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= X__gmpfr_emax)), ppint64(1)) != 0 {
	} else {
		Xmpfr_check_range(cgtls, cgbp, 0, ppint32(ecMPFR_RNDD))
	}
	if !(Xmpfr_number_p(cgtls, cgbp) != 0) {

		Xprintf(cgtls, "Error: 2^1024 rounded down should give a normal number\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(1023), ppint32(ecMPFR_RNDN))
	Xmpfr_add(cgtls, cgbp, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv8 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(2)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))); !ccv8 {
		if ccv6 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2))), ppint64(1)) != 0; !ccv6 {
			Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(514), "! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+2))\x00")
		}
		pp_ = ccv6 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1))), ppint64(1)) != 0; !ccv7 {
			Xmpfr_assert_fail(cgtls, "texceptions.c\x00", ppint32(514), "! (((x)->_mpfr_exp) == (((-0x7fffffffffffffffL-1))+1))\x00")
		}
		pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
	}
	if ccv8 || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_sign <= 0 {

		Xprintf(cgtls, "Error: x+x rounded to nearest for x=2^1023 should give +Inf\n\x00", 0)
		Xprintf(cgtls, "emax = %ld\n\x00", iqlibc.ppVaList(cgbp+72, X__gmpfr_emax))
		Xprintf(cgtls, "got \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_mul_2ui(cgtls, cgbp, cgbp, ppuint64(1023), ppint32(ecMPFR_RNDN))
	Xmpfr_add(cgtls, cgbp, cgbp, cgbp, ppint32(ecMPFR_RNDD))
	if !(Xmpfr_number_p(cgtls, cgbp) != 0) {

		Xprintf(cgtls, "Error: x+x rounded down for x=2^1023 should give a normal number\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(1022), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "1.1e-1022\x00") /* y = 3/2*x */
	Xmpfr_sub(cgtls, cgbp+32, cgbp+32, cgbp, ppint32(ecMPFR_RNDZ))
	if Xmpfr_sgn(cgtls, cgbp+32) != 0 {

		Xprintf(cgtls, "Error: y-x rounded to zero should give 0 for y=3/2*2^(-1022), x=2^(-1022)\n\x00", 0)
		Xprintf(cgtls, "Got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xset_emin(cgtls, ppint64(-ppint32(1026)))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_div_2ui(cgtls, cgbp, cgbp, ppuint64(1025), ppint32(ecMPFR_RNDN))
	simpfr_set_double_range(cgtls)
	if X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp >= X__gmpfr_emin && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= X__gmpfr_emax)), ppint64(1)) != 0 {
	} else {
		Xmpfr_check_range(cgtls, cgbp, 0, ppint32(ecMPFR_RNDN))
	}
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) {

		Xprintf(cgtls, "Error: x rounded to nearest for x=2^-1024 should give Zero\n\x00", 0)
		Xprintf(cgtls, "emin = %ld\n\x00", iqlibc.ppVaList(cgbp+72, X__gmpfr_emin))
		Xprintf(cgtls, "got \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)

	Xset_emin(cgtls, aaemin)
	Xset_emax(cgtls, aaemax)

	sicheck_emin_emax(cgtls)
	sicheck_flags(cgtls)
	sicheck_get_prec(cgtls)
	sicheck_powerof2(cgtls)
	sicheck_set(cgtls)
	sicheck_groups(cgtls)

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___builtin_unreachable(*iqlibc.ppTLS)

var ___gmpfr_default_fp_bit_precision ppint64

var ___gmpfr_default_rounding_mode ppint32

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuint64

func _exit(*iqlibc.ppTLS, ppint32)

func _mpfr_add(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_check_range(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_divby0(*iqlibc.ppTLS)

func _mpfr_clear_erangeflag(*iqlibc.ppTLS)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clear_inexflag(*iqlibc.ppTLS)

func _mpfr_clear_nanflag(*iqlibc.ppTLS)

func _mpfr_clear_overflow(*iqlibc.ppTLS)

func _mpfr_clear_underflow(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp3(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_cos(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_div_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_divby0_p(*iqlibc.ppTLS) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_erangeflag_p(*iqlibc.ppTLS) ppint32

func _mpfr_flags_clear(*iqlibc.ppTLS, ppuint32)

func _mpfr_flags_restore(*iqlibc.ppTLS, ppuint32, ppuint32)

func _mpfr_flags_save(*iqlibc.ppTLS) ppuint32

func _mpfr_flags_set(*iqlibc.ppTLS, ppuint32)

func _mpfr_flags_test(*iqlibc.ppTLS, ppuint32) ppuint32

func _mpfr_get_default_prec(*iqlibc.ppTLS) ppint64

func _mpfr_get_default_rounding_mode(*iqlibc.ppTLS) ppint32

func _mpfr_get_emax(*iqlibc.ppTLS) ppint64

func _mpfr_get_emax_max(*iqlibc.ppTLS) ppint64

func _mpfr_get_emax_min(*iqlibc.ppTLS) ppint64

func _mpfr_get_emin(*iqlibc.ppTLS) ppint64

func _mpfr_get_emin_max(*iqlibc.ppTLS) ppint64

func _mpfr_get_emin_min(*iqlibc.ppTLS) ppint64

func _mpfr_get_prec(*iqlibc.ppTLS, ppuintptr) ppint64

func _mpfr_get_ui(*iqlibc.ppTLS, ppuintptr, ppint32) ppuint64

func _mpfr_inexflag_p(*iqlibc.ppTLS) ppint32

func _mpfr_init(*iqlibc.ppTLS, ppuintptr)

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_inits(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_inits2(*iqlibc.ppTLS, ppint64, ppuintptr, ppuintptr)

func _mpfr_mul_2ui(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuint64, ppint32) ppint32

func _mpfr_nanflag_p(*iqlibc.ppTLS) ppint32

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_number_p(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_overflow(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_overflow_p(*iqlibc.ppTLS) ppint32

func _mpfr_powerof2_raw(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_set_default_prec(*iqlibc.ppTLS, ppint64)

func _mpfr_set_default_rounding_mode(*iqlibc.ppTLS, ppint32)

func _mpfr_set_divby0(*iqlibc.ppTLS)

func _mpfr_set_emax(*iqlibc.ppTLS, ppint64) ppint32

func _mpfr_set_emin(*iqlibc.ppTLS, ppint64) ppint32

func _mpfr_set_erangeflag(*iqlibc.ppTLS)

func _mpfr_set_inexflag(*iqlibc.ppTLS)

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nanflag(*iqlibc.ppTLS)

func _mpfr_set_overflow(*iqlibc.ppTLS)

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_set_underflow(*iqlibc.ppTLS)

func _mpfr_sgn(*iqlibc.ppTLS, ppuintptr) ppint32

func _mpfr_sub(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_underflow(*iqlibc.ppTLS, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_underflow_p(*iqlibc.ppTLS) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

var _stdout ppuintptr

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`

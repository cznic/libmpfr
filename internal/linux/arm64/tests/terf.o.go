// Code generated for linux/arm64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -I/tmp/libmpfr/mpfr-4.2.0/ccgo -DMPFR_WANT_PROVEN_CODE=1 -DHAVE_STDIO_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DLT_OBJDIR=".libs/" -DHAVE_LITTLE_ENDIAN=1 -DHAVE_CLOCK_GETTIME=1 -DHAVE_LOCALE_H=1 -DHAVE_WCHAR_H=1 -DHAVE_STDARG=1 -DHAVE_STRUCT_LCONV_DECIMAL_POINT=1 -DHAVE_STRUCT_LCONV_THOUSANDS_SEP=1 -DHAVE_ALLOCA_H=1 -DHAVE_ALLOCA=1 -DHAVE_UINTPTR_T=1 -DHAVE_VA_COPY=1 -DHAVE_SETLOCALE=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_SIGNAL=1 -DHAVE_SIGACTION=1 -DHAVE_LONG_LONG=1 -DHAVE_INTMAX_T=1 -DMPFR_HAVE_INTMAX_MAX=1 -DMPFR_HAVE_NORETURN=1 -DMPFR_HAVE_BUILTIN_UNREACHABLE=1 -DMPFR_HAVE_CONSTRUCTOR_ATTR=1 -DMPFR_HAVE_FESETROUND=1 -DHAVE_SUBNORM_DBL=1 -DHAVE_SUBNORM_FLT=1 -DHAVE_SIGNEDZ=1 -DHAVE_ROUND=1 -DHAVE_TRUNC=1 -DHAVE_FLOOR=1 -DHAVE_CEIL=1 -DHAVE_NEARBYINT=1 -DHAVE_DOUBLE_IEEE_LITTLE_ENDIAN=1 -DHAVE_LDOUBLE_IEEE_QUAD_LITTLE=1 -DMPFR_USE_STATIC_ASSERT=1 -DHAVE_ATTRIBUTE_MODE=1 -DPRINTF_L=1 -DPRINTF_T=1 -DPRINTF_GROUPFLAG=1 -DHAVE___GMPN_SBPI1_DIVAPPR_Q=1 -DHAVE___GMPN_INVERT_LIMB=1 -DHAVE___GMPN_RSBLSH1_N=1 -DMPFR_LONG_WITHIN_LIMB=1 -DMPFR_INTMAX_WITHIN_LIMB=1 -DHAVE_GETRUSAGE=1 -I. -DSRCDIR="." -I../src -I../src -DNDEBUG -DNO_ASM -I../libgmp/include/linux/arm64 -UHAVE_NEARBYINT -c -o terf.o.go terf.c', DO NOT EDIT.

//go:build ignore
// +build ignore

package __ccgo_object_file_v1

const mvARG_MAX = 131072
const mvBASE_MAX = 62
const mvBC_BASE_MAX = 99
const mvBC_DIM_MAX = 2048
const mvBC_SCALE_MAX = 99
const mvBC_STRING_MAX = 1000
const mvBUFSIZ = 1024
const mvCHARCLASS_NAME_MAX = 14
const mvCHAR_BIT = 8
const mvCHAR_MAX = 255
const mvCHAR_MIN = 0
const mvCOLL_WEIGHTS_MAX = 2
const mvDBL_DECIMAL_DIG = 17
const mvDBL_DIG = 15
const mvDBL_EPSILON = 2.22044604925031308085e-16
const mvDBL_HAS_SUBNORM = 1
const mvDBL_MANT_DIG = 53
const mvDBL_MAX = 1.79769313486231570815e+308
const mvDBL_MAX_10_EXP = 308
const mvDBL_MAX_EXP = 1024
const mvDBL_MIN = 2.22507385850720138309e-308
const mvDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvDECIMAL_DIG = 17
const mvDELAYTIMER_MAX = 0x7fffffff
const mvEXIT_FAILURE = 1
const mvEXIT_SUCCESS = 0
const mvEXPR_NEST_MAX = 32
const mvFILENAME_MAX = 4096
const mvFILESIZEBITS = 64
const mvFLT_DECIMAL_DIG = 9
const mvFLT_DIG = 6
const mvFLT_EPSILON = 1.1920928955078125e-07
const mvFLT_EVAL_METHOD = 0
const mvFLT_HAS_SUBNORM = 1
const mvFLT_MANT_DIG = 24
const mvFLT_MAX = 3.40282346638528859812e+38
const mvFLT_MAX_10_EXP = 38
const mvFLT_MAX_EXP = 128
const mvFLT_MIN = 1.17549435082228750797e-38
const mvFLT_RADIX = 2
const mvFLT_TRUE_MIN = 1.40129846432481707092e-45
const mvFOPEN_MAX = 1000
const mvGMP_LIMB_BITS = 64
const mvGMP_NAIL_BITS = 0
const mvGMP_NUMB_MAX = "GMP_NUMB_MASK"
const mvGMP_RNDD = "MPFR_RNDD"
const mvGMP_RNDN = "MPFR_RNDN"
const mvGMP_RNDU = "MPFR_RNDU"
const mvGMP_RNDZ = "MPFR_RNDZ"
const mvHAVE_ALLOCA = 1
const mvHAVE_ALLOCA_H = 1
const mvHAVE_ATTRIBUTE_MODE = 1
const mvHAVE_CEIL = 1
const mvHAVE_CLOCK_GETTIME = 1
const mvHAVE_DOUBLE_IEEE_LITTLE_ENDIAN = 1
const mvHAVE_FLOOR = 1
const mvHAVE_GETRUSAGE = 1
const mvHAVE_GETTIMEOFDAY = 1
const mvHAVE_INTMAX_T = 1
const mvHAVE_INTTYPES_H = 1
const mvHAVE_LDOUBLE_IEEE_QUAD_LITTLE = 1
const mvHAVE_LITTLE_ENDIAN = 1
const mvHAVE_LOCALE_H = 1
const mvHAVE_LONG_LONG = 1
const mvHAVE_ROUND = 1
const mvHAVE_SETLOCALE = 1
const mvHAVE_SIGACTION = 1
const mvHAVE_SIGNAL = 1
const mvHAVE_SIGNEDZ = 1
const mvHAVE_STDARG = 1
const mvHAVE_STDINT_H = 1
const mvHAVE_STDIO_H = 1
const mvHAVE_STRUCT_LCONV_DECIMAL_POINT = 1
const mvHAVE_STRUCT_LCONV_THOUSANDS_SEP = 1
const mvHAVE_SUBNORM_DBL = 1
const mvHAVE_SUBNORM_FLT = 1
const mvHAVE_TRUNC = 1
const mvHAVE_UINTPTR_T = 1
const mvHAVE_VA_COPY = 1
const mvHAVE_WCHAR_H = 1
const mvHAVE___GMPN_INVERT_LIMB = 1
const mvHAVE___GMPN_RSBLSH1_N = 1
const mvHAVE___GMPN_SBPI1_DIVAPPR_Q = 1
const mvHOST_NAME_MAX = 255
const mvIEEE_DBL_MANT_DIG = 53
const mvIEEE_FLOAT128_MANT_DIG = 113
const mvIEEE_FLT_MANT_DIG = 24
const mvINT_MAX = 0x7fffffff
const mvIOV_MAX = 1024
const mvLC_ALL = 6
const mvLC_ALL_MASK = 0x7fffffff
const mvLC_COLLATE = 3
const mvLC_CTYPE = 0
const mvLC_MESSAGES = 5
const mvLC_MONETARY = 4
const mvLC_NUMERIC = 1
const mvLC_TIME = 2
const mvLDBL_DECIMAL_DIG = "DECIMAL_DIG"
const mvLDBL_DIG = 15
const mvLDBL_EPSILON = 2.22044604925031308085e-16
const mvLDBL_HAS_SUBNORM = 1
const mvLDBL_MANT_DIG = 53
const mvLDBL_MAX = 1.79769313486231570815e+308
const mvLDBL_MAX_10_EXP = 308
const mvLDBL_MAX_EXP = 1024
const mvLDBL_MIN = 2.22507385850720138309e-308
const mvLDBL_TRUE_MIN = 4.94065645841246544177e-324
const mvLINE_MAX = 4096
const mvLLONG_MAX = 0x7fffffffffffffff
const mvLOG2 = 0.69314718055994528622
const mvLOGIN_NAME_MAX = 256
const mvLONG_BIT = 64
const mvLONG_MAX = "__LONG_MAX"
const mvLT_OBJDIR = ".libs/"
const mvL_ctermid = 20
const mvL_cuserid = 20
const mvL_tmpnam = 20
const mvMAXNORM = 1.7976931348623157081e308
const mvMB_LEN_MAX = 4
const mvMINNORM = 2.2250738585072013831e-308
const mvMPFR_AI_THRESHOLD2 = 1311
const mvMPFR_AI_THRESHOLD3 = 19661
const mvMPFR_ALLOCA_MAX = 16384
const mvMPFR_CACHE_ATTR = "MPFR_THREAD_ATTR"
const mvMPFR_DIV_THRESHOLD = 25
const mvMPFR_DOUBLE_SPEC = 1
const mvMPFR_EVEN_INEX = 2
const mvMPFR_EXP_2_THRESHOLD = 100
const mvMPFR_EXP_FSPEC = "l"
const mvMPFR_EXP_MAX = "LONG_MAX"
const mvMPFR_EXP_MIN = "LONG_MIN"
const mvMPFR_EXP_THRESHOLD = 25000
const mvMPFR_FLAGS_DIVBY0 = 32
const mvMPFR_FLAGS_ERANGE = 16
const mvMPFR_FLAGS_INEXACT = 8
const mvMPFR_FLAGS_NAN = 4
const mvMPFR_FLAGS_OVERFLOW = 2
const mvMPFR_FLAGS_UNDERFLOW = 1
const mvMPFR_GROUP_STATIC_SIZE = 16
const mvMPFR_HAVE_BUILTIN_UNREACHABLE = 1
const mvMPFR_HAVE_CONSTRUCTOR_ATTR = 1
const mvMPFR_HAVE_FESETROUND = 1
const mvMPFR_HAVE_INTMAX_MAX = 1
const mvMPFR_HAVE_NORETURN = 1
const mvMPFR_INTMAX_WITHIN_LIMB = 1
const mvMPFR_LCONV_DPTS = 1
const mvMPFR_LDBL_MANT_DIG = "LDBL_MANT_DIG"
const mvMPFR_LOG2_GMP_NUMB_BITS = 6
const mvMPFR_LOG2_PREC_BITS = 6
const mvMPFR_LOG_BADCASE_F = 16
const mvMPFR_LOG_INPUT_F = 1
const mvMPFR_LOG_INTERNAL_F = 4
const mvMPFR_LOG_MSG_F = 32
const mvMPFR_LOG_OUTPUT_F = 2
const mvMPFR_LOG_STAT_F = 64
const mvMPFR_LOG_TIME_F = 8
const mvMPFR_MUL_THRESHOLD = 20
const mvMPFR_NORETURN = "_Noreturn"
const mvMPFR_POOL_NENTRIES = 32
const mvMPFR_PREC_BITS = 64
const mvMPFR_PREC_MAX_TEMP = "ULONG_MAX"
const mvMPFR_PREC_MIN = 1
const mvMPFR_SIGN_POS = 1
const mvMPFR_SINCOS_THRESHOLD = 30000
const mvMPFR_SMALL_PRECISION = 32
const mvMPFR_SQR_THRESHOLD = 20
const mvMPFR_TMP_ALLOC = "TMP_ALLOC"
const mvMPFR_TMP_DECL = "TMP_DECL"
const mvMPFR_TMP_FREE = "TMP_FREE"
const mvMPFR_TMP_MARK = "TMP_MARK"
const mvMPFR_TUNE_CASE = "default"
const mvMPFR_USE_C99_FEATURE = 1
const mvMPFR_USE_STATIC_ASSERT = 1
const mvMPFR_VERSION_MAJOR = 4
const mvMPFR_VERSION_MINOR = 2
const mvMPFR_VERSION_PATCHLEVEL = 0
const mvMPFR_VERSION_STRING = "4.2.0"
const mvMPFR_WANT_ASSERT = 0
const mvMPFR_WANT_PROVEN_CODE = 1
const mvMP_SIZE_T_MAX = "LONG_MAX"
const mvMP_SIZE_T_MIN = "LONG_MIN"
const mvMQ_PRIO_MAX = 32768
const mvMUL_FFT_THRESHOLD = 8448
const mvNAME_MAX = 255
const mvNDEBUG = 1
const mvNGROUPS_MAX = 32
const mvNL_ARGMAX = 9
const mvNL_LANGMAX = 32
const mvNL_MSGMAX = 32767
const mvNL_NMAX = 16
const mvNL_SETMAX = 255
const mvNL_TEXTMAX = 2048
const mvNO_ASM = 1
const mvNZERO = 20
const mvPATH_MAX = 4096
const mvPIPE_BUF = 4096
const mvPRINTF_GROUPFLAG = 1
const mvPRINTF_L = 1
const mvPRINTF_T = 1
const mvPTHREAD_DESTRUCTOR_ITERATIONS = 4
const mvPTHREAD_KEYS_MAX = 128
const mvPTHREAD_STACK_MIN = 2048
const mvP_tmpdir = "/tmp"
const mvRAND_MAX = 0x7fffffff
const mvRE_DUP_MAX = 255
const mvSCHAR_MAX = 127
const mvSEM_NSEMS_MAX = 256
const mvSEM_VALUE_MAX = 0x7fffffff
const mvSHRT_MAX = 0x7fff
const mvSRCDIR = "."
const mvSSIZE_MAX = "LONG_MAX"
const mvSYMLOOP_MAX = 40
const mvTGENERIC_SO_TEST = 1
const mvTMP_MAX = 10000
const mvTTY_NAME_MAX = 32
const mvTZNAME_MAX = 6
const mvUCHAR_MAX = 255
const mvUINT_MAX = 4294967295
const mvUSHRT_MAX = 65535
const mvWNOHANG = 1
const mvWORD_BIT = 32
const mvWUNTRACED = 2
const mvW_TYPE_SIZE = "GMP_NUMB_BITS"
const mv_GMP_H_HAVE_FILE = 1
const mv_GNU_SOURCE = 1
const mv_IOFBF = 0
const mv_IOLBF = 1
const mv_IONBF = 2
const mv_LP64 = 1
const mv_MPFR_EXP_FORMAT = "_MPFR_PREC_FORMAT"
const mv_MPFR_H_HAVE_FILE = 1
const mv_MPFR_IEEE_FLOATS = 1
const mv_MPFR_PREC_FORMAT = 3
const mv_POSIX2_BC_BASE_MAX = 99
const mv_POSIX2_BC_DIM_MAX = 2048
const mv_POSIX2_BC_SCALE_MAX = 99
const mv_POSIX2_BC_STRING_MAX = 1000
const mv_POSIX2_CHARCLASS_NAME_MAX = 14
const mv_POSIX2_COLL_WEIGHTS_MAX = 2
const mv_POSIX2_EXPR_NEST_MAX = 32
const mv_POSIX2_LINE_MAX = 2048
const mv_POSIX2_RE_DUP_MAX = 255
const mv_POSIX_AIO_LISTIO_MAX = 2
const mv_POSIX_AIO_MAX = 1
const mv_POSIX_ARG_MAX = 4096
const mv_POSIX_CHILD_MAX = 25
const mv_POSIX_CLOCKRES_MIN = 20000000
const mv_POSIX_DELAYTIMER_MAX = 32
const mv_POSIX_HOST_NAME_MAX = 255
const mv_POSIX_LINK_MAX = 8
const mv_POSIX_LOGIN_NAME_MAX = 9
const mv_POSIX_MAX_CANON = 255
const mv_POSIX_MAX_INPUT = 255
const mv_POSIX_MQ_OPEN_MAX = 8
const mv_POSIX_MQ_PRIO_MAX = 32
const mv_POSIX_NAME_MAX = 14
const mv_POSIX_NGROUPS_MAX = 8
const mv_POSIX_OPEN_MAX = 20
const mv_POSIX_PATH_MAX = 256
const mv_POSIX_PIPE_BUF = 512
const mv_POSIX_RE_DUP_MAX = 255
const mv_POSIX_RTSIG_MAX = 8
const mv_POSIX_SEM_NSEMS_MAX = 256
const mv_POSIX_SEM_VALUE_MAX = 32767
const mv_POSIX_SIGQUEUE_MAX = 32
const mv_POSIX_SSIZE_MAX = 32767
const mv_POSIX_SS_REPL_MAX = 4
const mv_POSIX_STREAM_MAX = 8
const mv_POSIX_SYMLINK_MAX = 255
const mv_POSIX_SYMLOOP_MAX = 8
const mv_POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const mv_POSIX_THREAD_KEYS_MAX = 128
const mv_POSIX_THREAD_THREADS_MAX = 64
const mv_POSIX_TIMER_MAX = 32
const mv_POSIX_TRACE_EVENT_NAME_MAX = 30
const mv_POSIX_TRACE_NAME_MAX = 8
const mv_POSIX_TRACE_SYS_MAX = 8
const mv_POSIX_TRACE_USER_EVENT_MAX = 32
const mv_POSIX_TTY_NAME_MAX = 9
const mv_POSIX_TZNAME_MAX = 6
const mv_STDC_PREDEF_H = 1
const mv_XOPEN_IOV_MAX = 16
const mv_XOPEN_NAME_MAX = 255
const mv_XOPEN_PATH_MAX = 1024
const mv__AARCH64EL__ = 1
const mv__AARCH64_CMODEL_SMALL__ = 1
const mv__ARM_64BIT_STATE = 1
const mv__ARM_ALIGN_MAX_PWR = 28
const mv__ARM_ALIGN_MAX_STACK_PWR = 16
const mv__ARM_ARCH = 8
const mv__ARM_ARCH_8A = 1
const mv__ARM_ARCH_ISA_A64 = 1
const mv__ARM_ARCH_PROFILE = 65
const mv__ARM_FEATURE_CLZ = 1
const mv__ARM_FEATURE_FMA = 1
const mv__ARM_FEATURE_IDIV = 1
const mv__ARM_FEATURE_NUMERIC_MAXMIN = 1
const mv__ARM_FEATURE_UNALIGNED = 1
const mv__ARM_FP = 14
const mv__ARM_FP16_ARGS = 1
const mv__ARM_FP16_FORMAT_IEEE = 1
const mv__ARM_NEON = 1
const mv__ARM_PCS_AAPCS64 = 1
const mv__ARM_SIZEOF_MINIMAL_ENUM = 4
const mv__ARM_SIZEOF_WCHAR_T = 4
const mv__ATOMIC_ACQUIRE = 2
const mv__ATOMIC_ACQ_REL = 4
const mv__ATOMIC_CONSUME = 1
const mv__ATOMIC_RELAXED = 0
const mv__ATOMIC_RELEASE = 3
const mv__ATOMIC_SEQ_CST = 5
const mv__BIGGEST_ALIGNMENT__ = 16
const mv__BIG_ENDIAN = 4321
const mv__BYTE_ORDER = 1234
const mv__BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__CCGO__ = 1
const mv__CHAR_BIT__ = 8
const mv__CHAR_UNSIGNED__ = 1
const mv__DBL_DECIMAL_DIG__ = 17
const mv__DBL_DIG__ = 15
const mv__DBL_HAS_DENORM__ = 1
const mv__DBL_HAS_INFINITY__ = 1
const mv__DBL_HAS_QUIET_NAN__ = 1
const mv__DBL_IS_IEC_60559__ = 2
const mv__DBL_MANT_DIG__ = 53
const mv__DBL_MAX_10_EXP__ = 308
const mv__DBL_MAX_EXP__ = 1024
const mv__DECIMAL_DIG__ = 36
const mv__DEC_EVAL_METHOD__ = 2
const mv__ELF__ = 1
const mv__FINITE_MATH_ONLY__ = 0
const mv__FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const mv__FLT128_DECIMAL_DIG__ = 36
const mv__FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT128_DIG__ = 33
const mv__FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT128_HAS_DENORM__ = 1
const mv__FLT128_HAS_INFINITY__ = 1
const mv__FLT128_HAS_QUIET_NAN__ = 1
const mv__FLT128_IS_IEC_60559__ = 2
const mv__FLT128_MANT_DIG__ = 113
const mv__FLT128_MAX_10_EXP__ = 4932
const mv__FLT128_MAX_EXP__ = 16384
const mv__FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT16_DECIMAL_DIG__ = 5
const mv__FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const mv__FLT16_DIG__ = 3
const mv__FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const mv__FLT16_HAS_DENORM__ = 1
const mv__FLT16_HAS_INFINITY__ = 1
const mv__FLT16_HAS_QUIET_NAN__ = 1
const mv__FLT16_IS_IEC_60559__ = 2
const mv__FLT16_MANT_DIG__ = 11
const mv__FLT16_MAX_10_EXP__ = 4
const mv__FLT16_MAX_EXP__ = 16
const mv__FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const mv__FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const mv__FLT32X_DECIMAL_DIG__ = 17
const mv__FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT32X_DIG__ = 15
const mv__FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT32X_HAS_DENORM__ = 1
const mv__FLT32X_HAS_INFINITY__ = 1
const mv__FLT32X_HAS_QUIET_NAN__ = 1
const mv__FLT32X_IS_IEC_60559__ = 2
const mv__FLT32X_MANT_DIG__ = 53
const mv__FLT32X_MAX_10_EXP__ = 308
const mv__FLT32X_MAX_EXP__ = 1024
const mv__FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT32_DECIMAL_DIG__ = 9
const mv__FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT32_DIG__ = 6
const mv__FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT32_HAS_DENORM__ = 1
const mv__FLT32_HAS_INFINITY__ = 1
const mv__FLT32_HAS_QUIET_NAN__ = 1
const mv__FLT32_IS_IEC_60559__ = 2
const mv__FLT32_MANT_DIG__ = 24
const mv__FLT32_MAX_10_EXP__ = 38
const mv__FLT32_MAX_EXP__ = 128
const mv__FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT64X_DECIMAL_DIG__ = 36
const mv__FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__FLT64X_DIG__ = 33
const mv__FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__FLT64X_HAS_DENORM__ = 1
const mv__FLT64X_HAS_INFINITY__ = 1
const mv__FLT64X_HAS_QUIET_NAN__ = 1
const mv__FLT64X_IS_IEC_60559__ = 2
const mv__FLT64X_MANT_DIG__ = 113
const mv__FLT64X_MAX_10_EXP__ = 4932
const mv__FLT64X_MAX_EXP__ = 16384
const mv__FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__FLT64_DECIMAL_DIG__ = 17
const mv__FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const mv__FLT64_DIG__ = 15
const mv__FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const mv__FLT64_HAS_DENORM__ = 1
const mv__FLT64_HAS_INFINITY__ = 1
const mv__FLT64_HAS_QUIET_NAN__ = 1
const mv__FLT64_IS_IEC_60559__ = 2
const mv__FLT64_MANT_DIG__ = 53
const mv__FLT64_MAX_10_EXP__ = 308
const mv__FLT64_MAX_EXP__ = 1024
const mv__FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const mv__FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const mv__FLT_DECIMAL_DIG__ = 9
const mv__FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const mv__FLT_DIG__ = 6
const mv__FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const mv__FLT_EVAL_METHOD_C99__ = 0
const mv__FLT_EVAL_METHOD_TS_18661_3__ = 0
const mv__FLT_EVAL_METHOD__ = 0
const mv__FLT_HAS_DENORM__ = 1
const mv__FLT_HAS_INFINITY__ = 1
const mv__FLT_HAS_QUIET_NAN__ = 1
const mv__FLT_IS_IEC_60559__ = 2
const mv__FLT_MANT_DIG__ = 24
const mv__FLT_MAX_10_EXP__ = 38
const mv__FLT_MAX_EXP__ = 128
const mv__FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const mv__FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const mv__FLT_RADIX__ = 2
const mv__FP_FAST_FMA = 1
const mv__FP_FAST_FMAF = 1
const mv__FP_FAST_FMAF32 = 1
const mv__FP_FAST_FMAF32x = 1
const mv__FP_FAST_FMAF64 = 1
const mv__FUNCTION__ = "__func__"
const mv__GCC_ASM_FLAG_OUTPUTS__ = 1
const mv__GCC_ATOMIC_BOOL_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const mv__GCC_ATOMIC_CHAR_LOCK_FREE = 2
const mv__GCC_ATOMIC_INT_LOCK_FREE = 2
const mv__GCC_ATOMIC_LLONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_LONG_LOCK_FREE = 2
const mv__GCC_ATOMIC_POINTER_LOCK_FREE = 2
const mv__GCC_ATOMIC_SHORT_LOCK_FREE = 2
const mv__GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const mv__GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const mv__GCC_CONSTRUCTIVE_SIZE = 64
const mv__GCC_DESTRUCTIVE_SIZE = 256
const mv__GCC_HAVE_DWARF2_CFI_ASM = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const mv__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const mv__GCC_IEC_559 = 2
const mv__GCC_IEC_559_COMPLEX = 2
const mv__GMP_CC = "aarch64-linux-gnu-gcc"
const mv__GMP_CFLAGS = "-g -O2 -ffile-prefix-map=BUILDPATH=. -fstack-protector-strong -Wformat -Werror=format-security -O3"
const mv__GMP_HAVE_HOST_CPU_FAMILY_power = 0
const mv__GMP_HAVE_HOST_CPU_FAMILY_powerpc = 0
const mv__GMP_INLINE_PROTOTYPES = 1
const mv__GMP_LIBGMP_DLL = 0
const mv__GMP_MP_SIZE_T_INT = 0
const mv__GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const mv__GNUC_MINOR__ = 2
const mv__GNUC_PATCHLEVEL__ = 0
const mv__GNUC_STDC_INLINE__ = 1
const mv__GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const mv__GNUC__ = 12
const mv__GNU_MP_VERSION = 6
const mv__GNU_MP_VERSION_MINOR = 2
const mv__GNU_MP_VERSION_PATCHLEVEL = 1
const mv__GNU_MP__ = 6
const mv__GXX_ABI_VERSION = 1017
const mv__HAVE_SPECULATION_SAFE_VALUE = 1
const mv__INT16_MAX__ = 0x7fff
const mv__INT32_MAX__ = 0x7fffffff
const mv__INT32_TYPE__ = "int"
const mv__INT64_MAX__ = 0x7fffffffffffffff
const mv__INT8_MAX__ = 0x7f
const mv__INTMAX_MAX__ = 0x7fffffffffffffff
const mv__INTMAX_WIDTH__ = 64
const mv__INTPTR_MAX__ = 0x7fffffffffffffff
const mv__INTPTR_WIDTH__ = 64
const mv__INT_FAST16_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST16_WIDTH__ = 64
const mv__INT_FAST32_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST32_WIDTH__ = 64
const mv__INT_FAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_FAST64_WIDTH__ = 64
const mv__INT_FAST8_MAX__ = 0x7f
const mv__INT_FAST8_WIDTH__ = 8
const mv__INT_LEAST16_MAX__ = 0x7fff
const mv__INT_LEAST16_WIDTH__ = 16
const mv__INT_LEAST32_MAX__ = 0x7fffffff
const mv__INT_LEAST32_TYPE__ = "int"
const mv__INT_LEAST32_WIDTH__ = 32
const mv__INT_LEAST64_MAX__ = 0x7fffffffffffffff
const mv__INT_LEAST64_WIDTH__ = 64
const mv__INT_LEAST8_MAX__ = 0x7f
const mv__INT_LEAST8_WIDTH__ = 8
const mv__INT_MAX__ = 0x7fffffff
const mv__INT_WIDTH__ = 32
const mv__LDBL_DECIMAL_DIG__ = 36
const mv__LDBL_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const mv__LDBL_DIG__ = 33
const mv__LDBL_EPSILON__ = 1.92592994438723585305597794258492732e-34
const mv__LDBL_HAS_DENORM__ = 1
const mv__LDBL_HAS_INFINITY__ = 1
const mv__LDBL_HAS_QUIET_NAN__ = 1
const mv__LDBL_IS_IEC_60559__ = 2
const mv__LDBL_MANT_DIG__ = 113
const mv__LDBL_MAX_10_EXP__ = 4932
const mv__LDBL_MAX_EXP__ = 16384
const mv__LDBL_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LDBL_MIN__ = 3.36210314311209350626267781732175260e-4932
const mv__LDBL_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const mv__LITTLE_ENDIAN = 1234
const mv__LONG_LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_LONG_WIDTH__ = 64
const mv__LONG_MAX = 9223372036854775807
const mv__LONG_MAX__ = 0x7fffffffffffffff
const mv__LONG_WIDTH__ = 64
const mv__LP64__ = 1
const mv__MPFR_DECLSPEC = "__GMP_DECLSPEC"
const mv__NO_INLINE__ = 1
const mv__ORDER_BIG_ENDIAN__ = 4321
const mv__ORDER_LITTLE_ENDIAN__ = 1234
const mv__ORDER_PDP_ENDIAN__ = 3412
const mv__PIC__ = 2
const mv__PIE__ = 2
const mv__PRAGMA_REDEFINE_EXTNAME = 1
const mv__PRETTY_FUNCTION__ = "__func__"
const mv__PTRDIFF_MAX__ = 0x7fffffffffffffff
const mv__PTRDIFF_WIDTH__ = 64
const mv__SCHAR_MAX__ = 0x7f
const mv__SCHAR_WIDTH__ = 8
const mv__SHRT_MAX__ = 0x7fff
const mv__SHRT_WIDTH__ = 16
const mv__SIG_ATOMIC_MAX__ = 0x7fffffff
const mv__SIG_ATOMIC_TYPE__ = "int"
const mv__SIG_ATOMIC_WIDTH__ = 32
const mv__SIZEOF_DOUBLE__ = 8
const mv__SIZEOF_FLOAT__ = 4
const mv__SIZEOF_INT128__ = 16
const mv__SIZEOF_INT__ = 4
const mv__SIZEOF_LONG_DOUBLE__ = 8
const mv__SIZEOF_LONG_LONG__ = 8
const mv__SIZEOF_LONG__ = 8
const mv__SIZEOF_POINTER__ = 8
const mv__SIZEOF_PTRDIFF_T__ = 8
const mv__SIZEOF_SHORT__ = 2
const mv__SIZEOF_SIZE_T__ = 8
const mv__SIZEOF_WCHAR_T__ = 4
const mv__SIZEOF_WINT_T__ = 4
const mv__SIZE_MAX__ = 0xffffffffffffffff
const mv__SIZE_WIDTH__ = 64
const mv__STDC_HOSTED__ = 1
const mv__STDC_IEC_559_COMPLEX__ = 1
const mv__STDC_IEC_559__ = 1
const mv__STDC_IEC_60559_BFP__ = 201404
const mv__STDC_IEC_60559_COMPLEX__ = 201404
const mv__STDC_ISO_10646__ = 201706
const mv__STDC_UTF_16__ = 1
const mv__STDC_UTF_32__ = 1
const mv__STDC_VERSION__ = 201710
const mv__STDC__ = 1
const mv__UINT16_MAX__ = 0xffff
const mv__UINT32_MAX__ = 0xffffffff
const mv__UINT64_MAX__ = 0xffffffffffffffff
const mv__UINT8_MAX__ = 0xff
const mv__UINTMAX_MAX__ = 0xffffffffffffffff
const mv__UINTPTR_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST16_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST32_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_FAST8_MAX__ = 0xff
const mv__UINT_LEAST16_MAX__ = 0xffff
const mv__UINT_LEAST32_MAX__ = 0xffffffff
const mv__UINT_LEAST64_MAX__ = 0xffffffffffffffff
const mv__UINT_LEAST8_MAX__ = 0xff
const mv__USE_TIME_BITS64 = 1
const mv__VERSION__ = "12.2.0"
const mv__WCHAR_MAX__ = 0xffffffff
const mv__WCHAR_MIN__ = 0
const mv__WCHAR_WIDTH__ = 32
const mv__WINT_MAX__ = 0xffffffff
const mv__WINT_MIN__ = 0
const mv__WINT_WIDTH__ = 32
const mv__aarch64__ = 1
const mv__gnu_linux__ = 1
const mv__inline = "inline"
const mv__linux = 1
const mv__linux__ = 1
const mv__pic__ = 2
const mv__pie__ = 2
const mv__restrict = "restrict"
const mv__restrict_arr = "restrict"
const mv__unix = 1
const mv__unix__ = 1
const mv_mpq_cmp_si = "__gmpq_cmp_si"
const mv_mpq_cmp_ui = "__gmpq_cmp_ui"
const mv_mpz_cmp_si = "__gmpz_cmp_si"
const mv_mpz_cmp_ui = "__gmpz_cmp_ui"
const mv_mpz_realloc = "__gmpz_realloc"
const mvalloca = "__builtin_alloca"
const mvgmp_asprintf = "__gmp_asprintf"
const mvgmp_errno = "__gmp_errno"
const mvgmp_fprintf = "__gmp_fprintf"
const mvgmp_fscanf = "__gmp_fscanf"
const mvgmp_obstack_printf = "__gmp_obstack_printf"
const mvgmp_obstack_vprintf = "__gmp_obstack_vprintf"
const mvgmp_printf = "__gmp_printf"
const mvgmp_randclear = "__gmp_randclear"
const mvgmp_randinit = "__gmp_randinit"
const mvgmp_randinit_default = "__gmp_randinit_default"
const mvgmp_randinit_lc_2exp = "__gmp_randinit_lc_2exp"
const mvgmp_randinit_lc_2exp_size = "__gmp_randinit_lc_2exp_size"
const mvgmp_randinit_mt = "__gmp_randinit_mt"
const mvgmp_randinit_set = "__gmp_randinit_set"
const mvgmp_randseed = "__gmp_randseed"
const mvgmp_randseed_ui = "__gmp_randseed_ui"
const mvgmp_scanf = "__gmp_scanf"
const mvgmp_snprintf = "__gmp_snprintf"
const mvgmp_sprintf = "__gmp_sprintf"
const mvgmp_sscanf = "__gmp_sscanf"
const mvgmp_urandomb_ui = "__gmp_urandomb_ui"
const mvgmp_urandomm_ui = "__gmp_urandomm_ui"
const mvgmp_vasprintf = "__gmp_vasprintf"
const mvgmp_version = "__gmp_version"
const mvgmp_vfprintf = "__gmp_vfprintf"
const mvgmp_vfscanf = "__gmp_vfscanf"
const mvgmp_vprintf = "__gmp_vprintf"
const mvgmp_vscanf = "__gmp_vscanf"
const mvgmp_vsnprintf = "__gmp_vsnprintf"
const mvgmp_vsprintf = "__gmp_vsprintf"
const mvgmp_vsscanf = "__gmp_vsscanf"
const mvlinux = 1
const mvmp_bits_per_limb = "__gmp_bits_per_limb"
const mvmp_get_memory_functions = "__gmp_get_memory_functions"
const mvmp_prec_t = "mpfr_prec_t"
const mvmp_rnd_t = "mpfr_rnd_t"
const mvmp_set_memory_functions = "__gmp_set_memory_functions"
const mvmpf_abs = "__gmpf_abs"
const mvmpf_add = "__gmpf_add"
const mvmpf_add_ui = "__gmpf_add_ui"
const mvmpf_ceil = "__gmpf_ceil"
const mvmpf_clear = "__gmpf_clear"
const mvmpf_clears = "__gmpf_clears"
const mvmpf_cmp = "__gmpf_cmp"
const mvmpf_cmp_d = "__gmpf_cmp_d"
const mvmpf_cmp_si = "__gmpf_cmp_si"
const mvmpf_cmp_ui = "__gmpf_cmp_ui"
const mvmpf_cmp_z = "__gmpf_cmp_z"
const mvmpf_div = "__gmpf_div"
const mvmpf_div_2exp = "__gmpf_div_2exp"
const mvmpf_div_ui = "__gmpf_div_ui"
const mvmpf_dump = "__gmpf_dump"
const mvmpf_eq = "__gmpf_eq"
const mvmpf_fits_sint_p = "__gmpf_fits_sint_p"
const mvmpf_fits_slong_p = "__gmpf_fits_slong_p"
const mvmpf_fits_sshort_p = "__gmpf_fits_sshort_p"
const mvmpf_fits_uint_p = "__gmpf_fits_uint_p"
const mvmpf_fits_ulong_p = "__gmpf_fits_ulong_p"
const mvmpf_fits_ushort_p = "__gmpf_fits_ushort_p"
const mvmpf_floor = "__gmpf_floor"
const mvmpf_get_d = "__gmpf_get_d"
const mvmpf_get_d_2exp = "__gmpf_get_d_2exp"
const mvmpf_get_default_prec = "__gmpf_get_default_prec"
const mvmpf_get_prec = "__gmpf_get_prec"
const mvmpf_get_si = "__gmpf_get_si"
const mvmpf_get_str = "__gmpf_get_str"
const mvmpf_get_ui = "__gmpf_get_ui"
const mvmpf_init = "__gmpf_init"
const mvmpf_init2 = "__gmpf_init2"
const mvmpf_init_set = "__gmpf_init_set"
const mvmpf_init_set_d = "__gmpf_init_set_d"
const mvmpf_init_set_si = "__gmpf_init_set_si"
const mvmpf_init_set_str = "__gmpf_init_set_str"
const mvmpf_init_set_ui = "__gmpf_init_set_ui"
const mvmpf_inits = "__gmpf_inits"
const mvmpf_inp_str = "__gmpf_inp_str"
const mvmpf_integer_p = "__gmpf_integer_p"
const mvmpf_mul = "__gmpf_mul"
const mvmpf_mul_2exp = "__gmpf_mul_2exp"
const mvmpf_mul_ui = "__gmpf_mul_ui"
const mvmpf_neg = "__gmpf_neg"
const mvmpf_out_str = "__gmpf_out_str"
const mvmpf_pow_ui = "__gmpf_pow_ui"
const mvmpf_random2 = "__gmpf_random2"
const mvmpf_reldiff = "__gmpf_reldiff"
const mvmpf_set = "__gmpf_set"
const mvmpf_set_d = "__gmpf_set_d"
const mvmpf_set_default_prec = "__gmpf_set_default_prec"
const mvmpf_set_prec = "__gmpf_set_prec"
const mvmpf_set_prec_raw = "__gmpf_set_prec_raw"
const mvmpf_set_q = "__gmpf_set_q"
const mvmpf_set_si = "__gmpf_set_si"
const mvmpf_set_str = "__gmpf_set_str"
const mvmpf_set_ui = "__gmpf_set_ui"
const mvmpf_set_z = "__gmpf_set_z"
const mvmpf_size = "__gmpf_size"
const mvmpf_sqrt = "__gmpf_sqrt"
const mvmpf_sqrt_ui = "__gmpf_sqrt_ui"
const mvmpf_sub = "__gmpf_sub"
const mvmpf_sub_ui = "__gmpf_sub_ui"
const mvmpf_swap = "__gmpf_swap"
const mvmpf_trunc = "__gmpf_trunc"
const mvmpf_ui_div = "__gmpf_ui_div"
const mvmpf_ui_sub = "__gmpf_ui_sub"
const mvmpf_urandomb = "__gmpf_urandomb"
const mvmpfr_cmp_abs = "mpfr_cmpabs"
const mvmpfr_custom_get_mantissa = "mpfr_custom_get_significand"
const mvmpfr_fpif_export = "__gmpfr_fpif_export"
const mvmpfr_fpif_import = "__gmpfr_fpif_import"
const mvmpfr_fprintf = "__gmpfr_fprintf"
const mvmpfr_get_z_exp = "mpfr_get_z_2exp"
const mvmpfr_inp_str = "__gmpfr_inp_str"
const mvmpfr_out_str = "__gmpfr_out_str"
const mvmpq_abs = "__gmpq_abs"
const mvmpq_add = "__gmpq_add"
const mvmpq_canonicalize = "__gmpq_canonicalize"
const mvmpq_clear = "__gmpq_clear"
const mvmpq_clears = "__gmpq_clears"
const mvmpq_cmp = "__gmpq_cmp"
const mvmpq_cmp_z = "__gmpq_cmp_z"
const mvmpq_div = "__gmpq_div"
const mvmpq_div_2exp = "__gmpq_div_2exp"
const mvmpq_equal = "__gmpq_equal"
const mvmpq_get_d = "__gmpq_get_d"
const mvmpq_get_den = "__gmpq_get_den"
const mvmpq_get_num = "__gmpq_get_num"
const mvmpq_get_str = "__gmpq_get_str"
const mvmpq_init = "__gmpq_init"
const mvmpq_inits = "__gmpq_inits"
const mvmpq_inp_str = "__gmpq_inp_str"
const mvmpq_inv = "__gmpq_inv"
const mvmpq_mul = "__gmpq_mul"
const mvmpq_mul_2exp = "__gmpq_mul_2exp"
const mvmpq_neg = "__gmpq_neg"
const mvmpq_out_str = "__gmpq_out_str"
const mvmpq_set = "__gmpq_set"
const mvmpq_set_d = "__gmpq_set_d"
const mvmpq_set_den = "__gmpq_set_den"
const mvmpq_set_f = "__gmpq_set_f"
const mvmpq_set_num = "__gmpq_set_num"
const mvmpq_set_si = "__gmpq_set_si"
const mvmpq_set_str = "__gmpq_set_str"
const mvmpq_set_ui = "__gmpq_set_ui"
const mvmpq_set_z = "__gmpq_set_z"
const mvmpq_sub = "__gmpq_sub"
const mvmpq_swap = "__gmpq_swap"
const mvmpz_2fac_ui = "__gmpz_2fac_ui"
const mvmpz_abs = "__gmpz_abs"
const mvmpz_add = "__gmpz_add"
const mvmpz_add_ui = "__gmpz_add_ui"
const mvmpz_addmul = "__gmpz_addmul"
const mvmpz_addmul_ui = "__gmpz_addmul_ui"
const mvmpz_and = "__gmpz_and"
const mvmpz_array_init = "__gmpz_array_init"
const mvmpz_bin_ui = "__gmpz_bin_ui"
const mvmpz_bin_uiui = "__gmpz_bin_uiui"
const mvmpz_cdiv_q = "__gmpz_cdiv_q"
const mvmpz_cdiv_q_2exp = "__gmpz_cdiv_q_2exp"
const mvmpz_cdiv_q_ui = "__gmpz_cdiv_q_ui"
const mvmpz_cdiv_qr = "__gmpz_cdiv_qr"
const mvmpz_cdiv_qr_ui = "__gmpz_cdiv_qr_ui"
const mvmpz_cdiv_r = "__gmpz_cdiv_r"
const mvmpz_cdiv_r_2exp = "__gmpz_cdiv_r_2exp"
const mvmpz_cdiv_r_ui = "__gmpz_cdiv_r_ui"
const mvmpz_cdiv_ui = "__gmpz_cdiv_ui"
const mvmpz_clears = "__gmpz_clears"
const mvmpz_clrbit = "__gmpz_clrbit"
const mvmpz_cmp = "__gmpz_cmp"
const mvmpz_cmp_d = "__gmpz_cmp_d"
const mvmpz_cmpabs = "__gmpz_cmpabs"
const mvmpz_cmpabs_d = "__gmpz_cmpabs_d"
const mvmpz_cmpabs_ui = "__gmpz_cmpabs_ui"
const mvmpz_com = "__gmpz_com"
const mvmpz_combit = "__gmpz_combit"
const mvmpz_congruent_2exp_p = "__gmpz_congruent_2exp_p"
const mvmpz_congruent_p = "__gmpz_congruent_p"
const mvmpz_congruent_ui_p = "__gmpz_congruent_ui_p"
const mvmpz_div = "mpz_fdiv_q"
const mvmpz_div_2exp = "mpz_fdiv_q_2exp"
const mvmpz_div_ui = "mpz_fdiv_q_ui"
const mvmpz_divexact = "__gmpz_divexact"
const mvmpz_divexact_ui = "__gmpz_divexact_ui"
const mvmpz_divisible_2exp_p = "__gmpz_divisible_2exp_p"
const mvmpz_divisible_p = "__gmpz_divisible_p"
const mvmpz_divisible_ui_p = "__gmpz_divisible_ui_p"
const mvmpz_divmod = "mpz_fdiv_qr"
const mvmpz_divmod_ui = "mpz_fdiv_qr_ui"
const mvmpz_dump = "__gmpz_dump"
const mvmpz_eor = "__gmpz_xor"
const mvmpz_export = "__gmpz_export"
const mvmpz_fac_ui = "__gmpz_fac_ui"
const mvmpz_fdiv_q = "__gmpz_fdiv_q"
const mvmpz_fdiv_q_2exp = "__gmpz_fdiv_q_2exp"
const mvmpz_fdiv_q_ui = "__gmpz_fdiv_q_ui"
const mvmpz_fdiv_qr = "__gmpz_fdiv_qr"
const mvmpz_fdiv_qr_ui = "__gmpz_fdiv_qr_ui"
const mvmpz_fdiv_r = "__gmpz_fdiv_r"
const mvmpz_fdiv_r_2exp = "__gmpz_fdiv_r_2exp"
const mvmpz_fdiv_r_ui = "__gmpz_fdiv_r_ui"
const mvmpz_fdiv_ui = "__gmpz_fdiv_ui"
const mvmpz_fib2_ui = "__gmpz_fib2_ui"
const mvmpz_fib_ui = "__gmpz_fib_ui"
const mvmpz_fits_sint_p = "__gmpz_fits_sint_p"
const mvmpz_fits_slong_p = "__gmpz_fits_slong_p"
const mvmpz_fits_sshort_p = "__gmpz_fits_sshort_p"
const mvmpz_fits_uint_p = "__gmpz_fits_uint_p"
const mvmpz_fits_ulong_p = "__gmpz_fits_ulong_p"
const mvmpz_fits_ushort_p = "__gmpz_fits_ushort_p"
const mvmpz_gcd = "__gmpz_gcd"
const mvmpz_gcd_ui = "__gmpz_gcd_ui"
const mvmpz_gcdext = "__gmpz_gcdext"
const mvmpz_get_d = "__gmpz_get_d"
const mvmpz_get_d_2exp = "__gmpz_get_d_2exp"
const mvmpz_get_si = "__gmpz_get_si"
const mvmpz_get_str = "__gmpz_get_str"
const mvmpz_get_ui = "__gmpz_get_ui"
const mvmpz_getlimbn = "__gmpz_getlimbn"
const mvmpz_hamdist = "__gmpz_hamdist"
const mvmpz_import = "__gmpz_import"
const mvmpz_init_set_d = "__gmpz_init_set_d"
const mvmpz_init_set_si = "__gmpz_init_set_si"
const mvmpz_init_set_str = "__gmpz_init_set_str"
const mvmpz_inits = "__gmpz_inits"
const mvmpz_inp_raw = "__gmpz_inp_raw"
const mvmpz_inp_str = "__gmpz_inp_str"
const mvmpz_invert = "__gmpz_invert"
const mvmpz_ior = "__gmpz_ior"
const mvmpz_jacobi = "__gmpz_jacobi"
const mvmpz_kronecker = "mpz_jacobi"
const mvmpz_kronecker_si = "__gmpz_kronecker_si"
const mvmpz_kronecker_ui = "__gmpz_kronecker_ui"
const mvmpz_lcm = "__gmpz_lcm"
const mvmpz_lcm_ui = "__gmpz_lcm_ui"
const mvmpz_legendre = "mpz_jacobi"
const mvmpz_limbs_finish = "__gmpz_limbs_finish"
const mvmpz_limbs_modify = "__gmpz_limbs_modify"
const mvmpz_limbs_read = "__gmpz_limbs_read"
const mvmpz_limbs_write = "__gmpz_limbs_write"
const mvmpz_lucnum2_ui = "__gmpz_lucnum2_ui"
const mvmpz_lucnum_ui = "__gmpz_lucnum_ui"
const mvmpz_mdiv = "mpz_fdiv_q"
const mvmpz_mdiv_ui = "mpz_fdiv_q_ui"
const mvmpz_mdivmod = "mpz_fdiv_qr"
const mvmpz_mfac_uiui = "__gmpz_mfac_uiui"
const mvmpz_millerrabin = "__gmpz_millerrabin"
const mvmpz_mmod = "mpz_fdiv_r"
const mvmpz_mod = "__gmpz_mod"
const mvmpz_mod_2exp = "mpz_fdiv_r_2exp"
const mvmpz_mod_ui = "mpz_fdiv_r_ui"
const mvmpz_mul = "__gmpz_mul"
const mvmpz_mul_2exp = "__gmpz_mul_2exp"
const mvmpz_mul_si = "__gmpz_mul_si"
const mvmpz_mul_ui = "__gmpz_mul_ui"
const mvmpz_neg = "__gmpz_neg"
const mvmpz_nextprime = "__gmpz_nextprime"
const mvmpz_out_raw = "__gmpz_out_raw"
const mvmpz_out_str = "__gmpz_out_str"
const mvmpz_perfect_power_p = "__gmpz_perfect_power_p"
const mvmpz_perfect_square_p = "__gmpz_perfect_square_p"
const mvmpz_popcount = "__gmpz_popcount"
const mvmpz_pow_ui = "__gmpz_pow_ui"
const mvmpz_powm = "__gmpz_powm"
const mvmpz_powm_sec = "__gmpz_powm_sec"
const mvmpz_powm_ui = "__gmpz_powm_ui"
const mvmpz_primorial_ui = "__gmpz_primorial_ui"
const mvmpz_probab_prime_p = "__gmpz_probab_prime_p"
const mvmpz_random = "__gmpz_random"
const mvmpz_random2 = "__gmpz_random2"
const mvmpz_realloc = "__gmpz_realloc"
const mvmpz_realloc2 = "__gmpz_realloc2"
const mvmpz_remove = "__gmpz_remove"
const mvmpz_roinit_n = "__gmpz_roinit_n"
const mvmpz_root = "__gmpz_root"
const mvmpz_rootrem = "__gmpz_rootrem"
const mvmpz_rrandomb = "__gmpz_rrandomb"
const mvmpz_scan0 = "__gmpz_scan0"
const mvmpz_scan1 = "__gmpz_scan1"
const mvmpz_set = "__gmpz_set"
const mvmpz_set_d = "__gmpz_set_d"
const mvmpz_set_f = "__gmpz_set_f"
const mvmpz_set_fr = "mpfr_get_z"
const mvmpz_set_q = "__gmpz_set_q"
const mvmpz_set_si = "__gmpz_set_si"
const mvmpz_set_str = "__gmpz_set_str"
const mvmpz_set_ui = "__gmpz_set_ui"
const mvmpz_setbit = "__gmpz_setbit"
const mvmpz_si_kronecker = "__gmpz_si_kronecker"
const mvmpz_size = "__gmpz_size"
const mvmpz_sizeinbase = "__gmpz_sizeinbase"
const mvmpz_sqrt = "__gmpz_sqrt"
const mvmpz_sqrtrem = "__gmpz_sqrtrem"
const mvmpz_sub = "__gmpz_sub"
const mvmpz_sub_ui = "__gmpz_sub_ui"
const mvmpz_submul = "__gmpz_submul"
const mvmpz_submul_ui = "__gmpz_submul_ui"
const mvmpz_swap = "__gmpz_swap"
const mvmpz_tdiv_q = "__gmpz_tdiv_q"
const mvmpz_tdiv_q_2exp = "__gmpz_tdiv_q_2exp"
const mvmpz_tdiv_q_ui = "__gmpz_tdiv_q_ui"
const mvmpz_tdiv_qr = "__gmpz_tdiv_qr"
const mvmpz_tdiv_qr_ui = "__gmpz_tdiv_qr_ui"
const mvmpz_tdiv_r = "__gmpz_tdiv_r"
const mvmpz_tdiv_r_2exp = "__gmpz_tdiv_r_2exp"
const mvmpz_tdiv_r_ui = "__gmpz_tdiv_r_ui"
const mvmpz_tdiv_ui = "__gmpz_tdiv_ui"
const mvmpz_tstbit = "__gmpz_tstbit"
const mvmpz_ui_kronecker = "__gmpz_ui_kronecker"
const mvmpz_ui_pow_ui = "__gmpz_ui_pow_ui"
const mvmpz_ui_sub = "__gmpz_ui_sub"
const mvmpz_urandomb = "__gmpz_urandomb"
const mvmpz_urandomm = "__gmpz_urandomm"
const mvmpz_xor = "__gmpz_xor"
const mvunix = 1

type tn__builtin_va_list = ppuintptr

type tn__predefined_size_t = ppuint64

type tn__predefined_wchar_t = ppuint32

type tn__predefined_ptrdiff_t = ppint64

type tnsize_t = ppuint64

type tnssize_t = ppint64

type tnoff_t = ppint64

type tnva_list = ppuintptr

type tn__isoc_va_list = ppuintptr

type tnfpos_t = struct {
	fd__lldata [0]ppint64
	fd__align  [0]ppfloat64
	fd__opaque [16]ppuint8
}

type tu_G_fpos64_t = tnfpos_t

type tncookie_io_functions_t = struct {
	fdread   ppuintptr
	fdwrite  ppuintptr
	fdseek   ppuintptr
	fdclose1 ppuintptr
}

type ts_IO_cookie_io_functions_t = tncookie_io_functions_t

type tnlocale_t = ppuintptr

type tnwchar_t = ppuint32

type tndiv_t = struct {
	fdquot ppint32
	fdrem  ppint32
}

type tnldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnlldiv_t = struct {
	fdquot ppint64
	fdrem  ppint64
}

type tnmax_align_t = struct {
	fd__ll ppint64
	fd__ld ppfloat64
}

type tnptrdiff_t = ppint64

type tnmp_limb_t = ppuint64

type tnmp_limb_signed_t = ppint64

type tnmp_bitcnt_t = ppuint64

type tn__mpz_struct = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnMP_INT = struct {
	fd_mp_alloc ppint32
	fd_mp_size  ppint32
	fd_mp_d     ppuintptr
}

type tnmpz_t = [1]tn__mpz_struct

type tnmp_ptr = ppuintptr

type tnmp_srcptr = ppuintptr

type tnmp_size_t = ppint64

type tnmp_exp_t = ppint64

type tn__mpq_struct = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnMP_RAT = struct {
	fd_mp_num tn__mpz_struct
	fd_mp_den tn__mpz_struct
}

type tnmpq_t = [1]tn__mpq_struct

type tn__mpf_struct = struct {
	fd_mp_prec ppint32
	fd_mp_size ppint32
	fd_mp_exp  tnmp_exp_t
	fd_mp_d    ppuintptr
}

type tnmpf_t = [1]tn__mpf_struct

type tngmp_randalg_t = ppint32

const ecGMP_RAND_ALG_DEFAULT = 0
const ecGMP_RAND_ALG_LC = 0

type tn__gmp_randstate_struct = struct {
	fd_mp_seed    tnmpz_t
	fd_mp_alg     tngmp_randalg_t
	fd_mp_algdata struct {
		fd_mp_lc ppuintptr
	}
}

type tngmp_randstate_t = [1]tn__gmp_randstate_struct

type tnmpz_srcptr = ppuintptr

type tnmpz_ptr = ppuintptr

type tnmpf_srcptr = ppuintptr

type tnmpf_ptr = ppuintptr

type tnmpq_srcptr = ppuintptr

type tnmpq_ptr = ppuintptr

const ecGMP_ERROR_NONE = 0
const ecGMP_ERROR_UNSUPPORTED_ARGUMENT = 1
const ecGMP_ERROR_DIVISION_BY_ZERO = 2
const ecGMP_ERROR_SQRT_OF_NEGATIVE = 4
const ecGMP_ERROR_INVALID_ARGUMENT = 8

type tnmpfr_void = struct{}

type tnmpfr_int = ppint32

type tnmpfr_uint = ppuint32

type tnmpfr_long = ppint64

type tnmpfr_ulong = ppuint64

type tnmpfr_size_t = ppuint64

type tnmpfr_flags_t = ppuint32

type tnmpfr_rnd_t = ppint32

const ecMPFR_RNDN = 0
const ecMPFR_RNDZ = 1
const ecMPFR_RNDU = 2
const ecMPFR_RNDD = 3
const ecMPFR_RNDA = 4
const ecMPFR_RNDF = 5
const ecMPFR_RNDNA = -1

type tnmpfr_prec_t = ppint64

type tnmpfr_uprec_t = ppuint64

type tnmpfr_sign_t = ppint32

type tnmpfr_exp_t = ppint64

type tnmpfr_uexp_t = ppuint64

type tn__mpfr_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
}

type tnmpfr_t = [1]tn__mpfr_struct

type tnmpfr_ptr = ppuintptr

type tnmpfr_srcptr = ppuintptr

type tnmpfr_kind_t = ppint32

const ecMPFR_NAN_KIND = 0
const ecMPFR_INF_KIND = 1
const ecMPFR_ZERO_KIND = 2
const ecMPFR_REGULAR_KIND = 3

type tnmpfr_free_cache_t = ppint32

const ecMPFR_FREE_LOCAL_CACHE = 1
const ecMPFR_FREE_GLOBAL_CACHE = 2

type tnUQItype = ppuint32

type tnSItype = ppint32

type tnUSItype = ppuint32

type tnDItype = ppint32

type tnUDItype = ppuint32

type tnUWtype = ppuint64

type tnUHWtype = ppuint32

type tsbases = struct {
	fdchars_per_bit_exactly ppfloat64
}

type tstmp_marker = struct {
	fdptr  ppuintptr
	fdsize tnsize_t
	fdnext ppuintptr
}

type tnmpfr_pi1_t = struct {
	fdinv32 tnmp_limb_t
}

type tnmpfr_limb_ptr = ppuintptr

type tnmpfr_limb_srcptr = ppuintptr

type tumpfr_ieee_double_extract = struct {
	fdd [0]ppfloat64
	fds struct {
		fd__ccgo0 uint32
		fd__ccgo4 uint32
	}
}

type ts__gmpfr_cache_s = struct {
	fdx       tnmpfr_t
	fdinexact ppint32
	fdfunc1   ppuintptr
}

type tnmpfr_cache_t = [1]ts__gmpfr_cache_s

type tnmpfr_cache_ptr = ppuintptr

type tnmpfr_eexp_t = ppint64

type tnmpfr_ueexp_t = ppuint64

type tnmpfr_size_limb_t = struct {
	fdl [0]tnmp_limb_t
	fds tnmp_size_t
}

type tslconv = struct {
	fddecimal_point      ppuintptr
	fdthousands_sep      ppuintptr
	fdgrouping           ppuintptr
	fdint_curr_symbol    ppuintptr
	fdcurrency_symbol    ppuintptr
	fdmon_decimal_point  ppuintptr
	fdmon_thousands_sep  ppuintptr
	fdmon_grouping       ppuintptr
	fdpositive_sign      ppuintptr
	fdnegative_sign      ppuintptr
	fdint_frac_digits    ppuint8
	fdfrac_digits        ppuint8
	fdp_cs_precedes      ppuint8
	fdp_sep_by_space     ppuint8
	fdn_cs_precedes      ppuint8
	fdn_sep_by_space     ppuint8
	fdp_sign_posn        ppuint8
	fdn_sign_posn        ppuint8
	fdint_p_cs_precedes  ppuint8
	fdint_p_sep_by_space ppuint8
	fdint_n_cs_precedes  ppuint8
	fdint_n_sep_by_space ppuint8
	fdint_p_sign_posn    ppuint8
	fdint_n_sign_posn    ppuint8
}

type tnmpfr_save_expo_t = struct {
	fdsaved_flags tnmpfr_flags_t
	fdsaved_emin  tnmpfr_exp_t
	fdsaved_emax  tnmpfr_exp_t
}

type tsmpfr_group_t = struct {
	fdalloc tnsize_t
	fdmant  ppuintptr
	fdtab   [16]tnmp_limb_t
}

type tn__mpfr_ubf_struct = struct {
	fd_mpfr_prec tnmpfr_prec_t
	fd_mpfr_sign tnmpfr_sign_t
	fd_mpfr_exp  tnmpfr_exp_t
	fd_mpfr_d    ppuintptr
	fd_mpfr_zexp tnmpz_t
}

type tnmpfr_ubf_t = [1]tn__mpfr_ubf_struct

type tnmpfr_ubf_ptr = ppuintptr

/* With GCC, a macro "volatile" can be defined to test some special code
   in mpfr-impl.h (code for compilers that define such a macro), but the
   volatile keyword is necessary in some tests to avoid some GCC bugs.
   Thus we need to undef this macro (if defined). We do that at the end,
   so that mpfr-impl.h (included earlier) is not affected by this undef.
*/

/* Generic test file for functions with one or two arguments (the second being
   either mpfr_t or double or unsigned long).

Copyright 2001-2023 Free Software Foundation, Inc.
Contributed by the AriC and Caramba projects, INRIA.

This file is part of the GNU MPFR Library.

The GNU MPFR Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The GNU MPFR Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

/* Define TWO_ARGS for two-argument functions like mpfr_pow.
   Define DOUBLE_ARG1 or DOUBLE_ARG2 for function with a double operand in
   first or second place like sub_d or d_sub.
   Define ULONG_ARG1 or ULONG_ARG2 for function with an unsigned long
   operand in first or second place like sub_ui or ui_sub.
   Define THREE_ARGS for three-argument functions like mpfr_atan2u. */

/* TODO: Add support for type long and extreme integer values, as done
   in tgeneric_ui.c; then tgeneric_ui.c could probably disappear. */

/* For the random function: one number on two is negative. */

/* For the random function: one number on two is negative. */

/* If the MPFR_SUSPICIOUS_OVERFLOW test fails but this is not a bug,
   then define TGENERIC_SO_TEST with an adequate test (possibly 0) to
   omit this particular case. */

/* For some functions (for example cos), the argument reduction is too
   expensive when using mpfr_get_emax(). Then simply define REDUCE_EMAX
   to some reasonable value before including tgeneric.c. */

/* same for mpfr_get_emin() */

func sitest_generic_erf(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aanmax ppuint32) {
	cgbp := cgtls.ppAlloc(288)
	defer cgtls.ppFree(288)

	var aa_p tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, aainfinite_input, aatest_of, aatest_uf, ccv11, ccv6, ccv8 ppint32
	var aactrn, aactrt ppuint64
	var aae, aaemax, aaemin, aaoemax, aaoemin, aaold_emax, aaold_emin tnmpfr_exp_t
	var aaex_flags, aaex_flags1, aaflags, aaoldflags tnmpfr_flags_t
	var aan, ccv3 ppuint32
	var aaprec, aaxprec, aayprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv10, ccv12, ccv13, ccv14, ccv15, ccv5, ccv7, ccv9 ppbool
	var ccv4 ppfloat64
	var pp_ /* t at bp+96 */ tnmpfr_t
	var pp_ /* w at bp+128 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* yd at bp+160 */ tnmpfr_t
	var pp_ /* yu at bp+192 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aacompare, aacompare2, aactrn, aactrt, aae, aaemax, aaemin, aaex_flags, aaex_flags1, aaflags, aainexact, aainfinite_input, aan, aaoemax, aaoemin, aaold_emax, aaold_emin, aaoldflags, aaprec, aarnd, aatest_of, aatest_uf, aaxprec, aayprec, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9
	aactrt = ppuint64(0)
	aactrn = ppuint64(0)

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	Xmpfr_inits2(cgtls, ppint64(mvMPFR_PREC_MIN), cgbp, iqlibc.ppVaList(cgbp+232, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))

	/* generic tests */
	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		/* Number of overflow/underflow tests for each precision.
		   Since MPFR uses several algorithms and there may also be
		   early overflow/underflow detection, several tests may be
		   needed to detect a bug. */
		aatest_of = ppint32(3)
		aatest_uf = ppint32(3)

		Xmpfr_set_prec(cgtls, cgbp+64, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+96, aaprec)
		aayprec = aaprec + ppint64(20)
		Xmpfr_set_prec(cgtls, cgbp+32, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+160, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+192, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+128, aayprec)

		/* Note: in precision p1, we test NSPEC special cases. */
		aan = ppuint32(0)
		for {
			if aaprec == aap1 {
				ccv3 = aanmax + ppuint32(5)
			} else {
				ccv3 = aanmax
			}
			if !(aan < ccv3) {
				break
			}

			aainfinite_input = 0

			aaxprec = aaprec
			if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {

				/* In half cases, modify the precision of the inputs:
				   If the base precision (for the result) is small,
				   take a larger input precision in general, else
				   take a smaller precision. */
				if aaprec < ppint64(16) {
					ccv4 = ppfloat64(256)
				} else {
					ccv4 = ppfloat64(1)
				}
				aaxprec = tnmpfr_prec_t(ppfloat64(aaxprec) * (ccv4 * ppfloat64(Xrandlimb(cgtls)) / ppfloat64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1)))))
				if aaxprec < ppint64(mvMPFR_PREC_MIN) {
					aaxprec = ppint64(mvMPFR_PREC_MIN)
				}
			}
			Xmpfr_set_prec(cgtls, cgbp, aaxprec)

			/* Generate random arguments, even in the special cases
			   (this may not be needed, but this is simpler).
			   Note that if RAND_FUNCTION is defined, this specific
			   random function is used for all arguments; this is
			   typically mpfr_random2, which generates a positive
			   random mpfr_t with long runs of consecutive ones and
			   zeros in the binary representation. */

			Xtests_default_random(cgtls, cgbp, ppint32(256), ppint64(-ppint32(256)), ppint64(255), 0)

			if aan < ppuint32(5) && aaprec == aap1 {

				/* Special cases tested in precision p1 if n < NSPEC. They are
				   useful really in the extended exponent range. */
				/* TODO: x2 is set even when it is associated with a double;
				   check whether this really makes sense. */
				Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
				Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
				if aan == ppuint32(0) {

					Xmpfr_set_nan(cgtls, cgbp)
				} else {
					if aan <= ppuint32(2) {

						if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(1) || aan == ppuint32(2))), ppint64(1)) != 0; !ccv5 {
							Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(290), "n == 1 || n == 2\x00")
						}
						pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
						if aan == ppuint32(1) {
							ccv6 = ppint32(1)
						} else {
							ccv6 = -ppint32(1)
						}
						pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv6), 0, ppint32(ecMPFR_RNDN))
						Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)
					} else {
						if aan <= ppuint32(4) {

							if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(3) || aan == ppuint32(4))), ppint64(1)) != 0; !ccv7 {
								Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(300), "n == 3 || n == 4\x00")
							}
							pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
							if aan == ppuint32(3) {
								ccv8 = ppint32(1)
							} else {
								ccv8 = -ppint32(1)
							}
							pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv8), 0, ppint32(ecMPFR_RNDN))
							Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)
						}
					}
				}
			}

			/* Exponent range for the test. */
			aaoemin = X__gmpfr_emin
			aaoemax = X__gmpfr_emax

			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt32(ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)))
			Xmpfr_clear_flags(cgtls)
			aacompare = Xmpfr_erf(cgtls, cgbp+32, cgbp, aarnd)
			aaflags = X__gmpfr_flags
			if X__gmpfr_emin != aaoemin || X__gmpfr_emax != aaoemax {

				Xprintf(cgtls, "tgeneric: the exponent range has been modified by the tested function!\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
			if aarnd != ppint32(ecMPFR_RNDF) {
				if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) == iqlibc.ppInt32FromInt32(0)) != 0) {
					Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad inexact flag for mpfr_erf\x00"))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					if 0 != 0 {
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, ppuintptr(0))
					}
					if -ppint32(1) >= 0 {
						Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
					}
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}
			aactrt++

			/* If rnd = RNDF, check that we obtain the same result as
			   RNDD or RNDU. */
			if aarnd == ppint32(ecMPFR_RNDF) {

				Xmpfr_erf(cgtls, cgbp+160, cgbp, ppint32(ecMPFR_RNDD))
				Xmpfr_erf(cgtls, cgbp+192, cgbp, ppint32(ecMPFR_RNDU))
				if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+160) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_sign || ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+192) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_sign)) {

					Xprintf(cgtls, "tgeneric: error formpfr_erf, RNDF; result matches neither RNDD nor RNDU\n\x00", 0)
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "yd (RNDD) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+160)
					Xprintf(cgtls, "yu (RNDU) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+192)
					Xprintf(cgtls, "y  (RNDF) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xexit(cgtls, ppint32(1))
				}
			}

			/* Tests in a reduced exponent range. */

			aaoldflags = aaflags

			/* Determine the smallest exponent range containing the
			   exponents of the mpfr_t inputs (x, and u if TWO_ARGS)
			   and output (y). */
			aaemin = iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) - iqlibc.ppInt64FromInt32(1)
			aaemax = iqlibc.ppInt64FromInt32(1) - iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))

			if ccv9 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv9 {
			}
			if ccv9 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv10 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv10 {
			}
			if ccv10 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp /* exponent of the result */

				if aatest_of > 0 && aae-ppint64(1) >= aaemax { /* overflow test */

					/* Exponent e of the result > exponents of the inputs;
					   let's set emax to e - 1, so that one should get an
					   overflow. */
					Xset_emax(cgtls, aae-ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = Xmpfr_erf(cgtls, cgbp+128, cgbp, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emax(cgtls, aaoemax)
					aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for mpfr_erf, reduced exponent range [%ld,%ld] (overflow test) on:\n\x00", iqlibc.ppVaList(cgbp+232, aaoemin, aae-ppint64(1)))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+232, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_of--
				}

				if aatest_uf > 0 && aae+ppint64(1) <= aaemin { /* underflow test */

					/* Exponent e of the result < exponents of the inputs;
					   let's set emin to e + 1, so that one should get an
					   underflow. */
					Xset_emin(cgtls, aae+ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = Xmpfr_erf(cgtls, cgbp+128, cgbp, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emin(cgtls, aaoemin)
					aaex_flags1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags1 && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for mpfr_erf, reduced exponent range [%ld,%ld] (underflow test) on:\n\x00", iqlibc.ppVaList(cgbp+232, aae+ppint64(1), aaoemax))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags1)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+232, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_uf--
				}

				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			} /* MPFR_IS_PURE_FP (y) */

			if aaemin > aaemax {
				aaemin = aaemax
			} /* case where all values are singular */

			/* Consistency test in a reduced exponent range. Doing it
			   for the first 10 samples and for prec == p1 (which has
			   some special cases) should be sufficient. */
			if aactrt <= ppuint64(10) || aaprec == aap1 {

				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				Xmpfr_clear_flags(cgtls)
				aainexact = Xmpfr_erf(cgtls, cgbp+128, cgbp, aarnd)
				aaflags = X__gmpfr_flags
				Xset_emin(cgtls, aaoemin)
				Xset_emax(cgtls, aaoemax)
				/* That test makes no sense for RNDF. */
				if aarnd != ppint32(ecMPFR_RNDF) && !(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+128, cgbp+32) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign) && iqlibc.ppBoolInt32(aainexact > 0)-iqlibc.ppBoolInt32(aainexact < 0) == iqlibc.ppBoolInt32(aacompare > 0)-iqlibc.ppBoolInt32(aacompare < 0) && aaflags == aaoldflags) {

					Xprintf(cgtls, "tgeneric: error for mpfr_erf, reduced exponent range [%ld,%ld] on:\n\x00", iqlibc.ppVaList(cgbp+232, aaemin, aaemax))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "Expected:\n  y = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+232, aacompare))
					Xflags_out(cgtls, aaoldflags)
					Xprintf(cgtls, "Got:\n  w = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+128)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+232, aainexact))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}

			X__gmpfr_flags = aaoldflags /* restore the flags */
			/* tests in a reduced exponent range */

			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 {
					if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad NaN flag for mpfr_erf\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

						if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad overflow flag for mpfr_erf\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(iqlibc.ppBoolInt32(aacompare == 0 && !(aainfinite_input != 0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad divide-by-zero flag for mpfr_erf\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) {
							if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad underflow flag for mpfr_erf\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if 0 != 0 {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, ppuintptr(0))
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						}
					}
				}
			} else {
				if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0 {

					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "both overflow and divide-by-zero for mpfr_erf\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "both underflow and divide-by-zero for mpfr_erf\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !(aacompare == iqlibc.ppInt32FromInt32(0)) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad compare value (divide-by-zero) for mpfr_erf\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 {

						if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "both underflow and overflow for mpfr_erf\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad compare value (overflow) for mpfr_erf\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						Xmpfr_nexttoinf(cgtls, cgbp+32)
						if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "should have been max MPFR number (overflow) for mpfr_erf\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {

							if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad compare value (underflow) for mpfr_erf\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if 0 != 0 {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, ppuintptr(0))
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
							Xmpfr_nexttozero(cgtls, cgbp+32)
							if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "should have been min MPFR number (underflow) for mpfr_erf\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if 0 != 0 {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, ppuintptr(0))
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						} else {
							if aacompare == 0 || aarnd == ppint32(ecMPFR_RNDF) || Xmpfr_can_round(cgtls, cgbp+32, aayprec, aarnd, aarnd, aaprec) != 0 {

								aactrn++
								{
									aa_p = cgbp + 32
									ccv11 = Xmpfr_set4(cgtls, cgbp+96, aa_p, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
								}
								pp_ = ccv11
								/* Risk of failures are known when some flags are already set
								   before the function call. Do not set the erange flag, as
								   it will remain set after the function call and no checks
								   are performed in such a case (see the mpfr_erangeflag_p
								   test below). */
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									X__gmpfr_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0) ^ iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE))
								}
								/* Let's increase the precision of the inputs in a random way.
								   In most cases, this doesn't make any difference, but for
								   the mpfr_fmod bug fixed in r6230, this triggers the bug. */
								Xmpfr_prec_round(cgtls, cgbp, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint64(15)), ppint32(ecMPFR_RNDN))
								aainexact = Xmpfr_erf(cgtls, cgbp+64, cgbp, aarnd)
								if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0 {
									goto ppnext_n
								}
								if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+64) != 0) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "tgeneric: results differ for mpfr_erf on\n\x00", 0)
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "prec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
									Xprintf(cgtls, "Got      \x00", 0)
									Xmpfr_dump(cgtls, cgbp+64)
									Xprintf(cgtls, "Expected \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xprintf(cgtls, "Approx   \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xexit(cgtls, ppint32(1))
								}
								aacompare2 = Xmpfr_cmp3(cgtls, cgbp+96, cgbp+32, ppint32(1))
								/* if rounding to nearest, cannot know the sign of t - f(x)
								   because of composed rounding: y = o(f(x)) and t = o(y) */
								if aacompare*aacompare2 >= 0 {
									aacompare = aacompare + aacompare2
								} else {
									aacompare = aainexact
								} /* cannot determine sign(t-f(x)) */
								if !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aacompare > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aacompare < iqlibc.ppInt32FromInt32(0))) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+232, Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare, aainexact))
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "y = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xprintf(cgtls, "t = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xexit(cgtls, ppint32(1))
								}
							} else {
								if Xgetenv(cgtls, "MPFR_SUSPICIOUS_OVERFLOW\x00") != iqlibc.ppUintptrFromInt32(0) {

									/* For developers only! */

									if ccv12 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv12 {
									}
									if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv12 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0))), ppint64(1)) != 0; !ccv13 {
										Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(803), "(!(((y)->_mpfr_exp) <= (((-0x7fffffffffffffffL-1))+3)) && ((! __builtin_constant_p (!!(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) || !(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0))) || (((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) ? (void) 0 : __builtin_unreachable()), 1))\x00")
									}
									pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
									Xmpfr_nexttoinf(cgtls, cgbp+32)

									if ccv15 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3); ccv15 {
										if ccv14 = aarnd == ppint32(ecMPFR_RNDZ); !ccv14 {
										}
									}
									if ccv15 && (ccv14 || aarnd+iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)) && !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) && iqlibc.Bool(ppint32(mvTGENERIC_SO_TEST) != 0) {

										Xprintf(cgtls, "Possible bug! |y| is the maximum finite number (with yprec = %u) and has\nbeen obtained when rounding toward zero (%s). Thus there is a very\nprobable overflow, but the overflow flag is not set!\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
										Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp)
										Xexit(cgtls, ppint32(1))
									}
								}
							}
						}
					}
				}
			}

			goto ppnext_n
		ppnext_n:
			;
			/* In case the exponent range has been changed by
			   tests_default_random() or for special values... */
			Xset_emin(cgtls, aaold_emin)
			Xset_emax(cgtls, aaold_emax)

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	if Xgetenv(cgtls, "MPFR_TGENERIC_STAT\x00") != iqlibc.ppUintptrFromInt32(0) {
		Xprintf(cgtls, "tgeneric: normal cases / total = %lu / %lu\n\x00", iqlibc.ppVaList(cgbp+232, aactrn, aactrt))
	}

	if ppuint64(3)*aactrn < ppuint64(2)*aactrt {
		Xprintf(cgtls, "Warning! Too few normal cases in generic tests (%lu / %lu)\n\x00", iqlibc.ppVaList(cgbp+232, aactrn, aactrt))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+232, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
}

/* Generic test file for functions with one or two arguments (the second being
   either mpfr_t or double or unsigned long).

Copyright 2001-2023 Free Software Foundation, Inc.
Contributed by the AriC and Caramba projects, INRIA.

This file is part of the GNU MPFR Library.

The GNU MPFR Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The GNU MPFR Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the GNU MPFR Library; see the file COPYING.LESSER.  If not, see
https://www.gnu.org/licenses/ or write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */

/* Define TWO_ARGS for two-argument functions like mpfr_pow.
   Define DOUBLE_ARG1 or DOUBLE_ARG2 for function with a double operand in
   first or second place like sub_d or d_sub.
   Define ULONG_ARG1 or ULONG_ARG2 for function with an unsigned long
   operand in first or second place like sub_ui or ui_sub.
   Define THREE_ARGS for three-argument functions like mpfr_atan2u. */

/* TODO: Add support for type long and extreme integer values, as done
   in tgeneric_ui.c; then tgeneric_ui.c could probably disappear. */

/* For the random function: one number on two is negative. */

/* For the random function: one number on two is negative. */

/* If the MPFR_SUSPICIOUS_OVERFLOW test fails but this is not a bug,
   then define TGENERIC_SO_TEST with an adequate test (possibly 0) to
   omit this particular case. */

/* For some functions (for example cos), the argument reduction is too
   expensive when using mpfr_get_emax(). Then simply define REDUCE_EMAX
   to some reasonable value before including tgeneric.c. */

/* same for mpfr_get_emin() */

func sitest_generic_erfc(cgtls *iqlibc.ppTLS, aap0 tnmpfr_prec_t, aap1 tnmpfr_prec_t, aanmax ppuint32) {
	cgbp := cgtls.ppAlloc(288)
	defer cgtls.ppFree(288)

	var aa_p tnmpfr_srcptr
	var aacompare, aacompare2, aainexact, aainfinite_input, aatest_of, aatest_uf, ccv11, ccv6, ccv8 ppint32
	var aactrn, aactrt ppuint64
	var aae, aaemax, aaemin, aaoemax, aaoemin, aaold_emax, aaold_emin tnmpfr_exp_t
	var aaex_flags, aaex_flags1, aaflags, aaoldflags tnmpfr_flags_t
	var aan, ccv3 ppuint32
	var aaprec, aaxprec, aayprec tnmpfr_prec_t
	var aarnd tnmpfr_rnd_t
	var ccv10, ccv12, ccv13, ccv14, ccv15, ccv5, ccv7, ccv9 ppbool
	var ccv4 ppfloat64
	var pp_ /* t at bp+96 */ tnmpfr_t
	var pp_ /* w at bp+128 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* yd at bp+160 */ tnmpfr_t
	var pp_ /* yu at bp+192 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_, pp_ = aa_p, aacompare, aacompare2, aactrn, aactrt, aae, aaemax, aaemin, aaex_flags, aaex_flags1, aaflags, aainexact, aainfinite_input, aan, aaoemax, aaoemin, aaold_emax, aaold_emin, aaoldflags, aaprec, aarnd, aatest_of, aatest_uf, aaxprec, aayprec, ccv10, ccv11, ccv12, ccv13, ccv14, ccv15, ccv3, ccv4, ccv5, ccv6, ccv7, ccv8, ccv9
	aactrt = ppuint64(0)
	aactrn = ppuint64(0)

	aaold_emin = X__gmpfr_emin
	aaold_emax = X__gmpfr_emax

	Xmpfr_inits2(cgtls, ppint64(mvMPFR_PREC_MIN), cgbp, iqlibc.ppVaList(cgbp+232, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))

	/* generic tests */
	aaprec = aap0
	for {
		if !(aaprec <= aap1) {
			break
		}

		/* Number of overflow/underflow tests for each precision.
		   Since MPFR uses several algorithms and there may also be
		   early overflow/underflow detection, several tests may be
		   needed to detect a bug. */
		aatest_of = ppint32(3)
		aatest_uf = ppint32(3)

		Xmpfr_set_prec(cgtls, cgbp+64, aaprec)
		Xmpfr_set_prec(cgtls, cgbp+96, aaprec)
		aayprec = aaprec + ppint64(20)
		Xmpfr_set_prec(cgtls, cgbp+32, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+160, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+192, aayprec)
		Xmpfr_set_prec(cgtls, cgbp+128, aayprec)

		/* Note: in precision p1, we test NSPEC special cases. */
		aan = ppuint32(0)
		for {
			if aaprec == aap1 {
				ccv3 = aanmax + ppuint32(5)
			} else {
				ccv3 = aanmax
			}
			if !(aan < ccv3) {
				break
			}

			aainfinite_input = 0

			aaxprec = aaprec
			if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {

				/* In half cases, modify the precision of the inputs:
				   If the base precision (for the result) is small,
				   take a larger input precision in general, else
				   take a smaller precision. */
				if aaprec < ppint64(16) {
					ccv4 = ppfloat64(256)
				} else {
					ccv4 = ppfloat64(1)
				}
				aaxprec = tnmpfr_prec_t(ppfloat64(aaxprec) * (ccv4 * ppfloat64(Xrandlimb(cgtls)) / ppfloat64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1)))))
				if aaxprec < ppint64(mvMPFR_PREC_MIN) {
					aaxprec = ppint64(mvMPFR_PREC_MIN)
				}
			}
			Xmpfr_set_prec(cgtls, cgbp, aaxprec)

			/* Generate random arguments, even in the special cases
			   (this may not be needed, but this is simpler).
			   Note that if RAND_FUNCTION is defined, this specific
			   random function is used for all arguments; this is
			   typically mpfr_random2, which generates a positive
			   random mpfr_t with long runs of consecutive ones and
			   zeros in the binary representation. */

			Xtests_default_random(cgtls, cgbp, ppint32(256), ppint64(-ppint32(256)), ppint64(63), 0)

			if aan < ppuint32(5) && aaprec == aap1 {

				/* Special cases tested in precision p1 if n < NSPEC. They are
				   useful really in the extended exponent range. */
				/* TODO: x2 is set even when it is associated with a double;
				   check whether this really makes sense. */
				Xset_emin(cgtls, iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)))
				Xset_emax(cgtls, iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))-iqlibc.ppInt64FromInt32(1))
				if aan == ppuint32(0) {

					Xmpfr_set_nan(cgtls, cgbp)
				} else {
					if aan <= ppuint32(2) {

						if ccv5 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(1) || aan == ppuint32(2))), ppint64(1)) != 0; !ccv5 {
							Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(290), "n == 1 || n == 2\x00")
						}
						pp_ = ccv5 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
						if aan == ppuint32(1) {
							ccv6 = ppint32(1)
						} else {
							ccv6 = -ppint32(1)
						}
						pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv6), 0, ppint32(ecMPFR_RNDN))
						Xmpfr_set_exp(cgtls, cgbp, X__gmpfr_emin)
					} else {
						if aan <= ppuint32(4) {

							if ccv7 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aan == ppuint32(3) || aan == ppuint32(4))), ppint64(1)) != 0; !ccv7 {
								Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(300), "n == 3 || n == 4\x00")
							}
							pp_ = ccv7 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
							if aan == ppuint32(3) {
								ccv8 = ppint32(1)
							} else {
								ccv8 = -ppint32(1)
							}
							pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(ccv8), 0, ppint32(ecMPFR_RNDN))
							Xmpfr_setmax(cgtls, cgbp, X__gmpfr_emax)
						}
					}
				}
			}

			/* Exponent range for the test. */
			aaoemin = X__gmpfr_emin
			aaoemax = X__gmpfr_emax

			aarnd = iqlibc.ppInt32FromUint64(Xrandlimb(cgtls) % iqlibc.ppUint64FromInt32(ppint32(ecMPFR_RNDF)+iqlibc.ppInt32FromInt32(1)))
			Xmpfr_clear_flags(cgtls)
			aacompare = Xmpfr_erfc(cgtls, cgbp+32, cgbp, aarnd)
			aaflags = X__gmpfr_flags
			if X__gmpfr_emin != aaoemin || X__gmpfr_emax != aaoemax {

				Xprintf(cgtls, "tgeneric: the exponent range has been modified by the tested function!\n\x00", 0)
				Xexit(cgtls, ppint32(1))
			}
			if aarnd != ppint32(ecMPFR_RNDF) {
				if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT)) == iqlibc.ppInt32FromInt32(0)) != 0) {
					Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad inexact flag for mpfr_erfc\x00"))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					if 0 != 0 {
						Xprintf(cgtls, "x2 = \x00", 0)
						Xmpfr_dump(cgtls, ppuintptr(0))
					}
					if -ppint32(1) >= 0 {
						Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
					}
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}
			aactrt++

			/* If rnd = RNDF, check that we obtain the same result as
			   RNDD or RNDU. */
			if aarnd == ppint32(ecMPFR_RNDF) {

				Xmpfr_erfc(cgtls, cgbp+160, cgbp, ppint32(ecMPFR_RNDD))
				Xmpfr_erfc(cgtls, cgbp+192, cgbp, ppint32(ecMPFR_RNDU))
				if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+160) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+160)).fd_mpfr_sign || ((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+32, cgbp+192) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+192)).fd_mpfr_sign)) {

					Xprintf(cgtls, "tgeneric: error formpfr_erfc, RNDF; result matches neither RNDD nor RNDU\n\x00", 0)
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "yd (RNDD) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+160)
					Xprintf(cgtls, "yu (RNDU) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+192)
					Xprintf(cgtls, "y  (RNDF) = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xexit(cgtls, ppint32(1))
				}
			}

			/* Tests in a reduced exponent range. */

			aaoldflags = aaflags

			/* Determine the smallest exponent range containing the
			   exponents of the mpfr_t inputs (x, and u if TWO_ARGS)
			   and output (y). */
			aaemin = iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2)) - iqlibc.ppInt64FromInt32(1)
			aaemax = iqlibc.ppInt64FromInt32(1) - iqlibc.ppInt64FromInt32(1)<<(iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(mvGMP_LIMB_BITS)-iqlibc.ppInt32FromInt32(mvGMP_NAIL_BITS))*iqlibc.ppUint64FromInt64(8)/iqlibc.ppUint64FromInt64(8)-iqlibc.ppUint64FromInt32(2))

			if ccv9 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv9 {
			}
			if ccv9 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_exp
				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			}

			if ccv10 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv10 {
			}
			if ccv10 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0) {

				aae = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp + 32)).fd_mpfr_exp /* exponent of the result */

				if aatest_of > 0 && aae-ppint64(1) >= aaemax { /* overflow test */

					/* Exponent e of the result > exponents of the inputs;
					   let's set emax to e - 1, so that one should get an
					   overflow. */
					Xset_emax(cgtls, aae-ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = Xmpfr_erfc(cgtls, cgbp+128, cgbp, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emax(cgtls, aaoemax)
					aaex_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for mpfr_erfc, reduced exponent range [%ld,%ld] (overflow test) on:\n\x00", iqlibc.ppVaList(cgbp+232, aaoemin, aae-ppint64(1)))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+232, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_of--
				}

				if aatest_uf > 0 && aae+ppint64(1) <= aaemin { /* underflow test */

					/* Exponent e of the result < exponents of the inputs;
					   let's set emin to e + 1, so that one should get an
					   underflow. */
					Xset_emin(cgtls, aae+ppint64(1))
					Xmpfr_clear_flags(cgtls)
					aainexact = Xmpfr_erfc(cgtls, cgbp+128, cgbp, aarnd)
					aaflags = X__gmpfr_flags
					Xset_emin(cgtls, aaoemin)
					aaex_flags1 = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT))
					/* For RNDF, this test makes no sense, since RNDF
					   might return either the maximal floating-point
					   value or infinity, and the flags might differ in
					   those two cases. */
					if aaflags != aaex_flags1 && aarnd != ppint32(ecMPFR_RNDF) {

						Xprintf(cgtls, "tgeneric: error for mpfr_erfc, reduced exponent range [%ld,%ld] (underflow test) on:\n\x00", iqlibc.ppVaList(cgbp+232, aae+ppint64(1), aaoemax))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
						Xprintf(cgtls, "Expected flags =\x00", 0)
						Xflags_out(cgtls, aaex_flags1)
						Xprintf(cgtls, "     got flags =\x00", 0)
						Xflags_out(cgtls, aaflags)
						Xprintf(cgtls, "inex = %d, w = \x00", iqlibc.ppVaList(cgbp+232, aainexact))
						Xmpfr_dump(cgtls, cgbp+128)
						Xexit(cgtls, ppint32(1))
					}
					aatest_uf--
				}

				if aae < aaemin {
					aaemin = aae
				}
				if aae > aaemax {
					aaemax = aae
				}
			} /* MPFR_IS_PURE_FP (y) */

			if aaemin > aaemax {
				aaemin = aaemax
			} /* case where all values are singular */

			/* Consistency test in a reduced exponent range. Doing it
			   for the first 10 samples and for prec == p1 (which has
			   some special cases) should be sufficient. */
			if aactrt <= ppuint64(10) || aaprec == aap1 {

				Xset_emin(cgtls, aaemin)
				Xset_emax(cgtls, aaemax)
				Xmpfr_clear_flags(cgtls)
				aainexact = Xmpfr_erfc(cgtls, cgbp+128, cgbp, aarnd)
				aaflags = X__gmpfr_flags
				Xset_emin(cgtls, aaoemin)
				Xset_emax(cgtls, aaoemax)
				/* That test makes no sense for RNDF. */
				if aarnd != ppint32(ecMPFR_RNDF) && !(((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || Xmpfr_equal_p(cgtls, cgbp+128, cgbp+32) != 0 && (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+128)).fd_mpfr_sign == (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign) && iqlibc.ppBoolInt32(aainexact > 0)-iqlibc.ppBoolInt32(aainexact < 0) == iqlibc.ppBoolInt32(aacompare > 0)-iqlibc.ppBoolInt32(aacompare < 0) && aaflags == aaoldflags) {

					Xprintf(cgtls, "tgeneric: error for mpfr_erfc, reduced exponent range [%ld,%ld] on:\n\x00", iqlibc.ppVaList(cgbp+232, aaemin, aaemax))
					Xprintf(cgtls, "x1 = \x00", 0)
					Xmpfr_dump(cgtls, cgbp)
					Xprintf(cgtls, "yprec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
					Xprintf(cgtls, "Expected:\n  y = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+32)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+232, aacompare))
					Xflags_out(cgtls, aaoldflags)
					Xprintf(cgtls, "Got:\n  w = \x00", 0)
					Xmpfr_dump(cgtls, cgbp+128)
					Xprintf(cgtls, "  inex = %d, flags =\x00", iqlibc.ppVaList(cgbp+232, aainexact))
					Xflags_out(cgtls, aaflags)
					Xexit(cgtls, ppint32(1))
				}
			}

			X__gmpfr_flags = aaoldflags /* restore the flags */
			/* tests in a reduced exponent range */

			if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

				if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) || iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0 {
					if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(2) && iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_NAN)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad NaN flag for mpfr_erfc\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3) {

						if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad overflow flag for mpfr_erfc\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(iqlibc.ppBoolInt32(aacompare == 0 && !(aainfinite_input != 0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) == iqlibc.ppInt32FromInt32(0)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad divide-by-zero flag for mpfr_erfc\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) {
							if !(iqlibc.ppBoolInt32(aacompare != iqlibc.ppInt32FromInt32(0))^iqlibc.ppBoolInt32(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) == iqlibc.ppInt32FromInt32(0)) != 0) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad underflow flag for mpfr_erfc\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if 0 != 0 {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, ppuintptr(0))
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						}
					}
				}
			} else {
				if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_DIVBY0)) != 0 {

					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "both overflow and divide-by-zero for mpfr_erfc\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "both underflow and divide-by-zero for mpfr_erfc\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
					if !(aacompare == iqlibc.ppInt32FromInt32(0)) {
						Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad compare value (divide-by-zero) for mpfr_erfc\x00"))
						Xprintf(cgtls, "x1 = \x00", 0)
						Xmpfr_dump(cgtls, cgbp)
						if 0 != 0 {
							Xprintf(cgtls, "x2 = \x00", 0)
							Xmpfr_dump(cgtls, ppuintptr(0))
						}
						if -ppint32(1) >= 0 {
							Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
						}
						Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
						Xflags_out(cgtls, aaflags)
						Xexit(cgtls, ppint32(1))
					}
				} else {
					if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0 {

						if !!(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "both underflow and overflow for mpfr_erfc\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad compare value (overflow) for mpfr_erfc\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
						Xmpfr_nexttoinf(cgtls, cgbp+32)
						if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)) {
							Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "should have been max MPFR number (overflow) for mpfr_erfc\x00"))
							Xprintf(cgtls, "x1 = \x00", 0)
							Xmpfr_dump(cgtls, cgbp)
							if 0 != 0 {
								Xprintf(cgtls, "x2 = \x00", 0)
								Xmpfr_dump(cgtls, ppuintptr(0))
							}
							if -ppint32(1) >= 0 {
								Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
							}
							Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
							Xflags_out(cgtls, aaflags)
							Xexit(cgtls, ppint32(1))
						}
					} else {
						if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_UNDERFLOW)) != 0 {

							if !(aacompare != iqlibc.ppInt32FromInt32(0)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "bad compare value (underflow) for mpfr_erfc\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if 0 != 0 {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, ppuintptr(0))
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
							Xmpfr_nexttozero(cgtls, cgbp+32)
							if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) {
								Xprintf(cgtls, "tgeneric: %s\n\x00", iqlibc.ppVaList(cgbp+232, "should have been min MPFR number (underflow) for mpfr_erfc\x00"))
								Xprintf(cgtls, "x1 = \x00", 0)
								Xmpfr_dump(cgtls, cgbp)
								if 0 != 0 {
									Xprintf(cgtls, "x2 = \x00", 0)
									Xmpfr_dump(cgtls, ppuintptr(0))
								}
								if -ppint32(1) >= 0 {
									Xprintf(cgtls, "u = %lu\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))))
								}
								Xprintf(cgtls, "yprec = %u, rnd_mode = %s, inexact = %d\nflags =\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare))
								Xflags_out(cgtls, aaflags)
								Xexit(cgtls, ppint32(1))
							}
						} else {
							if aacompare == 0 || aarnd == ppint32(ecMPFR_RNDF) || Xmpfr_can_round(cgtls, cgbp+32, aayprec, aarnd, aarnd, aaprec) != 0 {

								aactrn++
								{
									aa_p = cgbp + 32
									ccv11 = Xmpfr_set4(cgtls, cgbp+96, aa_p, aarnd, (*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign)
								}
								pp_ = ccv11
								/* Risk of failures are known when some flags are already set
								   before the function call. Do not set the erange flag, as
								   it will remain set after the function call and no checks
								   are performed in such a case (see the mpfr_erangeflag_p
								   test below). */
								if Xrandlimb(cgtls)&ppuint64(1) != ppuint64(0) {
									X__gmpfr_flags = iqlibc.ppUint32FromInt32(iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_UNDERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_OVERFLOW) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_NAN) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_INEXACT) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE) | iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_DIVBY0) ^ iqlibc.ppInt32FromInt32(mvMPFR_FLAGS_ERANGE))
								}
								/* Let's increase the precision of the inputs in a random way.
								   In most cases, this doesn't make any difference, but for
								   the mpfr_fmod bug fixed in r6230, this triggers the bug. */
								Xmpfr_prec_round(cgtls, cgbp, iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)+Xrandlimb(cgtls)&ppuint64(15)), ppint32(ecMPFR_RNDN))
								aainexact = Xmpfr_erfc(cgtls, cgbp+64, cgbp, aarnd)
								if iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_ERANGE)) != 0 {
									goto ppnext_n
								}
								if !(Xmpfr_equal_p(cgtls, cgbp+96, cgbp+64) != 0) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "tgeneric: results differ for mpfr_erfc on\n\x00", 0)
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "prec = %u, rnd_mode = %s\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aaprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
									Xprintf(cgtls, "Got      \x00", 0)
									Xmpfr_dump(cgtls, cgbp+64)
									Xprintf(cgtls, "Expected \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xprintf(cgtls, "Approx   \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xexit(cgtls, ppint32(1))
								}
								aacompare2 = Xmpfr_cmp3(cgtls, cgbp+96, cgbp+32, ppint32(1))
								/* if rounding to nearest, cannot know the sign of t - f(x)
								   because of composed rounding: y = o(f(x)) and t = o(y) */
								if aacompare*aacompare2 >= 0 {
									aacompare = aacompare + aacompare2
								} else {
									aacompare = aainexact
								} /* cannot determine sign(t-f(x)) */
								if !(iqlibc.ppBoolInt32(aainexact > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aainexact < iqlibc.ppInt32FromInt32(0)) == iqlibc.ppBoolInt32(aacompare > iqlibc.ppInt32FromInt32(0))-iqlibc.ppBoolInt32(aacompare < iqlibc.ppInt32FromInt32(0))) && aarnd != ppint32(ecMPFR_RNDF) {

									Xprintf(cgtls, "Wrong inexact flag for rnd=%s: expected %d, got %d\n\x00", iqlibc.ppVaList(cgbp+232, Xmpfr_print_rnd_mode(cgtls, aarnd), aacompare, aainexact))
									Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
									Xmpfr_dump(cgtls, cgbp)
									Xprintf(cgtls, "y = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+32)
									Xprintf(cgtls, "t = \x00", 0)
									Xmpfr_dump(cgtls, cgbp+96)
									Xexit(cgtls, ppint32(1))
								}
							} else {
								if Xgetenv(cgtls, "MPFR_SUSPICIOUS_OVERFLOW\x00") != iqlibc.ppUintptrFromInt32(0) {

									/* For developers only! */

									if ccv12 = !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp <= -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3)); ccv12 {
									}
									if ccv13 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(ccv12 && iqlibc.Bool(iqlibc.ppInt32FromInt32(1) != 0))), ppint64(1)) != 0; !ccv13 {
										Xmpfr_assert_fail(cgtls, "tgeneric.c\x00", ppint32(803), "(!(((y)->_mpfr_exp) <= (((-0x7fffffffffffffffL-1))+3)) && ((! __builtin_constant_p (!!(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) || !(((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0))) || (((y)->_mpfr_exp) >= (1-((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))) && ((y)->_mpfr_exp) <= (((mpfr_exp_t) 1 << ((64 - 0)*sizeof(mpfr_exp_t)/sizeof(mp_limb_t)-2))-1) && (((mp_limb_t) ((((y)->_mpfr_d)[((((! __builtin_constant_p (!!(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) || !(((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256))))) || (((((y)->_mpfr_prec)) >= 1 && (((y)->_mpfr_prec)) <= ((mpfr_prec_t) ((((mpfr_uprec_t) -1) >> 1) - 256)))) ? (void) 0 : __builtin_unreachable()), (((y)->_mpfr_prec))) - 1) / (64 - 0))]) & ((((mp_limb_t) 1)) << ((64 - 0) - 1)))) != 0)) ? (void) 0 : __builtin_unreachable()), 1))\x00")
									}
									pp_ = ccv13 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
									Xmpfr_nexttoinf(cgtls, cgbp+32)

									if ccv15 = (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(3); ccv15 {
										if ccv14 = aarnd == ppint32(ecMPFR_RNDZ); !ccv14 {
										}
									}
									if ccv15 && (ccv14 || aarnd+iqlibc.ppBoolInt32((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < iqlibc.ppInt32FromInt32(0)) == ppint32(ecMPFR_RNDD)) && !(iqlibc.ppInt32FromUint32(X__gmpfr_flags&iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_OVERFLOW)) != 0) && iqlibc.Bool(ppint32(mvTGENERIC_SO_TEST) != 0) {

										Xprintf(cgtls, "Possible bug! |y| is the maximum finite number (with yprec = %u) and has\nbeen obtained when rounding toward zero (%s). Thus there is a very\nprobable overflow, but the overflow flag is not set!\n\x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64(aayprec), Xmpfr_print_rnd_mode(cgtls, aarnd)))
										Xprintf(cgtls, "x1[%u] = \x00", iqlibc.ppVaList(cgbp+232, iqlibc.ppUint32FromInt64((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp)).fd_mpfr_prec)))
										Xmpfr_dump(cgtls, cgbp)
										Xexit(cgtls, ppint32(1))
									}
								}
							}
						}
					}
				}
			}

			goto ppnext_n
		ppnext_n:
			;
			/* In case the exponent range has been changed by
			   tests_default_random() or for special values... */
			Xset_emin(cgtls, aaold_emin)
			Xset_emax(cgtls, aaold_emax)

			goto cg_2
		cg_2:
			;
			aan++
		}

		goto cg_1
	cg_1:
		;
		aaprec++
	}

	if Xgetenv(cgtls, "MPFR_TGENERIC_STAT\x00") != iqlibc.ppUintptrFromInt32(0) {
		Xprintf(cgtls, "tgeneric: normal cases / total = %lu / %lu\n\x00", iqlibc.ppVaList(cgbp+232, aactrn, aactrt))
	}

	if ppuint64(3)*aactrn < ppuint64(2)*aactrt {
		Xprintf(cgtls, "Warning! Too few normal cases in generic tests (%lu / %lu)\n\x00", iqlibc.ppVaList(cgbp+232, aactrn, aactrt))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+232, cgbp+32, cgbp+160, cgbp+192, cgbp+64, cgbp+96, cgbp+128, iqlibc.ppUintptrFromInt32(0)))
}

func sispecial_erf(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(80)
	defer cgtls.ppFree(80)

	var aa_p tnmpfr_ptr
	var aainex, ccv1 ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_ = aa_p, aainex, ccv1

	Xmpfr_init2(cgtls, cgbp, ppint64(53))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(53))

	/* erf(NaN) = NaN */
	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_erf(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "mpfr_erf failed for x=NaN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* erf(+Inf) = 1 */
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	Xmpfr_erf(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=+Inf\n\x00", 0)
		Xprintf(cgtls, "expected 1.0, got \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* erf(-Inf) = -1 */
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_erf(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_si_2exp(cgtls, cgbp+32, ppint64(-iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=-Inf\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* erf(+0) = +0 */
	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1
	Xmpfr_erf(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp != -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=+0\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* erf(-0) = -0 */
	Xmpfr_neg(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_erf(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp != -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=-0\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.11010111101110110011110100111010000010000100010001011\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=1.0, rnd=MPFR_RNDN\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp, "6.6\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=6.6, rnd=MPFR_RNDN\n\x00", 0)
		Xprintf(cgtls, "expected 1\n\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp, "-6.6\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=-6.6, rnd=MPFR_RNDN\n\x00", 0)
		Xprintf(cgtls, "expected -1\n\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp, "6.6\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDZ))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.11111111111111111111111111111111111111111111111111111\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=6.6, rnd=MPFR_RNDZ\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str(cgtls, cgbp, "4.5\x00", ppint32(10), ppint32(ecMPFR_RNDN))
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.1111111111111111111111111111111100100111110100011\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=4.5, rnd=MPFR_RNDN\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(120))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(120))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.110100110011001100110011001100110011001100110011001100110011001100110011001100110011001100110011001100110011001100110011E3\x00")
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.11111111111111111111111111111111111111111111111111111111111111111100111111000100111011111011010000110101111100011001101\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=6.6, rnd=MPFR_RNDN\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(8))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(8))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(50)), 0, ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_erf(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=50, rnd=MPFR_RNDN\n\x00", 0)
		Xprintf(cgtls, "expected 1, got \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if aainex <= 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=50, rnd=MPFR_RNDN: wrong ternary value\nexpected positive, got %d\n\x00", iqlibc.ppVaList(cgbp+72, aainex))
		Xexit(cgtls, ppint32(1))
	}
	aainex = Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDZ))
	Xmpfr_nextbelow(cgtls, cgbp+32)
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=50, rnd=MPFR_RNDZ\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xprintf(cgtls, "got      \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(2), ppuint64(0), cgbp, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if aainex >= 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=50, rnd=MPFR_RNDN: wrong ternary value\nexpected negative, got %d\n\x00", iqlibc.ppVaList(cgbp+72, aainex))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(32))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(32))

	Xmpfr_set_str_binary(cgtls, cgbp, "0.1010100100111011001111100101E-1\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.10111000001110011010110001101011E-1\x00")
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str_binary(cgtls, cgbp, "-0.10110011011010111110010001100001\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "-0.1010110110101011100010111000111\x00")
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (2)\n\x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str_binary(cgtls, cgbp, "100.10001110011110100000110000111\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.11111111111111111111111111111111\x00")
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (3)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str_binary(cgtls, cgbp, "100.10001110011110100000110000111\x00")
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDZ))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (4)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str_binary(cgtls, cgbp, "100.10001110011110100000110000111\x00")
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (5)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str_binary(cgtls, cgbp, "100.10001110011110100000110001000\x00")
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (6)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str_binary(cgtls, cgbp, "100.10001110011110100000110001000\x00")
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.11111111111111111111111111111111\x00")
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDZ))
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (7)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str_binary(cgtls, cgbp, "100.10001110011110100000110001000\x00")
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (8)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(5)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (9)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(5)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDU))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (10)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(5)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDZ))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.11111111111111111111111111111111\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (11)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(5)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_erf(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp+32, "0.11111111111111111111111111111111\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for prec=32 (12)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(43))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(64))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.1101110110101111100101011101110101101001001e3\x00")
	Xmpfr_erf(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDU))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(64))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.1111111111111111111111111111111111111111111111111111111111111111\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for prec=43,64 (13)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	/* worst cases */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(53))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.0000000000000000000000000000000000000110000000101101\x00")
	Xmpfr_erf(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.110101111011101100111101001110100000101011000011001\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for worst case (1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_str_binary(cgtls, cgbp, "1.0000000000000000000000000000011000111010101101011010\x00")
	Xmpfr_erf(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDU))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11010111101110110011110100111100100111100011111000110\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for worst case (2a)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_set_str_binary(cgtls, cgbp, "1.0000000000000000000000000000011000111010101101011010\x00")
	Xmpfr_erf(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDD))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.11010111101110110011110100111100100111100011111000101\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "Error: erf for worst case (2b)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
}

func sispecial_erfc(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aa_p tnmpfr_ptr
	var ccv1 ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_ = aa_p, ccv1

	Xmpfr_inits(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+32, iqlibc.ppUintptrFromInt32(0)))

	/* erfc (NaN) = NaN */
	Xmpfr_set_nan(cgtls, cgbp)
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == iqlibc.ppInt64FromInt32(1)-iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))) {

		Xprintf(cgtls, "mpfr_erfc failed for x=NaN\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	/* erfc(+Inf) = 0+ */
	Xmpfr_set_inf(cgtls, cgbp, ppint32(1))
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp == -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1)) || !((*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign > iqlibc.ppInt32FromInt32(0)) {

		Xprintf(cgtls, "mpfr_erf failed for x=+Inf\n\x00", 0)
		Xprintf(cgtls, "expected 0+, got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}
	/* erfc(-Inf) = 2 */
	Xmpfr_set_inf(cgtls, cgbp, -ppint32(1))
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=-Inf\n\x00", 0)
		Xprintf(cgtls, "expected 2, got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}
	/* erf(+0) = 1 */
	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(1)), 0) != 0 {

		Xprintf(cgtls, "mpfr_erf failed for x=+0\n\x00", 0)
		Xprintf(cgtls, "expected 1, got \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+72, cgbp+32, iqlibc.ppUintptrFromInt32(0)))
}

func silarge_arg(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(96)
	defer cgtls.ppFree(96)

	var aa_p, aa_p1 tnmpfr_ptr
	var aaflags ppuint32
	var ccv1, ccv2 ppint32
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aa_p, aa_p1, aaflags, ccv1, ccv2

	Xmpfr_init2(cgtls, cgbp, ppint64(88))
	Xmpfr_init2(cgtls, cgbp+32, ppint64(98))

	Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-ppint32(1)), ppint64(173), ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	aaflags = X__gmpfr_flags
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) != 0 {

		Xprintf(cgtls, "mpfr_erfc failed for large x (1)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if aaflags != ppuint32(mvMPFR_FLAGS_INEXACT) {

		Xprintf(cgtls, "mpfr_erfc sets incorrect flags for large x (1)\n\x00", 0)
		Xprintf(cgtls, "Expected %u, got %u\n\x00", iqlibc.ppVaList(cgbp+72, iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT), aaflags))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-ppint32(1)), X__gmpfr_emax-ppint64(3), ppint32(ecMPFR_RNDN))
	Xmpfr_clear_flags(cgtls)
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	aaflags = X__gmpfr_flags
	if Xmpfr_cmp_ui_2exp(cgtls, cgbp+32, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) != 0 {

		Xprintf(cgtls, "mpfr_erfc failed for large x (1b)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if aaflags != ppuint32(mvMPFR_FLAGS_INEXACT) {

		Xprintf(cgtls, "mpfr_erfc sets incorrect flags for large x (1b)\n\x00", 0)
		Xprintf(cgtls, "Expected %u, got %u\n\x00", iqlibc.ppVaList(cgbp+72, iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT), aaflags))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(33))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(43))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.11000101010111011000111100101001e6\x00")
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDD))
	Xmpfr_set_prec(cgtls, cgbp, ppint64(43))
	Xmpfr_set_str_binary(cgtls, cgbp, "100010011100101100001101100101011101101E-18579\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_erfc failed for large x (2)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(43))
	Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(1), ppint64(11), ppint32(ecMPFR_RNDN))
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.1100000100100010101111001111010010001000110E-6051113\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_erfc failed for large x (3)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(75))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(85))
	Xmpfr_set_str_binary(cgtls, cgbp, "0.111110111111010011101011001100001010011110101010011111010010111101010001011E15\x00")
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_exp != -iqlibc.ppInt64FromInt64(0x7fffffffffffffff)-iqlibc.ppInt64FromInt32(1)+iqlibc.ppInt64FromInt32(1) || (*tn__mpfr_struct)(iqunsafe.ppPointer(cgbp+32)).fd_mpfr_sign < 0 {

		Xprintf(cgtls, "mpfr_erfc failed for large x (3b)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(21))
	Xmpfr_set_str_binary(cgtls, cgbp, "-1.0e3\x00")
	Xmpfr_clear_flags(cgtls)
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDZ))
	aaflags = X__gmpfr_flags
	Xmpfr_set_prec(cgtls, cgbp, ppint64(21))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.11111111111111111111\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_erfc failed for large x (4)\n\x00", 0)
		Xexit(cgtls, ppint32(1))
	}
	if aaflags != ppuint32(mvMPFR_FLAGS_INEXACT) {

		Xprintf(cgtls, "mpfr_erfc sets incorrect flags for large x (4)\n\x00", 0)
		Xprintf(cgtls, "Expected %u, got %u\n\x00", iqlibc.ppVaList(cgbp+72, iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT), aaflags))
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_set_prec(cgtls, cgbp, ppint64(2))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(31))
	Xmpfr_set_str_binary(cgtls, cgbp, "-1.0e3\x00")
	Xmpfr_clear_flags(cgtls)
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDZ))
	aaflags = X__gmpfr_flags
	Xmpfr_set_prec(cgtls, cgbp, ppint64(31))
	Xmpfr_set_str_binary(cgtls, cgbp, "1.111111111111111111111111111111\x00")
	if Xmpfr_cmp3(cgtls, cgbp, cgbp+32, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_erfc failed for x=-8, prec=31 (5)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}
	if aaflags != ppuint32(mvMPFR_FLAGS_INEXACT) {

		Xprintf(cgtls, "mpfr_erfc sets incorrect flags for large x (5)\n\x00", 0)
		Xprintf(cgtls, "Expected %u, got %u\n\x00", iqlibc.ppVaList(cgbp+72, iqlibc.ppUint32FromInt32(mvMPFR_FLAGS_INEXACT), aaflags))
		Xexit(cgtls, ppint32(1))
	}

	/* Reported by Christopher Creutzig on 2007-07-10. */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(53))
	Xmpfr_set_prec(cgtls, cgbp+32, ppint64(53))
	Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(54563), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN))
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDZ))
	{
		aa_p = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv1 = 0
	}
	pp_ = ccv1
	if !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp) != 0) {

		Xprintf(cgtls, "mpfr_erfc failed for x=27281.5, prec=53 (6)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	/* same test with rounding away from zero */
	Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(54563), ppint64(-ppint32(1)), ppint32(ecMPFR_RNDN))
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDU))
	{
		aa_p1 = cgbp
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_sign = ppint32(1)
		(*tn__mpfr_struct)(iqunsafe.ppPointer(aa_p1)).fd_mpfr_exp = iqlibc.ppInt64FromInt32(0) - iqlibc.ppInt64FromUint64(iqlibc.ppUint64FromInt32(-iqlibc.ppInt32FromInt32(1))>>iqlibc.ppInt32FromInt32(1))
		pp_ = ppint32(ecMPFR_RNDN)
		ccv2 = 0
	}
	pp_ = ccv2
	Xmpfr_nextabove(cgtls, cgbp)
	if !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp) != 0) {

		Xprintf(cgtls, "mpfr_erfc failed for x=27281.5, prec=53 (7)\n\x00", 0)
		Xprintf(cgtls, "expected \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got      \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xexit(cgtls, ppint32(1))
	}

	Xmpfr_clear(cgtls, cgbp)
	Xmpfr_clear(cgtls, cgbp+32)
}

func sitest_erfc(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaemin tnmpfr_exp_t
	var aainex ppint32
	var ccv2, ccv4 ppbool
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	var pp_ /* z at bp+64 */ tnmpfr_t
	pp_, pp_, pp_, pp_ = aaemin, aainex, ccv2, ccv4

	Xmpfr_inits2(cgtls, ppint64(40), cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))

	Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-ppint32(1)), ppint64(-ppint32(10)), ppint32(ecMPFR_RNDN))
	Xmpfr_set_str_binary(cgtls, cgbp+64, "0.1000000000100100000110111010110111100000E1\x00")
	Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))
	if Xmpfr_cmp3(cgtls, cgbp+32, cgbp+64, ppint32(1)) != 0 {

		Xprintf(cgtls, "mpfr_erfc failed for x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "got        \x00", 0)
		Xmpfr_dump(cgtls, cgbp+32)
		Xprintf(cgtls, "instead of \x00", 0)
		Xmpfr_dump(cgtls, cgbp+64)
		Xexit(cgtls, ppint32(1))
	}

	/* slowness detected by Kevin Rauch on 26 Oct 2007 */
	Xmpfr_set_prec(cgtls, cgbp, ppint64(128))
	pp_ = Xmpfr_set_si_2exp(cgtls, cgbp, ppint64(-iqlibc.ppInt32FromInt32(256)), 0, ppint32(ecMPFR_RNDN))
	aainex = Xmpfr_erfc(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))

	if ccv2 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(aainex > 0 && Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) == 0)), ppint64(1)) != 0; !ccv2 {
		Xmpfr_assert_fail(cgtls, "terf.c\x00", ppint32(586), "inex > 0 && (__builtin_constant_p (2) && (mpfr_ulong) (2) == 0 ? (mpfr_sgn) (x) : mpfr_cmp_ui_2exp ((x), (2), 0)) == 0\x00")
		if !(iqlibc.Bool(!(0 != 0)) || aainex > 0 && Xmpfr_cmp_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(2)), 0) == 0) {
			X__builtin_unreachable(cgtls)
		}
	}
	pp_ = ccv2 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)

	/* bug found by Pascal Molin on March 10, 2011 */
	aaemin = X__gmpfr_emin
	if !(Xmpfr_set_emin(cgtls, ppint64(-ppint32(1073808789))) != 0) {

		/* Typically, a 64-bit machine. */
		pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt64(ppint64(iqlibc.ppInt32FromInt32(27282))), 0, ppint32(ecMPFR_RNDN))
		Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDN))

		if ccv4 = X__builtin_expect(cgtls, iqlibc.ppBoolInt64(!!(Xmpfr_sgn(cgtls, cgbp+32) != iqlibc.ppInt32FromInt32(0))), ppint64(1)) != 0; !ccv4 {
			Xmpfr_assert_fail(cgtls, "terf.c\x00", ppint32(595), "(__builtin_constant_p (0) && (mpfr_ulong) (0) == 0 ? (mpfr_sgn) (y) : mpfr_cmp_ui_2exp ((y), (0), 0)) != 0\x00")
			if !(iqlibc.Bool(!(0 != 0)) || Xmpfr_sgn(cgtls, cgbp+32) != 0) {
				X__builtin_unreachable(cgtls)
			}
		}
		pp_ = ccv4 || iqlibc.Bool(iqlibc.ppInt32FromInt32(0) != 0)
		Xset_emin(cgtls, aaemin)
	}

	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
}

// C documentation
//
//	/* Failure in r7569 (2011-03-15) due to incorrect flags. */
func sireduced_expo_range(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(128)
	defer cgtls.ppFree(128)

	var aaemax tnmpfr_exp_t
	var aaex_flags, aaflags ppuint32
	var aaex_inex, aainex ppint32
	var pp_ /* ex_y at bp+64 */ tnmpfr_t
	var pp_ /* x at bp+0 */ tnmpfr_t
	var pp_ /* y at bp+32 */ tnmpfr_t
	pp_, pp_, pp_, pp_, pp_ = aaemax, aaex_flags, aaex_inex, aaflags, aainex

	aaemax = X__gmpfr_emax
	Xset_emax(cgtls, ppint64(3))
	Xmpfr_init2(cgtls, cgbp, ppint64(33))
	Xmpfr_inits2(cgtls, ppint64(110), cgbp+32, iqlibc.ppVaList(cgbp+104, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xmpfr_set_str_binary(cgtls, cgbp, "-0.111100110111111111011101010101110E3\x00")
	Xmpfr_clear_flags(cgtls)
	aainex = Xmpfr_erfc(cgtls, cgbp+32, cgbp, ppint32(ecMPFR_RNDZ))
	aaflags = X__gmpfr_flags
	Xmpfr_set_str(cgtls, cgbp+64, "1.fffffffffffffffffffffe607440\x00", ppint32(16), ppint32(ecMPFR_RNDN))
	aaex_inex = -ppint32(1)
	aaex_flags = ppuint32(mvMPFR_FLAGS_INEXACT)
	if iqlibc.ppBoolInt32(aainex > 0)-iqlibc.ppBoolInt32(aainex < 0) != aaex_inex || aaflags != aaex_flags || !(Xmpfr_equal_p(cgtls, cgbp+32, cgbp+64) != 0) {

		Xprintf(cgtls, "Error in reduced_expo_range\non x = \x00", 0)
		Xmpfr_dump(cgtls, cgbp)
		Xprintf(cgtls, "Expected y = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp+64, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n         inex = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+104, aaex_inex, aaex_flags))
		Xprintf(cgtls, "Got      y = \x00", 0)
		X__gmpfr_out_str(cgtls, Xstdout, ppint32(16), ppuint64(0), cgbp+32, ppint32(ecMPFR_RNDN))
		Xprintf(cgtls, "\n         inex = %d, flags = %u\n\x00", iqlibc.ppVaList(cgbp+104, iqlibc.ppBoolInt32(aainex > 0)-iqlibc.ppBoolInt32(aainex < 0), aaflags))
		Xexit(cgtls, ppint32(1))
	}
	Xmpfr_clears(cgtls, cgbp, iqlibc.ppVaList(cgbp+104, cgbp+32, cgbp+64, iqlibc.ppUintptrFromInt32(0)))
	Xset_emax(cgtls, aaemax)
}

// C documentation
//
//	/* Similar to a bug reported by Naoki Shibata:
//	   https://sympa.inria.fr/sympa/arc/mpfr/2018-07/msg00028.html
//	*/
func sibug20180723(cgtls *iqlibc.ppTLS) {
	cgbp := cgtls.ppAlloc(32)
	defer cgtls.ppFree(32)

	var pp_ /* x at bp+0 */ tnmpfr_t

	Xmpfr_init2(cgtls, cgbp, ppint64(256))
	pp_ = Xmpfr_set_ui_2exp(cgtls, cgbp, iqlibc.ppUint64FromInt32(iqlibc.ppInt32FromInt32(28)), 0, ppint32(ecMPFR_RNDN))
	Xmpfr_erfc(cgtls, cgbp, cgbp, ppint32(ecMPFR_RNDN))
	Xmpfr_clear(cgtls, cgbp)
}

func Xmain(cgtls *iqlibc.ppTLS, aaargc ppint32, aaargv ppuintptr) (cgr ppint32) {

	Xtests_start_mpfr(cgtls)

	sispecial_erf(cgtls)
	sispecial_erfc(cgtls)
	silarge_arg(cgtls)
	sitest_erfc(cgtls)
	sireduced_expo_range(cgtls)
	sibug20180723(cgtls)

	sitest_generic_erf(cgtls, ppint64(mvMPFR_PREC_MIN), ppint64(300), ppuint32(150))
	sitest_generic_erfc(cgtls, ppint64(mvMPFR_PREC_MIN), ppint64(300), ppuint32(150))

	Xdata_check(cgtls, "data/erf\x00", pp__ccgo_fp(Xmpfr_erf), "mpfr_erf\x00")
	Xdata_check(cgtls, "data/erfc\x00", pp__ccgo_fp(Xmpfr_erfc), "mpfr_erfc\x00")

	Xtests_end_mpfr(cgtls)
	return 0
}

func ppmain() {
	iqlibc.ppStart(Xmain)
}

func ___builtin_expect(*iqlibc.ppTLS, ppint64, ppint64) ppint64

func ___builtin_unreachable(*iqlibc.ppTLS)

var ___gmpfr_emax ppint64

var ___gmpfr_emin ppint64

var ___gmpfr_flags ppuint32

func ___gmpfr_out_str(*iqlibc.ppTLS, ppuintptr, ppint32, ppuint64, ppuintptr, ppint32) ppuint64

func _data_check(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppuintptr)

func _exit(*iqlibc.ppTLS, ppint32)

func _flags_out(*iqlibc.ppTLS, ppuint32)

func _getenv(*iqlibc.ppTLS, ppuintptr) ppuintptr

func _mpfr_assert_fail(*iqlibc.ppTLS, ppuintptr, ppint32, ppuintptr)

func _mpfr_can_round(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32, ppint32, ppint64) ppint32

func _mpfr_clear(*iqlibc.ppTLS, ppuintptr)

func _mpfr_clear_flags(*iqlibc.ppTLS)

func _mpfr_clears(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_cmp3(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_cmp_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64) ppint32

func _mpfr_cmp_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64) ppint32

func _mpfr_dump(*iqlibc.ppTLS, ppuintptr)

func _mpfr_equal_p(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _mpfr_erf(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_erfc(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_init2(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_inits(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_inits2(*iqlibc.ppTLS, ppint64, ppuintptr, ppuintptr)

func _mpfr_neg(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32) ppint32

func _mpfr_nextabove(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nextbelow(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttoinf(*iqlibc.ppTLS, ppuintptr)

func _mpfr_nexttozero(*iqlibc.ppTLS, ppuintptr)

func _mpfr_prec_round(*iqlibc.ppTLS, ppuintptr, ppint64, ppint32) ppint32

func _mpfr_print_rnd_mode(*iqlibc.ppTLS, ppint32) ppuintptr

func _mpfr_set4(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_emin(*iqlibc.ppTLS, ppint64) ppint32

func _mpfr_set_exp(*iqlibc.ppTLS, ppuintptr, ppint64) ppint32

func _mpfr_set_inf(*iqlibc.ppTLS, ppuintptr, ppint32)

func _mpfr_set_nan(*iqlibc.ppTLS, ppuintptr)

func _mpfr_set_prec(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_set_si_2exp(*iqlibc.ppTLS, ppuintptr, ppint64, ppint64, ppint32) ppint32

func _mpfr_set_str(*iqlibc.ppTLS, ppuintptr, ppuintptr, ppint32, ppint32) ppint32

func _mpfr_set_str_binary(*iqlibc.ppTLS, ppuintptr, ppuintptr)

func _mpfr_set_ui_2exp(*iqlibc.ppTLS, ppuintptr, ppuint64, ppint64, ppint32) ppint32

func _mpfr_setmax(*iqlibc.ppTLS, ppuintptr, ppint64)

func _mpfr_sgn(*iqlibc.ppTLS, ppuintptr) ppint32

func _printf(*iqlibc.ppTLS, ppuintptr, ppuintptr) ppint32

func _randlimb(*iqlibc.ppTLS) ppuint64

func _set_emax(*iqlibc.ppTLS, ppint64)

func _set_emin(*iqlibc.ppTLS, ppint64)

var _stdout ppuintptr

func _tests_default_random(*iqlibc.ppTLS, ppuintptr, ppint32, ppint64, ppint64, ppint32)

func _tests_end_mpfr(*iqlibc.ppTLS)

func _tests_start_mpfr(*iqlibc.ppTLS)

const ___ccgo_meta_json = `{
	"Aliases": {},
	"Visibility": {},
	"WeakAliases": {}
}`
